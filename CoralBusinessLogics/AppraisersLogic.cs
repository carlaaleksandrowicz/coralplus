﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoralBusinessLogics
{
    //מחלקה העוסקת בשמאים
    public class AppraisersLogic
    {
        //מחזיר רשימה של כל השמאים
        public List<Appraiser> GetAllAppraisers()
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.Appraisers.OrderByDescending(a => a.Status).ThenBy(a => a.AppraiserName).ToList();
            }
        }

        //מחזיר רשימה של כל השמאים הפעילים
        public List<Appraiser> GetAllActiveAppraisers()
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.Appraisers.Where(a => a.Status == true).OrderBy(a => a.AppraiserName).ToList();
            }
        }

        //מוסיף שמאי למערכת
        public Appraiser InsertAppraiser(string appraiserName, bool status)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var appraiserInContext = context.Appraisers.FirstOrDefault(a => a.AppraiserName == appraiserName);
                if (appraiserInContext == null)
                {
                    try
                    {
                        Appraiser newAppraiser = new Appraiser() { AppraiserName = appraiserName, Status = status };
                        context.Appraisers.Add(newAppraiser);
                        context.SaveChanges();
                        return newAppraiser;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }
                else
                {
                    throw new Exception("שמאי קיים במערכת");
                }
            }
        }

        //מעדכן שמאי למערכת
        public Appraiser UpdateAppraiserStatus(string appraiserName, bool status)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var appraiserInContext = context.Appraisers.FirstOrDefault(a => a.AppraiserName == appraiserName);
                if (appraiserInContext != null)
                {
                    try
                    {
                        appraiserInContext.Status = status;
                        context.SaveChanges();
                        return appraiserInContext;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }
                else
                {
                    throw new Exception("שמאי לא קיים במערכת");
                }
            }
        }
    }
}
