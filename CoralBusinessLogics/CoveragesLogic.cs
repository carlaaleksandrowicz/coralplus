﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoralBusinessLogics
{
    public class CoveragesLogic
    {
        //מחזיר רשימה של כל סוגי הכיסוים האלמנטריים לפי סוג ביטוח
        public List<ElementaryCoverageType> GetAllElementaryCoverageTypesByIndustry(int industryId)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.ElementaryCoverageTypes.Where(c => c.InsuranceIndustryID == industryId).OrderByDescending(c => c.Status).ThenBy(c => c.ElementaryCoverageTypeName).ToList();
            }
        }

        //מחזיר רשימה של סוגי הכיסוים האלמנטריים האקטיבים לפי סוג ביטוח
        public List<ElementaryCoverageType> GetAllActiveElementaryCoverageTypesByIndustry(int industryId)
        {
            List<ElementaryCoverageType> activeCoverages = GetAllElementaryCoverageTypesByIndustry(industryId).Where(c => c.Status == true).ToList();
            return activeCoverages;
        }

        //מוסיף סוג כיסוי אלמנטרי
        public ElementaryCoverageType InsertElementaryCoverageType(int industryId, string coverageCode, string coverageName, bool status)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                ElementaryCoverageType coverage = new ElementaryCoverageType();
                coverage.InsuranceIndustryID = industryId;
                coverage.ElementaryCoverageCode = coverageCode;
                coverage.ElementaryCoverageTypeName = coverageName;
                coverage.Status = status;
                try
                {
                    context.ElementaryCoverageTypes.Add(coverage);
                    context.SaveChanges();
                    return coverage;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
        }

        //מעדכן סוג כיסוי אלמנטרי
        public ElementaryCoverageType UpdateElementaryCoverageType(ElementaryCoverageType coverage, bool status)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var coverageInContext = context.ElementaryCoverageTypes.FirstOrDefault(c => c.ElementaryCoverageTypeID == coverage.ElementaryCoverageTypeID);
                coverageInContext.Status = status;
                if (coverageInContext != null)
                {
                    try
                    {
                        context.SaveChanges();
                        return coverageInContext;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }
                else
                {
                    throw new Exception("כיסוי לא קיים במערכת");
                }
            }
        }

        //מחזיר רשימה של כל הכיסוים לפי פוליסה
        public List<ElementaryCoverage> GetAllCoveragesByPolicy(string policyNumber, int? addition)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                //var policyInContext = context.ElementaryPolicies.FirstOrDefault(p => p.PolicyNumber == policyNumber);
                var policyInContext = context.ElementaryPolicies.Where(p => p.PolicyNumber == policyNumber).Where(p => p.Addition == addition).FirstOrDefault();

                if (policyInContext != null)
                {
                    return context.ElementaryCoverages.Include("ElementaryCoverageType").Where(c => c.ElementaryPolicyID == policyInContext.ElementaryPolicyID).OrderByDescending(c => c.Date).ToList();
                }
                else
                {
                    throw new Exception("פוליסה לא קיימת במערכת");
                }
            }
        }

        //מוסיף כיסוים לפוליסה אלמנטרית
        public void InsertCoveragesToElementaryPolicy(int policyId,string policyNumber, int? addition, List<ElementaryCoverage> coveragesToAdd)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                //var policyInContext = context.ElementaryPolicies.Where(p => p.PolicyNumber == oldPolicyNumber)

                //var policiesInContext = context.ElementaryPolicies.Where(c => c.PolicyNumber == policyNumber).ToList();
                //var policyInContext = context.ElementaryPolicies.Where(c => c.PolicyNumber == policyNumber).Where(p => p.Addition == addition).FirstOrDefault();
                var policyInContext = context.ElementaryPolicies.FirstOrDefault(c => c.ElementaryPolicyID == policyId);


                if (policyInContext != null)
                {
                    //foreach (var item in policiesInContext)
                    //{
                    try
                    {
                        foreach (ElementaryCoverage coverage in coveragesToAdd)
                        {
                            if (coverage.ElementaryCoverageID != 0)
                            {
                                ElementaryCoverage newCoverage = new ElementaryCoverage() { Comments = coverage.Comments, Addition = coverage.Addition, Date = coverage.Date, ElementaryPolicyID = policyInContext.ElementaryPolicyID, CoverageAmount = coverage.CoverageAmount, ElementaryCoverageTypeID = coverage.ElementaryCoverageTypeID, Porcentage = coverage.Porcentage, premium = coverage.premium };
                                context.ElementaryCoverages.Add(newCoverage);
                            }
                            else
                            {
                                coverage.ElementaryCoverageType = null;
                                coverage.ElementaryPolicyID = policyInContext.ElementaryPolicyID;
                                coverage.Addition = addition;
                                context.ElementaryCoverages.Add(coverage);
                            }

                            context.SaveChanges();
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                    //}

                }
                else
                {
                    throw new Exception("פוליסה לא קיימת במערכת");
                }
            }
        }

        //public void UpdateCoveragesToElementaryPolicy(string policyNumber, List<ElementaryCoverage> coveragesToAdd)
        //{
        //    using (Coral_DB_Entities context = new Coral_DB_Entities())
        //    {
        //        List<ElementaryCoverage> oldCoverages = context.ElementaryCoverages.Where(c => c.ElementaryPolicy.PolicyNumber == policyNumber).ToList();
        //        List<ElementaryCoverage> coveragesToDelete = context.ElementaryCoverages.Where(c => c.ElementaryPolicy.PolicyNumber == policyNumber).ToList();
        //        foreach (ElementaryCoverage oldCoverage in oldCoverages)
        //        {
        //            foreach (var newCoverage in coveragesToAdd)
        //            {
        //                //if ()
        //                //{

        //                //}
        //            }
        //        }
        //    }
        //}

        //מעדכן כיסוים לפוליסה אלמנטרית
        public void UpdateCoveragesToElementaryPolicy(int policyId, string policyNumber, int? addition, List<ElementaryCoverage> coveragesToAdd)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                //List<ElementaryCoverage> oldCoverages = context.ElementaryCoverages.Where(c => c.ElementaryPolicy.PolicyNumber == policyNumber).Where(c => c.ElementaryPolicy.Addition == addition).ToList();
                //List<ElementaryCoverage> oldCoveragesToDelete = context.ElementaryCoverages.Where(c => c.ElementaryPolicy.PolicyNumber == policyNumber).Where(c => c.ElementaryPolicy.Addition == addition).ToList();
                List<ElementaryCoverage> oldCoverages = context.ElementaryCoverages.Where(c => c.ElementaryPolicyID == policyId).ToList();
                List<ElementaryCoverage> oldCoveragesToDelete = context.ElementaryCoverages.Where(c => c.ElementaryPolicyID == policyId).ToList();

                foreach (ElementaryCoverage oldCoverage in oldCoverages)
                {
                    bool isBraked = false;
                    foreach (var newCoverage in coveragesToAdd)
                    {
                        //if (oldCoverage.ElementaryPolicy.Addition == addition)
                        if (oldCoverage.ElementaryCoverageID == newCoverage.ElementaryCoverageID)
                        //if (oldCoverage.ElementaryPolicy.PolicyNumber == newCoverage.ElementaryPolicy.PolicyNumber)
                        {
                            //לעדכן נתונים
                            var coverageInContext = context.ElementaryCoverages.FirstOrDefault(c => c.ElementaryCoverageID == oldCoverage.ElementaryCoverageID);
                            coverageInContext.ElementaryCoverageTypeID = newCoverage.ElementaryCoverageTypeID;
                            coverageInContext.CoverageAmount = newCoverage.CoverageAmount;
                            coverageInContext.Porcentage = newCoverage.Porcentage;
                            coverageInContext.premium = newCoverage.premium;
                            coverageInContext.Comments = newCoverage.Comments;
                            context.SaveChanges();
                            oldCoveragesToDelete.Remove(oldCoverage);
                            coveragesToAdd.Remove(newCoverage);
                            isBraked = true;
                            break;
                        }
                    }
                    //למחוק
                    if (!isBraked)
                    {
                        context.ElementaryCoverages.Remove(oldCoverage);
                        context.SaveChanges();
                    }
                }
                InsertCoveragesToElementaryPolicy(policyId,policyNumber, addition, coveragesToAdd);
            }
        }

        public List<PersonalAccidentsCoverage> GetAllPersonalAccidentsCoveragesByPolicy(int policyId)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var policyInContext = context.PersonalAccidentsPolicies.FirstOrDefault(p => p.PersonalAccidentsPolicyID == policyId);

                if (policyInContext != null)
                {
                    return context.PersonalAccidentsCoverages.Include("PersonalAccidentsCoverageType").Where(c => c.PersonalAccidentsPolicyID == policyInContext.PersonalAccidentsPolicyID).OrderByDescending(c => c.StartDate).ToList();
                }
                else
                {
                    throw new Exception("פוליסה לא קיימת במערכת");
                }
            }
        }

        public List<PersonalAccidentsCoverageType> GetAllActivePersonalAccidentsCoverageTypes()
        {              
            List<PersonalAccidentsCoverageType> activeCoverages = GetAllPersonalAccidentsCoverageTypes().Where(c => c.Status == true).OrderBy(c => c.PersonalAccidentsCoverageTypeName).ToList();
            return activeCoverages;
        }

        public List<PersonalAccidentsCoverageType> GetAllPersonalAccidentsCoverageTypes()
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.PersonalAccidentsCoverageTypes.OrderByDescending(c => c.Status).ThenBy(c => c.PersonalAccidentsCoverageTypeName).ToList();
            }
        }

        //מחזיר רשימה של סוגי כיסוים חיים לפי ענף
        public List<LifeCoverageType> GetAllLifeCoverageTypesByIndustry(int industryId)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.LifeCoverageTypes.Where(c => c.LifeIndustryID == industryId).OrderByDescending(c => c.Status).ThenBy(c => c.LifeCoverageTypeName).ToList();
            }
        }

        //מחזיר רשימה של סוגי כיסוים חיים אקטיבים לפי ענף
        public List<LifeCoverageType> GetAllActiveLifeCoverageTypesByIndustry(int industryId)
        {
            List<LifeCoverageType> activeCoverages = GetAllLifeCoverageTypesByIndustry(industryId).Where(c => c.Status == true).OrderBy(c => c.LifeCoverageTypeName).ToList();
            return activeCoverages;
        }

        //מוסיף סוג כיסוי חיים
        public LifeCoverageType InsertLifeCoverageType(int industryId, string coverageCode, string coverageName, bool status)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                LifeCoverageType coverage = new LifeCoverageType();
                coverage.LifeIndustryID = industryId;
                coverage.LifeCoverageCode = coverageCode;
                coverage.LifeCoverageTypeName = coverageName;
                coverage.Status = status;
                try
                {
                    context.LifeCoverageTypes.Add(coverage);
                    context.SaveChanges();
                    return coverage;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
        }

        //מעדכן סוג כיסוי חיים
        public LifeCoverageType UpdateLifeCoverageType(LifeCoverageType coverage, bool status)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var coverageInContext = context.LifeCoverageTypes.FirstOrDefault(c => c.LifeCoverageTypeID == coverage.LifeCoverageTypeID);
                
                if (coverageInContext != null)
                {
                    try
                    {
                        coverageInContext.Status = status;
                        context.SaveChanges();
                        return coverageInContext;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }
                else
                {
                    throw new Exception("כיסוי לא קיים במערכת");
                }
            }
        }

        //מחזיר רשימה של כל הכיסוים החיים לפי פוליסה
        public List<LifeCoverage> GetAllLifeCoveragesByPolicy(int policyId)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                //var policyInContext = context.ElementaryPolicies.FirstOrDefault(p => p.PolicyNumber == policyNumber);
                var policyInContext = context.LifePolicies.FirstOrDefault(p => p.LifePolicyID == policyId);

                if (policyInContext != null)
                {
                    return context.LifeCoverages.Include("LifeCoverageType").Where(c => c.LifePolicyID == policyInContext.LifePolicyID).OrderByDescending(c => c.StartDate).ToList();
                }
                else
                {
                    throw new Exception("פוליסה לא קיימת במערכת");
                }
            }
        }

        //מוסיף/מעדכן כיסוים לפוליסת חיים
        public void InsertCoveragesToLifePolicy(int policyId, List<LifeCoverage> coveragesToAdd, bool isAddition)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var policyInContext = context.LifePolicies.FirstOrDefault(c => c.LifePolicyID == policyId);

                if (policyInContext != null)
                {
                    try
                    {
                        var policyCoverages = context.LifeCoverages.Where(c => c.LifePolicyID == policyId).ToList();
                        foreach (LifeCoverage coverage in coveragesToAdd)
                        {
                            if (coverage.LifeCoverageID == 0 )
                            {
                                coverage.LifePolicyID = policyId;
                                coverage.Addition = policyInContext.Addition;
                                coverage.LifeCoverageType = null;
                                context.LifeCoverages.Add(coverage);
                            }
                            else if (isAddition)
                            {
                                LifeCoverage newCoverage = new LifeCoverage() { Addition = coverage.Addition, Comments = coverage.Comments, EndDate = coverage.EndDate, IndexedAmount = coverage.IndexedAmount, IndexedPremium = coverage.IndexedPremium, InsuranceAmount = coverage.InsuranceAmount, LifeCoverageTypeID = coverage.LifeCoverageTypeID, LifeCoverageType = null, LifePolicyID = policyId, MedicalAddition = coverage.MedicalAddition, MonthlyPremuim = coverage.MonthlyPremuim, ProfessionalAddition = coverage.ProfessionalAddition, StartDate = coverage.StartDate, Status = coverage.Status, UpToAge = coverage.UpToAge };
                                context.LifeCoverages.Add(newCoverage);
                            }
                            else
                            {
                                var coverageInContext = context.LifeCoverages.FirstOrDefault(c => c.LifeCoverageID == coverage.LifeCoverageID);
                                if (coverageInContext != null)
                                {
                                    coverageInContext.Addition = policyInContext.Addition;
                                    coverageInContext.Comments = coverage.Comments;
                                    coverageInContext.EndDate = coverage.EndDate;
                                    coverageInContext.IndexedAmount = coverage.IndexedAmount;
                                    coverageInContext.IndexedPremium = coverage.IndexedPremium;
                                    coverageInContext.InsuranceAmount = coverage.InsuranceAmount;
                                    coverageInContext.MedicalAddition = coverage.MedicalAddition;
                                    coverageInContext.MonthlyPremuim = coverage.MonthlyPremuim;
                                    coverageInContext.ProfessionalAddition = coverage.ProfessionalAddition;
                                    coverageInContext.StartDate = coverage.StartDate;
                                    coverageInContext.Status = coverage.Status;
                                    coverageInContext.UpToAge = coverage.UpToAge;
                                    coverageInContext.LifeCoverageType = null;
                                    coverageInContext.LifeCoverageTypeID = coverage.LifeCoverageTypeID;

                                    policyCoverages.Remove(coverageInContext);
                                }
                            }
                        }
                        if (policyCoverages.Count > 0)
                        {
                            foreach (var item in policyCoverages)
                            {
                                context.LifeCoverages.Remove(item);
                            }
                        }
                        context.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }
                else
                {
                    throw new Exception("פוליסה לא קיימת במערכת");
                }
            }
        }


        //מחזיר רשימה של סוגי כיסוים בריאות 
        public List<HealthCoverageType> GetAllHealthCoverageTypes()
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.HealthCoverageTypes.OrderByDescending(c => c.Status).ThenBy(c => c.HealthCoverageTypeName).ToList();
            }
        }

        //מחזיר רשימה של סוגי כיסוים בריאות אקטיבים 
        public List<HealthCoverageType> GetAllActiveHealthCoverageTypes()
        {
            List<HealthCoverageType> activeCoverages = GetAllHealthCoverageTypes().Where(c => c.Status == true).OrderBy(c => c.HealthCoverageTypeName).ToList();
            return activeCoverages;
        }

        //מוסיף סוג כיסוי בריאות
        public HealthCoverageType InsertHealthCoverageType(string coverageCode, string coverageName, bool status)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                HealthCoverageType coverage = new HealthCoverageType();
                
                coverage.HealthCoverageCode = coverageCode;
                coverage.HealthCoverageTypeName = coverageName;
                coverage.Status = status;
                try
                {
                    context.HealthCoverageTypes.Add(coverage);
                    context.SaveChanges();
                    return coverage;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
        }

        //מעדכן סוג כיסוי בריאות
        public HealthCoverageType UpdateHealthCoverageType(HealthCoverageType coverage, bool status)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var coverageInContext = context.HealthCoverageTypes.FirstOrDefault(c => c.HealthCoverageTypeID == coverage.HealthCoverageTypeID);
                
                if (coverageInContext != null)
                {
                    try
                    {
                        coverageInContext.Status = status;
                        context.SaveChanges();
                        return coverageInContext;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }
                else
                {
                    throw new Exception("כיסוי לא קיים במערכת");
                }
            }
        }

        //מחזיר רשימה של כל הכיסוים הבריאות לפי פוליסה
        public List<HealthCoverage> GetAllHealthCoveragesByPolicy(int policyId)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                //var policyInContext = context.ElementaryPolicies.FirstOrDefault(p => p.PolicyNumber == policyNumber);
                var policyInContext = context.HealthPolicies.FirstOrDefault(p => p.HealthPolicyID == policyId);

                if (policyInContext != null)
                {
                    return context.HealthCoverages.Include("HealthCoverageType").Where(c => c.HealthPolicyID == policyInContext.HealthPolicyID).OrderByDescending(c => c.StartDate).ToList();
                }
                else
                {
                    throw new Exception("פוליסה לא קיימת במערכת");
                }
            }
        }

        //מוסיף/מעדכן כיסוים לפוליסת בריאות
        public void InsertCoveragesToHealthPolicy(int policyId, List<HealthCoverage> coveragesToAdd, bool isAddition)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var policyInContext = context.HealthPolicies.FirstOrDefault(c => c.HealthPolicyID == policyId);

                if (policyInContext != null)
                {
                    try
                    {
                        var policyCoverages = context.HealthCoverages.Where(c => c.HealthPolicyID == policyId).ToList();
                        foreach (HealthCoverage coverage in coveragesToAdd)
                        {
                            if (coverage.HealthCoverageID == 0)
                            {
                                coverage.HealthPolicyID = policyId;
                                coverage.Addition = policyInContext.Addition;
                                coverage.HealthCoverageType = null;
                                context.HealthCoverages.Add(coverage);
                            }
                            else if (isAddition)
                            {
                                HealthCoverage newCoverage = new HealthCoverage() { Addition = coverage.Addition, Comments = coverage.Comments, EndDate = coverage.EndDate, IndexedAmount = coverage.IndexedAmount, IndexedPremium = coverage.IndexedPremium, InsuranceAmount = coverage.InsuranceAmount, HealthCoverageTypeID = coverage.HealthCoverageTypeID, HealthCoverageType = null, HealthPolicyID = policyId, MedicalAddition = coverage.MedicalAddition, MonthlyPremuim = coverage.MonthlyPremuim, ProfessionalAddition = coverage.ProfessionalAddition, StartDate = coverage.StartDate, Status = coverage.Status, Period = coverage.Period };
                                context.HealthCoverages.Add(newCoverage);
                            }
                            else
                            {
                                var coverageInContext = context.HealthCoverages.FirstOrDefault(c => c.HealthCoverageID == coverage.HealthCoverageID);
                                if (coverageInContext != null)
                                {
                                    coverageInContext.Addition = policyInContext.Addition;
                                    coverageInContext.Comments = coverage.Comments;
                                    coverageInContext.EndDate = coverage.EndDate;
                                    coverageInContext.IndexedAmount = coverage.IndexedAmount;
                                    coverageInContext.IndexedPremium = coverage.IndexedPremium;
                                    coverageInContext.InsuranceAmount = coverage.InsuranceAmount;
                                    coverageInContext.MedicalAddition = coverage.MedicalAddition;
                                    coverageInContext.MonthlyPremuim = coverage.MonthlyPremuim;
                                    coverageInContext.ProfessionalAddition = coverage.ProfessionalAddition;
                                    coverageInContext.StartDate = coverage.StartDate;
                                    coverageInContext.Status = coverage.Status;
                                    coverageInContext.Period = coverage.Period;
                                    coverageInContext.HealthCoverageType = null;
                                    coverageInContext.HealthCoverageTypeID = coverage.HealthCoverageTypeID;

                                    policyCoverages.Remove(coverageInContext);
                                }
                            }
                        }
                        if (policyCoverages.Count > 0)
                        {
                            foreach (var item in policyCoverages)
                            {
                                context.HealthCoverages.Remove(item);
                            }
                        }
                        context.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }
                else
                {
                    throw new Exception("פוליסה לא קיימת במערכת");
                }
            }
        }

        public PersonalAccidentsCoverageType InsertPersonalAccidentsCoverageType(string coverageCode, string coverageName, bool status)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                PersonalAccidentsCoverageType coverage = new PersonalAccidentsCoverageType();

                coverage.PersonalAccidentsCoverageCode = coverageCode;
                coverage.PersonalAccidentsCoverageTypeName = coverageName;
                coverage.Status = status;
                try
                {
                    context.PersonalAccidentsCoverageTypes.Add(coverage);
                    context.SaveChanges();
                    return coverage;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
        }

        public PersonalAccidentsCoverageType UpdatePersonalAccidentsCoverageType(PersonalAccidentsCoverageType coverage, bool status)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var coverageInContext = context.PersonalAccidentsCoverageTypes.FirstOrDefault(c => c.PersonalAccidentsCoverageTypeID == coverage.PersonalAccidentsCoverageTypeID);

                if (coverageInContext != null)
                {
                    try
                    {
                        coverageInContext.Status = status;
                        context.SaveChanges();
                        return coverageInContext;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }
                else
                {
                    throw new Exception("כיסוי לא קיים במערכת");
                }
            }
        }

        public void InsertCoveragesToPersonalAccidentsPolicy(int policyId, List<PersonalAccidentsCoverage> coveragesToAdd, bool isAddition)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var policyInContext = context.PersonalAccidentsPolicies.FirstOrDefault(c => c.PersonalAccidentsPolicyID == policyId);

                if (policyInContext != null)
                {
                    try
                    {
                        var policyCoverages = context.PersonalAccidentsCoverages.Where(c => c.PersonalAccidentsPolicyID == policyId).ToList();
                        foreach (PersonalAccidentsCoverage coverage in coveragesToAdd)
                        {
                            if (coverage.PersonalAccidentsCoverageID == 0)
                            {
                                coverage.PersonalAccidentsPolicyID = policyId;
                                coverage.Addition = policyInContext.Addition;
                                coverage.PersonalAccidentsCoverageType = null;
                                context.PersonalAccidentsCoverages.Add(coverage);
                            }
                            else if (isAddition)
                            {
                                PersonalAccidentsCoverage newCoverage = new PersonalAccidentsCoverage() { Addition = coverage.Addition, Comments = coverage.Comments, EndDate = coverage.EndDate, IndexedAmount = coverage.IndexedAmount, IndexedPremium = coverage.IndexedPremium, InsuranceAmount = coverage.InsuranceAmount, PersonalAccidentsCoverageTypeID = coverage.PersonalAccidentsCoverageTypeID, PersonalAccidentsCoverageType = null, PersonalAccidentsPolicyID = policyId, MedicalAddition = coverage.MedicalAddition, MonthlyPremuim = coverage.MonthlyPremuim, ProfessionalAddition = coverage.ProfessionalAddition, StartDate = coverage.StartDate, Status = coverage.Status, Period = coverage.Period };
                                context.PersonalAccidentsCoverages.Add(newCoverage);
                            }
                            else
                            {
                                var coverageInContext = context.PersonalAccidentsCoverages.FirstOrDefault(c => c.PersonalAccidentsCoverageID == coverage.PersonalAccidentsCoverageID);
                                if (coverageInContext != null)
                                {
                                    coverageInContext.Addition = policyInContext.Addition;
                                    coverageInContext.Comments = coverage.Comments;
                                    coverageInContext.EndDate = coverage.EndDate;
                                    coverageInContext.IndexedAmount = coverage.IndexedAmount;
                                    coverageInContext.IndexedPremium = coverage.IndexedPremium;
                                    coverageInContext.InsuranceAmount = coverage.InsuranceAmount;
                                    coverageInContext.MedicalAddition = coverage.MedicalAddition;
                                    coverageInContext.MonthlyPremuim = coverage.MonthlyPremuim;
                                    coverageInContext.ProfessionalAddition = coverage.ProfessionalAddition;
                                    coverageInContext.StartDate = coverage.StartDate;
                                    coverageInContext.Status = coverage.Status;
                                    coverageInContext.Period = coverage.Period;
                                    coverageInContext.PersonalAccidentsCoverageType = null;
                                    coverageInContext.PersonalAccidentsCoverageTypeID = coverage.PersonalAccidentsCoverageTypeID;

                                    policyCoverages.Remove(coverageInContext);
                                }
                            }
                        }
                        if (policyCoverages.Count > 0)
                        {
                            foreach (var item in policyCoverages)
                            {
                                context.PersonalAccidentsCoverages.Remove(item);
                            }
                        }
                        context.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }
                else
                {
                    throw new Exception("פוליסה לא קיימת במערכת");
                }
            }
        }


        ////public void UpdateCoveragesToElementaryPolicy(string policyNumber, List<ElementaryCoverage> coveragesToAdd)
        ////{
        ////    using (Coral_DB_Entities context = new Coral_DB_Entities())
        ////    {
        ////        List<ElementaryCoverage> oldCoverages = context.ElementaryCoverages.Where(c => c.ElementaryPolicy.PolicyNumber == policyNumber).ToList();
        ////        List<ElementaryCoverage> coveragesToDelete = context.ElementaryCoverages.Where(c => c.ElementaryPolicy.PolicyNumber == policyNumber).ToList();
        ////        foreach (ElementaryCoverage oldCoverage in oldCoverages)
        ////        {
        ////            foreach (var newCoverage in coveragesToAdd)
        ////            {
        ////                //if ()
        ////                //{

        ////                //}
        ////            }
        ////        }
        ////    }
        ////}

        //public void UpdateCoveragesToElementaryPolicy(string policyNumber, int? addition, List<ElementaryCoverage> coveragesToAdd)
        //{
        //    using (Coral_DB_Entities context = new Coral_DB_Entities())
        //    {
        //        List<ElementaryCoverage> oldCoverages = context.ElementaryCoverages.Where(c => c.ElementaryPolicy.PolicyNumber == policyNumber).Where(c => c.ElementaryPolicy.Addition == addition).ToList();
        //        List<ElementaryCoverage> oldCoveragesToDelete = context.ElementaryCoverages.Where(c => c.ElementaryPolicy.PolicyNumber == policyNumber).Where(c => c.ElementaryPolicy.Addition == addition).ToList();

        //        foreach (ElementaryCoverage oldCoverage in oldCoverages)
        //        {
        //            bool isBraked = false;
        //            foreach (var newCoverage in coveragesToAdd)
        //            {
        //                //if (oldCoverage.ElementaryPolicy.Addition == addition)
        //                if (oldCoverage.ElementaryCoverageID == newCoverage.ElementaryCoverageID)
        //                //if (oldCoverage.ElementaryPolicy.PolicyNumber == newCoverage.ElementaryPolicy.PolicyNumber)
        //                {
        //                    //לעדכן נתונים
        //                    var coverageInContext = context.ElementaryCoverages.FirstOrDefault(c => c.ElementaryCoverageID == oldCoverage.ElementaryCoverageID);
        //                    coverageInContext.ElementaryCoverageTypeID = newCoverage.ElementaryCoverageTypeID;
        //                    coverageInContext.CoverageAmount = newCoverage.CoverageAmount;
        //                    coverageInContext.Porcentage = newCoverage.Porcentage;
        //                    coverageInContext.premium = newCoverage.premium;
        //                    coverageInContext.Comments = newCoverage.Comments;
        //                    context.SaveChanges();
        //                    oldCoveragesToDelete.Remove(oldCoverage);
        //                    coveragesToAdd.Remove(newCoverage);
        //                    isBraked = true;
        //                    break;
        //                }
        //            }
        //            //למחוק
        //            if (!isBraked)
        //            {
        //                context.ElementaryCoverages.Remove(oldCoverage);
        //                context.SaveChanges();
        //            }
        //        }
        //        InsertCoveragesToElementaryPolicy(policyNumber, addition, coveragesToAdd);
        //    }
        //}

        //מחזיר רשימה של כל סוגי הכיסוים חו"ל לפי סוג ביטוח
        public List<TravelCoverageType> GetAllTravelCoverageTypes()
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.TravelCoverageTypes.OrderByDescending(c => c.Status).ThenBy(c => c.TravelCoverageTypeName).ToList();
            }
        }

        public List<ForeingWorkersCoverageType> GetAllForeingWorkersCoverageTypes()
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.ForeingWorkersCoverageTypes.OrderByDescending(c => c.Status).ThenBy(c => c.ForeingWorkersCoverageTypeName).ToList();
            }
        }

        //מחזיר רשימה של סוגי הכיסוים חו"ל האקטיבים לפי סוג ביטוח
        public List<TravelCoverageType> GetAllActiveTravelCoverageTypes()
        {
            List<TravelCoverageType> activeCoverages = GetAllTravelCoverageTypes().Where(c => c.Status == true).ToList();
            return activeCoverages;
        }

        public List<ForeingWorkersCoverageType> GetAllActiveForeingWorkersCoverageTypes()
        {
            List<ForeingWorkersCoverageType> activeCoverages = GetAllForeingWorkersCoverageTypes().Where(c => c.Status == true).ToList();
            return activeCoverages;
        }

        //מוסיף סוג כיסוי חו"ל
        public TravelCoverageType InsertTravelCoverageType(string coverageCode, string coverageName, bool status)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                TravelCoverageType coverage = new TravelCoverageType();                
                coverage.TravelCoverageCode = coverageCode;
                coverage.TravelCoverageTypeName = coverageName;
                coverage.Status = status;
                try
                {
                    context.TravelCoverageTypes.Add(coverage);
                    context.SaveChanges();
                    return coverage;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
        }

        //מעדכן סוג כיסוי חו"ל
        public TravelCoverageType UpdateTravelCoverageType(TravelCoverageType coverage, bool status)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var coverageInContext = context.TravelCoverageTypes.FirstOrDefault(c => c.TravelCoverageTypeID == coverage.TravelCoverageTypeID);
                coverageInContext.Status = status;
                if (coverageInContext != null)
                {
                    try
                    {
                        context.SaveChanges();
                        return coverageInContext;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }
                else
                {
                    throw new Exception("כיסוי לא קיים במערכת");
                }
            }
        }

        //מחזיר רשימה של כל הכיסוים לפי פוליסה
        public List<TravelCoverage> GetAllTravelCoveragesByPolicy(int policyId)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                //var policyInContext = context.TravelPolicies.FirstOrDefault(p => p.PolicyNumber == policyNumber);
                var policyInContext = context.TravelPolicies.FirstOrDefault(p => p.TravelPolicyID == policyId);

                if (policyInContext != null)
                {
                    return context.TravelCoverages.Include("TravelCoverageType").Where(c => c.TravelPolicyID == policyInContext.TravelPolicyID).OrderByDescending(c => c.Date).ToList();
                }
                else
                {
                    throw new Exception("פוליסה לא קיימת במערכת");
                }
            }
        }

        public void InsertCoveragesToTravelPolicy(int policyId, List<TravelCoverage> coveragesToAdd, bool isAddition)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var policyInContext = context.TravelPolicies.FirstOrDefault(c => c.TravelPolicyID == policyId);

                if (policyInContext != null)
                {
                    try
                    {
                        var policyCoverages = context.TravelCoverages.Where(c => c.TravelPolicyID == policyId).ToList();
                        foreach (TravelCoverage coverage in coveragesToAdd)
                        {
                            if (coverage.TravelCoverageID == 0)
                            {
                                coverage.TravelPolicyID = policyId;
                                coverage.Addition = policyInContext.Addition;
                                coverage.TravelCoverageType = null;
                                context.TravelCoverages.Add(coverage);
                            }
                            else if (isAddition)
                            {
                                TravelCoverage newCoverage = new TravelCoverage() { Comments = coverage.Comments, Addition = coverage.Addition, Date = coverage.Date, TravelPolicyID = policyInContext.TravelPolicyID, CoverageAmount = coverage.CoverageAmount, TravelCoverageTypeID = coverage.TravelCoverageTypeID, Porcentage = coverage.Porcentage, premium = coverage.premium };
                                context.TravelCoverages.Add(newCoverage);
                            }
                            else
                            {
                                var coverageInContext = context.TravelCoverages.FirstOrDefault(c => c.TravelCoverageID == coverage.TravelCoverageID);
                                if (coverageInContext != null)
                                {
                                    coverageInContext.Addition = policyInContext.Addition;
                                    coverageInContext.Comments = coverage.Comments;
                                    coverageInContext.CoverageAmount = coverage.CoverageAmount;
                                    coverageInContext.Date = coverage.Date;
                                    coverageInContext.Porcentage = coverage.Porcentage;
                                    coverageInContext.premium = coverage.premium;                                    
                                    coverageInContext.TravelCoverageType = null;
                                    coverageInContext.TravelCoverageTypeID = coverage.TravelCoverageTypeID;

                                    policyCoverages.Remove(coverageInContext);
                                }
                            }
                        }
                        if (policyCoverages.Count > 0)
                        {
                            foreach (var item in policyCoverages)
                            {
                                context.TravelCoverages.Remove(item);
                            }
                        }
                        context.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }
                else
                {
                    throw new Exception("פוליסה לא קיימת במערכת");
                }
            }
        }

        public ForeingWorkersCoverageType InsertForeingWorkersCoverageType(string coverageCode, string coverageName, bool status)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                ForeingWorkersCoverageType coverage = new ForeingWorkersCoverageType();
                coverage.ForeingWorkersCoverageCode = coverageCode;
                coverage.ForeingWorkersCoverageTypeName = coverageName;
                coverage.Status = status;
                try
                {
                    context.ForeingWorkersCoverageTypes.Add(coverage);
                    context.SaveChanges();
                    return coverage;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
        }

        //מעדכן סוג כיסוי חו"ל
        public ForeingWorkersCoverageType UpdateForeingWorkersCoverageType(ForeingWorkersCoverageType coverage, bool status)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var coverageInContext = context.ForeingWorkersCoverageTypes.FirstOrDefault(c => c.ForeingWorkersCoverageTypeID == coverage.ForeingWorkersCoverageTypeID);
                coverageInContext.Status = status;
                if (coverageInContext != null)
                {
                    try
                    {
                        context.SaveChanges();
                        return coverageInContext;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }
                else
                {
                    throw new Exception("כיסוי לא קיים במערכת");
                }
            }
        }

        //מחזיר רשימה של כל הכיסוים לפי פוליסה
        public List<ForeingWorkersCoverage> GetAllForeingWorkersCoveragesByPolicy(int policyId)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                //var policyInContext = context.ForeingWorkersPolicies.FirstOrDefault(p => p.PolicyNumber == policyNumber);
                var policyInContext = context.ForeingWorkersPolicies.FirstOrDefault(p => p.ForeingWorkersPolicyID == policyId);

                if (policyInContext != null)
                {
                    return context.ForeingWorkersCoverages.Include("ForeingWorkersCoverageType").Where(c => c.ForeingWorkersPolicyID == policyInContext.ForeingWorkersPolicyID).OrderByDescending(c => c.Date).ToList();
                }
                else
                {
                    throw new Exception("פוליסה לא קיימת במערכת");
                }
            }
        }

        public void InsertCoveragesToForeingWorkersPolicy(int policyId, List<ForeingWorkersCoverage> coveragesToAdd, bool isAddition)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var policyInContext = context.ForeingWorkersPolicies.FirstOrDefault(c => c.ForeingWorkersPolicyID == policyId);

                if (policyInContext != null)
                {
                    try
                    {
                        var policyCoverages = context.ForeingWorkersCoverages.Where(c => c.ForeingWorkersPolicyID == policyId).ToList();
                        foreach (ForeingWorkersCoverage coverage in coveragesToAdd)
                        {
                            if (coverage.ForeingWorkersCoverageID == 0)
                            {
                                coverage.ForeingWorkersPolicyID = policyId;
                                coverage.Addition = policyInContext.Addition;
                                coverage.ForeingWorkersCoverageType = null;
                                context.ForeingWorkersCoverages.Add(coverage);
                            }
                            else if (isAddition)
                            {
                                ForeingWorkersCoverage newCoverage = new ForeingWorkersCoverage() { Comments = coverage.Comments, Addition = coverage.Addition, Date = coverage.Date, ForeingWorkersPolicyID = policyInContext.ForeingWorkersPolicyID, CoverageAmount = coverage.CoverageAmount, ForeingWorkersCoverageTypeID = coverage.ForeingWorkersCoverageTypeID, Porcentage = coverage.Porcentage, premium = coverage.premium };
                                context.ForeingWorkersCoverages.Add(newCoverage);
                            }
                            else
                            {
                                var coverageInContext = context.ForeingWorkersCoverages.FirstOrDefault(c => c.ForeingWorkersCoverageID == coverage.ForeingWorkersCoverageID);
                                if (coverageInContext != null)
                                {
                                    coverageInContext.Addition = policyInContext.Addition;
                                    coverageInContext.Comments = coverage.Comments;
                                    coverageInContext.CoverageAmount = coverage.CoverageAmount;
                                    coverageInContext.Date = coverage.Date;
                                    coverageInContext.Porcentage = coverage.Porcentage;
                                    coverageInContext.premium = coverage.premium;
                                    coverageInContext.ForeingWorkersCoverageType = null;
                                    coverageInContext.ForeingWorkersCoverageTypeID = coverage.ForeingWorkersCoverageTypeID;

                                    policyCoverages.Remove(coverageInContext);
                                }
                            }
                        }
                        if (policyCoverages.Count > 0)
                        {
                            foreach (var item in policyCoverages)
                            {
                                context.ForeingWorkersCoverages.Remove(item);
                            }
                        }
                        context.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }
                else
                {
                    throw new Exception("פוליסה לא קיימת במערכת");
                }
            }
        }



    }
}
