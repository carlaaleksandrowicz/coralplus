﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoralBusinessLogics
{
    //מחלקה העוסקת בסוגי מבנה של פוליסת דירה/רכב
    public class StructureTypesLogic
    {
        //מחזיר רשימה של כל סוגי המבנה
        public List<StructureType> GetAllStructures()
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.StructureTypes.OrderByDescending(s=>s.Status).ThenBy(s => s.StructureTypeName).ToList();     
            }   
        }

        //מחזיר רשימה של כל סוגי המבנה הפעילים
        public List<StructureType> GetAllActiveStructures()
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.StructureTypes.Where(s=>s.Status==true).OrderBy(s => s.StructureTypeName).ToList();
            }
        }

        //מוסיף סוג מבנה למערכת
        public StructureType InsertStructureType(string structureName, bool status)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var structureInContext = context.StructureTypes.FirstOrDefault(s => s.StructureTypeName == structureName);
                if (structureInContext==null)
                {
                    try
                    {
                        StructureType newStructureType = new StructureType() { StructureTypeName = structureName, Status = status };
                        context.StructureTypes.Add(newStructureType);
                        context.SaveChanges();
                        return newStructureType;
                    }
                    catch (Exception ex)
                    {                        
                        throw new Exception(ex.Message);
                    }
                }
                else
                {
                    throw new Exception("סוג מבנה קיים במערכת");
                }
            }
        }

        //מעדכן סוג מבנה במערכת
        public StructureType UpdateStructureTypeStatus(string structureName, bool status)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var structureInContext = context.StructureTypes.FirstOrDefault(s => s.StructureTypeName == structureName);
                if (structureInContext != null)
                {
                    try
                    {
                        structureInContext.Status = status;
                        context.SaveChanges();
                        return structureInContext;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }
                else
                {
                    throw new Exception("סוג מבנה לא קיים במערכת");
                }
            }
        }

        public StructureType GetStructureTypeById(int structureId)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.StructureTypes.FirstOrDefault(s => s.StructureTypeID == structureId);
            }
        }

    }
}
