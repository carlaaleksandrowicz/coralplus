﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoralBusinessLogics
{
    public class TravelClaimsLogic
    {
        //מחזיר רשימה של כל התביעות חול
        public List<TravelClaim> GetAllTravelClaims()
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.TravelClaims.Include("TravelClaimCondition").Include("TravelClaimType").Include("TravelPolicy").Include("TravelPolicy.Company").Include("TravelPolicy.Client").OrderByDescending(c => c.OpenDate).ToList();
            }
        }

        //מחזיר רשימה של כל התביעות חול לפי לקוח
        public List<TravelClaim> GetAllTravelClaimsByClient(int clientId)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.TravelClaims.Include("TravelPolicy.Client").Include("TravelClaimCondition").Include("TravelClaimType").Include("TravelPolicy").Include("TravelPolicy.Company").Include("TravelPolicy.Insurance").Where(c => c.TravelPolicy.ClientID == clientId).OrderByDescending(c => c.OpenDate).ToList();
            }
        }

        //מחדיר רשימה של כל סוגי התביעות חול
        public List<TravelClaimType> GetAllTravelClaimTypes()
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.TravelClaimTypes.OrderBy(c => c.ClaimTypeName).ToList();
            }
        }
        

        //מחדיר רשימה של סוגי התביעות חול האקטיביות לפי סוג ביטוח
        public List<TravelClaimType> GetActiveTravelClaimTypes()
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.TravelClaimTypes.Where(t => t.Status == true).OrderBy(t => t.ClaimTypeName).ToList();
            }
        }      

     

        //מוסיף סוג תביעה חול לבסיס הנתונים
        public TravelClaimType InsertTravelClaimType(string claimTypeName, bool status)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var claimTypeInContext = context.TravelClaimTypes.FirstOrDefault(t => t.ClaimTypeName == claimTypeName);
                if (claimTypeInContext == null)
                {
                    try
                    {
                        TravelClaimType newType = new TravelClaimType() { ClaimTypeName = claimTypeName, Status = status };
                        context.TravelClaimTypes.Add(newType);
                        context.SaveChanges();
                        return newType;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }
                else
                {
                    throw new Exception("סוג תביעה קיימת במערכת");
                }
            }
        }

        //מעדכן את הסטטוס של תביעה חול
        public TravelClaimType UpdateTravelClaimTypeStatus(string typeName, bool status)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {

                var claimTypeInContext = context.TravelClaimTypes.Where(c => c.ClaimTypeName == typeName).FirstOrDefault();
                if (claimTypeInContext != null)
                {
                    try
                    {
                        claimTypeInContext.Status = status;
                        context.SaveChanges();
                        return claimTypeInContext;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }
                else
                {
                    throw new Exception("סוג תביעה לא קיימת במערכת");
                }
            }
        }

        //מחזיר רשימה של כל מצבי תביעה חול
        public List<TravelClaimCondition> GetAllTravelClaimConditions()
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.TravelClaimConditions.OrderBy(c => c.Description).ToList();
            }
        }
       

        //מחזיר רשימה של מצבי תביעה חול אקטיביים לפי ענף
        public List<TravelClaimCondition> GetActiveTravelClaimConditions()
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.TravelClaimConditions.Where(c => c.Status == true).OrderBy(c => c.Description).ToList();
            }
        }

        //מוסיף מצב תביעה לבסיס הנתונים
        public TravelClaimCondition InsertTravelClaimCondition(string claimConditionName, bool status)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var claimConditionInContext = context.TravelClaimConditions.FirstOrDefault(c => c.Description == claimConditionName);
                if (claimConditionInContext == null)
                {
                    try
                    {
                        TravelClaimCondition newCondition = new TravelClaimCondition() { Description = claimConditionName, Status = status };
                        context.TravelClaimConditions.Add(newCondition);
                        context.SaveChanges();
                        return newCondition;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }
                else
                {
                    throw new Exception("מצב תביעה קיימת במערכת");
                }
            }
        }

        //מעדכן את הסטטוס של מצב תביעה
        public TravelClaimCondition UpdateTravelClaimConditionStatus(string conditionName, bool status)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var claimConditionInContext = context.TravelClaimConditions.Where(c => c.Description == conditionName).FirstOrDefault();
                if (claimConditionInContext != null)
                {
                    try
                    {
                        claimConditionInContext.Status = status;
                        context.SaveChanges();
                        return claimConditionInContext;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }
                else
                {
                    throw new Exception("סוג תביעה לא קיים במערכת");
                }
            }
        }

        //מוסיף תביעה חול לבסיס הנתונים
        public int InsertTravelClaim(int policyId, int? claimTypeId, int? claimConditionId, bool claimStatus, string claimNumber, DateTime? openDate, string thirdPartyClaimNumber, DateTime? deliveredToCompanyDate, DateTime? moneyRecivedDate, decimal? claimAmount, decimal? amountRecived, DateTime? eventDateAndTime, string eventPlace, string eventDescription, string contactName, string contactAddress, string contactPhone, string contactCellPhone, string contactFax, string contactEmail, decimal? damageAmountClaimed, string damagedAddress,string damagedCellPhone,int? damagedCompanyId,string damagedEmail, string damageDescription, string damagedIdNumber,string damagedName, string damagedPhone,bool? isThirdPartyDamages,decimal? totalItemsAmount)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                try
                {
                    TravelClaim newClaim = new TravelClaim()
                    {
                        TravelPolicyID = policyId,
                        TravelClaimTypeID = claimTypeId,
                        TravelClaimConditionID = claimConditionId,
                        ClaimStatus = claimStatus,
                        ClaimNumber = claimNumber,
                        OpenDate = openDate,
                        ThirdPartyClaimNumber = thirdPartyClaimNumber,
                        DeliveredToCompanyDate = deliveredToCompanyDate,
                        MoneyReceivedDate = moneyRecivedDate,
                        ClaimAmount = claimAmount,
                        AmountReceived = amountRecived,
                        EventDateAndTime = eventDateAndTime,
                        EventPlace = eventPlace,
                        EventDescription = eventDescription,
                        ContactName = contactName,
                        ContactAddress = contactAddress,
                        ContactPhone = contactPhone,
                        ContactCellPhone = contactCellPhone,
                        ContactFax = contactFax,
                        ContactEmail = contactEmail,
                        DamageAmountClaimed=damageAmountClaimed,
                        DamagedAddress=damagedAddress,
                        DamagedCellPhone=damagedCellPhone,
                        DamagedCompanyID=damagedCompanyId,
                        DamagedEmail=damagedEmail,
                        DamageDescription=damageDescription,
                        DamagedIdNumber=damagedIdNumber,
                        DamagedName=damagedName,
                        DamagedPhone=damagedPhone,
                        IsThirdPartyDamages=isThirdPartyDamages,
                        TotalItmesAmount=totalItemsAmount                       
                    };
                    context.TravelClaims.Add(newClaim);
                    context.SaveChanges();
                    return newClaim.TravelClaimID;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
        }

        //מעדכן תביעה אלמנטרית
        public int UpdateTravelClaim(int claimID, int? claimTypeId, int? claimConditionId, bool claimStatus, string claimNumber, DateTime? openDate, string thirdPartyClaimNumber, DateTime? deliveredToCompanyDate, DateTime? moneyRecivedDate, decimal? claimAmount, decimal? amountRecived, DateTime? eventDateAndTime, string eventPlace, string eventDescription, string contactName, string contactAddress, string contactPhone, string contactCellPhone, string contactFax, string contactEmail, decimal? damageAmountClaimed, string damagedAddress, string damagedCellPhone, int? damagedCompanyId, string damagedEmail, string damageDescription, string damagedIdNumber, string damagedName, string damagedPhone, bool? isThirdPartyDamages, decimal? totalItemsAmount)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                try
                {
                    var claimInContext = context.TravelClaims.FirstOrDefault(c => c.TravelClaimID == claimID);
                    if (claimInContext != null)
                    {
                        claimInContext.TravelClaimTypeID = claimTypeId;
                        claimInContext.TravelClaimConditionID = claimConditionId;
                        claimInContext.ClaimStatus = claimStatus;
                        claimInContext.ClaimNumber = claimNumber;
                        claimInContext.OpenDate = openDate;
                        claimInContext.ThirdPartyClaimNumber = thirdPartyClaimNumber;
                        claimInContext.DeliveredToCompanyDate = deliveredToCompanyDate;
                        claimInContext.MoneyReceivedDate = moneyRecivedDate;
                        claimInContext.ClaimAmount = claimAmount;
                        claimInContext.AmountReceived = amountRecived;
                        claimInContext.EventDateAndTime = eventDateAndTime;
                        claimInContext.EventPlace = eventPlace;
                        claimInContext.EventDescription = eventDescription;
                        claimInContext.ContactName = contactName;
                        claimInContext.ContactAddress = contactAddress;
                        claimInContext.ContactPhone = contactPhone;
                        claimInContext.ContactCellPhone = contactCellPhone;
                        claimInContext.ContactFax = contactFax;
                        claimInContext.ContactEmail = contactEmail;
                        claimInContext.DamageAmountClaimed = damageAmountClaimed;
                        claimInContext.DamagedAddress = damagedAddress;
                        claimInContext.DamagedCellPhone = damagedCellPhone;
                        claimInContext.DamagedCompanyID = damagedCompanyId;
                        claimInContext.DamagedEmail = damagedEmail;
                        claimInContext.DamageDescription = damageDescription;
                        claimInContext.DamagedIdNumber = damagedIdNumber;
                        claimInContext.DamagedName = damagedName;
                        claimInContext.DamagedPhone = damagedPhone;
                        claimInContext.IsThirdPartyDamages = isThirdPartyDamages;
                        claimInContext.TotalItmesAmount = totalItemsAmount;

                        context.SaveChanges();
                        return claimInContext.TravelClaimID;
                    }
                    else
                    {
                        throw new Exception("תביעה לא קיימת במערכת");
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
        }

        //מחזיר רשימה של עדים לפי תביעה
        public List<TravelWitness> GetWitnessesByClaim(int claimId)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.TravelWitnesses.Where(w => w.TravelClaimID == claimId).ToList();
            }
        }

        //מוסיף עד לתביעה
        public void InsertWitnesses(List<TravelWitness> witnesses, int claimID)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var claimInContext = context.TravelClaims.FirstOrDefault(c => c.TravelClaimID == claimID);
                if (claimInContext != null)
                {
                    try
                    {
                        foreach (var item in witnesses)
                        {
                            var witnessInContext = context.TravelWitnesses.FirstOrDefault(w => w.TravelWitnessID == item.TravelWitnessID);
                            if (witnessInContext == null)
                            {
                                item.TravelClaimID = claimID;
                                item.TravelClaim = null;
                                context.TravelWitnesses.Add(item);
                            }
                            else
                            {

                                witnessInContext.WitnessCellPhone = item.WitnessCellPhone;
                                witnessInContext.WitnessName = item.WitnessName;
                                witnessInContext.WitnessPhone = item.WitnessPhone;
                                witnessInContext.WittnessAddress = item.WittnessAddress;
                            }
                        }
                        context.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }
                else
                {
                    throw new Exception("תביעה לא קיימת במערכת");
                }
            }
        }



        //מחזיר רשימה של נזקים לפי תביעה
        public List<TravelDamage> GetAllDamagesByClaim(int claimId)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var claimInContext = context.TravelClaims.FirstOrDefault(c => c.TravelClaimID == claimId);
                if (claimInContext != null)
                {
                    try
                    {
                        return context.TravelDamages.Where(d => d.TravelClaimID == claimId).ToList();
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }
                else
                {
                    throw new Exception("תביעה לא קיימת במערכת");
                }
            }
        }
        

        //מוסיף נזקים לתביעת 
        public void InsertDamages(int claimId, List<TravelDamage> damages)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var claimInContext = context.TravelClaims.FirstOrDefault(c => c.TravelClaimID == claimId);
                if (claimInContext != null)
                {
                    try
                    {
                        foreach (var damage in damages)
                        {
                            if (damage.TravelDamageID == 0)
                            {
                                damage.TravelClaimID = claimId;
                                context.TravelDamages.Add(damage);
                            }
                        }
                        context.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }
                else
                {
                    throw new Exception("תביעה לא קיימת במערכת");
                }
            }
        }

        public void DeleteDamage(TravelDamage damage)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var damageInContext = context.TravelDamages.FirstOrDefault(t => t.TravelDamageID == damage.TravelDamageID);
                if (damageInContext != null)
                {
                    context.TravelDamages.Remove(damageInContext);
                    context.SaveChanges();
                }
            }

        }

        ////מחזיר רשימה של תביעות אלמנטריות לפי שדות חיפוש 
        //public List<TravelClaim> GetTravelClaimsByFilter(List<TravelClaim> claimsInContext, string status, TravelClaimCondition claimCondition, TravelClaimType claimType, InsuranceIndustry industry, Company company, string searchKey)
        //{
        //    try
        //    {
        //        //var claimsInContext = GetAllTravelClaims();
        //        if (searchKey != "")
        //        {
        //            claimsInContext = claimsInContext.Where(c => (c.ClaimNumber != null && c.ClaimNumber.StartsWith(searchKey)) || (c.ThirdPartyClaimNumber != null && c.ThirdPartyClaimNumber.StartsWith(searchKey)) || c.TravelPolicy.Client.FirstName.StartsWith(searchKey) || c.TravelPolicy.Client.LastName.StartsWith(searchKey) || c.TravelPolicy.Client.IdNumber.StartsWith(searchKey) || c.TravelPolicy.PolicyNumber.StartsWith(searchKey)).ToList();
        //        }
        //        if (status != null && status != "הצג הכל")
        //        {
        //            if (status == "פתוח")
        //            {
        //                claimsInContext = claimsInContext.Where(c => c.ClaimStatus == true).ToList();
        //            }
        //            else
        //            {
        //                claimsInContext = claimsInContext.Where(c => c.ClaimStatus == false).ToList();
        //            }
        //        }
        //        if (claimCondition != null)
        //        {
        //            claimsInContext = claimsInContext.Where(c => c.TravelClaimConditionID == claimCondition.TravelClaimConditionID).ToList();
        //        }
        //        if (claimType != null)
        //        {
        //            claimsInContext = claimsInContext.Where(c => c.TravelClaimTypeID == claimType.TravelClaimTypeID).ToList();
        //        }
        //        if (industry != null)
        //        {
        //            claimsInContext = claimsInContext.Where(c => c.TravelPolicy.InsuranceIndustryID == industry.InsuranceIndustryID).ToList();
        //        }
        //        if (company != null)
        //        {
        //            claimsInContext = claimsInContext.Where(c => c.TravelPolicy.CompanyID == company.CompanyID).ToList();
        //        }
        //        return claimsInContext;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception(ex.Message);
        //    }
        //}



    }
}
