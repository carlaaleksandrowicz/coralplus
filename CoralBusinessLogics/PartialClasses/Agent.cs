﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoralBusinessLogics
{
    partial class Agent
    {
        public string FullName { get { return String.Format("{0} {1}", LastName ,FirstName); } }
    }
}
