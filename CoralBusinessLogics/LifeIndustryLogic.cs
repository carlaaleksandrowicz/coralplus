﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoralBusinessLogics
{
    public class LifeIndustryLogic
    {
        //מחזיר רשימה של כל הענפים
        public List<LifeIndustry> GetAllLifeIndustries()
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.LifeIndustries.OrderBy(i => i.LifeIndustryName).ToList();
            }
        }

        //מוסיף ענף חדש
        public void InsertLifeIndustry(string industryName)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var industryInContext = context.LifeIndustries.FirstOrDefault(i => i.LifeIndustryName == industryName);
                if (industryInContext == null)
                {
                    try
                    {
                        LifeIndustry newIndustry = new LifeIndustry() { LifeIndustryName = industryName };
                        context.LifeIndustries.Add(newIndustry);
                        context.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }
                else
                {
                    throw new Exception("ענף קיים");
                }
            }
        }

        //מחזיר רשימה של כל סוגי ביטוחי חיים לפי ענף
        public List<LifeInsuranceType> GetLifeInsuranceTypesByIndustry(int industryID)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.LifeInsuranceTypes.Where(i=>i.LifeIndustryID==industryID).ToList();
            }
        }

        //מחזיר רשימה של סוגי ביטוחי חיים פעילים
        public List<LifeInsuranceType> GetActiveLifeInsuranceTypesByIndustry(int industryID)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.LifeInsuranceTypes.Where(i => i.Status == true && i.LifeIndustryID == industryID).ToList();
            }
        }

        //מוסיף סוג ביטוח חדש
        public void InsertLifeInsuranceType(string insuranceTypeName, int industryID, bool status)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var insuranceTypeInContext = context.LifeInsuranceTypes.FirstOrDefault(i => i.LifeInsuranceTypeName == insuranceTypeName);
                if (insuranceTypeInContext == null)
                {
                    try
                    {
                        LifeInsuranceType newType = new LifeInsuranceType() { LifeIndustryID = industryID, LifeInsuranceTypeName = insuranceTypeName, Status = status };
                        context.LifeInsuranceTypes.Add(newType);
                        context.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }
                else
                {
                    throw new Exception("סוג ביטוח קיים");
                }
            }
        }

        //מעדכן את הסטטוס של סוג ביטוח קיים
        public void UpdateLifeInsuranceTypeStatus(LifeInsuranceType insuranceType, bool status)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var insuranceTypeInContext = context.LifeInsuranceTypes.FirstOrDefault(i => i.LifeInsuranceTypeName == insuranceType.LifeInsuranceTypeName);
                if (insuranceTypeInContext != null)
                {
                    try
                    {
                        insuranceTypeInContext.Status = status;
                        context.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }
                else
                {
                    throw new Exception("סוג ביטוח לא קיים");
                }

            }
        }
    }
}
