﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoralBusinessLogics
{
    public class LifeClaimsLogic
    {
        //מחזיר רשימה של כל התביעות חיים לפי לקוח
        public List<LifeClaim> GetAllLifeClaimsByClient(int clientId)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.LifeClaims.Include("LifePolicy.Client").Include("LifeClaimCondition").Include("LifeClaimType").Include("LifePolicy").Include("LifePolicy.Company").Include("LifePolicy.LifeIndustry").Include("LifePolicy.FundType").Include("LifePolicy.Insurance").Where(c => c.LifePolicy.ClientID == clientId).OrderByDescending(c => c.OpenDate).ToList();
            }
        }
        public List<LifeClaimType> GetAllLifeClaimTypes()
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.LifeClaimTypes.OrderByDescending(t => t.Status).ThenBy(t => t.ClaimTypeName).ToList();
            }
        }
        public List<LifeClaimType> GetActiveLifeClaimTypes()
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.LifeClaimTypes.Where(t => t.Status == true).OrderBy(t=>t.ClaimTypeName).ToList();
            }
        }

        public LifeClaimType InsertLifeClaimType(string claimTypeName, bool status)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var claimTypeInContext = context.LifeClaimTypes.FirstOrDefault(t => t.ClaimTypeName == claimTypeName);
                if (claimTypeInContext == null)
                {
                    try
                    {
                        LifeClaimType newType = new LifeClaimType() { ClaimTypeName = claimTypeName, Status = status };
                        context.LifeClaimTypes.Add(newType);
                        context.SaveChanges();
                        return newType;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }
                else
                {
                    throw new Exception("סוג תביעה קיימת במערכת");
                }
            }
        }

        public LifeClaimType UpdateLifeClaimTypeStatus(string typeName, bool status)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {

                var claimTypeInContext = context.LifeClaimTypes.Where(c => c.ClaimTypeName == typeName).FirstOrDefault();
                if (claimTypeInContext != null)
                {
                    try
                    {
                        claimTypeInContext.Status = status;
                        context.SaveChanges();
                        return claimTypeInContext;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }
                else
                {
                    throw new Exception("סוג תביעה לא קיימת במערכת");
                }
            }
        }


        //מחזיר רשימה של כל מצבי תביעה חיים
        public List<LifeClaimCondition> GetAllLifeClaimConditions()
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.LifeClaimConditions.OrderByDescending(c=>c.Status).ThenBy(c=>c.Description).ToList();
            }
        }

        //מחזיר רשימה של מצבי תביעה חיים אקטיביים לפי ענף
        public List<LifeClaimCondition> GetActiveLifeClaimConditions()
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.LifeClaimConditions.Where(c => c.Status == true).OrderBy(c => c.Description).ToList();
            }
        }

        //מוסיף מצב תביעה לבסיס הנתונים
        public LifeClaimCondition InsertLifeClaimCondition(string claimConditionName, bool status)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var claimConditionInContext = context.LifeClaimConditions.FirstOrDefault(c => c.Description == claimConditionName);
                if (claimConditionInContext == null)
                {
                    try
                    {
                        LifeClaimCondition newCondition = new LifeClaimCondition() { Description = claimConditionName, Status = status };
                        context.LifeClaimConditions.Add(newCondition);
                        context.SaveChanges();
                        return newCondition;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }
                else
                {
                    throw new Exception("מצב תביעה קיימת במערכת");
                }
            }
        }

        //מעדכן את הסטטוס של מצב תביעה
        public LifeClaimCondition UpdateLifeClaimConditionStatus(string conditionName, bool status)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var claimConditionInContext = context.LifeClaimConditions.FirstOrDefault(c => c.Description == conditionName);
                if (claimConditionInContext != null)
                {
                    try
                    {
                        claimConditionInContext.Status = status;
                        context.SaveChanges();
                        return claimConditionInContext;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }
                else
                {
                    throw new Exception("סוג תביעה לא קיים במערכת");
                }
            }
        }

        //מוסיף תביעה חיים לבסיס הנתונים
        public int InsertLifeClaim(int policyId, int? claimTypeId, int? claimConditionId, bool claimStatus, string claimNumber, DateTime? openDate, DateTime? deliveredToCompanyDate, DateTime? moneyRecivedDate, decimal? claimAmount, decimal? amountRecived, DateTime? eventDate, string eventPlace, string eventDescription)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                try
                {
                    LifeClaim newClaim = new LifeClaim()
                    {
                        LifePolicyID = policyId,
                        LifeClaimTypeID = claimTypeId,
                        LifeClaimConditionID = claimConditionId,
                        ClaimStatus = claimStatus,
                        ClaimNumber = claimNumber,
                        OpenDate = openDate,
                        DeliveredToCompanyDate = deliveredToCompanyDate,
                        MoneyReceivedDate = moneyRecivedDate,
                        ClaimAmount = claimAmount,
                        AmountReceived = amountRecived,
                        EventDateAndTime = eventDate,
                        EventPlace = eventPlace,
                        EventDescription = eventDescription
                    };
                    context.LifeClaims.Add(newClaim);
                    context.SaveChanges();
                    return newClaim.LifeClaimID;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
        }


        //מוסיף עד לתביעה
        public void InsertWitnesses(List<LifeWitness> witnesses, int claimID)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var claimInContext = context.LifeClaims.FirstOrDefault(c => c.LifeClaimID == claimID);
                if (claimInContext != null)
                {
                    try
                    {
                        foreach (var item in witnesses)
                        {
                            if (item.LifeWitnessID == 0)
                            {
                                item.LifeClaimID = claimID;
                                context.LifeWitnesses.Add(item);
                            }
                            else
                            {
                                var witnessInContext = context.LifeWitnesses.FirstOrDefault(w => w.LifeWitnessID == item.LifeWitnessID);
                                if (witnessInContext != null)
                                {
                                    witnessInContext.WitnessCellPhone = item.WitnessCellPhone;
                                    witnessInContext.WitnessName = item.WitnessName;
                                    witnessInContext.WitnessPhone = item.WitnessPhone;
                                    witnessInContext.WittnessAddress = item.WittnessAddress;
                                }
                            }
                        }
                        context.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }
                else
                {
                    throw new Exception("תביעה לא קיימת במערכת");
                }
            }
        }

        //מחזיר רשימה של עדים לפי תביעה
        public List<LifeWitness> GetWitnessesByClaim(int claimId)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.LifeWitnesses.Where(w => w.LifeClaimID == claimId).ToList();
            }
        }


        //מעדכן תביעה חיים
        public void UpdateLifeClaim(int claimID, int? claimTypeId, int? claimConditionId, bool claimStatus, string claimNumber, DateTime? deliveredToCompanyDate, DateTime? moneyRecivedDate, decimal? claimAmount, decimal? amountRecived, DateTime? eventDateTime, string eventPlace, string eventDescription)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                try
                {
                    var claimInContext = context.LifeClaims.FirstOrDefault(c => c.LifeClaimID == claimID);
                    if (claimInContext != null)
                    {
                        claimInContext.LifeClaimTypeID = claimTypeId;
                        claimInContext.LifeClaimConditionID = claimConditionId;
                        claimInContext.ClaimStatus = claimStatus;
                        claimInContext.ClaimNumber = claimNumber;
                        claimInContext.DeliveredToCompanyDate = deliveredToCompanyDate;
                        claimInContext.MoneyReceivedDate = moneyRecivedDate;
                        claimInContext.ClaimAmount = claimAmount;
                        claimInContext.AmountReceived = amountRecived;
                        claimInContext.EventDateAndTime = eventDateTime;
                        claimInContext.EventPlace = eventPlace;
                        claimInContext.EventDescription = eventDescription;
                        
                        context.SaveChanges();                        
                    }
                    else
                    {
                        throw new Exception("תביעה לא קיימת במערכת");
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
        }
    }
}
