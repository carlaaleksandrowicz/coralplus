﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoralBusinessLogics
{
    public class RenewalsLogic
    {
        //public Renewal InsertRenewal(int elementaryPolicyId, int renewalStatusId)
        //{
        //    using (Coral_DB_Entities context = new Coral_DB_Entities())
        //    {
        //        try
        //        {
        //            Renewal newRenewal = new Renewal() { ElementaryPolicyID = elementaryPolicyId, RenewalStatusID = renewalStatusId };
        //            context.Renewals.Add(newRenewal);
        //            context.SaveChanges();
        //            return context.Renewals.FirstOrDefault(r => r.ElementaryPolicyID == elementaryPolicyId);
        //        }
        //        catch (Exception ex)
        //        {
        //            throw new Exception(ex.Message);
        //        }
        //    }
        //}

        //public void InsertRenewals(List<int> elementaryPolicyId, int renewalStatusId)
        //{
        //    using (Coral_DB_Entities context = new Coral_DB_Entities())
        //    {
        //        try
        //        {
        //            foreach (var item in elementaryPolicyId)
        //            {
        //                Renewal newRenewal = new Renewal() { ElementaryPolicyID = item, RenewalStatusID = renewalStatusId };
        //                context.Renewals.Add(newRenewal);
        //                context.SaveChanges(); 
        //            }
        //            //return context.Renewals.FirstOrDefault(r => r.ElementaryPolicyID == elementaryPolicyId);
        //        }
        //        catch (Exception ex)
        //        {
        //            throw new Exception(ex.Message);
        //        }
        //    }
        //}

        //public Renewal GetRenewalByPolicy(ElementaryPolicy policy)
        //{
        //    using (Coral_DB_Entities context = new Coral_DB_Entities())
        //    {
        //        var renewalInContext = context.Renewals.FirstOrDefault(r => r.ElementaryPolicy.PolicyNumber == policy.PolicyNumber);
        //        if (renewalInContext != null)
        //        {
        //            return renewalInContext;
        //        }
        //        else
        //        {
        //            return null;
        //        }
        //    }
        //}

        //public List<RenewalStatus> GetRenewalStatusByPolicy(List<ElementaryPolicy> policies)
        //{
        //    using (Coral_DB_Entities context = new Coral_DB_Entities())
        //    {
        //        List<RenewalStatus> statuses = new List<RenewalStatus>();
        //        foreach (var policy in policies)
        //        {
        //            var renewalInContext = context.Renewals.FirstOrDefault(r => r.ElementaryPolicy.PolicyNumber == policy.PolicyNumber);
        //            if (renewalInContext != null)
        //            {
        //                statuses.Add(renewalInContext.RenewalStatus);
        //            }
        //        }
        //        return statuses;
        //    }
        //}



        //public void UpdateRenewal(Renewal renewalToUpdate, int renewalStatusId)
        //{
        //    using (Coral_DB_Entities context = new Coral_DB_Entities())
        //    {
        //        try
        //        {
        //            var renewalInContext = context.Renewals.FirstOrDefault(r => r.RenewalID == renewalToUpdate.RenewalID);
        //            renewalInContext.RenewalStatusID = renewalStatusId;
        //            context.SaveChanges();
        //        }
        //        catch (Exception ex)
        //        {
        //            throw new Exception(ex.Message);
        //        }
        //    }
        //}

        public List<RenewalStatus> GetAllRenewalStatuses()
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.RenewalStatuses.OrderByDescending(c => c.Status).ThenBy(s => s.RenewalStatusName).ToList();
            }
        }

        public List<RenewalStatus> GetAllActiveRenewalStatuses()
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.RenewalStatuses.Where(s => s.Status == true).OrderBy(s => s.RenewalStatusName).ToList();
            }
        }

        public void InsertForeingWorkersRenewalQuote(ForeingWorkersPolicy policy, string newPolicyNumber, int newPremium, InsuranceCompany newCompany)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                int? companyID = null;
                if (newCompany != null)
                {
                    companyID = newCompany.CompanyID;
                }
                ForeingWorkersRenewalQuote newQuote = new ForeingWorkersRenewalQuote() { ForeingWorkersPolicyID = policy.ForeingWorkersPolicyID, PolicyNumber = newPolicyNumber, Premium = newPremium, CompanyID = companyID };
                try
                {
                    context.ForeingWorkersRenewalQuotes.Add(newQuote);
                    context.SaveChanges();
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
        }

        public IEnumerable GetForeingWorkersRenewalQuotesByRenewalId(int policyId)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.ForeingWorkersRenewalQuotes.Include("Company").Where(q => q.ForeingWorkersPolicyID == policyId).OrderBy(q => q.Premium).ToList();
            }
        }

        public void InsertRenewalStatus(string renewalStatusName, bool status)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var renewalStatusInContext = context.RenewalStatuses.FirstOrDefault(s => s.RenewalStatusName == renewalStatusName);
                if (renewalStatusInContext == null)
                {
                    try
                    {
                        RenewalStatus newStatus = new RenewalStatus() { RenewalStatusName = renewalStatusName, Status = status };
                        context.RenewalStatuses.Add(newStatus);
                        context.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }
                else if (renewalStatusName == "בהפקה")
                {
                    return;
                }
                else
                {
                    throw new Exception("סטטוס חידוש קיים");
                }
            }
        }

        public void DeleteForeingWorkersQuote(ForeingWorkersRenewalQuote quote)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                try
                {
                    var quoteInContext = context.ForeingWorkersRenewalQuotes.FirstOrDefault(q => q.ForeingWorkersRenewalQuoteID == quote.ForeingWorkersRenewalQuoteID);
                    if (quoteInContext != null)
                    {
                        context.ForeingWorkersRenewalQuotes.Remove(quoteInContext);
                        context.SaveChanges();
                    }
                    else
                    {
                        throw new Exception("ההצעה לא קיימת במערכת");
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
        }

        public void UpdateRenewalStatusStatus(RenewalStatus renewalStatus, bool status)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var renewalStatusInContext = context.RenewalStatuses.FirstOrDefault(s => s.RenewalStatusID == renewalStatus.RenewalStatusID);
                if (renewalStatusInContext != null)
                {
                    try
                    {
                        renewalStatusInContext.Status = status;
                        context.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }
                else
                {
                    throw new Exception("סטטוס חידוש לא קיים");
                }

            }
        }

        public RenewalStatus GetRenewalStatusByRenewalStatusId(int renewalStatusId)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.RenewalStatuses.FirstOrDefault(r => r.RenewalStatusID == renewalStatusId);
            }
        }

        public void UpdateForeingWorkersRenewalTracking(ForeingWorkersRenewalTracking tracking, string content)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                try
                {
                    List<ForeingWorkersRenewalTracking> trackingsInContext = context.ForeingWorkersRenewalTrackings.Where(t => t.TrackingContent == tracking.TrackingContent && ((DateTime)t.Date) == (DateTime)tracking.Date && t.UserID == tracking.UserID).ToList();
                    if (trackingsInContext != null)
                    {
                        foreach (var item in trackingsInContext)
                        {
                            item.TrackingContent = content;
                        }
                        context.SaveChanges();
                    }
                    else
                    {
                        throw new Exception("המעקב לא קיים במערכת");
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
        }

        public void InsertAptOrBusinessRenewalQuote(ElementaryPolicy policy, string newPolicyNumber, int newPremium, InsuranceCompany newCompany)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                int? companyID = null;
                if (newCompany != null)
                {
                    companyID = newCompany.CompanyID;
                }
                ApartmentRenewalQuote newQuote = new ApartmentRenewalQuote() { ElementaryPolicyID = policy.ElementaryPolicyID, PolicyNumber = newPolicyNumber, Premium = newPremium, CompanyID = companyID };
                try
                {
                    context.ApartmentRenewalQuotes.Add(newQuote);
                    context.SaveChanges();
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
        }

        public void InsertCarRenewalQuote(ElementaryPolicy policy, string newPolicyNumber, int newPremium, InsuranceCompany newCompany, InsuranceIndustry industry, AllowedToDrive allowedToDrive, bool? isPackageIncluded)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                int? companyID = null;
                if (newCompany != null)
                {
                    companyID = newCompany.CompanyID;
                }
                int? allowedToDriveID = null;
                if (allowedToDrive != null)
                {
                    allowedToDriveID = allowedToDrive.AllowedToDriveID;
                }
                CarRenewalQuote newQuote = new CarRenewalQuote() { ElementaryPolicyID = policy.ElementaryPolicyID, PolicyNumber = newPolicyNumber, Premium = newPremium, CompanyID = companyID, InsuranceIndustryID = industry.InsuranceIndustryID, AllowedToDriveID = allowedToDriveID, IsPackageIncluded = isPackageIncluded };
                try
                {
                    context.CarRenewalQuotes.Add(newQuote);
                    context.SaveChanges();
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
        }

        public int GetTrackingsCountByPolicyId(int policyID)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.RenewalTrackings.Where(t => t.ElementaryPolicyID == policyID).OrderByDescending(t => t.RenewalTrackingID).Count();
            }
        }

        public int GetForeingWorkersTrackingsCountByPolicyId(int policyID)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.ForeingWorkersRenewalTrackings.Where(t => t.ForeingWorkersPolicyID == policyID).OrderByDescending(t => t.ForeingWorkersRenewalTrackingID).Count();
            }
        }



        public void DeleteAptOrBusinessQuote(ApartmentRenewalQuote quote)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                try
                {
                    var quoteInContext = context.ApartmentRenewalQuotes.FirstOrDefault(q => q.ApartmentRenewalQuoteID == quote.ApartmentRenewalQuoteID);
                    if (quoteInContext != null)
                    {
                        context.ApartmentRenewalQuotes.Remove(quoteInContext);
                        context.SaveChanges();
                    }
                    else
                    {
                        throw new Exception("ההצעה לא קיימת במערכת");
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
        }

        public void DeleteCarQuote(CarRenewalQuote quote)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                try
                {
                    var quoteInContext = context.CarRenewalQuotes.FirstOrDefault(q => q.CarRenewalQuoteID == quote.CarRenewalQuoteID);
                    if (quoteInContext != null)
                    {
                        context.CarRenewalQuotes.Remove(quoteInContext);
                        context.SaveChanges();
                    }
                    else
                    {
                        throw new Exception("ההצעה לא קיימת במערכת");
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
        }

        public List<ApartmentRenewalQuote> GetAptOrBusinessRenewalQuotesByRenewalId(int policyId)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.ApartmentRenewalQuotes.Include("Company").Where(q => q.ElementaryPolicyID == policyId).OrderBy(q => q.Premium).ToList();
            }
        }

        public List<CarRenewalQuote> GetCarRenewalQuotesByRenewalId(int policyId)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.CarRenewalQuotes.Include("Company").Include("InsuranceIndustry").Include("AllowedToDrive").Where(q => q.ElementaryPolicyID == policyId).OrderBy(q => q.Premium).ToList();
            }
        }

        public void InsertRenewalTracking(int policyID, int userId, DateTime date, string content)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                RenewalTracking newTracking = new RenewalTracking() { ElementaryPolicyID = policyID, UserID = userId, Date = date, TrackingContent = content };
                try
                {
                    context.RenewalTrackings.Add(newTracking);
                    context.SaveChanges();
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
        }

        public void InsertForeingWorkersRenewalTracking(int policyID, int userId, DateTime date, string content)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                ForeingWorkersRenewalTracking newTracking = new ForeingWorkersRenewalTracking() { ForeingWorkersPolicyID = policyID, UserID = userId, Date = date, TrackingContent = content };
                try
                {
                    context.ForeingWorkersRenewalTrackings.Add(newTracking);
                    context.SaveChanges();
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
        }

        public List<RenewalTracking> GetTrackingsByRenewalId(int policyID)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.RenewalTrackings.Include("User").Where(t => t.ElementaryPolicyID == policyID).OrderByDescending(t => t.RenewalTrackingID).ToList();
            }
        }

        public List<ForeingWorkersRenewalTracking> GetForeingWorkersRenewalTrackingsByPolicyId(int policyID)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.ForeingWorkersRenewalTrackings.Include("User").Where(t => t.ForeingWorkersPolicyID == policyID).OrderByDescending(t => t.ForeingWorkersRenewalTrackingID).ToList();
            }
        }

        public void DeleteTracking(RenewalTracking tracking)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                try
                {
                    List<RenewalTracking> trackingsInContext = context.RenewalTrackings.Where(t => t.TrackingContent == tracking.TrackingContent && ((DateTime)t.Date) == (DateTime)tracking.Date && t.UserID == tracking.UserID).ToList();
                    if (trackingsInContext != null)
                    {
                        foreach (var item in trackingsInContext)
                        {
                            context.RenewalTrackings.Remove(item);
                        }
                        context.SaveChanges();
                    }
                    else
                    {
                        throw new Exception("המעקב לא קיים במערכת");
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
        }

        public void DeleteForeingWorkersRenewalTracking(ForeingWorkersRenewalTracking tracking)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                try
                {
                    List<ForeingWorkersRenewalTracking> trackingsInContext = context.ForeingWorkersRenewalTrackings.Where(t => t.TrackingContent == tracking.TrackingContent && ((DateTime)t.Date) == (DateTime)tracking.Date && t.UserID == tracking.UserID).ToList();
                    if (trackingsInContext != null)
                    {
                        foreach (var item in trackingsInContext)
                        {
                            context.ForeingWorkersRenewalTrackings.Remove(item);
                        }
                        context.SaveChanges();
                    }
                    else
                    {
                        throw new Exception("המעקב לא קיים במערכת");
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
        }

        public void UpdateRenewalTracking(RenewalTracking tracking, string content)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                try
                {
                    List<RenewalTracking> trackingsInContext = context.RenewalTrackings.Where(t => t.TrackingContent == tracking.TrackingContent && ((DateTime)t.Date) == (DateTime)tracking.Date && t.UserID == tracking.UserID).ToList();
                    if (trackingsInContext != null)
                    {
                        foreach (var item in trackingsInContext)
                        {
                            item.TrackingContent = content;
                        }
                        context.SaveChanges();
                    }
                    else
                    {
                        throw new Exception("המעקב לא קיים במערכת");
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
        }


        public void AddRenewalStatusToPolicies()
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                try
                {
                    int statusId = context.RenewalStatuses.FirstOrDefault(r => r.RenewalStatusName == "לא טופל").RenewalStatusID;
                    var policies = context.ElementaryPolicies.ToList();
                    foreach (var item in policies)
                    {
                        item.RenewalStatusID = statusId;
                    }
                    context.SaveChanges();
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
        }


        public decimal GetTotalPremium(DateTime? date, List<ElementaryPolicy> policiesToRenew, Client client)
        {
            PoliciesLogic policiesLogic = new PoliciesLogic();
            decimal totalPremium = 0;
            if (date != null)
            {
                DateTime endDate = (DateTime)date;
                List<ElementaryPolicy> allPolicies = policiesLogic.GetPoliciesByEndDate(endDate).Where(p => p.TotalPremium != null).ToList();
                List<ElementaryPolicy> filteredPolicies = new List<ElementaryPolicy>();
                foreach (var item in allPolicies)
                {
                    if (policiesToRenew.Any(p => p.PolicyNumber == item.PolicyNumber && p.InsuranceIndustryID == item.InsuranceIndustryID))
                    {
                        filteredPolicies.Add(item);
                    }
                }
                totalPremium = filteredPolicies.Sum(p => (decimal)p.TotalPremium);
            }
            else if (client != null)
            {
                List<ElementaryPolicy> allPolicies = policiesLogic.GetAllElementaryPoliciesByClient(client).Where(p => p.TotalPremium != null).ToList();
                List<ElementaryPolicy> filteredPolicies = new List<ElementaryPolicy>();
                foreach (var item in allPolicies)
                {
                    if (policiesToRenew.Any(p => p.PolicyNumber == item.PolicyNumber && p.InsuranceIndustryID == item.InsuranceIndustryID))
                    {
                        filteredPolicies.Add(item);
                    }
                }
                totalPremium = filteredPolicies.Sum(p => (decimal)p.TotalPremium);
            }
            return totalPremium;
        }

        public decimal GetForeingWorkersTotalPremium(DateTime? date, List<ForeingWorkersPolicy> policiesToRenew, Client client)
        {
            decimal totalPremium = 0;
            if (date != null)
            {
                DateTime endDate = (DateTime)date;
                List<ForeingWorkersPolicy> allPolicies = new ForeingWorkersLogic().GetPoliciesByEndDate(endDate).Where(p => p.TotalPremium != null).ToList();
                List<ForeingWorkersPolicy> filteredPolicies = new List<ForeingWorkersPolicy>();
                foreach (var item in allPolicies)
                {
                    if (policiesToRenew.Any(p => p.PolicyNumber == item.PolicyNumber && p.ForeingWorkersIndustryID == item.ForeingWorkersIndustryID))
                    {
                        filteredPolicies.Add(item);
                    }
                }
                totalPremium = filteredPolicies.Sum(p => (decimal)p.TotalPremium);
                //totalPremium = new ForeingWorkersLogic().GetPoliciesByEndDate(endDate).Where(p => p.TotalPremium != null).Sum(p => (decimal)p.TotalPremium);
            }
            else if (client != null)
            {
                List<ForeingWorkersPolicy> allPolicies = new ForeingWorkersLogic().GetAllForeingWorkersPoliciesByClient(client).Where(p => p.TotalPremium != null).ToList();
                List<ForeingWorkersPolicy> filteredPolicies = new List<ForeingWorkersPolicy>();
                foreach (var item in allPolicies)
                {
                    if (policiesToRenew.Any(p => p.PolicyNumber == item.PolicyNumber && p.ForeingWorkersIndustryID == item.ForeingWorkersIndustryID))
                    {
                        filteredPolicies.Add(item);
                    }
                }
                totalPremium = filteredPolicies.Sum(p => (decimal)p.TotalPremium);
                //totalPremium = new ForeingWorkersLogic().GetAllForeingWorkersPoliciesByClient(client).Where(p => p.TotalPremium != null).Sum(p => (decimal)p.TotalPremium);
            }
            return totalPremium;
        }

        public int? GetRenewalRenewalStatusId(string statusName)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.RenewalStatuses.FirstOrDefault(r => r.RenewalStatusName == statusName).RenewalStatusID;
            }
        }
    }
}
