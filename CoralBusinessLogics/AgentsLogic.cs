﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoralBusinessLogics
{
    //ניהול סוכנים
    public class AgentsLogic //: BaseLogics
    {
        //מחזיר רשימה של כל הסוכנים הרשומים
        //agent1=subordinateTo
        public List<Agent> GetAllAgents()
        {
            using (Coral_DB_Entities context=new Coral_DB_Entities())
            {
                return context.Agents.Include("Agent1"). Include("AgentNumbers").OrderByDescending(a => a.Status).ThenBy(a => a.LastName).ToList();                
            }
        }

        //מחזיר רשימה של כל הסוכנים הראשיים הפעילים
        public List<Agent> GetActivePrincipalAgents()
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return (from a in context.Agents
                        where a.AgentLevel == true
                        where a.Status == true
                        select a).OrderByDescending(a => a.Status).ThenBy(a => a.LastName).ToList();
            }
        }

        //מחזיר רשימה של כל הסוכנים המישניים הפעילים
        public List<Agent> GetActiveSubAgents()
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {

                return (from a in context.Agents
                        where a.AgentLevel == false
                        where a.Status == true
                        select a).OrderByDescending(a => a.Status).ThenBy(a => a.LastName).ToList();
            }
        }

        //מחזיר רשימה של כל הסוכנים המישניים הפעילים לפי סוכן ראשי
        public List<Agent> GetActiveSubAgentsByPrincipalAgent(Agent principalAgent)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {

                return (from a in context.Agents
                        where a.AgentLevel == false
                        where a.Status == true
                        where a.SubordinateToAgentID==principalAgent.AgentID
                        select a).OrderByDescending(a => a.Status).ThenBy(a => a.LastName).ToList();
            }
        }

        //מוסיף סוכן חדש
        public void InsertAgent(bool level, Agent subordinateTo, string lastName, string firstName, string idNumber, DateTime birthDate, string city, string street, string homeNumber, string aptNumber, string zipCode, string homePhone, string cellphone, string workPhone, string fax, string email, string agencyName, bool status)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {

                var agentInContext = (from a in context.Agents
                                      where a.IdNumber == idNumber
                                      select a).FirstOrDefault();
                if (agentInContext == null)
                {
                    Agent agent = new Agent() { AgentLevel = level, LastName = lastName, FirstName = firstName, IdNumber = idNumber, BirthDate = birthDate, City = city, Street = street, HomeNumber = homeNumber, ApartmentNumber = aptNumber, ZipCode = zipCode, HomePhone = homePhone, CellPhone = cellphone, WorkPhone = workPhone, Fax = fax, Email = email, AgencyName = agencyName, Status = status };
                    if (subordinateTo != null)
                    {
                        agent.SubordinateToAgentID = subordinateTo.AgentID;
                    }
                    try
                    {
                        context.Agents.Add(agent);
                        context.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }
                else
                {
                    throw new Exception("סוכן קיים");
                }
            }
        }

        //מעדכן סוכן קיים
        public void UpdateAgent(Agent agentToUpdate)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {

                var agentInContext = (from a in context.Agents
                                      where a.IdNumber == agentToUpdate.IdNumber
                                      select a).FirstOrDefault();
                if (agentInContext != null)
                {
                    agentInContext.AgencyName = agentToUpdate.AgencyName;
                    agentInContext.AgentLevel = agentToUpdate.AgentLevel;
                    agentInContext.ApartmentNumber = agentToUpdate.ApartmentNumber;
                    agentInContext.BirthDate = agentToUpdate.BirthDate;
                    agentInContext.CellPhone = agentToUpdate.CellPhone;
                    agentInContext.City = agentToUpdate.City;
                    agentInContext.Email = agentToUpdate.Email;
                    agentInContext.Fax = agentToUpdate.Fax;
                    agentInContext.FirstName = agentToUpdate.FirstName;
                    agentInContext.HomeNumber = agentToUpdate.HomeNumber;
                    agentInContext.HomePhone = agentToUpdate.HomePhone;
                    agentInContext.LastName = agentToUpdate.LastName;
                    agentInContext.Status = agentToUpdate.Status;
                    agentInContext.Street = agentToUpdate.Street;
                    agentInContext.SubordinateToAgentID = agentToUpdate.SubordinateToAgentID;
                    agentInContext.WorkPhone = agentToUpdate.WorkPhone;
                    agentInContext.ZipCode = agentToUpdate.ZipCode;
                    //agentInContext.Agent1 = agentToUpdate.Agent1;
                    try
                    {
                        context.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }
                else
                {
                    throw new Exception("סוכן לא קיים במערכת");
                }
            }
        }

        //מוסיף רשימה של מספרים לסוכן מסוים
        public void InsertNumbersToAgent(string agentIdNumber, ObservableCollection<AgentNumber> agentsNumbers)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {

                var agentInContext = (from a in context.Agents
                                      where a.IdNumber == agentIdNumber
                                      select a).FirstOrDefault();
                if (agentInContext != null)
                {
                    try
                    {
                        foreach (AgentNumber agentNumber in agentsNumbers)
                        {
                            agentNumber.AgentID = agentInContext.AgentID;
                            context.AgentNumbers.Add(agentNumber);
                        }
                        context.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }

                else
                {
                    throw new Exception("סוכן לא קיים");
                }
            }
        }

        //מחזיר רשימה של כל המספרים של סוכן מסוים
        public List<AgentNumber> GetNumbersByAgent(Agent agent)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {

                var agentInContext = context.Agents.FirstOrDefault(a => a.AgentID == agent.AgentID);
                if (agentInContext != null)
                {
                    return context.AgentNumbers.Include("Insurance").Include("Company").Where(a => a.AgentID == agentInContext.AgentID).ToList();
                }
                else
                {
                    throw new Exception("סוכן לא קיים במערכת");
                }
            }
        }

        //מעדכן מס' סוכן קיים במערכת
        public void UpdateNumberToAgent(Agent agent, string oldNumber,string newNumber, string companyName)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {

                var agentInContext = context.Agents.FirstOrDefault(a => a.AgentID == agent.AgentID);
                if (agentInContext != null)
                {
                    var numberInContext = context.AgentNumbers.FirstOrDefault(a => a.Number == oldNumber && a.Company.CompanyName == companyName);
                    if (numberInContext != null)
                    {
                        try
                        {
                            numberInContext.Number = newNumber;
                            context.SaveChanges();
                        }
                        catch (Exception ex)
                        {
                            throw new Exception(ex.Message);
                        }
                    }
                    else
                    {
                        throw new Exception("מספר לא קיים במערכת");
                    }
                }
                else
                {
                    throw new Exception("סוכן לא קיים במערכת");
                }
            }
        }

        //מחזיר רשימה של סוכנים התואמים עם מפתח חיפוש מסוים
        public List<Agent> FindAgentByString(string key)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.Agents.Where(a => a.LastName.StartsWith(key) || a.FirstName.StartsWith(key) || a.AgencyName.StartsWith(key) || a.City.StartsWith(key)).OrderByDescending(a => a.Status).ThenBy(a => a.LastName).ToList();
            }
        }

        //מחזיר את מספרי סוכן של חברה וביטוח מסוים
        public List<AgentNumber> GetAgentNumbersByCompanyAndInsurance(int? agentID, InsuranceCompany company, int? insuranceID)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.AgentNumbers.Where(n => n.AgentID == agentID && n.CompanyID == company.CompanyID && n.InsuranceID == insuranceID).ToList();
            }
        }
    }
}
