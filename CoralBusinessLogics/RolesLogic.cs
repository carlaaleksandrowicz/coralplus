﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoralBusinessLogics
{
    public class RolesLogic //: BaseLogics
    {
        //מוסיף משתמש לתפקיד
        public void AddUser2Role(string username, string roleName)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {

                Role role2Add = context.Roles.First(r => r.RoleName == roleName);
                User user2Add = context.Users.First(u => u.Usermame == username);
                user2Add.Roles.Add(role2Add);
                context.SaveChanges();
            }
        }
         //מוסיף משתמשים לתפקידים
        public void AddUsersToRoles(List<string> usernames, List<string> roleNames)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {

                foreach (string username in usernames)
                    foreach (string roleName in roleNames)
                        AddUser2Role(username, roleName);
            }
        }

        //מוסיף תפקיד חדש
        public void CreateRole(string roleName, bool status)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {

                try
                {
                    if (RoleExists(roleName))
                    {
                        new Exception("Role already exists");

                    }
                    Role role = new Role { RoleName = roleName, Status = status };
                    context.Roles.Add(role);
                    context.SaveChanges();
                }
                catch (Exception e)
                {
                    throw new Exception(e.Message);
                }
            }
        }

        //מעדכן תפקיד קיים
        public void UpdateRole(string roleName, string roleNewName)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {

                if (!RoleExists(roleName))
                {
                    throw new Exception("Role does not exists");
                }
                Role role = context.Roles.First(r => r.RoleName == roleName);
                role.RoleName = roleNewName;
                context.SaveChanges();
            }
        }

        //מוחק תפקיד
        public bool DeleteRole(string roleName, bool throwOnPopulatedRole)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {

                try
                {
                    if (!RoleExists(roleName))
                    {
                        throw new Exception("role does not exist");
                    }
                    if (throwOnPopulatedRole && GetUsersInRole(roleName).Length > 0)
                    {
                        throw new Exception("can not delete a populated role");
                    }
                    Role role = context.Roles.First(r => r.RoleName == roleName);
                    context.Roles.Remove(role);
                    context.SaveChanges();
                    return true;
                }
                catch
                {
                    return false;
                }
            }
        }

        //מחזיר רשימה של משתמשים מסוימים עם תפקיד מסוים
        public string[] FindUsersInRole(string roleName, string usernameToMatch)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.Users.Where(u => u.Usermame.Contains(usernameToMatch) && u.Roles.Any(r => r.RoleName == roleName)).Select(u => u.Usermame).ToArray();
            }
        }

        //מחזיר רשימה של כל התפקידים
        public List<Role> GetAllRoles()
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {

                return context.Roles.ToList();
            }
        }

        //מחזיר רשימה של התפקידים של משתמש מסוים
        public string[] GetRolesForUser(string username)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {

                return context.Users.First(u => u.Usermame == username).Roles.Select(r => r.RoleName).ToArray();
            }
        }

        //מחזיר רשימה של כל המשתמשים בתפקיד מסוים
        public User[] GetUsersInRole(string roleName)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.Roles.First(r => r.RoleName == roleName).Users.ToArray();
            }
        }

        //בודק האם משתמשמסוים שייך לתפקיד מסוים
        public bool IsUserInRole(string username, string roleName)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.Roles.First(r => r.RoleName == roleName).Users.Any(u => u.Usermame == username);
            }
        }

        //מסיר משתמשים מתפקיד מסוים 
        private void RemoveUsersFromRole(string[] usernames, string roleName)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {

                Role role = context.Roles.First(r => r.RoleName == roleName);
                foreach (string username in usernames)
                    context.Users.First(u => u.Usermame == username).Roles.Remove(role);
                context.SaveChanges();
            }
        }

        //מסיר משתמשים מתפקידים מסוימים 
        public void RemoveUsersFromRoles(string[] usernames, string[] roleNames)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {

                foreach (string roleName in roleNames)
                    RemoveUsersFromRole(usernames, roleName);
            }
        }

        //בודק האם תפקיד מסוים קיים
        public bool RoleExists(string roleName)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {

                return context.Roles.Any(r => r.RoleName == roleName);
            }
        }

        
    }
}
