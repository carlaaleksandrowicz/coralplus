﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoralBusinessLogics
{
    //מאפיינים של נתונים להצגה בטבלאות המעקב
    public class TrackingsViewModel
    {
        public string TrackingContent { get; set; }
        public DateTime? TrackingDate { get; set; }
        public User User { get; set; }
    }
}
