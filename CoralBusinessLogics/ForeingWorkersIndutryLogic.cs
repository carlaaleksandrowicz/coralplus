﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoralBusinessLogics
{
    public class ForeingWorkersIndutryLogic
    {
        //מחזיר רשימה של כל הענפים
        public List<ForeingWorkersIndustry> GetAllForeingWorkersIndustries()
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.ForeingWorkersIndustries.OrderBy(i => i.ForeingWorkersIndustryName).ToList();
            }
        }


        //מחזיר רשימה של כל התכניות לפי סוג תכנית
        public List<ForeingWorkersInsuranceType> GetForeingWorkersInsuranceTypesByIndustryAndCompany(int industryID, int companyId)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.ForeingWorkersInsuranceTypes.Where(i => i.ForeingWorkersIndustryID == industryID && i.CompanyID == companyId).ToList();
            }
        }

        //מחזיר רשימה של כל התכניות לפי סוג תכנית
        public List<ForeingWorkersInsuranceType> GetForeingWorkersInsuranceTypesByIndustry(int industryID)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.ForeingWorkersInsuranceTypes.Where(i => i.ForeingWorkersIndustryID == industryID).ToList();
            }
        }

        //מחזיר רשימה של כל התכניות לפי סוג תכנית
        public List<ForeingWorkersInsuranceType> GetActiveForeingWorkersInsuranceTypesByIndustryAndCompany(int industryID, int companyId)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.ForeingWorkersInsuranceTypes.Where(i => i.Status == true && i.ForeingWorkersIndustryID == industryID && i.CompanyID == companyId).ToList();
            }
        }

        //מוסיף תכנית חדש
        public ForeingWorkersInsuranceType InsertForeingWorkersInsuranceType(string insuranceTypeName, int industryID, int companyID, bool status)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var insuranceTypeInContext = context.ForeingWorkersInsuranceTypes.FirstOrDefault(i => i.ForeingWorkersInsuranceTypeName == insuranceTypeName&&i.ForeingWorkersIndustryID==industryID&&i.CompanyID==companyID);
                if (insuranceTypeInContext == null)
                {
                    try
                    {
                        ForeingWorkersInsuranceType newType = new ForeingWorkersInsuranceType() { ForeingWorkersIndustryID = industryID, ForeingWorkersInsuranceTypeName = insuranceTypeName, CompanyID = companyID, Status = status };
                        context.ForeingWorkersInsuranceTypes.Add(newType);
                        context.SaveChanges();
                        return newType;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }
                else
                {
                    throw new Exception("סוג ביטוח קיים");
                }
            }
        }

        //מעדכן את הסטטוס של תכנית קיים
        public ForeingWorkersInsuranceType UpdateForeingWorkersInsuranceType(ForeingWorkersInsuranceType insuranceType, bool status)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var insuranceTypeInContext = context.ForeingWorkersInsuranceTypes.FirstOrDefault(i => i.ForeingWorkersInsuranceTypeID == insuranceType.ForeingWorkersInsuranceTypeID);
                if (insuranceTypeInContext != null)
                {
                    try
                    {
                        insuranceTypeInContext.Status = status;
                        context.SaveChanges();
                        return insuranceTypeInContext;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }
                else
                {
                    throw new Exception("תכנית לא קיים");
                }
            }
        }

    }
}
