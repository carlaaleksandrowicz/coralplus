﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoralBusinessLogics
{
    //class in charge of user logics 
    public class UserLogic //: BaseLogics
    {
        //get all the users 
        public IEnumerable GetAllUsers()
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {

                return context.Users.OrderByDescending(u => u.Status).ThenBy(u => u.Usermame).ToList();
            }
        }

        //get only active users
        public List<User> GetActiveUsers()
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {

                return (from u in context.Users
                        where u.Status == true
                        select u).ToList();
            }
        }

        //get only inactive users
        public List<User> GetInactiveUsers()
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {

                return (from u in context.Users
                        where u.Status == false
                        select u).ToList();
            }
        }

        //add a new user to DB. Must be a unique username
        public void InsertUser(string username, string password, bool status, bool? isCalendarPermission,bool? isClientProfilePermission,bool? isCreateClientPermission,bool? isElementaryPermission,bool? isFinancePermission,bool? isForeingWorkersPermission,bool? isHealthPermission, bool? isLifePermission,bool? isOffersPermission,bool? isPersonalAccidentsPermission, bool? isRenewalsPermission,bool? isReportsPermission,bool? isScanPermission, bool? isTasksPermission,bool? isTravelPermission)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {

                var userInContext = (from u in context.Users
                                     where u.Usermame == username
                                     select u).FirstOrDefault();
                if (userInContext == null)
                {
                    User user = new User();
                    user.Usermame = username;
                    user.Password = password;
                    user.Status = status;
                    user.IsCalendarPermission = isCalendarPermission;
                    user.IsClientProfilePermission = isClientProfilePermission;
                    user.IsCreateClientPermission = isCreateClientPermission;
                    user.IsElementaryPermission = isElementaryPermission;
                    user.IsFinancePermission = isFinancePermission;
                    user.IsForeingWorkersPermission = isForeingWorkersPermission;
                    user.IsHealthPermission = isHealthPermission;
                    user.IsLifePermission = isLifePermission;
                    user.IsOffersPermission = isOffersPermission;
                    user.IsPersonalAccidentsPermission = isPersonalAccidentsPermission;
                    user.IsRenewalsPermission = isRenewalsPermission;
                    user.IsReportsPermission = isReportsPermission;
                    user.IsScanPermission = isScanPermission;
                    user.IsTasksPermission = isTasksPermission;
                    user.IsTravelPermission = isTravelPermission;
                    
                    try
                    {
                        context.Users.Add(user);
                        context.SaveChanges();
                    }
                    catch (DbEntityValidationException ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }
                else
                {
                    throw new Exception("שם משתמש קיים. נא בחר אחר");
                }
            }
        }

        public void UpdateUser(string username, string password, bool status, bool? isCalendarPermission, bool? isClientProfilePermission, bool? isCreateClientPermission, bool? isElementaryPermission, bool? isFinancePermission, bool? isForeingWorkersPermission, bool? isHealthPermission, bool? isLifePermission, bool? isOffersPermission, bool? isPersonalAccidentsPermission, bool? isRenewalsPermission, bool? isReportsPermission, bool? isScanPermission, bool? isTasksPermission, bool? isTravelPermission)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {

                var userInContext = (from u in context.Users
                                     where u.Usermame == username
                                     select u).FirstOrDefault();
                if (userInContext != null)
                {
                    userInContext.Password = password;
                    userInContext.Status = status;
                    userInContext.IsCalendarPermission = isCalendarPermission;
                    userInContext.IsClientProfilePermission = isClientProfilePermission;
                    userInContext.IsCreateClientPermission = isCreateClientPermission;
                    userInContext.IsElementaryPermission = isElementaryPermission;
                    userInContext.IsFinancePermission = isFinancePermission;
                    userInContext.IsForeingWorkersPermission = isForeingWorkersPermission;
                    userInContext.IsHealthPermission = isHealthPermission;
                    userInContext.IsLifePermission = isLifePermission;
                    userInContext.IsOffersPermission = isOffersPermission;
                    userInContext.IsPersonalAccidentsPermission = isPersonalAccidentsPermission;
                    userInContext.IsRenewalsPermission = isRenewalsPermission;
                    userInContext.IsReportsPermission = isReportsPermission;
                    userInContext.IsScanPermission = isScanPermission;
                    userInContext.IsTasksPermission = isTasksPermission;
                    userInContext.IsTravelPermission = isTravelPermission;
                    try
                    {
                        context.SaveChanges();
                    }
                    catch (Exception)
                    {
                        throw new Exception();
                    }
                }
                else
                {
                    throw new Exception("משתמש לא קיים");
                }
            }
        }

        //checks if a user exist by matching username and password
        public bool IsUserExist(string username, string password)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.Users.Any(u => u.Usermame == username && u.Password == password);
            }
        }

        //בודק האם המשתמש הוא פעיל
        public bool IsUserActive(string username)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.Users.Any(u => u.Usermame == username && u.Status == true);
            }
        }

        //updates the user status. (Status=true - active user. Status=false - inactive user)
        public bool UpdateUserStatus(string username, bool IsActive)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var userInContext = (from u in context.Users
                                     where u.Usermame == username
                                     select u).FirstOrDefault();

                if (userInContext != null)
                {
                    try
                    {
                        userInContext.Status = IsActive;
                        context.SaveChanges();
                        return true;
                    }

                    catch (Exception)
                    {
                        return false;
                    }
                }
                return false;
            }
        }       
        //updates the user password.
        public bool UpdatePassword(string username, string newPassword)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {

                var userInContext = (from u in context.Users
                                     where u.Usermame == username
                                     select u).FirstOrDefault();

                if (userInContext != null)
                {
                    try
                    {
                        userInContext.Password = newPassword;
                        context.SaveChanges();
                        return true;
                    }

                    catch (Exception)
                    {
                        return false;
                    }
                }
                return false;
            }
        }

        //find user by username
        public User FindUserByUsername(string username)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {

                var usersInContext = (from u in context.Users
                                      where u.Usermame == username
                                      select u).FirstOrDefault();

                return usersInContext;
            }
        }

        public List<User> FindUsersByString(string username)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {

                return (from u in context.Users
                        where u.Usermame.Contains(username)
                        select u).ToList();
            }
        }
    }
}
