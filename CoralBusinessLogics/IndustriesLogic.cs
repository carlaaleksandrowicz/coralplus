﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoralBusinessLogics
{
    //ניהול ענפי ביטוח אלמנטרי
    public class IndustriesLogic
    {
        //מחזיר רשימה של כל הענפים
        public List<InsuranceIndustry> GetAllIndustries()
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.InsuranceIndustries.OrderBy(i => i.InsuranceIndustryName).ToList();
            }
        }

        //מוסיף ענף חדש
        public void InsertIndustry(string industryName)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var industryInContext = context.InsuranceIndustries.FirstOrDefault(i => i.InsuranceIndustryName == industryName);
                if (industryInContext == null)
                {
                    try
                    {
                        InsuranceIndustry newIndustry = new InsuranceIndustry() { InsuranceIndustryName = industryName };
                        context.InsuranceIndustries.Add(newIndustry);
                        context.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }
                else
                {
                    throw new Exception("ענף קיים");
                }
            }
        }

        //מחזיר רשימה של סוגי ביטוח לפי ענף
        public List<ElementaryInsuranceType> GetAllInsuranceTypesByIndustry(int industryID)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.ElementaryInsuranceTypes.Where(i => i.InsuranceIndustryID == industryID).ToList();
            }
        }

        //מחזיר רשימה של סוגי ביטוח רכב
        public List<ElementaryInsuranceType> GetAllActiveCarInsuranceTypes()
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.ElementaryInsuranceTypes.Include("InsuranceIndustry").Where(i => i.InsuranceIndustry.InsuranceIndustryName.Contains("רכב")&&i.Status==true).OrderBy(i=>i.InsuranceIndustryID).ThenBy(i=>i.ElementaryInsuranceTypeName).ToList();
            }
        }

        public List<ElementaryInsuranceType> GetAllCarInsuranceTypes()
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.ElementaryInsuranceTypes.Where(i => i.InsuranceIndustry.InsuranceIndustryName.Contains("רכב")).OrderByDescending(i=>i.Status).ThenBy(i => i.ElementaryInsuranceTypeName).ToList();
            }
        }

        //מחזיר רשימה של סוגי ביטוח פעילים לפי ענף
        public List<ElementaryInsuranceType> GetActiveInsuranceTypesByIndustry(int industryID)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.ElementaryInsuranceTypes.Where(i => i.InsuranceIndustryID == industryID&&i.Status==true).ToList();
            }
        }        

        //מוסיף סוג ביטוח חדש
        public ElementaryInsuranceType InsertInsuranceType(string insuranceTypeName, int industryID, bool status)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var insuranceTypeInContext = context.ElementaryInsuranceTypes.FirstOrDefault(i => i.ElementaryInsuranceTypeName == insuranceTypeName&&i.InsuranceIndustryID==industryID);
                if (insuranceTypeInContext == null)
                {
                    try
                    {
                        ElementaryInsuranceType newType = new ElementaryInsuranceType() { InsuranceIndustryID = industryID, ElementaryInsuranceTypeName = insuranceTypeName, Status = status };
                        context.ElementaryInsuranceTypes.Add(newType);
                        context.SaveChanges();
                        return newType;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }
                else
                {
                    throw new Exception("סוג ביטוח קיים");
                }
            }
        }

        //מעדכן את הסטטוס של סוג ביטוח קיים
        public ElementaryInsuranceType UpdateInsuranceTypeStatus(ElementaryInsuranceType insuranceType, bool status)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var insuranceTypeInContext = context.ElementaryInsuranceTypes.FirstOrDefault(i => i.ElementaryInsuranceTypeID == insuranceType.ElementaryInsuranceTypeID);
                if (insuranceTypeInContext != null)
                {
                    try
                    {
                        insuranceTypeInContext.Status = status;
                        context.SaveChanges();
                        return insuranceTypeInContext;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }
                else
                {
                    throw new Exception("סוג ביטוח לא קיים");
                }

            }
        }

        public List<ThirdPartyInsuranceType> GetAllActiveThirdPartyInsuranceTypes()
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.ThirdPartyInsuranceTypes.Where(i => i.Status == true).OrderBy(i=>i.ThirdPartyInsuranceTypeName).ToList();
            }
        }

        public List<ThirdPartyInsuranceType> GetAllThirdPartyInsuranceTypes()
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.ThirdPartyInsuranceTypes.OrderByDescending(i=>i.Status).ThenBy(i => i.ThirdPartyInsuranceTypeName).ToList();
            }
        }

        public ThirdPartyInsuranceType InsertThirdPartyInsuranceType(string insuranceTypeName, bool status)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var insuranceTypeInContext = context.ThirdPartyInsuranceTypes.FirstOrDefault(i => i.ThirdPartyInsuranceTypeName == insuranceTypeName);
                if (insuranceTypeInContext == null)
                {
                    try
                    {
                        ThirdPartyInsuranceType newType = new ThirdPartyInsuranceType() { ThirdPartyInsuranceTypeName = insuranceTypeName, Status = status };
                        context.ThirdPartyInsuranceTypes.Add(newType);
                        context.SaveChanges();
                        return newType;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }
                else
                {
                    throw new Exception("סוג ביטוח קיים");
                }
            }
        }

        //מעדכן את הסטטוס של סוג ביטוח קיים
        public ThirdPartyInsuranceType UpdateThirdPartyInsuranceTypeStatus(string insuranceType, bool status)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var insuranceTypeInContext = context.ThirdPartyInsuranceTypes.FirstOrDefault(i => i.ThirdPartyInsuranceTypeName == insuranceType);
                if (insuranceTypeInContext != null)
                {
                    try
                    {
                        insuranceTypeInContext.Status = status;
                        context.SaveChanges();
                        return insuranceTypeInContext;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }
                else
                {
                    throw new Exception("סוג ביטוח לא קיים");
                }

            }
        }



    }
}
