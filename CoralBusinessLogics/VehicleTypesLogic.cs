﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoralBusinessLogics
{
    //מחלקה העוסקת בסוגי רכב בפוליסת רכב
    public class VehicleTypesLogic
    {
        //מחזיר רשימה של כל סוגי הרכב
        public List<VehicleType> GetAllVehicleTypes()
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.VehicleTypes.OrderByDescending(v => v.Status).ThenBy(v => v.Description).ToList();
            }
        }

        // מחזיר רשימה של כל סוגי הרכב הפעילים
        public List<VehicleType> GetAllActiveVehicleTypes()
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.VehicleTypes.Where(v => v.Status==true).OrderBy(v => v.Description).ToList();
            }
        }

        //מוסיף סוג רכב למערכת
        public VehicleType InsertVehicleType(string description, bool status)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var vehicleInContext = context.VehicleTypes.FirstOrDefault(v => v.Description == description);
                if (vehicleInContext == null)
                {
                    try
                    {
                        VehicleType newVehicleType = new VehicleType() { Description = description, Status = status };
                        context.VehicleTypes.Add(newVehicleType);
                        context.SaveChanges();
                        return newVehicleType;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }
                else
                {
                    throw new Exception("סוג רכב קיים במערכת");
                }
            }
        }

        //מעדכן את הסטטוס  של סוג רכב במערכת
        public VehicleType UpdateVehicleTypeStatus(string description, bool status)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var vehicleInContext = context.VehicleTypes.FirstOrDefault(v => v.Description == description);
                if (vehicleInContext != null)
                {
                    try
                    {
                        vehicleInContext.Status = status;
                        context.SaveChanges();
                        return vehicleInContext;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }
                else
                {
                    throw new Exception("סוג רכב לא קיים במערכת");
                }
            }
        }
    }
}
