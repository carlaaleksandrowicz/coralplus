﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoralBusinessLogics
{
    public class PolicyHoldersLogic
    {
        public List<PolicyOwner> GetAllPolicyHolders()
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.PolicyOwners.OrderBy(o=>o.PolicyOwnerName).ToList();
            }
        }

        public int InsertPolicyHolder(string name, string idNumber, string city, string street, string homeNumber, string aptNumber, string zipCode, string mailingAddress, string phone, string cellPhone, string fax, string mail, string contactPerson)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var policyHolderInContext = context.PolicyOwners.FirstOrDefault(h => h.PolicyOwnerName == name);
                if (policyHolderInContext == null)
                {
                    try
                    {
                        PolicyOwner newPolicyHolder = new PolicyOwner()
                        {
                            PolicyOwnerName = name,
                            IdNumber = idNumber,
                            City = city,
                            Street = street,
                            HomeNumber = homeNumber,
                            AptNumber = aptNumber,
                            ZipCode = zipCode,
                            MailingAddress = mailingAddress,
                            Phone = phone,
                            CellPhone = cellPhone,
                            Fax = fax,
                            Email = mail,
                            ContactPerson = contactPerson
                        };
                        context.PolicyOwners.Add(newPolicyHolder);
                        context.SaveChanges();
                        return newPolicyHolder.PolicyOwnerID;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }
                else
                {
                    throw new Exception("מפעל קיים במערכת");
                }
            }
        }

        public int UpdatePolicyHolder(PolicyOwner policyOwner, string name, string idNumber, string city, string street, string homeNumber, string aptNumber, string zipCode, string mailingAddress, string phone, string cellPhone, string fax, string mail, string contactPerson)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var policyHolderInContext = context.PolicyOwners.FirstOrDefault(h => h.PolicyOwnerID == policyOwner.PolicyOwnerID);
                if (policyHolderInContext != null)
                {
                    try
                    {
                        policyHolderInContext.PolicyOwnerName = name;
                        policyHolderInContext.IdNumber = idNumber;
                        policyHolderInContext.City = city;
                        policyHolderInContext.Street = street;
                        policyHolderInContext.HomeNumber = homeNumber;
                        policyHolderInContext.AptNumber = aptNumber;
                        policyHolderInContext.ZipCode = zipCode;
                        policyHolderInContext.MailingAddress = mailingAddress;
                        policyHolderInContext.Phone = phone;
                        policyHolderInContext.CellPhone = cellPhone;
                        policyHolderInContext.Fax = fax;
                        policyHolderInContext.Email = mail;
                        policyHolderInContext.ContactPerson = contactPerson;

                        context.SaveChanges();
                        return policyHolderInContext.PolicyOwnerID;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }
                else
                {
                    throw new Exception("מפעל לא קיים במערכת");
                }
            }
        }

        public void InsertNumbersToPolicyHolder(int policyHolderId, List<FactoryNumber> numbers)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var policyHolderInContext = context.PolicyOwners.FirstOrDefault(h => h.PolicyOwnerID == policyHolderId);
                if (policyHolderInContext != null)
                {
                    try
                    {
                        foreach (var item in numbers)
                        {
                            if (item.FactoryNumberID==0)
                            {
                                item.PolicyOwnerID = policyHolderId;
                                item.Company = null;
                                item.Insurance = null;
                                context.FactoryNumbers.Add(item); 
                            }
                            else
                            {
                                var numberInContext = context.FactoryNumbers.FirstOrDefault(f => f.FactoryNumberID == item.FactoryNumberID);
                                if (numberInContext!=null)
                                {
                                    numberInContext.Number = item.Number;
                                }
                            }
                        }
                        context.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }
                else
                {
                    throw new Exception("מפעל לא קיים במערכת");
                }
            }
        }

        public List<FactoryNumber> GetNumbersByPolicyHolderAndCompany(int policyHolderId, int companyId, int insuranceId)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.FactoryNumbers.Where(n => n.PolicyOwnerID == policyHolderId && n.CompanyID == companyId && n.InsuranceID == insuranceId).ToList();
            }
        }

        public List<FactoryNumber> GetNumbersByPolicyHolder(int policyHolderId)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.FactoryNumbers.Include("Company").Include("Insurance").Where(n => n.PolicyOwnerID == policyHolderId).ToList();
            }
        }

    }
}
