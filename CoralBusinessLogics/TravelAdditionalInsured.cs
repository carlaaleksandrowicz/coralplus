//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CoralBusinessLogics
{
    using System;
    using System.Collections.Generic;
    
    public partial class TravelAdditionalInsured
    {
        public TravelAdditionalInsured()
        {
            this.TravelInsuranceDetails = new HashSet<TravelInsuranceDetail>();
        }
    
        public int TavelAdditionalInsuredID { get; set; }
        public int TravelPolicyID { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string IdNumber { get; set; }
        public Nullable<System.DateTime> BirthDate { get; set; }
        public string Profession { get; set; }
        public Nullable<bool> IsMale { get; set; }
        public string MaritalStatus { get; set; }
        public string Relationship { get; set; }
    
        public virtual TravelPolicy TravelPolicy { get; set; }
        public virtual ICollection<TravelInsuranceDetail> TravelInsuranceDetails { get; set; }
    }
}
