﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoralBusinessLogics
{
    public class WorkTasksLogic
    {
        RolesLogic rolesLogic = new RolesLogic();
        public List<RequestCategory> GetAllRequestCategories()
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.RequestCategories.Include("Insurance").OrderByDescending(c => c.Status).ThenBy(c => c.RequestCategoryName).ToList();
            }
        }

        public List<RequestCategory> GetRequestCategoriesByInsuranceId(int? insuranceId)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                if (insuranceId != null)
                {
                    return context.RequestCategories.Where(c => c.InsuranceID == insuranceId).OrderByDescending(c => c.Status).ThenBy(c => c.RequestCategoryName).ToList();

                }
                else
                {
                    return context.RequestCategories.Where(c => c.InsuranceID.Equals(null)).OrderByDescending(c => c.Status).ThenBy(c => c.RequestCategoryName).ToList();
                }
            }
        }

        //public List<RequestCategory> GetAllRequestCategories()
        //{
        //    using (Coral_DB_Entities context = new Coral_DB_Entities())
        //    {
        //        return context.RequestCategories.ToList();
        //    }
        //}

        public List<RequestCategory> GetActiveRequestCategoriesByInsuranceId(int? insuranceId)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                if (insuranceId != null)
                {
                    return context.RequestCategories.Where(c => c.InsuranceID == insuranceId && c.Status == true).OrderBy(c => c.RequestCategoryName).ToList();
                }
                else
                {
                    return context.RequestCategories.Where(c => c.InsuranceID.Equals(null) && c.Status == true).OrderBy(c => c.RequestCategoryName).ToList();

                }
            }
        }

        public RequestCategory InsertRequestCategory(int? insuranceId, string categoryName, bool status)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                try
                {
                    RequestCategory newRequestCategory = new RequestCategory()
                    {
                        InsuranceID = insuranceId,
                        RequestCategoryName = categoryName,
                        Status = status
                    };
                    context.RequestCategories.Add(newRequestCategory);
                    context.SaveChanges();
                    return newRequestCategory;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
        }

        public RequestCategory UpdateRequestCategorystatus(int requestCategoryId, bool status)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                try
                {
                    var requestCategoryInContext = context.RequestCategories.FirstOrDefault(r => r.RequestCategoryID == requestCategoryId);
                    if (requestCategoryInContext != null)
                    {
                        requestCategoryInContext.Status = status;
                        context.SaveChanges();
                        return requestCategoryInContext;
                    }
                    else
                    {
                        throw new Exception("קטגוריה לא קיימת במערכת");
                    }

                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
        }

        public List<RequestCondition> GetAllRequestConditions()
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.RequestConditions.Include("RequestCategory").OrderByDescending(c => c.Status).ThenBy(c => c.RequestConditionName).ToList();
            }
        }

        public List<RequestCondition> GetAllRequestConditionsByCategory(int categoryId)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.RequestConditions.Where(c => c.RequestCategoryID == categoryId).OrderByDescending(c => c.Status).ThenBy(c => c.RequestConditionName).ToList();
            }
        }

        public List<RequestCondition> GetActiveRequestConditionsByCategory(int categoryId)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.RequestConditions.Where(c => c.RequestCategoryID == categoryId && c.Status == true).OrderBy(c => c.RequestConditionName).ToList();
            }
        }

        public RequestCondition InsertRequestCondition(int categoryId, string conditionyName, bool status)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                try
                {
                    RequestCondition newRequestCondition = new RequestCondition()
                    {
                        RequestCategoryID = categoryId,
                        RequestConditionName = conditionyName,
                        Status = status
                    };
                    context.RequestConditions.Add(newRequestCondition);
                    context.SaveChanges();
                    return newRequestCondition;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
        }

        public RequestCondition UpdateRequestConditionStatus(int requestConditionId, bool status)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                try
                {
                    var requestConditionInContext = context.RequestConditions.FirstOrDefault(r => r.RequestConditionID == requestConditionId);
                    if (requestConditionInContext != null)
                    {
                        requestConditionInContext.Status = status;
                        context.SaveChanges();
                        return requestConditionInContext;
                    }
                    else
                    {
                        throw new Exception("מצב משימה לא קיימת במערכת");
                    }

                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
        }
        public List<Request> GetAllRequests(User user)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var requests = context.Requests.Include("User1").Include("RequestCategory").Include("RequestCondition").Include("ElementaryPolicy").Include("HealthPolicy").Include("TravelPolicy").Include("LifePolicy").Include("FinancePolicy").Include("PersonalAccidentsPolicy").Include("ForeingWorkersPolicy").Include("ForeingWorkersPolicy.ForeingWorkersInsuranceType").Include("ElementaryPolicy.ElementaryInsuranceType").Include("HealthPolicy.HealthInsuranceType").Include("LifePolicy.FundType").Include("FinancePolicy.FinanceProgram").Include("Client").OrderBy(r => r.RequestStatus == false).ThenBy(r => r.RequestDueDate).ToList();
                List<User> admins = rolesLogic.GetUsersInRole("מנהל ראשי").ToList();
                if (rolesLogic.IsUserInRole(user.Usermame, "מנהל ראשי"))
                {
                    return requests.ToList();
                }
                else if (rolesLogic.IsUserInRole(user.Usermame, "מנהל"))
                {
                    foreach (var item in admins)
                    {
                        requests.RemoveAll(r => r.HandledByUserID == item.UserID);
                    }
                    return requests.ToList();
                }
                else
                {
                    return requests.Where(r => r.HandledByUserID == user.UserID).ToList();
                }
            }
        }

        public int GetActiveRequestsCount(User user)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var requestsInContext = context.Requests.Where(r => r.HandledByUserID == user.UserID && r.RequestStatus == true).ToList();
                if (requestsInContext != null)
                {
                    return requestsInContext.Count;
                }
                else
                {
                    return 0;
                }
            }
        }
        public List<Request> GetAllRequestsByClient(int clientId, User user)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var requests = context.Requests.Include("Client").Include("User1").Include("RequestCategory").Include("RequestCondition").Include("ElementaryPolicy").Include("HealthPolicy").Include("TravelPolicy").Include("LifePolicy").Include("FinancePolicy").Include("PersonalAccidentsPolicy").Include("ForeingWorkersPolicy").Include("ForeingWorkersPolicy.ForeingWorkersInsuranceType").Include("ElementaryPolicy.ElementaryInsuranceType").Include("HealthPolicy.HealthInsuranceType").Include("FinancePolicy.FinanceProgram").Include("Client").Where(r => r.ClientID == clientId).OrderBy(r => r.RequestStatus == false).ThenBy(r => r.RequestDueDate).ToList();
                List<User> admins = rolesLogic.GetUsersInRole("מנהל ראשי").ToList();
                if (rolesLogic.IsUserInRole(user.Usermame, "מנהל ראשי"))
                {
                    return requests.ToList();
                }
                else if (rolesLogic.IsUserInRole(user.Usermame, "מנהל"))
                {
                    foreach (var item in admins)
                    {
                        requests.RemoveAll(r => r.HandledByUserID == item.UserID);
                    }
                    return requests.ToList();
                }
                else
                {
                    return requests.Where(r => r.HandledByUserID == user.UserID).ToList();
                }
            }
        }

        public int InsertRequest(int? clientId, DateTime openDate, int? requestCategoryId, int? elementaryPolicyId, int? lifePolicyId,int? healthPolicyId, int? financePolicyId, int? personalAccidentsPolicyId, int? travelPolicyId, int? foreingWorkersPolicyId, bool? requestStatus, int? requestConditionId, int? openedByUserId, int? handledByUserId, bool? isRequesterClient, string requesterLastName, string requesterFirstName, string requesterIdNumber, string requesterPhone, string requesterCellPhone, string requesterFax, string requesterEmail, string requesterAddress, DateTime? requestDate, TimeSpan? requestHour, string requestSubject, string requestDescription, DateTime? requestDueDate, DateTime? requestClosingDate, string handlerCompanyName, string handlerLastName, string handlerFirstName, string handlerPhone, string handlerCellPhone, string handlerFax, string handlerEmail, string handlerAddress, string handlerZipCode, DateTime? handlingOpenDate, DateTime? handlingClosingDate, bool? isReminder, DateTime? reminderDate, TimeSpan? reminderHour)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                try
                {
                    Request newRequest = new Request()
                    {
                        ClientID = clientId,
                        OpenDate = openDate,
                        RequestCategoryID = requestCategoryId,
                        ElementaryPolicyID = elementaryPolicyId,
                        LifePolicyID = lifePolicyId,
                        HealthPolicyID=healthPolicyId,
                        FinancePolicyID = financePolicyId,
                        ForeingWorkersPolicyID = foreingWorkersPolicyId,
                        PersonalAccidentsPolicyID = personalAccidentsPolicyId,
                        TravelPolicyID = travelPolicyId,
                        RequestStatus = requestStatus,
                        RequestConditionID = requestConditionId,
                        OpenedByUserID = openedByUserId,
                        HandledByUserID = handledByUserId,
                        IsRequesterClient = isRequesterClient,
                        RequesterLastName = requesterLastName,
                        RequesterFirstName = requesterFirstName,
                        RequesterIdNumber = requesterIdNumber,
                        RequesterPhone = requesterPhone,
                        RequesterCellPhone = requesterCellPhone,
                        RequesterFax = requesterFax,
                        RequesterEmail = requesterEmail,
                        RequesterAddress = requesterAddress,
                        RequesteDate = requestDate,
                        RequestHour = requestHour,
                        RequestSubject = requestSubject,
                        RequestDescription = requestDescription,
                        RequestDueDate = requestDueDate,
                        RequestClosingDate = requestClosingDate,
                        HandlerCompanyName = handlerCompanyName,
                        HandlerLastName = handlerLastName,
                        HandlerFirstName = handlerFirstName,
                        HandlerPhone = handlerPhone,
                        HandlerCellPhone = handlerCellPhone,
                        HandlerFax = handlerFax,
                        HandlerEmail = handlerEmail,
                        HandlerAddress = handlerAddress,
                        HandlerZipCode = handlerZipCode,
                        HandlerOpenDate = handlingOpenDate,
                        HandlerClosingDate = handlingClosingDate,
                        IsReminder = isReminder,
                        ReminderDate = reminderDate,
                        ReminderHour = reminderHour
                    };
                    context.Requests.Add(newRequest);
                    context.SaveChanges();
                    return newRequest.RequestID;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
        }

        public int UpdateRequest(int requestId, int? clientId, int? requestCategoryId, int? elementaryPolicyId, int? lifePolicyId, int? healthPolicyId, int? financePolicyId, int? personalAccidentsPolicyId, int? travelPolicyId, int? foreingWorkersPolicyId, bool? requestStatus, int? requestConditionId, int? openedByUserId, int? handledByUserId, bool? isRequesterClient, string requesterLastName, string requesterFirstName, string requesterIdNumber, string requesterPhone, string requesterCellPhone, string requesterFax, string requesterEmail, string requesterAddress, DateTime? requestDate, TimeSpan? requestHour, string requestSubject, string requestDescription, DateTime? requestDueDate, DateTime? requestClosingDate, string handlerCompanyName, string handlerLastName, string handlerFirstName, string handlerPhone, string handlerCellPhone, string handlerFax, string handlerEmail, string handlerAddress, string handlerZipCode, DateTime? handlingOpenDate, DateTime? handlingClosingDate, bool? isReminder, DateTime? reminderDate, TimeSpan? reminderHour)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                try
                {
                    var requestInContext = context.Requests.FirstOrDefault(r => r.RequestID == requestId);

                    requestInContext.ClientID = clientId;
                    requestInContext.RequestCategoryID = requestCategoryId;
                    requestInContext.ElementaryPolicyID = elementaryPolicyId;
                    requestInContext.LifePolicyID = lifePolicyId;
                    requestInContext.HealthPolicyID = healthPolicyId;
                    requestInContext.FinancePolicyID = financePolicyId;
                    requestInContext.ForeingWorkersPolicyID = foreingWorkersPolicyId;
                    requestInContext.PersonalAccidentsPolicyID = personalAccidentsPolicyId;
                    requestInContext.TravelPolicyID = travelPolicyId;
                    requestInContext.RequestStatus = requestStatus;
                    requestInContext.RequestConditionID = requestConditionId;
                    requestInContext.OpenedByUserID = openedByUserId;
                    requestInContext.HandledByUserID = handledByUserId;
                    requestInContext.IsRequesterClient = isRequesterClient;
                    requestInContext.RequesterLastName = requesterLastName;
                    requestInContext.RequesterFirstName = requesterFirstName;
                    requestInContext.RequesterIdNumber = requesterIdNumber;
                    requestInContext.RequesterPhone = requesterPhone;
                    requestInContext.RequesterCellPhone = requesterCellPhone;
                    requestInContext.RequesterFax = requesterFax;
                    requestInContext.RequesterEmail = requesterEmail;
                    requestInContext.RequesterAddress = requesterAddress;
                    requestInContext.RequesteDate = requestDate;
                    requestInContext.RequestHour = requestHour;
                    requestInContext.RequestSubject = requestSubject;
                    requestInContext.RequestDescription = requestDescription;
                    requestInContext.RequestDueDate = requestDueDate;
                    requestInContext.RequestClosingDate = requestClosingDate;
                    requestInContext.HandlerCompanyName = handlerCompanyName;
                    requestInContext.HandlerLastName = handlerLastName;
                    requestInContext.HandlerFirstName = handlerFirstName;
                    requestInContext.HandlerPhone = handlerPhone;
                    requestInContext.HandlerCellPhone = handlerCellPhone;
                    requestInContext.HandlerFax = handlerFax;
                    requestInContext.HandlerEmail = handlerEmail;
                    requestInContext.HandlerAddress = handlerAddress;
                    requestInContext.HandlerZipCode = handlerZipCode;
                    requestInContext.HandlerOpenDate = handlingOpenDate;
                    requestInContext.HandlerClosingDate = handlingClosingDate;
                    requestInContext.IsReminder = isReminder;
                    requestInContext.ReminderDate = reminderDate;
                    requestInContext.ReminderHour = reminderHour;
                    context.SaveChanges();
                    return requestInContext.RequestID;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
        }
        public List<Request> GetRequestsByDueDate(DateTime? fromDate, DateTime? toDate, User user)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var requests = context.Requests.Include("User1").Include("RequestCategory").Include("RequestCondition").Include("ElementaryPolicy").Include("LifePolicy").Include("HealthPolicy").Include("TravelPolicy").Include("FinancePolicy").Include("PersonalAccidentsPolicy").Include("ForeingWorkersPolicy").Include("ForeingWorkersPolicy.ForeingWorkersInsuranceType").Include("HealthPolicy.HealthInsuranceType").Include("ElementaryPolicy.ElementaryInsuranceType").Include("LifePolicy.FundType").Include("FinancePolicy.FinanceProgram").Include("Client").Where(r => r.RequestDueDate >= fromDate && r.RequestDueDate <= toDate).OrderBy(r => r.RequestStatus == false).ThenBy(r => r.RequestDueDate).ToList();
                List<User> admins = rolesLogic.GetUsersInRole("מנהל ראשי").ToList();
                if (rolesLogic.IsUserInRole(user.Usermame, "מנהל ראשי"))
                {
                    return requests.ToList();
                }
                else if (rolesLogic.IsUserInRole(user.Usermame, "מנהל"))
                {
                    foreach (var item in admins)
                    {
                        requests.RemoveAll(r => r.HandledByUserID == item.UserID);
                    }
                    return requests.ToList();
                }
                else
                {
                    return requests.Where(r => r.HandledByUserID == user.UserID).ToList();
                }
            }
        }

        public void DeleteRequest(Request requestsSelected)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                try
                {
                    var requestInContext = context.Requests.FirstOrDefault(r => r.RequestID == requestsSelected.RequestID);
                    if (requestInContext != null)
                    {
                        context.Requests.Remove(requestInContext);
                        context.SaveChanges();
                    }
                    else
                    {
                        throw new Exception("המשימה לא נמצא במערכת");
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
        }

        public List<Request> GetRequestsByContactDate(DateTime? fromDate, DateTime? toDate, User user)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var requests = context.Requests.Include("User1").Include("RequestCategory").Include("RequestCondition").Include("ElementaryPolicy").Include("HealthPolicy").Include("TravelPolicy").Include("LifePolicy").Include("FinancePolicy").Include("PersonalAccidentsPolicy").Include("ForeingWorkersPolicy").Include("ForeingWorkersPolicy.ForeingWorkersInsuranceType").Include("HealthPolicy.HealthInsuranceType").Include("ElementaryPolicy.ElementaryInsuranceType").Include("LifePolicy.FundType").Include("FinancePolicy.FinanceProgram").Include("Client").Where(r => r.RequesteDate >= fromDate && r.RequesteDate <= toDate).OrderBy(r => r.RequestStatus == false).ThenBy(r => r.RequestDueDate).ToList();
                List<User> admins = rolesLogic.GetUsersInRole("מנהל ראשי").ToList();
                if (rolesLogic.IsUserInRole(user.Usermame, "מנהל ראשי"))
                {
                    return requests.ToList();
                }
                else if (rolesLogic.IsUserInRole(user.Usermame, "מנהל"))
                {
                    foreach (var item in admins)
                    {
                        requests.RemoveAll(r => r.HandledByUserID == item.UserID);
                    }
                    return requests.ToList();
                }
                else
                {
                    return requests.Where(r => r.HandledByUserID == user.UserID).ToList();
                }
            }

        }

        public List<Request> GetRequestsByFilter(List<Request> requestsByDate, User userInCharge, RequestCategory category, string status, RequestCondition condition, string searchKey, bool isClientRequests)
        {
            if (requestsByDate != null)
            {
                if (userInCharge != null)
                {
                    requestsByDate = requestsByDate.Where(r => r.HandledByUserID == userInCharge.UserID).ToList();
                }
                if (searchKey != "")
                {
                    if (!isClientRequests)
                    {
                        requestsByDate = requestsByDate.Where(r => (r.Client != null && (r.Client.FirstName.StartsWith(searchKey) || r.Client.LastName.StartsWith(searchKey) || r.Client.IdNumber.StartsWith(searchKey))) || r.RequesterFirstName.StartsWith(searchKey) || r.RequesterLastName.StartsWith(searchKey) || r.RequestSubject.Contains(searchKey)).ToList();
                    }
                    else
                    {
                        requestsByDate = requestsByDate.Where(r => r.RequesterFirstName.StartsWith(searchKey) || r.RequesterLastName.StartsWith(searchKey) || r.RequestSubject.Contains(searchKey)).ToList();
                    }
                }
                if (category != null)
                {
                    requestsByDate = requestsByDate.Where(r => r.RequestCategoryID == category.RequestCategoryID).ToList();
                }
                if (condition != null)
                {
                    requestsByDate = requestsByDate.Where(r => r.RequestConditionID == condition.RequestConditionID).ToList();
                }
                if (status != null && status != "הצג הכל")
                {
                    if (status == "פתוח")
                    {
                        requestsByDate = requestsByDate.Where(r => r.RequestStatus == true).ToList();
                    }
                    else
                    {
                        requestsByDate = requestsByDate.Where(r => r.RequestStatus == false).ToList();
                    }
                }
            }
            return requestsByDate;
        }

        public Request GetRequestById(int requestId)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.Requests.Include("User1").Include("RequestCategory").Include("RequestCondition").Include("ElementaryPolicy").Include("HealthPolicy").Include("TravelPolicy").Include("LifePolicy").Include("FinancePolicy").Include("PersonalAccidentsPolicy").Include("ForeingWorkersPolicy").Include("ForeingWorkersPolicy.ForeingWorkersInsuranceType").Include("HealthPolicy.HealthInsuranceType").Include("ElementaryPolicy.ElementaryInsuranceType").Include("LifePolicy.FundType").Include("FinancePolicy.FinanceProgram").Include("Client").FirstOrDefault(r => r.RequestID == requestId);
            }
        }

        public void TransferRequests(List<Request> requestsToTransfer, int? transferToUserId)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                try
                {
                    foreach (var item in requestsToTransfer)
                    {
                        var requestInContext = context.Requests.FirstOrDefault(r => r.RequestID == item.RequestID);
                        requestInContext.HandledByUserID = transferToUserId;
                    }
                    context.SaveChanges();
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
        }

        public List<Request> Get24HoursReminders(User handlerUser)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                try
                {
                    DateTime now = DateTime.Now;
                    DateTime tomorrowNow = now.AddDays(1).AddMilliseconds(-1);
                    var reminderRequests = context.Requests.Include("User1").Include("RequestCategory").Include("RequestCondition").Include("ElementaryPolicy").Include("HealthPolicy").Include("TravelPolicy").Include("LifePolicy").Include("FinancePolicy").Include("PersonalAccidentsPolicy").Include("ForeingWorkersPolicy").Include("ForeingWorkersPolicy.ForeingWorkersInsuranceType").Include("HealthPolicy.HealthInsuranceType").Include("ElementaryPolicy.ElementaryInsuranceType").Include("LifePolicy.FundType").Include("FinancePolicy.FinanceProgram").Include("Client").Where(r => r.HandledByUserID == handlerUser.UserID && r.IsReminder == true).ToList();
                    List<Request> oneDayRequests = new List<Request>();
                    foreach (var item in reminderRequests)
                    {
                        DateTime reminderTime = Convert.ToDateTime(item.ReminderHour.ToString());
                        DateTime reminderDateTime = new DateTime(((DateTime)item.ReminderDate).Year, ((DateTime)item.ReminderDate).Month, ((DateTime)item.ReminderDate).Day, reminderTime.Hour, reminderTime.Minute, reminderTime.Second, reminderTime.Millisecond);
                        if (reminderDateTime >= now && reminderDateTime <= tomorrowNow)
                        {
                            oneDayRequests.Add(item);
                        }
                    }
                    return oneDayRequests;

                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
        }

        public void UpdateRequestViewedStatus(Request request, bool wasRequestSeen)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                try
                {
                    var requestInContext = context.Requests.FirstOrDefault(r => r.RequestID == request.RequestID);
                    if (requestInContext != null)
                    {
                        requestInContext.WasReminderSeen = wasRequestSeen;
                        context.SaveChanges();
                    }
                    else
                    {
                        throw new Exception("התזכורת לא נמצא במערכת");
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
        }
        public List<Request> GetNotSeenReminders(User handlerUser)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                try
                {
                    DateTime now = DateTime.Now;
                    //DateTime tomorrowNow = now.AddDays(1).AddMilliseconds(-1);
                    var reminderRequests = context.Requests.Include("User1").Include("RequestCategory").Include("RequestCondition").Include("ElementaryPolicy").Include("LifePolicy").Include("FinancePolicy").Include("HealthPolicy").Include("TravelPolicy").Include("PersonalAccidentsPolicy").Include("ElementaryPolicy.ElementaryInsuranceType").Include("ForeingWorkersPolicy").Include("ForeingWorkersPolicy.ForeingWorkersInsuranceType").Include("HealthPolicy.HealthInsuranceType").Include("LifePolicy.FundType").Include("FinancePolicy.FinanceProgram").Include("Client").Where(r => r.HandledByUserID == handlerUser.UserID && r.IsReminder == true).ToList();
                    List<Request> nonSeenRequests = new List<Request>();
                    foreach (var item in reminderRequests)
                    {
                        DateTime reminderTime = Convert.ToDateTime(item.ReminderHour.ToString());
                        DateTime reminderDateTime = new DateTime(((DateTime)item.ReminderDate).Year, ((DateTime)item.ReminderDate).Month, ((DateTime)item.ReminderDate).Day, reminderTime.Hour, reminderTime.Minute, reminderTime.Second, reminderTime.Millisecond);
                        if (item.WasReminderSeen != true && reminderDateTime < now)
                        {
                            nonSeenRequests.Add(item);
                        }
                    }
                    return nonSeenRequests;

                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
        }

        public Request ResetReminder(Request request, DateTime reminderDate, TimeSpan reminderTime)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                try
                {
                    var requestInContext = context.Requests.Include("User1").Include("RequestCategory").Include("RequestCondition").Include("ElementaryPolicy").Include("HealthPolicy").Include("TravelPolicy").Include("LifePolicy").Include("FinancePolicy").Include("PersonalAccidentsPolicy").Include("ElementaryPolicy.ElementaryInsuranceType").Include("LifePolicy.FundType").Include("HealthPolicy.HealthInsuranceType").Include("FinancePolicy.FinanceProgram").Include("Client").FirstOrDefault(r => r.RequestID == request.RequestID);
                    if (requestInContext != null)
                    {
                        requestInContext.ReminderDate = reminderDate;
                        requestInContext.ReminderHour = reminderTime;
                        context.SaveChanges();
                        return requestInContext;
                    }
                    else
                    {
                        throw new Exception("התזכורת לא נמצא במערכת");
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
        }
    }
}
