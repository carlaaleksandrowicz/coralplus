﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoralBusinessLogics
{
    //ניהול חברות וסוגי ביטוח
    public class InsurancesLogic
    {
        //מחזיר רשימה של כל סוגי הביטוח
        public List<Insurance> GetAllInsurances()
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.Insurances.OrderBy(i => i.InsuranceName).ToList();
            }
        }

        //מוסיף סוג ביטוח חדש
        public void InsertInsurance(string insuranceName)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var insuranceInContext = context.Insurances.FirstOrDefault(i => i.InsuranceName == insuranceName);
                if (insuranceInContext == null)
                {
                    try
                    {
                        Insurance newInsurance = new Insurance() { InsuranceName = insuranceName };
                        context.Insurances.Add(newInsurance);
                        context.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }
                else
                {
                    throw new Exception("סוג ביטוח קיים");
                }
            }
        }

        //מוסיף חברת ביטוח לסוג ביטוח
        public InsuranceCompany InsertCompanyToInsurance(string companyName, int insuranceID, bool status)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var insuranceInContext = context.Insurances.Where(i => i.InsuranceID == insuranceID).FirstOrDefault();
                if (insuranceInContext != null)
                {
                    var companyInContext = context.Companies.Where(c => c.CompanyName == companyName).FirstOrDefault();
                    if (companyInContext == null)
                    {
                        try
                        {
                            CompaniesLogic logic = new CompaniesLogic();
                            logic.InsertCompany(companyName);
                            companyInContext = context.Companies.Where(c => c.CompanyName == companyName).FirstOrDefault();
                        }
                        catch (Exception ex)
                        {
                            throw new Exception(ex.Message);
                        }
                    }
                    var insuranceCompanyInContext = context.InsuranceCompanies.Where(c => c.CompanyID == companyInContext.CompanyID && c.InsuranceID == insuranceInContext.InsuranceID).FirstOrDefault();
                    if (insuranceCompanyInContext == null)
                    {
                        try
                        {
                            InsuranceCompany companyToAdd = new InsuranceCompany() { InsuranceID = insuranceInContext.InsuranceID, CompanyID = companyInContext.CompanyID, Status = status };
                            context.InsuranceCompanies.Add(companyToAdd);
                            context.SaveChanges();
                            return companyToAdd;
                        }
                        catch (Exception ex)
                        {
                            throw new Exception(ex.Message);
                        }
                    }
                    else
                    {
                        throw new Exception("חברה קיימת במערכת");
                    }
                }
                else
                {
                    throw new Exception("סוג ביטוח לא תקין");
                }
            }
        }

        //מעדכן את הסטטוס של חברת ביטוח
        public InsuranceCompany UpdateInsuranceCompanieStatus(InsuranceCompany company, bool status)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {

                var insuranceCompanyInContext = context.InsuranceCompanies.Where(c => c.CompanyID == company.CompanyID && c.InsuranceID == company.InsuranceID).FirstOrDefault();
                if (insuranceCompanyInContext != null)
                {
                    try
                    {
                        insuranceCompanyInContext.Status = status;
                        context.SaveChanges();
                        return insuranceCompanyInContext;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }
                else
                {
                    throw new Exception("חברה לא קיימת במערכת");
                }
            }
        }

        public List<Company> GetAllCompanies()
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.Companies.OrderBy(c=>c.CompanyName).ToList();                
            }
        }

        //מחזיר רשימה של כל החברות לפי סוג ביטוח
        public List<InsuranceCompany> GetCompaniesByInsurance(int insuranceID)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {

                return (from c in context.InsuranceCompanies.Include("Company")
                        where c.InsuranceID == insuranceID
                        select c).OrderByDescending(c => c.Status).ThenBy(c => c.Company.CompanyName).ToList();
            }
        }

        // מחזיר רשימה של כל החברות הפעילות לפי סוג ביטוח עפ"י מס' זיהוי
        public List<InsuranceCompany> GetActiveCompaniesByInsurance(int insuranceID)
        {
            var companiesByInsurance = GetCompaniesByInsurance(insuranceID);
            return companiesByInsurance.Where(c => c.Status == true).ToList();
        }

        // מחזיר רשימה של כל החברות הפעילות לפי סוג ביטוח עפ"י שם הביטוח

        public List<InsuranceCompany> GetActiveCompaniesByInsurance(string insuranceName)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return (from c in context.InsuranceCompanies.Include("Company")
                        where c.Insurance.InsuranceName == insuranceName
                        where c.Status==true
                        select c).OrderBy(c => c.Company.CompanyName).ToList();
            }
        }

        //מחזיר את מס' הזיהוי של סוג ביטוח עפ"י שם ביטוח
        public int GetInsuranceID(string insuranceName)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.Insurances.FirstOrDefault(i => i.InsuranceName == insuranceName).InsuranceID;
            }
        }
    }
}
