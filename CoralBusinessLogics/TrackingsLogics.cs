﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoralBusinessLogics
{
    //מחלקה העוסקת במעקב פוליסות, תביעות וכו
    public class TrackingsLogics
    {
        //מחזיר רשימה של כל המעקבים של פוליסה אלמנטרית מסוימת
        public List<ElementaryTracking> GetAllElementaryTrackingsByPolicy(string policyNumber, int? addition)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                //var policyInContext = context.ElementaryPolicies.FirstOrDefault(p => p.PolicyNumber == policyNumber);
                var policyInContext = context.ElementaryPolicies.Where(c => c.PolicyNumber == policyNumber).Where(p => p.Addition == addition).FirstOrDefault();
                return context.ElementaryTrackings.Include("User").Where(t => t.ElementaryPolicyID == policyInContext.ElementaryPolicyID).ToList();
            }
        }

        //מוסיף מעקבים לפוליסה מסוימת
        public void InsertElementaryTrackings(int policyId,List<TrackingsViewModel> trackings, int? addition, string policyNumber, int userID)
        {
            if (trackings.Count > 0)
            {
                using (Coral_DB_Entities context = new Coral_DB_Entities())
                {
                    //var policyInContext = context.ElementaryPolicies.FirstOrDefault(p => p.PolicyNumber == policyNumber);
                    //var policyInContext = context.ElementaryPolicies.Where(c => c.PolicyNumber == policyNumber).Where(p => p.Addition == addition).FirstOrDefault();
                    var policyInContext = context.ElementaryPolicies.FirstOrDefault(c => c.ElementaryPolicyID == policyId);


                    if (policyInContext != null)
                    {
                        try
                        {
                            foreach (TrackingsViewModel tracking in trackings)
                            {
                                ElementaryTracking newTracking = new ElementaryTracking() { ElementaryPolicyID = policyInContext.ElementaryPolicyID, UserID = userID, Date = tracking.TrackingDate, TrackingContent = tracking.TrackingContent };
                                context.ElementaryTrackings.Add(newTracking);
                            }
                            context.SaveChanges();
                        }
                        catch (Exception e)
                        {
                            throw new Exception(e.Message);
                        }
                    }
                    else
                    {
                        throw new Exception("פוליסה לא קיימת במערכת");
                    }

                }
            }

        }

        //מעדכן מעקבים בפוליסה מסוימת
        public void UpdateElementaryTrackings(int policyId, List<TrackingsViewModel> trackings, int? addition, List<TrackingsViewModel> trackingsToAdd, string policyNumber, int userId)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                //List<TrackingsViewModel> trackingsToAdd = new List<TrackingsViewModel>();
                //var policyInContext = context.ElementaryPolicies.FirstOrDefault(p => p.PolicyNumber == policyNumber);
                //var policyInContext = context.ElementaryPolicies.Where(c => c.PolicyNumber == policyNumber).Where(p => p.Addition == addition).FirstOrDefault();
                var policyInContext = context.ElementaryPolicies.FirstOrDefault(c => c.ElementaryPolicyID == policyId);

                if (policyInContext != null)
                {

                    var trackingsInContext = context.ElementaryTrackings.Where(t => t.ElementaryPolicyID == policyInContext.ElementaryPolicyID).ToList();
                    foreach (var trackingInContext in trackingsInContext)
                    {
                        bool isBreaked = false;
                        foreach (var item in trackings)
                        {
                            if (trackingInContext.TrackingContent == item.TrackingContent && trackingInContext.Date == item.TrackingDate)
                            {
                                trackingsToAdd.Remove(item);
                                isBreaked = true;
                                break;
                            }
                        }
                        if (!isBreaked)
                        {
                            context.ElementaryTrackings.Remove(trackingInContext);
                            context.SaveChanges();
                        }
                    }
                    InsertElementaryTrackings(policyId,trackingsToAdd, addition, policyNumber, userId);
                }
                else
                {
                    throw new Exception("פוליסה לא קיימת במערכת");
                }
            }
        }

        public List<ElementaryClaimTracking> GetAllClaimElementaryTrackingsByClaimID(int claimId)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.ElementaryClaimTrackings.Include("User").Where(t => t.ElementaryClaimID == claimId).ToList();
            }
        }

        public void InsertElementaryClaimTrackings(List<ElementaryClaimTracking> trackings, int claimId, int userID)
        {
            if (trackings.Count > 0)
            {
                using (Coral_DB_Entities context = new Coral_DB_Entities())
                {
                    var claimInContext = context.ElementaryClaims.FirstOrDefault(c => c.ElementaryClaimID == claimId);
                    if (claimInContext != null)
                    {
                        try
                        {
                            foreach (var tracking in trackings)
                            {
                                if (tracking.ElementaryClaimTrackingID == 0)
                                {
                                    ElementaryClaimTracking newTracking = new ElementaryClaimTracking() { ElementaryClaimID = claimId, UserID = userID, Date = tracking.Date, TrackingContent = tracking.TrackingContent };
                                    context.ElementaryClaimTrackings.Add(newTracking);
                                }
                                else
                                {
                                    var trackingInContext = context.ElementaryClaimTrackings.FirstOrDefault(t => t.ElementaryClaimTrackingID == tracking.ElementaryClaimTrackingID);
                                    trackingInContext.TrackingContent = tracking.TrackingContent;
                                }
                            }
                            context.SaveChanges();
                        }
                        catch (Exception e)
                        {
                            throw new Exception(e.Message);
                        }
                    }
                    else
                    {
                        throw new Exception("תביעה לא קיימת במערכת");
                    }

                }
            }

        }

        public List<ForeingWorkersClaimTracking> GetAllForeingWorkersClaimTrackingsByClaimID(int claimId)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.ForeingWorkersClaimTrackings.Include("User").Where(t => t.ForeingWorkersClaimID == claimId).ToList();
            }
        }

        public void DeleteElementaryClaimTracking(ElementaryClaimTracking tracking)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var trackingInContext = context.ElementaryClaimTrackings.FirstOrDefault(t => t.ElementaryClaimTrackingID == tracking.ElementaryClaimTrackingID);
                if (trackingInContext != null)
                {
                    context.ElementaryClaimTrackings.Remove(trackingInContext);
                    context.SaveChanges();
                }
            }

        }


        public List<RequestTracking> GetAllRequestTrackingsByRequestID(int requestId)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.RequestTrackings.Include("User").Where(t => t.RequestID == requestId).ToList();
            }
        }

        public void InsertRequestTrackings(List<RequestTracking> trackings, int requestId, int userID)
        {
            if (trackings.Count > 0)
            {
                using (Coral_DB_Entities context = new Coral_DB_Entities())
                {
                    var requestInContext = context.Requests.FirstOrDefault(r => r.RequestID == requestId);
                    if (requestInContext != null)
                    {
                        try
                        {
                            foreach (var tracking in trackings)
                            {
                                if (tracking.RequestTrackingID == 0)
                                {
                                    RequestTracking newTracking = new RequestTracking() { RequestID = requestId, UserID = userID, Date = tracking.Date, TrackingContent = tracking.TrackingContent };
                                    context.RequestTrackings.Add(newTracking);
                                }
                                else
                                {
                                    var trackingInContext = context.RequestTrackings.FirstOrDefault(t => t.RequestTrackingID == tracking.RequestTrackingID);
                                    trackingInContext.TrackingContent = tracking.TrackingContent;
                                }
                            }
                            context.SaveChanges();
                        }
                        catch (Exception e)
                        {
                            throw new Exception(e.Message);
                        }
                    }
                    else
                    {
                        throw new Exception("משימה לא קיימת במערכת");
                    }

                }
            }

        }

        public List<TravelTracking> GetAllTravelTrackingsByPolicy(int policyId)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.TravelTrackings.Include("User").Where(t => t.TravelPolicyID == policyId).ToList();
            }
        }

        public void DeleteRequestTracking(RequestTracking tracking)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var trackingInContext = context.RequestTrackings.FirstOrDefault(t => t.RequestTrackingID == tracking.RequestTrackingID);
                if (trackingInContext != null)
                {
                    context.RequestTrackings.Remove(trackingInContext);
                    context.SaveChanges();
                }
            }

        }

        public List<PersonalAccidentsTracking> GetAllPersonalAccidentsTrackingsByPolicy(int policyId)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.PersonalAccidentsTrackings.Include("User").Where(t => t.PersonalAccidentsPolicyID == policyId).ToList();
            }

        }

        public List<LifeTracking> GetAllLifeTrackingsByPolicy(int policyId)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                //var policyInContext = context.ElementaryPolicies.FirstOrDefault(p => p.PolicyNumber == policyNumber);
                //var policyInContext = context.LifePolicies.FirstOrDefault(c => c.LifePolicyID == policyId);
                return context.LifeTrackings.Include("User").Where(t => t.LifePolicyID == policyId).ToList();
            }
        }

        //מוסיף מעקבים לפוליסה מסוימת
        public void InsertLifeTrackings(int policyId, List<LifeTracking> trackings, bool isAddition)
        {
            if (trackings.Count > 0)
            {
                using (Coral_DB_Entities context = new Coral_DB_Entities())
                {
                    //var policyInContext = context.ElementaryPolicies.FirstOrDefault(p => p.PolicyNumber == policyNumber);
                    var policyInContext = context.LifePolicies.FirstOrDefault(c => c.LifePolicyID == policyId);
                    if (policyInContext != null)
                    {
                        try
                        {
                            var policyTrackings = context.LifeTrackings.Where(t => t.LifePolicyID == policyId).ToList();
                            foreach (LifeTracking tracking in trackings)
                            {
                                if (tracking.LifeTrackingID == 0)
                                {
                                    tracking.User = null;
                                    tracking.LifePolicyID = policyId;
                                    context.LifeTrackings.Add(tracking);
                                }
                                else if (isAddition)
                                {
                                    LifeTracking newTracking = new LifeTracking() { Date = tracking.Date, LifePolicyID = policyId, TrackingContent = tracking.TrackingContent, UserID = tracking.UserID };
                                    context.LifeTrackings.Add(newTracking);
                                }
                                else
                                {
                                    var trakingInContext = context.LifeTrackings.FirstOrDefault(t => t.LifeTrackingID == tracking.LifeTrackingID);
                                    if (trakingInContext != null)
                                    {
                                        trakingInContext.TrackingContent = tracking.TrackingContent;
                                    }
                                    policyTrackings.Remove(trakingInContext);
                                }
                                //LifeTracking newTracking = new ElementaryTracking() { ElementaryPolicyID = policyInContext.ElementaryPolicyID, UserID = userID, Date = tracking.TrackingDate, TrackingContent = tracking.TrackingContent };

                            }
                            if (policyTrackings.Count > 0)
                            {
                                foreach (var item in policyTrackings)
                                {
                                    context.LifeTrackings.Remove(item);
                                }
                            }
                            context.SaveChanges();
                        }
                        catch (Exception e)
                        {
                            throw new Exception(e.Message);
                        }
                    }
                    else
                    {
                        throw new Exception("פוליסה לא קיימת במערכת");
                    }

                }
            }

        }

        public void InsertForeingWorkersClaimTrackings(List<ForeingWorkersClaimTracking> trackings, int claimId, int userID)
        {
            if (trackings.Count > 0)
            {
                using (Coral_DB_Entities context = new Coral_DB_Entities())
                {
                    var claimInContext = context.ForeingWorkersClaims.FirstOrDefault(c => c.ForeingWorkersClaimID == claimId);
                    if (claimInContext != null)
                    {
                        try
                        {
                            foreach (var tracking in trackings)
                            {
                                if (tracking.ForeingWorkersClaimTrackingID == 0)
                                {
                                    //ForeingWorkersClaimTracking newTracking = new ForeingWorkersClaimTracking() { ForeingWorkersClaimID = claimId, UserID = userID, Date = tracking.Date, TrackingContent = tracking.TrackingContent };
                                    tracking.User = null;
                                    tracking.ForeingWorkersClaimID = claimId;
                                    tracking.UserID = userID;
                                    context.ForeingWorkersClaimTrackings.Add(tracking);
                                }
                                else
                                {
                                    var trackingInContext = context.ForeingWorkersClaimTrackings.FirstOrDefault(t => t.ForeingWorkersClaimTrackingID == tracking.ForeingWorkersClaimTrackingID);
                                    if (trackingInContext != null)
                                    {
                                        trackingInContext.TrackingContent = tracking.TrackingContent;
                                    }
                                }
                            }
                            context.SaveChanges();
                        }
                        catch (Exception e)
                        {
                            throw new Exception(e.Message);
                        }
                    }
                    else
                    {
                        throw new Exception("תביעה לא קיימת במערכת");
                    }

                }
            }
        }

        public void InsertLifeClaimTrackings(List<LifeClaimTracking> trackings, int claimId, int userID)
        {
            if (trackings.Count > 0)
            {
                using (Coral_DB_Entities context = new Coral_DB_Entities())
                {
                    var claimInContext = context.LifeClaims.FirstOrDefault(c => c.LifeClaimID == claimId);
                    if (claimInContext != null)
                    {
                        try
                        {
                            foreach (var tracking in trackings)
                            {
                                if (tracking.LifeClaimTrackingID == 0)
                                {
                                    //LifeClaimTracking newTracking = new LifeClaimTracking() { LifeClaimID = claimId, UserID = userID, Date = tracking.Date, TrackingContent = tracking.TrackingContent };
                                    tracking.User = null;
                                    tracking.LifeClaimID = claimId;
                                    tracking.UserID = userID;
                                    context.LifeClaimTrackings.Add(tracking);
                                }
                                else
                                {
                                    var trackingInContext = context.LifeClaimTrackings.FirstOrDefault(t => t.LifeClaimTrackingID == tracking.LifeClaimTrackingID);
                                    if (trackingInContext != null)
                                    {
                                        trackingInContext.TrackingContent = tracking.TrackingContent;
                                    }
                                }
                            }
                            context.SaveChanges();
                        }
                        catch (Exception e)
                        {
                            throw new Exception(e.Message);
                        }
                    }
                    else
                    {
                        throw new Exception("תביעה לא קיימת במערכת");
                    }

                }
            }

        }

        public void DeleteLifeClaimTracking(LifeClaimTracking tracking)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var trackingInContext = context.LifeClaimTrackings.FirstOrDefault(t => t.LifeClaimTrackingID == tracking.LifeClaimTrackingID);
                if (trackingInContext != null)
                {
                    context.LifeClaimTrackings.Remove(trackingInContext);
                    context.SaveChanges();
                }
            }

        }

        public void DeleteForeingWorkersClaimTracking(ForeingWorkersClaimTracking tracking)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var trackingInContext = context.ForeingWorkersClaimTrackings.FirstOrDefault(t => t.ForeingWorkersClaimTrackingID == tracking.ForeingWorkersClaimTrackingID);
                if (trackingInContext != null)
                {
                    context.ForeingWorkersClaimTrackings.Remove(trackingInContext);
                    context.SaveChanges();
                }
            }
        }

        public List<LifeClaimTracking> GetAllLifeClaimTrackingsByClaimID(int claimId)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.LifeClaimTrackings.Include("User").Where(t => t.LifeClaimID == claimId).ToList();
            }
        }

        public List<HealthTracking> GetAllHealthTrackingsByPolicy(int policyId)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.HealthTrackings.Include("User").Where(t => t.HealthPolicyID == policyId).ToList();
            }
        }

        //מוסיף מעקבים לפוליסה מסוימת
        public void InsertHealthTrackings(int policyId, List<HealthTracking> trackings, bool isAddition)
        {
            if (trackings.Count > 0)
            {
                using (Coral_DB_Entities context = new Coral_DB_Entities())
                {
                    //var policyInContext = context.ElementaryPolicies.FirstOrDefault(p => p.PolicyNumber == policyNumber);
                    var policyInContext = context.HealthPolicies.FirstOrDefault(c => c.HealthPolicyID == policyId);
                    if (policyInContext != null)
                    {
                        try
                        {
                            var policyTrackings = context.HealthTrackings.Where(t => t.HealthPolicyID == policyId).ToList();
                            foreach (HealthTracking tracking in trackings)
                            {
                                if (tracking.HealthTrackingID == 0)
                                {
                                    tracking.User = null;
                                    tracking.HealthPolicyID = policyId;
                                    context.HealthTrackings.Add(tracking);
                                }
                                else if (isAddition)
                                {
                                    HealthTracking newTracking = new HealthTracking() { Date = tracking.Date, HealthPolicyID = policyId, TrackingContent = tracking.TrackingContent, UserID = tracking.UserID };
                                    context.HealthTrackings.Add(newTracking);
                                }
                                else
                                {
                                    var trakingInContext = context.HealthTrackings.FirstOrDefault(t => t.HealthTrackingID == tracking.HealthTrackingID);
                                    if (trakingInContext != null)
                                    {
                                        trakingInContext.TrackingContent = tracking.TrackingContent;
                                    }
                                    policyTrackings.Remove(trakingInContext);
                                }
                                //LifeTracking newTracking = new ElementaryTracking() { ElementaryPolicyID = policyInContext.ElementaryPolicyID, UserID = userID, Date = tracking.TrackingDate, TrackingContent = tracking.TrackingContent };

                            }
                            if (policyTrackings.Count > 0)
                            {
                                foreach (var item in policyTrackings)
                                {
                                    context.HealthTrackings.Remove(item);
                                }
                            }
                            context.SaveChanges();
                        }
                        catch (Exception e)
                        {
                            throw new Exception(e.Message);
                        }
                    }
                    else
                    {
                        throw new Exception("פוליסה לא קיימת במערכת");
                    }

                }
            }

        }

        public void InsertHealthClaimTrackings(List<HealthClaimTracking> trackings, int claimId, int userID)
        {
            if (trackings.Count > 0)
            {
                using (Coral_DB_Entities context = new Coral_DB_Entities())
                {
                    var claimInContext = context.HealthClaims.FirstOrDefault(c => c.HealthClaimID == claimId);
                    if (claimInContext != null)
                    {
                        try
                        {
                            foreach (var tracking in trackings)
                            {
                                if (tracking.HealthClaimTrackingID == 0)
                                {
                                    //LifeClaimTracking newTracking = new LifeClaimTracking() { LifeClaimID = claimId, UserID = userID, Date = tracking.Date, TrackingContent = tracking.TrackingContent };
                                    tracking.User = null;
                                    tracking.HealthClaimID = claimId;
                                    tracking.UserID = userID;
                                    context.HealthClaimTrackings.Add(tracking);
                                }
                                else
                                {
                                    var trackingInContext = context.HealthClaimTrackings.FirstOrDefault(t => t.HealthClaimTrackingID == tracking.HealthClaimTrackingID);
                                    if (trackingInContext != null)
                                    {
                                        trackingInContext.TrackingContent = tracking.TrackingContent;
                                    }
                                }
                            }
                            context.SaveChanges();
                        }
                        catch (Exception e)
                        {
                            throw new Exception(e.Message);
                        }
                    }
                    else
                    {
                        throw new Exception("תביעה לא קיימת במערכת");
                    }

                }
            }

        }

        public void DeleteHealthClaimTracking(HealthClaimTracking tracking)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var trackingInContext = context.HealthClaimTrackings.FirstOrDefault(t => t.HealthClaimTrackingID == tracking.HealthClaimTrackingID);
                if (trackingInContext != null)
                {
                    context.HealthClaimTrackings.Remove(trackingInContext);
                    context.SaveChanges();
                }
            }

        }

        public List<HealthClaimTracking> GetAllHealthClaimTrackingsByClaimID(int claimId)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.HealthClaimTrackings.Include("User").Where(t => t.HealthClaimID == claimId).ToList();
            }
        }

        public List<FinanceTracking> GetAllFinanceTrackingsByPolicy(int policyId)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.FinanceTrackings.Include("User").Where(t => t.FinancePolicyID == policyId).ToList();
            }
        }

        public void InsertFinanceTrackings(int policyId, List<FinanceTracking> trackings, bool isAddition)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var policyInContext = context.FinancePolicies.FirstOrDefault(c => c.FinancePolicyID == policyId);
                if (policyInContext != null)
                {
                    try
                    {
                        var policyTrackings = context.FinanceTrackings.Where(t => t.FinancePolicyID == policyId).ToList();
                        foreach (FinanceTracking tracking in trackings)
                        {
                            if (tracking.FinanceTrackingID == 0)
                            {
                                tracking.User = null;
                                tracking.FinancePolicyID = policyId;
                                context.FinanceTrackings.Add(tracking);
                            }
                            //else if (isAddition)
                            //{
                            //    LifeTracking newTracking = new LifeTracking() { Date = tracking.Date, LifePolicyID = policyId, TrackingContent = tracking.TrackingContent, UserID = tracking.UserID };
                            //    context.LifeTrackings.Add(newTracking);
                            //}
                            else
                            {
                                var trakingInContext = context.FinanceTrackings.FirstOrDefault(t => t.FinanceTrackingID == tracking.FinanceTrackingID);
                                if (trakingInContext != null)
                                {
                                    trakingInContext.TrackingContent = tracking.TrackingContent;
                                }
                                policyTrackings.Remove(trakingInContext);
                            }
                        }
                        if (policyTrackings.Count > 0)
                        {
                            foreach (var item in policyTrackings)
                            {
                                context.FinanceTrackings.Remove(item);
                            }
                        }
                        context.SaveChanges();
                    }
                    catch (Exception e)
                    {
                        throw new Exception(e.Message);
                    }
                }
                else
                {
                    throw new Exception("פוליסה לא קיימת במערכת");
                }
            }
        }

        public void InsertPersonalAccidentsTrackings(int policyId, List<PersonalAccidentsTracking> trackings, bool isAddition)
        {
            if (trackings.Count > 0)
            {
                using (Coral_DB_Entities context = new Coral_DB_Entities())
                {
                    var policyInContext = context.PersonalAccidentsPolicies.FirstOrDefault(c => c.PersonalAccidentsPolicyID == policyId);
                    if (policyInContext != null)
                    {
                        try
                        {
                            var policyTrackings = context.PersonalAccidentsTrackings.Where(t => t.PersonalAccidentsPolicyID == policyId).ToList();
                            foreach (PersonalAccidentsTracking tracking in trackings)
                            {
                                if (tracking.PersonalAccidentsTrackingID == 0)
                                {
                                    tracking.User = null;
                                    tracking.PersonalAccidentsPolicyID = policyId;
                                    context.PersonalAccidentsTrackings.Add(tracking);
                                }
                                else if (isAddition)
                                {
                                    PersonalAccidentsTracking newTracking = new PersonalAccidentsTracking() { Date = tracking.Date, PersonalAccidentsPolicyID = policyId, TrackingContent = tracking.TrackingContent, UserID = tracking.UserID };
                                    context.PersonalAccidentsTrackings.Add(newTracking);
                                }
                                else
                                {
                                    var trakingInContext = context.PersonalAccidentsTrackings.FirstOrDefault(t => t.PersonalAccidentsTrackingID == tracking.PersonalAccidentsTrackingID);
                                    if (trakingInContext != null)
                                    {
                                        trakingInContext.TrackingContent = tracking.TrackingContent;
                                    }
                                    policyTrackings.Remove(trakingInContext);
                                }

                            }
                            if (policyTrackings.Count > 0)
                            {
                                foreach (var item in policyTrackings)
                                {
                                    context.PersonalAccidentsTrackings.Remove(item);
                                }
                            }
                            context.SaveChanges();
                        }
                        catch (Exception e)
                        {
                            throw new Exception(e.Message);
                        }
                    }
                    else
                    {
                        throw new Exception("פוליסה לא קיימת במערכת");
                    }

                }
            }
        }

        public void InsertPersonalAccidentsClaimTrackings(List<PersonalAccidentsClaimTracking> trackings, int claimId, int userID)
        {
            if (trackings.Count > 0)
            {
                using (Coral_DB_Entities context = new Coral_DB_Entities())
                {
                    var claimInContext = context.PersonalAccidentsClaims.FirstOrDefault(c => c.PersonalAccidentsClaimID == claimId);
                    if (claimInContext != null)
                    {
                        try
                        {
                            foreach (var tracking in trackings)
                            {
                                if (tracking.PersonalAccidentsClaimTrackingID == 0)
                                {
                                    //PersonalAccidentsClaimTracking newTracking = new PersonalAccidentsClaimTracking() { PersonalAccidentsClaimID = claimId, UserID = userID, Date = tracking.Date, TrackingContent = tracking.TrackingContent };
                                    tracking.User = null;
                                    tracking.PersonalAccidentsClaimID = claimId;
                                    tracking.UserID = userID;
                                    context.PersonalAccidentsClaimTrackings.Add(tracking);
                                }
                                else
                                {
                                    var trackingInContext = context.PersonalAccidentsClaimTrackings.FirstOrDefault(t => t.PersonalAccidentsClaimTrackingID == tracking.PersonalAccidentsClaimTrackingID);
                                    if (trackingInContext != null)
                                    {
                                        trackingInContext.TrackingContent = tracking.TrackingContent;
                                    }
                                }
                            }
                            context.SaveChanges();
                        }
                        catch (Exception e)
                        {
                            throw new Exception(e.Message);
                        }
                    }
                    else
                    {
                        throw new Exception("תביעה לא קיימת במערכת");
                    }

                }
            }

        }

        public void DeletePersonalAccidentsClaimTracking(PersonalAccidentsClaimTracking tracking)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var trackingInContext = context.PersonalAccidentsClaimTrackings.FirstOrDefault(t => t.PersonalAccidentsClaimTrackingID == tracking.PersonalAccidentsClaimTrackingID);
                if (trackingInContext != null)
                {
                    context.PersonalAccidentsClaimTrackings.Remove(trackingInContext);
                    context.SaveChanges();
                }
            }

        }

        public List<PersonalAccidentsClaimTracking> GetAllPersonalAccidentsClaimTrackingsByClaimID(int claimId)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.PersonalAccidentsClaimTrackings.Include("User").Where(t => t.PersonalAccidentsClaimID == claimId).ToList();
            }
        }

        public void InsertTravelTrackings(int policyId, List<TravelTracking> trackings, bool isAddition)
        {
            if (trackings.Count > 0)
            {
                using (Coral_DB_Entities context = new Coral_DB_Entities())
                {
                    var policyInContext = context.TravelPolicies.FirstOrDefault(c => c.TravelPolicyID == policyId);
                    if (policyInContext != null)
                    {
                        try
                        {
                            var policyTrackings = context.TravelTrackings.Where(t => t.TravelPolicyID == policyId).ToList();
                            foreach (TravelTracking tracking in trackings)
                            {
                                if (tracking.TravelTrackingID == 0)
                                {
                                    tracking.User = null;
                                    tracking.TravelPolicyID = policyId;
                                    context.TravelTrackings.Add(tracking);
                                }
                                else if (isAddition)
                                {
                                    TravelTracking newTracking = new TravelTracking() { Date = tracking.Date, TravelPolicyID = policyId, TrackingContent = tracking.TrackingContent, UserID = tracking.UserID };
                                    context.TravelTrackings.Add(newTracking);
                                }
                                else
                                {
                                    var trakingInContext = context.TravelTrackings.FirstOrDefault(t => t.TravelTrackingID == tracking.TravelTrackingID);
                                    if (trakingInContext != null)
                                    {
                                        trakingInContext.TrackingContent = tracking.TrackingContent;
                                    }
                                    policyTrackings.Remove(trakingInContext);
                                }

                            }
                            if (policyTrackings.Count > 0)
                            {
                                foreach (var item in policyTrackings)
                                {
                                    context.TravelTrackings.Remove(item);
                                }
                            }
                            context.SaveChanges();
                        }
                        catch (Exception e)
                        {
                            throw new Exception(e.Message);
                        }
                    }
                    else
                    {
                        throw new Exception("פוליסה לא קיימת במערכת");
                    }

                }
            }
        }

        public void InsertTravelClaimTrackings(List<TravelClaimTracking> trackings, int claimId, int userID)
        {
            if (trackings.Count > 0)
            {
                using (Coral_DB_Entities context = new Coral_DB_Entities())
                {
                    var claimInContext = context.TravelClaims.FirstOrDefault(c => c.TravelClaimID == claimId);
                    if (claimInContext != null)
                    {
                        try
                        {
                            foreach (var tracking in trackings)
                            {
                                if (tracking.TravelClaimTrackingID == 0)
                                {
                                    //TravelClaimTracking newTracking = new TravelClaimTracking() { TravelClaimID = claimId, UserID = userID, Date = tracking.Date, TrackingContent = tracking.TrackingContent };
                                    tracking.User = null;
                                    tracking.TravelClaimID = claimId;
                                    tracking.UserID = userID;
                                    context.TravelClaimTrackings.Add(tracking);
                                }
                                else
                                {
                                    var trackingInContext = context.TravelClaimTrackings.FirstOrDefault(t => t.TravelClaimTrackingID == tracking.TravelClaimTrackingID);
                                    if (trackingInContext != null)
                                    {
                                        trackingInContext.TrackingContent = tracking.TrackingContent;
                                    }
                                }
                            }
                            context.SaveChanges();
                        }
                        catch (Exception e)
                        {
                            throw new Exception(e.Message);
                        }
                    }
                    else
                    {
                        throw new Exception("תביעה לא קיימת במערכת");
                    }

                }
            }

        }

        public void DeleteTravelClaimTracking(TravelClaimTracking tracking)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var trackingInContext = context.TravelClaimTrackings.FirstOrDefault(t => t.TravelClaimTrackingID == tracking.TravelClaimTrackingID);
                if (trackingInContext != null)
                {
                    context.TravelClaimTrackings.Remove(trackingInContext);
                    context.SaveChanges();
                }
            }

        }

        public List<TravelClaimTracking> GetAllTravelClaimTrackingsByClaimID(int claimId)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.TravelClaimTrackings.Include("User").Where(t => t.TravelClaimID == claimId).ToList();
            }
        }

        public List<ForeingWorkersTracking> GetAllForeingWorkersTrackingsByPolicy(int policyId)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.ForeingWorkersTrackings.Include("User").Where(t => t.ForeingWorkersPolicyID == policyId).ToList();
            }
        }
        public void InsertForeingWorkersTrackings(int policyId, List<ForeingWorkersTracking> trackings, bool isAddition)
        {
            if (trackings.Count > 0)
            {
                using (Coral_DB_Entities context = new Coral_DB_Entities())
                {
                    var policyInContext = context.ForeingWorkersPolicies.FirstOrDefault(c => c.ForeingWorkersPolicyID == policyId);
                    if (policyInContext != null)
                    {
                        try
                        {
                            var policyTrackings = context.ForeingWorkersTrackings.Where(t => t.ForeingWorkersPolicyID == policyId).ToList();
                            foreach (ForeingWorkersTracking tracking in trackings)
                            {
                                if (tracking.ForeingWorkersTrackingID == 0)
                                {
                                    tracking.User = null;
                                    tracking.ForeingWorkersPolicyID = policyId;
                                    context.ForeingWorkersTrackings.Add(tracking);
                                }
                                else if (isAddition)
                                {
                                    ForeingWorkersTracking newTracking = new ForeingWorkersTracking() { Date = tracking.Date, ForeingWorkersPolicyID = policyId, TrackingContent = tracking.TrackingContent, UserID = tracking.UserID };
                                    context.ForeingWorkersTrackings.Add(newTracking);
                                }
                                else
                                {
                                    var trakingInContext = context.ForeingWorkersTrackings.FirstOrDefault(t => t.ForeingWorkersTrackingID == tracking.ForeingWorkersTrackingID);
                                    if (trakingInContext != null)
                                    {
                                        trakingInContext.TrackingContent = tracking.TrackingContent;
                                    }
                                    policyTrackings.Remove(trakingInContext);
                                }

                            }
                            if (policyTrackings.Count > 0)
                            {
                                foreach (var item in policyTrackings)
                                {
                                    context.ForeingWorkersTrackings.Remove(item);
                                }
                            }
                            context.SaveChanges();
                        }
                        catch (Exception e)
                        {
                            throw new Exception(e.Message);
                        }
                    }
                    else
                    {
                        throw new Exception("פוליסה לא קיימת במערכת");
                    }

                }
            }
        }

    }
}

