//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CoralBusinessLogics
{
    using System;
    using System.Collections.Generic;
    
    public partial class Client
    {
        public Client()
        {
            this.ClientRelatives = new HashSet<ClientRelative>();
            this.ElementaryPolicies = new HashSet<ElementaryPolicy>();
            this.FinancePolicies = new HashSet<FinancePolicy>();
            this.ForeingWorkersPolicies = new HashSet<ForeingWorkersPolicy>();
            this.HealthPolicies = new HashSet<HealthPolicy>();
            this.LifePolicies = new HashSet<LifePolicy>();
            this.MessagesAndReminders = new HashSet<MessagesAndReminder>();
            this.PersonalAccidentsPolicies = new HashSet<PersonalAccidentsPolicy>();
            this.Requests = new HashSet<Request>();
            this.TravelPolicies = new HashSet<TravelPolicy>();
        }
    
        public int ClientID { get; set; }
        public int ClientTypeID { get; set; }
        public int CategoryID { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string IdNumber { get; set; }
        public string PhoneHome { get; set; }
        public string PhoneWork { get; set; }
        public string CellPhone { get; set; }
        public string Fax { get; set; }
        public string City { get; set; }
        public string Street { get; set; }
        public string HomeNumber { get; set; }
        public string ApartmentNumber { get; set; }
        public string ZipCode { get; set; }
        public string Email { get; set; }
        public string Email2 { get; set; }
        public string Website { get; set; }
        public Nullable<System.DateTime> BirthDate { get; set; }
        public string BusinessName { get; set; }
        public string ClientNumber { get; set; }
        public string Remarks { get; set; }
        public Nullable<System.DateTime> OpenDate { get; set; }
        public Nullable<int> PrincipalAgentID { get; set; }
        public Nullable<int> SecundaryAgentID { get; set; }
        public string MaritalStatus { get; set; }
        public Nullable<bool> Smokes { get; set; }
        public string Profession { get; set; }
        public string WorkerType { get; set; }
        public string CompanyAddress { get; set; }
        public string KupatJolim { get; set; }
        public string PassportNumber { get; set; }
        public Nullable<bool> DrivesInShabbat { get; set; }
        public string DrivingLicenseNumber { get; set; }
        public Nullable<System.DateTime> DrivingLicenseDate { get; set; }
        public string MailingAddress { get; set; }
        public string Ocupation { get; set; }
        public Nullable<System.DateTime> JobStartDate { get; set; }
        public Nullable<int> DangerousHobbyID { get; set; }
        public Nullable<int> ReferedByClientID { get; set; }
        public string ReferedByRelation { get; set; }
        public string PicturePath { get; set; }
        public Nullable<bool> isNew { get; set; }
    
        public virtual Agent Agent { get; set; }
        public virtual Agent Agent1 { get; set; }
        public virtual Category Category { get; set; }
        public virtual ICollection<ClientRelative> ClientRelatives { get; set; }
        public virtual ClientType ClientType { get; set; }
        public virtual DangerousHobby DangerousHobby { get; set; }
        public virtual ICollection<ElementaryPolicy> ElementaryPolicies { get; set; }
        public virtual ICollection<FinancePolicy> FinancePolicies { get; set; }
        public virtual ICollection<ForeingWorkersPolicy> ForeingWorkersPolicies { get; set; }
        public virtual ICollection<HealthPolicy> HealthPolicies { get; set; }
        public virtual ICollection<LifePolicy> LifePolicies { get; set; }
        public virtual ICollection<MessagesAndReminder> MessagesAndReminders { get; set; }
        public virtual ICollection<PersonalAccidentsPolicy> PersonalAccidentsPolicies { get; set; }
        public virtual ICollection<Request> Requests { get; set; }
        public virtual ICollection<TravelPolicy> TravelPolicies { get; set; }
    }
}
