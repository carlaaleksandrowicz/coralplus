﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoralBusinessLogics
{
    public class PersonalAccidentsInsuranceTypesLogic
    {
        //מחזיר רשימה של כל סוגי ביטוחי תאונות אישיות לפי חברת ביטוח
        public List<PersonalAccidentsInsuranceType> GetPersonalAccidentsInsuranceTypesByCompany(int companyId)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.PersonalAccidentsInsuranceTypes.Include("Company").Where(i => i.CompanyID == companyId).OrderByDescending(i => i.Status).ThenBy(i => i.PersonalAccidentsInsuranceTypeName).ToList();
            }
        }

        //מחזיר רשימה של סוגי ביטוחי תאונות אישיות פעילים
        public List<PersonalAccidentsInsuranceType> GetActivePersonalAccidentsInsuranceTypesByCompany(int companyId)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.PersonalAccidentsInsuranceTypes.Where(i => i.CompanyID == companyId&&i.Status==true).OrderBy(i => i.PersonalAccidentsInsuranceTypeName).ToList();
            }
        }


        //מוסיף סוג ביטוח חדש
        public PersonalAccidentsInsuranceType InsertPersonalAccidentsInsuranceType(string insuranceTypeName, bool status, int companyId, decimal? deathAmount, decimal? discapacityAmount, decimal? fracturesAndBurnsAmount, decimal? hospitalizationAmount, decimal? siudAmount)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var insuranceTypeInContext = context.PersonalAccidentsInsuranceTypes.FirstOrDefault(i => i.PersonalAccidentsInsuranceTypeName == insuranceTypeName && i.CompanyID == companyId);
                if (insuranceTypeInContext == null)
                {
                    try
                    {
                        PersonalAccidentsInsuranceType newType = new PersonalAccidentsInsuranceType() { PersonalAccidentsInsuranceTypeName = insuranceTypeName, Status = status, CompanyID = companyId, CompensationForHospitalizationAmount=hospitalizationAmount, DeathByAccidentAmount=deathAmount, FracturesAndBurnsByAccidentAmount=fracturesAndBurnsAmount, IncapacityByAccidentAmount=discapacityAmount, SiudByAccidentAmount=siudAmount };
                        context.PersonalAccidentsInsuranceTypes.Add(newType);
                        context.SaveChanges();
                        return newType;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }
                else
                {
                    throw new Exception("סוג ביטוח קיים");
                }
            }
        }

        //מעדכן את הסטטוס של סוג ביטוח קיים
        public PersonalAccidentsInsuranceType UpdatePersonalAccidentsInsuranceTypeStatus(PersonalAccidentsInsuranceType insuranceType, bool status, int companyId, decimal? deathAmount, decimal? discapacityAmount, decimal? fracturesAndBurnsAmount, decimal? hospitalizationAmount, decimal? siudAmount)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var insuranceTypeInContext = context.PersonalAccidentsInsuranceTypes.FirstOrDefault(i => i.PersonalAccidentsInsuranceTypeName == insuranceType.PersonalAccidentsInsuranceTypeName && i.CompanyID == companyId);
                if (insuranceTypeInContext != null)
                {
                    try
                    {
                        insuranceTypeInContext.Status = status;
                        insuranceTypeInContext.CompensationForHospitalizationAmount = hospitalizationAmount;
                        insuranceTypeInContext.DeathByAccidentAmount = deathAmount;
                        insuranceTypeInContext.FracturesAndBurnsByAccidentAmount = fracturesAndBurnsAmount;
                        insuranceTypeInContext.IncapacityByAccidentAmount = discapacityAmount;
                        insuranceTypeInContext.SiudByAccidentAmount = siudAmount;
                        context.SaveChanges();
                        return insuranceTypeInContext;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }
                else
                {
                    throw new Exception("סוג ביטוח לא קיים");
                }

            }
        }

        public void DeleteInsuranceType(PersonalAccidentsInsuranceType insuranceType)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var insuranceTypeInContext = context.PersonalAccidentsInsuranceTypes.FirstOrDefault(i => i.PersonalAccidentsInsuranceTypeID == insuranceType.PersonalAccidentsInsuranceTypeID);
                if (insuranceTypeInContext != null)
                {
                    try
                    {
                        context.PersonalAccidentsInsuranceTypes.Remove(insuranceTypeInContext);
                        context.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }
                else
                {
                    throw new Exception("סוג ביטוח לא קיים");
                }

            }
        }
    }
}

