﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoralBusinessLogics
{
    public class StandardMoneyCollectionReport
    {
        public string ClientName { get; set; }
        public string Cellphone { get; set; }

        public string Phone { get; set; }

        public string PolicyNumber { get; set; }

        public string CompanyName { get; set; }

        public string StartDate { get; set; }

        public string EndDate { get; set; }
        public string IndustryName { get; set; }

        public string Premium { get; set; }

        public string MoneyCollectionType { get; set; }

        public string MoneyCollectionAmount { get; set; }

        public string Header { get; set; }
    }
}
