﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoralBusinessLogics
{
    public class ClaimsReport
    {
        public string ClientName { get; set; }
        public string IdNumber{ get; set; }
        public string EventDate{ get; set; }
        public string CompanyName{ get; set; }
        public string Industry{ get; set; }
        public string PolicyNumber{ get; set; }
        public string RegistrationNumber{ get; set; }
        public string ClaimNumber{ get; set; }
        public string ThirdPartyClaimNumber{ get; set; }
        public string ThirdPartyCompanyName{ get; set; }
        public string Header { get; set; }

    }
}
