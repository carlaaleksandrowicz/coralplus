﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoralBusinessLogics
{
    public class PoliciesReport
    {
        public string Insurance { get; set; }
        public string ClientName{ get; set; }
        public string IdNumber{ get; set; }
        public string PolicyNumber{ get; set; }
        public string CompanyName{ get; set; }
        public string StartDate{ get; set; }
        public string EndDate{ get; set; }
        public string Industry { get; set; }
        public string RegistrationNumber{ get; set; }
        public string Premium{ get; set; }
        public string Header { get; set; }
    }
}
