﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoralBusinessLogics
{
    public class ForeingWorkersLogic
    {
        //מחזיר רשימה של כל הפוליסות עובדים זרים
        public List<ForeingWorkersPolicy> GetAllForeingWorkersPoliciesWithoutOffers()
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.ForeingWorkersPolicies.Include("Client").Include("Client.Agent").Include("Client.Agent1").Include("Insurance").Include("Company").Include("ForeingWorkersIndustry").Include("ForeingWorkersInsuranceType").Where(p => p.IsOffer == false).OrderByDescending(p => p.StartDate).ThenBy(p => p.Client.LastName).ToList();
            }
        }

        //מחזיר רשימה של פוליסות עובדים זרים של לקוח מסוים
        public List<ForeingWorkersPolicy> GetAllForeingWorkersPoliciesByClient(Client client)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                if (client != null)
                {
                    var policies = context.ForeingWorkersPolicies.Where(p => p.ClientID == client.ClientID && p.IsOffer == false).OrderByDescending(p => p.OpenDate).ToList();
                    var sortedPolicies = context.ForeingWorkersPolicies.Include("Insurance").Include("Client").Include("RenewalStatus").Include("Country").Include("Company").Include("ForeingWorkersIndustry").Include("ForeingWorkersInsuranceType").Where(p => p.ClientID == client.ClientID && p.IsOffer == false).OrderByDescending(p => p.OpenDate).ToList();
                    for (int i = policies.Count - 1; i >= 0; i--)
                    {
                        if (policies[i].Addition > 0)
                        {
                            var policy = policies.Where(p => p.PolicyNumber == policies[i].PolicyNumber && p.Addition == policies[i].Addition - 1).FirstOrDefault();
                            if (policy != null)
                            {
                                sortedPolicies.Remove(policies[i]);
                                var index = sortedPolicies.FindIndex(p => p.PolicyNumber == policy.PolicyNumber && p.Addition == policy.Addition);
                                sortedPolicies.Insert(index, policies[i]);
                            }
                        }
                    }
                    return sortedPolicies;
                }
                else
                {
                    return null;
                }
            }
        }

        public List<ForeingWorkersPolicy> GetPoliciesWithoutOffersByStartDate(DateTime fromDate, DateTime? toDate)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.ForeingWorkersPolicies.Include("Client").Include("Client.Agent").Include("Client.Agent1").Include("Insurance").Include("Company").Include("ForeingWorkersIndustry").Include("ForeingWorkersInsuranceType").Where(p => p.IsOffer == false && p.StartDate >= fromDate && p.StartDate <= toDate).ToList();
            }
        }
        public List<ForeingWorkersPolicy> GetPoliciesByEndDate(DateTime endDate)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.ForeingWorkersPolicies.Include("Country").Include("Client").Include("RenewalStatus").Include("Company").Include("ForeingWorkersInsuranceType").Include("ForeingWorkersIndustry").Where(p => ((DateTime)p.EndDate).Month == endDate.Month && ((DateTime)p.EndDate).Year == endDate.Year && p.IsOffer == false).ToList();
            }
        }


        //מוסיף פוליסת עובדים זרים חדשה
        public int InsertForeingWorkersPolicy(int clientId, bool isOffer, int industryId, int insuranceTypeId, string policyNumber, int addition, DateTime openDate, DateTime? startDate, DateTime? endDate, int? companyId, int? principalAgentNumberId, int? subAgentNumberId, string paymentMethod, string currency, int? paymentsNumber, DateTime? paymentDate, decimal totalPremium, string contactLastName, string contactFirstName,string contactPhone, string contactCellphone, string contactFax, string contactEmail,int? insuredBirthCountryId, string insuredFirstName, string insuredMiddleName, string insuredLastName, DateTime? insuredBirthDate, string insuredPassportNumber, bool? isMale, string insuredCellphone, string insuredHomePhone, string homeAddress, string workAddress, DateTime? firstEntryToIsrael, DateTime? lastEntryToIsrael, bool? isPreviousInsurancesInIsrael, DateTime? previousInsuranceFrom, DateTime? previousInsuranceTo, int? previousInsuranceCompanyId, string previousInsurancePolicyNumber, string clinicName,string clinicNumber, string clinicAddress , string comments)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var policyInContext = context.ForeingWorkersPolicies.FirstOrDefault(p => p.PolicyNumber == policyNumber && p.ForeingWorkersIndustryID == industryId && p.Addition == addition);
                if (policyInContext == null)
                {
                    try
                    {
                        RenewalStatus renewalStatus = context.RenewalStatuses.FirstOrDefault(r => r.RenewalStatusName == "לא טופל");
                        var insuranceInContext = context.Insurances.FirstOrDefault(i => i.InsuranceName == "עובדים זרים");
                        ForeingWorkersPolicy newForeingWorkersPolicy = new ForeingWorkersPolicy()
                        {
                            InsuranceID = insuranceInContext.InsuranceID,
                            ClientID = clientId,
                            IsOffer = isOffer,
                            ForeingWorkersInsuranceTypeID = insuranceTypeId,
                            PolicyNumber = policyNumber,
                            Addition = addition,
                            OpenDate = openDate,
                            StartDate = startDate,
                            EndDate = endDate,
                            CompanyID = companyId,
                            PrincipalAgentNumberID = principalAgentNumberId,
                            SubAgentNumberID = subAgentNumberId,
                            ForeingWorkersIndustryID = industryId,                            
                            PaymentMethod = paymentMethod,
                            Comments = comments,
                            ClinicAddress=clinicAddress,
                            ClinicName=clinicName,
                            ClinicNumber=clinicNumber,
                            ContactCellPhone=contactCellphone,
                            ContactEmail=contactEmail, 
                            ContactFax=contactFax,
                            ContactLastName=contactLastName,
                            ContactName=contactFirstName,
                            ContactPhone=contactPhone,
                            Currency=currency,
                            FirstEntryToIsrael=firstEntryToIsrael,
                            InsuredBirthCountryID=insuredBirthCountryId,
                            InsuredBirthDate=insuredBirthDate,
                            InsuredCellphone=insuredCellphone,
                            InsuredFirstName=insuredFirstName,
                            InsuredHomeAddress=homeAddress,
                            InsuredHomePhone=insuredHomePhone,
                            InsuredLastName=insuredLastName,
                            InsuredMiddleName=insuredMiddleName,
                            InsuredPassportNumber=insuredPassportNumber,
                            InsuredWorkAddress=workAddress,
                            IsMale=isMale,
                            IsPreviousInsurencesInIsrael=isPreviousInsurancesInIsrael,
                            LastEntryToIsrael=lastEntryToIsrael,
                            PaymentDate=paymentDate,
                            PaymentsNumber=paymentsNumber,
                            PreviousInsuranceCompanyID=previousInsuranceCompanyId,
                            PreviousInsurancePolicyNumber=previousInsurancePolicyNumber,
                            PreviousInsuranceTo=previousInsuranceTo,
                            PreviousIsuranceFrom=previousInsuranceFrom,
                            TotalPremium=totalPremium,
                            RenewalStatusID= renewalStatus.RenewalStatusID
                        };
                        context.ForeingWorkersPolicies.Add(newForeingWorkersPolicy);
                        context.SaveChanges();
                        return newForeingWorkersPolicy.ForeingWorkersPolicyID;
                    }
                    catch (Exception e)
                    {

                        throw new Exception(e.Message);
                    }
                }
                else
                {
                    throw new Exception("פוליסה קיימת במערכת");
                }
            }
        }

        public void TransferPolicyToClient(ForeingWorkersPolicy policy, Client client)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var policyInContext = context.ForeingWorkersPolicies.FirstOrDefault(p => p.ForeingWorkersPolicyID == policy.ForeingWorkersPolicyID);
                if (policyInContext != null)
                {
                    policyInContext.ClientID = client.ClientID;
                    context.SaveChanges();
                }
                else
                {
                    throw new Exception("פוליסה לא קיימת במערכת");
                }
            }
        }

        public List<ForeingWorkersPolicy> GetPoliciesByPolicyNumberAndIndustry(string policyNumber, int industryID)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.ForeingWorkersPolicies.Include("Client").Include("Company").Include("ForeingWorkersInsuranceType").Include("ForeingWorkersIndustry").Include("RenewalStatus").Where(p => p.PolicyNumber == policyNumber && p.ForeingWorkersIndustryID == industryID).ToList();
            }
        }


        public bool IsLastPolicyAddition(ForeingWorkersPolicy policy, List<ForeingWorkersPolicy> allRenewals)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                List<ForeingWorkersPolicy> policiesInContext = allRenewals.Where(p => p.PolicyNumber == policy.PolicyNumber && p.ForeingWorkersIndustryID == policy.ForeingWorkersIndustryID).ToList();
                if (policiesInContext.Count() == 1)
                {
                    return true;
                }
                int? addition = 0;
                foreach (var item in policiesInContext)
                {
                    if (item.Addition > addition)
                    {
                        addition = item.Addition;
                    }
                }
                if (policy.Addition == addition)
                {
                    return true;
                }
                return false;
            }
        }
        public List<ForeingWorkersPolicy> GetOffers()
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.ForeingWorkersPolicies.Include("Client").Include("Company").Include("ForeingWorkersIndustry").Include("ForeingWorkersInsuranceType").Where(p => p.IsOffer == true).OrderByDescending(p => p.StartDate).ThenBy(p => p.Client.LastName).ToList();
            }
        }

        public List<ForeingWorkersPolicy> GetAllForeingWorkersOffersByClient(Client client)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                if (client == null)
                {
                    return null;
                }
                return context.ForeingWorkersPolicies.Include("Client").Include("Insurance").Include("Company").Include("ForeingWorkersIndustry").Include("ForeingWorkersInsuranceType").Where(p => p.ClientID == client.ClientID && p.IsOffer == true).OrderByDescending(p => p.StartDate).ToList();
            }
        }

        public List<ForeingWorkersPolicy> GetActivePoliciesWithoutOffers()
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.ForeingWorkersPolicies.Include("Client").Include("Client.Agent").Include("Client.Agent1").Include("Insurance").Include("Company").Include("ForeingWorkersIndustry").Include("ForeingWorkersInsuranceType").Where(p => p.IsOffer == false && p.EndDate >= DateTime.Today).OrderByDescending(p => p.StartDate).ThenBy(p => p.Client.LastName).ToList();
            }

        }



        public string GetAdditionNumber(string policyNumber)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var policiesInContext = context.ForeingWorkersPolicies.Where(p => p.PolicyNumber == policyNumber);
                int? addition = 0;
                foreach (var item in policiesInContext)
                {
                    if (item.Addition > addition)
                    {
                        addition = item.Addition;
                    }
                }
                return (addition + 1).ToString();
            }
        }

        //מעדכן פוליסת עובדים זרים קיימת
        public void UpdateForeingWorkersPolicy(int foreingWorkersPolicyId, string policyNumber, DateTime? startDate, DateTime? endDate, int? companyId, int? principalAgentNumberId, int? subAgentNumberId, int insuranceTypeId, string paymentMethod, string currency, int? paymentsNumber, DateTime? paymentDate, decimal totalPremium, string contactLastName, string contactFirstName, string contactPhone, string contactCellphone, string contactFax, string contactEmail, int? insuredBirthCountryId, string insuredFirstName, string insuredMiddleName, string insuredLastName, DateTime? insuredBirthDate, string insuredPassportNumber, bool? isMale, string insuredCellphone, string insuredHomePhone, string homeAddress, string workAddress, DateTime? firstEntryToIsrael, DateTime? lastEntryToIsrael, bool? isPreviousInsurancesInIsrael, DateTime? previousInsuranceFrom, DateTime? previousInsuranceTo, int? previousInsuranceCompanyId, string previousInsurancePolicyNumber, string clinicName, string clinicNumber, string clinicAddress, string comments)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var policyInContext = context.ForeingWorkersPolicies.FirstOrDefault(p => p.ForeingWorkersPolicyID == foreingWorkersPolicyId);
                if (policyInContext != null)
                {
                    try
                    {
                        policyInContext.PolicyNumber = policyNumber;
                        policyInContext.CompanyID = companyId;
                        policyInContext.PrincipalAgentNumberID = principalAgentNumberId;
                        policyInContext.SubAgentNumberID = subAgentNumberId;
                        policyInContext.ForeingWorkersInsuranceTypeID = insuranceTypeId;                       
                        policyInContext.PaymentMethod = paymentMethod;
                        policyInContext.Comments = comments;                        
                        policyInContext.StartDate = startDate;
                        policyInContext.EndDate = endDate;
                        policyInContext.ClinicAddress = clinicAddress;
                        policyInContext.ClinicName = clinicName;
                        policyInContext.ClinicNumber = clinicNumber;
                        policyInContext.ContactCellPhone = contactCellphone;
                        policyInContext.ContactEmail = contactEmail;
                        policyInContext.ContactFax = contactFax;
                        policyInContext.ContactLastName = contactLastName;
                        policyInContext.ContactName = contactFirstName;
                        policyInContext.ContactPhone = contactPhone;
                        policyInContext.Currency = currency;
                        policyInContext.FirstEntryToIsrael = firstEntryToIsrael;
                        policyInContext.InsuredBirthCountryID = insuredBirthCountryId;
                        policyInContext.InsuredBirthDate = insuredBirthDate;
                        policyInContext.InsuredCellphone = insuredCellphone;
                        policyInContext.InsuredFirstName = insuredFirstName;
                        policyInContext.InsuredHomeAddress = homeAddress;
                        policyInContext.InsuredHomePhone = insuredHomePhone;
                        policyInContext.InsuredLastName = insuredLastName;
                        policyInContext.InsuredMiddleName = insuredMiddleName;
                        policyInContext.InsuredPassportNumber = insuredPassportNumber;
                        policyInContext.InsuredWorkAddress = workAddress;
                        policyInContext.IsMale = isMale;
                        policyInContext.IsPreviousInsurencesInIsrael = isPreviousInsurancesInIsrael;
                        policyInContext.LastEntryToIsrael = lastEntryToIsrael;
                        policyInContext.PaymentDate = paymentDate;
                        policyInContext.PaymentsNumber = paymentsNumber;
                        policyInContext.PreviousInsuranceCompanyID = previousInsuranceCompanyId;
                        policyInContext.PreviousInsurancePolicyNumber = previousInsurancePolicyNumber;
                        policyInContext.PreviousInsuranceTo = previousInsuranceTo;
                        policyInContext.PreviousIsuranceFrom = previousInsuranceFrom;
                        policyInContext.TotalPremium = totalPremium;
                        
                        context.SaveChanges();
                    }
                    catch (Exception e)
                    {

                        throw new Exception(e.Message);
                    }
                }
                else
                {
                    throw new Exception("פוליסה לא קיימת במערכת");
                }
            }
        }


        public List<ForeingWorkersPolicy> GetActiveForeingWorkersPoliciesWithoutAdditionByClient(Client client)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.ForeingWorkersPolicies.Include("Insurance").Where(p => p.ClientID == client.ClientID && p.Addition == 0 && p.IsOffer == false).OrderByDescending(p => p.StartDate).ToList();
            }
        }     


        public List<ForeingWorkersPolicy> GetAllActiveForeingWorkersPoliciesByClient(Client client)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                if (client != null)
                {
                    var policies = context.ForeingWorkersPolicies.Where(p => p.ClientID == client.ClientID && p.IsOffer == false && p.EndDate >= DateTime.Today).OrderByDescending(p => p.OpenDate).ToList();
                    var sortedPolicies = context.ForeingWorkersPolicies.Include("Insurance").Include("Client").Include("Country").Include("RenewalStatus").Include("Company").Include("ForeingWorkersIndustry").Include("ForeingWorkersInsuranceType").Where(p => p.ClientID == client.ClientID && p.IsOffer == false && p.EndDate >= DateTime.Today).OrderByDescending(p => p.OpenDate).ToList();
                    for (int i = policies.Count - 1; i >= 0; i--)
                    {
                        if (policies[i].Addition > 0)
                        {
                            var policy = policies.Where(p => p.PolicyNumber == policies[i].PolicyNumber && p.Addition == policies[i].Addition - 1).FirstOrDefault();
                            if (policy != null)
                            {
                                sortedPolicies.Remove(policies[i]);
                                var index = sortedPolicies.FindIndex(p => p.PolicyNumber == policy.PolicyNumber && p.Addition == policy.Addition);
                                sortedPolicies.Insert(index, policies[i]);
                            }
                        }
                    }
                    return sortedPolicies;
                }
                else
                {
                    return null;
                }
            }
        }

        public IEnumerable GetNonActiveForeingWorkersPoliciesByClient(Client client)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                if (client != null)
                {
                    var policies = context.ForeingWorkersPolicies.Where(p => p.ClientID == client.ClientID && p.IsOffer == false && p.EndDate < DateTime.Today).OrderByDescending(p => p.OpenDate).ToList();
                    var sortedPolicies = context.ForeingWorkersPolicies.Include("Insurance").Include("Client").Include("Country").Include("RenewalStatus").Include("Company").Include("ForeingWorkersIndustry").Include("ForeingWorkersInsuranceType").Where(p => p.ClientID == client.ClientID && p.IsOffer == false && p.EndDate < DateTime.Today).OrderByDescending(p => p.OpenDate).ToList();
                    for (int i = policies.Count - 1; i >= 0; i--)
                    {
                        if (policies[i].Addition > 0)
                        {
                            var policy = policies.Where(p => p.PolicyNumber == policies[i].PolicyNumber && p.Addition == policies[i].Addition - 1).FirstOrDefault();
                            if (policy != null)
                            {
                                sortedPolicies.Remove(policies[i]);
                                var index = sortedPolicies.FindIndex(p => p.PolicyNumber == policy.PolicyNumber && p.Addition == policy.Addition);
                                sortedPolicies.Insert(index, policies[i]);
                            }
                        }
                    }
                    return sortedPolicies;
                }
                else
                {
                    return null;
                }
            }
        }

        public void ChangeForeingWorkersPolicyOfferStatus(int foreingWorkersPolicyID, bool isOffer)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var policyInContext = context.ForeingWorkersPolicies.FirstOrDefault(p => p.ForeingWorkersPolicyID == foreingWorkersPolicyID);
                policyInContext.IsOffer = isOffer;
                context.SaveChanges();
            }
        }

        public int UpdatePolicyRenewalStatus(ForeingWorkersPolicy policy, string newStatus)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                //var policyInContext = context.ForeingWorkersPolicies.Where(p => p.PolicyNumber == policy.PolicyNumber && p.ForeingWorkersIndustryID == policy.ForeingWorkersIndustryID).FirstOrDefault();
                var policyInContext = context.ForeingWorkersPolicies.FirstOrDefault(p => p.ForeingWorkersPolicyID == policy.ForeingWorkersPolicyID);

                if (policyInContext != null)
                {
                    try
                    {
                        var renewalStatusInContext = context.RenewalStatuses.FirstOrDefault(s => s.RenewalStatusName == newStatus);
                        if (renewalStatusInContext != null)
                        {
                            policyInContext.RenewalStatusID = renewalStatusInContext.RenewalStatusID;
                            context.SaveChanges();
                            return renewalStatusInContext.RenewalStatusID;
                        }
                        else
                        {
                            throw new Exception("לא ניתן לעדכן את סטטוס החידוש");
                        }
                    }

                    catch (Exception e)
                    {
                        throw new Exception(e.Message);
                    }
                }
                else
                {
                    throw new Exception("פוליסה לא קיימת במערכת");
                }
            }
        }

        public ForeingWorkersPolicy GetForeingWorkersPolicyByPolicyId(int policyId)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.ForeingWorkersPolicies.Include("ForeingWorkersIndustry").Include("Client").FirstOrDefault(p => p.ForeingWorkersPolicyID == policyId);
            }
        }
    }
}
