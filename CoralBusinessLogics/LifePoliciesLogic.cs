﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoralBusinessLogics
{
    public class LifePoliciesLogic
    {
        //מחזיר רשימה של כל הפוליסות חיים
        public List<LifePolicy> GetAllLifePoliciesWithoutOffers()
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.LifePolicies.Include("Client").Include("Client.Agent").Include("Client.Agent1").Include("Insurance").Include("Company").Include("FundType").Include("LifeIndustry").Include("ManagerPolicy").Include("MortgageLifeIndustry").Include("PrivateLifeIndustry").Include("PensionLifeIndustry").Include("PensionLifeIndustry").Where(p => p.IsOffer == false).OrderByDescending(p => p.StartDate).ThenBy(p => p.Client.LastName).ToList();
            }
        }

        public List<LifePolicy> GetActivePoliciesWithoutOffers()
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.LifePolicies.Include("Client").Include("Client.Agent").Include("Client.Agent1").Include("Insurance").Include("Company").Include("FundType").Include("LifeIndustry").Include("ManagerPolicy").Include("MortgageLifeIndustry").Include("PrivateLifeIndustry").Include("PensionLifeIndustry").Include("PensionLifeIndustry").Where(p => p.IsOffer == false && p.EndDate >= DateTime.Today).OrderByDescending(p => p.StartDate).ThenBy(p => p.Client.LastName).ToList();
            }
        }

        //מחזיר רשימה של פוליסות חיים של לקוח מסוים
        public List<LifePolicy> GetAllLifePoliciesByClient(Client client)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                if (client != null)
                {
                    var policies = context.LifePolicies.Where(p => p.ClientID == client.ClientID && p.IsOffer == false).OrderByDescending(p => p.StartDate).ThenByDescending(p => p.OpenDate).ToList();
                    var sortedPolicies = context.LifePolicies.Include("Insurance").Include("Client").Include("Company").Include("FundType").Include("LifeIndustry").Include("ManagerPolicy").Include("MortgageLifeIndustry").Include("PrivateLifeIndustry").Include("PensionLifeIndustry").Where(p => p.ClientID == client.ClientID && p.IsOffer == false).OrderByDescending(p => p.StartDate).ThenByDescending(p => p.OpenDate).ToList();
                    for (int i = policies.Count - 1; i >= 0; i--)
                    {
                        if (policies[i].Addition > 0)
                        {
                            var policy = policies.Where(p => p.PolicyNumber == policies[i].PolicyNumber && p.Addition == policies[i].Addition - 1).FirstOrDefault();
                            if (policy != null)
                            {
                                sortedPolicies.Remove(policies[i]);
                                var index = sortedPolicies.FindIndex(p => p.PolicyNumber == policy.PolicyNumber && p.Addition == policy.Addition);
                                sortedPolicies.Insert(index, policies[i]);
                            }
                        }
                    }
                    return sortedPolicies;
                }
                else
                {
                    return null;
                }
            }
        }

        public List<LifePolicy> GetPoliciesByOwner(PolicyOwner owner)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.LifePolicies.Include("Client").Include("Client.Agent").Include("Client.Agent1").Include("Insurance").Include("Company").Include("FundType").Include("LifeIndustry").Include("ManagerPolicy").Include("MortgageLifeIndustry").Include("PrivateLifeIndustry").Include("PensionLifeIndustry").Include("PensionLifeIndustry").Where(p => p.IsOffer == false && p.PolicyOwnerID == owner.PolicyOwnerID).OrderByDescending(p => p.StartDate).ThenBy(p => p.Client.LastName).ToList();
            }
        }

        public List<LifePolicy> GetPoliciesWithoutOffersByStartDate(DateTime fromDate, DateTime? toDate)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.LifePolicies.Include("Client").Include("Client.Agent").Include("Client.Agent1").Include("Insurance").Include("Company").Include("FundType").Include("LifeIndustry").Include("ManagerPolicy").Include("MortgageLifeIndustry").Include("PrivateLifeIndustry").Include("PensionLifeIndustry").Include("PensionLifeIndustry").Where(p => p.IsOffer == false && p.StartDate >= fromDate && p.StartDate <= toDate).OrderByDescending(p => p.StartDate).ThenBy(p => p.Client.LastName).ToList();
            }
        }
        public List<LifePolicy> GetOffers()
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.LifePolicies.Include("Client").Include("Company").Include("FundType").Include("LifeIndustry").Include("ManagerPolicy").Include("MortgageLifeIndustry").Include("PrivateLifeIndustry").Include("PensionLifeIndustry").Where(p => p.IsOffer == true).OrderByDescending(p => p.StartDate).ThenBy(p => p.Client.LastName).ToList();
            }
        }

        //מוסיף פוליסת חיים חדשה
        public int InsertLifePolicy(int clientId, bool isOffer, int industryId, string policyNumber, int addition, DateTime openDate, int? companyId, int? principalAgentNumberId, int? subAgentNumberId, int fundType, DateTime? startDate, DateTime? endDate, string primaryIndex, string insuredName, string insuredIdNumber, int? policyOwnerId, int? factoryNumberId, string paymentMethod, bool? isIndexation, decimal? subAnnual, decimal? policyFactor, decimal? totalMonthlyPayment, string bankPayment, string bankBranchPayment, string accountNumberPayment, bool? isMortgaged, string bankName, string bankBranch, string bankAddress, string bankPhone, string bankFax, string bankContactName, string bankEmail, string comments)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var policyInContext = context.LifePolicies.FirstOrDefault(p => p.PolicyNumber == policyNumber && p.LifeIndustryID == industryId && p.Addition == addition);
                if (policyInContext == null)
                {
                    try
                    {
                        var insuranceInContext = context.Insurances.FirstOrDefault(i => i.InsuranceName == "חיים");
                        LifePolicy newLifePolicy = new LifePolicy()
                        {
                            InsuranceID = insuranceInContext.InsuranceID,
                            ClientID = clientId,
                            IsOffer = isOffer,
                            LifeIndustryID = industryId,
                            PolicyNumber = policyNumber,
                            Addition = addition,
                            OpenDate = openDate,
                            CompanyID = companyId,
                            PrincipalAgentNumberID = principalAgentNumberId,
                            SubAgentNumberID = subAgentNumberId,
                            FundTypeID = fundType,
                            StartDate = startDate,
                            EndDate = endDate,
                            PrimaryIndex = primaryIndex,
                            InsuredName = insuredName,
                            InsuredIdNumber = insuredIdNumber,
                            PolicyOwnerID = policyOwnerId,
                            FactoryNumberID = factoryNumberId,
                            PaymentMethod = paymentMethod,
                            IsIndexation = isIndexation,
                            SubAnnual = subAnnual,
                            PolicyFactor = policyFactor,
                            TotalMonthlyPayment = totalMonthlyPayment,
                            BankPayment = bankPayment,
                            BankBranchPayment = bankBranchPayment,
                            AccountNumberPayment = accountNumberPayment,
                            IsMortgaged = isMortgaged,
                            BankName = bankName,
                            BankBranchNumber = bankBranch,
                            BankAddress = bankAddress,
                            BankPhone1 = bankPhone,
                            BankFax = bankFax,
                            BankContactName = bankContactName,
                            BankEmail = bankEmail,
                            Comments = comments
                        };
                        context.LifePolicies.Add(newLifePolicy);
                        context.SaveChanges();
                        return newLifePolicy.LifePolicyID;
                    }
                    catch (Exception e)
                    {

                        throw new Exception(e.Message);
                    }
                }
                else
                {
                    throw new Exception("פוליסה קיימת במערכת");
                }
            }
        }

        public void TransferPolicyToClient(LifePolicy policy, Client client)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var policyInContext = context.LifePolicies.FirstOrDefault(p => p.LifePolicyID == policy.LifePolicyID);
                if (policyInContext != null)
                {
                    policyInContext.ClientID = client.ClientID;
                    context.SaveChanges();
                }
                else
                {
                    throw new Exception("פוליסה לא קיימת במערכת");
                }
            }
        }

        public List<LifePolicy> GetAllLifeOffersByClient(Client client)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                if (client == null)
                {
                    return null;
                }
                return context.LifePolicies.Include("Client").Include("Insurance").Include("Company").Include("FundType").Include("LifeIndustry").Include("ManagerPolicy").Include("MortgageLifeIndustry").Include("PrivateLifeIndustry").Include("PensionLifeIndustry").Where(p => p.ClientID == client.ClientID && p.IsOffer == true).OrderByDescending(p => p.StartDate).ToList();
            }
        }

        //מעדכן פוליסה קיימת
        public void UpdateLifePolicy(int lifePolicyId, string policyNumber, int industryID, int? companyId, int? principalAgentNumberId, int? subAgentNumberId, int fundType, DateTime? startDate, DateTime? endDate, string primaryIndex, string insuredName, string insuredNumber, int? policyOwnerId, int? factoryNumberId, string paymentMethod, bool? isIndexation, decimal? subAnnual, decimal? policyFactor, decimal? totalMonthlyPayment, string bankPayment, string bankBranchPayment, string accountNumberPayment, bool? isMortgaged, string bankName, string bankBranch, string bankAddress, string bankPhone, string bankFax, string bankContactName, string bankEmail, string comments)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var policyInContext = context.LifePolicies.FirstOrDefault(p => p.LifePolicyID == lifePolicyId);
                if (policyInContext != null)
                {
                    try
                    {
                        policyInContext.PolicyNumber = policyNumber;
                        policyInContext.LifeIndustryID = industryID;
                        policyInContext.CompanyID = companyId;
                        policyInContext.PrincipalAgentNumberID = principalAgentNumberId;
                        policyInContext.SubAgentNumberID = subAgentNumberId;
                        policyInContext.FundTypeID = fundType;
                        policyInContext.StartDate = startDate;
                        policyInContext.EndDate = endDate;
                        policyInContext.PrimaryIndex = primaryIndex;
                        policyInContext.InsuredName = insuredName;
                        policyInContext.InsuredIdNumber = insuredNumber;
                        policyInContext.PolicyOwnerID = policyOwnerId;
                        policyInContext.FactoryNumberID = factoryNumberId;
                        policyInContext.PaymentMethod = paymentMethod;
                        policyInContext.IsIndexation = isIndexation;
                        policyInContext.SubAnnual = subAnnual;
                        policyInContext.PolicyFactor = policyFactor;
                        policyInContext.TotalMonthlyPayment = totalMonthlyPayment;
                        policyInContext.BankPayment = bankPayment;
                        policyInContext.BankBranchPayment = bankBranchPayment;
                        policyInContext.AccountNumberPayment = accountNumberPayment;
                        policyInContext.IsMortgaged = isMortgaged;
                        policyInContext.BankName = bankName;
                        policyInContext.BankBranchNumber = bankBranch;
                        policyInContext.BankAddress = bankAddress;
                        policyInContext.BankPhone1 = bankPhone;
                        policyInContext.BankFax = bankFax;
                        policyInContext.BankContactName = bankContactName;
                        policyInContext.BankEmail = bankEmail;
                        policyInContext.Comments = comments;

                        context.SaveChanges();

                    }
                    catch (Exception e)
                    {
                        throw new Exception(e.Message);
                    }
                }
                else
                {
                    throw new Exception("פוליסה לא קיימת במערכת");
                }
            }
        }


        public void InsertAdmPolicy(int lifePolicyId, bool? isCompulsoryPension, bool? isShareholder, string comments)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var policyInContext = context.LifePolicies.FirstOrDefault(p => p.LifePolicyID == lifePolicyId);
                if (policyInContext != null)
                {
                    var admPolicyInContext = context.ManagerPolicies.FirstOrDefault(m => m.LifePolicyID == lifePolicyId);
                    if (admPolicyInContext == null)
                    {
                        try
                        {
                            ManagerPolicy newAdmPolicy = new ManagerPolicy()
                            {
                                LifePolicyID = lifePolicyId,
                                //IsFundNotPaysForCompensation = isFundNotPaysForCompensation,
                                IsCompulsoryPension = isCompulsoryPension,
                                IsShareholder = isShareholder,
                                Comments = comments
                            };
                            context.ManagerPolicies.Add(newAdmPolicy);
                            context.SaveChanges();
                        }
                        catch (Exception e)
                        {

                            throw new Exception(e.Message);
                        }
                    }
                    else
                    {
                        throw new Exception("קיימת פוליסת מנהלים תחת פוליסת חיים זאת במערכת");
                    }
                }
                else
                {
                    throw new Exception("פוליסה לא קיימת במערכת");
                }
            }
        }

        public void UpdateAdmPolicy(int lifePolicyId, bool? isCompulsoryPension, bool? isShareholder, string comments)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var admPolicyInContext = context.ManagerPolicies.FirstOrDefault(m => m.LifePolicyID == lifePolicyId);
                if (admPolicyInContext != null)
                {
                    try
                    {
                        //admPolicyInContext.IsFundNotPaysForCompensation = isFundNotPaysForCompensation;
                        admPolicyInContext.IsCompulsoryPension = isCompulsoryPension;
                        admPolicyInContext.IsShareholder = isShareholder;
                        admPolicyInContext.Comments = comments;
                        context.SaveChanges();
                    }
                    catch (Exception e)
                    {

                        throw new Exception(e.Message);
                    }
                }
                else
                {
                    InsertAdmPolicy(lifePolicyId, isCompulsoryPension, isShareholder, comments);
                    //throw new Exception("פוליסה לא קיימת במערכת");
                }
            }
        }

        public void InsertSalaryChangesToPolicy(int lifePolicyId, List<ManagerPolicyChanx> salaryChanges, bool isAddition)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var policyInContext = context.LifePolicies.FirstOrDefault(p => p.LifePolicyID == lifePolicyId);
                if (policyInContext != null)
                {
                    try
                    {
                        var policySalariesInContext = context.ManagerPolicyChanges.Where(s => s.LifePolicyID == lifePolicyId).ToList();

                        foreach (var item in salaryChanges)
                        {
                            if (item.ManagerPolicyChangeID == 0)
                            {
                                item.LifePolicyID = lifePolicyId;
                                context.ManagerPolicyChanges.Add(item);
                            }
                            else if (isAddition)
                            {
                                ManagerPolicyChanx newChange = new ManagerPolicyChanx()
                                {
                                    EmployerBenefitsProvisions = item.EmployerBenefitsProvisions,
                                    EmployerBenefitsRevalued = item.EmployerBenefitsRevalued,
                                    EmployerCompensationProvisions = item.EmployerCompensationProvisions,
                                    EmployerCompensationRevalued = item.EmployerCompensationRevalued,
                                    EmployerDisabilityProvisions = item.EmployerDisabilityProvisions,
                                    EmployerDisabilityRevalued = item.EmployerDisabilityRevalued,
                                    EmployerOthersProvisions = item.EmployerOthersProvisions,
                                    EmployerOthersRevalued = item.EmployerOthersRevalued,
                                    EmployerTotalProvisions = item.EmployerTotalProvisions,
                                    EmployerTotalRevalued = item.EmployerTotalRevalued,
                                    EndPeriod = item.EndPeriod,
                                    FromDate = item.FromDate,
                                    LifePolicyID = lifePolicyId,
                                    MonthlyBudget = item.MonthlyBudget,
                                    TotalPremium = item.TotalPremium,
                                    TotalSalary = item.TotalSalary,
                                    WorkerBenefits45Provisions = item.WorkerBenefits45Provisions,
                                    WorkerBenefits45Revalued = item.WorkerBenefits45Revalued,
                                    WorkerBenefits47Provisions = item.WorkerBenefits47Provisions,
                                    WorkerBenefits47Revalued = item.WorkerBenefits47Revalued,
                                    WorkerOthersProvisions1 = item.WorkerOthersProvisions1,
                                    WorkerOthersRevalued1 = item.WorkerOthersRevalued1,
                                    WorkerTotalProvisions1 = item.WorkerTotalProvisions1,
                                    WorkerTotalRevalued1 = item.WorkerTotalRevalued1
                                };
                                context.ManagerPolicyChanges.Add(newChange);
                            }
                            else
                            {
                                var salaryChangesInContext = context.ManagerPolicyChanges.FirstOrDefault(s => s.ManagerPolicyChangeID == item.ManagerPolicyChangeID);
                                if (salaryChangesInContext != null)
                                {
                                    salaryChangesInContext.EmployerBenefitsProvisions = item.EmployerBenefitsProvisions;
                                    salaryChangesInContext.EmployerBenefitsRevalued = item.EmployerBenefitsRevalued;
                                    salaryChangesInContext.EmployerCompensationProvisions = item.EmployerCompensationProvisions;
                                    salaryChangesInContext.EmployerCompensationRevalued = item.EmployerCompensationRevalued;
                                    salaryChangesInContext.EmployerDisabilityProvisions = item.EmployerDisabilityProvisions;
                                    salaryChangesInContext.EmployerDisabilityRevalued = item.EmployerDisabilityRevalued;
                                    salaryChangesInContext.EmployerOthersProvisions = item.EmployerOthersProvisions;
                                    salaryChangesInContext.EmployerOthersRevalued = item.EmployerOthersRevalued;
                                    salaryChangesInContext.EmployerTotalProvisions = item.EmployerTotalProvisions;
                                    salaryChangesInContext.EmployerTotalRevalued = item.EmployerTotalRevalued;
                                    salaryChangesInContext.EndPeriod = item.EndPeriod;
                                    salaryChangesInContext.FromDate = item.FromDate;
                                    salaryChangesInContext.MonthlyBudget = item.MonthlyBudget;
                                    salaryChangesInContext.TotalPremium = item.TotalPremium;
                                    salaryChangesInContext.TotalSalary = item.TotalSalary;
                                    salaryChangesInContext.WorkerBenefits45Provisions = item.WorkerBenefits45Provisions;
                                    salaryChangesInContext.WorkerBenefits45Revalued = item.WorkerBenefits45Revalued;
                                    salaryChangesInContext.WorkerBenefits47Provisions = item.WorkerBenefits47Provisions;
                                    salaryChangesInContext.WorkerBenefits47Revalued = item.WorkerBenefits47Revalued;
                                    salaryChangesInContext.WorkerOthersProvisions1 = item.WorkerOthersProvisions1;
                                    salaryChangesInContext.WorkerOthersRevalued1 = item.WorkerOthersRevalued1;
                                    salaryChangesInContext.WorkerTotalProvisions1 = item.WorkerTotalProvisions1;
                                    salaryChangesInContext.WorkerTotalRevalued1 = item.WorkerTotalRevalued1;
                                    policySalariesInContext.Remove(salaryChangesInContext);
                                }

                            }
                        }
                        if (policySalariesInContext.Count > 0)
                        {
                            foreach (var item in policySalariesInContext)
                            {
                                context.ManagerPolicyChanges.Remove(item);
                            }
                        }
                        context.SaveChanges();
                    }
                    catch (Exception e)
                    {
                        throw new Exception(e.Message);
                    }
                }
                else
                {
                    throw new Exception("פוליסה לא קיימת במערכת");
                }
            }
        }

        public List<ManagerPolicyChanx> GetSalaryChangesByPolicy(int lifePolicyId)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                try
                {
                    return context.ManagerPolicyChanges.Where(s => s.LifePolicyID == lifePolicyId).ToList();
                }
                catch (Exception e)
                {
                    throw new Exception(e.Message);
                }
            }
        }

        public List<ManagerPolicyChanx> GetSalaryChangesByClient(int clientId)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                try
                {
                    return context.ManagerPolicyChanges.Where(s => s.LifePolicy.ClientID == clientId).ToList();
                }
                catch (Exception e)
                {
                    throw new Exception(e.Message);
                }
            }
        }

        public void InsertPolicyTransfer(int lifePolicyId, List<LifePolicyTransfer> transfers, bool isAddition)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var policyInContext = context.LifePolicies.FirstOrDefault(p => p.LifePolicyID == lifePolicyId);
                if (policyInContext != null)
                {
                    try
                    {
                        var policyTransfersInContext = context.LifePolicyTransfers.Where(t => t.LifePolicyID == lifePolicyId).ToList();
                        foreach (var item in transfers)
                        {
                            if (item.LifePolicyTransferID == 0)
                            {
                                item.LifePolicyID = lifePolicyId;
                                item.Company = null;
                                item.Company1 = null;
                                item.FundType = null;
                                item.FundType1 = null;
                                context.LifePolicyTransfers.Add(item);
                            }
                            else if (isAddition)
                            {
                                LifePolicyTransfer newTransfer = new LifePolicyTransfer()
                                {
                                    Company = null,
                                    Company1 = null,
                                    FundType = null,
                                    FundType1 = null,
                                    FundTypeID = item.FundTypeID,
                                    LifePolicyID = lifePolicyId,
                                    TransferAmount = item.TransferAmount,
                                    TransferDate = item.TransferDate,
                                    TransferedToCompanyID = item.TransferedToCompanyID,
                                    TransferedToFundTypeID = item.TransferedToFundTypeID,
                                    TransferringCompanyID = item.TransferringCompanyID
                                };
                                context.LifePolicyTransfers.Add(newTransfer);
                            }
                            else
                            {
                                var transferInContext = context.LifePolicyTransfers.FirstOrDefault(t => t.LifePolicyTransferID == item.LifePolicyTransferID);
                                if (transferInContext != null)
                                {
                                    transferInContext.FundTypeID = item.FundTypeID;
                                    transferInContext.TransferAmount = item.TransferAmount;
                                    transferInContext.TransferDate = item.TransferDate;
                                    transferInContext.TransferedToCompanyID = item.TransferedToCompanyID;
                                    transferInContext.TransferedToFundTypeID = item.TransferedToFundTypeID;
                                    transferInContext.TransferringCompanyID = item.TransferringCompanyID;
                                    policyTransfersInContext.Remove(transferInContext);
                                }

                            }
                        }
                        if (policyTransfersInContext.Count > 0)
                        {
                            foreach (var item in policyTransfersInContext)
                            {
                                context.LifePolicyTransfers.Remove(item);
                            }
                        }
                        context.SaveChanges();
                    }
                    catch (Exception e)
                    {
                        throw new Exception(e.Message);
                    }
                }
                else
                {
                    throw new Exception("פוליסה לא קיימת במערכת");
                }
            }
        }

        public List<LifePolicyTransfer> GetTransfersByPolicy(int lifePolicyId)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                try
                {
                    return context.LifePolicyTransfers.Include("Company").Include("Company1").Include("FundType").Include("FundType1").Where(t => t.LifePolicyID == lifePolicyId).ToList();
                }
                catch (Exception e)
                {
                    throw new Exception(e.Message);
                }
            }
        }

        public List<int> InsertAdditionalInsureds(int lifePolicyId, List<LifeAdditionalInsured> additionalInsureds, bool isAddition)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                List<int> additionalIds = new List<int>();
                var policyInContext = context.LifePolicies.FirstOrDefault(p => p.LifePolicyID == lifePolicyId);
                if (policyInContext != null)
                {
                    try
                    {
                        foreach (var item in additionalInsureds)
                        {
                            if (item.LifeAdditionalInsuredID == 0)
                            {
                                item.LifePolicyID = lifePolicyId;
                                context.LifeAdditionalInsureds.Add(item);
                                context.SaveChanges();
                                additionalIds.Add(item.LifeAdditionalInsuredID);
                            }
                            else if (isAddition)
                            {
                                LifeAdditionalInsured newAdditional = new LifeAdditionalInsured() { BirthDate = item.BirthDate, FirstName = item.FirstName, IdNumber = item.IdNumber, LastName = item.LastName, LifePolicyID = lifePolicyId, RelationshipWithPrincipalInsured = item.RelationshipWithPrincipalInsured };
                                context.LifeAdditionalInsureds.Add(newAdditional);
                                context.SaveChanges();
                                additionalIds.Add(newAdditional.LifeAdditionalInsuredID);
                            }
                            else
                            {
                                //update
                                var additionalInsuredInContext = context.LifeAdditionalInsureds.FirstOrDefault(a => a.LifeAdditionalInsuredID == item.LifeAdditionalInsuredID);
                                if (additionalInsuredInContext != null)
                                {
                                    additionalInsuredInContext.BirthDate = item.BirthDate;
                                    additionalInsuredInContext.FirstName = item.FirstName;
                                    additionalInsuredInContext.IdNumber = item.IdNumber;
                                    additionalInsuredInContext.LastName = item.LastName;
                                    additionalInsuredInContext.RelationshipWithPrincipalInsured = item.RelationshipWithPrincipalInsured;
                                    context.SaveChanges();
                                }
                            }
                        }

                        return additionalIds;
                    }
                    catch (Exception e)
                    {
                        throw new Exception(e.Message);
                    }
                }
                else
                {
                    throw new Exception("פוליסה לא קיימת במערכת");
                }
            }
        }

        public LifePolicy GetLifePolicyByPolicyId(int policyId)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.LifePolicies.Include("LifeIndustry").Include("ManagerPolicy").Include("MortgageLifeIndustry").Include("PensionLifeIndustry").Include("PrivateLifeIndustry").Include("Risks").Include("Client").FirstOrDefault(p => p.LifePolicyID == policyId);
            }
        }

        public void InsertAdditionalInsured(int lifePolicyId, string firsName, string lastName, string idNumber, DateTime? birthDate, string relationship)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var policyInContext = context.LifePolicies.FirstOrDefault(p => p.LifePolicyID == lifePolicyId);
                if (policyInContext != null)
                {
                    try
                    {
                        LifeAdditionalInsured newInsured = new LifeAdditionalInsured()
                        {
                            FirstName = firsName,
                            LastName = lastName,
                            IdNumber = idNumber,
                            BirthDate = birthDate,
                            RelationshipWithPrincipalInsured = relationship,
                            LifePolicyID = lifePolicyId
                        };
                        context.LifeAdditionalInsureds.Add(newInsured);
                        context.SaveChanges();
                    }
                    catch (Exception e)
                    {
                        throw new Exception(e.Message);
                    }
                }
                else
                {
                    throw new Exception("פוליסה לא קיימת במערכת");
                }
            }
        }

        public void UpdateAdditionalInsured(int lifePolicyId, string firsName, string lastName, string idNumber, DateTime? birthDate, string relationship)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var additionalInsuredInContext = context.LifeAdditionalInsureds.FirstOrDefault(a => a.LifePolicyID == lifePolicyId);

                if (additionalInsuredInContext != null)
                {
                    try
                    {
                        if (additionalInsuredInContext != null)
                        {
                            additionalInsuredInContext.FirstName = firsName;
                            additionalInsuredInContext.LastName = lastName;
                            additionalInsuredInContext.IdNumber = idNumber;
                            additionalInsuredInContext.BirthDate = birthDate;
                            additionalInsuredInContext.RelationshipWithPrincipalInsured = relationship;

                            context.SaveChanges();
                        }
                    }
                    catch (Exception e)
                    {
                        throw new Exception(e.Message);
                    }
                }
                else if (additionalInsuredInContext == null)
                {
                    InsertAdditionalInsured(lifePolicyId, firsName, lastName, idNumber, birthDate, relationship);

                }
                else
                {
                    throw new Exception("פוליסה לא קיימת במערכת");
                }
            }
        }

        public void DeleteAdditionalInsured(int lifePolicyId)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var additionalInsuredInContext = context.LifeAdditionalInsureds.FirstOrDefault(a => a.LifePolicyID == lifePolicyId);

                if (additionalInsuredInContext != null)
                {
                    try
                    {

                        context.LifeAdditionalInsureds.Remove(additionalInsuredInContext);
                        context.SaveChanges();
                    }
                    catch (Exception e)
                    {
                        throw new Exception(e.Message);
                    }
                }
                //else
                //{
                //    throw new Exception("פוליסה לא קיימת במערכת");
                //}
            }
        }

        public List<LifeAdditionalInsured> GetAdditionalInsuredsByPolicy(int lifePolicyId)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                try
                {
                    return context.LifeAdditionalInsureds.Where(t => t.LifePolicyID == lifePolicyId).ToList();
                }
                catch (Exception e)
                {
                    throw new Exception(e.Message);
                }
            }
        }

        public void InsertRisks(int lifePolicyId, List<Risk> risks)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var policyInContext = context.LifePolicies.FirstOrDefault(p => p.LifePolicyID == lifePolicyId);
                if (policyInContext != null)
                {
                    try
                    {
                        var policyRisksInContext = context.Risks.Where(r => r.LifePolicyID == lifePolicyId).ToList();
                        foreach (var item in risks)
                        {
                            if (item.RiskID == 0)
                            {
                                item.LifePolicyID = lifePolicyId;
                                context.Risks.Add(item);
                            }
                            else
                            {
                                var riskInContext = context.Risks.FirstOrDefault(r => r.RiskID == item.RiskID);
                                if (riskInContext != null)
                                {
                                    riskInContext.IsDeathType1 = item.IsDeathType1;
                                    riskInContext.IsDeathType5 = item.IsDeathType5;
                                    riskInContext.IsDeathTypeFixed = item.IsDeathTypeFixed;
                                    riskInContext.DeathAmount = item.DeathAmount;
                                    riskInContext.DeathPeriod = item.DeathPeriod;
                                    riskInContext.DeathCoverageType = item.DeathCoverageType;
                                    riskInContext.DeathPremium = item.DeathPremium;
                                    riskInContext.DeathComments = item.DeathComments;
                                    riskInContext.IsProfessionalWorkImpossibility = item.IsProfessionalWorkImpossibility;
                                    riskInContext.WorkImpossibilityAmount = item.WorkImpossibilityAmount;
                                    riskInContext.WorkImpossibilityPeriod = item.WorkImpossibilityPeriod;
                                    riskInContext.WorkImpossibilityCoverageType = item.WorkImpossibilityCoverageType;
                                    riskInContext.WorkImpossibilityPremium = item.WorkImpossibilityPremium;
                                    riskInContext.WorkImpossibilityComments = item.WorkImpossibilityComments;
                                    riskInContext.SecureIncomeAmount = item.SecureIncomeAmount;
                                    riskInContext.SecureIncomePeriod = item.SecureIncomePeriod;
                                    riskInContext.SecureIncomeCoverageType = item.SecureIncomeCoverageType;
                                    riskInContext.SecureIncomePremium = item.SecureIncomePremium;
                                    riskInContext.SecureIncomeComments = item.SecureIncomeComments;
                                    riskInContext.DisabilityByAccidentAmount = item.DisabilityByAccidentAmount;
                                    riskInContext.DisabilityByAccidentPeriod = item.DisabilityByAccidentPeriod;
                                    riskInContext.DisabilityByAccidentCoverageType = item.DisabilityByAccidentCoverageType;
                                    riskInContext.DisabilityByAccidentPremium = item.DisabilityByAccidentPremium;
                                    riskInContext.DisabilityByAccidentComments = item.DisabilityByAccidentComments;
                                    riskInContext.DeathByAccidentAmount = item.DeathByAccidentAmount;
                                    riskInContext.DeathByAccidentPeriod = item.DeathByAccidentPeriod;
                                    riskInContext.DeathByAccidentCoverageType = item.DeathByAccidentCoverageType;
                                    riskInContext.DeathByAccidentPremium = item.DeathByAccidentPremium;
                                    riskInContext.DeathByAccidentComments = item.DeathByAccidentComments;
                                    riskInContext.CriticalIllnessAmount = item.CriticalIllnessAmount;
                                    riskInContext.CriticalIllnessPeriod = item.CriticalIllnessPeriod;
                                    riskInContext.CriticalIllnessCoverageType = item.CriticalIllnessCoverageType;
                                    riskInContext.CriticalIllnessPremium = item.CriticalIllnessPremium;
                                    riskInContext.CriticalIllnessComments = item.CriticalIllnessComments;
                                    riskInContext.Other1Name = item.Other1Name;
                                    riskInContext.Other1Amount = item.Other1Amount;
                                    riskInContext.Other1Period = item.Other1Period;
                                    riskInContext.Other1CoverageType = item.Other1CoverageType;
                                    riskInContext.Other1Premium = item.Other1Premium;
                                    riskInContext.Other1Comments = item.Other1Comments;
                                    riskInContext.Other2Name = item.Other2Name;
                                    riskInContext.Other2Amount = item.Other2Amount;
                                    riskInContext.Other2Period = item.Other2Period;
                                    riskInContext.Other2CoverageType = item.Other2CoverageType;
                                    riskInContext.Other2Premium = item.Other2Premium;
                                    riskInContext.Other2Comments = item.Other2Comments;
                                    riskInContext.Other3Name = item.Other3Name;
                                    riskInContext.Other3Amount = item.Other3Amount;
                                    riskInContext.Other3Period = item.Other3Period;
                                    riskInContext.Other3CoverageType = item.Other3CoverageType;
                                    riskInContext.Other3Premium = item.Other3Premium;
                                    riskInContext.Other3Comments = item.Other3Comments;
                                    riskInContext.Other4Name = item.Other4Name;
                                    riskInContext.Other4Amount = item.Other4Amount;
                                    riskInContext.Other4Period = item.Other4Period;
                                    riskInContext.Other4CoverageType = item.Other4CoverageType;
                                    riskInContext.Other4Premium = item.Other4Premium;
                                    riskInContext.Other4Comments = item.Other4Comments;
                                    riskInContext.Other5Name = item.Other5Name;
                                    riskInContext.Other5Amount = item.Other5Amount;
                                    riskInContext.Other5Period = item.Other5Period;
                                    riskInContext.Other5CoverageType = item.Other5CoverageType;
                                    riskInContext.Other5Premium = item.Other5Premium;
                                    riskInContext.Other5Comments = item.Other5Comments;

                                    policyRisksInContext.Remove(riskInContext);
                                }
                            }
                        }
                        if (policyRisksInContext.Count > 0)
                        {
                            foreach (var item in policyRisksInContext)
                            {
                                context.Risks.Remove(item);
                            }
                        }
                        context.SaveChanges();
                    }
                    catch (Exception e)
                    {
                        throw new Exception(e.Message);
                    }
                }
                else
                {
                    throw new Exception("פוליסה לא קיימת במערכת");
                }
            }
        }

        public List<LifePolicy> GetAllActiveLifePoliciesByClient(Client client)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                if (client != null)
                {
                    var policies = context.LifePolicies.Where(p => p.ClientID == client.ClientID && p.EndDate >= DateTime.Today && p.IsOffer == false).OrderByDescending(p => p.StartDate).ThenByDescending(p => p.OpenDate).ToList();
                    var sortedPolicies = context.LifePolicies.Include("Insurance").Include("Client").Include("Company").Include("FundType").Include("LifeIndustry").Include("ManagerPolicy").Include("MortgageLifeIndustry").Include("PrivateLifeIndustry").Include("PensionLifeIndustry").Where(p => p.ClientID == client.ClientID && p.EndDate >= DateTime.Today && p.IsOffer == false).OrderByDescending(p => p.StartDate).ThenByDescending(p => p.OpenDate).ToList();
                    for (int i = policies.Count - 1; i >= 0; i--)
                    {
                        if (policies[i].Addition > 0)
                        {
                            var policy = policies.Where(p => p.PolicyNumber == policies[i].PolicyNumber && p.Addition == policies[i].Addition - 1).FirstOrDefault();
                            if (policy != null)
                            {
                                sortedPolicies.Remove(policies[i]);
                                var index = sortedPolicies.FindIndex(p => p.PolicyNumber == policy.PolicyNumber && p.Addition == policy.Addition);
                                sortedPolicies.Insert(index, policies[i]);
                            }
                        }
                    }
                    return sortedPolicies;
                }
                else
                {
                    return null;
                }
            }
        }

        public List<Risk> GetRisksByPolicy(int lifePolicyId)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                try
                {
                    return context.Risks.Where(r => r.LifePolicyID == lifePolicyId).ToList();
                }
                catch (Exception e)
                {
                    throw new Exception(e.Message);
                }
            }
        }
        public void InsertMortgagePolicy(int lifePolicyId, string irrevocableBeneficiary, string bankNumber, string bankBranch, string bankAddress, string bankContactName, string bankPhone, string bankFax, string bankEmail, bool? isReleaseFromPayingPremiums, decimal? premium, string comments)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {

                var policyInContext = context.LifePolicies.FirstOrDefault(p => p.LifePolicyID == lifePolicyId);
                if (policyInContext != null)
                {
                    try
                    {
                        MortgageLifeIndustry newMortgagePolicy = new MortgageLifeIndustry()
                        {
                            LifePolicyID = lifePolicyId,
                            IrrevocableBeneficiary = irrevocableBeneficiary,
                            BankNumber = bankNumber,
                            BankBranch = bankBranch,
                            BankAddress = bankAddress,
                            BankContactName = bankContactName,
                            BankPhone = bankPhone,
                            BankFax = bankFax,
                            BankEmail = bankEmail,
                            IsReleaseFromPayingPremiums = isReleaseFromPayingPremiums,
                            Premium = premium,
                            Comments = comments
                        };
                        context.MortgageLifeIndustries.Add(newMortgagePolicy);
                        context.SaveChanges();
                    }
                    catch (Exception e)
                    {
                        throw new Exception(e.Message);
                    }
                }
                else
                {
                    throw new Exception("פוליסה לא קיימת במערכת");
                }
            }
        }

        public List<LifePolicy> GetNonActiveLifePoliciesByClient(Client client)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                if (client != null)
                {
                    var policies = context.LifePolicies.Where(p => p.ClientID == client.ClientID && p.EndDate < DateTime.Today && p.IsOffer == false).OrderByDescending(p => p.StartDate).ThenByDescending(p => p.OpenDate).ToList();
                    var sortedPolicies = context.LifePolicies.Include("Insurance").Include("Client").Include("Company").Include("FundType").Include("LifeIndustry").Include("ManagerPolicy").Include("MortgageLifeIndustry").Include("PrivateLifeIndustry").Include("PensionLifeIndustry").Where(p => p.ClientID == client.ClientID && p.EndDate < DateTime.Today && p.IsOffer == false).OrderByDescending(p => p.StartDate).ThenByDescending(p => p.OpenDate).ToList();
                    for (int i = policies.Count - 1; i >= 0; i--)
                    {
                        if (policies[i].Addition > 0)
                        {
                            var policy = policies.Where(p => p.PolicyNumber == policies[i].PolicyNumber && p.Addition == policies[i].Addition - 1).FirstOrDefault();
                            if (policy != null)
                            {
                                sortedPolicies.Remove(policies[i]);
                                var index = sortedPolicies.FindIndex(p => p.PolicyNumber == policy.PolicyNumber && p.Addition == policy.Addition);
                                sortedPolicies.Insert(index, policies[i]);
                            }
                        }
                    }
                    return sortedPolicies;
                }
                else
                {
                    return null;
                }
            }
        }

        public void UpdateMortgagePolicy(int lifePolicyId, string irrevocableBeneficiary, string bankNumber, string bankBranch, string bankAddress, string bankContactName, string bankPhone, string bankFax, string bankEmail, bool? isReleaseFromPayingPremiums, decimal? premium, string comments)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {

                var policyInContext = context.MortgageLifeIndustries.FirstOrDefault(m => m.LifePolicyID == lifePolicyId);
                if (policyInContext != null)
                {
                    try
                    {
                        policyInContext.IrrevocableBeneficiary = irrevocableBeneficiary;
                        policyInContext.BankNumber = bankNumber;
                        policyInContext.BankBranch = bankBranch;
                        policyInContext.BankAddress = bankAddress;
                        policyInContext.BankContactName = bankContactName;
                        policyInContext.BankPhone = bankPhone;
                        policyInContext.BankFax = bankFax;
                        policyInContext.BankEmail = bankEmail;
                        policyInContext.IsReleaseFromPayingPremiums = isReleaseFromPayingPremiums;
                        policyInContext.Premium = premium;
                        policyInContext.Comments = comments;

                        context.SaveChanges();
                    }
                    catch (Exception e)
                    {
                        throw new Exception(e.Message);
                    }
                }
                else
                {
                    InsertMortgagePolicy(lifePolicyId, irrevocableBeneficiary, bankNumber, bankBranch, bankAddress, bankContactName, bankPhone, bankFax, bankEmail, isReleaseFromPayingPremiums, premium, comments);
                    //throw new Exception("פוליסה לא קיימת במערכת");
                }
            }
        }

        public void InsertMortgages(int lifePolicyId, List<Mortgage> mortgages, bool isAddition)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var policyInContext = context.LifePolicies.FirstOrDefault(p => p.LifePolicyID == lifePolicyId);
                if (policyInContext != null)
                {
                    try
                    {
                        var policyMortgages = context.Mortgages.Where(m => m.LifePolicyID == lifePolicyId).ToList();
                        foreach (var item in mortgages)
                        {
                            if (item.MortgageID == 0)
                            {
                                item.LifePolicyID = lifePolicyId;
                                context.Mortgages.Add(item);
                            }
                            else if (isAddition)
                            {
                                Mortgage newMortgage = new Mortgage() { AnualInterest = item.AnualInterest, EndDate = item.EndDate, HatzmadaType = item.HatzmadaType, IsPrincipalInsuredPolicy = item.IsPrincipalInsuredPolicy, LifePolicyID = lifePolicyId, MortgageAmount = item.MortgageAmount, MortgageNumber = item.MortgageNumber, MortgagePeriod = item.MortgagePeriod, Premium = item.Premium, PremiumPeriod = item.PremiumPeriod, StartDate = item.StartDate };
                                context.Mortgages.Add(newMortgage);
                            }
                            else
                            {
                                var mortgageInContext = context.Mortgages.FirstOrDefault(m => m.MortgageID == item.MortgageID);
                                if (mortgageInContext != null)
                                {
                                    mortgageInContext.AnualInterest = item.AnualInterest;
                                    mortgageInContext.EndDate = item.EndDate;
                                    mortgageInContext.HatzmadaType = item.HatzmadaType;
                                    mortgageInContext.IsPrincipalInsuredPolicy = item.IsPrincipalInsuredPolicy;
                                    mortgageInContext.MortgageAmount = item.MortgageAmount;
                                    mortgageInContext.MortgageNumber = item.MortgageNumber;
                                    mortgageInContext.MortgagePeriod = item.MortgagePeriod;
                                    mortgageInContext.Premium = item.Premium;
                                    mortgageInContext.PremiumPeriod = item.PremiumPeriod;
                                    mortgageInContext.StartDate = item.StartDate;
                                    policyMortgages.Remove(mortgageInContext);
                                }
                            }
                        }
                        if (policyMortgages.Count > 0)
                        {
                            foreach (var item in policyMortgages)
                            {
                                context.Mortgages.Remove(item);
                            }
                        }
                        context.SaveChanges();
                    }
                    catch (Exception e)
                    {
                        throw new Exception(e.Message);
                    }
                }
                else
                {
                    throw new Exception("פוליסה לא קיימת במערכת");
                }
            }
        }

        public List<Mortgage> GetMortgagesByPolicy(int lifePolicyId)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                try
                {
                    return context.Mortgages.Include("LifePolicy").Where(m => m.LifePolicyID == lifePolicyId).ToList();
                }
                catch (Exception e)
                {
                    throw new Exception(e.Message);
                }
            }
        }

        public void InsertPrivatePolicy(int lifePolicyId, bool? isRewards, int? age, decimal? budget, bool? IsFundNotPaysForCompensation, decimal? startBudget, decimal? oneTimeDeposit, decimal? premiumFees, decimal? aggregateFees, int? investmentPlanId, string comments)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var policyInContext = context.LifePolicies.FirstOrDefault(p => p.LifePolicyID == lifePolicyId);
                if (policyInContext != null)
                {
                    try
                    {
                        PrivateLifeIndustry newPrivatePolicy = new PrivateLifeIndustry()
                        {
                            LifePolicyID = lifePolicyId,
                            IsRewards = isRewards,
                            Age = age,
                            Budget = budget,
                            IsFundNotPaysForCompensation = IsFundNotPaysForCompensation,
                            StartBudget = startBudget,
                            OneTimeDeposit = oneTimeDeposit,
                            PremiumFees = premiumFees,
                            AggregateFees = aggregateFees,
                            InvestmentPlanID = investmentPlanId,
                            Comments = comments
                        };
                        context.PrivateLifeIndustries.Add(newPrivatePolicy);
                        context.SaveChanges();
                    }
                    catch (Exception e)
                    {
                        throw new Exception(e.Message);
                    }
                }
                else
                {
                    throw new Exception("פוליסה לא קיימת במערכת");
                }
            }
        }

        public void UpdatePrivatePolicy(int lifePolicyId, bool? isRewards, int? age, decimal? budget, bool? IsFundNotPaysForCompensation, decimal? startBudget, decimal? oneTimeDeposit, decimal? premiumFees, decimal? aggregateFees, int? investmentPlanId, string comments)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var policyInContext = context.PrivateLifeIndustries.FirstOrDefault(p => p.LifePolicyID == lifePolicyId);
                if (policyInContext != null)
                {
                    try
                    {
                        policyInContext.IsRewards = isRewards;
                        policyInContext.Age = age;
                        policyInContext.Budget = budget;
                        policyInContext.IsFundNotPaysForCompensation = IsFundNotPaysForCompensation;
                        policyInContext.StartBudget = startBudget;
                        policyInContext.OneTimeDeposit = oneTimeDeposit;
                        policyInContext.PremiumFees = premiumFees;
                        policyInContext.AggregateFees = aggregateFees;
                        policyInContext.InvestmentPlanID = investmentPlanId;
                        policyInContext.Comments = comments;

                        context.SaveChanges();
                    }
                    catch (Exception e)
                    {
                        throw new Exception(e.Message);
                    }
                }
                else
                {
                    InsertPrivatePolicy(lifePolicyId, isRewards, age, budget, IsFundNotPaysForCompensation, startBudget, oneTimeDeposit, premiumFees, aggregateFees, investmentPlanId, comments);
                    //throw new Exception("פוליסה לא קיימת במערכת");
                }
            }
        }

        public void InsertPensionPolicy(int lifePolicyId, bool? isEmployee, bool? isCompulsoryPension, bool? isShareholder, int age, decimal? budget, decimal? employeeSalary, bool isPersonalPlanType, decimal? startSalary, decimal? compensationToProgram, decimal? employerRewards, decimal? employeeRewards, decimal? otherRewards, decimal? premiumFees, decimal? aggregateFees, int? investmentPlanId, int? fundJoiningPlanId, int? insurancesWaiverId, int? retirementPlanId, int? pensionReceptionAge, int? retirementAgeToPlan, int? EndContributionsPaymentAge, bool? isDesabilityExtension, bool? isIndividualCoverage, bool? isWithChildren, decimal? oldAgePension, decimal? pensionerRest, decimal? disabilityPension, decimal? disabilityPercentage, decimal? widowRests, decimal? widowPercentage, decimal? orphanRestsUntil21, decimal? orphanPercentage, decimal? maximumRests, decimal? parentSupportedPercentage, decimal? revenueValue, string comments)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var policyInContext = context.LifePolicies.FirstOrDefault(p => p.LifePolicyID == lifePolicyId);
                if (policyInContext != null)
                {
                    try
                    {
                        PensionLifeIndustry newPensionPolicy = new PensionLifeIndustry()
                        {
                            LifePolicyID = lifePolicyId,
                            IsEmployee = isEmployee,
                            //IsFundNotPaysForCompensation = isFundNotPaysForCompensation,
                            IsCompulsoryPension = isCompulsoryPension,
                            IsShareholder = isShareholder,
                            Age = age,
                            Budget = budget,
                            EmployeeSalary = employeeSalary,
                            IsPersonalPlanType = isPersonalPlanType,
                            StartSalary = startSalary,
                            CompensationToProgram = compensationToProgram,
                            EmployerRewards = employerRewards,
                            EmployeeRewards = employeeRewards,
                            OtherRewards = otherRewards,
                            PremiumFees = premiumFees,
                            AggregateFees = aggregateFees,
                            InvestmentPlanID = investmentPlanId,
                            FundJoiningPlanID = fundJoiningPlanId,
                            InsurancesWaiverID = insurancesWaiverId,
                            RetirementPlanID = retirementPlanId,
                            PensionReceptionAge = pensionReceptionAge,
                            RetirementAgeToPlan = retirementAgeToPlan,
                            EndContributionsPaymentAge = EndContributionsPaymentAge,
                            IsDesabilityExtension = isDesabilityExtension,
                            IsIndividualCoverage = isIndividualCoverage,
                            IsWithChildren = isWithChildren,
                            OldAgePension = oldAgePension,
                            PensionerRest = pensionerRest,
                            DisabilityPension = disabilityPension,
                            DisabilityPercentage = disabilityPercentage,
                            WidowRests = widowRests,
                            WidowPercentage = widowPercentage,
                            OrphanRestsUntil21 = orphanRestsUntil21,
                            OrphanPercentage = orphanPercentage,
                            MaximumRests = maximumRests,
                            ParentSupportedPercentage = parentSupportedPercentage,
                            RevenueValue = revenueValue,
                            Comments = comments
                        };
                        context.PensionLifeIndustries.Add(newPensionPolicy);
                        context.SaveChanges();
                    }
                    catch (Exception e)
                    {
                        throw new Exception(e.Message);
                    }
                }
                else
                {
                    throw new Exception("פוליסה לא קיימת במערכת");
                }
            }
        }

        public void UpdatePensionPolicy(int lifePolicyId, bool? isEmployee, bool? isCompulsoryPension, bool? isShareholder, int age, decimal? budget, decimal? employeeSalary, bool isPersonalPlanType, decimal? startSalary, decimal? compensationToProgram, decimal? employerRewards, decimal? employeeRewards, decimal? otherRewards, decimal? premiumFees, decimal? aggregateFees, int? investmentPlanId, int? fundJoiningPlanId, int? insurancesWaiverId, int? retirementPlanId, int? pensionReceptionAge, int? retirementAgeToPlan, int? EndContributionsPaymentAge, bool? isDesabilityExtension, bool? isIndividualCoverage, bool? isWithChildren, decimal? oldAgePension, decimal? pensionerRest, decimal? disabilityPension, decimal? disabilityPercentage, decimal? widowRests, decimal? widowPercentage, decimal? orphanRestsUntil21, decimal? orphanPercentage, decimal? maximumRests, decimal? parentSupportedPercentage, decimal? revenueValue, string comments)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var policyInContext = context.PensionLifeIndustries.FirstOrDefault(p => p.LifePolicyID == lifePolicyId);
                if (policyInContext != null)
                {
                    try
                    {
                        policyInContext.IsEmployee = isEmployee;
                        // policyInContext.IsFundNotPaysForCompensation = isFundNotPaysForCompensation;
                        policyInContext.IsCompulsoryPension = isCompulsoryPension;
                        policyInContext.IsShareholder = isShareholder;
                        policyInContext.Age = age;
                        policyInContext.Budget = budget;
                        policyInContext.EmployeeSalary = employeeSalary;
                        policyInContext.IsPersonalPlanType = isPersonalPlanType;
                        policyInContext.StartSalary = startSalary;
                        policyInContext.CompensationToProgram = compensationToProgram;
                        policyInContext.EmployerRewards = employerRewards;
                        policyInContext.EmployeeRewards = employeeRewards;
                        policyInContext.OtherRewards = otherRewards;
                        policyInContext.PremiumFees = premiumFees;
                        policyInContext.AggregateFees = aggregateFees;
                        policyInContext.InvestmentPlanID = investmentPlanId;
                        policyInContext.FundJoiningPlanID = fundJoiningPlanId;
                        policyInContext.InsurancesWaiverID = insurancesWaiverId;
                        policyInContext.RetirementPlanID = retirementPlanId;
                        policyInContext.PensionReceptionAge = pensionReceptionAge;
                        policyInContext.RetirementAgeToPlan = retirementAgeToPlan;
                        policyInContext.EndContributionsPaymentAge = EndContributionsPaymentAge;
                        policyInContext.IsDesabilityExtension = isDesabilityExtension;
                        policyInContext.IsIndividualCoverage = isIndividualCoverage;
                        policyInContext.IsWithChildren = isWithChildren;
                        policyInContext.OldAgePension = oldAgePension;
                        policyInContext.PensionerRest = pensionerRest;
                        policyInContext.DisabilityPension = disabilityPension;
                        policyInContext.DisabilityPercentage = disabilityPercentage;
                        policyInContext.WidowRests = widowRests;
                        policyInContext.WidowPercentage = widowPercentage;
                        policyInContext.OrphanRestsUntil21 = orphanRestsUntil21;
                        policyInContext.OrphanPercentage = orphanPercentage;
                        policyInContext.MaximumRests = maximumRests;
                        policyInContext.ParentSupportedPercentage = parentSupportedPercentage;
                        policyInContext.RevenueValue = revenueValue;
                        policyInContext.Comments = comments;

                        context.SaveChanges();
                    }
                    catch (Exception e)
                    {
                        throw new Exception(e.Message);
                    }
                }
                else
                {
                    InsertPensionPolicy(lifePolicyId, isEmployee, isCompulsoryPension, isShareholder, age, budget, employeeSalary, isPersonalPlanType, startSalary, compensationToProgram, employerRewards, employeeRewards, otherRewards, premiumFees, aggregateFees, investmentPlanId, fundJoiningPlanId, insurancesWaiverId, retirementPlanId, pensionReceptionAge, retirementAgeToPlan, EndContributionsPaymentAge, isDesabilityExtension, isIndividualCoverage, isWithChildren, oldAgePension, pensionerRest, disabilityPension, disabilityPercentage, widowRests, widowPercentage, orphanRestsUntil21, orphanPercentage, maximumRests, parentSupportedPercentage, revenueValue, comments);
                    //throw new Exception("פוליסה לא קיימת במערכת");
                }
            }
        }

        public string GetAdditionNumber(string policyNumber, LifeIndustry industry)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var policiesInContext = context.LifePolicies.Where(p => p.PolicyNumber == policyNumber && p.LifeIndustryID == industry.LifeIndustryID);
                int? addition = 0;
                foreach (var item in policiesInContext)
                {
                    if (item.Addition > addition)
                    {
                        addition = item.Addition;
                    }
                }
                return (addition + 1).ToString();
            }
        }


        public List<LifePolicy> GetActiveLifePoliciesWithoutAdditionsByClient(Client client)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.LifePolicies.Include("LifeIndustry").Where(p => p.ClientID == client.ClientID && p.Addition == 0 && p.IsOffer == false).OrderByDescending(p => p.StartDate).ToList();
            }
        }

        public void ChangeLifePolicyOfferStatus(int lifePolicyID, bool isOffer)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var policyInContext = context.LifePolicies.FirstOrDefault(p => p.LifePolicyID == lifePolicyID);
                policyInContext.IsOffer = isOffer;
                context.SaveChanges();
            }
        }
    }
}



