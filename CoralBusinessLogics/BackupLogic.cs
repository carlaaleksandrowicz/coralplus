﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoralBusinessLogics
{
    public class BackupLogic
    {
        public List<BackupLog> GetAllBackupLogs()
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.BackupLogs.Include("User").OrderByDescending(b => b.BackupDate).ToList();
            }
        }
        public void CreateBackupLog(DateTime date, string zipPath, bool isBackupSucceeded, int userID)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                try
                {
                    context.BackupLogs.Add(new BackupLog {BackupDate=date,BackupPath=zipPath,Status=isBackupSucceeded,UserID=userID });
                    context.SaveChanges();
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
        }
    }
}
