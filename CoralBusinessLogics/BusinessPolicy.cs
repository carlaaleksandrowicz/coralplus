//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CoralBusinessLogics
{
    using System;
    using System.Collections.Generic;
    
    public partial class BusinessPolicy
    {
        public int ElementaryPolicyID { get; set; }
        public string BusinessOwner { get; set; }
        public string BusinessDescription { get; set; }
        public string BusinessName { get; set; }
        public string City { get; set; }
        public string Street { get; set; }
        public string HomeNumber { get; set; }
        public string ApartmentNumber { get; set; }
        public string ZipCode { get; set; }
        public string BusinessPhone { get; set; }
        public string BusinessPhone2 { get; set; }
        public string BusinessFax { get; set; }
        public string BusinessEmail { get; set; }
        public Nullable<int> StructureTypeID { get; set; }
        public string Floor { get; set; }
        public string TotalBuildingFloorsNumber { get; set; }
        public Nullable<decimal> SizeInMeters { get; set; }
        public string RoomsNumber { get; set; }
        public string ContactLastName { get; set; }
        public string ContactName { get; set; }
        public string ContactPhone { get; set; }
        public string ContactCellPhone { get; set; }
        public string ContactFax { get; set; }
        public string ContactEmail { get; set; }
        public Nullable<System.DateTime> SurveyDate { get; set; }
        public Nullable<bool> IsSurveyDone { get; set; }
        public Nullable<int> SurveyByAppraiserID { get; set; }
        public Nullable<bool> InsuranceThisYear { get; set; }
        public Nullable<bool> InsuranceLastYear { get; set; }
        public Nullable<bool> Insurance2YearsAgo { get; set; }
        public Nullable<bool> Insurance3YearsAgo { get; set; }
        public Nullable<int> ClaimsNumberThisYear { get; set; }
        public Nullable<int> ClaimsNumberLastYear { get; set; }
        public Nullable<int> ClaimsNumber2YearsAgo { get; set; }
        public Nullable<int> ClaimsNumber3YearsAgo { get; set; }
        public string InsuranceCompanyThisYear { get; set; }
        public string InsuranceCompanyLastYear { get; set; }
        public string InsuranceCompany2YearsAgo { get; set; }
        public string InsuranceCompany3YearsAgo { get; set; }
    
        public virtual Appraiser Appraiser { get; set; }
        public virtual ElementaryPolicy ElementaryPolicy { get; set; }
        public virtual StructureType StructureType { get; set; }
    }
}
