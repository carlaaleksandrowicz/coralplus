﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoralBusinessLogics
{
    public class HealthClaimsLogic
    {
        //מחזיר רשימה של כל התביעות בריאות לפי לקוח
        public List<HealthClaim> GetAllHealthClaimsByClient(int clientId)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.HealthClaims.Include("HealthPolicy.Client").Include("HealthClaimCondition").Include("HealthClaimType").Include("HealthPolicy").Include("HealthPolicy.Company").Include("HealthPolicy.HealthInsuranceType").Include("HealthPolicy.Insurance").Where(c => c.HealthPolicy.ClientID == clientId).OrderByDescending(c => c.OpenDate).ToList();
            }
        }
        public List<HealthClaimType> GetAllHealthClaimTypes()
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.HealthClaimTypes.OrderByDescending(t=>t.Status).ThenBy(t=>t.ClaimTypeName).ToList();
            }
        }
        public List<HealthClaimType> GetActiveHealthClaimTypes()
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.HealthClaimTypes.Where(t => t.Status == true).OrderBy(t => t.ClaimTypeName).ToList();
            }
        }

        public HealthClaimType InsertHealthClaimType(string claimTypeName, bool status)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var claimTypeInContext = context.HealthClaimTypes.FirstOrDefault(t => t.ClaimTypeName == claimTypeName);
                if (claimTypeInContext == null)
                {
                    try
                    {
                        HealthClaimType newType = new HealthClaimType() { ClaimTypeName = claimTypeName, Status = status };
                        context.HealthClaimTypes.Add(newType);
                        context.SaveChanges();
                        return newType;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }
                else
                {
                    throw new Exception("סוג תביעה קיימת במערכת");
                }
            }
        }

        public HealthClaimType UpdateHealthClaimTypeStatus(string typeName, bool status)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {

                var claimTypeInContext = context.HealthClaimTypes.Where(c => c.ClaimTypeName == typeName).FirstOrDefault();
                if (claimTypeInContext != null)
                {
                    try
                    {
                        claimTypeInContext.Status = status;
                        context.SaveChanges();
                        return claimTypeInContext;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }
                else
                {
                    throw new Exception("סוג תביעה לא קיימת במערכת");
                }
            }
        }


        //מחזיר רשימה של כל מצבי תביעה בריאות
        public List<HealthClaimCondition> GetAllHealthClaimConditions()
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.HealthClaimConditions.OrderByDescending(c=>c.Status).ThenBy(c=>c.Description).ToList();
            }
        }

        //מחזיר רשימה של מצבי תביעה בריאות אקטיביים לפי ענף
        public List<HealthClaimCondition> GetActiveHealthClaimConditions()
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.HealthClaimConditions.Where(c => c.Status == true).OrderBy(c => c.Description).ToList();
            }
        }

        //מוסיף מצב תביעה לבסיס הנתונים
        public HealthClaimCondition InsertHealthClaimCondition(string claimConditionName, bool status)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var claimConditionInContext = context.HealthClaimConditions.FirstOrDefault(c => c.Description == claimConditionName);
                if (claimConditionInContext == null)
                {
                    try
                    {
                        HealthClaimCondition newCondition = new HealthClaimCondition() { Description = claimConditionName, Status = status };
                        context.HealthClaimConditions.Add(newCondition);
                        context.SaveChanges();
                        return newCondition;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }
                else
                {
                    throw new Exception("מצב תביעה קיימת במערכת");
                }
            }
        }

        //מעדכן את הסטטוס של מצב תביעה
        public HealthClaimCondition UpdateHealthClaimConditionStatus(string conditionName, bool status)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var claimConditionInContext = context.HealthClaimConditions.FirstOrDefault(c => c.Description == conditionName);
                if (claimConditionInContext != null)
                {
                    try
                    {
                        claimConditionInContext.Status = status;
                        context.SaveChanges();
                        return claimConditionInContext;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }
                else
                {
                    throw new Exception("סוג תביעה לא קיים במערכת");
                }
            }
        }

        //מוסיף תביעה בריאות לבסיס הנתונים
        public int InsertHealthClaim(int policyId, int? claimTypeId, int? claimConditionId, bool claimStatus, string claimNumber, DateTime? openDate, DateTime? deliveredToCompanyDate, DateTime? moneyRecivedDate, decimal? claimAmount, decimal? amountRecived, DateTime? eventDate, string eventPlace, string eventDescription)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                try
                {
                    HealthClaim newClaim = new HealthClaim()
                    {
                        HealthPolicyID = policyId,
                        HealthClaimTypeID = claimTypeId,
                        HealthClaimConditionID = claimConditionId,
                        ClaimStatus = claimStatus,
                        ClaimNumber = claimNumber,
                        OpenDate = openDate,
                        DeliveredToCompanyDate = deliveredToCompanyDate,
                        MoneyReceivedDate = moneyRecivedDate,
                        ClaimAmount = claimAmount,
                        AmountReceived = amountRecived,
                        EventDateAndTime = eventDate,
                        EventPlace = eventPlace,
                        EventDescription = eventDescription
                    };
                    context.HealthClaims.Add(newClaim);
                    context.SaveChanges();
                    return newClaim.HealthClaimID;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
        }


        //מוסיף עד לתביעה
        public void InsertWitnesses(List<HealthWitness> witnesses, int claimID)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var claimInContext = context.HealthClaims.FirstOrDefault(c => c.HealthClaimID == claimID);
                if (claimInContext != null)
                {
                    try
                    {
                        foreach (var item in witnesses)
                        {
                            if (item.HealthWitnessID == 0)
                            {
                                item.HealthClaimID = claimID;
                                context.HealthWitnesses.Add(item);
                            }
                            else
                            {
                                var witnessInContext = context.HealthWitnesses.FirstOrDefault(w => w.HealthWitnessID == item.HealthWitnessID);
                                if (witnessInContext != null)
                                {
                                    witnessInContext.WitnessCellPhone = item.WitnessCellPhone;
                                    witnessInContext.WitnessName = item.WitnessName;
                                    witnessInContext.WitnessPhone = item.WitnessPhone;
                                    witnessInContext.WittnessAddress = item.WittnessAddress;
                                }
                            }
                        }
                        context.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }
                else
                {
                    throw new Exception("תביעה לא קיימת במערכת");
                }
            }
        }

        //מחזיר רשימה של עדים לפי תביעה
        public List<HealthWitness> GetWitnessesByClaim(int claimId)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.HealthWitnesses.Where(w => w.HealthClaimID == claimId).ToList();
            }
        }


        //מעדכן תביעה בריאות
        public void UpdateHealthClaim(int claimID, int? claimTypeId, int? claimConditionId, bool claimStatus, string claimNumber, DateTime? deliveredToCompanyDate, DateTime? moneyRecivedDate, decimal? claimAmount, decimal? amountRecived, DateTime? eventDateTime, string eventPlace, string eventDescription)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                try
                {
                    var claimInContext = context.HealthClaims.FirstOrDefault(c => c.HealthClaimID == claimID);
                    if (claimInContext != null)
                    {
                        claimInContext.HealthClaimTypeID = claimTypeId;
                        claimInContext.HealthClaimConditionID = claimConditionId;
                        claimInContext.ClaimStatus = claimStatus;
                        claimInContext.ClaimNumber = claimNumber;
                        claimInContext.DeliveredToCompanyDate = deliveredToCompanyDate;
                        claimInContext.MoneyReceivedDate = moneyRecivedDate;
                        claimInContext.ClaimAmount = claimAmount;
                        claimInContext.AmountReceived = amountRecived;
                        claimInContext.EventDateAndTime = eventDateTime;
                        claimInContext.EventPlace = eventPlace;
                        claimInContext.EventDescription = eventDescription;

                        context.SaveChanges();
                    }
                    else
                    {
                        throw new Exception("תביעה לא קיימת במערכת");
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
        }
    }
}

