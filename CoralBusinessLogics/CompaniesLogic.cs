﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoralBusinessLogics
{
    public class CompaniesLogic //: BaseLogics
    {
        //מחזיר רשימה של כל חברות הביטוח
        public List<Company> GetAllCompanies()
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.Companies.OrderBy(c => c.CompanyName).ToList();
            }
        }

        //מוסיף חברת ביטוח
        public void InsertCompany(string companyName)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var companyInContext = context.Companies.FirstOrDefault(c => c.CompanyName == companyName);
                if (companyInContext == null)
                {
                    try
                    {
                        Company newCompany = new Company() { CompanyName = companyName };
                        context.Companies.Add(newCompany);
                        context.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }
                else
                {
                    throw new Exception("חברה קיימת במערכת");
                }
            }
        }

    }
}
