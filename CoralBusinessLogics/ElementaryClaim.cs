//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CoralBusinessLogics
{
    using System;
    using System.Collections.Generic;
    
    public partial class ElementaryClaim
    {
        public ElementaryClaim()
        {
            this.ElementaryClaimTrackings = new HashSet<ElementaryClaimTracking>();
            this.ElementaryWitnesses = new HashSet<ElementaryWitness>();
        }
    
        public int ElementaryClaimID { get; set; }
        public int ElementaryPolicyID { get; set; }
        public Nullable<int> ElementaryClaimTypeID { get; set; }
        public Nullable<int> ElementaryClaimConditionID { get; set; }
        public Nullable<bool> ClaimStatus { get; set; }
        public string ClaimNumber { get; set; }
        public Nullable<System.DateTime> OpenDate { get; set; }
        public string ThirdPartyClaimNumber { get; set; }
        public Nullable<System.DateTime> DeliveredToCompanyDate { get; set; }
        public Nullable<System.DateTime> MoneyReceivedDate { get; set; }
        public Nullable<decimal> ClaimAmount { get; set; }
        public Nullable<decimal> AmountReceived { get; set; }
        public Nullable<System.DateTime> EventDate { get; set; }
        public Nullable<System.TimeSpan> EventTime { get; set; }
        public string EventPlace { get; set; }
        public string EventDescription { get; set; }
        public string AppraiserName { get; set; }
        public string AppraiserAddress { get; set; }
        public string AppraiserPhone { get; set; }
        public string AppraiserCellPhone { get; set; }
        public string AppraiserFax { get; set; }
        public string AppraiserEmail { get; set; }
    
        public virtual ApartmentBusinessClaim ApartmentBusinessClaim { get; set; }
        public virtual CarClaim CarClaim { get; set; }
        public virtual ElementaryClaimCondition ElementaryClaimCondition { get; set; }
        public virtual ElementaryClaimType ElementaryClaimType { get; set; }
        public virtual ElementaryPolicy ElementaryPolicy { get; set; }
        public virtual ICollection<ElementaryClaimTracking> ElementaryClaimTrackings { get; set; }
        public virtual ICollection<ElementaryWitness> ElementaryWitnesses { get; set; }
    }
}
