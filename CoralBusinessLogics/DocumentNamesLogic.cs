﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoralBusinessLogics
{
    public class DocumentNamesLogic
    {
        //מחזיר רשימה של כל שמות הקבצים
        public List<DocumentName> GetAllDocumentNames()
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.DocumentNames.OrderByDescending(d => d.Status).ThenBy(d => d.DocName).ToList();  
            }   
        }

        // מחזיר רשימה של שמות הקבצים האקטיבים
        public List<DocumentName> GetAllActiveDocumentNames()
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.DocumentNames.Where(d => d.Status == true).OrderBy(d => d.DocName).ToList();
            }
        }

        //מוסיף שם קובץ
        public DocumentName InsertDocumentName(string documentName, bool status)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var docNameInContext = context.DocumentNames.FirstOrDefault(d => d.DocName == documentName);
                if (docNameInContext == null)
                {
                    try
                    {
                        DocumentName newDocName = new DocumentName() {DocName=documentName,Status=status };
                        context.DocumentNames.Add(newDocName);
                        context.SaveChanges();
                        return newDocName;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }
                else
                {
                    throw new Exception("שם קובץ קיים במערכת");
                }
            }
        }

        //מעדכן שם קובץ
        public DocumentName UpdateDocumentName(string documentName, bool status)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var docNameInContext = context.DocumentNames.FirstOrDefault(d => d.DocName == documentName);
                if (docNameInContext != null)
                {
                    try
                    {
                        docNameInContext.Status = status;
                        context.SaveChanges();
                        return docNameInContext;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }
                else
                {
                    throw new Exception("שם קובץ לא קיים במערכת");
                }
            }
        }
    }
}
