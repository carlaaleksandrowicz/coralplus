﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoralBusinessLogics
{
    public class FundsLogic
    {
        //מחזיר רשימה של סוגי קופה לפי ביטוח, ענף וחברת ביטוח
        public List<FundType> GetAllFundTypesByInsuranceAndCompany(int insuranceId, int industryId, int companyId)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.FundTypes.Where(f => f.InsuranceID == insuranceId && f.LifeIndustryID == industryId && f.CompanyID == companyId).OrderByDescending(f => f.Status).ThenBy(f => f.FundTypeName).ToList();
            }
        }

        //מחזיר רשימה של סוגי קופה לפי ביטוח, ענף ו
        public List<FundType> GetAllFundTypesByInsuranceAndIndustry(int insuranceId, int industryId)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.FundTypes.Where(f => f.InsuranceID == insuranceId && f.LifeIndustryID == industryId).OrderByDescending(f => f.Status).ThenBy(f => f.FundTypeName).ToList();
            }
        }

        //מחזיר רשימה של סוגי קופה אקטיבים לפי ביטוח, ענף וחברת ביטוח
        public List<FundType> GetActiveFundTypesByInsuranceAndCompany(int insuranceId, int industryId, int companyId)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.FundTypes.Where(f => f.Status == true && f.InsuranceID == insuranceId && f.LifeIndustryID == industryId && f.CompanyID == companyId).OrderBy(f => f.FundTypeName).ToList();
            }
        }

        //מוסיף סוג קופה
        public FundType InsertFundType(int insuranceId, int industryId, int companyId, string fundTypeName, bool status)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var fundTypeNameInContext = context.FundTypes.FirstOrDefault(f => f.FundTypeName == fundTypeName && f.InsuranceID == insuranceId && f.LifeIndustryID == industryId);
                if (fundTypeNameInContext == null)
                {
                    try
                    {
                        FundType newFundType = new FundType() { InsuranceID = insuranceId, LifeIndustryID = industryId, CompanyID = companyId, FundTypeName = fundTypeName, Status = status };
                        context.FundTypes.Add(newFundType);
                        context.SaveChanges();
                        return newFundType;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }
                else
                {
                    throw new Exception("סוג קופה קיים במערכת");
                }
            }
        }

        //מעדכן את הסטטוס של סוג קופה
        public FundType UpdateFundTypeStatus(FundType fundType, bool status)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var fundTypeInContext = context.FundTypes.FirstOrDefault(f => f.FundTypeID == fundType.FundTypeID);
                if (fundTypeInContext != null)
                {
                    try
                    {
                        fundTypeInContext.Status = status;
                        context.SaveChanges();
                        return fundTypeInContext;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }
                else
                {
                    throw new Exception("סוג קופה לא קיים במערכת");
                }
            }
        }

        //מחזיר רשימה של כל תוכניות הצטרפות
        public List<FundJoiningPlan> GetAllJoiningPlans()
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.FundJoiningPlans.OrderByDescending(j => j.Status).ThenBy(j => j.FundJoiningPlanName).ToList();
            }
        }

        //מחזיר רשימה של תוכניות הצטרפות אקטיבים
        public List<FundJoiningPlan> GetActiveJoiningPlans()
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.FundJoiningPlans.Where(j => j.Status == true).OrderBy(i => i.FundJoiningPlanName).ToList();
            }
        }

        //מוסיף תכנית הצטרפות
        public FundJoiningPlan InsertJoiningPlan(string planName, bool status)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var planNameInContext = context.FundJoiningPlans.FirstOrDefault(i => i.FundJoiningPlanName == planName);
                if (planNameInContext == null)
                {
                    try
                    {
                        FundJoiningPlan newPlanName = new FundJoiningPlan() { FundJoiningPlanName = planName, Status = status };
                        context.FundJoiningPlans.Add(newPlanName);
                        context.SaveChanges();
                        return newPlanName;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }
                else
                {
                    throw new Exception("שם מסלול קיים במערכת");
                }
            }
        }

        //מעדכן תכנית הצטרפות
        public FundJoiningPlan UpdateJoiningPlanName(string planName, bool status)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var planNameInContext = context.FundJoiningPlans.FirstOrDefault(i => i.FundJoiningPlanName == planName);
                if (planNameInContext != null)
                {
                    try
                    {
                        planNameInContext.Status = status;
                        context.SaveChanges();
                        return planNameInContext;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }
                else
                {
                    throw new Exception("שם מסלול לא קיים במערכת");
                }
            }
        }

        //מחזיר רשימה של סוגי קופה פיננסים לפי ענף וחברת ביטוח
        public List<FinanceFundType> GetAllFinanceFundTypesByProgramTypeAndCompany(int programTypeId, int companyId)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.FinanceFundTypes.Where(f => f.ProgramTypeID == programTypeId && f.CompanyID == companyId).OrderByDescending(f => f.Status).ThenBy(f => f.FundTypeName).ToList();
            }
        }

        //מחזיר רשימה של סוגי קופה פיננסים אקטיבים לפי ענף וחברת ביטוח
        public List<FinanceFundType> GetActiveFinanceFundTypesByProgramTypeAndCompany(int programTypeId, int companyId)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.FinanceFundTypes.Where(f => f.Status == true && f.ProgramTypeID == programTypeId && f.CompanyID == companyId).OrderBy(f => f.FundTypeName).ToList();
            }
        }

        //מוסיף סוג קופה פיננסית
        public FinanceFundType InsertFinanceFundType(int programTypeId, int companyId, string fundTypeName, bool status)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var fundTypeNameInContext = context.FinanceFundTypes.FirstOrDefault(f => f.FundTypeName == fundTypeName && f.ProgramTypeID == programTypeId);
                if (fundTypeNameInContext == null)
                {
                    try
                    {
                        FinanceFundType newFundType = new FinanceFundType() { ProgramTypeID = programTypeId, CompanyID = companyId, FundTypeName = fundTypeName, Status = status };
                        context.FinanceFundTypes.Add(newFundType);
                        context.SaveChanges();
                        return newFundType;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }
                else
                {
                    throw new Exception("סוג קופה קיים במערכת");
                }
            }
        }

        //מעדכן את הסטטוס של סוג קופה פיננסית
        public FinanceFundType UpdateFinanceFundTypeStatus(FinanceFundType fundType, bool status)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var fundTypeInContext = context.FinanceFundTypes.FirstOrDefault(f => f.FinanceFundTypeID == fundType.FinanceFundTypeID);
                if (fundTypeInContext != null)
                {
                    try
                    {
                        fundTypeInContext.Status = status;
                        context.SaveChanges();
                        return fundTypeInContext;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }
                else
                {
                    throw new Exception("סוג קופה לא קיים במערכת");
                }
            }
        }

    }
}
