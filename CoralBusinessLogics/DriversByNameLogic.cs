﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoralBusinessLogics
{
    //מחלקה העוסקת בנהגים הנקובים בשם בפוליסת רכב
    public class DriversByNameLogic
    {
        //מחזיר רשימה של כל הנהגים הנקובים בשם בפוליסה מסוימת
        public List<DeclaredDriver> GetAllDeclareDriversByPolicy(string policyNumber,int? addition)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var policyInContext = context.CarPolicies.FirstOrDefault(c => c.ElementaryPolicy.PolicyNumber == policyNumber&&c.ElementaryPolicy.Addition==addition);
                if (policyInContext != null)
                {
                    return context.DeclaredDrivers.Where(d => d.ElementaryPolicyID == policyInContext.ElementaryPolicyID).ToList();
                }
                else
                {
                    throw new Exception("פוליסה לא קיימת במערכת");
                }
            }
        }

        //מוסיף נהגים נקובים בשם לפוליסה מסוימת
        public void InsertDeclaredDriversToPolicy(int policyId,string policyNumber, int? addition, List<DeclaredDriver> driversToAdd)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                //var policyInContext = context.CarPolicies.Where(c => c.ElementaryPolicy.PolicyNumber == policyNumber && c.ElementaryPolicy.Addition == addition).FirstOrDefault();
                var policyInContext = context.CarPolicies.FirstOrDefault(c => c.ElementaryPolicyID == policyId);

                if (policyInContext != null)
                {
                    try
                    {
                        foreach (DeclaredDriver driver in driversToAdd)
                        {                            
                            if (driver.DeclaredDriverID != 0)
                            {
                                DeclaredDriver newDriver = new DeclaredDriver() { BirthDate = driver.BirthDate,ElementaryPolicyID=policyInContext.ElementaryPolicyID, DriverFirstName = driver.DriverFirstName, DriverIdNumber = driver.DriverIdNumber, DriverLastName = driver.DriverLastName, LicenseDate = driver.LicenseDate, LicenseNumber = driver.LicenseNumber, Remarks = driver.Remarks };
                                context.DeclaredDrivers.Add(newDriver);
                            }
                            else
                            {
                                driver.ElementaryPolicyID = policyInContext.ElementaryPolicyID;
                                context.DeclaredDrivers.Add(driver);
                            }
                        }
                        context.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }
                else
                {
                    throw new Exception("פוליסה לא קיימת במערכת");
                }
            }
        }

        //מעדכן נהגים נקובים בשם לפוליסה מסוימת
        public void UpdateDeclaredDriversToPolicy(int policyId,string policyNumber, int? addition, List<DeclaredDriver> newDriversList, List<DeclaredDriver> driversToAdd)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                //List<DeclaredDriver> oldDriversList = context.DeclaredDrivers.Where(d => d.CarPolicy.ElementaryPolicy.PolicyNumber == policyNumber && d.CarPolicy.ElementaryPolicy.Addition == addition).ToList();
                //List<DeclaredDriver> driversToDelete = context.DeclaredDrivers.Where(d => d.CarPolicy.ElementaryPolicy.PolicyNumber == policyNumber && d.CarPolicy.ElementaryPolicy.Addition == addition).ToList();
                List<DeclaredDriver> oldDriversList = context.DeclaredDrivers.Where(d => d.ElementaryPolicyID == policyId).ToList();
                List<DeclaredDriver> driversToDelete = context.DeclaredDrivers.Where(d => d.ElementaryPolicyID == policyId).ToList();

                foreach (DeclaredDriver oldDriver in oldDriversList)
                {
                    foreach (var newDriver in newDriversList)
                    {
                        if (oldDriver.DriverIdNumber == newDriver.DriverIdNumber)
                        {
                            oldDriver.DriverFirstName = newDriver.DriverFirstName;
                            oldDriver.DriverLastName = newDriver.DriverLastName;
                            oldDriver.BirthDate = newDriver.BirthDate;
                            oldDriver.LicenseNumber = newDriver.LicenseNumber;
                            oldDriver.LicenseDate = newDriver.LicenseDate;
                            oldDriver.Remarks = newDriver.Remarks;
                            context.SaveChanges();
                            driversToDelete.Remove(oldDriver);
                            driversToAdd.Remove(newDriver);
                            break;
                        }
                    }
                }
                InsertDeclaredDriversToPolicy(policyId,policyNumber, addition, driversToAdd);
                foreach (DeclaredDriver item in driversToDelete)
                {
                    context.DeclaredDrivers.Remove(item);
                }
                context.SaveChanges();
            }
        }

    }
}
