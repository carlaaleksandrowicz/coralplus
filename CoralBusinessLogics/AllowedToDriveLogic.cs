﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoralBusinessLogics
{
    //מחלקה העוסקת בנהגים הרשאים לנהוג בפוליסת רכב
    public class AllowedToDriveLogic
    {
        //מחזיר רשימה של כל הסוגים של נהגים הרשאים לנהוג רכב
        public List<AllowedToDrive> GetAllAllowedToDrive()
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.AllowedToDrives.OrderByDescending(a=>a.Status).ThenBy(a => a.Description).ToList();
            }
        }

        //מחזיר רשימה של כל הסוגים הפעילים של נהגים הרשאים לנהוג רכב
        public List<AllowedToDrive> GetActiveAllowedToDrive()
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.AllowedToDrives.Where(a => a.Status==true).OrderBy(a => a.Description).ToList();
            }
        }

        //מוסיף סוג של נהג שרשאי לנהוג
        public AllowedToDrive InsertAllowedToDrive(string description, bool status)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var allowedToDriveInContext = context.AllowedToDrives.FirstOrDefault(a => a.Description == description);
                if (allowedToDriveInContext == null)
                {
                    try
                    {
                        AllowedToDrive newAllowedToDrive = new AllowedToDrive() { Description = description, Status=status};
                        context.AllowedToDrives.Add(newAllowedToDrive);
                        context.SaveChanges();
                        return newAllowedToDrive;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }
                else
                {
                    throw new Exception("רשאי לנהוג קיים במערכת");
                }
            }
        }

        //מעדכן סוג של נהג שרשאי לנהוג
        public AllowedToDrive UpdateAllowedToDrive(string description, bool status)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var allowedToDriveInContext = context.AllowedToDrives.FirstOrDefault(a => a.Description == description);
                if (allowedToDriveInContext != null)
                {
                    try
                    {
                        allowedToDriveInContext.Status = status;                        
                        context.SaveChanges();
                        return allowedToDriveInContext;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }
                else
                {
                    throw new Exception("רשאי לנהוג לא קיים במערכת");
                }
            }
        }

        //מחזיר ראשאי לנהוג לפי מס' זיהוי בבסיס הנתונים
        public AllowedToDrive GetAllowToDriveById(int allowToDriveId)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.AllowedToDrives.FirstOrDefault(a => a.AllowedToDriveID == allowToDriveId);
            }

        }

     }
}
