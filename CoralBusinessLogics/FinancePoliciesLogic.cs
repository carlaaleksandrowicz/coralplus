﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoralBusinessLogics
{
    public class FinancePoliciesLogic
    {
        //מחזיר רשימה של כל הפוליסות פיננסים
        public List<FinancePolicy> GetAllFinancePoliciesWithoutOffers()
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.FinancePolicies.Include("Client").Include("Client.Agent").Include("Client.Agent1").Include("Insurance").Include("Company").Include("FinanceProgram").Include("FinanceProgramType").Where(p=>p.IsOffer==false).OrderByDescending(p => p.StartDate).ThenBy(p => p.Client.LastName).ToList();
            }
        }

        //מחזיר רשימה של פוליסות פיננסים של לקוח מסוים
        public List<FinancePolicy> GetAllFinancePoliciesByClient(Client client)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                if (client != null)
                {
                    var policies = context.FinancePolicies.Where(p => p.ClientID == client.ClientID&&p.IsOffer==false).OrderByDescending(p => p.OpenDate).ToList();
                    var sortedPolicies = context.FinancePolicies.Include("Insurance").Include("Client").Include("Company").Include("FinanceProgram").Include("FinanceProgramType").Where(p => p.ClientID == client.ClientID && p.IsOffer == false).OrderByDescending(p => p.OpenDate).ToList();
                    for (int i = policies.Count - 1; i >= 0; i--)
                    {
                        if (policies[i].Addition > 0)
                        {
                            var policy = policies.Where(p => p.AssociateNumber == policies[i].AssociateNumber && p.Addition == policies[i].Addition - 1).FirstOrDefault();
                            if (policy != null)
                            {
                                sortedPolicies.Remove(policies[i]);
                                var index = sortedPolicies.FindIndex(p => p.AssociateNumber == policy.AssociateNumber && p.Addition == policy.Addition);
                                sortedPolicies.Insert(index, policies[i]);
                            }
                        }
                    }
                    return sortedPolicies;
                }
                else
                {
                    return null;
                }
            }
        }

        public List<FinancePolicy> GetPoliciesWithoutOffersByStartDate(DateTime fromDate, DateTime? toDate)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.FinancePolicies.Include("Client").Include("Client.Agent").Include("Client.Agent1").Include("Insurance").Include("Company").Include("FinanceProgram").Include("FinanceProgramType").Where(p => p.IsOffer == false && p.StartDate >= fromDate && p.StartDate <= toDate).OrderByDescending(p => p.StartDate).ThenBy(p => p.Client.LastName).ToList();
            }
        }

        //מוסיף פוליסת פיננסים חדשה
        public int InsertFinancePolicy(int clientId, bool isOffer, int programTypeId, string associateNumber, int addition, DateTime openDate, DateTime? startDate, DateTime? endDate, int? companyId, int? principalAgentNumberId, int? subAgentNumberId, int programId, int? policyOwnerId, int? factoryNumberId, string paymentMethod, string comments, decimal? admFeesEmployee,decimal? workerDeposit, DateTime? startDateIndependent, DateTime? startDateEmployee, decimal? reportedSalary, string policyOwnerPhone, string policyOwnerIdNumber, string policyOwnerFax, string policyOwnerEmail, string policyOwnerCellphone, string policyOwnerAddress, string managerName, decimal? employerDeposit, decimal? independentDepositAmount, decimal? employeeDepositAmount, decimal? compensation, string associateStatus,decimal? aggregateFeesIndependent, decimal? aggregateFeesEmployee,decimal? admFeesIndependent)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var policyInContext = context.FinancePolicies.FirstOrDefault(p => p.AssociateNumber == associateNumber && p.ProgramTypeID == programTypeId && p.Addition == addition);
                if (policyInContext == null)
                {
                    try
                    {
                        var insuranceInContext = context.Insurances.FirstOrDefault(i => i.InsuranceName == "פיננסים");
                        FinancePolicy newFinancePolicy = new FinancePolicy()
                        {
                            InsuranceID = insuranceInContext.InsuranceID,
                            ClientID = clientId,
                            IsOffer = isOffer,
                            ProgramTypeID = programTypeId,
                            AssociateNumber = associateNumber,
                            Addition = addition,
                            OpenDate = openDate,
                            StartDate=startDate,
                            EndDate=endDate,
                            CompanyID = companyId,
                            PrincipalAgentNumberID = principalAgentNumberId,
                            SubAgentNumberID = subAgentNumberId,
                            ProgramID = programId,                           
                            PolicyOwnerID = policyOwnerId,
                            FactoryNumberID = factoryNumberId,
                            PaymentMethod = paymentMethod,                            
                            Comments = comments,
                            AdministrationFeesEmployee=admFeesEmployee,
                            AdministrationFeesIndependent=admFeesIndependent,
                            AggregateFeesEmployee=aggregateFeesEmployee,
                            AggregateFeesIndependent=aggregateFeesIndependent,
                            AssociateStatus=associateStatus,
                            Compansation=compensation,
                            DepositAmountIndependent=independentDepositAmount,
                            DepositAmountEmployee=employeeDepositAmount,
                            EmployerDeposit=employerDeposit,
                            ManagerName=managerName,
                            PolicyOwnerAddress=policyOwnerAddress,
                            PolicyOwnerCellphone=policyOwnerCellphone,
                            PolicyOwnerEmail=policyOwnerEmail,
                            PolicyOwnerFax=policyOwnerFax,
                            PolicyOwnerIdNumber=policyOwnerIdNumber,
                            PolicyOwnerPhone=policyOwnerPhone,
                            ReportedSalary=reportedSalary,
                            StartDateEmployee=startDateEmployee,
                            StartDateIndependent=startDateIndependent,
                            WorkerDeposit=workerDeposit                           
                        };
                        context.FinancePolicies.Add(newFinancePolicy);
                        context.SaveChanges();
                        return newFinancePolicy.FinancePolicyID;
                    }
                    catch (Exception e)
                    {

                        throw new Exception(e.Message);
                    }
                }
                else
                {
                    throw new Exception("פוליסה קיימת במערכת");
                }
            }
        }

        public void TransferPolicyToClient(FinancePolicy policy, Client client)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var policyInContext = context.FinancePolicies.FirstOrDefault(p => p.FinancePolicyID == policy.FinancePolicyID);
                if (policyInContext != null)
                {
                    policyInContext.ClientID = client.ClientID;
                    context.SaveChanges();
                }
                else
                {
                    throw new Exception("פוליסה לא קיימת במערכת");
                }
            }
        }

        public List<FinancePolicy> GetActivePoliciesWithoutOffers()
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.FinancePolicies.Include("Client").Include("Client.Agent").Include("Client.Agent1").Include("Insurance").Include("Company").Include("FinanceProgram").Include("FinanceProgramType").Where(p => p.IsOffer == false&&p.EndDate>=DateTime.Today).OrderByDescending(p => p.StartDate).ThenBy(p => p.Client.LastName).ToList();
            }

        }

        public List<FinancePolicy> GetOffers()
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.FinancePolicies.Include("Client").Include("Company").Include("FinanceProgram").Include("FinanceProgramType").Where(p => p.IsOffer == true).OrderByDescending(p => p.StartDate).ThenBy(p => p.Client.LastName).ToList();
            }
        }

        public List<FinancePolicy> GetAllFinanceOffersByClient(Client client)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                if (client == null)
                {
                    return null;
                }
                return context.FinancePolicies.Include("Client").Include("Insurance").Include("Company").Include("FinanceProgramType").Include("FinanceProgram").Where(p => p.ClientID == client.ClientID && p.IsOffer == true).OrderByDescending(p => p.StartDate).ToList();
            }
        }

        public List<FinancePolicy> GetPoliciesByOwner(PolicyOwner owner)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.FinancePolicies.Include("Client").Include("Client.Agent").Include("Client.Agent1").Include("Insurance").Include("Company").Include("FinanceProgramType").Include("FinanceProgram").Where(p => p.IsOffer == false && p.PolicyOwnerID == owner.PolicyOwnerID).OrderByDescending(p => p.StartDate).ThenBy(p => p.Client.LastName).ToList();
            }
        }

        //מעדכן פוליסת פיננסים קיימת
        public void UpdateFinancePolicy(int financePolicyId, string associateNumber, FinanceProgramType programType, DateTime? startDate, DateTime? endDate, int? companyId, int? principalAgentNumberId, int? subAgentNumberId, int programId, int? policyOwnerId, int? factoryNumberId, string paymentMethod, string comments, decimal? admFeesEmployee,decimal? workerDeposit, DateTime? startDateIndependent, DateTime? startDateEmployee, decimal? reportedSalary, string policyOwnerPhone, string policyOwnerIdNumber, string policyOwnerFax, string policyOwnerEmail, string policyOwnerCellphone, string policyOwnerAddress, string managerName, decimal? employerDeposit, decimal? independentDepositAmount, decimal? employeeDepositAmount, decimal? compensation, string associateStatus,decimal? aggregateFeesIndependent, decimal? aggregateFeesEmployee,decimal? admFeesIndependent)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var policyInContext = context.FinancePolicies.FirstOrDefault(p => p.FinancePolicyID == financePolicyId);
                if (policyInContext != null)
                {
                    try
                    {                        
                        policyInContext.AssociateNumber = associateNumber;
                        policyInContext.ProgramTypeID = programType.ProgramTypeID;                 
                        policyInContext.CompanyID = companyId;
                        policyInContext.PrincipalAgentNumberID = principalAgentNumberId;
                        policyInContext.SubAgentNumberID = subAgentNumberId;
                        policyInContext.ProgramID = programId;
                        policyInContext.PolicyOwnerID = policyOwnerId;
                        policyInContext.FactoryNumberID = factoryNumberId;
                        policyInContext.PaymentMethod = paymentMethod;
                        policyInContext.Comments = comments;
                        policyInContext.AdministrationFeesEmployee=admFeesEmployee;
                        policyInContext.AdministrationFeesIndependent=admFeesIndependent;
                        policyInContext.AggregateFeesEmployee=aggregateFeesEmployee;
                        policyInContext.AggregateFeesIndependent=aggregateFeesIndependent;
                        policyInContext.AssociateStatus=associateStatus;
                        policyInContext.Compansation=compensation;
                        policyInContext.DepositAmountIndependent=independentDepositAmount;
                        policyInContext.EmployerDeposit=employerDeposit;
                        policyInContext.ManagerName=managerName;
                        policyInContext.PolicyOwnerAddress=policyOwnerAddress;
                        policyInContext.PolicyOwnerCellphone=policyOwnerCellphone;
                        policyInContext.PolicyOwnerEmail=policyOwnerEmail;
                        policyInContext.PolicyOwnerFax=policyOwnerFax;
                        policyInContext.PolicyOwnerIdNumber=policyOwnerIdNumber;
                        policyInContext.PolicyOwnerPhone=policyOwnerPhone;
                        policyInContext.ReportedSalary=reportedSalary;
                        policyInContext.StartDateEmployee=startDateEmployee;
                        policyInContext.StartDateIndependent=startDateIndependent;
                        policyInContext.WorkerDeposit=workerDeposit;
                        policyInContext.StartDate = startDate;
                        policyInContext.EndDate = endDate;
                        policyInContext.DepositAmountEmployee = employeeDepositAmount;    
                        context.SaveChanges();                        
                    }
                    catch (Exception e)
                    {

                        throw new Exception(e.Message);
                    }
                }
                else
                {
                    throw new Exception("פוליסה לא קיימת במערכת");
                }
            }
        }

        public void InsertPolicyTransfer(int policyId, List<FinancePolicyTransfer> transfers, bool isAddition)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var policyInContext = context.FinancePolicies.FirstOrDefault(p => p.FinancePolicyID == policyId);
                if (policyInContext != null)
                {
                    try
                    {
                        var policyTransfersInContext = context.FinancePolicyTransfers.Where(t => t.FinancePolicyID == policyId).ToList();
                        foreach (var item in transfers)
                        {
                            if (item.FinancePolicyTransferID == 0)
                            {
                                item.FinancePolicyID = policyId;
                                item.Company = null;
                                item.Company1 = null;
                                item.FinanceFundType = null;
                                item.FinanceFundType1 = null;
                                context.FinancePolicyTransfers.Add(item);
                            }
                            else if (isAddition)
                            {
                                FinancePolicyTransfer newTransfer = new FinancePolicyTransfer()
                                {
                                    Company = null,
                                    Company1 = null,
                                    FinanceFundType = null,
                                    FinanceFundType1 = null,
                                    FinanceFundTypeID = item.FinanceFundTypeID,
                                    FinancePolicyID = policyId,
                                    TransferAmount = item.TransferAmount,
                                    TransferDate = item.TransferDate,
                                    TransferedToCompanyID = item.TransferedToCompanyID,
                                    TransferedToFinanceFundTypeID = item.TransferedToFinanceFundTypeID,
                                    TransferringCompanyID = item.TransferringCompanyID
                                };
                                context.FinancePolicyTransfers.Add(newTransfer);
                            }
                            else
                            {
                                var transferInContext = context.FinancePolicyTransfers.FirstOrDefault(t => t.FinancePolicyTransferID == item.FinancePolicyTransferID);
                                if (transferInContext != null)
                                {
                                    transferInContext.FinanceFundTypeID = item.FinanceFundTypeID;
                                    transferInContext.TransferAmount = item.TransferAmount;
                                    transferInContext.TransferDate = item.TransferDate;
                                    transferInContext.TransferedToCompanyID = item.TransferedToCompanyID;
                                    transferInContext.TransferedToFinanceFundTypeID = item.TransferedToFinanceFundTypeID;
                                    transferInContext.TransferringCompanyID = item.TransferringCompanyID;
                                    policyTransfersInContext.Remove(transferInContext);
                                }

                            }
                        }
                        if (policyTransfersInContext.Count > 0)
                        {
                            foreach (var item in policyTransfersInContext)
                            {
                                context.FinancePolicyTransfers.Remove(item);
                            }
                        }
                        context.SaveChanges();
                    }
                    catch (Exception e)
                    {
                        throw new Exception(e.Message);
                    }
                }
                else
                {
                    throw new Exception("פוליסה לא קיימת במערכת");
                }
            }
        }

        public List<FinancePolicy> GetActiveFinancePoliciesWithoutAdditionByClient(Client client)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.FinancePolicies.Include("Insurance").Where(p => p.ClientID == client.ClientID && p.Addition == 0 && p.IsOffer == false).OrderByDescending(p => p.StartDate).ToList();
            }
        }

        public List<FinancePolicyTransfer> GetTransfersByPolicy(int policyId)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                try
                {
                    return context.FinancePolicyTransfers.Include("Company").Include("Company1").Include("FinanceFundType").Include("FinanceFundType1").Where(t => t.FinancePolicyID == policyId).ToList();
                }
                catch (Exception e)
                {
                    throw new Exception(e.Message);
                }
            }
        }


        public List<FinancePolicyMonthlyDeposit> GetMonthlyDepositsByPolicy(int policyId)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.FinancePolicyMonthlyDeposits.Where(d => d.FinancePolicyID == policyId).ToList();
            }
        }

        public void InsertMonthlyDeposit(int policyId, List<FinancePolicyMonthlyDeposit> deposits, bool isAddition)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var policyInContext = context.FinancePolicies.FirstOrDefault(p => p.FinancePolicyID == policyId);
                if (policyInContext != null)
                {
                    try
                    {
                        var policyDepositsInContext = context.FinancePolicyMonthlyDeposits.Where(t => t.FinancePolicyID == policyId).ToList();
                        foreach (var item in deposits)
                        {
                            if (item.FinancePolicyMonthlyDepositID == 0)
                            {
                                item.FinancePolicyID = policyId;                                
                                context.FinancePolicyMonthlyDeposits.Add(item);
                            }
                            //else if (isAddition)
                            //{
                            //    FinancePolicyTransfer newTransfer = new FinancePolicyTransfer()
                            //    {
                            //        Company = null,
                            //        Company1 = null,
                            //        FinanceFundType = null,
                            //        FinanceFundType1 = null,
                            //        FinanceFundTypeID = item.FinanceFundTypeID,
                            //        FinancePolicyID = policyId,
                            //        TransferAmount = item.TransferAmount,
                            //        TransferDate = item.TransferDate,
                            //        TransferedToCompanyID = item.TransferedToCompanyID,
                            //        TransferedToFinanceFundTypeID = item.TransferedToFinanceFundTypeID,
                            //        TransferringCompanyID = item.TransferringCompanyID
                            //    };
                            //    context.FinancePolicyTransfers.Add(newTransfer);
                            //}
                            else
                            {
                                var depositInContext = context.FinancePolicyMonthlyDeposits.FirstOrDefault(t => t.FinancePolicyMonthlyDepositID == item.FinancePolicyMonthlyDepositID);
                                if (depositInContext != null)
                                {
                                    depositInContext.Comments = item.Comments;
                                    depositInContext.DepositAmount = item.DepositAmount;
                                    depositInContext.DepositDate = item.DepositDate;
                                    depositInContext.DepositForTheMonth = item.DepositForTheMonth;
                                    depositInContext.TransferredTo = item.TransferredTo;                                    
                                    policyDepositsInContext.Remove(depositInContext);
                                }
                            }
                        }
                        if (policyDepositsInContext.Count > 0)
                        {
                            foreach (var item in policyDepositsInContext)
                            {
                                context.FinancePolicyMonthlyDeposits.Remove(item);
                            }
                        }
                        context.SaveChanges();
                    }
                    catch (Exception e)
                    {
                        throw new Exception(e.Message);
                    }
                }
                else
                {
                    throw new Exception("פוליסה לא קיימת במערכת");
                }
            }
        }

        public List<FinancePolicy> GetAllActiveFinancePoliciesByClient(Client client)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                if (client != null)
                {
                    var policies = context.FinancePolicies.Where(p => p.ClientID == client.ClientID && p.IsOffer == false&&p.EndDate>=DateTime.Today).OrderByDescending(p => p.OpenDate).ToList();
                    var sortedPolicies = context.FinancePolicies.Include("Insurance").Include("Client").Include("Company").Include("FinanceProgram").Include("FinanceProgramType").Where(p => p.ClientID == client.ClientID && p.IsOffer == false && p.EndDate >= DateTime.Today).OrderByDescending(p => p.OpenDate).ToList();
                    for (int i = policies.Count - 1; i >= 0; i--)
                    {
                        if (policies[i].Addition > 0)
                        {
                            var policy = policies.Where(p => p.AssociateNumber == policies[i].AssociateNumber && p.Addition == policies[i].Addition - 1).FirstOrDefault();
                            if (policy != null)
                            {
                                sortedPolicies.Remove(policies[i]);
                                var index = sortedPolicies.FindIndex(p => p.AssociateNumber == policy.AssociateNumber && p.Addition == policy.Addition);
                                sortedPolicies.Insert(index, policies[i]);
                            }
                        }
                    }
                    return sortedPolicies;
                }
                else
                {
                    return null;
                }
            }
        }

        public IEnumerable GetNonActiveFinancePoliciesByClient(Client client)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                if (client != null)
                {
                    var policies = context.FinancePolicies.Where(p => p.ClientID == client.ClientID && p.IsOffer == false && p.EndDate < DateTime.Today).OrderByDescending(p => p.OpenDate).ToList();
                    var sortedPolicies = context.FinancePolicies.Include("Insurance").Include("Client").Include("Company").Include("FinanceProgram").Include("FinanceProgramType").Where(p => p.ClientID == client.ClientID && p.IsOffer == false && p.EndDate < DateTime.Today).OrderByDescending(p => p.OpenDate).ToList();
                    for (int i = policies.Count - 1; i >= 0; i--)
                    {
                        if (policies[i].Addition > 0)
                        {
                            var policy = policies.Where(p => p.AssociateNumber == policies[i].AssociateNumber && p.Addition == policies[i].Addition - 1).FirstOrDefault();
                            if (policy != null)
                            {
                                sortedPolicies.Remove(policies[i]);
                                var index = sortedPolicies.FindIndex(p => p.AssociateNumber == policy.AssociateNumber && p.Addition == policy.Addition);
                                sortedPolicies.Insert(index, policies[i]);
                            }
                        }
                    }
                    return sortedPolicies;
                }
                else
                {
                    return null;
                }
            }
        }

        public void ChangeFinancePolicyOfferStatus(int financePolicyID, bool isOffer)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var policyInContext = context.FinancePolicies.FirstOrDefault(p => p.FinancePolicyID == financePolicyID);
                policyInContext.IsOffer = isOffer;
                context.SaveChanges();
            }
        }

        public FinancePolicy GetFinancePolicyByPolicyId(int policyId)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.FinancePolicies.Include("FinanceProgramType").Include("Client").FirstOrDefault(p => p.FinancePolicyID == policyId);
            }
        }
    }
}
