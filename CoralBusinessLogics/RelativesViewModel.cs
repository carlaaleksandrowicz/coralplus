﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoralBusinessLogics
{
    //מאפיינים של בני משפחה להצגה בטבךת בני המשפחה של לקוח
    public class RelativeViewModel
    {
        public string Relationship { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string IdNumber { get; set; }
        public DateTime? BirthDate { get; set; }
        public string LicenseNumber { get; set; }
        public DateTime? LicenseDate { get; set; }
        public string Profession { get; set; }
        public string Gender { get; set; }
        public string PassportNumber { get; set; }
    }
}
