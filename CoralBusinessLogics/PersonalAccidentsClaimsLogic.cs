﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoralBusinessLogics
{
    public class PersonalAccidentsClaimsLogic
    {
        //מחזיר רשימה של כל התביעות חיים לפי לקוח
        public List<PersonalAccidentsClaim> GetAllPersonalAccidentsClaimsByClient(int clientId)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.PersonalAccidentsClaims.Include("PersonalAccidentsPolicy.Client").Include("PersonalAccidentsClaimCondition").Include("PersonalAccidentsClaimType").Include("PersonalAccidentsPolicy").Include("PersonalAccidentsPolicy.Company").Include("PersonalAccidentsPolicy.Insurance").Where(c => c.PersonalAccidentsPolicy.ClientID == clientId).OrderByDescending(c => c.OpenDate).ToList();
            }
        }
        public List<PersonalAccidentsClaimType> GetAllPersonalAccidentsClaimTypes()
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.PersonalAccidentsClaimTypes.OrderByDescending(t => t.Status).ThenBy(t => t.ClaimTypeName).ToList();
            }
        }
        public List<PersonalAccidentsClaimType> GetActivePersonalAccidentsClaimTypes()
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.PersonalAccidentsClaimTypes.Where(t => t.Status == true).OrderBy(t => t.ClaimTypeName).ToList();
            }
        }

        public PersonalAccidentsClaimType InsertPersonalAccidentsClaimType(string claimTypeName, bool status)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var claimTypeInContext = context.PersonalAccidentsClaimTypes.FirstOrDefault(t => t.ClaimTypeName == claimTypeName);
                if (claimTypeInContext == null)
                {
                    try
                    {
                        PersonalAccidentsClaimType newType = new PersonalAccidentsClaimType() { ClaimTypeName = claimTypeName, Status = status };
                        context.PersonalAccidentsClaimTypes.Add(newType);
                        context.SaveChanges();
                        return newType;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }
                else
                {
                    throw new Exception("סוג תביעה קיימת במערכת");
                }
            }
        }

        public PersonalAccidentsClaimType UpdatePersonalAccidentsClaimTypeStatus(string typeName, bool status)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {

                var claimTypeInContext = context.PersonalAccidentsClaimTypes.Where(c => c.ClaimTypeName == typeName).FirstOrDefault();
                if (claimTypeInContext != null)
                {
                    try
                    {
                        claimTypeInContext.Status = status;
                        context.SaveChanges();
                        return claimTypeInContext;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }
                else
                {
                    throw new Exception("סוג תביעה לא קיימת במערכת");
                }
            }
        }


        //מחזיר רשימה של כל מצבי תביעה חיים
        public List<PersonalAccidentsClaimCondition> GetAllPersonalAccidentsClaimConditions()
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.PersonalAccidentsClaimConditions.OrderByDescending(c => c.Status).ThenBy(c => c.Description).ToList();
            }
        }

        //מחזיר רשימה של מצבי תביעה חיים אקטיביים לפי ענף
        public List<PersonalAccidentsClaimCondition> GetActivePersonalAccidentsClaimConditions()
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.PersonalAccidentsClaimConditions.Where(c => c.Status == true).OrderBy(c => c.Description).ToList();
            }
        }

        //מוסיף מצב תביעה לבסיס הנתונים
        public PersonalAccidentsClaimCondition InsertPersonalAccidentsClaimCondition(string claimConditionName, bool status)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var claimConditionInContext = context.PersonalAccidentsClaimConditions.FirstOrDefault(c => c.Description == claimConditionName);
                if (claimConditionInContext == null)
                {
                    try
                    {
                        PersonalAccidentsClaimCondition newCondition = new PersonalAccidentsClaimCondition() { Description = claimConditionName, Status = status };
                        context.PersonalAccidentsClaimConditions.Add(newCondition);
                        context.SaveChanges();
                        return newCondition;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }
                else
                {
                    throw new Exception("מצב תביעה קיימת במערכת");
                }
            }
        }

        //מעדכן את הסטטוס של מצב תביעה
        public PersonalAccidentsClaimCondition UpdatePersonalAccidentsClaimConditionStatus(string conditionName, bool status)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var claimConditionInContext = context.PersonalAccidentsClaimConditions.FirstOrDefault(c => c.Description == conditionName);
                if (claimConditionInContext != null)
                {
                    try
                    {
                        claimConditionInContext.Status = status;
                        context.SaveChanges();
                        return claimConditionInContext;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }
                else
                {
                    throw new Exception("סוג תביעה לא קיים במערכת");
                }
            }
        }

        //מוסיף תביעה חיים לבסיס הנתונים
        public int InsertPersonalAccidentsClaim(int policyId, int? claimTypeId, int? claimConditionId, bool claimStatus, string claimNumber, DateTime? openDate, DateTime? deliveredToCompanyDate, DateTime? moneyRecivedDate, decimal? claimAmount, decimal? amountRecived, DateTime? eventDate, string eventPlace, string eventDescription)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                try
                {
                    PersonalAccidentsClaim newClaim = new PersonalAccidentsClaim()
                    {
                        PersonalAccidentsPolicyID = policyId,
                        PersonalAccidentsClaimTypeID = claimTypeId,
                        PersonalAccidentsClaimConditionID = claimConditionId,
                        ClaimStatus = claimStatus,
                        ClaimNumber = claimNumber,
                        OpenDate = openDate,
                        DeliveredToCompanyDate = deliveredToCompanyDate,
                        MoneyReceivedDate = moneyRecivedDate,
                        ClaimAmount = claimAmount,
                        AmountReceived = amountRecived,
                        EventDateAndTime = eventDate,
                        EventPlace = eventPlace,
                        EventDescription = eventDescription
                    };
                    context.PersonalAccidentsClaims.Add(newClaim);
                    context.SaveChanges();
                    return newClaim.PersonalAccidentsClaimID;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
        }


        //מוסיף עד לתביעה
        public void InsertWitnesses(List<PersonalAccidentsWitness> witnesses, int claimID)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var claimInContext = context.PersonalAccidentsClaims.FirstOrDefault(c => c.PersonalAccidentsClaimID == claimID);
                if (claimInContext != null)
                {
                    try
                    {
                        foreach (var item in witnesses)
                        {
                            if (item.PersonalAccidentsWitnessID == 0)
                            {
                                item.PersonalAccidentsClaimID = claimID;
                                context.PersonalAccidentsWitnesses.Add(item);
                            }
                            else
                            {
                                var witnessInContext = context.PersonalAccidentsWitnesses.FirstOrDefault(w => w.PersonalAccidentsWitnessID == item.PersonalAccidentsWitnessID);
                                if (witnessInContext != null)
                                {
                                    witnessInContext.WitnessCellPhone = item.WitnessCellPhone;
                                    witnessInContext.WitnessName = item.WitnessName;
                                    witnessInContext.WitnessPhone = item.WitnessPhone;
                                    witnessInContext.WittnessAddress = item.WittnessAddress;
                                }
                            }
                        }
                        context.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }
                else
                {
                    throw new Exception("תביעה לא קיימת במערכת");
                }
            }
        }

        //מחזיר רשימה של עדים לפי תביעה
        public List<PersonalAccidentsWitness> GetWitnessesByClaim(int claimId)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.PersonalAccidentsWitnesses.Where(w => w.PersonalAccidentsClaimID == claimId).ToList();
            }
        }


        //מעדכן תביעה חיים
        public void UpdatePersonalAccidentsClaim(int claimID, int? claimTypeId, int? claimConditionId, bool claimStatus, string claimNumber, DateTime? deliveredToCompanyDate, DateTime? moneyRecivedDate, decimal? claimAmount, decimal? amountRecived, DateTime? eventDateTime, string eventPlace, string eventDescription)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                try
                {
                    var claimInContext = context.PersonalAccidentsClaims.FirstOrDefault(c => c.PersonalAccidentsClaimID == claimID);
                    if (claimInContext != null)
                    {
                        claimInContext.PersonalAccidentsClaimTypeID = claimTypeId;
                        claimInContext.PersonalAccidentsClaimConditionID = claimConditionId;
                        claimInContext.ClaimStatus = claimStatus;
                        claimInContext.ClaimNumber = claimNumber;
                        claimInContext.DeliveredToCompanyDate = deliveredToCompanyDate;
                        claimInContext.MoneyReceivedDate = moneyRecivedDate;
                        claimInContext.ClaimAmount = claimAmount;
                        claimInContext.AmountReceived = amountRecived;
                        claimInContext.EventDateAndTime = eventDateTime;
                        claimInContext.EventPlace = eventPlace;
                        claimInContext.EventDescription = eventDescription;

                        context.SaveChanges();
                    }
                    else
                    {
                        throw new Exception("תביעה לא קיימת במערכת");
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
        }
    }
}
