﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoralBusinessLogics
{
    public class ContactCategoriesLogic //: BaseLogics
    {
        //מחזיר רשימה של כל הקטגוריות של אנשי הקשר
        public List<ContactCategory> GetAllContactCategories()
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {

                return context.ContactCategories.OrderByDescending(c => c.Status).ThenBy(c => c.ContactCategoryName).ToList();
            }
        }

        //מחזיר רק את הקטגוריות הפעילות
        public List<ContactCategory> GetActiveCategories()
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {

                return (from c in context.ContactCategories
                        where c.Status == true
                        select c).OrderBy(c => c.ContactCategoryName).ToList();
            }
        }

        //מוסיף קטגוריה חדשה
        public ContactCategory InsertCategory(string name, bool status)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {

                var categoryInContext = (from c in context.ContactCategories
                                         where c.ContactCategoryName == name
                                         select c).FirstOrDefault();
                if (categoryInContext == null)
                {
                    ContactCategory category = new ContactCategory();
                    category.ContactCategoryName = name;
                    category.Status = status;
                    try
                    {
                        context.ContactCategories.Add(category);
                        context.SaveChanges();
                        return category;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }
                else
                {
                    throw new Exception("קטגוריה קיימת");
                }
            }
        }

        //מעדכן את הסטטוס של קטגוריה קיימת
        public ContactCategory UpdateCategoryStatus(string categoryName, bool IsActive)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {

                var categoryInContext = (from c in context.ContactCategories
                                         where c.ContactCategoryName == categoryName
                                         select c).FirstOrDefault();

                if (categoryInContext != null)
                {
                    try
                    {
                        categoryInContext.Status = IsActive;
                        context.SaveChanges();
                        return categoryInContext;
                    }

                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }
                else
                {
                    throw new Exception("קטגוריה לא קיימת");
                }
            }
        }       
    }
}
