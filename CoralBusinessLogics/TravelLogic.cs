﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoralBusinessLogics
{
    public class TravelLogic
    {
        public List<TravelPolicy> GetAllTravelPoliciesWithoutOffers()
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.TravelPolicies.Include("Insurance").Include("Client").Include("Client.Agent").Include("Client.Agent1").Include("Company").Where(p => p.IsOffer == false).OrderByDescending(p => p.StartDate).ThenBy(p => p.Client.LastName).ToList();
            }
        }

        public List<TravelPolicy> GetActivePoliciesWithoutOffers()
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.TravelPolicies.Include("Insurance").Include("Client").Include("Client.Agent").Include("Client.Agent1").Include("Company").Where(p => p.IsOffer == false&&p.EndDate>=DateTime.Today).OrderByDescending(p => p.StartDate).ThenBy(p => p.Client.LastName).ToList();
            }
        }

        public List<TravelPolicy> GetPoliciesWithoutOffersByStartDate(DateTime fromDate, DateTime? toDate)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.TravelPolicies.Include("Insurance").Include("Client").Include("Client.Agent").Include("Client.Agent1").Include("Company").Where(p => p.IsOffer == false && p.StartDate >= fromDate && p.StartDate <= toDate).OrderByDescending(p => p.StartDate).ThenBy(p => p.Client.LastName).ToList();
            }
        }
        public int InsertTravelPolicy(Client client, bool isOffer, DateTime openDate, string policyNumber, int? addition, int? principalAgentNumberId, int? secundaryAgentNumberId, DateTime? startDate, DateTime? endDate, int companyId, string travelPurpose, bool? isEnglishAuthorizationRequired, string currency, string paymentMethod, int? paymentsNumber, DateTime? paymentDate, decimal premium, int? country1Id, int? country2Id, int? country3Id, int? country4Id, string otherCountries, DateTime? country1From, DateTime? country2From, DateTime? country3From, DateTime? country4From, DateTime? otherCountryFrom, DateTime? country1To, DateTime? country2To, DateTime? country3To, DateTime? country4To, DateTime? otherCountryTo, string comments)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var policyInContext = context.TravelPolicies.FirstOrDefault(p => p.PolicyNumber == policyNumber && p.Addition == addition);
                if (policyInContext == null)
                {
                    try
                    {
                        var insuranceInContext = context.Insurances.FirstOrDefault(i => i.InsuranceName == "נסיעות לחו''ל");

                        TravelPolicy newPolicy = new TravelPolicy()
                        {
                            Addition = addition,
                            ClientID = client.ClientID,
                            Comments = comments,
                            CompanyID = companyId,
                            Currency = currency,
                            EndDate = endDate,
                            InsuranceID = insuranceInContext.InsuranceID,
                            IsOffer = isOffer,
                            OpenDate = openDate,
                            PaymentDate = paymentDate,
                            PaymentMethod = paymentMethod,
                            PaymentsNumber = paymentsNumber,
                            PolicyNumber = policyNumber,
                            PrincipalAgentNumberID = principalAgentNumberId,
                            StartDate = startDate,
                            SubAgentNumberID = secundaryAgentNumberId,
                            Premium = premium,
                            Country1FromDate = country1From,
                            Country1ID = country1Id,
                            Country1ToDate = country1To,
                            Country2FromDate = country2From,
                            Country2ID = country2Id,
                            Country2ToDate = country2To,
                            Country3FromDate = country3From,
                            Country3ID = country3Id,
                            Country3ToDate = country3To,
                            Country4FromDate = country4From,
                            Country4ID = country4Id,
                            Country4ToDate = country4To,
                            IsEnglishAuthorizationRequired = isEnglishAuthorizationRequired,
                            OtherCountry = otherCountries,
                            OtherCountryFromDate = otherCountryFrom,
                            OtherCountryToDate = otherCountryTo,
                            TravelPurpose = travelPurpose
                        };
                        context.TravelPolicies.Add(newPolicy);
                        context.SaveChanges();
                        return newPolicy.TravelPolicyID;
                    }
                    catch (Exception e)
                    {
                        throw new Exception(e.Message);
                    }
                }
                else
                {
                    throw new Exception("פוליסה קיימת במערכת");
                }
            }
        }

  

        public List<TravelPolicy> GetOffers()
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.TravelPolicies.Include("Country").Include("Client").Include("Company").Where(p => p.IsOffer == true).OrderByDescending(p => p.StartDate).ThenBy(p => p.Client.LastName).ToList();
            }
        }

        public List<TravelPolicy> GetActiveTravelPoliciesByClient(Client client)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                if (client != null)
                {
                    var policies = context.TravelPolicies.Where(p => p.ClientID == client.ClientID && p.IsOffer == false && p.EndDate >= DateTime.Today).OrderByDescending(p => p.OpenDate).ToList();
                    var sortedPolicies = context.TravelPolicies.Include("Insurance").Include("Client").Include("Company").Include("Country").Where(p => p.ClientID == client.ClientID && p.IsOffer == false && p.EndDate >= DateTime.Today).OrderByDescending(p => p.OpenDate).ToList();
                    for (int i = policies.Count - 1; i >= 0; i--)
                    {
                        if (policies[i].Addition > 0)
                        {
                            var policy = policies.Where(p => p.PolicyNumber == policies[i].PolicyNumber && p.Addition == policies[i].Addition - 1).FirstOrDefault();
                            if (policy != null)
                            {
                                sortedPolicies.Remove(policies[i]);
                                var index = sortedPolicies.FindIndex(p => p.PolicyNumber == policy.PolicyNumber && p.Addition == policy.Addition);
                                sortedPolicies.Insert(index, policies[i]);
                            }
                        }
                    }
                    return sortedPolicies;
                }
                else
                {
                    return null;
                }
            }
        }

        public void TransferPolicyToClient(TravelPolicy policy, Client client)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var policyInContext = context.TravelPolicies.FirstOrDefault(p => p.TravelPolicyID == policy.TravelPolicyID);
                if (policyInContext != null)
                {
                    policyInContext.ClientID = client.ClientID;
                    context.SaveChanges();
                }
                else
                {
                    throw new Exception("פוליסה לא קיימת במערכת");
                }
            }
        }

        public List<TravelPolicy> GetAllTravelOffersByClient(Client client)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                if (client == null)
                {
                    return null;
                }
                return context.TravelPolicies.Include("Client").Include("Insurance").Include("Company").Where(p => p.ClientID == client.ClientID && p.IsOffer == true).OrderByDescending(p => p.StartDate).ToList();
            }
        }

        public List<TravelPolicy> GetNonActiveTravelPoliciesByClient(Client client)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                if (client != null)
                {
                    var policies = context.TravelPolicies.Where(p => p.ClientID == client.ClientID && p.IsOffer == false && p.EndDate < DateTime.Today).OrderByDescending(p => p.OpenDate).ToList();
                    var sortedPolicies = context.TravelPolicies.Include("Insurance").Include("Client").Include("Company").Include("Country").Where(p => p.ClientID == client.ClientID && p.IsOffer == false && p.EndDate < DateTime.Today).OrderByDescending(p => p.OpenDate).ToList();
                    for (int i = policies.Count - 1; i >= 0; i--)
                    {
                        if (policies[i].Addition > 0)
                        {
                            var policy = policies.Where(p => p.PolicyNumber == policies[i].PolicyNumber && p.Addition == policies[i].Addition - 1).FirstOrDefault();
                            if (policy != null)
                            {
                                sortedPolicies.Remove(policies[i]);
                                var index = sortedPolicies.FindIndex(p => p.PolicyNumber == policy.PolicyNumber && p.Addition == policy.Addition);
                                sortedPolicies.Insert(index, policies[i]);
                            }
                        }
                    }
                    return sortedPolicies;
                }
                else
                {
                    return null;
                }
            }
        }

        public List<TravelPolicy> GetAllTravelPoliciesByClient(Client client)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                if (client != null)
                {
                    var policies = context.TravelPolicies.Where(p => p.ClientID == client.ClientID && p.IsOffer == false).OrderByDescending(p => p.OpenDate).ToList();
                    var sortedPolicies = context.TravelPolicies.Include("Insurance").Include("Client").Include("Company").Include("Country").Where(p => p.ClientID == client.ClientID && p.IsOffer == false).OrderByDescending(p => p.OpenDate).ToList();
                    for (int i = policies.Count - 1; i >= 0; i--)
                    {
                        if (policies[i].Addition > 0)
                        {
                            var policy = policies.Where(p => p.PolicyNumber == policies[i].PolicyNumber && p.Addition == policies[i].Addition - 1).FirstOrDefault();
                            if (policy != null)
                            {
                                sortedPolicies.Remove(policies[i]);
                                var index = sortedPolicies.FindIndex(p => p.PolicyNumber == policy.PolicyNumber && p.Addition == policy.Addition);
                                sortedPolicies.Insert(index, policies[i]);
                            }
                        }
                    }
                    return sortedPolicies;
                }
                else
                {
                    return null;
                }
            }
        }

        public List<TravelInsuranceDetail> GetInsuranceDetailsByPolicyId(int policyId)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.TravelInsuranceDetails.Include("TravelAdditionalInsured").Where(d => d.TravelPolicyID == policyId).ToList();
            }
        }        

        public List<TravelAdditionalInsured> GetAdditionalInsuredsByPolicyId(int travelPolicyID)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.TravelAdditionalInsureds.Where(i => i.TravelPolicyID == travelPolicyID).ToList();
            }
        }

        public List<TravelPolicy> GetActiveTravelPoliciesWithoutAdditionByClient(Client client)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.TravelPolicies.Include("Insurance").Where(p => p.ClientID == client.ClientID && p.Addition == 0 && p.IsOffer == false).OrderByDescending(p => p.StartDate).ToList();
            }
        }

        public void UpdateTravelPolicy(int travelPolicyID, string policyNumber, int? addition, int? principalAgentNumberId, int? secundaryAgentNumberId, DateTime? startDate, DateTime? endDate, int companyId, string travelPurpose, bool? isEnglishAuthorizationRequired, string currency, string paymentMethod, int? paymentsNumber, DateTime? paymentDate, decimal premium, int? country1Id, int? country2Id, int? country3Id, int? country4Id, string otherCountries, DateTime? country1From, DateTime? country2From, DateTime? country3From, DateTime? country4From, DateTime? otherCountryFrom, DateTime? country1To, DateTime? country2To, DateTime? country3To, DateTime? country4To, DateTime? otherCountryTo, string comments)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var policyInContext = context.TravelPolicies.Where(p => p.TravelPolicyID == travelPolicyID).FirstOrDefault();
                if (policyInContext != null)
                {
                    try
                    {
                        policyInContext.PolicyNumber = policyNumber;
                        policyInContext.PrincipalAgentNumberID = principalAgentNumberId;
                        policyInContext.SubAgentNumberID = secundaryAgentNumberId;
                        policyInContext.StartDate = startDate;
                        policyInContext.EndDate = endDate;
                        policyInContext.CompanyID = companyId;
                        policyInContext.Currency = currency;
                        policyInContext.PaymentMethod = paymentMethod;
                        policyInContext.PaymentsNumber = paymentsNumber;
                        policyInContext.PaymentDate = paymentDate;
                        policyInContext.Premium = premium;
                        policyInContext.Country1FromDate = country1From;
                        policyInContext.Country1ID = country1Id;
                        policyInContext.Country1ToDate = country1To;
                        policyInContext.Country2FromDate = country2From;
                        policyInContext.Country2ID = country2Id;
                        policyInContext.Country2ToDate = country2To;
                        policyInContext.Country3FromDate = country3From;
                        policyInContext.Country3ID = country3Id;
                        policyInContext.Country3ToDate = country3To;
                        policyInContext.Country4FromDate = country4From;
                        policyInContext.Country4ID = country4Id;
                        policyInContext.Country4ToDate = country4To;
                        policyInContext.IsEnglishAuthorizationRequired = isEnglishAuthorizationRequired;
                        policyInContext.OtherCountry = otherCountries;
                        policyInContext.OtherCountryFromDate = otherCountryFrom;
                        policyInContext.OtherCountryToDate = otherCountryTo;
                        policyInContext.TravelPurpose = travelPurpose;
                        policyInContext.Comments = comments;
                        context.SaveChanges();
                    }
                    catch (Exception e)
                    {
                        throw new Exception(e.Message);
                    }
                }
                else
                {
                    throw new Exception("פוליסה לא קיימת במערכת");
                }
            }
        }

        public void InsertAdditionalInsureds(int policyId, List<TravelAdditionalInsured> insureds, bool isAddition)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var policyInContext = context.TravelPolicies.FirstOrDefault(c => c.TravelPolicyID == policyId);

                if (policyInContext != null)
                {
                    try
                    {
                        var policyInsureds = context.TravelAdditionalInsureds.Where(c => c.TravelPolicyID == policyId).ToList();
                        foreach (TravelAdditionalInsured insured in insureds)
                        {
                            if (insured.TavelAdditionalInsuredID == 0)
                            {
                                insured.TravelPolicyID = policyId;
                                context.TravelAdditionalInsureds.Add(insured);
                            }
                            else if (isAddition)
                            {
                                TravelAdditionalInsured newInsured = new TravelAdditionalInsured() { BirthDate = insured.BirthDate, FirstName = insured.FirstName, IdNumber = insured.IdNumber, IsMale = insured.IsMale, LastName = insured.LastName, MaritalStatus = insured.MaritalStatus, TravelPolicyID = policyId, Profession = insured.Profession, Relationship = insured.Relationship };
                                context.TravelAdditionalInsureds.Add(newInsured);
                            }
                            else
                            {
                                var insuredInContext = context.TravelAdditionalInsureds.FirstOrDefault(c => c.TavelAdditionalInsuredID == insured.TavelAdditionalInsuredID);
                                if (insuredInContext != null)
                                {
                                    insuredInContext.BirthDate = insured.BirthDate;
                                    insuredInContext.FirstName = insured.FirstName;
                                    insuredInContext.IdNumber = insured.IdNumber;
                                    insuredInContext.Relationship = insured.Relationship;
                                    insuredInContext.IsMale = insured.IsMale;
                                    insuredInContext.LastName = insured.LastName;
                                    insuredInContext.MaritalStatus = insured.MaritalStatus;
                                    insuredInContext.Profession = insured.Profession;

                                    policyInsureds.Remove(insuredInContext);
                                }
                            }
                        }
                        if (policyInsureds.Count > 0)
                        {
                            foreach (var item in policyInsureds)
                            {
                                var insuranceDetailInContext = context.TravelInsuranceDetails.FirstOrDefault(d => d.TravelAdditionalInsuredID == item.TavelAdditionalInsuredID);
                                if (insuranceDetailInContext!=null)
                                {
                                    context.TravelInsuranceDetails.Remove(insuranceDetailInContext);
                                }
                                context.TravelAdditionalInsureds.Remove(item);
                            }
                        }
                        context.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }
                else
                {
                    throw new Exception("פוליסה לא קיימת במערכת");
                }
            }
        }

        public int? GetAdditionalInsuredByName(int policyId,string name)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var insureds = context.TravelAdditionalInsureds.Where(p => p.TravelPolicyID == policyId);
                foreach (var item in insureds)
                {
                    string nameInContext = item.FirstName + " " + item.LastName;
                    if (nameInContext==name)
                    {
                        return item.TavelAdditionalInsuredID;                        
                    }
                }
                return null;
            }
        }

        public void InsertInsurancesDetails(TravelInsuranceDetail[] travelInsurancesDetail)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                try
                {
                    foreach (var item in travelInsurancesDetail)
                    {
                        if (item == null)
                        {
                            continue;
                        }
                        var policyInContext = context.TravelPolicies.FirstOrDefault(c => c.TravelPolicyID == item.TravelPolicyID);

                        if (policyInContext != null)
                        {
                            if (item.TravelInsuranceDetailID == 0)
                            {
                                context.TravelInsuranceDetails.Add(item);
                            }
                            else
                            {
                                var insuranceDetailInContext = context.TravelInsuranceDetails.FirstOrDefault(d => d.TravelInsuranceDetailID == item.TravelInsuranceDetailID);
                                if (insuranceDetailInContext != null)
                                {
                                    insuranceDetailInContext.IsBaggage = item.IsBaggage;
                                    insuranceDetailInContext.IsBicycle = item.IsBicycle;
                                    insuranceDetailInContext.IsComputerOrTablet = item.IsComputerOrTablet;
                                    insuranceDetailInContext.IsExistingMedicalCondition = item.IsExistingMedicalCondition;
                                    insuranceDetailInContext.IsExtremeSport = item.IsExtremeSport;
                                    insuranceDetailInContext.IsOther = item.IsOther;
                                    insuranceDetailInContext.IsPhoneOrGPS = item.IsPhoneOrGPS;
                                    insuranceDetailInContext.IsPregnancy = item.IsPregnancy;
                                    insuranceDetailInContext.IsSearchAndRescue = item.IsSearchAndRescue;
                                    insuranceDetailInContext.IsWinterSport = item.IsWinterSport;
                                    insuranceDetailInContext.OtherName = item.OtherName;
                                    insuranceDetailInContext.TravelInsuranceTypeID = item.TravelInsuranceTypeID;
                                }
                            }
                        }
                        else
                        {
                            throw new Exception("פוליסה לא קיימת במערכת");
                        }
                    }
                    context.SaveChanges();
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
        }

        public string GetAdditionNumber(string policyNumber)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var policiesInContext = context.TravelPolicies.Where(p => p.PolicyNumber == policyNumber);
                int? addition = 0;
                foreach (var item in policiesInContext)
                {
                    if (item.Addition > addition)
                    {
                        addition = item.Addition;
                    }
                }
                return (addition + 1).ToString();
            }
        }

        public void ChangeTravelPolicyOfferStatus(int travelPolicyID, bool isOffer)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var policyInContext = context.TravelPolicies.FirstOrDefault(p => p.TravelPolicyID == travelPolicyID);
                policyInContext.IsOffer = isOffer;
                context.SaveChanges();
            }
        }

        public TravelPolicy GetTravelPolicyByPolicyId(int policyId)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.TravelPolicies.Include("Client").FirstOrDefault(p => p.TravelPolicyID == policyId);
            }
        }
    }
}
