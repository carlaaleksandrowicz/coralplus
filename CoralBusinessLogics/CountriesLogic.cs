﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoralBusinessLogics
{
    public class CountriesLogic
    {
        public List<Country> GetAllCountries()
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.Countries.OrderBy(c => c.CountryName).ToList();
            }
        }

        public List<Country> GetActiveCountries()
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.Countries.Where(c=>c.Status==true).OrderByDescending(c=>c.Status).ThenBy(c => c.CountryName).ToList();
            }
        }

        public Country InsertCountry(string name, bool isActive)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var countriesInContext = context.Countries.FirstOrDefault(a => a.CountryName == name);
                if (countriesInContext == null)
                {
                    try
                    {
                        Country newCountry = new Country() { CountryName=name, Status=isActive };
                        context.Countries.Add(newCountry);
                        context.SaveChanges();
                        return newCountry;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }
                else
                {
                    throw new Exception("מדינה קיימת במערכת");
                }
            }
        }

        public Country UpdateCountry(string name, bool status)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var countriesInContext = context.Countries.FirstOrDefault(a => a.CountryName == name);
                if (countriesInContext != null)
                {
                    try
                    {
                        countriesInContext.Status = status;
                        context.SaveChanges();
                        return countriesInContext;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }
                else
                {
                    throw new Exception("מדינה לא קיימת במערכת");
                }
            }
        }
    }
}
