﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoralBusinessLogics
{
    public class ForeingWorkersClaimsLogic
    {
        //מחזיר רשימה של כל התביעות חיים לפי לקוח
        public List<ForeingWorkersClaim> GetAllForeingWorkersClaimsByClient(int clientId)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.ForeingWorkersClaims.Include("ForeingWorkersPolicy.Client").Include("ForeingWorkersClaimCondition").Include("ForeingWorkersClaimType").Include("ForeingWorkersPolicy").Include("ForeingWorkersPolicy.Company").Include("ForeingWorkersPolicy.Insurance").Include("ForeingWorkersPolicy.ForeingWorkersIndustry").Where(c => c.ForeingWorkersPolicy.ClientID == clientId).OrderByDescending(c => c.OpenDate).ToList();
            }
        }
        public List<ForeingWorkersClaimType> GetAllForeingWorkersClaimTypes()
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.ForeingWorkersClaimTypes.OrderByDescending(t => t.Status).ThenBy(t => t.ClaimTypeName).ToList();
            }
        }
        public List<ForeingWorkersClaimType> GetActiveForeingWorkersClaimTypes()
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.ForeingWorkersClaimTypes.Where(t => t.Status == true).OrderBy(t => t.ClaimTypeName).ToList();
            }
        }

        public ForeingWorkersClaimType InsertForeingWorkersClaimType(string claimTypeName, bool status)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var claimTypeInContext = context.ForeingWorkersClaimTypes.FirstOrDefault(t => t.ClaimTypeName == claimTypeName);
                if (claimTypeInContext == null)
                {
                    try
                    {
                        ForeingWorkersClaimType newType = new ForeingWorkersClaimType() { ClaimTypeName = claimTypeName, Status = status };
                        context.ForeingWorkersClaimTypes.Add(newType);
                        context.SaveChanges();
                        return newType;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }
                else
                {
                    throw new Exception("סוג תביעה קיימת במערכת");
                }
            }
        }

        public ForeingWorkersClaimType UpdateForeingWorkersClaimTypeStatus(string typeName, bool status)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {

                var claimTypeInContext = context.ForeingWorkersClaimTypes.Where(c => c.ClaimTypeName == typeName).FirstOrDefault();
                if (claimTypeInContext != null)
                {
                    try
                    {
                        claimTypeInContext.Status = status;
                        context.SaveChanges();
                        return claimTypeInContext;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }
                else
                {
                    throw new Exception("סוג תביעה לא קיימת במערכת");
                }
            }
        }


        //מחזיר רשימה של כל מצבי תביעה חיים
        public List<ForeingWorkersClaimCondition> GetAllForeingWorkersClaimConditions()
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.ForeingWorkersClaimConditions.OrderByDescending(c => c.Status).ThenBy(c => c.Description).ToList();
            }
        }

        //מחזיר רשימה של מצבי תביעה חיים אקטיביים לפי ענף
        public List<ForeingWorkersClaimCondition> GetActiveForeingWorkersClaimConditions()
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.ForeingWorkersClaimConditions.Where(c => c.Status == true).OrderBy(c => c.Description).ToList();
            }
        }

        //מוסיף מצב תביעה לבסיס הנתונים
        public ForeingWorkersClaimCondition InsertForeingWorkersClaimCondition(string claimConditionName, bool status)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var claimConditionInContext = context.ForeingWorkersClaimConditions.FirstOrDefault(c => c.Description == claimConditionName);
                if (claimConditionInContext == null)
                {
                    try
                    {
                        ForeingWorkersClaimCondition newCondition = new ForeingWorkersClaimCondition() { Description = claimConditionName, Status = status };
                        context.ForeingWorkersClaimConditions.Add(newCondition);
                        context.SaveChanges();
                        return newCondition;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }
                else
                {
                    throw new Exception("מצב תביעה קיימת במערכת");
                }
            }
        }

        //מעדכן את הסטטוס של מצב תביעה
        public ForeingWorkersClaimCondition UpdateForeingWorkersClaimConditionStatus(string conditionName, bool status)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var claimConditionInContext = context.ForeingWorkersClaimConditions.FirstOrDefault(c => c.Description == conditionName);
                if (claimConditionInContext != null)
                {
                    try
                    {
                        claimConditionInContext.Status = status;
                        context.SaveChanges();
                        return claimConditionInContext;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }
                else
                {
                    throw new Exception("סוג תביעה לא קיים במערכת");
                }
            }
        }

        //מוסיף תביעה חיים לבסיס הנתונים
        public int InsertForeingWorkersClaim(int policyId, int? claimTypeId, int? claimConditionId, bool claimStatus, string claimNumber, DateTime? openDate, DateTime? deliveredToCompanyDate, DateTime? moneyRecivedDate, decimal? claimAmount, decimal? amountRecived, DateTime? eventDate, string eventPlace, string eventDescription)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                try
                {
                    ForeingWorkersClaim newClaim = new ForeingWorkersClaim()
                    {
                        ForeingWorkersPolicyID = policyId,
                        ForeingWorkersClaimTypeID = claimTypeId,
                        ForeingWorkersClaimConditionID = claimConditionId,
                        ClaimStatus = claimStatus,
                        ClaimNumber = claimNumber,
                        OpenDate = openDate,
                        DeliveredToCompanyDate = deliveredToCompanyDate,
                        MoneyReceivedDate = moneyRecivedDate,
                        ClaimAmount = claimAmount,
                        AmountReceived = amountRecived,
                        EventDateAndTime = eventDate,
                        EventPlace = eventPlace,
                        EventDescription = eventDescription
                    };
                    context.ForeingWorkersClaims.Add(newClaim);
                    context.SaveChanges();
                    return newClaim.ForeingWorkersClaimID;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
        }


        //מוסיף עד לתביעה
        public void InsertWitnesses(List<ForeingWorkersWitness> witnesses, int claimID)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var claimInContext = context.ForeingWorkersClaims.FirstOrDefault(c => c.ForeingWorkersClaimID == claimID);
                if (claimInContext != null)
                {
                    try
                    {
                        foreach (var item in witnesses)
                        {
                            if (item.ForeingWorkersWitnessID == 0)
                            {
                                item.ForeingWorkersClaimID = claimID;
                                context.ForeingWorkersWitnesses.Add(item);
                            }
                            else
                            {
                                var witnessInContext = context.ForeingWorkersWitnesses.FirstOrDefault(w => w.ForeingWorkersWitnessID == item.ForeingWorkersWitnessID);
                                if (witnessInContext != null)
                                {
                                    witnessInContext.WitnessCellPhone = item.WitnessCellPhone;
                                    witnessInContext.WitnessName = item.WitnessName;
                                    witnessInContext.WitnessPhone = item.WitnessPhone;
                                    witnessInContext.WittnessAddress = item.WittnessAddress;
                                }
                            }
                        }
                        context.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }
                else
                {
                    throw new Exception("תביעה לא קיימת במערכת");
                }
            }
        }

        //מחזיר רשימה של עדים לפי תביעה
        public List<ForeingWorkersWitness> GetWitnessesByClaim(int claimId)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.ForeingWorkersWitnesses.Where(w => w.ForeingWorkersClaimID == claimId).ToList();
            }
        }


        //מעדכן תביעה חיים
        public void UpdateForeingWorkersClaim(int claimID, int? claimTypeId, int? claimConditionId, bool claimStatus, string claimNumber, DateTime? deliveredToCompanyDate, DateTime? moneyRecivedDate, decimal? claimAmount, decimal? amountRecived, DateTime? eventDateTime, string eventPlace, string eventDescription)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                try
                {
                    var claimInContext = context.ForeingWorkersClaims.FirstOrDefault(c => c.ForeingWorkersClaimID == claimID);
                    if (claimInContext != null)
                    {
                        claimInContext.ForeingWorkersClaimTypeID = claimTypeId;
                        claimInContext.ForeingWorkersClaimConditionID = claimConditionId;
                        claimInContext.ClaimStatus = claimStatus;
                        claimInContext.ClaimNumber = claimNumber;
                        claimInContext.DeliveredToCompanyDate = deliveredToCompanyDate;
                        claimInContext.MoneyReceivedDate = moneyRecivedDate;
                        claimInContext.ClaimAmount = claimAmount;
                        claimInContext.AmountReceived = amountRecived;
                        claimInContext.EventDateAndTime = eventDateTime;
                        claimInContext.EventPlace = eventPlace;
                        claimInContext.EventDescription = eventDescription;

                        context.SaveChanges();
                    }
                    else
                    {
                        throw new Exception("תביעה לא קיימת במערכת");
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
        }
    }
}
