﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Objects;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Windows.Controls;
using System.Collections;

namespace CoralBusinessLogics
{
    //ניהול לקוחות
    public class ClientsLogic //: BaseLogics
    {
        //מחזיר רשימה של סוגי לקוחות
        public IEnumerable<ClientType> GetAllTypes()
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.ClientTypes.Where(t => t.ClientTypeName != "אלפון").ToList();
            }
        }

        //הוספת סוג לקוח חדש
        public void InsertClientType(string type)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var typeInContext = (from t in context.ClientTypes
                                     where t.ClientTypeName == type
                                     select t).FirstOrDefault();
                if (typeInContext == null)
                {
                    try
                    {
                        ClientType newClientType = new ClientType();
                        newClientType.ClientTypeName = type;
                        context.ClientTypes.Add(newClientType);
                        context.SaveChanges();
                    }
                    catch (Exception e)
                    {
                        throw new Exception(e.Message);
                    }
                }
                else
                {
                    throw new Exception("סוג זה כבר קיים");
                }
            }
        }

        //מחזיר את סוג הלקוח של לקוח מסוים
        public ClientType GetClientTypeForClient(Client client)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.ClientTypes.FirstOrDefault(c => c.ClientTypeID == client.ClientTypeID);
            }
        }

        //מחזיר רשימה של קטגוריות לקוח
        public List<Category> GetAllCategories()
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.Categories.OrderByDescending(c => c.Status).ThenBy(c => c.CategoryName).ThenBy(c => c.CategoryName == "הכל").ToList();
            }
        }

        public ClientType GetClientTypeForClient(int clientTypeID)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.ClientTypes.FirstOrDefault(t => t.ClientTypeID == clientTypeID);
            }
        }

        //מחזיר רשימה של קטגוריות לקוח פעילות
        public List<Category> GetActiveCategories()
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return (from c in context.Categories
                        where c.Status == true
                        select c).OrderBy(c => c.CategoryName).ToList();
            }
        }

        //הוספת קטגורית לקוח חדשה
        public Category InsertCategory(string name, bool isActive)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var categoryInContext = (from c in context.Categories
                                         where c.CategoryName == name
                                         select c).FirstOrDefault();

                if (categoryInContext == null)
                {
                    try
                    {
                        Category category = new Category();
                        category.CategoryName = name;
                        category.Status = isActive;
                        context.Categories.Add(category);
                        context.SaveChanges();
                        return category;
                    }
                    catch (Exception e)
                    {
                        throw new Exception(e.Message);
                    }
                }
                else
                {
                    throw new Exception("קטגוריה זו כבר קיימת");
                }
            }
        }

        public bool isActivePoliciesToClient(Client clientToUpdate)
        {
            PoliciesLogic e = new PoliciesLogic();
            if(e.GetAllActiveElementaryPoliciesByClient(clientToUpdate).Count>0)
            {
                return true;
            }
            PersonalAccidentsLogic p = new PersonalAccidentsLogic();
            if (p.GetActivePersonalAccidentsPoliciesByClient(clientToUpdate).Count > 0)
            {
                return true;
            }
            TravelLogic t = new TravelLogic();
            if (t.GetActiveTravelPoliciesByClient(clientToUpdate).Count > 0)
            {
                return true;
            }
            LifePoliciesLogic l = new LifePoliciesLogic();
            if (l.GetAllActiveLifePoliciesByClient(clientToUpdate).Count > 0)
            {
                return true;
            }
            HealthPoliciesLogic h = new HealthPoliciesLogic();
            if (h.GetActiveAllHealthPoliciesByClient(clientToUpdate).Count > 0)
            {
                return true;
            }
            ForeingWorkersLogic fw = new ForeingWorkersLogic();
            if (fw.GetAllActiveForeingWorkersPoliciesByClient(clientToUpdate).Count > 0)
            {
                return true;
            }
            FinancePoliciesLogic f = new FinancePoliciesLogic();
            if (f.GetAllActiveFinancePoliciesByClient(clientToUpdate).Count > 0)
            {
                return true;
            }
            return false;
        }

        //מעדכן את הסטטוס של קטגוריה קיימת
        public Category UpdateCategoryStatus(string categoryName, bool IsActive)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {

                var categoryInContext = (from c in context.Categories
                                         where c.CategoryName == categoryName
                                         select c).FirstOrDefault();

                if (categoryInContext != null)
                {
                    try
                    {
                        categoryInContext.Status = IsActive;
                        context.SaveChanges();
                        return categoryInContext;
                    }

                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }
                else
                {
                    throw new Exception("קטגוריה לא קיימת");
                }
            }
        }

        //מחזיר רשימה של כל הלקוחות
        public List<Client> GetAllClients()
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.Clients.Include("Agent").Include("Agent1").OrderBy(c => c.LastName).ToList();
            }
        }

        //מחזיר רשימה של לקוחות לפי סוג מסוים
        public List<Client> GetAllClientsByType(string typeName)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var typeInContext = context.ClientTypes.FirstOrDefault(t => t.ClientTypeName == typeName);
                return context.Clients.Include("Agent").Include("Agent1").Where(c => c.ClientTypeID == typeInContext.ClientTypeID).OrderBy(c => c.LastName).ToList();
            }
        }

        ////הוספת לקוח חדש
        //public void InsertClient(ClientType type, Category category, DateTime? openDate, string clientNumber, Agent principalAgent, Agent secundaryAgent, string lastName, string firstName, string idNumber, DateTime? birthDate, string maritalStatus, string city, string street, string homeNumber, string aptNumber, string zipCode, string homePhone, string cellPhone, string workPhone, string fax, string email,string email2, string mailingAddress, string companyName, string companyAddress, string website, string passportNumber, string kupatJolim, string profession, string workerType, string licenseNumber, DateTime? licenseDate, bool drivesInShabbat, bool smokes, string comments)
        //{
        //    using (Coral_DB_Entities context = new Coral_DB_Entities())
        //    {

        //        var clientInContext = context.Clients.FirstOrDefault(c => c.IdNumber == idNumber);
        //        if (clientInContext == null)
        //        {
        //            try
        //            {
        //                Client newClient = new Client() { ClientTypeID = type.ClientTypeID, CategoryID = category.CategoryID, OpenDate = openDate, ClientNumber = clientNumber, PrincipalAgentID = principalAgent.AgentID, LastName = lastName, FirstName = firstName, IdNumber = idNumber, BirthDate = birthDate, MaritalStatus = maritalStatus, City = city, Street = street, HomeNumber = homeNumber, ApartmentNumber = aptNumber, ZipCode = zipCode, PhoneHome = homePhone, CellPhone = cellPhone, PhoneWork = workPhone, Fax = fax, Email = email, Email2=email2 , MailingAddress = mailingAddress, BusinessName = companyName, CompanyAddress = companyAddress, Website = website, PassportNumber = passportNumber, KupatJolim = kupatJolim, Profession = profession, WorkerType = workerType, DrivingLicenseNumber = licenseNumber, DrivingLicenseDate = licenseDate, DrivesInShabbat = drivesInShabbat, Smokes = smokes, Remarks = comments };
        //                if (secundaryAgent != null)
        //                {
        //                    newClient.SecundaryAgentID = secundaryAgent.AgentID;
        //                }

        //                context.Clients.Add(newClient);
        //                context.SaveChanges();
        //            }
        //            catch (Exception ex)
        //            {
        //                throw new Exception(ex.Message);
        //            }
        //        }
        //        else
        //        {
        //            throw new Exception("לקוח קיים במערכת");
        //        }
        //    }
        //}

        //הוספת לקוח חדש
        public Client InsertClient(ClientType type, Category category, DateTime? openDate, string clientNumber, Agent principalAgent, Agent secundaryAgent, string lastName, string firstName, string idNumber, DateTime? birthDate, string maritalStatus, string city, string street, string homeNumber, string aptNumber, string zipCode, string homePhone, string cellPhone, string workPhone, string fax, string email, string email2, string mailingAddress, string companyName, string companyAddress, string website, string passportNumber, string kupatJolim, string profession, string workerType, string occupation, DateTime? jobStartDate, int? hobbyId, string licenseNumber, DateTime? licenseDate, bool drivesInShabbat, bool smokes, string comments, int? referedByClientId, string referedByRelation, bool isNew)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {

                var clientInContext = context.Clients.FirstOrDefault(c => c.IdNumber == idNumber);
                if (clientInContext == null)
                {
                    try
                    {
                        Client newClient = new Client() { ClientTypeID = type.ClientTypeID, CategoryID = category.CategoryID, OpenDate = openDate, ClientNumber = clientNumber, PrincipalAgentID = principalAgent.AgentID, LastName = lastName, FirstName = firstName, IdNumber = idNumber, BirthDate = birthDate, MaritalStatus = maritalStatus, City = city, Street = street, HomeNumber = homeNumber, ApartmentNumber = aptNumber, ZipCode = zipCode, PhoneHome = homePhone, CellPhone = cellPhone, PhoneWork = workPhone, Fax = fax, Email = email, Email2 = email2, MailingAddress = mailingAddress, BusinessName = companyName, CompanyAddress = companyAddress, Website = website, PassportNumber = passportNumber, KupatJolim = kupatJolim, Profession = profession, WorkerType = workerType, DrivingLicenseNumber = licenseNumber, DrivingLicenseDate = licenseDate, DrivesInShabbat = drivesInShabbat, Smokes = smokes, Remarks = comments, DangerousHobbyID = hobbyId, Ocupation = occupation, JobStartDate = jobStartDate, ReferedByClientID = referedByClientId, ReferedByRelation = referedByRelation, isNew = isNew};
                        if (secundaryAgent != null)
                        {
                            newClient.SecundaryAgentID = secundaryAgent.AgentID;
                        }

                        context.Clients.Add(newClient);
                        context.SaveChanges();
                        return newClient;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }
                else
                {
                    throw new Exception("ת.ז. קיימת במערכת");
                }
            }
        }

        //עדכון לקוח קיים
        public Client UpdateClient(Client clientToUpdate ,ClientType type, Category category, DateTime? openDate, string clientNumber, Agent principalAgent, Agent secundaryAgent, string lastName, string firstName, string idNumber, DateTime? birthDate, string maritalStatus, string city, string street, string homeNumber, string aptNumber, string zipCode, string homePhone, string cellPhone, string workPhone, string fax, string email, string email2, string mailingAddress, string companyName, string companyAddress, string website, string passportNumber, string kupatJolim, string profession, string workerType, string occupation, DateTime? jobStartDate, int? hobbyId, string licenseNumber, DateTime? licenseDate, bool drivesInShabbat, bool smokes, string comments, int? referedByClientId, string referedByRelation, bool isNew)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {

                var clientInContext = context.Clients.FirstOrDefault(c => c.ClientID == clientToUpdate.ClientID);
                if (clientInContext != null)
                {
                    var clientIdNumberInContext= context.Clients.FirstOrDefault(c => c.IdNumber == idNumber);
                    if (clientIdNumberInContext!=null&&clientIdNumberInContext.ClientID!=clientInContext.ClientID)
                    {
                        throw new Exception("ת.ז. קיימת במערכת");
                    }
                    try
                    {
                        clientInContext.ClientTypeID = type.ClientTypeID;
                        clientInContext.CategoryID = category.CategoryID;
                        clientInContext.ClientNumber = clientNumber;
                        clientInContext.PrincipalAgentID = principalAgent.AgentID;
                        clientInContext.LastName = lastName;
                        clientInContext.FirstName = firstName;
                        clientInContext.IdNumber = idNumber;
                        clientInContext.BirthDate = birthDate;
                        clientInContext.MaritalStatus = maritalStatus;
                        clientInContext.City = city;
                        clientInContext.Street = street;
                        clientInContext.HomeNumber = homeNumber;
                        clientInContext.ApartmentNumber = aptNumber;
                        clientInContext.ZipCode = zipCode;
                        clientInContext.PhoneHome = homePhone;
                        clientInContext.CellPhone = cellPhone;
                        clientInContext.PhoneWork = workPhone;
                        clientInContext.Fax = fax;
                        clientInContext.Email = email;
                        clientInContext.Email2 = email2;
                        clientInContext.MailingAddress = mailingAddress;
                        clientInContext.BusinessName = companyName;
                        clientInContext.CompanyAddress = companyAddress;
                        clientInContext.Website = website;
                        clientInContext.PassportNumber = passportNumber;
                        clientInContext.KupatJolim = kupatJolim;
                        clientInContext.Profession = profession;
                        clientInContext.WorkerType = workerType;
                        clientInContext.DrivingLicenseNumber = licenseNumber;
                        clientInContext.DrivingLicenseDate = licenseDate;
                        clientInContext.DrivesInShabbat = drivesInShabbat;
                        clientInContext.Smokes = smokes;
                        clientInContext.Remarks = comments;
                        clientInContext.DangerousHobbyID = hobbyId;
                        clientInContext.Ocupation = occupation;
                        clientInContext.JobStartDate = jobStartDate;
                        clientInContext.ReferedByClientID = referedByClientId;
                        clientInContext.ReferedByRelation = referedByRelation;
                        clientInContext.isNew = isNew;
                        if (secundaryAgent != null)
                        {
                            clientInContext.SecundaryAgentID = secundaryAgent.AgentID;
                        }
                        context.SaveChanges();
                        return clientInContext;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }
                else
                {
                    throw new Exception("לקוח לא קיים במערכת");
                }
            }
        }

        //מחזיר רשימה של בני המשפחה של לקוח מסוים
        public List<ClientRelative> GetAllRelativesForClient(Client client)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.ClientRelatives.Include("Relative").Where(cr => cr.ClientID == client.ClientID).ToList();
            }
        }

        //הוספה/עדכון של בני משפחה ללקוח מסוים
        public void InsertRelativesToClient(List<RelativeViewModel> relatives, Client client)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var clientInContext = context.Clients.FirstOrDefault(c => c.ClientID == client.ClientID);
                if (clientInContext != null)
                {
                    try
                    {
                        foreach (RelativeViewModel item in relatives)
                        {
                            bool? isMale = null;
                            if (item.Gender == "נקבה")
                            {
                                isMale = false;
                            }
                            else if (item.Gender == "זכר")
                            {
                                isMale = true;
                            }
                            var relativeInContext = context.Relatives.FirstOrDefault(r => r.IdNumber == item.IdNumber);
                            if (relativeInContext == null)
                            //מוסיף בן משפחה למערכת אם הוא לא קיים במערכת
                            {
                                Relative newRelative = new Relative { LastName = item.LastName, FirstName = item.FirstName, IdNumber = item.IdNumber, BirthDate = item.BirthDate, PassportNumber = item.PassportNumber, Profession = item.Profession, LicenseNumber = item.LicenseNumber, LicenseDate = item.LicenseDate, Gender = isMale };
                                context.Relatives.Add(newRelative);
                                context.SaveChanges();
                                relativeInContext = context.Relatives.FirstOrDefault(r => r.IdNumber == newRelative.IdNumber);
                            }
                            var clientRelativeInContext = context.ClientRelatives.FirstOrDefault(cr => cr.ClientID == clientInContext.ClientID && cr.RelativeID == relativeInContext.RelativeID);
                            if (clientRelativeInContext == null)
                            //משייך בן משפחה ללקוח אם בן המשפחה קיים אבל לא שייך ללקוח
                            {
                                ClientRelative clientRelative = new ClientRelative() { ClientID = clientInContext.ClientID, RelativeID = relativeInContext.RelativeID, Relationship = item.Relationship };
                                context.ClientRelatives.Add(clientRelative);
                                context.SaveChanges();
                            }
                            if (clientRelativeInContext != null)
                            //מעדכן בן משפחה קיים ששייך כבר ללקוח
                            {
                                clientRelativeInContext.Relationship = item.Relationship;
                                relativeInContext.LastName = item.LastName;
                                relativeInContext.FirstName = item.FirstName;
                                relativeInContext.BirthDate = item.BirthDate;
                                relativeInContext.LicenseNumber = item.LicenseNumber;
                                relativeInContext.LicenseDate = item.LicenseDate;
                                relativeInContext.Profession = item.Profession;
                                relativeInContext.PassportNumber = item.PassportNumber;
                                relativeInContext.Gender = isMale;
                                context.SaveChanges();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }
                else
                {
                    throw new Exception("לקוח לא קיים במערכת");
                }
            }
        }

        //חיפוש לקוחות לפי קטגוריה ו/אן מילת מפתח
        public List<Client> FindClientByString(string key, Category category, string typeName)
        {
            //using (Coral_DB_Entities context = new Coral_DB_Entities())
            //{
            //    var type= context.ClientTypes.FirstOrDefault(c=>c.ClientTypeName==typeName);
            //    if (category != null && category.CategoryName != "" && category.CategoryName != "הכל")
            //    {
            //        return (from c in context.Clients
            //                where c.ClientType.ClientTypeName == typeName &&
            //                c.CategoryID == category.CategoryID &&
            //                (c.LastName.StartsWith(key) || c.FirstName.StartsWith(key) || c.IdNumber.StartsWith(key))
            //                select c).OrderBy(c => c.LastName).ToList();
            //    }
            //    else
            //    {
            //        //return (from c in context.Clients
            //        //        where c.ClientType.ClientTypeName == typeName &&
            //        //        (c.LastName.StartsWith(key) || c.FirstName.StartsWith(key) || c.IdNumber.StartsWith(key))
            //        //        select c).OrderBy(c => c.LastName).ToList();

            //        return context.Clients.Where(c=>c.ClientTypeID==type.ClientTypeID&&(c.LastName.StartsWith(key)||c.FirstName.StartsWith(key)||c.IdNumber.StartsWith(key))).ToList();
            //    }
            //}
            var clients = GetAllClientsByType(typeName);
            if (category != null && category.CategoryName != "" && category.CategoryName != "הכל")
            {
                return clients.Where(c => c.CategoryID == category.CategoryID && (c.LastName.StartsWith(key) || c.FirstName.StartsWith(key) || c.IdNumber.StartsWith(key))).ToList();
            }
            else
            {
                return clients.Where(c => c.LastName.StartsWith(key) || c.FirstName.StartsWith(key) || c.IdNumber.StartsWith(key)).ToList();
            }
        }

        //מחזיר רשימה של לקוחות התואמים עם כל הפרמטרים של החיפוש
        public List<Client> AdvanceSearchAllCellsMatch(ClientType type, Category category, DateTime? openDate, string clientNumber, Agent principalAgent, string lastName, string firstName, string idNumber, string city, string street, string homePhone, string cellPhone, string workPhone, string email, string companyName)
        {
            var allClients = GetAllClientsByType(type.ClientTypeName);
            if (idNumber != "")
            {
                allClients = allClients.Where(c =>c.IdNumber != null && c.IdNumber.StartsWith(idNumber)).ToList();
            }
            if (clientNumber != "")
            {
                allClients = allClients.Where(c =>c.ClientNumber!= null && c.ClientNumber.StartsWith(clientNumber)).ToList();
            }
            if (lastName != "")
            {
                allClients = allClients.Where(c =>c.LastName != null && c.LastName.StartsWith(lastName)).ToList();
            }
            if (firstName != "")
            {
                allClients = allClients.Where(c =>c.FirstName != null && c.FirstName.StartsWith(firstName)).ToList();
            }
            if (openDate != null)
            {
                allClients = allClients.Where(c => c.OpenDate == openDate).ToList();
            }
            if (city != "")
            {
                allClients = allClients.Where(c =>c.City!=null&& c.City.StartsWith(city)).ToList();
            }
            if (street != "")
            {
                allClients = allClients.Where(c =>c.Street != null && c.Street.StartsWith(street)).ToList();
            }
            if (category != null && category.CategoryName != "הכל")
            {
                allClients = allClients.Where(c => c.CategoryID == category.CategoryID).ToList();
            }
            if (principalAgent != null)
            {
                allClients = allClients.Where(c => c.PrincipalAgentID == principalAgent.AgentID||c.SecundaryAgentID==principalAgent.AgentID).ToList();
            }
            if (homePhone != "")
            {
                allClients = allClients.Where(c =>c.PhoneHome != null && c.PhoneHome.StartsWith(homePhone)).ToList();
            }
            if (cellPhone != "")
            {
                allClients = allClients.Where(c =>c.CellPhone != null && c.CellPhone.StartsWith(cellPhone)).ToList();
            }
            if (workPhone != "")
            {
                allClients = allClients.Where(c =>c.PhoneWork != null && c.PhoneWork.StartsWith(workPhone)).ToList();
            }
            if (email != "")
            {
                allClients = allClients.Where(c =>c.Email != null && c.Email.StartsWith(email)).ToList();
            }
            if (companyName != "")
            {
                allClients = allClients.Where(c =>c.BusinessName != null && c.BusinessName.StartsWith(companyName)).ToList();
            }

            return allClients;
        }

        //מחזיר רשימה של לקוחות התואמים עם אחד או יותר מהפרמטרים של החיפוש
        public List<Client> AdvanceSearchOneCellMatch(ClientType type, Category category, DateTime? openDate, string clientNumber, Agent principalAgent, string lastName, string firstName, string idNumber, string city, string street, string policyNumber, string carNumber, string homePhone, string cellPhone, string workPhone, string email, string companyName)
        {
            var allClients = GetAllClientsByType(type.ClientTypeName);
            if (category != null && principalAgent != null)
            {
                allClients = allClients.Where(c => c.CategoryID == category.CategoryID ||
                    c.PrincipalAgentID == principalAgent.AgentID ||
                    (c.OpenDate != null && c.OpenDate == openDate) ||
                    (clientNumber != "" && c.ClientNumber != null && c.ClientNumber != "" && c.ClientNumber.StartsWith(clientNumber)) ||
                    (lastName != "" && c.LastName != null && c.LastName != "" && c.LastName.StartsWith(lastName)) ||
                    (firstName != "" && c.FirstName != null && c.FirstName != "" && c.FirstName.StartsWith(firstName)) ||
                    (idNumber != "" && c.IdNumber != null && c.IdNumber != "" && c.IdNumber.StartsWith(idNumber)) ||
                    (city != "" && c.City != null && c.City != "" && c.City.StartsWith(city)) ||
                    (street != "" && c.Street != null && c.Street != "" && c.Street.StartsWith(street)) ||
                    (homePhone != "" && c.PhoneHome != null && c.PhoneHome != "" && c.PhoneHome.StartsWith(homePhone)) ||
                    (cellPhone != "" && c.CellPhone != null && c.CellPhone != "" && c.CellPhone.StartsWith(cellPhone)) ||
                    (workPhone != "" && c.PhoneWork != null && c.PhoneWork != "" && c.PhoneWork.StartsWith(workPhone)) ||
                    (email != "" && c.Email != null && c.Email != "" && c.Email.StartsWith(email)) ||
                    (companyName != "" && c.BusinessName != null && c.BusinessName != "" && c.BusinessName.StartsWith(companyName))).ToList();
            }
            else if (category != null)
            {
                allClients = allClients.Where(c => c.CategoryID == category.CategoryID ||
                    (c.OpenDate != null && c.OpenDate == openDate) ||
                    (clientNumber != "" && c.ClientNumber != null && c.ClientNumber != "" && c.ClientNumber.StartsWith(clientNumber)) ||
                    (lastName != "" && c.LastName != null && c.LastName != "" && c.LastName.StartsWith(lastName)) ||
                    (firstName != "" && c.FirstName != null && c.FirstName != "" && c.FirstName.StartsWith(firstName)) ||
                    (idNumber != "" && c.IdNumber != null && c.IdNumber != "" && c.IdNumber.StartsWith(idNumber)) ||
                    (city != "" && c.City != null && c.City != "" && c.City.StartsWith(city)) ||
                    (street != "" && c.Street != null && c.Street != "" && c.Street.StartsWith(street)) ||
                    (homePhone != "" && c.PhoneHome != null && c.PhoneHome != "" && c.PhoneHome.StartsWith(homePhone)) ||
                    (cellPhone != "" && c.CellPhone != null && c.CellPhone != "" && c.CellPhone.StartsWith(cellPhone)) ||
                    (workPhone != "" && c.PhoneWork != null && c.PhoneWork != "" && c.PhoneWork.StartsWith(workPhone)) ||
                    (email != "" && c.Email != null && c.Email != "" && c.Email.StartsWith(email)) ||
                    (companyName != "" && c.BusinessName != null && c.BusinessName != "" && c.BusinessName.StartsWith(companyName))).ToList();
            }
            else if (principalAgent != null)
            {
                allClients = allClients.Where(c => c.PrincipalAgentID == principalAgent.AgentID ||
                    (c.OpenDate != null && c.OpenDate == openDate) ||
                    (clientNumber != "" && c.ClientNumber != null && c.ClientNumber != "" && c.ClientNumber.StartsWith(clientNumber)) ||
                    (lastName != "" && c.LastName != null && c.LastName != "" && c.LastName.StartsWith(lastName)) ||
                    (firstName != "" && c.FirstName != null && c.FirstName != "" && c.FirstName.StartsWith(firstName)) ||
                    (idNumber != "" && c.IdNumber != null && c.IdNumber != "" && c.IdNumber.StartsWith(idNumber)) ||
                    (city != "" && c.City != null && c.City != "" && c.City.StartsWith(city)) ||
                    (street != "" && c.Street != null && c.Street != "" && c.Street.StartsWith(street)) ||
                    (homePhone != "" && c.PhoneHome != null && c.PhoneHome != "" && c.PhoneHome.StartsWith(homePhone)) ||
                    (cellPhone != "" && c.CellPhone != null && c.CellPhone != "" && c.CellPhone.StartsWith(cellPhone)) ||
                    (workPhone != "" && c.PhoneWork != null && c.PhoneWork != "" && c.PhoneWork.StartsWith(workPhone)) ||
                    (email != "" && c.Email != null && c.Email != "" && c.Email.StartsWith(email)) ||
                    (companyName != "" && c.BusinessName != null && c.BusinessName != "" && c.BusinessName.StartsWith(companyName))).ToList();
            }
            else
            {
                allClients = allClients.Where(c => (c.OpenDate != null && c.OpenDate == openDate) ||
                    (clientNumber!=""&& c.ClientNumber != null && c.ClientNumber != "" && c.ClientNumber.StartsWith(clientNumber)) ||
                    (lastName!=""&& c.LastName != null && c.LastName != "" && c.LastName.StartsWith(lastName)) ||
                    (firstName!=""&& c.FirstName != null && c.FirstName != "" && c.FirstName.StartsWith(firstName)) ||
                    (idNumber != "" && c.IdNumber != null && c.IdNumber != "" && c.IdNumber.StartsWith(idNumber)) ||
                    (city != "" && c.City != null && c.City != "" && c.City.StartsWith(city)) ||
                    (street != "" && c.Street != null && c.Street != "" && c.Street.StartsWith(street)) ||
                    (homePhone != "" && c.PhoneHome != null && c.PhoneHome != "" && c.PhoneHome.StartsWith(homePhone)) ||
                    (cellPhone != "" && c.CellPhone != null && c.CellPhone != "" && c.CellPhone.StartsWith(cellPhone)) ||
                    (workPhone != "" && c.PhoneWork != null && c.PhoneWork != "" && c.PhoneWork.StartsWith(workPhone)) ||
                    (email != "" && c.Email != null && c.Email != "" && c.Email.StartsWith(email)) ||
                    (companyName != "" && c.BusinessName != null && c.BusinessName != "" && c.BusinessName.StartsWith(companyName))).ToList();
            }
            
            return allClients;
        }

        //מחזיר לקוח לפי מס' זיהוי בבסיס הנתונים
        public Client GetClientByClientID(int clientID)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.Clients.Include("ClientType").FirstOrDefault(c => c.ClientID == clientID);
            }
        }

        //מחזיר את מס' הזיהוי בבסיס הנתונים של לקוח לפי ת.ז
        public int GetClientID(string idNumber)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                Client client = context.Clients.FirstOrDefault(c => c.IdNumber == idNumber);
                return client.ClientID;
            }
        }

        //מחזיר רשימה של לקוחות לפי חודש לידה
        public List<Client> GetClientsByBirthMonth(int month)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.Clients.Where(c => ((DateTime)c.BirthDate).Month == month).ToList();
            }
        }

        //מעדכן סוג לקוח
        public void UpdateClientType(Client client, ClientType type)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                try
                {
                    var clientInContext = context.Clients.FirstOrDefault(c => c.ClientID == client.ClientID);
                    if (clientInContext != null)
                    {
                        clientInContext.ClientTypeID = type.ClientTypeID;
                        context.SaveChanges();
                    }
                    else
                    {
                        throw new Exception("לקוח לא קיים במערכת");
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
        }


        public List<Client> FindClientByPolicyNumber(string policyNumber)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                List<Client> clients = new List<Client>();
                if (policyNumber != "")
                {
                    var elementaryPolicy = context.ElementaryPolicies.FirstOrDefault(p => p.PolicyNumber == policyNumber);
                    if (elementaryPolicy != null)
                    {
                        var client = context.Clients.Include("ClientType").FirstOrDefault(c => c.ClientID == elementaryPolicy.ClientID);
                        clients.Add(client);
                        return clients;
                    }
                    var lifePolicy = context.LifePolicies.FirstOrDefault(p => p.PolicyNumber == policyNumber);
                    if (lifePolicy != null)
                    {
                        var client = context.Clients.Include("ClientType").FirstOrDefault(c => c.ClientID == lifePolicy.ClientID);
                        clients.Add(client);
                        return clients;
                    }
                    var healthPolicy = context.HealthPolicies.FirstOrDefault(p => p.PolicyNumber == policyNumber);
                    if (healthPolicy != null)
                    {
                        var client = context.Clients.Include("ClientType").FirstOrDefault(c => c.ClientID == healthPolicy.ClientID);
                        clients.Add(client);
                        return clients;
                    }
                    var financePolicy = context.FinancePolicies.FirstOrDefault(p => p.AssociateNumber == policyNumber);
                    if (financePolicy != null)
                    {
                        var client = context.Clients.Include("ClientType").FirstOrDefault(c => c.ClientID == financePolicy.ClientID);
                        clients.Add(client);
                        return clients;
                    }
                    var travelPolicy = context.TravelPolicies.FirstOrDefault(p => p.PolicyNumber == policyNumber);
                    if (travelPolicy != null)
                    {
                        var client = context.Clients.Include("ClientType").FirstOrDefault(c => c.ClientID == travelPolicy.ClientID);
                        clients.Add(client);
                        return clients;
                    }
                    var personalAccidentsPolicy = context.PersonalAccidentsPolicies.FirstOrDefault(p => p.PolicyNumber == policyNumber);
                    if (personalAccidentsPolicy != null)
                    {
                        var client = context.Clients.Include("ClientType").FirstOrDefault(c => c.ClientID == personalAccidentsPolicy.ClientID);
                        clients.Add(client);
                        return clients;
                    }
                    var foreingWorkersPolicy = context.ForeingWorkersPolicies.FirstOrDefault(p => p.PolicyNumber == policyNumber);
                    if (foreingWorkersPolicy != null)
                    {
                        var client = context.Clients.Include("ClientType").FirstOrDefault(c => c.ClientID == foreingWorkersPolicy.ClientID);
                        clients.Add(client);
                        return clients;
                    }
                }
                return clients;
            }
        }

        public List<Client> FindClientByCarNumber(string carNumber)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                List<Client> clients = new List<Client>();
                if (carNumber != "")
                {
                    var carPolicy = context.CarPolicies.Where(p => p.RegistrationNumber == carNumber).OrderByDescending(p => p.ElementaryPolicy.StartDate).FirstOrDefault();
                    if (carPolicy != null)
                    {
                        var client = context.Clients.Include("ClientType").FirstOrDefault(c => c.ClientID == carPolicy.ElementaryPolicy.ClientID);
                        clients.Add(client);
                        return clients;
                    }
                }
                return clients;
            }
        }

        public List<DangerousHobby> GetHobbies()
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.DangerousHobbies.OrderByDescending(h => h.Status).ThenBy(h => h.HobbyName).ToList();
            }
        }

        public List<DangerousHobby> GetActiveHobbies()
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.DangerousHobbies.Where(h => h.Status == true).OrderBy(h => h.HobbyName).ToList();
            }
        }

        public DangerousHobby InsertHobby(string name, bool status)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var hobbyInContext = context.DangerousHobbies.FirstOrDefault(r => r.HobbyName == name);
                if (hobbyInContext == null)
                {
                    try
                    {
                        DangerousHobby newHobby = new DangerousHobby() { HobbyName = name, Status = status };
                        context.DangerousHobbies.Add(newHobby);
                        context.SaveChanges();
                        return newHobby;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }
                else
                {
                    throw new Exception("תחביב קיים במערכת");
                }
            }
        }

        public DangerousHobby UpdateHobbyName(string name, bool status)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var hobbyInContext = context.DangerousHobbies.FirstOrDefault(r => r.HobbyName == name);
                if (hobbyInContext != null)
                {
                    try
                    {
                        hobbyInContext.Status = status;
                        context.SaveChanges();
                        return hobbyInContext;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }
                else
                {
                    throw new Exception("תחביב לא קיים במערכת");
                }
            }
        }

        public void InsertPicture(int clientId, string picturePath)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var clientInContext = context.Clients.FirstOrDefault(c => c.ClientID == clientId);
                if (clientInContext != null)
                {
                    clientInContext.PicturePath = picturePath;
                    context.SaveChanges();
                }
            }
        }

        public List<Client> GetReferedByClient(int clientId)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var clientInContext = context.Clients.FirstOrDefault(c => c.ClientID == clientId);
                if (clientInContext != null)
                {
                    return context.Clients.Where(c => c.ReferedByClientID == clientId).ToList();
                }
                return null;
            }
        }
    }
}

