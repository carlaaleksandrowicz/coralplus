//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CoralBusinessLogics
{
    using System;
    using System.Collections.Generic;
    
    public partial class ForeingWorkersRenewalTracking
    {
        public int ForeingWorkersRenewalTrackingID { get; set; }
        public int ForeingWorkersPolicyID { get; set; }
        public int UserID { get; set; }
        public Nullable<System.DateTime> Date { get; set; }
        public string TrackingContent { get; set; }
    
        public virtual ForeingWorkersPolicy ForeingWorkersPolicy { get; set; }
        public virtual User User { get; set; }
    }
}
