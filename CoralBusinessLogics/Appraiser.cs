//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CoralBusinessLogics
{
    using System;
    using System.Collections.Generic;
    
    public partial class Appraiser
    {
        public Appraiser()
        {
            this.ApartmentPolicies = new HashSet<ApartmentPolicy>();
            this.BusinessPolicies = new HashSet<BusinessPolicy>();
        }
    
        public int AppraiserID { get; set; }
        public string AppraiserName { get; set; }
        public Nullable<bool> Status { get; set; }
    
        public virtual ICollection<ApartmentPolicy> ApartmentPolicies { get; set; }
        public virtual ICollection<BusinessPolicy> BusinessPolicies { get; set; }
    }
}
