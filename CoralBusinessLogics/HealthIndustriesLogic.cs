﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoralBusinessLogics
{
    public class HealthIndustriesLogic
    {
        //מחזיר רשימה של כל הענפים
        public List<HealthIndustry> GetAllHealthIndustries()
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.HealthIndustries.OrderBy(i => i.HealthIndustryName).ToList();
            }
        }

        //מוסיף ענף חדש
        public void InsertHealthIndustry(string industryName)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var industryInContext = context.HealthIndustries.FirstOrDefault(i => i.HealthIndustryName == industryName);
                if (industryInContext == null)
                {
                    try
                    {
                        HealthIndustry newIndustry = new HealthIndustry() { HealthIndustryName = industryName };
                        context.HealthIndustries.Add(newIndustry);
                        context.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }
                else
                {
                    throw new Exception("ענף קיים");
                }
            }
        }

        //מחזיר רשימה של כל סוגי ביטוחי בריאות לפי ענף
        public List<HealthInsuranceType> GetHealthInsuranceTypesByCompany(int companyId)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.HealthInsuranceTypes.Where(i=>i.CompanyID==companyId).OrderByDescending(i=>i.Status).ThenBy(i => i.HealthInsuranceTypeName).ToList();
            }
        }

        //מחזיר רשימה של סוגי ביטוחי בריאות פעילים
        public List<HealthInsuranceType> GetActiveHealthInsuranceTypesByCompany(int companyId)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.HealthInsuranceTypes.Where(i => i.CompanyID == companyId&&i.Status==true).OrderBy(i => i.HealthInsuranceTypeName).ToList();
            }
        }

        //מחזיר רשימה של סוגי ביטוחי בריאות 
        public List<HealthInsuranceType> GetAllHealthInsuranceTypes()
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.HealthInsuranceTypes.OrderBy(i => i.HealthInsuranceTypeName).ToList();
            }
        }
        //מוסיף סוג ביטוח חדש
        public HealthInsuranceType InsertHealthInsuranceType(string insuranceTypeName, bool status, int companyId)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var insuranceTypeInContext = context.HealthInsuranceTypes.FirstOrDefault(i => i.HealthInsuranceTypeName == insuranceTypeName&&i.CompanyID==companyId);
                if (insuranceTypeInContext == null)
                {
                    try
                    {
                        HealthInsuranceType newType = new HealthInsuranceType() {HealthInsuranceTypeName = insuranceTypeName, Status = status, CompanyID=companyId };
                        context.HealthInsuranceTypes.Add(newType);
                        context.SaveChanges();
                        return newType;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }
                else
                {
                    throw new Exception("סוג ביטוח קיים");
                }
            }
        }

        //מעדכן את הסטטוס של סוג ביטוח קיים
        public HealthInsuranceType UpdateHealthInsuranceTypeStatus(HealthInsuranceType insuranceType, bool status, int companyId)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var insuranceTypeInContext = context.HealthInsuranceTypes.FirstOrDefault(i => i.HealthInsuranceTypeName == insuranceType.HealthInsuranceTypeName && i.CompanyID == companyId);
                if (insuranceTypeInContext != null)
                {
                    try
                    {
                        insuranceTypeInContext.Status = status;
                        context.SaveChanges();
                        return insuranceTypeInContext;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }
                else
                {
                    throw new Exception("סוג ביטוח לא קיים");
                }

            }
        }
    }
}
