﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoralBusinessLogics
{
    public class HealthPoliciesLogic
    {
        public List<HealthPolicy> GetAllHealthPoliciesWithoutOffers()
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.HealthPolicies.Include("Insurance").Include("Client").Include("Client.Agent").Include("Client.Agent1").Include("Company").Include("HealthInsuranceType").Where(p => p.IsOffer == false).OrderByDescending(p => p.StartDate).ThenBy(p => p.Client.LastName).ToList();
            }
        }

        //מחזיר רשימה של פוליסות בריאות של לקוח מסוים
        public List<HealthPolicy> GetAllHealthPoliciesByClient(Client client)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                if (client != null)
                {
                    var policies = context.HealthPolicies.Where(p => p.ClientID == client.ClientID && p.IsOffer == false).OrderByDescending(p => p.StartDate).ThenByDescending(p => p.OpenDate).ToList();
                    var sortedPolicies = context.HealthPolicies.Include("Insurance").Include("Client").Include("Company").Include("HealthInsuranceType").Where(p => p.ClientID == client.ClientID && p.IsOffer == false).OrderByDescending(p => p.StartDate).ThenByDescending(p => p.OpenDate).ToList();
                    for (int i = policies.Count - 1; i >= 0; i--)
                    {
                        if (policies[i].Addition > 0)
                        {
                            var policy = policies.Where(p => p.PolicyNumber == policies[i].PolicyNumber && p.Addition == policies[i].Addition - 1).FirstOrDefault();
                            if (policy != null)
                            {
                                sortedPolicies.Remove(policies[i]);
                                var index = sortedPolicies.FindIndex(p => p.PolicyNumber == policy.PolicyNumber && p.Addition == policy.Addition);
                                sortedPolicies.Insert(index, policies[i]);
                            }
                        }
                    }
                    return sortedPolicies;
                }
                else
                {
                    return null;
                }
            }
        }

        public List<HealthPolicy> GetPoliciesWithoutOffersByStartDate(DateTime fromDate, DateTime? toDate)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.HealthPolicies.Include("Insurance").Include("Client").Include("Client.Agent").Include("Client.Agent1").Include("Company").Include("HealthInsuranceType").Where(p => p.IsOffer == false && p.StartDate >= fromDate && p.StartDate <= toDate).OrderByDescending(p => p.StartDate).ThenBy(p => p.Client.LastName).ToList();
            }
        }

        //מוסיף פוליסת בריאות חדשה
        public int InsertHealthPolicy(int clientId, bool isOffer, string policyNumber, int addition, DateTime openDate, int? companyId, int? principalAgentNumberId, int? subAgentNumberId, int insuranceTypeId, DateTime? startDate, DateTime? endDate, string primaryIndex, string insuredName, string insuredIdNumber, int? policyOwnerId, int? factoryNumberId, string paymentMethod, bool? isIndexation, decimal? subAnnual, decimal? policyFactor, decimal? totalMonthlyPayment, string bankPayment, string bankBranchPayment, string accountNumberPayment, bool? isMortgaged, string bankName, string bankBranch, string bankAddress, string bankPhone, string bankFax, string bankContactName, string bankEmail, string comments)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var policyInContext = context.HealthPolicies.FirstOrDefault(p => p.PolicyNumber == policyNumber && p.Addition == addition);
                if (policyInContext == null)
                {
                    try
                    {
                        var insuranceInContext = context.Insurances.FirstOrDefault(i => i.InsuranceName == "בריאות");
                        HealthPolicy newHealthPolicy = new HealthPolicy()
                        {
                            InsuranceID = insuranceInContext.InsuranceID,
                            ClientID = clientId,
                            IsOffer = isOffer,
                            PolicyNumber = policyNumber,
                            Addition = addition,
                            OpenDate = openDate,
                            CompanyID = companyId,
                            PrincipalAgentNumberID = principalAgentNumberId,
                            SubAgentNumberID = subAgentNumberId,
                            HealthInsuranceTypeID = insuranceTypeId,
                            StartDate = startDate,
                            EndDate = endDate,
                            PrimaryIndex = primaryIndex,
                            InsuredName = insuredName,
                            InsuredIdNumber = insuredIdNumber,
                            PolicyOwnerID = policyOwnerId,
                            FactoryNumberID = factoryNumberId,
                            PaymentMethod = paymentMethod,
                            SubAnnual = subAnnual,
                            PolicyFactor = policyFactor,
                            TotalMonthlyPayment = totalMonthlyPayment,
                            AccountNumber = accountNumberPayment,
                            IsMortgaged = isMortgaged,
                            Bank = bankPayment,
                            BankBranch = bankBranchPayment,
                            Hatzmada = isIndexation,
                            BankName = bankName,
                            BankBranchNumber = bankBranch,
                            BankAddress = bankAddress,
                            BankPhone1 = bankPhone,
                            BankFax = bankFax,
                            BankContactName = bankContactName,
                            BankEmail = bankEmail,
                            Comments = comments

                        };
                        context.HealthPolicies.Add(newHealthPolicy);
                        context.SaveChanges();
                        return newHealthPolicy.HealthPolicyID;
                    }
                    catch (Exception e)
                    {

                        throw new Exception(e.Message);
                    }
                }
                else
                {
                    throw new Exception("פוליסה קיימת במערכת");
                }
            }
        }

        public List<HealthPolicy> GetActivePoliciesWithoutOffers()
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.HealthPolicies.Include("Insurance").Include("Client").Include("Client.Agent").Include("Client.Agent1").Include("Company").Include("HealthInsuranceType").Where(p => p.IsOffer == false&&p.EndDate>DateTime.Today).OrderByDescending(p => p.StartDate).ThenBy(p => p.Client.LastName).ToList();
            }
        }

        public List<HealthPolicy> GetOffers()
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.HealthPolicies.Include("Client").Include("Company").Include("HealthInsuranceType").Where(p => p.IsOffer == true).OrderByDescending(p => p.StartDate).ThenBy(p => p.Client.LastName).ToList();
            }
        }

        public void TransferPolicyToClient(HealthPolicy policy, Client client)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var policyInContext = context.HealthPolicies.FirstOrDefault(p => p.HealthPolicyID == policy.HealthPolicyID);
                if (policyInContext != null)
                {
                    policyInContext.ClientID = client.ClientID;
                    context.SaveChanges();
                }
                else
                {
                    throw new Exception("פוליסה לא קיימת במערכת");
                }
            }
        }

        public List<HealthPolicy> GetAllHealthOffersByClient(Client client)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                if (client == null)
                {
                    return null;
                }
                return context.HealthPolicies.Include("Client").Include("Insurance").Include("Company").Include("HealthInsuranceType").Where(p => p.ClientID == client.ClientID && p.IsOffer == true).OrderByDescending(p => p.StartDate).ToList();
            }
        }


        //מעדכן פוליסה קיימת
        public void UpdateHealthPolicy(int healthPolicyId, string policyNumber, int? companyId, int? principalAgentNumberId, int? subAgentNumberId, int insuranceTypeId, DateTime? startDate, DateTime? endDate, string primaryIndex, string insuredName, string insuredNumber, int? policyOwnerId, int? factoryNumberId, string paymentMethod, bool? isIndexation, decimal? subAnnual, decimal? policyFactor, decimal? totalMonthlyPayment, string bankPayment, string bankBranchPayment, string accountNumberPayment, bool? isMortgaged, string bankName, string bankBranch, string bankAddress, string bankPhone, string bankFax, string bankContactName, string bankEmail, string comments)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var policyInContext = context.HealthPolicies.FirstOrDefault(p => p.HealthPolicyID == healthPolicyId);
                if (policyInContext != null)
                {
                    try
                    {
                        policyInContext.PolicyNumber = policyNumber;
                        policyInContext.CompanyID = companyId;
                        policyInContext.PrincipalAgentNumberID = principalAgentNumberId;
                        policyInContext.SubAgentNumberID = subAgentNumberId;
                        policyInContext.HealthInsuranceTypeID = insuranceTypeId;
                        policyInContext.StartDate = startDate;
                        policyInContext.EndDate = endDate;
                        policyInContext.PrimaryIndex = primaryIndex;
                        policyInContext.InsuredName = insuredName;
                        policyInContext.InsuredIdNumber = insuredNumber;
                        policyInContext.PolicyOwnerID = policyOwnerId;
                        policyInContext.FactoryNumberID = factoryNumberId;
                        policyInContext.PaymentMethod = paymentMethod;
                        policyInContext.Hatzmada = isIndexation;
                        policyInContext.SubAnnual = subAnnual;
                        policyInContext.PolicyFactor = policyFactor;
                        policyInContext.TotalMonthlyPayment = totalMonthlyPayment;
                        policyInContext.Bank = bankPayment;
                        policyInContext.BankBranch = bankBranchPayment;
                        policyInContext.AccountNumber = accountNumberPayment;
                        policyInContext.IsMortgaged = isMortgaged;
                        policyInContext.BankName = bankName;
                        policyInContext.BankBranchNumber = bankBranch;
                        policyInContext.BankAddress = bankAddress;
                        policyInContext.BankPhone1 = bankPhone;
                        policyInContext.BankFax = bankFax;
                        policyInContext.BankContactName = bankContactName;
                        policyInContext.BankEmail = bankEmail;
                        policyInContext.Comments = comments;

                        context.SaveChanges();

                    }
                    catch (Exception e)
                    {
                        throw new Exception(e.Message);
                    }
                }
                else
                {
                    throw new Exception("פוליסה לא קיימת במערכת");
                }
            }
        }

        public List<HealthPolicy> GetPoliciesByOwner(PolicyOwner owner)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.HealthPolicies.Include("Client").Include("Client.Agent").Include("Client.Agent1").Include("Insurance").Include("Company").Include("HealthInsuranceType").Where(p => p.IsOffer == false && p.PolicyOwnerID == owner.PolicyOwnerID).OrderByDescending(p => p.StartDate).ThenBy(p => p.Client.LastName).ToList();
            }
        }

        public string GetAdditionNumber(string policyNumber)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var policiesInContext = context.HealthPolicies.Where(p => p.PolicyNumber == policyNumber);
                int? addition = 0;
                foreach (var item in policiesInContext)
                {
                    if (item.Addition > addition)
                    {
                        addition = item.Addition;
                    }
                }
                return (addition + 1).ToString();
            }
        }


        public List<HealthPolicy> GetActiveHealthPoliciesWithoutAdditionsByClient(Client client)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.HealthPolicies.Include("Insurance").Where(p => p.ClientID == client.ClientID && p.Addition == 0 && p.IsOffer == false).OrderByDescending(p => p.StartDate).ToList();
            }
        }

        public List<HealthPolicy> GetActiveAllHealthPoliciesByClient(Client client)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                if (client != null)
                {
                    var policies = context.HealthPolicies.Where(p => p.ClientID == client.ClientID && p.EndDate >= DateTime.Today && p.IsOffer == false).OrderByDescending(p => p.StartDate).ThenByDescending(p => p.OpenDate).ToList();
                    var sortedPolicies = context.HealthPolicies.Include("Insurance").Include("Client").Include("Company").Include("HealthInsuranceType").Where(p => p.ClientID == client.ClientID && p.EndDate >= DateTime.Today && p.IsOffer == false).OrderByDescending(p => p.StartDate).ThenByDescending(p => p.OpenDate).ToList();
                    for (int i = policies.Count - 1; i >= 0; i--)
                    {
                        if (policies[i].Addition > 0)
                        {
                            var policy = policies.Where(p => p.PolicyNumber == policies[i].PolicyNumber && p.Addition == policies[i].Addition - 1).FirstOrDefault();
                            if (policy != null)
                            {
                                sortedPolicies.Remove(policies[i]);
                                var index = sortedPolicies.FindIndex(p => p.PolicyNumber == policy.PolicyNumber && p.Addition == policy.Addition);
                                sortedPolicies.Insert(index, policies[i]);
                            }
                        }
                    }
                    return sortedPolicies;
                }
                else
                {
                    return null;
                }
            }
        }

        public List<HealthPolicy> GetNonActiveLifePoliciesByClient(Client client)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                if (client != null)
                {
                    var policies = context.HealthPolicies.Where(p => p.ClientID == client.ClientID && p.EndDate < DateTime.Today && p.IsOffer == false).OrderByDescending(p => p.StartDate).ThenByDescending(p => p.OpenDate).ToList();
                    var sortedPolicies = context.HealthPolicies.Include("Insurance").Include("Client").Include("Company").Include("HealthInsuranceType").Where(p => p.ClientID == client.ClientID && p.EndDate < DateTime.Today && p.IsOffer == false).OrderByDescending(p => p.StartDate).ThenByDescending(p => p.OpenDate).ToList();
                    for (int i = policies.Count - 1; i >= 0; i--)
                    {
                        if (policies[i].Addition > 0)
                        {
                            var policy = policies.Where(p => p.PolicyNumber == policies[i].PolicyNumber && p.Addition == policies[i].Addition - 1).FirstOrDefault();
                            if (policy != null)
                            {
                                sortedPolicies.Remove(policies[i]);
                                var index = sortedPolicies.FindIndex(p => p.PolicyNumber == policy.PolicyNumber && p.Addition == policy.Addition);
                                sortedPolicies.Insert(index, policies[i]);
                            }
                        }
                    }
                    return sortedPolicies;
                }
                else
                {
                    return null;
                }
            }
        }

        public void ChangeHealthPolicyOfferStatus(int healthPolicyID, bool isOffer)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var policyInContext = context.HealthPolicies.FirstOrDefault(p => p.HealthPolicyID == healthPolicyID);
                policyInContext.IsOffer = isOffer;
                context.SaveChanges();
            }
        }

        public HealthPolicy GetHealthPolicyByPolicyId(int policyId)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.HealthPolicies.Include("Client").FirstOrDefault(p => p.HealthPolicyID == policyId);
            }
        }
    }
}
