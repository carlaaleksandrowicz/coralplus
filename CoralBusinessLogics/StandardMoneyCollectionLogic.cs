﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoralBusinessLogics
{
    public class StandardMoneyCollectionLogic
    {
        public List<MoneyCollectionType> GetAllMoneyCollectionTypes()
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.MoneyCollectionTypes.OrderByDescending(a => a.Status).ThenBy(a => a.MoneyCollectionTypeName).ToList();
            }
        }

        public List<MoneyCollectionType> GetActiveMoneyCollectionTypes()
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.MoneyCollectionTypes.Where(a => a.Status == true).OrderBy(a => a.MoneyCollectionTypeName).ToList();
            }
        }

        public MoneyCollectionType InsertMoneyCollectionType(string typeName, bool status)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var typeInContext = context.MoneyCollectionTypes.FirstOrDefault(a => a.MoneyCollectionTypeName == typeName);
                if (typeInContext == null)
                {
                    try
                    {
                        MoneyCollectionType newType = new MoneyCollectionType() { MoneyCollectionTypeName = typeName, Status = status };
                        context.MoneyCollectionTypes.Add(newType);
                        context.SaveChanges();
                        return newType;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }
                else
                {
                    throw new Exception("סוג גבייה קיים במערכת");
                }
            }
        }

        public List<StandardMoneyCollection> GetAllStandardMoneyCollections()
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.StandardMoneyCollections.Include("ElementaryPolicy").Include("ElementaryPolicy.Client").Include("ElementaryPolicy.Company").Include("ElementaryPolicy.InsuranceIndustry").Include("PersonalAccidentsPolicy").Include("PersonalAccidentsPolicy.Client").Include("PersonalAccidentsPolicy.Company").Include("TravelPolicy").Include("TravelPolicy.Client").Include("TravelPolicy.Company").Include("LifePolicy").Include("LifePolicy.Client").Include("LifePolicy.Company").Include("LifePolicy.LifeIndustry").Include("HealthPolicy").Include("HealthPolicy.Client").Include("HealthPolicy.Company").Include("FinancePolicy").Include("FinancePolicy.Client").Include("FinancePolicy.Company").Include("FinancePolicy.FinanceProgramType").Include("ForeingWorkersPolicy").Include("ForeingWorkersPolicy.Client").Include("ForeingWorkersPolicy.Company").Include("ForeingWorkersPolicy.ForeingWorkersIndustry").Include("MoneyCollectionType").ToList();
            }
        }

        //מעדכן סוג גבייה במערכת
        public MoneyCollectionType UpdateMoneyCollectionTypeStatus(string typeName, bool status)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var typeInContext = context.MoneyCollectionTypes.FirstOrDefault(a => a.MoneyCollectionTypeName == typeName);
                if (typeInContext != null)
                {
                    try
                    {
                        typeInContext.Status = status;
                        context.SaveChanges();
                        return typeInContext;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }
                else
                {
                    throw new Exception("סוג גבייה לא קיים במערכת");
                }
            }
        }

        public List<StandardMoneyCollectionTracking> GetTrakcingsByStsndardMoneyCollection(StandardMoneyCollection standardMoneyCollection)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.StandardMoneyCollectionTrackings.Include("User").Where(t => t.StandardMoneyCollectionID == standardMoneyCollection.StandardMoneyCollectionID).ToList();
            }
        }

        public int InsertStandardMoneyCollection(StandardMoneyCollection standardMoneyCollection, Insurance insurance, int policyId)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                try
                {
                    if (standardMoneyCollection == null)
                    {
                        standardMoneyCollection = new StandardMoneyCollection();
                        standardMoneyCollection.IsFirstLetterSended = false;
                        standardMoneyCollection.IsPolicyCanceled = false;
                        standardMoneyCollection.IsSecondLetterSended = false;
                        standardMoneyCollection.IsStandardMoneyCollectionOpen = true;
                    }
                    if (insurance.InsuranceName == "אלמנטרי")
                    {

                        if (standardMoneyCollection.ElementaryPolicyID == 0 || standardMoneyCollection.ElementaryPolicyID == null)
                        {
                            standardMoneyCollection.ElementaryPolicyID = policyId;
                            context.StandardMoneyCollections.Add(standardMoneyCollection);
                        }
                        else
                        {
                            var standardMoneyCollectionInContext = context.StandardMoneyCollections.FirstOrDefault(s => s.ElementaryPolicyID == policyId);
                            standardMoneyCollectionInContext.Comments = standardMoneyCollection.Comments;
                            standardMoneyCollectionInContext.IsFirstLetterSended = standardMoneyCollection.IsFirstLetterSended;
                            standardMoneyCollectionInContext.IsPolicyCanceled = standardMoneyCollection.IsPolicyCanceled;
                            standardMoneyCollectionInContext.IsSecondLetterSended = standardMoneyCollection.IsSecondLetterSended;
                            standardMoneyCollectionInContext.IsStandardMoneyCollectionOpen = standardMoneyCollection.IsStandardMoneyCollectionOpen;
                            standardMoneyCollectionInContext.MoneyCollectionAmount = standardMoneyCollection.MoneyCollectionAmount;
                            standardMoneyCollectionInContext.MoneyCollectionTypeID = standardMoneyCollection.MoneyCollectionTypeID;
                            standardMoneyCollectionInContext.PotencialCancelationDate = standardMoneyCollection.PotencialCancelationDate;
                        }

                    }
                    else if (insurance.InsuranceName == "תאונות אישיות")
                    {
                        if (standardMoneyCollection.PersonalAccidentsPolicyID == 0 || standardMoneyCollection.PersonalAccidentsPolicyID == null)
                        {
                            standardMoneyCollection.PersonalAccidentsPolicyID = policyId;
                            context.StandardMoneyCollections.Add(standardMoneyCollection);
                        }
                        else
                        {
                            var standardMoneyCollectionInContext = context.StandardMoneyCollections.FirstOrDefault(s => s.PersonalAccidentsPolicyID == policyId);
                            standardMoneyCollectionInContext.Comments = standardMoneyCollection.Comments;
                            standardMoneyCollectionInContext.IsFirstLetterSended = standardMoneyCollection.IsFirstLetterSended;
                            standardMoneyCollectionInContext.IsPolicyCanceled = standardMoneyCollection.IsPolicyCanceled;
                            standardMoneyCollectionInContext.IsSecondLetterSended = standardMoneyCollection.IsSecondLetterSended;
                            standardMoneyCollectionInContext.IsStandardMoneyCollectionOpen = standardMoneyCollection.IsStandardMoneyCollectionOpen;
                            standardMoneyCollectionInContext.MoneyCollectionAmount = standardMoneyCollection.MoneyCollectionAmount;
                            standardMoneyCollectionInContext.MoneyCollectionTypeID = standardMoneyCollection.MoneyCollectionTypeID;
                            standardMoneyCollectionInContext.PotencialCancelationDate = standardMoneyCollection.PotencialCancelationDate;
                        }
                    }
                    else if (insurance.InsuranceName == "נסיעות לחו''ל")
                    {
                        if (standardMoneyCollection.TravelPolicyID == 0 || standardMoneyCollection.TravelPolicyID == null)
                        {
                            standardMoneyCollection.TravelPolicyID = policyId;
                            context.StandardMoneyCollections.Add(standardMoneyCollection);
                        }
                        else
                        {
                            var standardMoneyCollectionInContext = context.StandardMoneyCollections.FirstOrDefault(s => s.TravelPolicyID == policyId);
                            standardMoneyCollectionInContext.Comments = standardMoneyCollection.Comments;
                            standardMoneyCollectionInContext.IsFirstLetterSended = standardMoneyCollection.IsFirstLetterSended;
                            standardMoneyCollectionInContext.IsPolicyCanceled = standardMoneyCollection.IsPolicyCanceled;
                            standardMoneyCollectionInContext.IsSecondLetterSended = standardMoneyCollection.IsSecondLetterSended;
                            standardMoneyCollectionInContext.IsStandardMoneyCollectionOpen = standardMoneyCollection.IsStandardMoneyCollectionOpen;
                            standardMoneyCollectionInContext.MoneyCollectionAmount = standardMoneyCollection.MoneyCollectionAmount;
                            standardMoneyCollectionInContext.MoneyCollectionTypeID = standardMoneyCollection.MoneyCollectionTypeID;
                            standardMoneyCollectionInContext.PotencialCancelationDate = standardMoneyCollection.PotencialCancelationDate;
                        }
                    }
                    else if (insurance.InsuranceName == "חיים")
                    {
                        if (standardMoneyCollection.LifePolicyID == 0 || standardMoneyCollection.LifePolicyID == null)
                        {
                            standardMoneyCollection.LifePolicyID = policyId;
                            context.StandardMoneyCollections.Add(standardMoneyCollection);
                        }
                        else
                        {
                            var standardMoneyCollectionInContext = context.StandardMoneyCollections.FirstOrDefault(s => s.LifePolicyID == policyId);
                            standardMoneyCollectionInContext.Comments = standardMoneyCollection.Comments;
                            standardMoneyCollectionInContext.IsFirstLetterSended = standardMoneyCollection.IsFirstLetterSended;
                            standardMoneyCollectionInContext.IsPolicyCanceled = standardMoneyCollection.IsPolicyCanceled;
                            standardMoneyCollectionInContext.IsSecondLetterSended = standardMoneyCollection.IsSecondLetterSended;
                            standardMoneyCollectionInContext.IsStandardMoneyCollectionOpen = standardMoneyCollection.IsStandardMoneyCollectionOpen;
                            standardMoneyCollectionInContext.MoneyCollectionAmount = standardMoneyCollection.MoneyCollectionAmount;
                            standardMoneyCollectionInContext.MoneyCollectionTypeID = standardMoneyCollection.MoneyCollectionTypeID;
                            standardMoneyCollectionInContext.PotencialCancelationDate = standardMoneyCollection.PotencialCancelationDate;
                        }
                    }
                    else if (insurance.InsuranceName == "בריאות")
                    {
                        if (standardMoneyCollection.HealthPolicyID == 0 || standardMoneyCollection.HealthPolicyID == null)
                        {
                            standardMoneyCollection.HealthPolicyID = policyId;
                            context.StandardMoneyCollections.Add(standardMoneyCollection);
                        }
                        else
                        {
                            var standardMoneyCollectionInContext = context.StandardMoneyCollections.FirstOrDefault(s => s.HealthPolicyID == policyId);
                            standardMoneyCollectionInContext.Comments = standardMoneyCollection.Comments;
                            standardMoneyCollectionInContext.IsFirstLetterSended = standardMoneyCollection.IsFirstLetterSended;
                            standardMoneyCollectionInContext.IsPolicyCanceled = standardMoneyCollection.IsPolicyCanceled;
                            standardMoneyCollectionInContext.IsSecondLetterSended = standardMoneyCollection.IsSecondLetterSended;
                            standardMoneyCollectionInContext.IsStandardMoneyCollectionOpen = standardMoneyCollection.IsStandardMoneyCollectionOpen;
                            standardMoneyCollectionInContext.MoneyCollectionAmount = standardMoneyCollection.MoneyCollectionAmount;
                            standardMoneyCollectionInContext.MoneyCollectionTypeID = standardMoneyCollection.MoneyCollectionTypeID;
                            standardMoneyCollectionInContext.PotencialCancelationDate = standardMoneyCollection.PotencialCancelationDate;
                        }
                    }
                    else if (insurance.InsuranceName == "פיננסים")
                    {
                        if (standardMoneyCollection.FinancePolicyID == 0 || standardMoneyCollection.FinancePolicyID == null)
                        {
                            standardMoneyCollection.FinancePolicyID = policyId;
                            context.StandardMoneyCollections.Add(standardMoneyCollection);
                        }
                        else
                        {
                            var standardMoneyCollectionInContext = context.StandardMoneyCollections.FirstOrDefault(s => s.FinancePolicyID == policyId);
                            standardMoneyCollectionInContext.Comments = standardMoneyCollection.Comments;
                            standardMoneyCollectionInContext.IsFirstLetterSended = standardMoneyCollection.IsFirstLetterSended;
                            standardMoneyCollectionInContext.IsPolicyCanceled = standardMoneyCollection.IsPolicyCanceled;
                            standardMoneyCollectionInContext.IsSecondLetterSended = standardMoneyCollection.IsSecondLetterSended;
                            standardMoneyCollectionInContext.IsStandardMoneyCollectionOpen = standardMoneyCollection.IsStandardMoneyCollectionOpen;
                            standardMoneyCollectionInContext.MoneyCollectionAmount = standardMoneyCollection.MoneyCollectionAmount;
                            standardMoneyCollectionInContext.MoneyCollectionTypeID = standardMoneyCollection.MoneyCollectionTypeID;
                            standardMoneyCollectionInContext.PotencialCancelationDate = standardMoneyCollection.PotencialCancelationDate;
                        }
                    }
                    else if (insurance.InsuranceName == "עובדים זרים")
                    {
                        if (standardMoneyCollection.ForeingWorkersPolicyID == 0 || standardMoneyCollection.ForeingWorkersPolicyID == null)
                        {
                            standardMoneyCollection.ForeingWorkersPolicyID = policyId;
                            context.StandardMoneyCollections.Add(standardMoneyCollection);
                        }
                        else
                        {
                            var standardMoneyCollectionInContext = context.StandardMoneyCollections.FirstOrDefault(s => s.ForeingWorkersPolicyID == policyId);
                            standardMoneyCollectionInContext.Comments = standardMoneyCollection.Comments;
                            standardMoneyCollectionInContext.IsFirstLetterSended = standardMoneyCollection.IsFirstLetterSended;
                            standardMoneyCollectionInContext.IsPolicyCanceled = standardMoneyCollection.IsPolicyCanceled;
                            standardMoneyCollectionInContext.IsSecondLetterSended = standardMoneyCollection.IsSecondLetterSended;
                            standardMoneyCollectionInContext.IsStandardMoneyCollectionOpen = standardMoneyCollection.IsStandardMoneyCollectionOpen;
                            standardMoneyCollectionInContext.MoneyCollectionAmount = standardMoneyCollection.MoneyCollectionAmount;
                            standardMoneyCollectionInContext.MoneyCollectionTypeID = standardMoneyCollection.MoneyCollectionTypeID;
                            standardMoneyCollectionInContext.PotencialCancelationDate = standardMoneyCollection.PotencialCancelationDate;
                        }
                    }
                    context.SaveChanges();
                    return standardMoneyCollection.StandardMoneyCollectionID;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
        }

        public void UpdateStandardMoneyCollection(StandardMoneyCollection standardMoneyCollection)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                try
                {
                    var standardMoneyCollectionInContext = context.StandardMoneyCollections.FirstOrDefault(s => s.StandardMoneyCollectionID == standardMoneyCollection.StandardMoneyCollectionID);
                    if (standardMoneyCollectionInContext != null)
                    {
                        standardMoneyCollectionInContext.Comments = standardMoneyCollection.Comments;
                        standardMoneyCollectionInContext.IsFirstLetterSended = standardMoneyCollection.IsFirstLetterSended;
                        standardMoneyCollectionInContext.IsPolicyCanceled = standardMoneyCollection.IsPolicyCanceled;
                        standardMoneyCollectionInContext.IsSecondLetterSended = standardMoneyCollection.IsSecondLetterSended;
                        standardMoneyCollectionInContext.IsStandardMoneyCollectionOpen = standardMoneyCollection.IsStandardMoneyCollectionOpen;
                        standardMoneyCollectionInContext.MoneyCollectionAmount = standardMoneyCollection.MoneyCollectionAmount;
                        standardMoneyCollectionInContext.MoneyCollectionTypeID = standardMoneyCollection.MoneyCollectionTypeID;
                        standardMoneyCollectionInContext.PotencialCancelationDate = standardMoneyCollection.PotencialCancelationDate;

                        context.SaveChanges();
                    }
                    else
                    {
                        throw new Exception("גבייה לא קיימת במערכת");
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
        }

        public StandardMoneyCollection GetStandardMoneyColletionByPolicy(object policy)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                try
                {
                    if (policy is ElementaryPolicy)
                    {
                        return context.StandardMoneyCollections.FirstOrDefault(s => s.ElementaryPolicyID == ((ElementaryPolicy)policy).ElementaryPolicyID);
                    }
                    else if (policy is PersonalAccidentsPolicy)
                    {
                        return context.StandardMoneyCollections.FirstOrDefault(s => s.PersonalAccidentsPolicyID == ((PersonalAccidentsPolicy)policy).PersonalAccidentsPolicyID);
                    }
                    else if (policy is TravelPolicy)
                    {
                        return context.StandardMoneyCollections.FirstOrDefault(s => s.TravelPolicyID == ((TravelPolicy)policy).TravelPolicyID);
                    }
                    else if (policy is LifePolicy)
                    {
                        return context.StandardMoneyCollections.FirstOrDefault(s => s.LifePolicyID == ((LifePolicy)policy).LifePolicyID);
                    }
                    else if (policy is HealthPolicy)
                    {
                        return context.StandardMoneyCollections.FirstOrDefault(s => s.HealthPolicyID == ((HealthPolicy)policy).HealthPolicyID);
                    }
                    else if (policy is FinancePolicy)
                    {
                        return context.StandardMoneyCollections.FirstOrDefault(s => s.FinancePolicyID == ((FinancePolicy)policy).FinancePolicyID);
                    }
                    else if (policy is ForeingWorkersPolicy)
                    {
                        return context.StandardMoneyCollections.FirstOrDefault(s => s.ForeingWorkersPolicyID == ((ForeingWorkersPolicy)policy).ForeingWorkersPolicyID);
                    }
                    else
                    {
                        return null;
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
        }

        public void InsertTrackingsToStandardMoneyCollection(int standardMoneyCollectionId, List<StandardMoneyCollectionTracking> trackings)
        {
            if (trackings!=null&&trackings.Count > 0)
            {
                using (Coral_DB_Entities context = new Coral_DB_Entities())
                {
                    var standardMoneyCollectionInContext = context.StandardMoneyCollections.FirstOrDefault(c => c.StandardMoneyCollectionID == standardMoneyCollectionId);
                    if (standardMoneyCollectionInContext != null)
                    {
                        try
                        {
                            var standardMoneyCollectionTrackings = context.StandardMoneyCollectionTrackings.Where(t => t.StandardMoneyCollectionID == standardMoneyCollectionId).ToList();
                            foreach (StandardMoneyCollectionTracking tracking in trackings)
                            {
                                if (tracking.StandardMoneyCollectionTrackingID == 0)
                                {
                                    tracking.User = null;
                                    tracking.StandardMoneyCollectionID = standardMoneyCollectionId;
                                    context.StandardMoneyCollectionTrackings.Add(tracking);
                                }                               
                                else
                                {
                                    var trakingInContext = context.StandardMoneyCollectionTrackings.FirstOrDefault(t => t.StandardMoneyCollectionTrackingID == tracking.StandardMoneyCollectionTrackingID);
                                    if (trakingInContext != null)
                                    {
                                        trakingInContext.TrackingContent = tracking.TrackingContent;
                                    }
                                    standardMoneyCollectionTrackings.Remove(trakingInContext);
                                }
                            }
                            if (standardMoneyCollectionTrackings.Count > 0)
                            {
                                foreach (var item in standardMoneyCollectionTrackings)
                                {
                                    context.StandardMoneyCollectionTrackings.Remove(item);
                                }
                            }
                            context.SaveChanges();
                        }
                        catch (Exception e)
                        {
                            throw new Exception(e.Message);
                        }
                    }
                    else
                    {
                        throw new Exception("גבייה רגילה לא קיימת במערכת");
                    }

                }
            }

        }
    }
}
