//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CoralBusinessLogics
{
    using System;
    using System.Collections.Generic;
    
    public partial class InsuranceCompany
    {
        public int InsuranceID { get; set; }
        public int CompanyID { get; set; }
        public Nullable<bool> Status { get; set; }
    
        public virtual Company Company { get; set; }
        public virtual Insurance Insurance { get; set; }
    }
}
