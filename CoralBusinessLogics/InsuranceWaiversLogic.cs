﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoralBusinessLogics
{
    public class InsuranceWaiversLogic
    {
        public List<InsurancesWaiver> GetAllInsuranceWaivers()
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.InsurancesWaivers.OrderByDescending(i => i.Status).ThenBy(i => i.InsurancesWaiverName).ToList();
            }
        }

        public List<InsurancesWaiver> GetActiveInsuranceWaivers()
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.InsurancesWaivers.Where(i => i.Status == true).OrderBy(i => i.InsurancesWaiverName).ToList();
            }
        }

        public InsurancesWaiver InsertInsuranceWaiver(string insuranceWaiverName, bool status)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var insuranceWaiverNameInContext = context.InsurancesWaivers.FirstOrDefault(i => i.InsurancesWaiverName == insuranceWaiverName);
                if (insuranceWaiverNameInContext == null)
                {
                    try
                    {
                        InsurancesWaiver newInsuranceWaiverName = new InsurancesWaiver() { InsurancesWaiverName = insuranceWaiverName, Status = status };
                        context.InsurancesWaivers.Add(newInsuranceWaiverName);
                        context.SaveChanges();
                        return newInsuranceWaiverName;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }
                else
                {
                    throw new Exception("שם ויתור על ביטוח קיים במערכת");
                }
            }
        }

        public InsurancesWaiver UpdateInsuranceWaiverStatus(string insuranceWaiverName, bool status)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var insuranceWaiverNameInContext = context.InsurancesWaivers.FirstOrDefault(i => i.InsurancesWaiverName == insuranceWaiverName);
                if (insuranceWaiverNameInContext != null)
                {
                    try
                    {
                        insuranceWaiverNameInContext.Status = status;
                        context.SaveChanges();
                        return insuranceWaiverNameInContext;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }
                else
                {
                    throw new Exception("שם ויתור על ביטוח לא קיים במערכת");
                }
            }
        }
    }
}
