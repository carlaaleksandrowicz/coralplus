﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoralBusinessLogics
{
    public class TravelInsuranceLogic
    {
        //מחזיר רשימה של כל סוגי ביטוחי חו"ל לפי חברת ביטוח
        public List<TravelInsuranceType> GetTravelInsuranceTypesByCompany(int companyId)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.TravelInsuranceTypes.Include("Company").Where(i => i.CompanyID == companyId).OrderByDescending(i => i.Status).ThenBy(i => i.TravelInsuranceTypeName).ToList();
            }
        }

        //מחזיר רשימה של סוגי ביטוחי חו"ל פעילים
        public List<TravelInsuranceType> GetActiveTravelInsuranceTypesByCompany(int companyId)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.TravelInsuranceTypes.Where(i => i.CompanyID == companyId && i.Status == true).OrderBy(i => i.TravelInsuranceTypeName).ToList();
            }
        }


        //מוסיף סוג ביטוח חדש
        public TravelInsuranceType InsertTravelInsuranceType(string insuranceTypeName, bool status, int companyId, bool? isWinterSport, bool? isExtremeSport, bool? isSearchAndRescue, bool? isPregnancy, bool? isExistingMedicalCondition, bool? isBaggage, bool? isComputerOrTablet, bool? isPhoneOrGPS, bool? isBicycle, string otherName, bool? isOther)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var insuranceTypeInContext = context.TravelInsuranceTypes.FirstOrDefault(i => i.TravelInsuranceTypeName == insuranceTypeName && i.CompanyID == companyId);
                if (insuranceTypeInContext == null)
                {
                    try
                    {
                        TravelInsuranceType newType = new TravelInsuranceType() { TravelInsuranceTypeName = insuranceTypeName, Status = status, CompanyID = companyId, IsPregnancy = isPregnancy, IsWinterSport = isWinterSport, IsSearchAndRescue = isSearchAndRescue, IsExtremeSport = isExtremeSport, IsBaggage=isBaggage, IsBicycle=isBicycle, IsComputerOrTablet=isComputerOrTablet, IsExistingMedicalCondition=isExistingMedicalCondition, IsOther=isOther, IsPhoneOrGPS=isPhoneOrGPS, OtherName=otherName };
                        context.TravelInsuranceTypes.Add(newType);
                        context.SaveChanges();
                        return newType;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }
                else
                {
                    throw new Exception("סוג ביטוח קיים");
                }
            }
        }

        //מעדכן את הסטטוס של סוג ביטוח קיים
        public TravelInsuranceType UpdateTravelInsuranceTypeStatus(TravelInsuranceType insuranceType, bool status, int companyId, bool? isWinterSport, bool? isExtremeSport, bool? isSearchAndRescue, bool? isPregnancy, bool? isExistingMedicalCondition, bool? isBaggage, bool? isComputerOrTablet, bool? isPhoneOrGPS, bool? isBicycle, string otherName, bool? isOther)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var insuranceTypeInContext = context.TravelInsuranceTypes.FirstOrDefault(i => i.TravelInsuranceTypeName == insuranceType.TravelInsuranceTypeName && i.CompanyID == companyId);
                if (insuranceTypeInContext != null)
                {
                    try
                    {
                        insuranceTypeInContext.Status = status;
                        insuranceTypeInContext.IsWinterSport = isWinterSport;
                        insuranceTypeInContext.IsBaggage = isBaggage;
                        insuranceTypeInContext.IsBicycle = isBicycle;
                        insuranceTypeInContext.IsComputerOrTablet = isComputerOrTablet;
                        insuranceTypeInContext.IsExistingMedicalCondition = isExistingMedicalCondition;
                        insuranceTypeInContext.IsExtremeSport = isExtremeSport;
                        insuranceTypeInContext.IsOther = isOther;
                        insuranceTypeInContext.IsPhoneOrGPS = isPhoneOrGPS;
                        insuranceTypeInContext.IsPregnancy = isPregnancy;
                        insuranceTypeInContext.IsSearchAndRescue = isSearchAndRescue;
                        insuranceTypeInContext.OtherName = otherName;
                        
                        context.SaveChanges();
                        return insuranceTypeInContext;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }
                else
                {
                    throw new Exception("סוג ביטוח לא קיים");
                }

            }
        }

        public void DeleteInsuranceType(TravelInsuranceType insuranceType)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var insuranceTypeInContext = context.TravelInsuranceTypes.FirstOrDefault(i => i.TravelInsuranceTypeID == insuranceType.TravelInsuranceTypeID);
                if (insuranceTypeInContext != null)
                {
                    try
                    {
                        context.TravelInsuranceTypes.Remove(insuranceTypeInContext);
                        context.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }
                else
                {
                    throw new Exception("סוג ביטוח לא קיים");
                }

            }
        }
    }
}
