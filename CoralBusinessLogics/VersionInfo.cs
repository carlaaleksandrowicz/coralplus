﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoralBusinessLogics
{
    public static class VersionInfo
    {
        public const string Version = "5.2.0.1";
    }
}
