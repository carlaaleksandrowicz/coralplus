//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CoralBusinessLogics
{
    using System;
    using System.Collections.Generic;
    
    public partial class HealthClaim
    {
        public HealthClaim()
        {
            this.HealthClaimTrackings = new HashSet<HealthClaimTracking>();
            this.HealthWitnesses = new HashSet<HealthWitness>();
        }
    
        public int HealthClaimID { get; set; }
        public int HealthPolicyID { get; set; }
        public Nullable<int> HealthClaimTypeID { get; set; }
        public Nullable<int> HealthClaimConditionID { get; set; }
        public Nullable<bool> ClaimStatus { get; set; }
        public string ClaimNumber { get; set; }
        public Nullable<System.DateTime> OpenDate { get; set; }
        public Nullable<System.DateTime> DeliveredToCompanyDate { get; set; }
        public Nullable<System.DateTime> MoneyReceivedDate { get; set; }
        public Nullable<decimal> ClaimAmount { get; set; }
        public Nullable<decimal> AmountReceived { get; set; }
        public Nullable<System.DateTime> EventDateAndTime { get; set; }
        public string EventPlace { get; set; }
        public string EventDescription { get; set; }
    
        public virtual HealthClaimCondition HealthClaimCondition { get; set; }
        public virtual HealthClaimType HealthClaimType { get; set; }
        public virtual HealthPolicy HealthPolicy { get; set; }
        public virtual ICollection<HealthClaimTracking> HealthClaimTrackings { get; set; }
        public virtual ICollection<HealthWitness> HealthWitnesses { get; set; }
    }
}
