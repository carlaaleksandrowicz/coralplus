//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CoralBusinessLogics
{
    using System;
    using System.Collections.Generic;
    
    public partial class AgentNumber
    {
        public AgentNumber()
        {
            this.ElementaryPolicies = new HashSet<ElementaryPolicy>();
            this.ElementaryPolicies1 = new HashSet<ElementaryPolicy>();
            this.FinancePolicies = new HashSet<FinancePolicy>();
            this.FinancePolicies1 = new HashSet<FinancePolicy>();
            this.ForeingWorkersPolicies = new HashSet<ForeingWorkersPolicy>();
            this.ForeingWorkersPolicies1 = new HashSet<ForeingWorkersPolicy>();
            this.HealthPolicies = new HashSet<HealthPolicy>();
            this.HealthPolicies1 = new HashSet<HealthPolicy>();
            this.LifePolicies = new HashSet<LifePolicy>();
            this.LifePolicies1 = new HashSet<LifePolicy>();
            this.PersonalAccidentsPolicies = new HashSet<PersonalAccidentsPolicy>();
            this.PersonalAccidentsPolicies1 = new HashSet<PersonalAccidentsPolicy>();
            this.TravelPolicies = new HashSet<TravelPolicy>();
            this.TravelPolicies1 = new HashSet<TravelPolicy>();
        }
    
        public int AgentNumberID { get; set; }
        public int AgentID { get; set; }
        public int InsuranceID { get; set; }
        public int CompanyID { get; set; }
        public string Number { get; set; }
    
        public virtual Agent Agent { get; set; }
        public virtual Company Company { get; set; }
        public virtual Insurance Insurance { get; set; }
        public virtual ICollection<ElementaryPolicy> ElementaryPolicies { get; set; }
        public virtual ICollection<ElementaryPolicy> ElementaryPolicies1 { get; set; }
        public virtual ICollection<FinancePolicy> FinancePolicies { get; set; }
        public virtual ICollection<FinancePolicy> FinancePolicies1 { get; set; }
        public virtual ICollection<ForeingWorkersPolicy> ForeingWorkersPolicies { get; set; }
        public virtual ICollection<ForeingWorkersPolicy> ForeingWorkersPolicies1 { get; set; }
        public virtual ICollection<HealthPolicy> HealthPolicies { get; set; }
        public virtual ICollection<HealthPolicy> HealthPolicies1 { get; set; }
        public virtual ICollection<LifePolicy> LifePolicies { get; set; }
        public virtual ICollection<LifePolicy> LifePolicies1 { get; set; }
        public virtual ICollection<PersonalAccidentsPolicy> PersonalAccidentsPolicies { get; set; }
        public virtual ICollection<PersonalAccidentsPolicy> PersonalAccidentsPolicies1 { get; set; }
        public virtual ICollection<TravelPolicy> TravelPolicies { get; set; }
        public virtual ICollection<TravelPolicy> TravelPolicies1 { get; set; }
    }
}
