﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoralBusinessLogics
{
    public class InvestmentPlansLogic
    {
        public List<InvesmentPlan> GetAllInvestmentPlans()
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.InvesmentPlans.OrderByDescending(i => i.Status).ThenBy(i => i.InvesmentPlanName).ToList();
            }
        }

        public List<InvesmentPlan> GetActiveInvestmentPlans()
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.InvesmentPlans.Where(i=>i.Status==true).OrderBy(i => i.InvesmentPlanName).ToList();
            }
        }

        public InvesmentPlan InsertInvestmentPlan(string planName, bool status)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var planNameInContext = context.InvesmentPlans.FirstOrDefault(i => i.InvesmentPlanName == planName);
                if (planNameInContext == null)
                {
                    try
                    {
                        InvesmentPlan newPlanName = new InvesmentPlan() { InvesmentPlanName = planName, Status = status };
                        context.InvesmentPlans.Add(newPlanName);
                        context.SaveChanges();
                        return newPlanName;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }
                else
                {
                    throw new Exception("שם מסלול קיים במערכת");
                }
            }
        }

        public InvesmentPlan UpdatePlanName(string planName, bool status)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var planNameInContext = context.InvesmentPlans.FirstOrDefault(i => i.InvesmentPlanName == planName);
                if (planNameInContext != null)
                {
                    try
                    {
                        planNameInContext.Status = status;
                        context.SaveChanges();
                        return planNameInContext;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }
                else
                {
                    throw new Exception("שם מסלול לא קיים במערכת");
                }
            }
        }
    }
}
