﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoralBusinessLogics
{
    public class ClaimsLogic
    {
        //מחזיר רשימה של כל התביעות האלמנטריות
        public List<ElementaryClaim> GetAllElementaryClaims()
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.ElementaryClaims.Include("ElementaryClaimCondition").Include("ElementaryClaimType").Include("ElementaryPolicy").Include("ElementaryPolicy.Company").Include("ElementaryPolicy.InsuranceIndustry").Include("ElementaryPolicy.ElementaryInsuranceType").Include("ElementaryPolicy.CarPolicy").Include("ElementaryPolicy.BusinessPolicy").Include("ElementaryPolicy.ApartmentPolicy").Include("ElementaryPolicy.Client").OrderByDescending(c => c.OpenDate).ToList();
            }
        }

        //מחזיר רשימה של כל התביעות האלמנטריות לפי לקוח
        public List<ElementaryClaim> GetAllElementaryClaimsByClient(int clientId)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.ElementaryClaims.Include("ElementaryPolicy.Client").Include("CarClaim").Include("ApartmentBusinessClaim").Include("ElementaryClaimCondition").Include("ElementaryClaimType").Include("ElementaryPolicy").Include("ElementaryPolicy.Company").Include("ElementaryPolicy.InsuranceIndustry").Include("ElementaryPolicy.ElementaryInsuranceType").Include("ElementaryPolicy.CarPolicy").Include("ElementaryPolicy.Insurance").Where(c => c.ElementaryPolicy.ClientID == clientId).OrderByDescending(c => c.OpenDate).ToList();
            }
        }

        //מחדיר רשימה של כל סוגי התביעות האלמנטריות
        public List<ElementaryClaimType> GetAllElementaryClaimTypes()
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.ElementaryClaimTypes.Include("InsuranceIndustry").OrderBy(c => c.InsuranceIndustryID).ThenBy(c => c.ClaimTypeName).ToList();
            }
        }

        //מחדיר רשימה של סוגי התביעות האלמנטריות לפי סוג ביטוח
        public List<ElementaryClaimType> GetAllElementaryClaimTypesByIndustry(int industryID)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.ElementaryClaimTypes.Where(t => t.InsuranceIndustryID == industryID).OrderByDescending(t=>t.Status).ThenBy(t=>t.ClaimTypeName).ToList();
            }
        }

        //מחדיר רשימה של סוגי התביעות האלמנטריות האקטיביות לפי סוג ביטוח
        public List<ElementaryClaimType> GetActiveElementaryClaimTypesByIndustry(int industryID)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.ElementaryClaimTypes.Where(t => t.InsuranceIndustryID == industryID && t.Status == true).OrderBy(t => t.ClaimTypeName).ToList();
            }
        }

        //מחזיר תביעת רכב לפי מס' זיהוי בבסיס הנתונים
        public CarClaim GetCarClaimByClaimID(int claimId)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.CarClaims.FirstOrDefault(c => c.ElementaryClaimID == claimId);
            }
        }

        //מחזיר תביעת דירה/עסק לפי מס' זיהוי בבסיס הנתונים
        public ApartmentBusinessClaim GetAptBusinessClaimByClaimID(int claimId)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.ApartmentBusinessClaims.FirstOrDefault(c => c.ElementaryClaimID == claimId);
            }
        }

        //מוסיף סוג תביעה אלמנטרית לבסיס הנתונים
        public ElementaryClaimType InsertElementaryClaimType(int industryId, string claimTypeName, bool status)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var claimTypeInContext = context.ElementaryClaimTypes.FirstOrDefault(t => t.ClaimTypeName == claimTypeName && t.InsuranceIndustryID == industryId);
                if (claimTypeInContext == null)
                {
                    try
                    {
                        ElementaryClaimType newType = new ElementaryClaimType() { InsuranceIndustryID = industryId, ClaimTypeName = claimTypeName, Status = status };
                        context.ElementaryClaimTypes.Add(newType);
                        context.SaveChanges();
                        return newType;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }
                else
                {
                    throw new Exception("סוג תביעה קיימת במערכת");
                }
            }
        }

        //מעדכן את הסטטוס של תביעה אלמנטרית
        public ElementaryClaimType UpdateElementaryClaimTypeStatus(ElementaryClaimType type, bool status)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {

                var claimTypeInContext = context.ElementaryClaimTypes.Where(c => c.ElementaryClaimTypeID == type.ElementaryClaimTypeID).FirstOrDefault();
                if (claimTypeInContext != null)
                {
                    try
                    {
                        claimTypeInContext.Status = status;
                        context.SaveChanges();
                        return claimTypeInContext;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }
                else
                {
                    throw new Exception("סוג תביעה לא קיימת במערכת");
                }
            }
        }

        //מחזיר רשימה של כל מצבי תביעה אלמנטרית
        public List<ElementaryClaimCondition> GetAllElementaryClaimConditions()
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.ElementaryClaimConditions.Include("InsuranceIndustry").OrderBy(c => c.InsuranceIndustryID).ThenBy(c => c.Description).ToList();
            }
        }

        //מחזיר רשימה של מצבי תביעה אלמנטרית לפי ענף
        public List<ElementaryClaimCondition> GetAllElementaryClaimConditionsByIndustrie(int industryID)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.ElementaryClaimConditions.Where(c => c.InsuranceIndustryID == industryID).OrderByDescending(c=>c.Status).ThenBy(c=>c.Description).ToList();
            }
        }

        //מחזיר רשימה של מצבי תביעה אלמנטרית אקטיביים לפי ענף
        public List<ElementaryClaimCondition> GetActiveElementaryClaimConditionsByIndustrie(int industryID)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.ElementaryClaimConditions.Where(c => c.InsuranceIndustryID == industryID && c.Status == true).OrderBy(c=>c.Description).ToList();
            }
        }

        //מוסיף מצב תביעה לבסיס הנתונים
        public ElementaryClaimCondition InsertElementaryClaimCondition(int industryID, string claimConditionName, bool status)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var claimConditionInContext = context.ElementaryClaimConditions.FirstOrDefault(c => c.Description == claimConditionName && c.InsuranceIndustryID == industryID);
                if (claimConditionInContext == null)
                {
                    try
                    {
                        ElementaryClaimCondition newCondition = new ElementaryClaimCondition() { InsuranceIndustryID = industryID, Description = claimConditionName, Status = status };
                        context.ElementaryClaimConditions.Add(newCondition);
                        context.SaveChanges();
                        return newCondition;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }
                else
                {
                    throw new Exception("מצב תביעה קיימת במערכת");
                }
            }
        }

        public List<ForeingWorkersClaim> GetAllForeingWorkersClaims()
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.ForeingWorkersClaims.Include("ForeingWorkersClaimCondition").Include("ForeingWorkersClaimType").Include("ForeingWorkersPolicy").Include("ForeingWorkersPolicy.ForeingWorkersIndustry").Include("ForeingWorkersPolicy.Company").Include("ForeingWorkersPolicy.Client").OrderByDescending(c => c.OpenDate).ToList();
            }
        }

        public List<PersonalAccidentsClaim> GetAllPersonalAccidentsClaims()
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.PersonalAccidentsClaims.Include("PersonalAccidentsClaimCondition").Include("PersonalAccidentsClaimType").Include("PersonalAccidentsPolicy").Include("PersonalAccidentsPolicy.Company").Include("PersonalAccidentsPolicy.Client").OrderByDescending(c => c.OpenDate).ToList();
            }

        }

        //מעדכן את הסטטוס של מצב תביעה
        public ElementaryClaimCondition UpdateElementaryClaimConditionStatus(ElementaryClaimCondition condition, bool status)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var claimConditionInContext = context.ElementaryClaimConditions.Where(c => c.ElementaryClaimConditionID == condition.ElementaryClaimConditionID).FirstOrDefault();
                if (claimConditionInContext != null)
                {
                    try
                    {
                        claimConditionInContext.Status = status;
                        context.SaveChanges();
                        return claimConditionInContext;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }
                else
                {
                    throw new Exception("סוג תביעה לא קיים במערכת");
                }
            }
        }

        //מוסיף תביעה אלמנטרית לבסיס הנתונים
        public int InsertElementaryClaim(int policyId, int? claimTypeId, int? claimConditionId, bool claimStatus, string claimNumber, DateTime? openDate, string thirdPartyClaimNumber, DateTime? deliveredToCompanyDate, DateTime? moneyRecivedDate, decimal? claimAmount, decimal? amountRecived, DateTime? eventDate, TimeSpan? eventTime, string eventPlace, string eventDescription, string appraiserName, string appraiserAddress, string appraiserPhone, string appraiserCellPhone, string appraiserFax, string appraiserEmail)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                try
                {
                    ElementaryClaim newClaim = new ElementaryClaim()
                    {
                        ElementaryPolicyID = policyId,
                        ElementaryClaimTypeID = claimTypeId,
                        ElementaryClaimConditionID = claimConditionId,
                        ClaimStatus = claimStatus,
                        ClaimNumber = claimNumber,
                        OpenDate = openDate,
                        ThirdPartyClaimNumber = thirdPartyClaimNumber,
                        DeliveredToCompanyDate = deliveredToCompanyDate,
                        MoneyReceivedDate = moneyRecivedDate,
                        ClaimAmount = claimAmount,
                        AmountReceived = amountRecived,
                        EventDate = eventDate,
                        EventPlace = eventPlace,
                        EventDescription = eventDescription,
                        AppraiserName = appraiserName,
                        AppraiserAddress = appraiserAddress,
                        AppraiserPhone = appraiserPhone,
                        AppraiserCellPhone = appraiserCellPhone,
                        AppraiserFax = appraiserFax,
                        AppraiserEmail = appraiserEmail,
                        EventTime = eventTime

                    };
                    context.ElementaryClaims.Add(newClaim);
                    context.SaveChanges();
                    return newClaim.ElementaryClaimID;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
        }

        //מעדכן תביעה אלמנטרית
        public int UpdateElementaryClaim(int claimID, int? claimTypeId, int? claimConditionId, bool claimStatus, string claimNumber, DateTime? openDate, string thirdPartyClaimNumber, DateTime? deliveredToCompanyDate, DateTime? moneyRecivedDate, decimal? claimAmount, decimal? amountRecived, DateTime? eventDate, TimeSpan? eventTime, string eventPlace, string eventDescription, string appraiserName, string appraiserAddress, string appraiserPhone, string appraiserCellPhone, string appraiserFax, string appraiserEmail)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                try
                {
                    var claimInContext = context.ElementaryClaims.FirstOrDefault(c => c.ElementaryClaimID == claimID);
                    if (claimInContext != null)
                    {
                        claimInContext.ElementaryClaimTypeID = claimTypeId;
                        claimInContext.ElementaryClaimConditionID = claimConditionId;
                        claimInContext.ClaimStatus = claimStatus;
                        claimInContext.ClaimNumber = claimNumber;
                        claimInContext.OpenDate = openDate;
                        claimInContext.ThirdPartyClaimNumber = thirdPartyClaimNumber;
                        claimInContext.DeliveredToCompanyDate = deliveredToCompanyDate;
                        claimInContext.MoneyReceivedDate = moneyRecivedDate;
                        claimInContext.ClaimAmount = claimAmount;
                        claimInContext.AmountReceived = amountRecived;
                        claimInContext.EventDate = eventDate;
                        claimInContext.EventPlace = eventPlace;
                        claimInContext.EventDescription = eventDescription;
                        claimInContext.AppraiserName = appraiserName;
                        claimInContext.AppraiserAddress = appraiserAddress;
                        claimInContext.AppraiserPhone = appraiserPhone;
                        claimInContext.AppraiserCellPhone = appraiserCellPhone;
                        claimInContext.AppraiserFax = appraiserFax;
                        claimInContext.AppraiserEmail = appraiserEmail;
                        claimInContext.EventTime = eventTime;
                        context.SaveChanges();
                        return claimInContext.ElementaryClaimID;
                    }
                    else
                    {
                        throw new Exception("תביעה לא קיימת במערכת");
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
        }

        //מחזיר רשימה של עדים לפי תביעה
        public List<ElementaryWitness> GetWitnessesByClaim(int claimId)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.ElementaryWitnesses.Where(w => w.ElementaryClaimID == claimId).ToList();
            }
        }

       

        //מוסיף עד לתביעה
        public void InsertWitnesses(List<ElementaryWitness> witnesses, int claimID)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var claimInContext = context.ElementaryClaims.FirstOrDefault(c => c.ElementaryClaimID == claimID);
                if (claimInContext != null)
                {
                    try
                    {
                        foreach (var item in witnesses)
                        {
                            var witnessInContext = context.ElementaryWitnesses.FirstOrDefault(w => w.ElementaryWitnessID == item.ElementaryWitnessID);
                            if (witnessInContext == null)
                            {
                                item.ElementaryClaimID = claimID;
                                item.ElementaryClaim = null;
                                context.ElementaryWitnesses.Add(item);
                            }
                            else
                            {

                                witnessInContext.WitnessCellPhone = item.WitnessCellPhone;
                                witnessInContext.WitnessName = item.WitnessName;
                                witnessInContext.WitnessPhone = item.WitnessPhone;
                                witnessInContext.WittnessAddress = item.WittnessAddress;
                            }
                        }
                        context.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }
                else
                {
                    throw new Exception("תביעה לא קיימת במערכת");
                }
            }
        }

        //מוסיף תביעת רכב לבסיס הנתונים
        public void InsertCarClaim(int claimId, decimal? damageAmount, decimal? appraiserFees, decimal? carDepreciation, decimal? otherDamages, string damageDescription, string driverLastName, string driverFirstName, string driverIdNumber, DateTime? driverBirthDate, string driverAddress, string driverPhone, string driverCellPhone, string driverEmail, string driverLicenseNumber, DateTime? driverLicenseDate, DateTime? driverLicenseValid, string driverLicenseType, string driverProximityToPolicyHolder, string garageName, string garageAddress, string garagePhone, string garageFax, string garageEmail, string garageContactName, bool? isGarageArrangement)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var claimInContext = context.ElementaryClaims.FirstOrDefault(c => c.ElementaryClaimID == claimId);
                if (claimInContext != null)
                {
                    try
                    {
                        CarClaim newCarClaim = new CarClaim()
                        {
                            ElementaryClaimID = claimId,
                            DamageAmount = damageAmount,
                            AppraiserFees = appraiserFees,
                            CarDepreciation = carDepreciation,
                            OtherDamages = otherDamages,
                            DamageDescription = damageDescription,
                            DriverLastName = driverLastName,
                            DriverFirsName = driverFirstName,
                            DriverIdNumber = driverIdNumber,
                            DriverBirthDate = driverBirthDate,
                            DriverAddress = driverAddress,
                            DriverPhone = driverPhone,
                            DriverCellPhone = driverCellPhone,
                            DriverEmail = driverEmail,
                            DriverLicenseNumber = driverLicenseNumber,
                            DriverLicenseDate = driverLicenseDate,
                            DriverLicenseValid = driverLicenseValid,
                            DriverLicenseType = driverLicenseType,
                            DriverProximityToPolicyHolder = driverProximityToPolicyHolder,
                            GarageName = garageName,
                            GarageAddress = garageAddress,
                            GaragePhone = garagePhone,
                            GarageFax = garageFax,
                            GarageEmail = garageEmail,
                            GarageContactName = garageContactName,
                            IsGarageArrangement = isGarageArrangement
                        };
                        context.CarClaims.Add(newCarClaim);
                        context.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }
                else
                {
                    throw new Exception("תביעה לא קיימת במערכת");
                }
            }
        }

        public IEnumerable GetForeingWorkersClaimsByFilter(List<ForeingWorkersClaim> foreingWorkersClaims, string status, ForeingWorkersClaimCondition foreingWorkersClaimCondition, ForeingWorkersClaimType foreingWorkersClaimType, ForeingWorkersIndustry industry, Company company, string searchKey)
        {
            try
            {
                //var claimsInContext = GetAllLifeClaims();
                if (searchKey != "")
                {
                    foreingWorkersClaims = foreingWorkersClaims.Where(c => (c.ClaimNumber != null && c.ClaimNumber.StartsWith(searchKey)) || c.ForeingWorkersPolicy.Client.FirstName.StartsWith(searchKey) || c.ForeingWorkersPolicy.Client.LastName.StartsWith(searchKey) || (c.ForeingWorkersPolicy.Client.LastName + " " + c.ForeingWorkersPolicy.Client.FirstName).StartsWith(searchKey) || (c.ForeingWorkersPolicy.Client.FirstName + " " + c.ForeingWorkersPolicy.Client.LastName).StartsWith(searchKey) || c.ForeingWorkersPolicy.Client.IdNumber.StartsWith(searchKey) || c.ForeingWorkersPolicy.PolicyNumber.StartsWith(searchKey)).ToList();
                }
                if (status != null && status != "הצג הכל")
                {
                    if (status == "פתוח")
                    {
                        foreingWorkersClaims = foreingWorkersClaims.Where(c => c.ClaimStatus == true).ToList();
                    }
                    else
                    {
                        foreingWorkersClaims = foreingWorkersClaims.Where(c => c.ClaimStatus == false).ToList();
                    }
                }
                if (foreingWorkersClaimCondition != null)
                {
                    foreingWorkersClaims = foreingWorkersClaims.Where(c => c.ForeingWorkersClaimConditionID == foreingWorkersClaimCondition.ForeingWorkersClaimConditionID).ToList();
                }
                if (foreingWorkersClaimType != null)
                {
                    foreingWorkersClaims = foreingWorkersClaims.Where(c => c.ForeingWorkersClaimTypeID == foreingWorkersClaimType.ForeingWorkersClaimTypeID).ToList();
                }
                if (industry != null)
                {
                    foreingWorkersClaims = foreingWorkersClaims.Where(c => c.ForeingWorkersPolicy.ForeingWorkersIndustryID == industry.ForeingWorkersIndustryID).ToList();
                }
                if (company != null)
                {
                    foreingWorkersClaims = foreingWorkersClaims.Where(c => c.ForeingWorkersPolicy.CompanyID == company.CompanyID).ToList();
                }
                return foreingWorkersClaims;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        //מעדכן תבעת רכב
        public void UpdateCarClaim(int carClaimId, decimal? damageAmount, decimal? appraiserFees, decimal? carDepreciation, decimal? otherDamages, string damageDescription, string driverLastName, string driverFirstName, string driverIdNumber, DateTime? driverBirthDate, string driverAddress, string driverPhone, string driverCellPhone, string driverEmail, string driverLicenseNumber, DateTime? driverLicenseDate, DateTime? driverLicenseValid, string driverLicenseType, string driverProximityToPolicyHolder, string garageName, string garageAddress, string garagePhone, string garageFax, string garageEmail, string garageContactName, bool? isGarageArrangement)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var claimInContext = context.CarClaims.FirstOrDefault(c => c.ElementaryClaimID == carClaimId);
                if (claimInContext != null)
                {
                    try
                    {
                        claimInContext.DamageAmount = damageAmount;
                        claimInContext.AppraiserFees = appraiserFees;
                        claimInContext.CarDepreciation = carDepreciation;
                        claimInContext.OtherDamages = otherDamages;
                        claimInContext.DamageDescription = damageDescription;
                        claimInContext.DriverLastName = driverLastName;
                        claimInContext.DriverFirsName = driverFirstName;
                        claimInContext.DriverIdNumber = driverIdNumber;
                        claimInContext.DriverBirthDate = driverBirthDate;
                        claimInContext.DriverAddress = driverAddress;
                        claimInContext.DriverPhone = driverPhone;
                        claimInContext.DriverCellPhone = driverCellPhone;
                        claimInContext.DriverEmail = driverEmail;
                        claimInContext.DriverLicenseNumber = driverLicenseNumber;
                        claimInContext.DriverLicenseDate = driverLicenseDate;
                        claimInContext.DriverLicenseValid = driverLicenseValid;
                        claimInContext.DriverLicenseType = driverLicenseType;
                        claimInContext.DriverProximityToPolicyHolder = driverProximityToPolicyHolder;
                        claimInContext.GarageName = garageName;
                        claimInContext.GarageAddress = garageAddress;
                        claimInContext.GaragePhone = garagePhone;
                        claimInContext.GarageFax = garageFax;
                        claimInContext.GarageEmail = garageEmail;
                        claimInContext.GarageContactName = garageContactName;
                        claimInContext.IsGarageArrangement = isGarageArrangement;

                        context.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }
                else
                {
                    throw new Exception("תביעה לא קיימת במערכת");
                }
            }
        }

        //מחזיר רשימה של נזקים לפי תביעת דירה/עסק
        public List<ApartmentBusinessDamage> GetAllDamagesByAptOrBussinesClaim(int claimId)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var claimInContext = context.ElementaryClaims.FirstOrDefault(c => c.ElementaryClaimID == claimId);
                if (claimInContext != null)
                {
                    try
                    {
                        return context.ApartmentBusinessDamages.Where(d => d.ElementaryClaimID == claimId).ToList();
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }
                else
                {
                    throw new Exception("תביעה לא קיימת במערכת");
                }
            }
        }

        //public void InsertDamage(int claimId, string damageDescription, string surveyItemNumber, decimal? surveyItemAmount, decimal? amountClaimed, string comments)
        //{
        //    using (Coral_DB_Entities context = new Coral_DB_Entities())
        //    {
        //        var claimInContext = context.ElementaryClaims.FirstOrDefault(c => c.ElementaryClaimID == claimId);
        //        if (claimInContext != null)
        //        {
        //            try
        //            {
        //                ApartmentBusinessDamage damage = new ApartmentBusinessDamage()
        //                {
        //                    ElementaryClaimID = claimId,
        //                    DamageAndPropertyDescription = damageDescription,
        //                    SurveyItemNumber = surveyItemNumber,
        //                    SurveyItemAmount = surveyItemAmount,
        //                    AmountClaimed = amountClaimed,
        //                    Comments = comments
        //                };
        //                context.ApartmentBusinessDamages.Add(damage);
        //                context.SaveChanges();
        //            }
        //            catch (Exception ex)
        //            {
        //                throw new Exception(ex.Message);
        //            }
        //        }
        //        else
        //        {
        //            throw new Exception("תביעה לא קיימת במערכת");
        //        }
        //    }
        //}

        //מוסיף נזקים לתביעת דירה/עסק
        public void InsertDamages(int claimId, List<ApartmentBusinessDamage> damages)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var claimInContext = context.ElementaryClaims.FirstOrDefault(c => c.ElementaryClaimID == claimId);
                if (claimInContext != null)
                {
                    try
                    {

                        foreach (var damage in damages)
                        {
                            if (damage.ApartmentBusinessDamageID == 0)
                            {
                                damage.ElementaryClaimID = claimId;
                                context.ApartmentBusinessDamages.Add(damage);
                            }
                            else
                            {
                                var damageInContext=context.ApartmentBusinessDamages.FirstOrDefault(d => d.ApartmentBusinessDamageID == damage.ApartmentBusinessDamageID);
                                if (damageInContext!=null)
                                {
                                    damageInContext.AmountClaimed = damage.AmountClaimed;
                                    damageInContext.Comments = damage.Comments;
                                    damageInContext.DamageAndPropertyDescription = damage.DamageAndPropertyDescription;
                                    damageInContext.SurveyItemAmount = damage.SurveyItemAmount;
                                    damageInContext.SurveyItemNumber = damage.SurveyItemNumber;
                                }
                            }
                        }

                        context.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }
                else
                {
                    throw new Exception("תביעה לא קיימת במערכת");
                }
            }
        }

        //מוסיף תביעת דירה/עסק לבסיס הנתונים
        public void InsertAptBusinessClaim(int claimId, decimal? amountClaimed, bool isThirdPartyDamage)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var claimInContext = context.ElementaryClaims.FirstOrDefault(c => c.ElementaryClaimID == claimId);
                if (claimInContext != null)
                {
                    try
                    {
                        ApartmentBusinessClaim newClaim = new ApartmentBusinessClaim() { ElementaryClaimID = claimId, TotalAmountClaimed = amountClaimed, IsThirdPartyDamages = isThirdPartyDamage };
                        context.ApartmentBusinessClaims.Add(newClaim);
                        context.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }
                else
                {
                    throw new Exception("תביעה לא קיימת במערכת");
                }
            }
        }

        //מעדכן תביעת דירה/עסק
        public void UpdateAptBusinessClaim(int claimId, decimal? amountClaimed, bool isThirdPartyDamage)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var claimInContext = context.ApartmentBusinessClaims.FirstOrDefault(c => c.ElementaryClaimID == claimId);
                if (claimInContext != null)
                {
                    try
                    {
                        claimInContext.ElementaryClaimID = claimId;
                        claimInContext.TotalAmountClaimed = amountClaimed;
                        claimInContext.IsThirdPartyDamages = isThirdPartyDamage;

                        context.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }
                else
                {
                    throw new Exception("תביעה לא קיימת במערכת");
                }
            }
        }

        //מחזיר צד ג לפי מס' זיהוי בבסיס הנתונים
        public ApartmentBusinessThirdParty GetAptBusinessThirdPartyByClaimId(int claimId)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.ApartmentBusinessThirdParties.FirstOrDefault(t => t.ElementaryClaimID == claimId);
            }
        }

        //מוסיף צד ג לתביעת דירה/עסק
        public void InsertAptBussinesThirdPartyDetails(int claimId, string damagedName, string damagedIdNumber, string damagedAddress, string damagedPhone, string damagedCellPhone, string damagedEmail, string damageDescription, decimal? totalAmountClaimed, InsuranceCompany damagedCompany)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var claimInContext = context.ElementaryClaims.FirstOrDefault(c => c.ElementaryClaimID == claimId);
                if (claimInContext != null)
                {
                    int? companyId = null;
                    if (damagedCompany != null)
                    {
                        companyId = damagedCompany.CompanyID;
                    }
                    try
                    {
                        ApartmentBusinessThirdParty thirdPartyDamage = new ApartmentBusinessThirdParty()
                        {
                            ElementaryClaimID = claimId,
                            DamagedName = damagedName,
                            DamagedIdNumber = damagedIdNumber,
                            DamagedAddress = damagedAddress,
                            DamagedPhone = damagedPhone,
                            DamagedCellPhone = damagedCellPhone,
                            DamagedEmail = damagedEmail,
                            DamageDescription = damageDescription,
                            TotalAmoundClaimed = totalAmountClaimed,
                            DamagedCompanyID = companyId
                        };
                        context.ApartmentBusinessThirdParties.Add(thirdPartyDamage);
                        context.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }
                else
                {
                    throw new Exception("תביעה לא קיימת במערכת");
                }
            }
        }

        //מעדכן צד ג 
        public void UpdateAptBussinesThirdPartyDetails(int claimId, string damagedName, string damagedIdNumber, string damagedAddress, string damagedPhone, string damagedCellPhone, string damagedEmail, string damageDescription, decimal? totalAmountClaimed, InsuranceCompany damagedCompany)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var claimInContext = context.ElementaryClaims.FirstOrDefault(c => c.ElementaryClaimID == claimId);
                if (claimInContext != null)
                {
                    var thirdPartyInContext = context.ApartmentBusinessThirdParties.FirstOrDefault(t => t.ElementaryClaimID == claimInContext.ElementaryClaimID);
                    if (thirdPartyInContext == null)
                    {
                        InsertAptBussinesThirdPartyDetails(claimId, damagedName, damagedIdNumber, damagedAddress, damagedPhone, damagedCellPhone, damagedEmail, damageDescription, totalAmountClaimed, damagedCompany);
                        return;
                    }
                    int? companyId = null;
                    if (damagedCompany != null)
                    {
                        companyId = damagedCompany.CompanyID;
                    }
                    try
                    {

                        thirdPartyInContext.ElementaryClaimID = claimId;
                        thirdPartyInContext.DamagedName = damagedName;
                        thirdPartyInContext.DamagedIdNumber = damagedIdNumber;
                        thirdPartyInContext.DamagedAddress = damagedAddress;
                        thirdPartyInContext.DamagedPhone = damagedPhone;
                        thirdPartyInContext.DamagedCellPhone = damagedCellPhone;
                        thirdPartyInContext.DamagedEmail = damagedEmail;
                        thirdPartyInContext.DamageDescription = damageDescription;
                        thirdPartyInContext.TotalAmoundClaimed = totalAmountClaimed;
                        thirdPartyInContext.DamagedCompanyID = companyId;

                        context.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }
                else
                {
                    throw new Exception("תביעה לא קיימת במערכת");
                }
            }
        }

        //מחזיר רשימה של צדדי ג לפי תביעת רכב
        public List<CarThirdParty> GetThirdPartiesByClaimID(int claimID)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.CarThirdParties.Where(t => t.ElementaryClaimID == claimID).ToList();
            }
        }

        //מוסיף צד ג לתביעת רכב
        public void InsertCarThirdParties(int claimId, List<CarThirdParty> thirdParties)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var claimInContext = context.ElementaryClaims.FirstOrDefault(c => c.ElementaryClaimID == claimId);
                if (claimInContext != null)
                {
                    try
                    {
                        var claimThirdParties = context.CarThirdParties.Where(t => t.ElementaryClaimID == claimId).ToList();
                        foreach (var item in thirdParties)
                        {
                            if (item.CarThirdPartyID == 0)
                            {
                                item.ElementaryClaimID = claimId;
                                context.CarThirdParties.Add(item);
                            }
                            else
                            {
                                var thirdPartyInContext = context.CarThirdParties.FirstOrDefault(t => t.CarThirdPartyID == item.CarThirdPartyID);
                                if (thirdPartyInContext!=null)
                                {
                                    thirdPartyInContext.AgentEmail = item.AgentEmail;
                                    thirdPartyInContext.AgentFax = item.AgentFax;
                                    thirdPartyInContext.AgentName = item.AgentName;
                                    thirdPartyInContext.AgentPhone = item.AgentPhone;
                                    thirdPartyInContext.AgentSecondPhone = item.AgentSecondPhone;
                                    thirdPartyInContext.CompanyID = item.CompanyID;
                                    thirdPartyInContext.DamageDescription = item.DamageDescription;
                                    thirdPartyInContext.DriverAddress = item.DriverAddress;
                                    thirdPartyInContext.DriverCellPhone = item.DriverCellPhone;
                                    thirdPartyInContext.DriverIdNumber = item.DriverIdNumber;
                                    thirdPartyInContext.DriverPhone = item.DriverPhone;
                                    thirdPartyInContext.ElementaryInsuranceTypeID = item.ElementaryInsuranceTypeID;
                                    thirdPartyInContext.IsHurtPeople = item.IsHurtPeople;
                                    thirdPartyInContext.Manufacturer = item.Manufacturer;
                                    thirdPartyInContext.PolicyHolderAddress = item.PolicyHolderAddress;
                                    thirdPartyInContext.PolicyHolderCellPhone = item.PolicyHolderCellPhone;
                                    thirdPartyInContext.PolicyHolderIdNumber = item.PolicyHolderIdNumber;
                                    thirdPartyInContext.PolicyHolderName = item.PolicyHolderName;
                                    thirdPartyInContext.PolicyHolderPhone = item.PolicyHolderPhone;
                                    thirdPartyInContext.PolicyNumber = item.PolicyNumber;
                                    thirdPartyInContext.RegistrationNumber = item.RegistrationNumber;
                                    thirdPartyInContext.VehicleTypeID = item.VehicleTypeID;
                                    thirdPartyInContext.DriverName = item.DriverName;

                                    claimThirdParties.Remove(thirdPartyInContext);
                                }
                            }
                        }
                        if (claimThirdParties.Count > 0)
                        {
                            foreach (var item in claimThirdParties)
                            {
                                context.CarThirdParties.Remove(item);
                            }
                        }
                        context.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }
                else
                {
                    throw new Exception("תביעה לא קיימת במערכת");
                }
            }
        }

        //מחזיר רשימה של תביעות אלמנטריות לפי שדות חיפוש 
        public List<ElementaryClaim> GetElementaryClaimsByFilter(List<ElementaryClaim> claimsInContext, string status, ElementaryClaimCondition claimCondition, ElementaryClaimType claimType, InsuranceIndustry industry, Company company, string searchKey)
        {
            try
            {
                //var claimsInContext = GetAllElementaryClaims();
                if (searchKey != "")
                {
                    claimsInContext = claimsInContext.Where(c => (c.ClaimNumber != null && c.ClaimNumber.StartsWith(searchKey)) || (c.ThirdPartyClaimNumber != null && c.ThirdPartyClaimNumber.StartsWith(searchKey)) || c.ElementaryPolicy.Client.FirstName.StartsWith(searchKey) || c.ElementaryPolicy.Client.LastName.StartsWith(searchKey) || (c.ElementaryPolicy.Client.LastName + " " + c.ElementaryPolicy.Client.FirstName).StartsWith(searchKey) || (c.ElementaryPolicy.Client.FirstName + " " + c.ElementaryPolicy.Client.LastName).StartsWith(searchKey) || c.ElementaryPolicy.Client.IdNumber.StartsWith(searchKey) || c.ElementaryPolicy.PolicyNumber.StartsWith(searchKey)).ToList();
                }
                if (status != null && status != "הצג הכל")
                {
                    if (status == "פתוח")
                    {
                        claimsInContext = claimsInContext.Where(c => c.ClaimStatus == true).ToList();
                    }
                    else
                    {
                        claimsInContext = claimsInContext.Where(c => c.ClaimStatus == false).ToList();
                    }
                }
                if (claimCondition != null)
                {
                    claimsInContext = claimsInContext.Where(c => c.ElementaryClaimConditionID == claimCondition.ElementaryClaimConditionID).ToList();
                }
                if (claimType != null)
                {
                    claimsInContext = claimsInContext.Where(c => c.ElementaryClaimTypeID == claimType.ElementaryClaimTypeID).ToList();
                }
                if (industry != null)
                {
                    claimsInContext = claimsInContext.Where(c => c.ElementaryPolicy.InsuranceIndustryID == industry.InsuranceIndustryID).ToList();
                }
                if (company != null)
                {
                    claimsInContext = claimsInContext.Where(c => c.ElementaryPolicy.CompanyID == company.CompanyID).ToList();
                }
                return claimsInContext;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public List<LifeClaim> GetAllLifeClaims()
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.LifeClaims.Include("LifeClaimCondition").Include("LifeClaimType").Include("LifePolicy").Include("LifePolicy.Company").Include("LifePolicy.LifeIndustry").Include("LifePolicy.FundType").Include("LifePolicy.Client").Include("LifePolicy.ManagerPolicy").Include("LifePolicy.MortgageLifeIndustry").Include("LifePolicy.PrivateLifeIndustry").Include("LifePolicy.PensionLifeIndustry").OrderByDescending(c => c.OpenDate).ToList();
            }
        }

        public List<HealthClaim> GetAllHealthClaims()
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.HealthClaims.Include("HealthClaimCondition").Include("HealthClaimType").Include("HealthPolicy").Include("HealthPolicy.Company").Include("HealthPolicy.HealthInsuranceType").Include("HealthPolicy.Client").OrderByDescending(c => c.OpenDate).ToList();
            }
        }


        public List<LifeClaim> GetLifeClaimsByFilter(List<LifeClaim> claimsInContext, string status, LifeClaimCondition lifeClaimCondition, LifeClaimType lifeClaimType, LifeIndustry lifeIndustry, Company company, string searchKey)
        {
            try
            {
                //var claimsInContext = GetAllLifeClaims();
                if (searchKey != "")
                {
                    claimsInContext = claimsInContext.Where(c => (c.ClaimNumber != null && c.ClaimNumber.StartsWith(searchKey)) || c.LifePolicy.Client.FirstName.StartsWith(searchKey) || c.LifePolicy.Client.LastName.StartsWith(searchKey) || (c.LifePolicy.Client.LastName + " " + c.LifePolicy.Client.FirstName).StartsWith(searchKey) || (c.LifePolicy.Client.FirstName + " " + c.LifePolicy.Client.LastName).StartsWith(searchKey) || c.LifePolicy.Client.IdNumber.StartsWith(searchKey) || c.LifePolicy.PolicyNumber.StartsWith(searchKey)).ToList();
                }
                if (status != null && status != "הצג הכל")
                {
                    if (status == "פתוח")
                    {
                        claimsInContext = claimsInContext.Where(c => c.ClaimStatus == true).ToList();
                    }
                    else
                    {
                        claimsInContext = claimsInContext.Where(c => c.ClaimStatus == false).ToList();
                    }
                }
                if (lifeClaimCondition != null)
                {
                    claimsInContext = claimsInContext.Where(c => c.LifeClaimConditionID == lifeClaimCondition.LifeClaimConditionID).ToList();
                }
                if (lifeClaimType != null)
                {
                    claimsInContext = claimsInContext.Where(c => c.LifeClaimTypeID == lifeClaimType.LifeClaimTypeID).ToList();
                }
                if (lifeIndustry != null)
                {
                    claimsInContext = claimsInContext.Where(c => c.LifePolicy.LifeIndustryID == lifeIndustry.LifeIndustryID).ToList();
                }
                if (company != null)
                {
                    claimsInContext = claimsInContext.Where(c => c.LifePolicy.CompanyID == company.CompanyID).ToList();
                }

                return claimsInContext;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public System.Collections.IEnumerable GetHealthClaimsByFilter(List<HealthClaim> healthClaims, string status, HealthClaimCondition healthClaimCondition, HealthClaimType healthClaimType, Company company, string searchKey)
        {
            try
            {
                //var claimsInContext = GetAllLifeClaims();
                if (searchKey != "")
                {
                    healthClaims = healthClaims.Where(c => (c.ClaimNumber != null && c.ClaimNumber.StartsWith(searchKey)) || c.HealthPolicy.Client.FirstName.StartsWith(searchKey) || c.HealthPolicy.Client.LastName.StartsWith(searchKey) || (c.HealthPolicy.Client.LastName + " " + c.HealthPolicy.Client.FirstName).StartsWith(searchKey) || (c.HealthPolicy.Client.FirstName + " " + c.HealthPolicy.Client.LastName).StartsWith(searchKey) || c.HealthPolicy.Client.IdNumber.StartsWith(searchKey) || c.HealthPolicy.PolicyNumber.StartsWith(searchKey)).ToList();
                }
                if (status != null && status != "הצג הכל")
                {
                    if (status == "פתוח")
                    {
                        healthClaims = healthClaims.Where(c => c.ClaimStatus == true).ToList();
                    }
                    else
                    {
                        healthClaims = healthClaims.Where(c => c.ClaimStatus == false).ToList();
                    }
                }
                if (healthClaimCondition != null)
                {
                    healthClaims = healthClaims.Where(c => c.HealthClaimConditionID == healthClaimCondition.HealthClaimConditionID).ToList();
                }
                if (healthClaimType != null)
                {
                    healthClaims = healthClaims.Where(c => c.HealthClaimTypeID == healthClaimType.HealthClaimTypeID).ToList();
                }
                if (company != null)
                {
                    healthClaims = healthClaims.Where(c => c.HealthPolicy.CompanyID == company.CompanyID).ToList();
                }
                return healthClaims;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public IEnumerable GetPersonalAccidentsClaimsByFilter(List<PersonalAccidentsClaim> personalAccidentsClaims, string status, PersonalAccidentsClaimCondition personalAccidentsClaimCondition, PersonalAccidentsClaimType personalAccidentsClaimType, Company company, string searchKey)
        {
            try
            {
                //var claimsInContext = GetAllLifeClaims();
                if (searchKey != "")
                {
                    personalAccidentsClaims = personalAccidentsClaims.Where(c => (c.ClaimNumber != null && c.ClaimNumber.StartsWith(searchKey)) || c.PersonalAccidentsPolicy.Client.FirstName.StartsWith(searchKey) || c.PersonalAccidentsPolicy.Client.LastName.StartsWith(searchKey) || (c.PersonalAccidentsPolicy.Client.LastName + " " + c.PersonalAccidentsPolicy.Client.FirstName).StartsWith(searchKey) || (c.PersonalAccidentsPolicy.Client.FirstName + " " + c.PersonalAccidentsPolicy.Client.LastName).StartsWith(searchKey) || c.PersonalAccidentsPolicy.Client.IdNumber.StartsWith(searchKey) || c.PersonalAccidentsPolicy.PolicyNumber.StartsWith(searchKey)).ToList();
                }
                if (status != null && status != "הצג הכל")
                {
                    if (status == "פתוח")
                    {
                        personalAccidentsClaims = personalAccidentsClaims.Where(c => c.ClaimStatus == true).ToList();
                    }
                    else
                    {
                        personalAccidentsClaims = personalAccidentsClaims.Where(c => c.ClaimStatus == false).ToList();
                    }
                }
                if (personalAccidentsClaimCondition != null)
                {
                    personalAccidentsClaims = personalAccidentsClaims.Where(c => c.PersonalAccidentsClaimConditionID == personalAccidentsClaimCondition.PersonalAccidentsClaimConditionID).ToList();
                }
                if (personalAccidentsClaimType != null)
                {
                    personalAccidentsClaims = personalAccidentsClaims.Where(c => c.PersonalAccidentsClaimTypeID == personalAccidentsClaimType.PersonalAccidentsClaimTypeID).ToList();
                }
                if (company != null)
                {
                    personalAccidentsClaims = personalAccidentsClaims.Where(c => c.PersonalAccidentsPolicy.CompanyID == company.CompanyID).ToList();
                }
                return personalAccidentsClaims;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public IEnumerable GetTravelClaimsByFilter(List<TravelClaim> travelClaims, string status, TravelClaimCondition travelClaimCondition, TravelClaimType travelClaimType, Company company, string searchKey)
        {
            try
            {
                //var claimsInContext = GetAllLifeClaims();
                if (searchKey != "")
                {
                    travelClaims = travelClaims.Where(c => (c.ClaimNumber != null && c.ClaimNumber.StartsWith(searchKey)) || c.TravelPolicy.Client.FirstName.StartsWith(searchKey) || c.TravelPolicy.Client.LastName.StartsWith(searchKey) || (c.TravelPolicy.Client.LastName + " " + c.TravelPolicy.Client.FirstName).StartsWith(searchKey) || (c.TravelPolicy.Client.FirstName + " " + c.TravelPolicy.Client.LastName).StartsWith(searchKey) || c.TravelPolicy.Client.IdNumber.StartsWith(searchKey) || c.TravelPolicy.PolicyNumber.StartsWith(searchKey)).ToList();
                }
                if (status != null && status != "הצג הכל")
                {
                    if (status == "פתוח")
                    {
                        travelClaims = travelClaims.Where(c => c.ClaimStatus == true).ToList();
                    }
                    else
                    {
                        travelClaims = travelClaims.Where(c => c.ClaimStatus == false).ToList();
                    }
                }
                if (travelClaimCondition != null)
                {
                    travelClaims = travelClaims.Where(c => c.TravelClaimConditionID == travelClaimCondition.TravelClaimConditionID).ToList();
                }
                if (travelClaimType != null)
                {
                    travelClaims = travelClaims.Where(c => c.TravelClaimTypeID == travelClaimType.TravelClaimTypeID).ToList();
                }
                if (company != null)
                {
                    travelClaims = travelClaims.Where(c => c.TravelPolicy.CompanyID == company.CompanyID).ToList();
                }
                return travelClaims;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public CarThirdParty GetCarThirdPartyByIndex(int index, int claimId)
        {
            var thirdParties = GetThirdPartiesByClaimID(claimId);
            return thirdParties[index];
        }
    }
}
