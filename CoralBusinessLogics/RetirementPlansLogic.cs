﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoralBusinessLogics
{
    public class RetirementPlansLogic
    {
        public List<RetirementPlan> GetAllRetirementPlans()
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.RetirementPlans.OrderByDescending(r => r.Status).ThenBy(r => r.RetirementPlanName).ToList();
            }
        }

        public List<RetirementPlan> GetActiveRetirementPlans()
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.RetirementPlans.Where(r => r.Status == true).OrderBy(r => r.RetirementPlanName).ToList();
            }
        }

        public RetirementPlan InsertRetirementPlans(string planName, bool status)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var planNameInContext = context.RetirementPlans.FirstOrDefault(r => r.RetirementPlanName == planName);
                if (planNameInContext == null)
                {
                    try
                    {
                        RetirementPlan newPlanName = new RetirementPlan() { RetirementPlanName = planName, Status = status };
                        context.RetirementPlans.Add(newPlanName);
                        context.SaveChanges();
                        return newPlanName;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }
                else
                {
                    throw new Exception("שם מסלול קיים במערכת");
                }
            }
        }

        public RetirementPlan UpdatePlanName(string planName, bool status)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var planNameInContext = context.RetirementPlans.FirstOrDefault(r => r.RetirementPlanName == planName);
                if (planNameInContext != null)
                {
                    try
                    {
                        planNameInContext.Status = status;
                        context.SaveChanges();
                        return planNameInContext;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }
                else
                {
                    throw new Exception("שם מסלול לא קיים במערכת");
                }
            }
        }
    }
}
