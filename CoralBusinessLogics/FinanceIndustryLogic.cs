﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoralBusinessLogics
{
    public class FinanceIndustryLogic
    {
        //מחזיר רשימה של כל הענפים
        public List<FinanceProgramType> GetAllFinanceProgramTypes()
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.FinanceProgramTypes.OrderBy(i => i.ProgramTypeName).ToList();
            }
        }


        //מחזיר רשימה של כל התכניות לפי סוג תכנית
        public List<FinanceProgram> GetFinanceProgramsByProgramTypeAndCompany(int programTypeID, int companyId)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.FinancePrograms.Where(i => i.ProgramTypeID == programTypeID&&i.CompanyID==companyId).ToList();
            }
        }

        //מחזיר רשימה של כל התכניות לפי סוג תכנית
        public List<FinanceProgram> GetFinanceProgramsByProgramType(int programTypeID)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.FinancePrograms.Where(i => i.ProgramTypeID == programTypeID).ToList();
            }
        }

        //מחזיר רשימה של כל התכניות לפי סוג תכנית
        public List<FinanceProgram> GetActiveFinanceProgramsByProgramType(int programTypeID, int companyId)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.FinancePrograms.Where(i => i.Status == true && i.ProgramTypeID == programTypeID && i.CompanyID == companyId).ToList();
            }
        }

        //מוסיף תכנית חדש
        public FinanceProgram InsertFinanceProgram(string programName, int programTypeID, int companyID, bool status)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var programInContext = context.FinancePrograms.FirstOrDefault(i => i.ProgramName == programName&&i.ProgramTypeID==programTypeID&&i.CompanyID==companyID);
                if (programInContext == null)
                {
                    try
                    {
                        FinanceProgram newType = new FinanceProgram() { ProgramTypeID = programTypeID, ProgramName = programName, CompanyID=companyID, Status = status };
                        context.FinancePrograms.Add(newType);
                        context.SaveChanges();
                        return newType;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }
                else
                {
                    throw new Exception("תכנית קיים");
                }
            }
        }

        //מעדכן את הסטטוס של תכנית קיים
        public FinanceProgram UpdateFinanceProgram(FinanceProgram program, bool status)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var programInContext = context.FinancePrograms.FirstOrDefault(i => i.ProgramName == program.ProgramName);
                if (programInContext != null)
                {
                    try
                    {
                        programInContext.Status = status;
                        context.SaveChanges();
                        return programInContext;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }
                else
                {
                    throw new Exception("תכנית לא קיים");
                }
            }
        }

    }
}
