﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoralBusinessLogics
{
    //ניהול אנשי קשר
    public class ContactsLogic //: BaseLogics
    {
        //מחזיר רשימה של כל אנשי קשר
        public List<Contact> GetAllContacts()
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.Contacts.Include("ContactCategory").OrderBy(c => c.LastName).ToList();
            }
        }

        //מוסיף איש קשר חדש
        public void InsertContact(string lastName, string firstName, string company, ContactCategory category, string city, string street, string homeNumber, string zipCode, string phone, string cellPhone, string fax, string email, string website, string comments)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {

                Contact contact = new Contact();
                contact.LastName = lastName;
                contact.FirstName = firstName;
                contact.CompanyName = company;
                if (category != null)
                {
                    contact.ContactCategoryID = category.ContactCategoryID;
                }
                contact.City = city;
                contact.Street = street;
                contact.HomeNumber = homeNumber;
                contact.ZipCode = zipCode;
                contact.Phone = phone;
                contact.CellPhone = cellPhone;
                contact.Fax = fax;
                contact.Email = email;
                contact.Website = website;
                contact.Remarks = comments;
                try
                {
                    context.Contacts.Add(contact);
                    context.SaveChanges();
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
        }

        //מעדכן איש קשר קיים
        public void UpdateContact(int id, string lastName, string firstName, string company, ContactCategory category, string city, string street, string homeNumber, string zipCode, string phone, string cellPhone, string fax, string email, string website, string comments)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {

                Contact contact = (from c in context.Contacts
                                   where c.ContactID == id
                                   select c).FirstOrDefault();
                if (contact != null)
                {
                    contact.LastName = lastName;
                    contact.FirstName = firstName;
                    contact.CompanyName = company;
                    if (category != null)
                    {
                        contact.ContactCategoryID = category.ContactCategoryID;
                    }
                    contact.City = city;
                    contact.Street = street;
                    contact.HomeNumber = homeNumber;
                    contact.ZipCode = zipCode;
                    contact.Phone = phone;
                    contact.CellPhone = cellPhone;
                    contact.Fax = fax;
                    contact.Email = email;
                    contact.Website = website;
                    contact.Remarks = comments;
                    try
                    {
                        context.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }
                else
                {
                    throw new Exception("איש קשר לא קיים");
                }
            }
        }

        //מחפש אנשי קשר לפי פרמטרים
        public List<Contact> FindContactByString(string stringSearch, ContactCategory category)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {

                if (category != null && category.ContactCategoryName != "" && category.ContactCategoryName != "הכל")
                {
                    return (from c in context.Contacts.Include("ContactCategory")
                            where c.ContactCategoryID == category.ContactCategoryID
                            where c.LastName.StartsWith(stringSearch) || c.FirstName.StartsWith(stringSearch) || (c.LastName + " " + c.FirstName).StartsWith(stringSearch) || (c.FirstName + " " + c.LastName).StartsWith(stringSearch) || c.City.StartsWith(stringSearch)
                            select c).OrderBy(c => c.LastName).ToList();
                }
                else
                {
                    return (from c in context.Contacts.Include("ContactCategory")
                            where c.LastName.StartsWith(stringSearch) || c.FirstName.StartsWith(stringSearch) || (c.LastName + " " + c.FirstName).StartsWith(stringSearch) || (c.FirstName + " " + c.LastName).StartsWith(stringSearch) || c.City.StartsWith(stringSearch)
                            select c).OrderBy(c => c.LastName).ToList();
                }
            }
        }

        //חיפוש מתקדם של אנשי קשר
        public List<Contact> FindContactAdvanceSearch(string lastName, string firstName, string company, ContactCategory category, string city, string street, string homeNumber, string zipCode, string phone, string cellPhone, string fax, string email, string website, string comments)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {

                var allContacts = GetAllContacts();
                if (lastName != "")
                {
                    allContacts = (from c in allContacts
                                   where c.LastName.Contains(lastName)
                                   select c).ToList();
                }
                if (firstName != "")
                {
                    allContacts = (from c in allContacts
                                   where c.FirstName.Contains(firstName)
                                   select c).ToList();
                }
                if (company != "")
                {
                    allContacts = (from c in allContacts
                                   where c.CompanyName.Contains(company)
                                   select c).ToList();
                }
                if (category != null)
                {
                    allContacts = (from c in allContacts
                                   where c.ContactCategoryID == category.ContactCategoryID
                                   select c).ToList();
                }
                if (city != "")
                {
                    allContacts = (from c in allContacts
                                   where c.City.Contains(city)
                                   select c).ToList();
                }
                if (street != "")
                {
                    allContacts = (from c in allContacts
                                   where c.Street.Contains(street)
                                   select c).ToList();
                }
                if (homeNumber != "")
                {
                    allContacts = (from c in allContacts
                                   where c.HomeNumber.Contains(homeNumber)
                                   select c).ToList();
                }
                if (zipCode != "")
                {
                    allContacts = (from c in allContacts
                                   where c.ZipCode.Contains(zipCode)
                                   select c).ToList();
                }
                if (phone != "")
                {
                    allContacts = (from c in allContacts
                                   where c.Phone.Contains(phone)
                                   select c).ToList();
                }
                if (cellPhone != "")
                {
                    allContacts = (from c in allContacts
                                   where c.CellPhone.Contains(cellPhone)
                                   select c).ToList();
                }
                if (fax != "")
                {
                    allContacts = (from c in allContacts
                                   where c.Fax.Contains(fax)
                                   select c).ToList();
                }
                if (email != "")
                {
                    allContacts = (from c in allContacts
                                   where c.Email.Contains(email)
                                   select c).ToList();
                }
                if (website != "")
                {
                    allContacts = (from c in allContacts
                                   where c.Website.Contains(website)
                                   select c).ToList();
                }
                if (comments != "")
                {
                    allContacts = (from c in allContacts
                                   where c.Remarks.Contains(comments)
                                   select c).ToList();
                }

                return allContacts;
            }
        }


        //מוחק איש קשר
        public void DeleteContact(Contact contactToDelete)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {

                var contactInContext = (from c in context.Contacts
                                        where c.ContactID == contactToDelete.ContactID
                                        select c).FirstOrDefault();
                if (contactInContext != null)
                {
                    try
                    {
                        context.Contacts.Remove(contactInContext);
                        context.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }
                else
                {
                    throw new Exception("איש קשר לא קיים");
                }
            }
        }
    }
}
