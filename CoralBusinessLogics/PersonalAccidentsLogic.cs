﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CoralBusinessLogics
{
    public class PersonalAccidentsLogic
    {
        public List<PersonalAccidentsPolicy> GetAllPersonalAccidentsPoliciesWithoutOffers()
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.PersonalAccidentsPolicies.Include("Insurance").Include("Client").Include("Client.Agent").Include("Client.Agent1").Include("Company").Where(p=>p.IsOffer==false).OrderByDescending(p => p.StartDate).ThenBy(p => p.Client.LastName).ToList();
            }
        }

        public List<PersonalAccidentsPolicy> GetActivePoliciesWithoutOffers()
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.PersonalAccidentsPolicies.Include("Insurance").Include("Client").Include("Client.Agent").Include("Client.Agent1").Include("Company").Where(p => p.IsOffer == false&&p.EndDate>=DateTime.Today).OrderByDescending(p => p.StartDate).ThenBy(p => p.Client.LastName).ToList();
            }
        }

        public List<PersonalAccidentsPolicy> GetPoliciesWithoutOffersByStartDate(DateTime fromDate, DateTime? toDate)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.PersonalAccidentsPolicies.Include("Insurance").Include("Client").Include("Client.Agent").Include("Client.Agent1").Include("Company").Where(p => p.IsOffer == false && p.StartDate >= fromDate && p.StartDate <= toDate).OrderByDescending(p => p.StartDate).ThenBy(p => p.Client.LastName).ToList();
            }
        }
        public int InsertPersonalAccidentsPolicy(Client client, bool isOffer, DateTime openDate, string policyNumber, int? addition, int? principalAgentNumberId, int? secundaryAgentNumberId, DateTime? startDate, DateTime? endDate, int companyId, string currency, string paymentMethod, int? paymentsNumber, DateTime? paymentDate, decimal? premium1, decimal? premium2, decimal? premium3, decimal? premium4, decimal? inscriptionFees, decimal? policyFees, decimal? projectionFees, decimal? stampsFees, decimal? handlingFees, decimal? totalPremium, decimal? totalFees, decimal? credit, string comments)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var policyInContext = context.PersonalAccidentsPolicies.FirstOrDefault(p => p.PolicyNumber == policyNumber && p.Addition == addition);
                if (policyInContext == null)
                {
                    try
                    {
                        RenewalStatus renewalStatus = context.RenewalStatuses.FirstOrDefault(r => r.RenewalStatusName == "לא טופל");
                        var insuranceInContext = context.Insurances.FirstOrDefault(i => i.InsuranceName == "תאונות אישיות");

                        PersonalAccidentsPolicy newPolicy = new PersonalAccidentsPolicy()
                        {
                            Addition = addition,
                            ClientID = client.ClientID,
                            Comments = comments,
                            CompanyID = companyId,
                            Credit = credit,
                            Currency = currency,
                            EndDate = endDate,
                            HandlingFees = handlingFees,
                            InscriptionFees = inscriptionFees,
                            InsuranceID = insuranceInContext.InsuranceID,
                            IsOffer = isOffer,
                            OpenDate = openDate,
                            PaymentDate = paymentDate,
                            PaymentMethod = paymentMethod,
                            PaymentsNumber = paymentsNumber,
                            PolicyFees = policyFees,
                            PolicyNumber = policyNumber,
                            PremiumNeto1 = premium1,
                            PremiumNeto2 = premium2,
                            PremiumNeto3 = premium3,
                            PremiumNeto4 = premium4,
                            PrincipalAgentNumberID = principalAgentNumberId,
                            ProjectionFees = projectionFees,
                            RenewalStatusID = renewalStatus.RenewalStatusID,
                            StampsFees = stampsFees,
                            StartDate = startDate,
                            SubAgentNumberID = secundaryAgentNumberId,
                            TotalFees = totalFees,
                            TotalPremium = totalPremium
                        };
                        context.PersonalAccidentsPolicies.Add(newPolicy);
                        context.SaveChanges();
                        return newPolicy.PersonalAccidentsPolicyID;
                    }
                    catch (Exception e)
                    {
                        throw new Exception(e.Message);
                    }
                }
                else
                {
                    throw new Exception("פוליסה קיימת במערכת");
                }
            }
        }

        

        public List<PersonalAccidentsPolicy> GetOffers()
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.PersonalAccidentsPolicies.Include("Client").Include("Company").Where(p => p.IsOffer == true).OrderByDescending(p => p.StartDate).ThenBy(p => p.Client.LastName).ToList();
            }
        }

        public List<PersonalAccidentsPolicy> GetActivePersonalAccidentsPoliciesByClient(Client client)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                if (client != null)
                {
                    var policies = context.PersonalAccidentsPolicies.Where(p => p.ClientID == client.ClientID && p.IsOffer == false && p.EndDate >= DateTime.Today).OrderByDescending(p => p.OpenDate).ToList();
                    var sortedPolicies = context.PersonalAccidentsPolicies.Include("Insurance").Include("Client").Include("Company").Where(p => p.ClientID == client.ClientID && p.IsOffer == false && p.EndDate >= DateTime.Today).OrderByDescending(p => p.OpenDate).ToList();
                    for (int i = policies.Count - 1; i >= 0; i--)
                    {
                        if (policies[i].Addition > 0)
                        {
                            var policy = policies.Where(p => p.PolicyNumber == policies[i].PolicyNumber && p.Addition == policies[i].Addition - 1).FirstOrDefault();
                            if (policy != null)
                            {
                                sortedPolicies.Remove(policies[i]);
                                var index = sortedPolicies.FindIndex(p => p.PolicyNumber == policy.PolicyNumber && p.Addition == policy.Addition);
                                sortedPolicies.Insert(index, policies[i]);
                            }
                        }
                    }
                    return sortedPolicies;
                }
                else
                {
                    return null;
                }
            }
        }

       
        public List<PersonalAccidentsPolicy> GetAllPersonalAccidentsOffersByClient(Client client)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                if (client == null)
                {
                    return null;
                }
                return context.PersonalAccidentsPolicies.Include("Client").Include("Insurance").Include("Company").Where(p => p.ClientID == client.ClientID && p.IsOffer == true).OrderByDescending(p => p.StartDate).ToList();
            }
        }

        public List<PersonalAccidentsPolicy> GetNonActivePersonalAccidentsPoliciesByClient(Client client)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                if (client != null)
                {
                    var policies = context.PersonalAccidentsPolicies.Where(p => p.ClientID == client.ClientID && p.IsOffer == false && p.EndDate < DateTime.Today).OrderByDescending(p => p.OpenDate).ToList();
                    var sortedPolicies = context.PersonalAccidentsPolicies.Include("Insurance").Include("Client").Include("Company").Where(p => p.ClientID == client.ClientID && p.IsOffer == false && p.EndDate < DateTime.Today).OrderByDescending(p => p.OpenDate).ToList();
                    for (int i = policies.Count - 1; i >= 0; i--)
                    {
                        if (policies[i].Addition > 0)
                        {
                            var policy = policies.Where(p => p.PolicyNumber == policies[i].PolicyNumber && p.Addition == policies[i].Addition - 1).FirstOrDefault();
                            if (policy != null)
                            {
                                sortedPolicies.Remove(policies[i]);
                                var index = sortedPolicies.FindIndex(p => p.PolicyNumber == policy.PolicyNumber && p.Addition == policy.Addition);
                                sortedPolicies.Insert(index, policies[i]);
                            }
                        }
                    }
                    return sortedPolicies;
                }
                else
                {
                    return null;
                }
            }
        }

        public void TransferPolicyToClient(PersonalAccidentsPolicy policy, Client client)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var policyInContext = context.PersonalAccidentsPolicies.FirstOrDefault(p => p.PersonalAccidentsPolicyID == policy.PersonalAccidentsPolicyID);
                if (policyInContext != null)
                {
                    policyInContext.ClientID = client.ClientID;
                    context.SaveChanges();
                }
                else
                {
                    throw new Exception("פוליסה לא קיימת במערכת");
                }
            }
        }

        public List<PersonalAccidentsPolicy> GetAllPersonalAccidentsPoliciesByClient(Client client)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                if (client != null)
                {
                    var policies = context.PersonalAccidentsPolicies.Where(p => p.ClientID == client.ClientID && p.IsOffer == false).OrderByDescending(p => p.OpenDate).ToList();
                    var sortedPolicies = context.PersonalAccidentsPolicies.Include("Insurance").Include("Client").Include("Company").Where(p => p.ClientID == client.ClientID && p.IsOffer == false).OrderByDescending(p => p.OpenDate).ToList();
                    for (int i = policies.Count - 1; i >= 0; i--)
                    {
                        if (policies[i].Addition > 0)
                        {
                            var policy = policies.Where(p => p.PolicyNumber == policies[i].PolicyNumber && p.Addition == policies[i].Addition - 1).FirstOrDefault();
                            if (policy != null)
                            {
                                sortedPolicies.Remove(policies[i]);
                                var index = sortedPolicies.FindIndex(p => p.PolicyNumber == policy.PolicyNumber && p.Addition == policy.Addition);
                                sortedPolicies.Insert(index, policies[i]);
                            }
                        }
                    }
                    return sortedPolicies;
                }
                else
                {
                    return null;
                }
            }
        }

        public InsuredType GetInsuredTypeByTypeId(int insuredTypeID)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.InsuredTypes.FirstOrDefault(i => i.InsuredTypeID == insuredTypeID);
            }
        }

        public PersonalAccidentsInsurancesDetail GetInsuranceDetails(int personalAccidentsPolicyID, int insuredTypeID)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.PersonalAccidentsInsurancesDetails.FirstOrDefault(d => d.PersonalAccidentsPolicyID == personalAccidentsPolicyID && d.InsuredTypeID == insuredTypeID);
            }
        }

        public List<PersonalAccidentsAdditionalInsured> GetAdditionalInsuredsByPolicyId(int personalAccidentsPolicyID)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.PersonalAccidentsAdditionalInsureds.Where(i => i.PersonalAccidensPolicyID == personalAccidentsPolicyID).ToList();
            }
        }

        public List<InsuredType> GetInsuredTypes()
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.InsuredTypes.ToList();
            }
        }



        public List<PersonalAccidentsPolicy> GetActivePersonalAccidentsPoliciesWithoutAdditionByClient(Client client)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.PersonalAccidentsPolicies.Include("Insurance").Where(p => p.ClientID == client.ClientID && p.Addition == 0 && p.IsOffer == false).OrderByDescending(p => p.StartDate).ToList();
            }
        }

        public void UpdatePersonalAccidentsPolicy(int personalAccidentsPolicyID, string policyNumber, int? principalAgentNumberID, int? subAgentNumberID, DateTime startDate, DateTime endDate, int companyID, string currency, string paymentMode, int? paymentNumbers, DateTime? paymentDate, decimal? premium1, decimal? premium2, decimal? premium3, decimal? premium4, decimal? inscriptionFees, decimal? policyFees, decimal? projectionFees, decimal? stampFees, decimal? handlingFees, decimal? totalPremium, decimal? totalFees, decimal? totalCredit, string comments)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var policyInContext = context.PersonalAccidentsPolicies.Where(p => p.PersonalAccidentsPolicyID == personalAccidentsPolicyID).FirstOrDefault();
                if (policyInContext != null)
                {
                    try
                    {
                        policyInContext.PolicyNumber = policyNumber;
                        policyInContext.PrincipalAgentNumberID = principalAgentNumberID;
                        policyInContext.SubAgentNumberID = subAgentNumberID;
                        policyInContext.StartDate = startDate;
                        policyInContext.EndDate = endDate;
                        policyInContext.CompanyID = companyID;
                        policyInContext.Currency = currency;
                        policyInContext.PaymentMethod = paymentMode;
                        policyInContext.PaymentsNumber = paymentNumbers;
                        policyInContext.PaymentDate = paymentDate;
                        policyInContext.PremiumNeto1 = premium1;
                        policyInContext.PremiumNeto2 = premium2;
                        policyInContext.PremiumNeto3 = premium3;
                        policyInContext.PremiumNeto4 = premium4;
                        policyInContext.InscriptionFees = inscriptionFees;
                        policyInContext.PolicyFees = policyFees;
                        policyInContext.ProjectionFees = projectionFees;
                        policyInContext.StampsFees = stampFees;
                        policyInContext.HandlingFees = handlingFees;
                        policyInContext.TotalPremium = totalPremium;
                        policyInContext.TotalFees = totalFees;
                        policyInContext.Credit = totalCredit;
                        policyInContext.Comments = comments;
                        context.SaveChanges();
                    }
                    catch (Exception e)
                    {
                        throw new Exception(e.Message);
                    }
                }
                else
                {
                    throw new Exception("פוליסה לא קיימת במערכת");
                }
            }
        }

        public void InsertAdditionalInsureds(int policyId, List<PersonalAccidentsAdditionalInsured> insureds, bool isAddition)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var policyInContext = context.PersonalAccidentsPolicies.FirstOrDefault(c => c.PersonalAccidentsPolicyID == policyId);

                if (policyInContext != null)
                {
                    try
                    {
                        var policyInsureds = context.PersonalAccidentsAdditionalInsureds.Where(c => c.PersonalAccidensPolicyID == policyId).ToList();
                        foreach (PersonalAccidentsAdditionalInsured insured in insureds)
                        {
                            if (insured.PersonalAccidentsAdditionalInsuredID == 0)
                            {
                                insured.PersonalAccidensPolicyID = policyId;
                                insured.InsuredType = null;
                                context.PersonalAccidentsAdditionalInsureds.Add(insured);
                            }
                            else if (isAddition)
                            {
                                PersonalAccidentsAdditionalInsured newInsured = new PersonalAccidentsAdditionalInsured() { BirthDate = insured.BirthDate, FirstName = insured.FirstName, IdNumber = insured.IdNumber, InsuredTypeID = insured.InsuredTypeID, IsMale = insured.IsMale, LastName = insured.LastName, MaritalStatus = insured.MaritalStatus, PersonalAccidensPolicyID = policyId, Profession = insured.Profession };
                                context.PersonalAccidentsAdditionalInsureds.Add(newInsured);
                            }
                            else
                            {
                                var insuredInContext = context.PersonalAccidentsAdditionalInsureds.FirstOrDefault(c => c.PersonalAccidentsAdditionalInsuredID == insured.PersonalAccidentsAdditionalInsuredID);
                                if (insuredInContext != null)
                                {
                                    insuredInContext.BirthDate = insured.BirthDate;
                                    insuredInContext.FirstName = insured.FirstName;
                                    insuredInContext.IdNumber = insured.IdNumber;
                                    insuredInContext.InsuredTypeID = insured.InsuredTypeID;
                                    insuredInContext.IsMale = insured.IsMale;
                                    insuredInContext.LastName = insured.LastName;
                                    insuredInContext.MaritalStatus = insured.MaritalStatus;
                                    insuredInContext.Profession = insured.Profession;

                                    policyInsureds.Remove(insuredInContext);
                                }
                            }
                        }
                        if (policyInsureds.Count > 0)
                        {
                            foreach (var item in policyInsureds)
                            {
                                context.PersonalAccidentsAdditionalInsureds.Remove(item);
                            }
                        }
                        context.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }
                else
                {
                    throw new Exception("פוליסה לא קיימת במערכת");
                }
            }
        }

        public void InsertInsurancesDetails(PersonalAccidentsInsurancesDetail[] personalAccidentsInsurancesDetail)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                try
                {
                    foreach (var item in personalAccidentsInsurancesDetail)
                    {
                        if (item == null)
                        {
                            continue;
                        }
                        var policyInContext = context.PersonalAccidentsPolicies.FirstOrDefault(c => c.PersonalAccidentsPolicyID == item.PersonalAccidentsPolicyID);

                        if (policyInContext != null)
                        {
                            if (item.PersonalAccidentsInsuranceDetailsID == 0)
                            {
                                context.PersonalAccidentsInsurancesDetails.Add(item);
                            }
                            else
                            {
                                var insuranceDetailInContext = context.PersonalAccidentsInsurancesDetails.FirstOrDefault(d => d.PersonalAccidentsInsuranceDetailsID == item.PersonalAccidentsInsuranceDetailsID);
                                if (insuranceDetailInContext != null)
                                {
                                    insuranceDetailInContext.CompensationForHospitalizationAmount = item.CompensationForHospitalizationAmount;
                                    insuranceDetailInContext.CompensationForHospitalizationWaitingDays = item.CompensationForHospitalizationWaitingDays;
                                    insuranceDetailInContext.ComplementaryAmount = item.ComplementaryAmount;
                                    insuranceDetailInContext.DeathByAccidentAmount = item.DeathByAccidentAmount;
                                    insuranceDetailInContext.DiagnosticTestsAndPregnancyAnnexAmount = item.DiagnosticTestsAndPregnancyAnnexAmount;
                                    insuranceDetailInContext.DiagnosticTestsAndPregnancyAnnexType = item.DiagnosticTestsAndPregnancyAnnexType;
                                    insuranceDetailInContext.DisabilityByAccidentAmount = item.DisabilityByAccidentAmount;
                                    insuranceDetailInContext.ExtremeSportCancellationAmount = item.ExtremeSportCancellationAmount;
                                    insuranceDetailInContext.FastDiagnosisAndMedicalGuidanceAmount = item.FastDiagnosisAndMedicalGuidanceAmount;
                                    insuranceDetailInContext.FracturesAndBurnsByAccidentAmount = item.FracturesAndBurnsByAccidentAmount;
                                    insuranceDetailInContext.IncapacityByAccidentAmount = item.IncapacityByAccidentAmount;
                                    insuranceDetailInContext.IncapacityByAccidentPeriod = item.IncapacityByAccidentPeriod;
                                    insuranceDetailInContext.IncapacityByAccidentWaitingDays = item.IncapacityByAccidentWaitingDays;
                                    insuranceDetailInContext.IncapacityBySicknessAmount = item.IncapacityBySicknessAmount;
                                    insuranceDetailInContext.IncapacityBySicknessPeriod = item.IncapacityBySicknessPeriod;
                                    insuranceDetailInContext.IncapacityBySicknessWaitingDays = item.IncapacityBySicknessWaitingDays;
                                    insuranceDetailInContext.InsuredTypeID = item.InsuredTypeID;
                                    insuranceDetailInContext.IsCompensationForHospitalization = item.IsCompensationForHospitalization;
                                    insuranceDetailInContext.IsComplementary = item.IsComplementary;
                                    insuranceDetailInContext.IsDeathByAccident = item.IsDeathByAccident;
                                    insuranceDetailInContext.IsDiagnosticTestsAndPregnancyAnnex = item.IsDiagnosticTestsAndPregnancyAnnex;
                                    insuranceDetailInContext.IsDisabilityByAccident = item.IsDisabilityByAccident;
                                    insuranceDetailInContext.IsExtremeSportCancellation = item.IsExtremeSportCancellation;
                                    insuranceDetailInContext.IsFastDiagnosisAndMedicalGuidance = item.IsFastDiagnosisAndMedicalGuidance;
                                    insuranceDetailInContext.IsFracturesAndBurnsByAccident = item.IsFracturesAndBurnsByAccident;
                                    insuranceDetailInContext.IsIncapacityByAccident = item.IsIncapacityByAccident;
                                    insuranceDetailInContext.IsIncapacityBySickness = item.IsIncapacityBySickness;
                                    insuranceDetailInContext.IsMedicalAdviceAndPeriodiCheck = item.IsMedicalAdviceAndPeriodiCheck;
                                    insuranceDetailInContext.IsMedicalAndDentalCharges = item.IsMedicalAndDentalCharges;
                                    insuranceDetailInContext.IsMedicalCharges = item.IsMedicalCharges;
                                    insuranceDetailInContext.IsOther1 = item.IsOther1;
                                    insuranceDetailInContext.IsOther2 = item.IsOther2;
                                    insuranceDetailInContext.IsOther3 = item.IsOther3;
                                    insuranceDetailInContext.IsOther4 = item.IsOther4;
                                    insuranceDetailInContext.IsOther5 = item.IsOther5;
                                    insuranceDetailInContext.IsPsicology = item.IsPsicology;
                                    insuranceDetailInContext.IsSiudByAccident = item.IsSiudByAccident;
                                    insuranceDetailInContext.IsSports = item.IsSports;
                                    insuranceDetailInContext.IsToTheChild = item.IsToTheChild;
                                    insuranceDetailInContext.IsTwoWheeledCancellation = item.IsTwoWheeledCancellation;
                                    insuranceDetailInContext.IsWarRiskCoverage = item.IsWarRiskCoverage;
                                    insuranceDetailInContext.MedicalAdviceAndPeriodiCheckAmount = item.MedicalAdviceAndPeriodiCheckAmount;
                                    insuranceDetailInContext.MedicalAndDentalChargesAmount = item.MedicalAndDentalChargesAmount;
                                    insuranceDetailInContext.MedicalChargesAmount = item.MedicalChargesAmount;
                                    insuranceDetailInContext.Other1Amount = item.Other1Amount;
                                    insuranceDetailInContext.Other2Amount = item.Other2Amount;
                                    insuranceDetailInContext.Other3Amount = item.Other3Amount;
                                    insuranceDetailInContext.Other4Amount = item.Other4Amount;
                                    insuranceDetailInContext.Other5Amount = item.Other5Amount;
                                    insuranceDetailInContext.Other1Name = item.Other1Name;
                                    insuranceDetailInContext.Other2Name = item.Other2Name;
                                    insuranceDetailInContext.Other3Name = item.Other3Name;
                                    insuranceDetailInContext.Other4Name = item.Other4Name;
                                    insuranceDetailInContext.Other5Name = item.Other5Name;
                                    insuranceDetailInContext.PersonalAccidentsInsurenceTypeID = item.PersonalAccidentsInsurenceTypeID;
                                    insuranceDetailInContext.PsicologyAmount = item.PsicologyAmount;
                                    insuranceDetailInContext.SiudByAccidentAmount = item.SiudByAccidentAmount;
                                    insuranceDetailInContext.SportsAmount = item.SportsAmount;
                                    insuranceDetailInContext.ToTheChildAmount = item.ToTheChildAmount;
                                    insuranceDetailInContext.TwoWheeledCancellationAmount = item.TwoWheeledCancellationAmount;
                                    insuranceDetailInContext.TwoWheeledCancellationUpToCC = item.TwoWheeledCancellationUpToCC;
                                    insuranceDetailInContext.WarRiskCoverageAmount = item.WarRiskCoverageAmount;
                                }
                            }
                        }
                        else
                        {
                            throw new Exception("פוליסה לא קיימת במערכת");
                        }
                    }
                    context.SaveChanges();
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
        }

        public string GetAdditionNumber(string policyNumber)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var policiesInContext = context.PersonalAccidentsPolicies.Where(p => p.PolicyNumber == policyNumber);
                int? addition = 0;
                foreach (var item in policiesInContext)
                {
                    if (item.Addition > addition)
                    {
                        addition = item.Addition;
                    }
                }
                return (addition + 1).ToString();
            }
        }

        public void ChangePersonalAccidentsPolicyOfferStatus(int personalAccidentsPolicyID, bool isOffer)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var policyInContext = context.PersonalAccidentsPolicies.FirstOrDefault(p => p.PersonalAccidentsPolicyID == personalAccidentsPolicyID);
                policyInContext.IsOffer = isOffer;
                context.SaveChanges();
            }
        }

        public PersonalAccidentsPolicy GetPersonalAccidentsPolicyByPolicyId(int policyId)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.PersonalAccidentsPolicies.Include("Client").FirstOrDefault(p => p.PersonalAccidentsPolicyID == policyId);
            }
        }
    }
}
