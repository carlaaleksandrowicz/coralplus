﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoralBusinessLogics
{
    //ניהול לוגיקת הפוליסות
    public class PoliciesLogic
    {
        RenewalsLogic renewalsLogics = new RenewalsLogic();
        //מחזיר רשימה של כל הפוליסות האלמנטריות
        public List<ElementaryPolicy> GetAllElementaryPoliciesWithoutOffers()
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.ElementaryPolicies.Include("Client").Include("Client.Agent").Include("Client.Agent1").Include("Insurance").Include("Company").Include("ElementaryInsuranceType").Include("InsuranceIndustry").Include("CarPolicy").Include("BusinessPolicy").Include("ApartmentPolicy").Where(p => p.IsOffer == false).OrderByDescending(p => p.StartDate).ThenBy(p => p.Client.LastName).ToList();
                //.Include("ElementaryPolicy.Company").Include("ElementaryPolicy.InsuranceIndustry").Include("ElementaryPolicy.ElementaryInsuranceType").Include("ElementaryPolicy.CarPolicy").Include("ElementaryPolicy.Client")
            }
        }
        public List<ElementaryPolicy> GetActivePoliciesWithoutOffers()
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.ElementaryPolicies.Include("Client").Include("Client.Agent").Include("Client.Agent1").Include("Insurance").Include("Company").Include("ElementaryInsuranceType").Include("InsuranceIndustry").Include("CarPolicy").Include("BusinessPolicy").Include("ApartmentPolicy").Where(p => p.IsOffer == false&&p.EndDate>=DateTime.Today).OrderByDescending(p => p.StartDate).ThenBy(p => p.Client.LastName).ToList();
            }
        }

        //מחזיר רשימה של פוליסות אלמנטריות של לקוח מסוים
        public List<ElementaryPolicy> GetAllElementaryPoliciesByClient(Client client)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                if (client != null)
                {
                    //return context.ElementaryPolicies.Include("CarPolicy").Include("BusinessPolicy").Include("ApartmentPolicy").Include("Company").Include("ElementaryInsuranceType").Include("InsuranceIndustry").Where(p => p.ClientID == client.ClientID).OrderBy(p => p.PolicyNumber).ThenByDescending(p => p.StartDate).ToList();
                    var policies = context.ElementaryPolicies.Where(p => p.ClientID == client.ClientID).OrderByDescending(p => p.StartDate).ThenByDescending(p => p.OpenDate).ToList();
                    var sortedPolicies = context.ElementaryPolicies.Include("Insurance").Include("Client").Include("RenewalStatus").Include("CarPolicy").Include("BusinessPolicy").Include("ApartmentPolicy").Include("Company").Include("ElementaryInsuranceType").Include("InsuranceIndustry").Where(p => p.ClientID == client.ClientID).OrderByDescending(p => p.StartDate).ThenByDescending(p => p.OpenDate).ToList();
                    for (int i = policies.Count - 1; i >= 0; i--)
                    //foreach (var item in policies)
                    {
                        //if (item.Addition > 0)
                        if (policies[i].Addition > 0)
                        {
                            //var policy = policies.Where(p => p.PolicyNumber == item.PolicyNumber && p.Addition == item.Addition - 1).FirstOrDefault();
                            //int? additionCount = policies[i].Addition - 1;
                            var policy = policies.Where(p => p.PolicyNumber == policies[i].PolicyNumber && p.Addition == policies[i].Addition - 1).FirstOrDefault();
                            //do
                            //{
                            //    var policy = policies.Where(p => p.PolicyNumber == policies[i].PolicyNumber && p.Addition == additionCount).FirstOrDefault();

                            //} while (true);
                            if (policy != null)
                            {
                                //sortedPolicies.Remove(item);
                                sortedPolicies.Remove(policies[i]);
                                var index = sortedPolicies.FindIndex(p => p.PolicyNumber == policy.PolicyNumber && p.Addition == policy.Addition);
                                sortedPolicies.Insert(index, policies[i]);
                            }
                        }
                    }
                    return sortedPolicies;
                }
                else
                {
                    return null;
                }
            }
        }

        public void TransferPolicyToClient(ElementaryPolicy elementaryPolicy, Client client)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var policyInContext = context.ElementaryPolicies.FirstOrDefault(p => p.ElementaryPolicyID == elementaryPolicy.ElementaryPolicyID);
                if (policyInContext!=null)
                {
                    policyInContext.ClientID = client.ClientID;
                    context.SaveChanges();
                }
                else
                {
                    throw new Exception("פוליסה לא קיימת במערכת");
                }
            }
        }

        public List<ElementaryPolicy> GetOffers()
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.ElementaryPolicies.Include("Client").Include("CarPolicy").Include("BusinessPolicy").Include("ApartmentPolicy").Include("Company").Include("ElementaryInsuranceType").Include("InsuranceIndustry").Where(p => p.IsOffer == true).OrderByDescending(p => p.StartDate).ThenBy(p => p.Client.LastName).ToList();
            }
        }

        

        //מחזיר רשימה של פוליסות אלמנטריות בתוקף של לקוח מסוים
        public List<ElementaryPolicy> GetAllActiveElementaryPoliciesByClient(Client client)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                if (client != null)
                {
                    var policies = context.ElementaryPolicies.Where(p => p.ClientID == client.ClientID && p.EndDate >= DateTime.Today && p.IsOffer == false).OrderByDescending(p => p.StartDate).ThenByDescending(p => p.OpenDate).ToList();
                    var sortedPolicies = context.ElementaryPolicies.Include("Client").Include("RenewalStatus").Include("CarPolicy").Include("BusinessPolicy").Include("ApartmentPolicy").Include("Company").Include("ElementaryInsuranceType").Include("InsuranceIndustry").Include("Insurance").Where(p => p.ClientID == client.ClientID && p.EndDate >= DateTime.Today && p.IsOffer == false).OrderByDescending(p => p.StartDate).ThenByDescending(p => p.OpenDate).ToList();
                    for (int i = policies.Count - 1; i >= 0; i--)
                    //foreach (var item in policies)
                    {
                        //if (item.Addition > 0)
                        if (policies[i].Addition > 0)
                        {
                            //var policy = policies.Where(p => p.PolicyNumber == item.PolicyNumber && p.Addition == item.Addition - 1).FirstOrDefault();
                            //int? additionCount = policies[i].Addition - 1;
                            var policy = policies.Where(p => p.PolicyNumber == policies[i].PolicyNumber && p.Addition == policies[i].Addition - 1).FirstOrDefault();
                            //do
                            //{
                            //    var policy = policies.Where(p => p.PolicyNumber == policies[i].PolicyNumber && p.Addition == additionCount).FirstOrDefault();

                            //} while (true);
                            if (policy != null)
                            {
                                //sortedPolicies.Remove(item);
                                sortedPolicies.Remove(policies[i]);
                                var index = sortedPolicies.FindIndex(p => p.PolicyNumber == policy.PolicyNumber && p.Addition == policy.Addition);
                                sortedPolicies.Insert(index, policies[i]);
                            }
                        }
                    }
                    return sortedPolicies;
                }
                else
                {
                    return null;
                }
            }
        }

        //מחזיר רשימה של פוליסות אלמנטריות לא בתוקף של לקוח מסוים
        public List<ElementaryPolicy> GetAllNonActiveElementaryPoliciesByClient(Client client)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                if (client != null)
                {
                    //return context.ElementaryPolicies.Include("CarPolicy").Include("BusinessPolicy").Include("ApartmentPolicy").Include("Company").Include("ElementaryInsuranceType").Include("InsuranceIndustry").Where(p => p.ClientID == client.ClientID && p.EndDate < DateTime.Now).OrderBy(p => p.PolicyNumber).ThenByDescending(p => p.StartDate).ToList();
                    var policies = context.ElementaryPolicies.Where(p => p.ClientID == client.ClientID && p.EndDate < DateTime.Today).OrderByDescending(p => p.StartDate).ThenByDescending(p => p.OpenDate).ToList();
                    var sortedPolicies = context.ElementaryPolicies.Include("Insurance").Include("Client").Include("RenewalStatus").Include("CarPolicy").Include("BusinessPolicy").Include("ApartmentPolicy").Include("Company").Include("ElementaryInsuranceType").Include("InsuranceIndustry").Where(p => p.ClientID == client.ClientID && p.EndDate < DateTime.Today && p.IsOffer == false).OrderByDescending(p => p.StartDate).ThenByDescending(p => p.OpenDate).ToList();
                    for (int i = policies.Count - 1; i >= 0; i--)
                    //foreach (var item in policies)
                    {
                        //if (item.Addition > 0)
                        if (policies[i].Addition > 0)
                        {
                            //var policy = policies.Where(p => p.PolicyNumber == item.PolicyNumber && p.Addition == item.Addition - 1).FirstOrDefault();
                            //int? additionCount = policies[i].Addition - 1;
                            var policy = policies.Where(p => p.PolicyNumber == policies[i].PolicyNumber && p.Addition == policies[i].Addition - 1).FirstOrDefault();
                            //do
                            //{
                            //    var policy = policies.Where(p => p.PolicyNumber == policies[i].PolicyNumber && p.Addition == additionCount).FirstOrDefault();

                            //} while (true);
                            if (policy != null)
                            {
                                //sortedPolicies.Remove(item);
                                sortedPolicies.Remove(policies[i]);
                                var index = sortedPolicies.FindIndex(p => p.PolicyNumber == policy.PolicyNumber && p.Addition == policy.Addition);
                                sortedPolicies.Insert(index, policies[i]);
                            }
                        }
                    }
                    return sortedPolicies;
                }
                else
                {
                    return null;
                }
            }
        }

        public bool IsLastPolicyAddition(ElementaryPolicy policy, List<ElementaryPolicy> allRenewals)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                List<ElementaryPolicy> policiesInContext = allRenewals.Where(p => p.PolicyNumber == policy.PolicyNumber && p.InsuranceIndustryID == policy.InsuranceIndustryID).ToList();
                if (policiesInContext.Count()==1)
                {
                    return true;
                }
                int? addition = 0;
                foreach (var item in policiesInContext)
                {
                    if (item.Addition > addition)
                    {
                        addition = item.Addition;
                    }
                }
                if (policy.Addition==addition)
                {
                    return true;
                }
                return false;
            }
        }

        public List<ElementaryPolicy> GetAllActiveElementaryPoliciesWithoutAdditionsByClient(Client client)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.ElementaryPolicies.Include("InsuranceIndustry").Where(p => p.ClientID == client.ClientID && p.Addition == 0 && p.IsOffer == false).OrderByDescending(p => p.StartDate).ToList();

            }
        }

        public List<ElementaryPolicy> GetAllOffersByClient(Client client)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                if (client == null)
                {
                    return null;
                }
                return context.ElementaryPolicies.Include("Client").Include("Insurance").Include("CarPolicy").Include("BusinessPolicy").Include("ApartmentPolicy").Include("Company").Include("ElementaryInsuranceType").Include("InsuranceIndustry").Where(p => p.ClientID == client.ClientID && p.IsOffer == true).OrderByDescending(p => p.StartDate).ToList();

            }
        }

        //מוסיף פוליסה אלמנטרית
        public int InsertElementaryPolicy(Client client, bool isOffer, DateTime openDate, InsuranceIndustry industry, string policyNumber, int? addition, AgentNumber principalAgentNumber, AgentNumber secundaryAgentNumber, DateTime? startDate, DateTime? endDate, InsuranceCompany company, ElementaryInsuranceType insuranceType, string currency, string paymentMethod, int? paymentsNumber, DateTime? paymentDate, decimal? premium1, decimal? premium2, decimal? premium3, decimal? premium4, decimal? inscriptionFees, decimal? policyFees, decimal? projectionFees, decimal? stampsFees, decimal? handlingFees, decimal? totalPremium, decimal? totalFees, decimal? credit, bool? isMortgaged, string bankName, string branch, string bankAddress, string bankPhone1, string bankFax, string bankContactName, string bankEmail, string comments, bool isNew)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                bool isAddition = true;
                bool isCarCopy = true;
                var policyInContext = context.ElementaryPolicies.Where(p => p.PolicyNumber == policyNumber);
                foreach (var item in policyInContext)
                {
                    if (item.Addition == addition && item.InsuranceIndustryID == industry.InsuranceIndustryID)
                    {
                        isAddition = false;
                        break;
                    }

                }
                foreach (var item in policyInContext)
                {
                    if (item.InsuranceIndustryID == industry.InsuranceIndustryID)
                    {
                        isCarCopy = false;

                    }

                }

                if (policyInContext == null || isAddition || isCarCopy)
                {
                    try
                    {
                        var insuranceInContext = context.Insurances.FirstOrDefault(i => i.InsuranceName == "אלמנטרי");
                        int? principalAgentNumberID = null;
                        if (principalAgentNumber != null)
                        {
                            principalAgentNumberID = principalAgentNumber.AgentNumberID;
                        }
                        int? secundaryAgentNumberID = null;
                        if (secundaryAgentNumber != null)
                        {
                            secundaryAgentNumberID = secundaryAgentNumber.AgentNumberID;
                        }
                        int? companyID = null;
                        if (company != null)
                        {
                            companyID = company.CompanyID;
                        }
                        RenewalStatus renewalStatus = context.RenewalStatuses.FirstOrDefault(r => r.RenewalStatusName == "לא טופל");
                        ElementaryPolicy newPolicy = new ElementaryPolicy() { InsuranceID = insuranceInContext.InsuranceID, ClientID = client.ClientID, IsOffer = isOffer, OpenDate = openDate, InsuranceIndustryID = industry.InsuranceIndustryID, PolicyNumber = policyNumber, Addition = addition, PrincipalAgentNumberID = principalAgentNumberID, SubAgentNumberID = secundaryAgentNumberID, StartDate = startDate, EndDate = endDate, CompanyID = companyID, ElementaryInsuranceTypeID = insuranceType.ElementaryInsuranceTypeID, Currency = currency, PaymentMethod = paymentMethod, PaymentsNumber = paymentsNumber, PaymentDate = paymentDate, PremiumNeto1 = premium1, PremiumNeto2 = premium2, PremiumNeto3 = premium3, PremiumNeto4 = premium4, InscriptionFees = inscriptionFees, PolicyFees = policyFees, ProjectionFees = projectionFees, StampsFees = stampsFees, HandlingFees = handlingFees, TotalPremium = totalPremium, TotalFees = totalFees, Credit = credit, IsMortgaged = isMortgaged, BankName = bankName, BankBranchNumber = branch, BankAddress = bankAddress, BankPhone1 = bankPhone1, BankContactName = bankContactName, BankFax = bankFax, BankEmail = bankEmail, Comments = comments, RenewalStatusID = renewalStatus.RenewalStatusID, isNew = isNew };
                        context.ElementaryPolicies.Add(newPolicy);
                        context.SaveChanges();
                        return newPolicy.ElementaryPolicyID;
                    }
                    catch (Exception e)
                    {
                        throw new Exception(e.Message);
                    }
                }
                else
                {
                    throw new Exception("פוליסה קיימת במערכת");
                }
            }
        }

        public List<ElementaryPolicy> GetPoliciesWithoutOffersByStartDate(DateTime fromDate, DateTime? toDate)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.ElementaryPolicies.Include("Client").Include("Client.Agent").Include("Client.Agent1").Include("Insurance").Include("Company").Include("ElementaryInsuranceType").Include("InsuranceIndustry").Include("CarPolicy").Include("BusinessPolicy").Include("ApartmentPolicy").Where(p => p.IsOffer==false&&p.StartDate >= fromDate && p.StartDate <= toDate).OrderByDescending(p => p.StartDate).ThenBy(p => p.Client.LastName).ToList();
            }
        }

        //מעדכן פוליסה אלמנטרית
        public void UpdateElementaryPolicy(int policyId,string oldPolicyNumber, int oldIndustryID, bool isOffer, string newPolicyNumber, int newIndustryID, int? addition, AgentNumber principalAgentNumber, AgentNumber secundaryAgentNumber, DateTime? startDate, DateTime? endDate, InsuranceCompany company, ElementaryInsuranceType insuranceType, string currency, string paymentMethod, int? paymentsNumber, DateTime? paymentDate, decimal? premium1, decimal? premium2, decimal? premium3, decimal? premium4, decimal? inscriptionFees, decimal? policyFees, decimal? projectionFees, decimal? stampsFees, decimal? handlingFees, decimal? totalPremium, decimal? totalFees, decimal? credit, bool? isMortgaged, string bankName, string branch, string bankAddress, string bankPhone1, string bankFax, string bankContactName, string bankEmail, string comments, bool isNew)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                //var policyInContext = context.ElementaryPolicies.Where(p => p.PolicyNumber == oldPolicyNumber && p.InsuranceIndustryID == oldIndustryID).Where(p => p.Addition == addition).FirstOrDefault();
                var policyInContext = context.ElementaryPolicies.FirstOrDefault(p => p.ElementaryPolicyID==policyId);
                if (policyInContext != null)
                {
                    try
                    {
                        int? principalAgentNumberID = null;
                        if (principalAgentNumber != null)
                        {
                            principalAgentNumberID = principalAgentNumber.AgentNumberID;
                        }
                        int? secundaryAgentNumberID = null;
                        if (secundaryAgentNumber != null)
                        {
                            secundaryAgentNumberID = secundaryAgentNumber.AgentNumberID;
                        }
                        int? companyID = null;
                        if (company != null)
                        {
                            companyID = company.CompanyID;
                        }
                        policyInContext.IsOffer = isOffer;
                        policyInContext.PolicyNumber = newPolicyNumber;
                        policyInContext.InsuranceIndustryID = newIndustryID;
                        //policyInContext.Addition = addition;                        
                        policyInContext.PrincipalAgentNumberID = principalAgentNumberID;
                        policyInContext.SubAgentNumberID = secundaryAgentNumberID;
                        policyInContext.StartDate = startDate;
                        policyInContext.EndDate = endDate;
                        policyInContext.CompanyID = companyID;
                        policyInContext.ElementaryInsuranceTypeID = insuranceType.ElementaryInsuranceTypeID;
                        policyInContext.Currency = currency;
                        policyInContext.PaymentMethod = paymentMethod;
                        policyInContext.PaymentsNumber = paymentsNumber;
                        policyInContext.PaymentDate = paymentDate;
                        policyInContext.PremiumNeto1 = premium1;
                        policyInContext.PremiumNeto2 = premium2;
                        policyInContext.PremiumNeto3 = premium3;
                        policyInContext.PremiumNeto4 = premium4;
                        policyInContext.InscriptionFees = inscriptionFees;
                        policyInContext.PolicyFees = policyFees;
                        policyInContext.ProjectionFees = projectionFees;
                        policyInContext.StampsFees = stampsFees;
                        policyInContext.HandlingFees = handlingFees;
                        policyInContext.TotalPremium = totalPremium;
                        policyInContext.TotalFees = totalFees;
                        policyInContext.Credit = credit;
                        policyInContext.IsMortgaged = isMortgaged;
                        policyInContext.BankName = bankName;
                        policyInContext.BankBranchNumber = branch;
                        policyInContext.BankAddress = bankAddress;
                        policyInContext.BankPhone1 = bankPhone1;
                        policyInContext.BankFax = bankFax;
                        policyInContext.BankContactName = bankContactName;
                        policyInContext.BankEmail = bankEmail;
                        policyInContext.Comments = comments;
                        policyInContext.isNew = isNew;
                        context.SaveChanges();
                    }
                    catch (Exception e)
                    {
                        throw new Exception(e.Message);
                    }
                }
                else
                {
                    throw new Exception("פוליסה לא קיימת במערכת");
                }
            }
        }

        //מוסיף פוליסת דירה
        public void InsertApartmentPolicy(int policyId,string policyNumber, int? addition, string city, string street, string homeNumber, string apartmentNumber, string zipCode, string floor, string totalFloors, decimal? size, string roomsNumber, StructureType structureType, string protectionRequired, string contactName, string contactPhone, string contactCellPhone, string contactFax, string contactEmail, bool? isSurveyDone, DateTime? surveyDate, int? surveyByAppraisserID, bool? insuranceThisYear, bool? insuranceLastYear, bool? insurance2YearsAgo, bool? insurance3YearsAgo, int? claimsNumberThisYear, int? claimsNumberLastYear, int? claimsNumber2YearsAgo, int? claimsNumber3YearsAgo, string insuranceCompanyThisYear, string InsuranceCompanyLastYear, string InsuranceCompany2YearsAgo, string insuranceCompany3YearsAgo, string comments)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                //var policyInContext = context.ElementaryPolicies.Where(p => p.PolicyNumber == policyNumber).Where(p => p.Addition == addition).FirstOrDefault();
                var policyInContext = context.ElementaryPolicies.FirstOrDefault(p => p.ElementaryPolicyID == policyId);

                if (policyInContext != null)
                {
                    try
                    {
                        int? structureID = null;
                        if (structureType != null)
                        {
                            structureID = structureType.StructureTypeID;
                        }
                        ApartmentPolicy newAptPolicy = new ApartmentPolicy { ElementaryPolicyID = policyInContext.ElementaryPolicyID, City = city, Street = street, HomeNumber = homeNumber, ApartmentNumber = apartmentNumber, ZipCode = zipCode, Floor = floor, TotalBuildingFloorsNumber = totalFloors, SizeInMeters = size, RoomsNumber = roomsNumber, StructureTypeID = structureID, ProtectionRequired = protectionRequired, ContactName = contactName, ContactPhone = contactPhone, ContactCellPhone = contactCellPhone, ContactEmail = contactEmail, ContactFax = contactFax, IsSurveyDone = isSurveyDone, SurveyDate = surveyDate, SurveyByAppraiserID = surveyByAppraisserID, InsuranceThisYear = insuranceThisYear, InsuranceLastYear = insuranceLastYear, Insurance2YearsAgo = insurance2YearsAgo, Insurance3YearsAgo = insurance3YearsAgo, ClaimsNumberThisYear = claimsNumberThisYear, ClaimsNumberLastYear = claimsNumberLastYear, ClaimsNumber2YearsAgo = claimsNumber2YearsAgo, ClaimsNumber3YearsAgo = claimsNumber3YearsAgo, InsuranceCompanyThisYear = insuranceCompanyThisYear, InsuranceCompanyLastYear = InsuranceCompanyLastYear, InsuranceCompany2YearsAgo = InsuranceCompany2YearsAgo, InsuranceCompany3YearsAgo = insuranceCompany3YearsAgo, Comments = comments };
                        context.ApartmentPolicies.Add(newAptPolicy);
                        context.SaveChanges();
                    }
                    catch (Exception e)
                    {
                        throw new Exception(e.Message);
                    }
                }
                else
                {
                    throw new Exception("פוליסה לא קיימת במערכת");
                }
            }
        }

        //מעדכן פוליסת דירה
        public void UpdateApartmentPolicy(int policyId,string policyNumber, int? addition, string city, string street, string homeNumber, string apartmentNumber, string zipCode, string floor, string totalFloors, decimal? size, string roomsNumber, StructureType structureType, string protectionRequired, string contactName, string contactPhone, string contactCellPhone, string contactFax, string contactEmail, bool? isSurveyDone, DateTime? surveyDate, int? surveyByAppraisserID, bool? insuranceThisYear, bool? insuranceLastYear, bool? insurance2YearsAgo, bool? insurance3YearsAgo, int? claimsNumberThisYear, int? claimsNumberLastYear, int? claimsNumber2YearsAgo, int? claimsNumber3YearsAgo, string insuranceCompanyThisYear, string InsuranceCompanyLastYear, string InsuranceCompany2YearsAgo, string insuranceCompany3YearsAgo, string comments)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var aptPolicyInContext = context.ApartmentPolicies.FirstOrDefault(p => p.ElementaryPolicyID == policyId);

                //var aptPolicyInContext = context.ApartmentPolicies.FirstOrDefault(p => p.ElementaryPolicy.PolicyNumber == policyNumber);
                if (aptPolicyInContext != null)
                {
                    try
                    {
                        int? structureID = null;
                        if (structureType != null)
                        {
                            structureID = structureType.StructureTypeID;
                        }
                        aptPolicyInContext.City = city;
                        aptPolicyInContext.Street = street;
                        aptPolicyInContext.HomeNumber = homeNumber;
                        aptPolicyInContext.ApartmentNumber = apartmentNumber;
                        aptPolicyInContext.ZipCode = zipCode;
                        aptPolicyInContext.Floor = floor;
                        aptPolicyInContext.TotalBuildingFloorsNumber = totalFloors;
                        aptPolicyInContext.SizeInMeters = size;
                        aptPolicyInContext.RoomsNumber = roomsNumber;
                        aptPolicyInContext.StructureTypeID = structureID;
                        aptPolicyInContext.ProtectionRequired = protectionRequired;
                        aptPolicyInContext.ContactName = contactName;
                        aptPolicyInContext.ContactPhone = contactPhone;
                        aptPolicyInContext.ContactCellPhone = contactCellPhone;
                        aptPolicyInContext.ContactEmail = contactEmail;
                        aptPolicyInContext.ContactFax = contactFax;
                        aptPolicyInContext.IsSurveyDone = isSurveyDone;
                        aptPolicyInContext.SurveyDate = surveyDate;
                        aptPolicyInContext.SurveyByAppraiserID = surveyByAppraisserID;
                        aptPolicyInContext.InsuranceThisYear = insuranceThisYear;
                        aptPolicyInContext.InsuranceLastYear = insuranceLastYear;
                        aptPolicyInContext.Insurance2YearsAgo = insurance2YearsAgo;
                        aptPolicyInContext.Insurance3YearsAgo = insurance3YearsAgo;
                        aptPolicyInContext.ClaimsNumberThisYear = claimsNumberThisYear;
                        aptPolicyInContext.ClaimsNumberLastYear = claimsNumberLastYear;
                        aptPolicyInContext.ClaimsNumber2YearsAgo = claimsNumber2YearsAgo;
                        aptPolicyInContext.ClaimsNumber3YearsAgo = claimsNumber3YearsAgo;
                        aptPolicyInContext.InsuranceCompanyThisYear = insuranceCompanyThisYear;
                        aptPolicyInContext.InsuranceCompanyLastYear = InsuranceCompanyLastYear;
                        aptPolicyInContext.InsuranceCompany2YearsAgo = InsuranceCompany2YearsAgo;
                        aptPolicyInContext.InsuranceCompany3YearsAgo = insuranceCompany3YearsAgo;
                        aptPolicyInContext.Comments = comments;
                        context.SaveChanges();
                    }
                    catch (Exception e)
                    {
                        throw new Exception(e.Message);
                    }
                }
                else
                {
                    InsertApartmentPolicy(policyId,policyNumber, addition, city, street, homeNumber, apartmentNumber, zipCode, floor, totalFloors, size, roomsNumber, structureType, protectionRequired, contactName, contactPhone, contactCellPhone, contactFax, contactEmail, isSurveyDone, surveyDate, surveyByAppraisserID, insuranceThisYear, insuranceLastYear, insurance2YearsAgo, insurance3YearsAgo, claimsNumberThisYear, claimsNumberLastYear, claimsNumber2YearsAgo, claimsNumber3YearsAgo, insuranceCompanyThisYear, InsuranceCompanyLastYear, InsuranceCompany2YearsAgo, insuranceCompany3YearsAgo, comments);
                    //throw new Exception("פוליסה לא קיימת במערכת");
                }
            }
        }

        //מוסיף פוליסת עסק
        public void InsertBusinessPolicy(int policyId,string policyNumber, int? addition, string businessOwner, string businessDescription, string businessName, string city, string street, string homeNumber, string apartmentNumber, string zipCode, string businessPhone, string businessPhone2, string businessFax, string businessEmail, StructureType structureType, string floor, string totalFloors, decimal? size, string roomsNumber, string contactLastName, string contactName, string contactPhone, string contactCellPhone, string contactFax, string contactEmail, bool? isSurveyDone, DateTime? surveyDate, int? surveyByAppraisserID, bool? insuranceThisYear, bool? insuranceLastYear, bool? insurance2YearsAgo, bool? insurance3YearsAgo, int? claimsNumberThisYear, int? claimsNumberLastYear, int? claimsNumber2YearsAgo, int? claimsNumber3YearsAgo, string insuranceCompanyThisYear, string InsuranceCompanyLastYear, string InsuranceCompany2YearsAgo, string insuranceCompany3YearsAgo)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                //var policyInContext = context.ElementaryPolicies.Where(p => p.PolicyNumber == policyNumber).Where(p => p.Addition == addition).FirstOrDefault();
                var policyInContext = context.ElementaryPolicies.FirstOrDefault(p => p.ElementaryPolicyID == policyId);

                if (policyInContext != null)
                {
                    try
                    {
                        int? structureID = null;
                        if (structureType != null)
                        {
                            structureID = structureType.StructureTypeID;
                        }
                        BusinessPolicy newBusinessPolicy = new BusinessPolicy() { ElementaryPolicyID = policyInContext.ElementaryPolicyID, BusinessOwner = businessOwner, BusinessDescription = businessDescription, BusinessName = businessName, City = city, Street = street, HomeNumber = homeNumber, ApartmentNumber = apartmentNumber, ZipCode = zipCode, BusinessPhone = businessPhone, BusinessPhone2 = businessPhone2, BusinessFax = businessFax, BusinessEmail = businessEmail, Floor = floor, TotalBuildingFloorsNumber = totalFloors, SizeInMeters = size, RoomsNumber = roomsNumber, StructureTypeID = structureID, ContactLastName = contactLastName, ContactName = contactName, ContactPhone = contactPhone, ContactCellPhone = contactCellPhone, ContactEmail = contactEmail, ContactFax = contactFax, IsSurveyDone = isSurveyDone, SurveyDate = surveyDate, SurveyByAppraiserID = surveyByAppraisserID, InsuranceThisYear = insuranceThisYear, InsuranceLastYear = insuranceLastYear, Insurance2YearsAgo = insurance2YearsAgo, Insurance3YearsAgo = insurance3YearsAgo, ClaimsNumberThisYear = claimsNumberThisYear, ClaimsNumberLastYear = claimsNumberLastYear, ClaimsNumber2YearsAgo = claimsNumber2YearsAgo, ClaimsNumber3YearsAgo = claimsNumber3YearsAgo, InsuranceCompanyThisYear = insuranceCompanyThisYear, InsuranceCompanyLastYear = InsuranceCompanyLastYear, InsuranceCompany2YearsAgo = InsuranceCompany2YearsAgo, InsuranceCompany3YearsAgo = insuranceCompany3YearsAgo };
                        context.BusinessPolicies.Add(newBusinessPolicy);
                        context.SaveChanges();
                    }
                    catch (Exception e)
                    {
                        throw new Exception(e.Message);
                    }
                }
                else
                {
                    throw new Exception("פוליסה לא קיימת במערכת");
                }
            }
        }

        //מעדכן פוליסת עסק
        public void UpdateBusinessPolicy(int policyId,string policyNumber, int? addition, string businessOwner, string businessDescription, string businessName, string city, string street, string homeNumber, string apartmentNumber, string zipCode, string businessPhone, string businessPhone2, string businessFax, string businessEmail, StructureType structureType, string floor, string totalFloors, decimal? size, string roomsNumber, string contactLastName, string contactName, string contactPhone, string contactCellPhone, string contactFax, string contactEmail, bool? isSurveyDone, DateTime? surveyDate, int? surveyByAppraisserID, bool? insuranceThisYear, bool? insuranceLastYear, bool? insurance2YearsAgo, bool? insurance3YearsAgo, int? claimsNumberThisYear, int? claimsNumberLastYear, int? claimsNumber2YearsAgo, int? claimsNumber3YearsAgo, string insuranceCompanyThisYear, string InsuranceCompanyLastYear, string InsuranceCompany2YearsAgo, string insuranceCompany3YearsAgo)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                //var policyInContext = context.BusinessPolicies.Where(p => p.ElementaryPolicy.PolicyNumber == policyNumber).Where(p => p.ElementaryPolicy.Addition == addition).FirstOrDefault();
                var policyInContext = context.BusinessPolicies.FirstOrDefault(p => p.ElementaryPolicyID == policyId);

                if (policyInContext != null)
                {
                    try
                    {
                        int? structureID = null;
                        if (structureType != null)
                        {
                            structureID = structureType.StructureTypeID;
                        }
                        policyInContext.BusinessOwner = businessOwner;
                        policyInContext.BusinessDescription = businessDescription;
                        policyInContext.BusinessName = businessName;
                        policyInContext.City = city;
                        policyInContext.Street = street;
                        policyInContext.HomeNumber = homeNumber;
                        policyInContext.ApartmentNumber = apartmentNumber;
                        policyInContext.ZipCode = zipCode;
                        policyInContext.BusinessPhone = businessPhone;
                        policyInContext.BusinessPhone2 = businessPhone2;
                        policyInContext.BusinessFax = businessFax;
                        policyInContext.BusinessEmail = businessEmail;
                        policyInContext.Floor = floor;
                        policyInContext.TotalBuildingFloorsNumber = totalFloors;
                        policyInContext.SizeInMeters = size;
                        policyInContext.RoomsNumber = roomsNumber;
                        policyInContext.StructureTypeID = structureID;
                        policyInContext.ContactLastName = contactLastName;
                        policyInContext.ContactName = contactName;
                        policyInContext.ContactPhone = contactPhone;
                        policyInContext.ContactCellPhone = contactCellPhone;
                        policyInContext.ContactEmail = contactEmail;
                        policyInContext.ContactFax = contactFax;
                        policyInContext.IsSurveyDone = isSurveyDone;
                        policyInContext.SurveyDate = surveyDate;
                        policyInContext.SurveyByAppraiserID = surveyByAppraisserID;
                        policyInContext.InsuranceThisYear = insuranceThisYear;
                        policyInContext.InsuranceLastYear = insuranceLastYear;
                        policyInContext.Insurance2YearsAgo = insurance2YearsAgo;
                        policyInContext.Insurance3YearsAgo = insurance3YearsAgo;
                        policyInContext.ClaimsNumberThisYear = claimsNumberThisYear;
                        policyInContext.ClaimsNumberLastYear = claimsNumberLastYear;
                        policyInContext.ClaimsNumber2YearsAgo = claimsNumber2YearsAgo;
                        policyInContext.ClaimsNumber3YearsAgo = claimsNumber3YearsAgo;
                        policyInContext.InsuranceCompanyThisYear = insuranceCompanyThisYear;
                        policyInContext.InsuranceCompanyLastYear = InsuranceCompanyLastYear;
                        policyInContext.InsuranceCompany2YearsAgo = InsuranceCompany2YearsAgo;
                        policyInContext.InsuranceCompany3YearsAgo = insuranceCompany3YearsAgo;
                        context.SaveChanges();
                    }
                    catch (Exception e)
                    {
                        throw new Exception(e.Message);
                    }
                }
                else
                {
                    InsertBusinessPolicy(policyId,policyNumber, addition, businessOwner, businessDescription, businessName, city, street, homeNumber, apartmentNumber, zipCode, businessPhone, businessPhone2, businessFax, businessEmail, structureType, floor, totalFloors, size, roomsNumber, contactLastName, contactName, contactPhone, contactCellPhone, contactFax, contactEmail, isSurveyDone, surveyDate, surveyByAppraisserID, insuranceThisYear, insuranceLastYear, insurance2YearsAgo, insurance3YearsAgo, claimsNumberThisYear, claimsNumberLastYear, claimsNumber2YearsAgo, claimsNumber3YearsAgo, insuranceCompanyThisYear, InsuranceCompanyLastYear, InsuranceCompany2YearsAgo, insuranceCompany3YearsAgo);
                    //throw new Exception("פוליסה לא קיימת במערכת");
                }
            }
        }

        //מוסיף פוליסת רכב 
        public void InsertCarPolicy(int policyId,string policyNumber, int? addition, InsuranceIndustry industry, string registrationNumber, string chovaCertificateNumber, VehicleType vehicleType, string manufacturer, string modelCode, int? year, string engine, decimal? weight, int? placesNumber, string protectionCode, string chassisNumber, bool? insuranceThisYear, bool? insuranceLastYear, bool? insurance2YearsAgo, bool? insurance3YearsAgo, int? claimsNumberThisYear, int? claimsNumberLastYear, int? claimsNumber2YearsAgo, int? claimsNumber3YearsAgo, string insuranceCompanyThisYear, string InsuranceCompanyLastYear, string InsuranceCompany2YearsAgo, string insuranceCompany3YearsAgo, int? youngestDriverAge, int? youngestDriverSeniority, int? otherDriversAge, int? otherDriversSeniority, AllowedToDrive allowedToDrive, bool? drivesInShabbat)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                //var policyInContext = context.ElementaryPolicies.FirstOrDefault(p => p.PolicyNumber == policyNumber);
                //var policyInContext = context.ElementaryPolicies.Where(p => p.PolicyNumber == policyNumber).Where(p => p.Addition == addition).Where(p => p.InsuranceIndustryID == industry.InsuranceIndustryID).FirstOrDefault();
                var policyInContext = context.ElementaryPolicies.FirstOrDefault(p => p.ElementaryPolicyID == policyId);

                if (policyInContext != null)
                {
                    try
                    {
                        int? vehicleTypeID = null;
                        if (vehicleType != null)
                        {
                            vehicleTypeID = vehicleType.VehicleTypeID;
                        }
                        int? allowedToDriveID = null;
                        if (allowedToDrive != null)
                        {
                            allowedToDriveID = allowedToDrive.AllowedToDriveID;
                        }
                        CarPolicy newCarPolicy = new CarPolicy() { ElementaryPolicyID = policyInContext.ElementaryPolicyID, RegistrationNumber = registrationNumber, ChovaCertificateNumber = chovaCertificateNumber, VehicleTypeID = vehicleTypeID, Manufacturer = manufacturer, ModelCode = modelCode, Year = year, Engine = engine, Weight = weight, PlacesNumber = placesNumber, ProtectionCode = protectionCode, ChassisNumber = chassisNumber, InsuranceThisYear = insuranceThisYear, InsuranceLastYear = insuranceLastYear, Insurance2YearsAgo = insurance2YearsAgo, Insurance3YearsAgo = insurance3YearsAgo, ClaimsNumberThisYear = claimsNumberThisYear, ClaimsNumberLastYear = claimsNumberLastYear, ClaimsNumber2YearsAgo = claimsNumber2YearsAgo, ClaimsNumber3YearsAgo = claimsNumber3YearsAgo, InsuranceCompanyThisYear = insuranceCompanyThisYear, InsuranceCompanyLastYear = InsuranceCompanyLastYear, InsuranceCompany2YearsAgo = InsuranceCompany2YearsAgo, InsuranceCompany3YearsAgo = insuranceCompany3YearsAgo, YoungestDriverAge = youngestDriverAge, YoungestDriverSeniority = youngestDriverSeniority, OtherDriversAge = otherDriversAge, OtherDriversSeniority = otherDriversSeniority, AllowedToDriveID = allowedToDriveID, DrivesInShabbat = drivesInShabbat };
                        context.CarPolicies.Add(newCarPolicy);
                        context.SaveChanges();
                    }
                    catch (Exception e)
                    {
                        throw new Exception(e.Message);
                    }
                }
                else
                {
                    throw new Exception("פוליסה לא קיימת במערכת");
                }
            }
        }

        //מעדכן פוליסת רכב 
        public void UpdateCarPolicy(int policyId,string PolicyNumber,InsuranceIndustry industry, int? addition, string registrationNumber, string chovaCertificateNumber, VehicleType vehicleType, string manufacturer, string modelCode, int? year, string engine, decimal? weight, int? placesNumber, string protectionCode, string chassisNumber, bool? insuranceThisYear, bool? insuranceLastYear, bool? insurance2YearsAgo, bool? insurance3YearsAgo, int? claimsNumberThisYear, int? claimsNumberLastYear, int? claimsNumber2YearsAgo, int? claimsNumber3YearsAgo, string insuranceCompanyThisYear, string InsuranceCompanyLastYear, string InsuranceCompany2YearsAgo, string insuranceCompany3YearsAgo, int? youngestDriverAge, int? youngestDriverSeniority, int? otherDriversAge, int? otherDriversSeniority, AllowedToDrive allowedToDrive, bool? drivesInShabbat)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                //var policyInContext = context.CarPolicies.Where(p => p.ElementaryPolicy.PolicyNumber == PolicyNumber).Where(p => p.ElementaryPolicy.Addition == addition).FirstOrDefault();
                var policyInContext = context.CarPolicies.FirstOrDefault(p => p.ElementaryPolicyID == policyId);

                if (policyInContext != null)
                {
                    try
                    {
                        int? vehicleTypeID = null;
                        if (vehicleType != null)
                        {
                            vehicleTypeID = vehicleType.VehicleTypeID;
                        }
                        int? allowedToDriveID = null;
                        if (allowedToDrive != null)
                        {
                            allowedToDriveID = allowedToDrive.AllowedToDriveID;
                        }
                        policyInContext.RegistrationNumber = registrationNumber;
                        policyInContext.ChovaCertificateNumber = chovaCertificateNumber;
                        policyInContext.VehicleTypeID = vehicleTypeID;
                        policyInContext.Manufacturer = manufacturer;
                        policyInContext.ModelCode = modelCode;
                        policyInContext.Year = year;
                        policyInContext.Engine = engine;
                        policyInContext.Weight = weight;
                        policyInContext.PlacesNumber = placesNumber;
                        policyInContext.ProtectionCode = protectionCode;
                        policyInContext.ChassisNumber = chassisNumber;
                        policyInContext.InsuranceThisYear = insuranceThisYear;
                        policyInContext.InsuranceLastYear = insuranceLastYear;
                        policyInContext.Insurance2YearsAgo = insurance2YearsAgo;
                        policyInContext.Insurance3YearsAgo = insurance3YearsAgo;
                        policyInContext.ClaimsNumberThisYear = claimsNumberThisYear;
                        policyInContext.ClaimsNumberLastYear = claimsNumberLastYear;
                        policyInContext.ClaimsNumber2YearsAgo = claimsNumber2YearsAgo;
                        policyInContext.ClaimsNumber3YearsAgo = claimsNumber3YearsAgo;
                        policyInContext.InsuranceCompanyThisYear = insuranceCompanyThisYear;
                        policyInContext.InsuranceCompanyLastYear = InsuranceCompanyLastYear;
                        policyInContext.InsuranceCompany2YearsAgo = InsuranceCompany2YearsAgo;
                        policyInContext.InsuranceCompany3YearsAgo = insuranceCompany3YearsAgo;
                        policyInContext.YoungestDriverAge = youngestDriverAge;
                        policyInContext.YoungestDriverSeniority = youngestDriverSeniority;
                        policyInContext.OtherDriversAge = otherDriversAge;
                        policyInContext.OtherDriversSeniority = otherDriversSeniority;
                        policyInContext.AllowedToDriveID = allowedToDriveID;
                        policyInContext.DrivesInShabbat = drivesInShabbat;
                        context.SaveChanges();
                    }
                    catch (Exception e)
                    {
                        throw new Exception(e.Message);
                    }
                }
                else
                {
                    InsertCarPolicy(policyId,PolicyNumber, addition,industry, registrationNumber, chovaCertificateNumber, vehicleType, manufacturer, modelCode, year, engine, weight, placesNumber, protectionCode, chassisNumber, insuranceThisYear, insuranceLastYear, insurance2YearsAgo, insurance3YearsAgo, claimsNumberThisYear, claimsNumberLastYear, claimsNumber2YearsAgo, claimsNumber3YearsAgo, insuranceCompanyThisYear, InsuranceCompanyLastYear, InsuranceCompany2YearsAgo, insuranceCompany3YearsAgo, youngestDriverAge, youngestDriverSeniority, otherDriversAge, otherDriversSeniority, allowedToDrive, drivesInShabbat);
                    //throw new Exception("פוליסה לא קיימת במערכת");
                }
            }
        }

        public ElementaryPolicy GetElementaryPolicyByPolicyId(int policyId)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.ElementaryPolicies.Include("InsuranceIndustry").Include("ApartmentPolicy").Include("CarPolicy").Include("BusinessPolicy").Include("Client").FirstOrDefault(p => p.ElementaryPolicyID == policyId);
            }
        }

        public string GetAdditionNumber(string policyNumber, InsuranceIndustry industry)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var policiesInContext = context.ElementaryPolicies.Where(p => p.PolicyNumber == policyNumber && p.InsuranceIndustryID == industry.InsuranceIndustryID);
                int? addition = 0;
                foreach (var item in policiesInContext)
                {
                    if (item.Addition > addition)
                    {
                        addition = item.Addition;
                    }
                }
                return (addition + 1).ToString();
            }
        }

        //public List<ElementaryPolicy> FindElementaryPolicyByString(Client client,string key, string policyStatus)
        //{
        //    using (Coral_DB_Entities context = new Coral_DB_Entities())
        //    {
        //        if (policyStatus=="בתוקף")
        //        {
        //            return (from p in context.ElementaryPolicies
        //                    where p.ClientID==client.ClientID
        //                    where p.EndDate>=DateTime.Now
        //                    where p.PolicyNumber.StartsWith(key) || p.CarPolicy.RegistrationNumber.StartsWith(key)
        //                    select p).OrderByDescending(p => p.StartDate).ToList();
        //            //return context.ElementaryPolicies.Where(p=>p.EndDate).Where()
        //            //return (from c in context.Clients
        //            //        where c.ClientType.ClientTypeName == typeName
        //            //        where c.CategoryID == category.CategoryID
        //            //        where c.LastName.StartsWith(key) || c.FirstName.StartsWith(key) || c.IdNumber.StartsWith(key)
        //            //        select c).OrderBy(c => c.LastName).ToList();
        //        }
        //        else if (policyStatus == "לא בתוקף")
        //        {
        //            return (from p in context.ElementaryPolicies
        //                    where p.ClientID == client.ClientID
        //                    where p.EndDate < DateTime.Now
        //                    where p.PolicyNumber.StartsWith(key) || p.CarPolicy.RegistrationNumber.StartsWith(key)
        //                    select p).OrderByDescending(p => p.StartDate).ToList();
        //        }
        //        else 
        //        {
        //            return (from p in context.ElementaryPolicies
        //                    where p.ClientID == client.ClientID
        //                    where p.PolicyNumber.StartsWith(key) || p.CarPolicy.RegistrationNumber.StartsWith(key)
        //                    select p).OrderByDescending(p => p.StartDate).ToList();
        //        }
        //    }
        //}


        public void ChangeElementaryPolicyOfferStatus(int policyId,string policyNumber, int industryId, bool isOffer)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var policyInContext = context.ElementaryPolicies.FirstOrDefault(p => p.ElementaryPolicyID == policyId);
                policyInContext.IsOffer = isOffer;
                context.SaveChanges();
            }
        }

        public List<ElementaryPolicy> GetPoliciesByEndDate(DateTime endDate)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.ElementaryPolicies.Include("Client").Include("RenewalStatus").Include("CarPolicy").Include("BusinessPolicy").Include("ApartmentPolicy").Include("Company").Include("ElementaryInsuranceType").Include("InsuranceIndustry").Where(p => ((DateTime)p.EndDate).Month == endDate.Month && ((DateTime)p.EndDate).Year == endDate.Year && p.IsOffer == false).ToList();
            }
        }

        public List<ElementaryPolicy> GetPoliciesByRegistrationNumberAndEndDate(string registrationNumber, DateTime endDate)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.ElementaryPolicies.Include("Client").Include("CarPolicy").Include("Company").Include("ElementaryInsuranceType").Include("InsuranceIndustry").Include("RenewalStatus").Where(p => p.CarPolicy.RegistrationNumber == registrationNumber && ((DateTime)p.EndDate).Month == endDate.Month && ((DateTime)p.EndDate).Year == endDate.Year).ToList();
            }
        }

        public List<ElementaryPolicy> GetPoliciesByPolicyNumberAndIndustry(string policyNumber, int industryID)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.ElementaryPolicies.Include("Client").Include("CarPolicy").Include("Company").Include("ElementaryInsuranceType").Include("InsuranceIndustry").Include("RenewalStatus").Where(p => p.PolicyNumber == policyNumber && p.InsuranceIndustryID==industryID).ToList();
            }
        }

        public int UpdatePolicyRenewalStatus(ElementaryPolicy policy, string newStatus)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                //var policyInContext = context.ElementaryPolicies.Where(p => p.PolicyNumber == policy.PolicyNumber && p.InsuranceIndustryID == policy.InsuranceIndustryID).FirstOrDefault();
                var policyInContext = context.ElementaryPolicies.FirstOrDefault(p => p.ElementaryPolicyID==policy.ElementaryPolicyID);

                if (policyInContext != null)
                {
                    try
                    {
                        var renewalStatusInContext = context.RenewalStatuses.FirstOrDefault(s => s.RenewalStatusName == newStatus);
                        if (renewalStatusInContext != null)
                        {
                            policyInContext.RenewalStatusID = renewalStatusInContext.RenewalStatusID;
                            //policyInContext.RenewalStatus.RenewalStatusName = renewalStatusInContext.RenewalStatusName;
                            context.SaveChanges();
                            return renewalStatusInContext.RenewalStatusID;
                        }
                        else
                        {
                            throw new Exception("לא ניתן לעדכן את סטטוס החידוש");
                        }
                    }

                    catch (Exception e)
                    {
                        throw new Exception(e.Message);
                    }
                }
                else
                {
                    throw new Exception("פוליסה לא קיימת במערכת");
                }
            }
        }

        public List<ElementaryPolicy> GetPoliciesToRenewByFilter(InsuranceCompany company, Category category, DateTime endDate, InsuranceIndustry industry, string policyStatus, RenewalStatus status)
        {
            //var filteredPolicies = GetPoliciesByEndDate(endDate);
            var policiesFilteredByStatus = GetPoliciesByEndDate(endDate);
            if (status != null)
            {
                policiesFilteredByStatus = policiesFilteredByStatus.Where(p => p.RenewalStatusID == status.RenewalStatusID).ToList();
            }
            //if (clientType != null)
            //{
            //    policiesFilteredByStatus = policiesFilteredByStatus.Where(p => p.Client.ClientTypeID == clientType.ClientTypeID).ToList();
            //}
            if (industry != null)
            {
                policiesFilteredByStatus = policiesFilteredByStatus.Where(p => p.InsuranceIndustryID == industry.InsuranceIndustryID).ToList();
            }
            if (category != null)
            {
                policiesFilteredByStatus = policiesFilteredByStatus.Where(p => p.Client.CategoryID == category.CategoryID).ToList();
            }
            if (company != null)
            {
                policiesFilteredByStatus = policiesFilteredByStatus.Where(p => p.CompanyID == company.CompanyID).ToList();
            }
            if (policyStatus == "בתוקף")
            {
                policiesFilteredByStatus = policiesFilteredByStatus.Where(p => p.EndDate >= DateTime.Now && p.IsOffer == false).ToList();
            }
            else if (policyStatus == "לא בתוקף")
            {
                policiesFilteredByStatus = policiesFilteredByStatus.Where(p => p.EndDate <= DateTime.Now && p.IsOffer == false).ToList();
            }
            else if (policyStatus == "הצעה")
            {
                policiesFilteredByStatus = policiesFilteredByStatus.Where(p => p.IsOffer == true).ToList();
            }
            return policiesFilteredByStatus;
        }

        public List<CarPolicy> GetAllCarPoliciesByClient(Client client)
        {
            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                return context.CarPolicies.Include("ElementaryPolicy").Where(c => c.ElementaryPolicy.ClientID == client.ClientID).ToList();
            }
        }
    }
}
