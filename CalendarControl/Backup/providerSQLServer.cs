using System;
using XtremeCalendarControl;

namespace CalendarControl
{	
	/// <summary>
	/// Summary description for providerSQLServer.
	/// </summary>
	public class providerSQLServer
	{		
		private AxXtremeCalendarControl.AxCalendarControl m_axCalendar;
		private SQLDataHelper m_DataHelper;

		private System.Data.OleDb.OleDbConnection	m_oleDbConnection;

		public providerSQLServer(AxXtremeCalendarControl.AxCalendarControl axCalendar)
		{			
			m_axCalendar = axCalendar;
			m_DataHelper = new SQLDataHelper(axCalendar);
			
			m_oleDbConnection = new System.Data.OleDb.OleDbConnection();
						
			m_axCalendar.DoRetrieveDayEvents += new AxXtremeCalendarControl._DCalendarControlEvents_DoRetrieveDayEventsEventHandler(m_axCalendar_DoRetrieveDayEvents);
			
			m_axCalendar.DoCreateEvent += new AxXtremeCalendarControl._DCalendarControlEvents_DoCreateEventEventHandler(m_axCalendar_DoCreateEvent);
			m_axCalendar.DoCreateRPattern += new AxXtremeCalendarControl._DCalendarControlEvents_DoCreateRPatternEventHandler(m_axCalendar_DoCreateRPattern);
			
			m_axCalendar.DoDeleteEvent += new AxXtremeCalendarControl._DCalendarControlEvents_DoDeleteEventEventHandler(m_axCalendar_DoDeleteEvent);
			m_axCalendar.DoDeleteRPattern += new AxXtremeCalendarControl._DCalendarControlEvents_DoDeleteRPatternEventHandler(m_axCalendar_DoDeleteRPattern);

			m_axCalendar.DoReadEvent += new AxXtremeCalendarControl._DCalendarControlEvents_DoReadEventEventHandler(m_axCalendar_DoReadEvent);
			m_axCalendar.DoReadRPattern += new AxXtremeCalendarControl._DCalendarControlEvents_DoReadRPatternEventHandler(m_axCalendar_DoReadRPattern);

			m_axCalendar.DoUpdateEvent += new AxXtremeCalendarControl._DCalendarControlEvents_DoUpdateEventEventHandler(m_axCalendar_DoUpdateEvent);
			m_axCalendar.DoUpdateRPattern += new AxXtremeCalendarControl._DCalendarControlEvents_DoUpdateRPatternEventHandler(m_axCalendar_DoUpdateRPattern);

			m_axCalendar.DoGetUpcomingEvents += new AxXtremeCalendarControl._DCalendarControlEvents_DoGetUpcomingEventsEventHandler(m_axCalendar_DoGetUpcomingEvents);
		}

		public Boolean Open(String strConnectionString)
		{			
			if(m_oleDbConnection == null) 
			{
				return false;
			}
			Close();

			m_oleDbConnection.ConnectionString = strConnectionString;
			m_oleDbConnection.Open();
		
			return IsOpen();
		}

		public Boolean IsOpen()
		{	
			return  m_oleDbConnection != null &&
				m_oleDbConnection.State == System.Data.ConnectionState.Open;			
		}

		public void Close()
		{	
			if (m_oleDbConnection != null && IsOpen()) 
			{
				m_oleDbConnection.Close();
			}
		}

		public String GetConnectionString()
		{	
			return  m_oleDbConnection.ConnectionString;
		}

		//==================================================================
		private void m_axCalendar_DoRetrieveDayEvents(object sender, AxXtremeCalendarControl._DCalendarControlEvents_DoRetrieveDayEventsEvent e)
		{
			if(!IsOpen()) 
			{
				return;
			}

			System.Data.OleDb.OleDbDataReader oleReader = null;

			try
			{	
				System.Data.OleDb.OleDbCommand oleDbCommand = m_oleDbConnection.CreateCommand();
				oleDbCommand.CommandText = m_DataHelper.MakeRetrieveDayEventsSQL(e.dtDay);

				oleReader = oleDbCommand.ExecuteReader();

				while(oleReader.Read())
				{					
					CalendarEvent axEvent = m_DataHelper.CreateEventFromData(oleReader);
					if (axEvent != null) 
					{
						e.events.Add(axEvent);
					}
				}
			}
			catch(System.Exception ex)
			{
				System.Diagnostics.Debug.WriteLine("EXCEPTION! providerSQLServer.m_axCalendar_DoRetrieveDayEvents: " + ex.Message);
			}

			//================================================
			if(oleReader != null && !oleReader.IsClosed) 
			{
				oleReader.Close();
			}
		}
		
		private void m_axCalendar_DoCreateEvent(object sender, AxXtremeCalendarControl._DCalendarControlEvents_DoCreateEventEvent e)
		{
			e.bResult = false;
    
			if(!IsOpen()) 
			{
				return;
			}

			try
			{
				String strSQL = "SELECT * FROM CalendarEvents WHERE EventID = 0";

				System.Data.OleDb.OleDbDataAdapter oleDbDataAdapter = new System.Data.OleDb.OleDbDataAdapter(strSQL, m_oleDbConnection);				
				System.Data.OleDb.OleDbCommandBuilder custCB = new System.Data.OleDb.OleDbCommandBuilder(oleDbDataAdapter);
				custCB.QuotePrefix = "[";
				custCB.QuoteSuffix = "]";

				System.Data.DataSet objEventDSet = new System.Data.DataSet();
				oleDbDataAdapter.Fill(objEventDSet);

				//code to modify data in dataset here
				System.Data.DataRow objDRow = objEventDSet.Tables[0].NewRow();
				objEventDSet.Tables[0].Rows.Add(objDRow);

				m_DataHelper.PutEventToData(e.pEvent, objDRow);

				oleDbDataAdapter.Update(objEventDSet);
				
				//'-- update Auto EventID
				System.Data.OleDb.OleDbCommand objCommand = m_oleDbConnection.CreateCommand();
				objCommand.CommandText = "SELECT MAX(EventID) AS NewEventID FROM CalendarEvents";
				System.Data.OleDb.OleDbDataReader objReader = objCommand.ExecuteReader();
				objReader.Read();
				 
				e.newEventID = objReader.GetInt32(0);
            
				objReader.Close();
    
				e.bResult = true;
			}
			catch(System.Exception ex)
			{
				System.Diagnostics.Debug.WriteLine("EXCEPTION! providerSQLServer.m_axCalendar_DoCreateEvent: " + ex.Message);
			}
		}

		private void m_axCalendar_DoCreateRPattern(object sender, AxXtremeCalendarControl._DCalendarControlEvents_DoCreateRPatternEvent e)
		{
			e.bResult = false;
    
			if(!IsOpen()) 
			{
				return;
			}

			try
			{
				String strSQL = "SELECT * FROM CalendarRecurrencePatterns WHERE RecurrencePatternID = 0 ";

				System.Data.OleDb.OleDbDataAdapter oleDbDataAdapter = new System.Data.OleDb.OleDbDataAdapter(strSQL, m_oleDbConnection);
				System.Data.OleDb.OleDbCommandBuilder custCB = new System.Data.OleDb.OleDbCommandBuilder(oleDbDataAdapter);
				custCB.QuotePrefix = "[";
				custCB.QuoteSuffix = "]";

				System.Data.DataSet objPatternDSet = new System.Data.DataSet();
				oleDbDataAdapter.Fill(objPatternDSet);

				//code to modify data in dataset here
				System.Data.DataRow objDRow = objPatternDSet.Tables[0].NewRow();
				objPatternDSet.Tables[0].Rows.Add(objDRow);

				m_DataHelper.PutRPatternToData(e.pPattern, objDRow);

				oleDbDataAdapter.Update(objPatternDSet);
				
				//'-- update Auto EventID
				System.Data.OleDb.OleDbCommand objCommand = m_oleDbConnection.CreateCommand();
				objCommand.CommandText = "SELECT MAX(RecurrencePatternID) AS NewNewPatternID FROM CalendarRecurrencePatterns";
				System.Data.OleDb.OleDbDataReader objReader = objCommand.ExecuteReader();
				objReader.Read();
				 
				e.newPatternID = objReader.GetInt32(0);
            
				objReader.Close();
    
				e.bResult = true;
			}
			catch(System.Exception ex)
			{
				System.Diagnostics.Debug.WriteLine("EXCEPTION! providerSQLServer.m_axCalendar_DoCreateRPattern: " + ex.Message);
			}
		}

		private void m_axCalendar_DoDeleteEvent(object sender, AxXtremeCalendarControl._DCalendarControlEvents_DoDeleteEventEvent e)
		{
			e.bResult = false;
    
			if(!IsOpen()) 
			{
				return;
			}

			try
			{
				System.Data.OleDb.OleDbCommand objCommand = m_oleDbConnection.CreateCommand();
				objCommand.CommandText = "DELETE FROM CalendarEvents WHERE EventID = " + Convert.ToString(e.pEvent.Id);
				objCommand.ExecuteNonQuery();
				
				e.bResult = true;
			}
			catch(System.Exception ex)
			{
				System.Diagnostics.Debug.WriteLine("EXCEPTION! providerSQLServer.m_axCalendar_DoCreateRPattern: " + ex.Message);
			}
		}

		private void m_axCalendar_DoDeleteRPattern(object sender, AxXtremeCalendarControl._DCalendarControlEvents_DoDeleteRPatternEvent e)
		{
			e.bResult = false;
    
			if(!IsOpen()) 
			{
				return;
			}

			try
			{
				System.Data.OleDb.OleDbCommand objCommand = m_oleDbConnection.CreateCommand();
				objCommand.CommandText = "DELETE FROM CalendarRecurrencePatterns WHERE RecurrencePatternID = " + Convert.ToString(e.pPattern.Id);
				objCommand.ExecuteNonQuery();
				
				e.bResult = true;
			}
			catch(System.Exception ex)
			{
				System.Diagnostics.Debug.WriteLine("EXCEPTION! providerSQLServer.m_axCalendar_DoCreateRPattern: " + ex.Message);
			}
		}

		private void m_axCalendar_DoReadEvent(object sender, AxXtremeCalendarControl._DCalendarControlEvents_DoReadEventEvent e)
		{
			if(!IsOpen()) 
			{
				return;
			}

			System.Data.OleDb.OleDbDataReader oleReader = null;

			try
			{	
				System.Data.OleDb.OleDbCommand oleDbCommand = m_oleDbConnection.CreateCommand();
				oleDbCommand.CommandText = "SELECT * FROM CalendarEvents WHERE EventID = " + Convert.ToString(e.eventID);

				oleReader = oleDbCommand.ExecuteReader();
				
				if(oleReader.Read())
				{
					e.pEvent = m_DataHelper.CreateEventFromData(oleReader);					
				}
			}
			catch(System.Exception ex)
			{
				System.Diagnostics.Debug.WriteLine("EXCEPTION! providerSQLServer.m_axCalendar_DoReadEvent: " + ex.Message);
			}

			//================================================
			if(oleReader != null && !oleReader.IsClosed) 
			{
				oleReader.Close();
			}
		}

		private void m_axCalendar_DoReadRPattern(object sender, AxXtremeCalendarControl._DCalendarControlEvents_DoReadRPatternEvent e)
		{
			if(!IsOpen()) 
			{
				return;
			}

			System.Data.OleDb.OleDbDataReader oleReader = null;

			try
			{	
				System.Data.OleDb.OleDbCommand oleDbCommand = m_oleDbConnection.CreateCommand();
				oleDbCommand.CommandText = "SELECT * FROM CalendarRecurrencePatterns WHERE RecurrencePatternID = " + Convert.ToString(e.patternID);

				oleReader = oleDbCommand.ExecuteReader();
				
				if(oleReader.Read())
				{
					e.pPattern = m_DataHelper.CreateRPatternFromData(oleReader, m_oleDbConnection);
				}
			}
			catch(System.Exception ex)
			{
				System.Diagnostics.Debug.WriteLine("EXCEPTION! providerSQLServer.m_axCalendar_DoReadRPattern: " + ex.Message);
			}

			//================================================
			if(oleReader != null && !oleReader.IsClosed) 
			{
				oleReader.Close();
			}
		}

		private void m_axCalendar_DoUpdateEvent(object sender, AxXtremeCalendarControl._DCalendarControlEvents_DoUpdateEventEvent e)
		{
			e.bResult = false;
    
			if(!IsOpen()) 
			{
				return;
			}

			try
			{
				String strSQL = "SELECT * FROM CalendarEvents WHERE EventID = " + Convert.ToString(e.pEvent.Id);

				System.Data.OleDb.OleDbDataAdapter oleDbDataAdapter = new System.Data.OleDb.OleDbDataAdapter(strSQL, m_oleDbConnection);				
				System.Data.OleDb.OleDbCommandBuilder custCB = new System.Data.OleDb.OleDbCommandBuilder(oleDbDataAdapter);
				custCB.QuotePrefix = "[";
				custCB.QuoteSuffix = "]";

				System.Data.DataSet objEventDSet = new System.Data.DataSet();
				oleDbDataAdapter.Fill(objEventDSet);

				if(objEventDSet.Tables[0].Rows.Count == 0)
				{
					return;
				}
				//code to modify data in dataset here
				System.Data.DataRow objDRow = objEventDSet.Tables[0].Rows[0];
				
				m_DataHelper.PutEventToData(e.pEvent, objDRow);

				oleDbDataAdapter.Update(objEventDSet);

				e.bResult = true;
			}
			catch(System.Exception ex)
			{
				System.Diagnostics.Debug.WriteLine("EXCEPTION! providerSQLServer.m_axCalendar_DoUpdateEvent: " + ex.Message);
			}
		}

		private void m_axCalendar_DoUpdateRPattern(object sender, AxXtremeCalendarControl._DCalendarControlEvents_DoUpdateRPatternEvent e)
		{
			e.bResult = false;
    
			if(!IsOpen()) 
			{
				return;
			}

			try
			{
				String strSQL = "SELECT * FROM CalendarRecurrencePatterns WHERE RecurrencePatternID = " + Convert.ToString(e.pPattern.Id);

				System.Data.OleDb.OleDbDataAdapter oleDbDataAdapter = new System.Data.OleDb.OleDbDataAdapter(strSQL, m_oleDbConnection);				
				System.Data.OleDb.OleDbCommandBuilder custCB = new System.Data.OleDb.OleDbCommandBuilder(oleDbDataAdapter);
				custCB.QuotePrefix = "[";
				custCB.QuoteSuffix = "]";

				System.Data.DataSet objPatternDSet = new System.Data.DataSet();
				oleDbDataAdapter.Fill(objPatternDSet);

				if(objPatternDSet.Tables[0].Rows.Count == 0)
				{
					return;
				}
				//code to modify data in dataset here
				System.Data.DataRow objDRow = objPatternDSet.Tables[0].Rows[0];
				
				m_DataHelper.PutRPatternToData(e.pPattern, objDRow);

				oleDbDataAdapter.Update(objPatternDSet);

				e.bResult = true;
			}
			catch(System.Exception ex)
			{
				System.Diagnostics.Debug.WriteLine("EXCEPTION! providerSQLServer.m_axCalendar_DoUpdateRPattern: " + ex.Message);
			}
		}

		private void m_axCalendar_DoGetUpcomingEvents(object sender, AxXtremeCalendarControl._DCalendarControlEvents_DoGetUpcomingEventsEvent e)
		{
			if(!IsOpen()) 
			{
				return;
			}

			System.Data.OleDb.OleDbDataReader oleReader = null;

			try
			{	
				System.Data.OleDb.OleDbCommand oleDbCommand = m_oleDbConnection.CreateCommand();
				oleDbCommand.CommandText = m_DataHelper.MakeGetUpcomingEventsSQL(e.dtFrom, e.periodMinutes, true);

				oleReader = oleDbCommand.ExecuteReader();

				while(oleReader.Read())
				{					
					CalendarEvent axEvent = m_DataHelper.CreateEventFromData(oleReader);
					if (axEvent != null) 
					{
						e.pEvents.Add(axEvent);
					}
				}
			}
			catch(System.Exception ex)
			{
				System.Diagnostics.Debug.WriteLine("EXCEPTION! providerSQLServer.m_axCalendar_DoGetUpcomingEvents: " + ex.Message);
			}

			//================================================
			if(oleReader != null && !oleReader.IsClosed) 
			{
				oleReader.Close();
			}
		}
	}
}
