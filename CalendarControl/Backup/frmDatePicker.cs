using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using XtremeCalendarControl;

namespace CalendarControl
{
	/// <summary>
	/// Summary description for frmDatePicker.
	/// </summary>
	public class frmDatePicker : System.Windows.Forms.Form
	{
		public AxXtremeCalendarControl.AxDatePicker DatePicker;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;
		static public frmDatePicker Instance;

		public frmDatePicker()
		{
			//
			// Required for Windows Form Designer support
			//
			Instance = this;
			InitializeComponent();
			InitializeDatePicker();
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		private void InitializeDatePicker()
		{
			//Set Auto-size to false to force the number of rows and columns
			//DatePicker.AutoSize = false;
			//DatePicker.ColumnCount = 6;
			//DatePicker.RowCount = 5;

			DatePicker.FirstDayOfWeek = frmMain.Instance.cmbFirstDayOfWeek.SelectedIndex + 1;
						
			XtremeCalendarControl.CalendarControl pCalendarOcx = 
				( (XtremeCalendarControl.CalendarControl)(frmMain.Instance.wndCalendarControl.GetOcx()) );						
			
			DatePicker.AttachToCalendar(pCalendarOcx);

		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(frmDatePicker));
			this.DatePicker = new AxXtremeCalendarControl.AxDatePicker();
			((System.ComponentModel.ISupportInitialize)(this.DatePicker)).BeginInit();
			this.SuspendLayout();
			// 
			// DatePicker
			// 
			this.DatePicker.Dock = System.Windows.Forms.DockStyle.Fill;
			this.DatePicker.Name = "DatePicker";
			this.DatePicker.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("DatePicker.OcxState")));
			this.DatePicker.Size = new System.Drawing.Size(346, 322);
			this.DatePicker.TabIndex = 0;
			// 
			// frmDatePicker
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(346, 322);
			this.Controls.AddRange(new System.Windows.Forms.Control[] {
																		  this.DatePicker});
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
			this.Name = "frmDatePicker";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Date Picker";
			this.Closed += new System.EventHandler(this.frmDatePicker_Closed);
			((System.ComponentModel.ISupportInitialize)(this.DatePicker)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		private void frmDatePicker_Closed(object sender, System.EventArgs e)
		{
			if (frmMain.Instance.mnuDatePicker.Checked == true) 
			{
				frmMain.Instance.mnuDatePicker.Checked = false;
			}

			//Called automatically on DatePicker destoy
			//DatePicker.DetachFromCalendar(); 

			frmMain.Instance.frmDatePicker = null;
		}

	}
}
