using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using XtremeCalendarControl;

namespace CalendarControl
{
	public enum CalendarView
	{
		DayView = 1,
		WorkWeekView = 2,
		WeekView = 4,
		MonthView = 8
	};

	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class frmMain : System.Windows.Forms.Form
	{
		private System.Windows.Forms.StatusBar sbStatusBar;
		private System.Windows.Forms.ContextMenu contextMenu;
		private System.Windows.Forms.ToolBar toolBar1;
		private System.Windows.Forms.MainMenu mainMenu1;
		private System.Windows.Forms.ImageList imlToolbarIcons;
		private System.Windows.Forms.Panel OptionsPane;
		private System.Windows.Forms.GroupBox gbxCalendarWorkWeek;
		private System.Windows.Forms.GroupBox gbxDayView;
		private System.Windows.Forms.GroupBox gbxWeekView;
		private System.Windows.Forms.GroupBox gbxMonthView;
		private System.Windows.Forms.ComboBox cmbTimeScale;
		private System.Windows.Forms.Button cmdTimeZone;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.CheckBox chkDayShowTimeAsClocks;
		private System.Windows.Forms.CheckBox chkDayShowEndTime;
		private System.Windows.Forms.CheckBox chkMonthShowTimeAsClocks;
		private System.Windows.Forms.CheckBox chkMonthShowEndTime;
		private System.Windows.Forms.CheckBox chkCompressWeekendDays;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.ComboBox cmbWeeksCount;
		private System.Windows.Forms.CheckBox chkMonday;
		private System.Windows.Forms.CheckBox chkTuesday;
		private System.Windows.Forms.CheckBox chkWednesday;
		private System.Windows.Forms.CheckBox chkThursday;
		private System.Windows.Forms.CheckBox chkFriday;
		private System.Windows.Forms.CheckBox chkSaturday;
		private System.Windows.Forms.Label label3;
		public System.Windows.Forms.ComboBox cmbFirstDayOfWeek;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.ComboBox cmbStartTime;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.ComboBox cmbEndTime;
		private System.Windows.Forms.Panel labelPane;
		private System.Windows.Forms.Label lblCalendar;
		private System.Windows.Forms.Label lblDate;
		private System.Windows.Forms.ToolBarButton toolBarButton1;
		private System.Windows.Forms.ToolBarButton toolBarButton2;
		private System.Windows.Forms.ToolBarButton toolBarButton3;
		private System.Windows.Forms.ToolBarButton toolBarButton4;
		private System.Windows.Forms.ToolBarButton toolBarButton5;
		private System.Windows.Forms.ToolBarButton toolBarButton6;
		private System.Windows.Forms.ToolBarButton tbrFileNew;
		private System.Windows.Forms.ToolBarButton tbrFileOpen;
		private System.Windows.Forms.ToolBarButton tbrFileSave;
		private System.Windows.Forms.ToolBarButton tbrEditUndo;
		private System.Windows.Forms.ToolBarButton tbrEditCut;
		private System.Windows.Forms.ToolBarButton tbrEditCopy;
		private System.Windows.Forms.ToolBarButton tbrEditPaste;
		private System.Windows.Forms.ToolBarButton tbrDayView;
		private System.Windows.Forms.ToolBarButton tbrWorkWeekView;
		private System.Windows.Forms.ToolBarButton tbrWeekView;
		private System.Windows.Forms.ToolBarButton tbrMonthView;
		private System.Windows.Forms.ToolBarButton tbrDatePicker;
		private System.Windows.Forms.ToolBarButton tbrOptionsPane;
		private System.Windows.Forms.ToolBarButton tbrHelpAbout;
		private System.Windows.Forms.MenuItem menuItem1;
		private System.Windows.Forms.MenuItem mnuFileNew;
		private System.Windows.Forms.MenuItem mnuFileOpen;
		private System.Windows.Forms.MenuItem mnuFileSave;
		private System.Windows.Forms.MenuItem mnuFileExit;
		private System.Windows.Forms.MenuItem menuItem2;
		private System.Windows.Forms.MenuItem mnuEditUndo;
		private System.Windows.Forms.MenuItem menuItem4;
		private System.Windows.Forms.MenuItem mnuEditCut;
		private System.Windows.Forms.MenuItem mnuEditCopy;
		private System.Windows.Forms.MenuItem mnuEditPaste;
		private System.Windows.Forms.MenuItem menuItem9;
		private System.Windows.Forms.MenuItem mnuEditAddNewEvent;
		private System.Windows.Forms.MenuItem menuItem3;
		private System.Windows.Forms.MenuItem mnuViewToolBar;
		private System.Windows.Forms.MenuItem mnuViewStatusBar;
		private System.Windows.Forms.MenuItem menuItem5;
		private System.Windows.Forms.MenuItem mnuViewOptionsBar;
		public System.Windows.Forms.MenuItem mnuDatePicker;
		private System.Windows.Forms.MenuItem menuItem7;
		private System.Windows.Forms.MenuItem menuItem8;
		private System.Windows.Forms.MenuItem mnuCalendarDayView;
		private System.Windows.Forms.MenuItem mnuCalendarWorkWeekView;
		private System.Windows.Forms.MenuItem mnuCalendarWeekView;
		private System.Windows.Forms.MenuItem mnuCalendarMonthView;
		public System.Windows.Forms.MenuItem mnuChangeTimeScalePopup;
		private System.Windows.Forms.MenuItem mnuCalendar60Min;
		private System.Windows.Forms.MenuItem mnuCalendar30Min;
		private System.Windows.Forms.MenuItem mnuCalendar15Min;
		private System.Windows.Forms.MenuItem mnuCalendar10Min;
		private System.Windows.Forms.MenuItem mnuCalendar6Min;
		private System.Windows.Forms.MenuItem mnuCalendar5Min;
		private System.Windows.Forms.MenuItem menuItem17;
		private System.Windows.Forms.MenuItem mnuCalendarChangeTimeZone;
		private System.Windows.Forms.MenuItem menuItem11;
		private System.Windows.Forms.MenuItem mnuHelpAbout;
		private System.Windows.Forms.MenuItem mnuContextEditEvent;
		private System.Windows.Forms.MenuItem mnuContextDeleteEvent;
		private System.Windows.Forms.CheckBox chkSunday;
		private System.ComponentModel.IContainer components;
		private System.Windows.Forms.MenuItem mnuContextNewEvent;
		static public frmMain Instance;
		frmEventProperties frmEventProperties = null;
		frmOpenRecurringItem frmOpenRecurringItem = null;
		frmTimeZone frmTimeZone = null;
		public CalendarEvent ContextEvent;		
		public frmDatePicker frmDatePicker = null;

		DateTime BeginSelection, EndSelection;
		Boolean AllDay;
		int nCustomIconCounter; 
		public AxXtremeCalendarControl.AxCalendarControl wndCalendarControl;
		
		private XtremeCalendarControl.CalendarDialogsClass objCalendraDialogsReminders;

		//Sting used to hold path to save xml data files.  This file will contain all of the event information displayed in
		//the calendar view.
		public String ConnectionString = @"Provider=XML;Data Source=" + System.Environment.CurrentDirectory + @"..\..\..\";
		private System.Windows.Forms.ToolBarButton tbrPrintPageSetup;
		private System.Windows.Forms.ToolBarButton tbrPrintPreview;
		private System.Windows.Forms.ToolBarButton tbrPrint;
		private System.Windows.Forms.ToolBarButton toolBarButton7;
		private System.Windows.Forms.MenuItem menuItem6;
		private System.Windows.Forms.MenuItem mnuFilePrint;
		private System.Windows.Forms.MenuItem mnuFilePrintPreview;
		private System.Windows.Forms.MenuItem mnuFilePrintPageSetup;
		private System.Windows.Forms.MenuItem menuItem14;
		private System.Windows.Forms.MenuItem menuItem10;
		private System.Windows.Forms.MenuItem mnuViewRemindersWindow;
		private System.Windows.Forms.MenuItem menuItem12;
		private System.Windows.Forms.MenuItem mnuEnableReminders;
		private System.Windows.Forms.MenuItem menuItem15;
		private System.Windows.Forms.MenuItem mnuThemeOffice2007;
		private System.Windows.Forms.MenuItem menuItem13;
		private System.Windows.Forms.MenuItem menuShowCustomIcons;

		// SQL Server data handler
		private providerSQLServer m_objSQLProvider;

		public frmMain()
		{
			//
			// Required for Windows Form Designer support
			//
			Instance = this;
			InitializeComponent();
			InitializeControls();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		private void InitializeControls()
		{
			objCalendraDialogsReminders = new XtremeCalendarControl.CalendarDialogsClass();

			cmbTimeScale.Items.Insert(0, "60 Minutes");
			cmbTimeScale.Items.Insert(1, "30 Minutes");
			cmbTimeScale.Items.Insert(2, "15 Minutes");
			cmbTimeScale.Items.Insert(3, "10 Minutes");
			cmbTimeScale.Items.Insert(4, "6 Minutes");
			cmbTimeScale.Items.Insert(5, "5 Minutes");
			cmbTimeScale.SelectedIndex = 1;

			//Min # of Weeks is 2
			//Max # of Weeks is 6
			cmbWeeksCount.Items.Insert(0, 2);
			cmbWeeksCount.Items.Insert(1, 3);
			cmbWeeksCount.Items.Insert(2, 4);
			cmbWeeksCount.Items.Insert(3, 5);
			cmbWeeksCount.Items.Insert(4, 6);
			cmbWeeksCount.SelectedIndex = 4;

			cmbFirstDayOfWeek.Items.Insert(0, WorkWeekDay.Sunday);
			cmbFirstDayOfWeek.Items.Insert(1, WorkWeekDay.Monday);
			cmbFirstDayOfWeek.Items.Insert(2, WorkWeekDay.Tuesday);
			cmbFirstDayOfWeek.Items.Insert(3, WorkWeekDay.Wednesday);
			cmbFirstDayOfWeek.Items.Insert(4, WorkWeekDay.Thursday);
			cmbFirstDayOfWeek.Items.Insert(5, WorkWeekDay.Friday);
			cmbFirstDayOfWeek.Items.Insert(6, WorkWeekDay.Saturday);
			cmbFirstDayOfWeek.SelectedIndex = 1;

			wndCalendarControl.Options.WorkWeekMask = 0;

			DateTime dtHours = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0);
		
			for ( int cnt = 0; cnt < 48; cnt++ ) 
			{        		
				cmbStartTime.Items.Insert(cnt, dtHours.ToShortTimeString());
				cmbEndTime.Items.Insert(cnt, dtHours.ToShortTimeString());
				
				dtHours = dtHours.AddMinutes(30);
			}
			
			cmbStartTime.SelectedIndex = 16;
			cmbEndTime.SelectedIndex = 34;		

			WorkWeekDayCheck_Click(this, new System.EventArgs());

			mnuCalendarWorkWeekView_Click(this, new System.EventArgs());

			UpdateDateLabel();

			AddTestEvents(true);
			
			wndCalendarControl.DayView.ScrollToWorkDayBegin();
		}

		public enum WorkWeekDay
		{
			Sunday = 1,
			Monday = 2,
			Tuesday = 3,
			Wednesday = 4,
			Thursday = 5,
			Friday = 6,
			Saturday = 7
		};
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			nCustomIconCounter = 0;
			this.components = new System.ComponentModel.Container();
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(frmMain));
			this.wndCalendarControl = new AxXtremeCalendarControl.AxCalendarControl();
			this.sbStatusBar = new System.Windows.Forms.StatusBar();
			this.contextMenu = new System.Windows.Forms.ContextMenu();
			this.mnuContextNewEvent = new System.Windows.Forms.MenuItem();
			this.mnuContextEditEvent = new System.Windows.Forms.MenuItem();
			this.mnuContextDeleteEvent = new System.Windows.Forms.MenuItem();
			this.toolBar1 = new System.Windows.Forms.ToolBar();
			this.tbrFileNew = new System.Windows.Forms.ToolBarButton();
			this.tbrFileOpen = new System.Windows.Forms.ToolBarButton();
			this.tbrFileSave = new System.Windows.Forms.ToolBarButton();
			this.toolBarButton7 = new System.Windows.Forms.ToolBarButton();
			this.tbrPrintPageSetup = new System.Windows.Forms.ToolBarButton();
			this.tbrPrintPreview = new System.Windows.Forms.ToolBarButton();
			this.tbrPrint = new System.Windows.Forms.ToolBarButton();
			this.toolBarButton1 = new System.Windows.Forms.ToolBarButton();
			this.tbrEditUndo = new System.Windows.Forms.ToolBarButton();
			this.toolBarButton2 = new System.Windows.Forms.ToolBarButton();
			this.tbrEditCut = new System.Windows.Forms.ToolBarButton();
			this.tbrEditCopy = new System.Windows.Forms.ToolBarButton();
			this.tbrEditPaste = new System.Windows.Forms.ToolBarButton();
			this.toolBarButton3 = new System.Windows.Forms.ToolBarButton();
			this.tbrDayView = new System.Windows.Forms.ToolBarButton();
			this.tbrWorkWeekView = new System.Windows.Forms.ToolBarButton();
			this.tbrWeekView = new System.Windows.Forms.ToolBarButton();
			this.tbrMonthView = new System.Windows.Forms.ToolBarButton();
			this.toolBarButton4 = new System.Windows.Forms.ToolBarButton();
			this.tbrDatePicker = new System.Windows.Forms.ToolBarButton();
			this.toolBarButton5 = new System.Windows.Forms.ToolBarButton();
			this.tbrOptionsPane = new System.Windows.Forms.ToolBarButton();
			this.toolBarButton6 = new System.Windows.Forms.ToolBarButton();
			this.tbrHelpAbout = new System.Windows.Forms.ToolBarButton();
			this.imlToolbarIcons = new System.Windows.Forms.ImageList(this.components);
			this.mainMenu1 = new System.Windows.Forms.MainMenu();
			this.menuItem1 = new System.Windows.Forms.MenuItem();
			this.mnuFileNew = new System.Windows.Forms.MenuItem();
			this.mnuFileOpen = new System.Windows.Forms.MenuItem();
			this.mnuFileSave = new System.Windows.Forms.MenuItem();
			this.menuItem6 = new System.Windows.Forms.MenuItem();
			this.mnuFilePrint = new System.Windows.Forms.MenuItem();
			this.mnuFilePrintPreview = new System.Windows.Forms.MenuItem();
			this.mnuFilePrintPageSetup = new System.Windows.Forms.MenuItem();
			this.menuItem14 = new System.Windows.Forms.MenuItem();
			this.mnuFileExit = new System.Windows.Forms.MenuItem();
			this.menuItem2 = new System.Windows.Forms.MenuItem();
			this.mnuEditUndo = new System.Windows.Forms.MenuItem();
			this.menuItem4 = new System.Windows.Forms.MenuItem();
			this.mnuEditCut = new System.Windows.Forms.MenuItem();
			this.mnuEditCopy = new System.Windows.Forms.MenuItem();
			this.mnuEditPaste = new System.Windows.Forms.MenuItem();
			this.menuItem9 = new System.Windows.Forms.MenuItem();
			this.mnuEditAddNewEvent = new System.Windows.Forms.MenuItem();
			this.menuItem3 = new System.Windows.Forms.MenuItem();
			this.mnuViewToolBar = new System.Windows.Forms.MenuItem();
			this.mnuViewStatusBar = new System.Windows.Forms.MenuItem();
			this.menuItem5 = new System.Windows.Forms.MenuItem();
			this.mnuViewOptionsBar = new System.Windows.Forms.MenuItem();
			this.mnuDatePicker = new System.Windows.Forms.MenuItem();
			this.menuItem10 = new System.Windows.Forms.MenuItem();
			this.mnuViewRemindersWindow = new System.Windows.Forms.MenuItem();
			this.menuItem7 = new System.Windows.Forms.MenuItem();
			this.menuItem8 = new System.Windows.Forms.MenuItem();
			this.mnuCalendarDayView = new System.Windows.Forms.MenuItem();
			this.mnuCalendarWorkWeekView = new System.Windows.Forms.MenuItem();
			this.mnuCalendarWeekView = new System.Windows.Forms.MenuItem();
			this.mnuCalendarMonthView = new System.Windows.Forms.MenuItem();
			this.mnuChangeTimeScalePopup = new System.Windows.Forms.MenuItem();
			this.mnuCalendar60Min = new System.Windows.Forms.MenuItem();
			this.mnuCalendar30Min = new System.Windows.Forms.MenuItem();
			this.mnuCalendar15Min = new System.Windows.Forms.MenuItem();
			this.mnuCalendar10Min = new System.Windows.Forms.MenuItem();
			this.mnuCalendar6Min = new System.Windows.Forms.MenuItem();
			this.mnuCalendar5Min = new System.Windows.Forms.MenuItem();
			this.menuItem17 = new System.Windows.Forms.MenuItem();
			this.mnuCalendarChangeTimeZone = new System.Windows.Forms.MenuItem();
			this.menuItem12 = new System.Windows.Forms.MenuItem();
			this.mnuEnableReminders = new System.Windows.Forms.MenuItem();
			this.menuItem15 = new System.Windows.Forms.MenuItem();
			this.mnuThemeOffice2007 = new System.Windows.Forms.MenuItem();
			this.menuItem11 = new System.Windows.Forms.MenuItem();
			this.mnuHelpAbout = new System.Windows.Forms.MenuItem();
			this.OptionsPane = new System.Windows.Forms.Panel();
			this.gbxMonthView = new System.Windows.Forms.GroupBox();
			this.cmbWeeksCount = new System.Windows.Forms.ComboBox();
			this.label2 = new System.Windows.Forms.Label();
			this.chkCompressWeekendDays = new System.Windows.Forms.CheckBox();
			this.chkMonthShowEndTime = new System.Windows.Forms.CheckBox();
			this.chkMonthShowTimeAsClocks = new System.Windows.Forms.CheckBox();
			this.gbxWeekView = new System.Windows.Forms.GroupBox();
			this.chkDayShowEndTime = new System.Windows.Forms.CheckBox();
			this.chkDayShowTimeAsClocks = new System.Windows.Forms.CheckBox();
			this.gbxDayView = new System.Windows.Forms.GroupBox();
			this.label1 = new System.Windows.Forms.Label();
			this.cmdTimeZone = new System.Windows.Forms.Button();
			this.cmbTimeScale = new System.Windows.Forms.ComboBox();
			this.gbxCalendarWorkWeek = new System.Windows.Forms.GroupBox();
			this.cmbEndTime = new System.Windows.Forms.ComboBox();
			this.label5 = new System.Windows.Forms.Label();
			this.cmbStartTime = new System.Windows.Forms.ComboBox();
			this.label4 = new System.Windows.Forms.Label();
			this.cmbFirstDayOfWeek = new System.Windows.Forms.ComboBox();
			this.label3 = new System.Windows.Forms.Label();
			this.chkSaturday = new System.Windows.Forms.CheckBox();
			this.chkFriday = new System.Windows.Forms.CheckBox();
			this.chkThursday = new System.Windows.Forms.CheckBox();
			this.chkWednesday = new System.Windows.Forms.CheckBox();
			this.chkTuesday = new System.Windows.Forms.CheckBox();
			this.chkMonday = new System.Windows.Forms.CheckBox();
			this.chkSunday = new System.Windows.Forms.CheckBox();
			this.labelPane = new System.Windows.Forms.Panel();
			this.lblDate = new System.Windows.Forms.Label();
			this.lblCalendar = new System.Windows.Forms.Label();
			this.menuItem13 = new System.Windows.Forms.MenuItem();
			this.menuShowCustomIcons = new System.Windows.Forms.MenuItem();
			((System.ComponentModel.ISupportInitialize)(this.wndCalendarControl)).BeginInit();
			this.OptionsPane.SuspendLayout();
			this.gbxMonthView.SuspendLayout();
			this.gbxWeekView.SuspendLayout();
			this.gbxDayView.SuspendLayout();
			this.gbxCalendarWorkWeek.SuspendLayout();
			this.labelPane.SuspendLayout();
			this.SuspendLayout();
			// 
			// wndCalendarControl
			// 
			this.wndCalendarControl.Dock = System.Windows.Forms.DockStyle.Fill;
			this.wndCalendarControl.Location = new System.Drawing.Point(0, 162);
			this.wndCalendarControl.Name = "wndCalendarControl";
			this.wndCalendarControl.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("wndCalendarControl.OcxState")));
			this.wndCalendarControl.Size = new System.Drawing.Size(808, 311);
			this.wndCalendarControl.TabIndex = 0;
			this.wndCalendarControl.MouseDownEvent += new AxXtremeCalendarControl._DCalendarControlEvents_MouseDownEventHandler(this.CalendarControl_MouseDownEvent);
			this.wndCalendarControl.ViewChanged += new System.EventHandler(this.CalendarControl_ViewChanged);
			this.wndCalendarControl.MouseMoveEvent += new AxXtremeCalendarControl._DCalendarControlEvents_MouseMoveEventHandler(this.CalendarControl_MouseMoveEvent);
			this.wndCalendarControl.DblClick += new System.EventHandler(this.CalendarControl_DblClick);
            
            // Use it only when you need - its quite slow - called for each cell on each redraw.
            //this.wndCalendarControl.BeforeDrawDayViewCell += new AxXtremeCalendarControl._DCalendarControlEvents_BeforeDrawDayViewCellEventHandler(this.wndCalendarControl_BeforeDrawDayViewCell);

            // 
			// sbStatusBar
			// 
			this.sbStatusBar.Location = new System.Drawing.Point(0, 473);
			this.sbStatusBar.Name = "sbStatusBar";
			this.sbStatusBar.Size = new System.Drawing.Size(808, 16);
			this.sbStatusBar.TabIndex = 1;
			// 
			// contextMenu
			// 
			this.contextMenu.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																						this.mnuContextNewEvent,
																						this.mnuContextEditEvent,
																						this.mnuContextDeleteEvent});
			// 
			// mnuContextNewEvent
			// 
			this.mnuContextNewEvent.Index = 0;
			this.mnuContextNewEvent.Text = "New Event";
			this.mnuContextNewEvent.Click += new System.EventHandler(this.mnuContextNewEvent_Click);
			// 
			// mnuContextEditEvent
			// 
			this.mnuContextEditEvent.Index = 1;
			this.mnuContextEditEvent.Text = "Edit Event";
			this.mnuContextEditEvent.Click += new System.EventHandler(this.mnuContextEditEvent_Click);
			// 
			// mnuContextDeleteEvent
			// 
			this.mnuContextDeleteEvent.Index = 2;
			this.mnuContextDeleteEvent.Text = "Delete Event";
			this.mnuContextDeleteEvent.Click += new System.EventHandler(this.mnuContextDeleteEvent_Click);
			// 
			// toolBar1
			// 
			this.toolBar1.Appearance = System.Windows.Forms.ToolBarAppearance.Flat;
			this.toolBar1.Buttons.AddRange(new System.Windows.Forms.ToolBarButton[] {
																						this.tbrFileNew,
																						this.tbrFileOpen,
																						this.tbrFileSave,
																						this.toolBarButton7,
																						this.tbrPrintPageSetup,
																						this.tbrPrintPreview,
																						this.tbrPrint,
																						this.toolBarButton1,
																						this.tbrEditUndo,
																						this.toolBarButton2,
																						this.tbrEditCut,
																						this.tbrEditCopy,
																						this.tbrEditPaste,
																						this.toolBarButton3,
																						this.tbrDayView,
																						this.tbrWorkWeekView,
																						this.tbrWeekView,
																						this.tbrMonthView,
																						this.toolBarButton4,
																						this.tbrDatePicker,
																						this.toolBarButton5,
																						this.tbrOptionsPane,
																						this.toolBarButton6,
																						this.tbrHelpAbout});
			this.toolBar1.DropDownArrows = true;
			this.toolBar1.ImageList = this.imlToolbarIcons;
			this.toolBar1.Location = new System.Drawing.Point(0, 0);
			this.toolBar1.Name = "toolBar1";
			this.toolBar1.ShowToolTips = true;
			this.toolBar1.Size = new System.Drawing.Size(808, 27);
			this.toolBar1.TabIndex = 2;
			this.toolBar1.ButtonClick += new System.Windows.Forms.ToolBarButtonClickEventHandler(this.toolBar1_ButtonClick);
			// 
			// tbrFileNew
			// 
			this.tbrFileNew.Enabled = false;
			this.tbrFileNew.ImageIndex = 0;
			this.tbrFileNew.Tag = "FileNew";
			// 
			// tbrFileOpen
			// 
			this.tbrFileOpen.ImageIndex = 1;
			this.tbrFileOpen.Tag = "FileOpen";
			// 
			// tbrFileSave
			// 
			this.tbrFileSave.ImageIndex = 2;
			this.tbrFileSave.Tag = "FileSave";
			// 
			// toolBarButton7
			// 
			this.toolBarButton7.Style = System.Windows.Forms.ToolBarButtonStyle.Separator;
			// 
			// tbrPrintPageSetup
			// 
			this.tbrPrintPageSetup.ImageIndex = 14;
			this.tbrPrintPageSetup.Tag = "PrintPageSetup";
			// 
			// tbrPrintPreview
			// 
			this.tbrPrintPreview.ImageIndex = 15;
			this.tbrPrintPreview.Tag = "PrintPreview";
			// 
			// tbrPrint
			// 
			this.tbrPrint.ImageIndex = 16;
			this.tbrPrint.Tag = "Print";
			// 
			// toolBarButton1
			// 
			this.toolBarButton1.Style = System.Windows.Forms.ToolBarButtonStyle.Separator;
			// 
			// tbrEditUndo
			// 
			this.tbrEditUndo.Enabled = false;
			this.tbrEditUndo.ImageIndex = 3;
			this.tbrEditUndo.Tag = "EditUndo";
			// 
			// toolBarButton2
			// 
			this.toolBarButton2.Style = System.Windows.Forms.ToolBarButtonStyle.Separator;
			// 
			// tbrEditCut
			// 
			this.tbrEditCut.Enabled = false;
			this.tbrEditCut.ImageIndex = 4;
			this.tbrEditCut.Tag = "EditCut";
			// 
			// tbrEditCopy
			// 
			this.tbrEditCopy.Enabled = false;
			this.tbrEditCopy.ImageIndex = 5;
			this.tbrEditCopy.Tag = "EditCopy";
			// 
			// tbrEditPaste
			// 
			this.tbrEditPaste.Enabled = false;
			this.tbrEditPaste.ImageIndex = 6;
			this.tbrEditPaste.Tag = "EditPaste";
			// 
			// toolBarButton3
			// 
			this.toolBarButton3.Style = System.Windows.Forms.ToolBarButtonStyle.Separator;
			// 
			// tbrDayView
			// 
			this.tbrDayView.ImageIndex = 7;
			this.tbrDayView.Tag = "DayView";
			// 
			// tbrWorkWeekView
			// 
			this.tbrWorkWeekView.ImageIndex = 8;
			this.tbrWorkWeekView.Tag = "WorkWeekView";
			// 
			// tbrWeekView
			// 
			this.tbrWeekView.ImageIndex = 9;
			this.tbrWeekView.Tag = "WeekView";
			// 
			// tbrMonthView
			// 
			this.tbrMonthView.ImageIndex = 10;
			this.tbrMonthView.Tag = "MonthView";
			// 
			// toolBarButton4
			// 
			this.toolBarButton4.Style = System.Windows.Forms.ToolBarButtonStyle.Separator;
			// 
			// tbrDatePicker
			// 
			this.tbrDatePicker.ImageIndex = 11;
			this.tbrDatePicker.Tag = "DatePicker";
			// 
			// toolBarButton5
			// 
			this.toolBarButton5.Style = System.Windows.Forms.ToolBarButtonStyle.Separator;
			// 
			// tbrOptionsPane
			// 
			this.tbrOptionsPane.ImageIndex = 12;
			this.tbrOptionsPane.Tag = "OptionsPane";
			// 
			// toolBarButton6
			// 
			this.toolBarButton6.Style = System.Windows.Forms.ToolBarButtonStyle.Separator;
			// 
			// tbrHelpAbout
			// 
			this.tbrHelpAbout.ImageIndex = 13;
			this.tbrHelpAbout.Tag = "HelpAbout";
			// 
			// imlToolbarIcons
			// 
			this.imlToolbarIcons.ColorDepth = System.Windows.Forms.ColorDepth.Depth32Bit;
			this.imlToolbarIcons.ImageSize = new System.Drawing.Size(16, 15);
			this.imlToolbarIcons.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imlToolbarIcons.ImageStream")));
			this.imlToolbarIcons.TransparentColor = System.Drawing.Color.Silver;
			// 
			// mainMenu1
			// 
			this.mainMenu1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					  this.menuItem1,
																					  this.menuItem2,
																					  this.menuItem3,
																					  this.menuItem7,
																					  this.menuItem11});
			// 
			// menuItem1
			// 
			this.menuItem1.Index = 0;
			this.menuItem1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					  this.mnuFileNew,
																					  this.mnuFileOpen,
																					  this.mnuFileSave,
																					  this.menuItem6,
																					  this.mnuFilePrint,
																					  this.mnuFilePrintPreview,
																					  this.mnuFilePrintPageSetup,
																					  this.menuItem14,
																					  this.mnuFileExit});
			this.menuItem1.Text = "File";
			// 
			// mnuFileNew
			// 
			this.mnuFileNew.Enabled = false;
			this.mnuFileNew.Index = 0;
			this.mnuFileNew.Text = "New";
			// 
			// mnuFileOpen
			// 
			this.mnuFileOpen.Index = 1;
			this.mnuFileOpen.Text = "Open...";
			this.mnuFileOpen.Click += new System.EventHandler(this.mnuFileOpen_Click);
			// 
			// mnuFileSave
			// 
			this.mnuFileSave.Index = 2;
			this.mnuFileSave.Text = "Save";
			this.mnuFileSave.Click += new System.EventHandler(this.mnuFileSave_Click);
			// 
			// menuItem6
			// 
			this.menuItem6.Index = 3;
			this.menuItem6.Text = "-";
			// 
			// mnuFilePrint
			// 
			this.mnuFilePrint.Index = 4;
			this.mnuFilePrint.Text = "Print...";
			this.mnuFilePrint.Click += new System.EventHandler(this.mnuFilePrint_Click);
			// 
			// mnuFilePrintPreview
			// 
			this.mnuFilePrintPreview.Index = 5;
			this.mnuFilePrintPreview.Text = "Print Preview";
			this.mnuFilePrintPreview.Click += new System.EventHandler(this.mnuFilePrintPreview_Click);
			// 
			// mnuFilePrintPageSetup
			// 
			this.mnuFilePrintPageSetup.Index = 6;
			this.mnuFilePrintPageSetup.Text = "Page Setup";
			this.mnuFilePrintPageSetup.Click += new System.EventHandler(this.mnuFilePrintPageSetup_Click);
			// 
			// menuItem14
			// 
			this.menuItem14.Index = 7;
			this.menuItem14.Text = "-";
			// 
			// mnuFileExit
			// 
			this.mnuFileExit.Index = 8;
			this.mnuFileExit.Text = "Exit";
			// 
			// menuItem2
			// 
			this.menuItem2.Index = 1;
			this.menuItem2.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					  this.mnuEditUndo,
																					  this.menuItem4,
																					  this.mnuEditCut,
																					  this.mnuEditCopy,
																					  this.mnuEditPaste,
																					  this.menuItem9,
																					  this.mnuEditAddNewEvent});
			this.menuItem2.Text = "Edit";
			// 
			// mnuEditUndo
			// 
			this.mnuEditUndo.Index = 0;
			this.mnuEditUndo.Text = "Undo";
			// 
			// menuItem4
			// 
			this.menuItem4.Index = 1;
			this.menuItem4.Text = "-";
			// 
			// mnuEditCut
			// 
			this.mnuEditCut.Index = 2;
			this.mnuEditCut.Text = "Cut";
			// 
			// mnuEditCopy
			// 
			this.mnuEditCopy.Index = 3;
			this.mnuEditCopy.Text = "Copy";
			// 
			// mnuEditPaste
			// 
			this.mnuEditPaste.Index = 4;
			this.mnuEditPaste.Text = "Paste";
			// 
			// menuItem9
			// 
			this.menuItem9.Index = 5;
			this.menuItem9.Text = "-";
			// 
			// mnuEditAddNewEvent
			// 
			this.mnuEditAddNewEvent.Index = 6;
			this.mnuEditAddNewEvent.Text = "Add New Event";
			// 
			// menuItem3
			// 
			this.menuItem3.Index = 2;
			this.menuItem3.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					  this.mnuViewToolBar,
																					  this.mnuViewStatusBar,
																					  this.menuItem5,
																					  this.mnuViewOptionsBar,
																					  this.mnuDatePicker,
																					  this.menuItem10,
																					  this.mnuViewRemindersWindow});
			this.menuItem3.Text = "View";
			// 
			// mnuViewToolBar
			// 
			this.mnuViewToolBar.Checked = true;
			this.mnuViewToolBar.Index = 0;
			this.mnuViewToolBar.Text = "ToolBar";
			this.mnuViewToolBar.Click += new System.EventHandler(this.mnuViewToolBar_Click);
			// 
			// mnuViewStatusBar
			// 
			this.mnuViewStatusBar.Checked = true;
			this.mnuViewStatusBar.Index = 1;
			this.mnuViewStatusBar.Text = "StatusBar";
			this.mnuViewStatusBar.Click += new System.EventHandler(this.mnuViewStatusBar_Click);
			// 
			// menuItem5
			// 
			this.menuItem5.Index = 2;
			this.menuItem5.Text = "-";
			// 
			// mnuViewOptionsBar
			// 
			this.mnuViewOptionsBar.Checked = true;
			this.mnuViewOptionsBar.Index = 3;
			this.mnuViewOptionsBar.Text = "Options Bar";
			this.mnuViewOptionsBar.Click += new System.EventHandler(this.mnuViewOptionsBar_Click);
			// 
			// mnuDatePicker
			// 
			this.mnuDatePicker.Index = 4;
			this.mnuDatePicker.Text = "Date Picker";
			this.mnuDatePicker.Click += new System.EventHandler(this.mnuDatePicker_Click);
			// 
			// menuItem10
			// 
			this.menuItem10.Index = 5;
			this.menuItem10.Text = "-";
			// 
			// mnuViewRemindersWindow
			// 
			this.mnuViewRemindersWindow.Index = 6;
			this.mnuViewRemindersWindow.Text = "Reminders Window";
			this.mnuViewRemindersWindow.Click += new System.EventHandler(this.mnuViewRemindersWindow_Click);
			// 
			// menuItem7
			// 
			this.menuItem7.Index = 3;
			this.menuItem7.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					  this.menuItem8,
																					  this.mnuChangeTimeScalePopup,
																					  this.menuItem12,
																					  this.mnuEnableReminders,
																					  this.menuItem15,
																					  this.mnuThemeOffice2007,
																					  this.menuItem13,
																					  this.menuShowCustomIcons});
			this.menuItem7.Text = "Calendar";
			// 
			// menuItem8
			// 
			this.menuItem8.Index = 0;
			this.menuItem8.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					  this.mnuCalendarDayView,
																					  this.mnuCalendarWorkWeekView,
																					  this.mnuCalendarWeekView,
																					  this.mnuCalendarMonthView});
			this.menuItem8.Text = "Change View";
			// 
			// mnuCalendarDayView
			// 
			this.mnuCalendarDayView.Index = 0;
			this.mnuCalendarDayView.Text = "Day";
			this.mnuCalendarDayView.Click += new System.EventHandler(this.mnuCalendarDayView_Click);
			// 
			// mnuCalendarWorkWeekView
			// 
			this.mnuCalendarWorkWeekView.Index = 1;
			this.mnuCalendarWorkWeekView.Text = "Work Week";
			this.mnuCalendarWorkWeekView.Click += new System.EventHandler(this.mnuCalendarWorkWeekView_Click);
			// 
			// mnuCalendarWeekView
			// 
			this.mnuCalendarWeekView.Index = 2;
			this.mnuCalendarWeekView.Text = "Week";
			this.mnuCalendarWeekView.Click += new System.EventHandler(this.mnuCalendarWeekView_Click);
			// 
			// mnuCalendarMonthView
			// 
			this.mnuCalendarMonthView.Index = 3;
			this.mnuCalendarMonthView.Text = "Month";
			this.mnuCalendarMonthView.Click += new System.EventHandler(this.mnuCalendarMonthView_Click);
			// 
			// mnuChangeTimeScalePopup
			// 
			this.mnuChangeTimeScalePopup.Index = 1;
			this.mnuChangeTimeScalePopup.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																									this.mnuCalendar60Min,
																									this.mnuCalendar30Min,
																									this.mnuCalendar15Min,
																									this.mnuCalendar10Min,
																									this.mnuCalendar6Min,
																									this.mnuCalendar5Min,
																									this.menuItem17,
																									this.mnuCalendarChangeTimeZone});
			this.mnuChangeTimeScalePopup.Text = "Change Time Scale";
			// 
			// mnuCalendar60Min
			// 
			this.mnuCalendar60Min.Index = 0;
			this.mnuCalendar60Min.Text = "60 Minutes";
			this.mnuCalendar60Min.Click += new System.EventHandler(this.mnuCalendar60Min_Click);
			// 
			// mnuCalendar30Min
			// 
			this.mnuCalendar30Min.Index = 1;
			this.mnuCalendar30Min.Text = "30 Minutes";
			this.mnuCalendar30Min.Click += new System.EventHandler(this.mnuCalendar30Min_Click);
			// 
			// mnuCalendar15Min
			// 
			this.mnuCalendar15Min.Index = 2;
			this.mnuCalendar15Min.Text = "15 Minutes";
			this.mnuCalendar15Min.Click += new System.EventHandler(this.mnuCalendar15Min_Click);
			// 
			// mnuCalendar10Min
			// 
			this.mnuCalendar10Min.Index = 3;
			this.mnuCalendar10Min.Text = "10 Minutes";
			this.mnuCalendar10Min.Click += new System.EventHandler(this.mnuCalendar10Min_Click);
			// 
			// mnuCalendar6Min
			// 
			this.mnuCalendar6Min.Index = 4;
			this.mnuCalendar6Min.Text = "6 Minutes";
			this.mnuCalendar6Min.Click += new System.EventHandler(this.mnuCalendar6Min_Click);
			// 
			// mnuCalendar5Min
			// 
			this.mnuCalendar5Min.Index = 5;
			this.mnuCalendar5Min.Text = "5 Minutes";
			this.mnuCalendar5Min.Click += new System.EventHandler(this.mnuCalendar5Min_Click);
			// 
			// menuItem17
			// 
			this.menuItem17.Index = 6;
			this.menuItem17.Text = "-";
			// 
			// mnuCalendarChangeTimeZone
			// 
			this.mnuCalendarChangeTimeZone.Index = 7;
			this.mnuCalendarChangeTimeZone.Text = "Change Time Zone";
			this.mnuCalendarChangeTimeZone.Click += new System.EventHandler(this.mnuCalendarChangeTimeZone_Click);
			// 
			// menuItem12
			// 
			this.menuItem12.Index = 2;
			this.menuItem12.Text = "-";
			// 
			// mnuEnableReminders
			// 
			this.mnuEnableReminders.Index = 3;
			this.mnuEnableReminders.Text = "Enable Reminders";
			this.mnuEnableReminders.Click += new System.EventHandler(this.mnuEnableReminders_Click);
			// 
			// menuItem15
			// 
			this.menuItem15.Index = 4;
			this.menuItem15.Text = "-";
			// 
			// mnuThemeOffice2007
			// 
			this.mnuThemeOffice2007.Checked = true;
			this.mnuThemeOffice2007.Index = 5;
			this.mnuThemeOffice2007.Text = "Enable Office 2007 theme";
			this.mnuThemeOffice2007.Click += new System.EventHandler(this.mnuThemeOffice2007_Click);
			// 
			// menuItem11
			// 
			this.menuItem11.Index = 4;
			this.menuItem11.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					   this.mnuHelpAbout});
			this.menuItem11.Text = "Help";
			// 
			// mnuHelpAbout
			// 
			this.mnuHelpAbout.Index = 0;
			this.mnuHelpAbout.Text = "About CalendarSample...";
			this.mnuHelpAbout.Click += new System.EventHandler(this.mnuHelpAbout_Click);
			// 
			// OptionsPane
			// 
			this.OptionsPane.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.OptionsPane.Controls.AddRange(new System.Windows.Forms.Control[] {
																					  this.gbxMonthView,
																					  this.gbxWeekView,
																					  this.gbxDayView,
																					  this.gbxCalendarWorkWeek});
			this.OptionsPane.Dock = System.Windows.Forms.DockStyle.Top;
			this.OptionsPane.Location = new System.Drawing.Point(0, 24);
			this.OptionsPane.Name = "OptionsPane";
			this.OptionsPane.Size = new System.Drawing.Size(808, 111);
			this.OptionsPane.TabIndex = 3;
			// 
			// gbxMonthView
			// 
			this.gbxMonthView.Controls.AddRange(new System.Windows.Forms.Control[] {
																					   this.cmbWeeksCount,
																					   this.label2,
																					   this.chkCompressWeekendDays,
																					   this.chkMonthShowEndTime,
																					   this.chkMonthShowTimeAsClocks});
			this.gbxMonthView.Location = new System.Drawing.Point(632, 6);
			this.gbxMonthView.Name = "gbxMonthView";
			this.gbxMonthView.Size = new System.Drawing.Size(178, 96);
			this.gbxMonthView.TabIndex = 3;
			this.gbxMonthView.TabStop = false;
			this.gbxMonthView.Text = "MonthView";
			// 
			// cmbWeeksCount
			// 
			this.cmbWeeksCount.Location = new System.Drawing.Point(96, 72);
			this.cmbWeeksCount.Name = "cmbWeeksCount";
			this.cmbWeeksCount.Size = new System.Drawing.Size(56, 21);
			this.cmbWeeksCount.TabIndex = 4;
			this.cmbWeeksCount.SelectedIndexChanged += new System.EventHandler(this.cmbWeeksCount_SelectedIndexChanged);
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(8, 72);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(80, 16);
			this.label2.TabIndex = 3;
			this.label2.Text = "Weeks Count:";
			// 
			// chkCompressWeekendDays
			// 
			this.chkCompressWeekendDays.Checked = true;
			this.chkCompressWeekendDays.CheckState = System.Windows.Forms.CheckState.Checked;
			this.chkCompressWeekendDays.Location = new System.Drawing.Point(8, 48);
			this.chkCompressWeekendDays.Name = "chkCompressWeekendDays";
			this.chkCompressWeekendDays.Size = new System.Drawing.Size(160, 16);
			this.chkCompressWeekendDays.TabIndex = 2;
			this.chkCompressWeekendDays.Text = "Compress Weekend Days";
			this.chkCompressWeekendDays.CheckedChanged += new System.EventHandler(this.chkCompressWeekendDays_CheckedChanged);
			// 
			// chkMonthShowEndTime
			// 
			this.chkMonthShowEndTime.Location = new System.Drawing.Point(8, 32);
			this.chkMonthShowEndTime.Name = "chkMonthShowEndTime";
			this.chkMonthShowEndTime.Size = new System.Drawing.Size(144, 16);
			this.chkMonthShowEndTime.TabIndex = 1;
			this.chkMonthShowEndTime.Text = "Show End Time";
			this.chkMonthShowEndTime.CheckedChanged += new System.EventHandler(this.chkMonthShowEndTime_CheckedChanged);
			// 
			// chkMonthShowTimeAsClocks
			// 
			this.chkMonthShowTimeAsClocks.Location = new System.Drawing.Point(8, 16);
			this.chkMonthShowTimeAsClocks.Name = "chkMonthShowTimeAsClocks";
			this.chkMonthShowTimeAsClocks.Size = new System.Drawing.Size(152, 16);
			this.chkMonthShowTimeAsClocks.TabIndex = 0;
			this.chkMonthShowTimeAsClocks.Text = "Show Time as Clocks";
			this.chkMonthShowTimeAsClocks.CheckedChanged += new System.EventHandler(this.chkMonthShowTimeAsClocks_CheckedChanged);
			// 
			// gbxWeekView
			// 
			this.gbxWeekView.Controls.AddRange(new System.Windows.Forms.Control[] {
																					  this.chkDayShowEndTime,
																					  this.chkDayShowTimeAsClocks});
			this.gbxWeekView.Location = new System.Drawing.Point(473, 5);
			this.gbxWeekView.Name = "gbxWeekView";
			this.gbxWeekView.Size = new System.Drawing.Size(154, 97);
			this.gbxWeekView.TabIndex = 2;
			this.gbxWeekView.TabStop = false;
			this.gbxWeekView.Text = "Week View";
			// 
			// chkDayShowEndTime
			// 
			this.chkDayShowEndTime.Location = new System.Drawing.Point(8, 40);
			this.chkDayShowEndTime.Name = "chkDayShowEndTime";
			this.chkDayShowEndTime.Size = new System.Drawing.Size(128, 16);
			this.chkDayShowEndTime.TabIndex = 1;
			this.chkDayShowEndTime.Text = "Show End Time";
			this.chkDayShowEndTime.CheckedChanged += new System.EventHandler(this.chkDayShowEndTime_CheckedChanged);
			// 
			// chkDayShowTimeAsClocks
			// 
			this.chkDayShowTimeAsClocks.Location = new System.Drawing.Point(8, 24);
			this.chkDayShowTimeAsClocks.Name = "chkDayShowTimeAsClocks";
			this.chkDayShowTimeAsClocks.Size = new System.Drawing.Size(136, 16);
			this.chkDayShowTimeAsClocks.TabIndex = 0;
			this.chkDayShowTimeAsClocks.Text = "Show Time as Clocks";
			this.chkDayShowTimeAsClocks.CheckedChanged += new System.EventHandler(this.chkDayShowTimeAsClocks_CheckedChanged);
			// 
			// gbxDayView
			// 
			this.gbxDayView.Controls.AddRange(new System.Windows.Forms.Control[] {
																					 this.label1,
																					 this.cmdTimeZone,
																					 this.cmbTimeScale});
			this.gbxDayView.ForeColor = System.Drawing.SystemColors.ControlText;
			this.gbxDayView.Location = new System.Drawing.Point(291, 5);
			this.gbxDayView.Name = "gbxDayView";
			this.gbxDayView.Size = new System.Drawing.Size(177, 97);
			this.gbxDayView.TabIndex = 1;
			this.gbxDayView.TabStop = false;
			this.gbxDayView.Text = "Day View";
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(8, 24);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(64, 16);
			this.label1.TabIndex = 2;
			this.label1.Text = "Time Scale:";
			// 
			// cmdTimeZone
			// 
			this.cmdTimeZone.Location = new System.Drawing.Point(80, 56);
			this.cmdTimeZone.Name = "cmdTimeZone";
			this.cmdTimeZone.Size = new System.Drawing.Size(80, 24);
			this.cmdTimeZone.TabIndex = 1;
			this.cmdTimeZone.Text = "Time Zone";
			this.cmdTimeZone.Click += new System.EventHandler(this.cmdTimeZone_Click);
			// 
			// cmbTimeScale
			// 
			this.cmbTimeScale.Location = new System.Drawing.Point(80, 24);
			this.cmbTimeScale.Name = "cmbTimeScale";
			this.cmbTimeScale.Size = new System.Drawing.Size(80, 21);
			this.cmbTimeScale.TabIndex = 0;
			this.cmbTimeScale.SelectedIndexChanged += new System.EventHandler(this.cmbTimeScale_SelectedIndexChanged);
			// 
			// gbxCalendarWorkWeek
			// 
			this.gbxCalendarWorkWeek.Controls.AddRange(new System.Windows.Forms.Control[] {
																							  this.cmbEndTime,
																							  this.label5,
																							  this.cmbStartTime,
																							  this.label4,
																							  this.cmbFirstDayOfWeek,
																							  this.label3,
																							  this.chkSaturday,
																							  this.chkFriday,
																							  this.chkThursday,
																							  this.chkWednesday,
																							  this.chkTuesday,
																							  this.chkMonday,
																							  this.chkSunday});
			this.gbxCalendarWorkWeek.Location = new System.Drawing.Point(9, 5);
			this.gbxCalendarWorkWeek.Name = "gbxCalendarWorkWeek";
			this.gbxCalendarWorkWeek.Size = new System.Drawing.Size(279, 97);
			this.gbxCalendarWorkWeek.TabIndex = 0;
			this.gbxCalendarWorkWeek.TabStop = false;
			this.gbxCalendarWorkWeek.Text = "Calendar Work Week";
			// 
			// cmbEndTime
			// 
			this.cmbEndTime.Location = new System.Drawing.Point(200, 69);
			this.cmbEndTime.Name = "cmbEndTime";
			this.cmbEndTime.Size = new System.Drawing.Size(72, 21);
			this.cmbEndTime.TabIndex = 12;
			this.cmbEndTime.SelectedIndexChanged += new System.EventHandler(this.cmbEndTime_SelectedIndexChanged);
			// 
			// label5
			// 
			this.label5.Location = new System.Drawing.Point(144, 69);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(56, 16);
			this.label5.TabIndex = 11;
			this.label5.Text = "End Time:";
			// 
			// cmbStartTime
			// 
			this.cmbStartTime.Location = new System.Drawing.Point(72, 69);
			this.cmbStartTime.Name = "cmbStartTime";
			this.cmbStartTime.Size = new System.Drawing.Size(72, 21);
			this.cmbStartTime.TabIndex = 10;
			this.cmbStartTime.SelectedIndexChanged += new System.EventHandler(this.cmbStartTime_SelectedIndexChanged);
			// 
			// label4
			// 
			this.label4.Location = new System.Drawing.Point(8, 69);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(64, 16);
			this.label4.TabIndex = 9;
			this.label4.Text = "Start Time:";
			// 
			// cmbFirstDayOfWeek
			// 
			this.cmbFirstDayOfWeek.Location = new System.Drawing.Point(112, 40);
			this.cmbFirstDayOfWeek.Name = "cmbFirstDayOfWeek";
			this.cmbFirstDayOfWeek.Size = new System.Drawing.Size(120, 21);
			this.cmbFirstDayOfWeek.TabIndex = 8;
			this.cmbFirstDayOfWeek.SelectedIndexChanged += new System.EventHandler(this.cmbFirstDayOfWeek_SelectedIndexChanged);
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(8, 40);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(104, 16);
			this.label3.TabIndex = 7;
			this.label3.Text = "First Day of Week:";
			// 
			// chkSaturday
			// 
			this.chkSaturday.Location = new System.Drawing.Point(200, 16);
			this.chkSaturday.Name = "chkSaturday";
			this.chkSaturday.Size = new System.Drawing.Size(32, 16);
			this.chkSaturday.TabIndex = 6;
			this.chkSaturday.Text = "S";
			this.chkSaturday.CheckedChanged += new System.EventHandler(this.WorkWeekDayCheck_Click);
			// 
			// chkFriday
			// 
			this.chkFriday.Checked = true;
			this.chkFriday.CheckState = System.Windows.Forms.CheckState.Checked;
			this.chkFriday.Location = new System.Drawing.Point(168, 16);
			this.chkFriday.Name = "chkFriday";
			this.chkFriday.Size = new System.Drawing.Size(32, 16);
			this.chkFriday.TabIndex = 5;
			this.chkFriday.Text = "F";
			this.chkFriday.CheckedChanged += new System.EventHandler(this.WorkWeekDayCheck_Click);
			// 
			// chkThursday
			// 
			this.chkThursday.Checked = true;
			this.chkThursday.CheckState = System.Windows.Forms.CheckState.Checked;
			this.chkThursday.Location = new System.Drawing.Point(136, 16);
			this.chkThursday.Name = "chkThursday";
			this.chkThursday.Size = new System.Drawing.Size(32, 16);
			this.chkThursday.TabIndex = 4;
			this.chkThursday.Text = "T";
			this.chkThursday.CheckedChanged += new System.EventHandler(this.WorkWeekDayCheck_Click);
			// 
			// chkWednesday
			// 
			this.chkWednesday.Checked = true;
			this.chkWednesday.CheckState = System.Windows.Forms.CheckState.Checked;
			this.chkWednesday.Location = new System.Drawing.Point(104, 16);
			this.chkWednesday.Name = "chkWednesday";
			this.chkWednesday.Size = new System.Drawing.Size(32, 16);
			this.chkWednesday.TabIndex = 3;
			this.chkWednesday.Text = "W";
			this.chkWednesday.CheckedChanged += new System.EventHandler(this.WorkWeekDayCheck_Click);
			// 
			// chkTuesday
			// 
			this.chkTuesday.Checked = true;
			this.chkTuesday.CheckState = System.Windows.Forms.CheckState.Checked;
			this.chkTuesday.Location = new System.Drawing.Point(72, 16);
			this.chkTuesday.Name = "chkTuesday";
			this.chkTuesday.Size = new System.Drawing.Size(32, 16);
			this.chkTuesday.TabIndex = 2;
			this.chkTuesday.Text = "T";
			this.chkTuesday.CheckedChanged += new System.EventHandler(this.WorkWeekDayCheck_Click);
			// 
			// chkMonday
			// 
			this.chkMonday.Checked = true;
			this.chkMonday.CheckState = System.Windows.Forms.CheckState.Checked;
			this.chkMonday.Location = new System.Drawing.Point(40, 16);
			this.chkMonday.Name = "chkMonday";
			this.chkMonday.Size = new System.Drawing.Size(32, 16);
			this.chkMonday.TabIndex = 1;
			this.chkMonday.Text = "M";
			this.chkMonday.CheckedChanged += new System.EventHandler(this.WorkWeekDayCheck_Click);
			// 
			// chkSunday
			// 
			this.chkSunday.Location = new System.Drawing.Point(8, 16);
			this.chkSunday.Name = "chkSunday";
			this.chkSunday.Size = new System.Drawing.Size(32, 16);
			this.chkSunday.TabIndex = 0;
			this.chkSunday.Text = "S";
			this.chkSunday.CheckedChanged += new System.EventHandler(this.WorkWeekDayCheck_Click);
			// 
			// labelPane
			// 
			this.labelPane.BackColor = System.Drawing.SystemColors.ControlDark;
			this.labelPane.Controls.AddRange(new System.Windows.Forms.Control[] {
																					this.lblDate,
																					this.lblCalendar});
			this.labelPane.Dock = System.Windows.Forms.DockStyle.Top;
			this.labelPane.Location = new System.Drawing.Point(0, 135);
			this.labelPane.Name = "labelPane";
			this.labelPane.Size = new System.Drawing.Size(808, 24);
			this.labelPane.TabIndex = 4;
			// 
			// lblDate
			// 
			this.lblDate.Anchor = ((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right);
			this.lblDate.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(177)));
			this.lblDate.ForeColor = System.Drawing.SystemColors.HighlightText;
			this.lblDate.Location = new System.Drawing.Point(216, 0);
			this.lblDate.Name = "lblDate";
			this.lblDate.Size = new System.Drawing.Size(584, 16);
			this.lblDate.TabIndex = 1;
			this.lblDate.Text = "Date";
			this.lblDate.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// lblCalendar
			// 
			this.lblCalendar.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(178)));
			this.lblCalendar.ForeColor = System.Drawing.SystemColors.HighlightText;
			this.lblCalendar.Name = "lblCalendar";
			this.lblCalendar.Size = new System.Drawing.Size(96, 24);
			this.lblCalendar.TabIndex = 0;
			this.lblCalendar.Text = "Calendar";
			this.lblCalendar.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// menuItem13
			// 
			this.menuItem13.Index = 6;
			this.menuItem13.Text = "-";
			// 
			// menuShowCustomIcons
			// 
			this.menuShowCustomIcons.Index = 7;
			this.menuShowCustomIcons.Text = "Office 2007 theme: Show Custom Icons Example ";
			this.menuShowCustomIcons.Click += new System.EventHandler(this.menuShowCustomIcons_Click);
			// 
			// frmMain
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(808, 489);
			this.Controls.AddRange(new System.Windows.Forms.Control[] {
																		  this.wndCalendarControl,
																		  this.labelPane,
																		  this.sbStatusBar,
																		  this.OptionsPane,
																		  this.toolBar1});
			this.Menu = this.mainMenu1;
			this.Name = "frmMain";
			this.Text = "Calendar Sample";
			this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
			this.Closing += new System.ComponentModel.CancelEventHandler(this.frmMain_Closing);
			this.Load += new System.EventHandler(this.frmMain_Load);
			((System.ComponentModel.ISupportInitialize)(this.wndCalendarControl)).EndInit();
			this.OptionsPane.ResumeLayout(false);
			this.gbxMonthView.ResumeLayout(false);
			this.gbxWeekView.ResumeLayout(false);
			this.gbxDayView.ResumeLayout(false);
			this.gbxCalendarWorkWeek.ResumeLayout(false);
			this.labelPane.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new frmMain());
		}

		private	void UpdateDateLabel()
		{
			if (wndCalendarControl.ActiveView.DaysCount == 1)
			{
				DateTime DateLabel = wndCalendarControl.ActiveView[0].Date;
				lblDate.Text = DateLabel.ToLongDateString();
			}
			else if (wndCalendarControl.ActiveView.DaysCount > 1)
			{
				DateTime DateLabelFrom = wndCalendarControl.ActiveView[0].Date;
				DateTime DateLabelTo = wndCalendarControl.ActiveView[wndCalendarControl.ActiveView.DaysCount - 1].Date;
				lblDate.Text = DateLabelFrom.ToLongDateString() + " - " + DateLabelTo.ToLongDateString();
			}
		
		}

		private void mnuViewOptionsBar_Click(object sender, System.EventArgs e)
		{
			mnuViewOptionsBar.Checked = (mnuViewOptionsBar.Checked ? false : true);
			OptionsPane.Visible = (OptionsPane.Visible ? false : true);
		}

		private void toolBar1_ButtonClick(object sender, System.Windows.Forms.ToolBarButtonClickEventArgs e)
		{
			switch (e.Button.Tag.ToString())
			{
				case "FileNew":
					break;
				case "FileOpen":
					mnuFileOpen_Click(this, new System.EventArgs());
					break;
				case "FileSave":
					mnuFileSave_Click(this, new System.EventArgs());
					break;
				
				case "PrintPageSetup":
					mnuFilePrintPageSetup_Click(this, new System.EventArgs());
					break;
				case "PrintPreview":
					mnuFilePrintPreview_Click(this, new System.EventArgs());
					break;
				case "Print":
					mnuFilePrint_Click(this, new System.EventArgs());
					break;
				
				case "EditUndo":
					break;
				case "EditCut":
					break;
				case "EditCopy":
					break;
				case "EditPaste":
					break;

				case "DayView" :
					mnuCalendarDayView_Click(this, new System.EventArgs());
					break;
				case "WorkWeekView" :
					mnuCalendarWorkWeekView_Click(this, new System.EventArgs());
					break;
				case "WeekView" :
					mnuCalendarWeekView_Click(this, new System.EventArgs());
					break;
				case "MonthView" :
					mnuCalendarMonthView_Click(this, new System.EventArgs());
					break;

				case "DatePicker" :
					mnuDatePicker_Click(this, new System.EventArgs());
					break;
				case "OptionsPane" :
					mnuViewOptionsBar_Click(this, new System.EventArgs());
					break;
				case "HelpAbout" :
					mnuHelpAbout_Click(this, new System.EventArgs());
					break;
			}		
		}

		private void mnuDatePicker_Click(object sender, System.EventArgs e)
		{			
			if (mnuDatePicker.Checked == true) 
			{
				if (frmDatePicker != null) {
					frmDatePicker.Instance.Close();
				}
				mnuDatePicker.Checked = false;
			}
			else
			{
				frmDatePicker = new frmDatePicker();
				
				frmDatePicker.Owner = this;
				frmDatePicker.ShowInTaskbar = false;
				frmDatePicker.Show();	

				UpdateDatePicker();

				mnuDatePicker.Checked = true;
			}
		}

		private void wndCalendarControl_PrePopulate(object sender, AxXtremeCalendarControl._DCalendarControlEvents_PrePopulateEvent e)
		{   			
			if (menuShowCustomIcons.Checked)
			{
				for (int i = 0; i < e.events.Count; i++)
				{
					CalendarEvent pEvent = e.events[i];
					System.Diagnostics.Debug.WriteLine("wndCalendarControl_PrePopulate = " + pEvent.Subject);

					// 1; // unread mail
					// 2; // read mail
					// 3; // replyed mail
					// 4; // attachment
					// 5; // Low priority
					// 6; // HIGH priority
					int nIconID = (i % 5) + 1;
					pEvent.CustomIcons.AddIfNeed(nIconID);
                    					
					nIconID = ((i + nCustomIconCounter) % 5) + 1;					
					pEvent.CustomIcons.AddIfNeed(nIconID);

					nIconID = ((i + nCustomIconCounter * 11) % 5) + 1;					
					pEvent.CustomIcons.AddIfNeed(nIconID);

					nCustomIconCounter++;
				}
			}
			else
			{
				// clean up
				for (int i = 0; i < e.events.Count; i++)
				{
					CalendarEvent pEvent = e.events[i];
					pEvent.CustomIcons.RemoveAll(); 
				}
			}
				
		}

		private String RemoveLastDir(String strPath)
		{
			int nNewLen = strPath.Length;

			if (nNewLen > 0 && (strPath[nNewLen-1] == '\\' || strPath[nNewLen-1] == '/'))
			{
				nNewLen--;
			}

			while(nNewLen > 0 && strPath[nNewLen-1] != '\\' && strPath[nNewLen-1] != '/')
			{
				nNewLen--;
			}
			return strPath.Substring(0, nNewLen);
			
		}

		private void menuShowCustomIcons_Click(object sender, System.EventArgs e)
		{
            this.wndCalendarControl.PrePopulate += new AxXtremeCalendarControl._DCalendarControlEvents_PrePopulateEventHandler(this.wndCalendarControl_PrePopulate);


			CalendarThemeOffice2007 pTheme2007 = (CalendarThemeOffice2007)wndCalendarControl.Theme;
			if (pTheme2007 != null) 
			{
				menuShowCustomIcons.Checked = !menuShowCustomIcons.Checked;
                
				System.Array arCustIconsIDs = System.Array.CreateInstance(typeof(Int32), 6);
				arCustIconsIDs.SetValue(1, 0); // unread mail
				arCustIconsIDs.SetValue(2, 1); // read mail
				arCustIconsIDs.SetValue(3, 2); // replyed mail
				arCustIconsIDs.SetValue(4, 3); // attachment
				arCustIconsIDs.SetValue(5, 4); // Low priority
				arCustIconsIDs.SetValue(6, 5); // HIGH priority
			
				pTheme2007.CustomIcons.RemoveAll();
				String strPath = Application.StartupPath;
				strPath = RemoveLastDir(strPath);
				strPath = RemoveLastDir(strPath);
				strPath += "Icons\\EventCustomIcons.bmp";
				pTheme2007.CustomIcons.LoadBitmap(strPath, arCustIconsIDs, XTPImageState.xtpImageNormal);
			}
			wndCalendarControl.Populate();			
		}

		private void frmMain_Load(object sender, System.EventArgs e)
		{
			mnuEnableReminders_Click(this, new System.EventArgs());
		}

		private void mnuHelpAbout_Click(object sender, System.EventArgs e)
		{
			wndCalendarControl.AboutBox();
		}

		private void mnuFileSave_Click(object sender, System.EventArgs e)
		{
			if (wndCalendarControl.DataProvider.IsOpen)
			{
				wndCalendarControl.DataProvider.Save();
			}
		}

		private void mnuViewRemindersWindow_Click(object sender, System.EventArgs e)
		{
			objCalendraDialogsReminders.ShowRemindersWindow();
		}

		private void mnuEnableReminders_Click(object sender, System.EventArgs e)
		{			
			mnuEnableReminders.Checked = !mnuEnableReminders.Checked;
            if (mnuEnableReminders.Checked)
			{
                objCalendraDialogsReminders.Calendar = (XtremeCalendarControl.CalendarControl)wndCalendarControl.GetOcx();
                //objCalendraDialogsReminders.ParentHWND = Me.Handle.ToInt32()
                objCalendraDialogsReminders.RemindersWindowShowInTaskBar = true;
                objCalendraDialogsReminders.CreateRemindersWindow();
            }
			else
			{
                objCalendraDialogsReminders.CloseRemindersWindow();
            }
            
			wndCalendarControl.EnableReminders(mnuEnableReminders.Checked);
            
			mnuViewRemindersWindow.Enabled = mnuEnableReminders.Checked;
		}

		private void mnuThemeOffice2007_Click(object sender, System.EventArgs e)
		{
			CalendarThemeOffice2007 pTheme2007;
			pTheme2007 = (CalendarThemeOffice2007)wndCalendarControl.Theme;
			if (pTheme2007 == null)
			{
				pTheme2007 = new CalendarThemeOffice2007();
				wndCalendarControl.SetTheme(pTheme2007);
				
				mnuThemeOffice2007.Checked = true;
			}
			else
			{			
				wndCalendarControl.SetTheme(null);
				mnuThemeOffice2007.Checked = false;
			}
			wndCalendarControl.Populate();
		}

		private void mnuFilePrintPageSetup_Click(object sender, System.EventArgs e)
		{
			wndCalendarControl.ShowPrintPageSetup();
		}

		private void mnuFilePrintPreview_Click(object sender, System.EventArgs e)
		{
			wndCalendarControl.PrintPreview(true);
		}

		private void mnuFilePrint_Click(object sender, System.EventArgs e)
		{
			wndCalendarControl.PrintCalendar2(true);
		}

		private void wndCalendarControl_BeforeDrawDayViewCell(object sender, AxXtremeCalendarControl._DCalendarControlEvents_BeforeDrawDayViewCellEvent e)
		{
			// standard colors are
			// non-work cell Bk = RGB(255, 244, 188)
			// work cell Bk = RGB(255, 255, 213)

			if (e.cellParams.Selected)
			{
				//e.cellParams.BackgroundColor = (uint)System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.FromArgb(20, 250, 50));
				return;
			}

			if (e.cellParams.BeginTime.Hour >= 13 && e.cellParams.BeginTime.Hour < 14 &&
				e.cellParams.BeginTime.DayOfWeek != System.DayOfWeek.Saturday &&
				e.cellParams.BeginTime.DayOfWeek != System.DayOfWeek.Sunday)
			{
				e.cellParams.BackgroundColor = (uint)System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.FromArgb(198, 198, 198));
			}
		}

		private void frmMain_Closing(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if(wndCalendarControl.DataProvider.IsOpen)
			{
				wndCalendarControl.DataProvider.Save();
				//wndCalendarControl.DataProvider.Close();
			}
		}

		private void mnuFileOpen_Click(object sender, System.EventArgs e)
		{
			frmOpenDataProvider frmOpenDP = new frmOpenDataProvider();
			System.Windows.Forms.DialogResult nDlgRes = frmOpenDP.ShowDialog(this);
			if (nDlgRes != System.Windows.Forms.DialogResult.OK) {
				return;
			}
			int eDPType = frmOpenDP.GetDataProviderType();			
			Boolean bOpened = false;
			m_objSQLProvider = null;

			if(eDPType == (int)DataProviderTypeCustom.dpSQLServer)
			{
				bOpened = OpenSQLServerDataProvider(frmOpenDP.GetConnectionString());
			}
			else
			{
				bOpened = OpenBuiltInDataProvider(frmOpenDP.GetConnectionString(), eDPType);
			}

			if (!bOpened) 
			{
				// set memory data provider
				wndCalendarControl.SetDataProvider("");
				wndCalendarControl.DataProvider.Open();
			}

			// Udate controls
			wndCalendarControl.Populate();			
			UpdateDatePicker();

			this.Text = "Codejock Calendar C# Sample.  " + GetDPTitle(eDPType);
		}

		private String GetDPTitle(int eDPType)
		{
			if (eDPType == (int)DataProviderType.dpMemory)
			{
				return "[Memory Data Provider]: " + wndCalendarControl.DataProvider.DataSource;
			}
			else if (eDPType == (int)DataProviderType.dpDB)
			{
				return "[DB (Access) Data Provider]: " + wndCalendarControl.DataProvider.DataSource;
			}
			else if (eDPType == (int)DataProviderType.dpMAPI)
			{
				return "[MAPI Data Provider]. ";
			}
			else if (eDPType == (int)DataProviderTypeCustom.dpSQLServer)
			{
				return "[SQL Server custom Data Provider]: " + m_objSQLProvider.GetConnectionString();
			}
			
			return "";
		}

		private Boolean OpenSQLServerDataProvider(String strConnectionString)
		{			
			try
			{
				m_objSQLProvider = new providerSQLServer(wndCalendarControl);
				m_objSQLProvider.Open(strConnectionString);

				// close previous data provider
				if(wndCalendarControl.DataProvider != null && wndCalendarControl.DataProvider.IsOpen)
				{
					wndCalendarControl.DataProvider.Close();
				}

				// set new custom data provider
				wndCalendarControl.SetDataProvider("Provider=Custom;");
				wndCalendarControl.DataProvider.Open();

				wndCalendarControl.DataProvider.CacheMode = CalendarDataProviderCacheMode.xtpCalendarDPCacheModeOnRepeat;
				//wndCalendarControl.DataProvider.CacheMode = CalendarDataProviderCacheMode.xtpCalendarDPCacheModeOff;

				return true;
			}
			catch(Exception ex)
			{
				System.Windows.Forms.MessageBox.Show("Cannot connect to server. \n Error: \n " + ex.Message);
			}
			return false;
		}

		private Boolean OpenBuiltInDataProvider(String strConnectionString, int eDPType)
		{
			// close previous data provider
			if(wndCalendarControl.DataProvider != null && wndCalendarControl.DataProvider.IsOpen)
			{
				wndCalendarControl.DataProvider.Save();
				wndCalendarControl.DataProvider.Close();
			}

			// set new custom data provider
			wndCalendarControl.SetDataProvider(strConnectionString);

			Boolean bOpened = wndCalendarControl.DataProvider.Open();

			if (bOpened)
			{
				return true;
			}
			
			if (eDPType == (int)DataProviderType.dpMAPI)
			{
				System.Windows.Forms.MessageBox.Show("Cannot open MAPI data provider.");

				return false;
			}
			
			System.Windows.Forms.DialogResult nDlgRes = System.Windows.Forms.MessageBox.Show(
				"Cannot open specified data source. \nWould You like to recreate it?", 
				"Calendar Control",
				System.Windows.Forms.MessageBoxButtons.YesNo);
				
			if (nDlgRes != System.Windows.Forms.DialogResult.Yes) 
			{
				return false;
			}

			Boolean bCreated = wndCalendarControl.DataProvider.Create();
				
			if (!bCreated) 
			{
				System.Windows.Forms.MessageBox.Show(
					"Cannot create specified data source:\n " + wndCalendarControl.DataProvider.DataSource); 
				return false;
			}

			nDlgRes = System.Windows.Forms.MessageBox.Show(
						"Would You like to add default test events set?", 
						"Calendar Control",
						System.Windows.Forms.MessageBoxButtons.YesNo);

			if (nDlgRes == System.Windows.Forms.DialogResult.Yes) 
			{
				AddTestEvents(false);							
			}
			
			return true;
		}

		private void mnuViewToolBar_Click(object sender, System.EventArgs e)
		{
			mnuViewToolBar.Checked = (mnuViewToolBar.Checked ? false : true);
			toolBar1.Visible = (toolBar1.Visible ? false : true);
		}

		private void mnuViewStatusBar_Click(object sender, System.EventArgs e)
		{
			mnuViewStatusBar.Checked = (mnuViewStatusBar.Checked ? false : true);
			sbStatusBar.Visible = (sbStatusBar.Visible ? false : true);
		}

		private DateTime GetStartDayTime()
		{
			DateTime BeginSelection, EndSelection;
			Boolean AllDay;
			
			BeginSelection = EndSelection = DateTime.Now;
			AllDay = false;

			if (wndCalendarControl.ActiveView.GetSelection(ref BeginSelection, ref EndSelection, ref AllDay) == false)
			{
				BeginSelection = DateTime.Now;
			}
			
			return BeginSelection;
		}

		private DateTime GetMonday(DateTime dtDate)
		{
			int Day;
			Day = (int)dtDate.DayOfWeek;

			if (Day >= 1 & Day <= 5)
			{
				Day = Day - (int)DayOfWeek.Monday;
				dtDate = dtDate.AddDays(-Day);
			}
			else if (Day == 6)
			{
				dtDate = dtDate.AddDays(2);
			}
			else if (Day == 0)
			{
				dtDate = dtDate.AddDays(1);
			}

			return dtDate;

		}

		public void CheckViewItems()
		{
			mnuCalendarDayView.Checked = (wndCalendarControl.ViewType == CalendarViewType.xtpCalendarDayView);
			mnuCalendarWorkWeekView.Checked = (wndCalendarControl.ViewType == CalendarViewType.xtpCalendarWorkWeekView);
			mnuCalendarWeekView.Checked = (wndCalendarControl.ViewType == CalendarViewType.xtpCalendarWeekView);
			mnuCalendarMonthView.Checked = (wndCalendarControl.ViewType == CalendarViewType.xtpCalendarMonthView);

			tbrDayView.Pushed = mnuCalendarDayView.Checked;
			tbrWorkWeekView.Pushed = mnuCalendarWorkWeekView.Checked;
			tbrWeekView.Pushed = mnuCalendarWeekView.Checked;
			tbrMonthView.Pushed = mnuCalendarMonthView.Checked;
		}

		private void UpdateDatePicker()
		{
			DateTime StartRange, EndRange;

			if (frmDatePicker != null)
			{
				wndCalendarControl.ActiveView.GetSelection(ref BeginSelection, ref EndSelection, ref AllDay);
				StartRange = BeginSelection;
				EndRange = EndSelection;

				if (wndCalendarControl.ActiveView.DaysCount == 1)
				{
					StartRange = wndCalendarControl.ActiveView[0].Date;
					EndRange = StartRange;
				}
				else if (wndCalendarControl.ActiveView.DaysCount > 1)
				{
					StartRange = wndCalendarControl.ActiveView[0].Date;
					EndRange = wndCalendarControl.ActiveView[wndCalendarControl.ActiveView.DaysCount - 1].Date;
				}

				frmDatePicker.Instance.DatePicker.SelectRange(StartRange, EndRange);

				frmDatePicker.Instance.DatePicker.Invalidate();
				frmDatePicker.Instance.DatePicker.Update();
			}
		}

		private void mnuCalendarDayView_Click(object sender, System.EventArgs e)
		{	
			if (wndCalendarControl.ViewType != CalendarViewType.xtpCalendarDayView)
			{
				wndCalendarControl.ViewType =  CalendarViewType.xtpCalendarDayView;
			}

			UpdateDatePicker();
		}

		private void mnuCalendarWorkWeekView_Click(object sender, System.EventArgs e)
		{
			if(wndCalendarControl.ViewType !=  CalendarViewType.xtpCalendarWorkWeekView)
			{
				wndCalendarControl.ViewType =  CalendarViewType.xtpCalendarWorkWeekView;																		
			}

			UpdateDatePicker();
		}

		private void mnuCalendarWeekView_Click(object sender, System.EventArgs e)
		{
			if (wndCalendarControl.ViewType != CalendarViewType.xtpCalendarWeekView)
			{
				wndCalendarControl.ViewType = CalendarViewType.xtpCalendarWeekView;
			}

			UpdateDatePicker();
		}

		private void mnuCalendarMonthView_Click(object sender, System.EventArgs e)
		{
			if(wndCalendarControl.ViewType != CalendarViewType.xtpCalendarMonthView)
			{
				wndCalendarControl.ViewType =  CalendarViewType.xtpCalendarMonthView;
			}

			UpdateDatePicker();
		}

		private void mnuCalendar60Min_Click(object sender, System.EventArgs e)
		{
			CheckTimeScaleItems(NewTimeScale.Hour);
			UpdateTimeScaleCombo(NewTimeScale.Hour);
			wndCalendarControl.DayView.TimeScale = 60;
		}

		private void mnuCalendar30Min_Click(object sender, System.EventArgs e)
		{
			CheckTimeScaleItems(NewTimeScale.HalfHour);
			UpdateTimeScaleCombo(NewTimeScale.HalfHour);
			wndCalendarControl.DayView.TimeScale = 30;
		}

		private void mnuCalendar15Min_Click(object sender, System.EventArgs e)
		{
			CheckTimeScaleItems(NewTimeScale.QuarterHour);
			UpdateTimeScaleCombo(NewTimeScale.QuarterHour);
			wndCalendarControl.DayView.TimeScale = 15;
		}

		private void mnuCalendar10Min_Click(object sender, System.EventArgs e)
		{
			CheckTimeScaleItems(NewTimeScale.TenMin);
			UpdateTimeScaleCombo(NewTimeScale.TenMin);
			wndCalendarControl.DayView.TimeScale = 10;
		}

		private void mnuCalendar6Min_Click(object sender, System.EventArgs e)
		{
			CheckTimeScaleItems(NewTimeScale.SixMin);
			UpdateTimeScaleCombo(NewTimeScale.SixMin);
			wndCalendarControl.DayView.TimeScale = 6;
		}

		private void mnuCalendar5Min_Click(object sender, System.EventArgs e)
		{	
			CheckTimeScaleItems(NewTimeScale.FiveMin);
			UpdateTimeScaleCombo(NewTimeScale.FiveMin);
			wndCalendarControl.DayView.TimeScale = 5;
		}

		enum NewTimeScale
		{
			Hour = 1,
			HalfHour = 2,
			QuarterHour = 4,
			TenMin = 8,
			SixMin = 16,
			FiveMin = 32
		};

		private void CheckTimeScaleItems(NewTimeScale CurrentTimeScale)
		{
			mnuCalendar60Min.Checked = ((CurrentTimeScale & NewTimeScale.Hour) == NewTimeScale.Hour);
			mnuCalendar30Min.Checked = ((CurrentTimeScale & NewTimeScale.HalfHour) == NewTimeScale.HalfHour);
			mnuCalendar15Min.Checked = ((CurrentTimeScale & NewTimeScale.QuarterHour) == NewTimeScale.QuarterHour);
			mnuCalendar10Min.Checked = ((CurrentTimeScale & NewTimeScale.TenMin) == NewTimeScale.TenMin);
			mnuCalendar6Min.Checked = ((CurrentTimeScale & NewTimeScale.SixMin) == NewTimeScale.SixMin);
			mnuCalendar5Min.Checked = ((CurrentTimeScale & NewTimeScale.FiveMin) == NewTimeScale.FiveMin);
		}

		private void UpdateTimeScaleCombo(NewTimeScale CurrentTimeScale)
		{
			switch(CurrentTimeScale) 
			{
				case NewTimeScale.Hour:
					cmbTimeScale.SelectedIndex = 0;
					break;
				case NewTimeScale.HalfHour:
					cmbTimeScale.SelectedIndex = 1;
					break;
				case NewTimeScale.QuarterHour:
					cmbTimeScale.SelectedIndex = 2;
					break;
				case NewTimeScale.TenMin:
					cmbTimeScale.SelectedIndex = 3;
					break;
				case NewTimeScale.SixMin:
					cmbTimeScale.SelectedIndex = 4;
					break;
				case NewTimeScale.FiveMin:
					cmbTimeScale.SelectedIndex = 5;
					break;
			}
		}

		private void cmbTimeScale_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			switch(cmbTimeScale.SelectedIndex) 
			{
				case 0:
					mnuCalendar60Min_Click(this, new System.EventArgs());
					break;
				case 1:
					mnuCalendar30Min_Click(this, new System.EventArgs());
					break;
				case 2:
					mnuCalendar15Min_Click(this, new System.EventArgs());
					break;
				case 3:
					mnuCalendar10Min_Click(this, new System.EventArgs());
					break;
				case 4:
					mnuCalendar6Min_Click(this, new System.EventArgs());
					break;
				case 5:
					mnuCalendar5Min_Click(this, new System.EventArgs());
					break;
			}
			wndCalendarControl.Populate();
		}

		private void CalendarControl_ViewChanged(object sender, System.EventArgs e)
		{
			UpdateDateLabel();

			CheckViewItems();
		}

		private void chkDayShowTimeAsClocks_CheckedChanged(object sender, System.EventArgs e)
		{
			wndCalendarControl.Options.WeekViewShowTimeAsClocks = (wndCalendarControl.Options.WeekViewShowTimeAsClocks ? false : true);
			wndCalendarControl.Populate();
		}

		private void chkDayShowEndTime_CheckedChanged(object sender, System.EventArgs e)
		{
			wndCalendarControl.Options.WeekViewShowEndDate = (wndCalendarControl.Options.WeekViewShowEndDate ? false : true);		
			wndCalendarControl.Populate();
		}

		private void chkMonthShowTimeAsClocks_CheckedChanged(object sender, System.EventArgs e)
		{
			wndCalendarControl.Options.MonthViewShowTimeAsClocks = (wndCalendarControl.Options.MonthViewShowTimeAsClocks ? false : true);				
			wndCalendarControl.Populate();
		}

		private void chkMonthShowEndTime_CheckedChanged(object sender, System.EventArgs e)
		{
			wndCalendarControl.Options.MonthViewShowEndDate = (wndCalendarControl.Options.MonthViewShowEndDate ? false : true);				
			wndCalendarControl.Populate();
		}

		private void chkCompressWeekendDays_CheckedChanged(object sender, System.EventArgs e)
		{
			wndCalendarControl.Options.MonthViewCompressWeekendDays = (wndCalendarControl.Options.MonthViewCompressWeekendDays ? false : true);				
			wndCalendarControl.Populate();
		}

		private void WorkWeekDayCheck_Click(object sender, System.EventArgs e)
		{
			CalendarWeekDay WeekMask = 0;
			WeekMask = (chkSunday.Checked ? (WeekMask | CalendarWeekDay.xtpCalendarDaySunday) : WeekMask );
			WeekMask = (chkMonday.Checked ? (WeekMask | CalendarWeekDay.xtpCalendarDayMonday) : WeekMask );
			WeekMask = (chkTuesday.Checked ? (WeekMask | CalendarWeekDay.xtpCalendarDayTuesday) : WeekMask );
			WeekMask = (chkWednesday.Checked ? (WeekMask | CalendarWeekDay.xtpCalendarDayWednesday) : WeekMask );
			WeekMask = (chkThursday.Checked ? (WeekMask | CalendarWeekDay.xtpCalendarDayThursday) : WeekMask );
			WeekMask = (chkFriday.Checked ? (WeekMask | CalendarWeekDay.xtpCalendarDayFriday) : WeekMask );
			WeekMask = (chkSaturday.Checked ? (WeekMask | CalendarWeekDay.xtpCalendarDaySaturday) : WeekMask );

			wndCalendarControl.Options.WorkWeekMask = WeekMask;
			wndCalendarControl.Populate();

			if (frmDatePicker != null)
			{
				frmDatePicker.Instance.DatePicker.FirstDayOfWeek = cmbFirstDayOfWeek.SelectedIndex + 1;
			}
		}

		private void cmbFirstDayOfWeek_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			wndCalendarControl.Options.FirstDayOfTheWeek = (int)cmbFirstDayOfWeek.SelectedItem;
			wndCalendarControl.Populate();
		}

		private void cmbWeeksCount_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if(wndCalendarControl.MonthView.WeeksCount != (int)cmbWeeksCount.SelectedItem)
			{
				wndCalendarControl.MonthView.WeeksCount = (int)cmbWeeksCount.SelectedItem;
				wndCalendarControl.Populate();
			}
		}

		private void cmbStartTime_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			//18000000000 Ticks = 30 Minutes
			DateTime StartTime = new DateTime(cmbStartTime.SelectedIndex * 18000000000);

			wndCalendarControl.Options.WorkDayStartTime = StartTime;

			if (cmbStartTime.SelectedIndex > cmbEndTime.SelectedIndex)
			{
				cmbEndTime.SelectedIndex = cmbStartTime.SelectedIndex;
			}
			wndCalendarControl.Populate();
		}

		private void cmbEndTime_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			//18000000000 Ticks = 30 Minutes
			DateTime EndTime = new DateTime(cmbEndTime.SelectedIndex * 18000000000);

			wndCalendarControl.Options.WorkDayEndTime = EndTime;

			if (cmbEndTime.SelectedIndex < cmbStartTime.SelectedIndex)
			{
				cmbStartTime.SelectedIndex = cmbEndTime.SelectedIndex;
			}
			wndCalendarControl.Populate();		
		}

		private void AddTestEvents(Boolean bCreateDefaultProvider)
		{
			CalendarDataProvider pCalendarData = wndCalendarControl.DataProvider;

			if (bCreateDefaultProvider)
			{
				wndCalendarControl.SetDataProvider(ConnectionString + @"\TestEvents.xml");
				pCalendarData = wndCalendarControl.DataProvider;
				if (!pCalendarData.Create()) 
				{
					wndCalendarControl.SetDataProvider("");
					wndCalendarControl.DataProvider.Open();
				}
			}
			
			//Creates TestEvents.xml file
			//System.Diagnostics.Debug.Assert(pCalendarData == null, "Can not create Data Provider for Events");

			/*
			if (wndCalendarControl.DataProvider.Create())	
			{
				System.Diagnostics.Debug.Assert(false, "Could not access data provider for calendar control");
				return;
			}
		*/

			// add test events
			DateTime dtNow = new DateTime(DateTime.Now.Ticks);

			// Create 4 normal events

			// Normal Event 1
			CalendarEvent ptrEvent = pCalendarData.CreateEvent();
			ptrEvent.StartTime = dtNow;
			ptrEvent.EndTime = dtNow.AddHours(1);
			ptrEvent.Subject = "Event &Now [Tentative Meeting]";
			ptrEvent.Location = "MSB - Room 204";
			//ptrEvent->SetReminderMinutesBeforeStart(10);
			ptrEvent.BusyStatus = CalendarEventBusyStatus.xtpCalendarBusyStatusTentative;
			ptrEvent.MeetingFlag = true;
			pCalendarData.AddEvent(ptrEvent);

			// Normal Event 2
			dtNow = DateTime.Now;
			ptrEvent = pCalendarData.CreateEvent();
			ptrEvent.StartTime = dtNow.AddMinutes(15);
			dtNow = dtNow.AddHours(4);
			dtNow = dtNow.AddMinutes(15);
			ptrEvent.EndTime = dtNow;
			ptrEvent.Subject = "Subject - Event Now + 15 min (4 hours)";
			ptrEvent.Location = "Location - [Free Time - Office Hours]";
			ptrEvent.BusyStatus = CalendarEventBusyStatus.xtpCalendarBusyStatusFree;
			ptrEvent.Importance = CalendarEventImportance.xtpCalendarImportanceNormal;
			ptrEvent.Label = (int)Label.Birthday;		
			pCalendarData.AddEvent(ptrEvent);

			// Normal Event 3
			dtNow = DateTime.Now;
			ptrEvent = pCalendarData.CreateEvent();
			ptrEvent.StartTime = dtNow.AddHours(1);
			ptrEvent.EndTime = dtNow.AddHours(2);
			ptrEvent.Subject = "Subject - Event Now + Hour (2 hours)";
			ptrEvent.Location = "Location - [OutOfOffice]";
			ptrEvent.BusyStatus = CalendarEventBusyStatus.xtpCalendarBusyStatusOutOfOffice;
			ptrEvent.Importance = CalendarEventImportance.xtpCalendarImportanceHigh;	
			ptrEvent.PrivateFlag = true;
			ptrEvent.Label = (int)Label.Important;
			pCalendarData.AddEvent(ptrEvent);

			// Normal Event 4
			dtNow = DateTime.Now;
			ptrEvent = pCalendarData.CreateEvent();
			dtNow = dtNow.AddDays(-1);
			dtNow = dtNow.AddHours(1);
			ptrEvent.StartTime = dtNow;
			ptrEvent.EndTime = dtNow.AddHours(3);
			ptrEvent.Subject = "Subject - Yesterday Event";
			ptrEvent.Location = "Location - [Old Location]";
			ptrEvent.BusyStatus = CalendarEventBusyStatus.xtpCalendarBusyStatusTentative;
			ptrEvent.Importance = CalendarEventImportance.xtpCalendarImportanceLow;	
			ptrEvent.Label = (int)Label.Business;
			pCalendarData.AddEvent(ptrEvent);

			// multi day event
			dtNow = DateTime.Now;
			ptrEvent = pCalendarData.CreateEvent();
			dtNow = dtNow.AddDays(-2);
			dtNow = dtNow.AddHours(1);
			ptrEvent.StartTime = dtNow;
			dtNow = dtNow.AddDays(3);
			ptrEvent.EndTime = dtNow;
			ptrEvent.Subject = "Subject - Multi day Event";
			ptrEvent.Location = "Location - [Multi day Location]";
			ptrEvent.Label = (int)Label.MustAttend;
			//ptrEvent->SetReminderMinutesBeforeStart(10);
			pCalendarData.AddEvent(ptrEvent);

			// "pure" all day event 
			ptrEvent = pCalendarData.CreateEvent();
			dtNow = DateTime.Now;
			ptrEvent.StartTime = dtNow;
			dtNow = dtNow.AddDays(10);
			ptrEvent.EndTime = dtNow;
			ptrEvent.Subject = "Subject - AllDay Event";
			ptrEvent.Location = "Location - [Old Location]";
			ptrEvent.AllDayEvent = true;
			pCalendarData.AddEvent(ptrEvent);

			// private event 	
			dtNow = DateTime.Now;
			ptrEvent = pCalendarData.CreateEvent();
			dtNow = dtNow.AddHours(-3);
			ptrEvent.StartTime = dtNow;
			dtNow = dtNow.AddHours(2);
			ptrEvent.EndTime = dtNow;
			ptrEvent.Subject = "Subject - Private";
			ptrEvent.Location = "Location - [Old Location]";
			ptrEvent.Label = (int)Label.PhoneCall;
			//ptrEvent->SetReminderMinutesBeforeStart(10);
			ptrEvent.PrivateFlag = true;
			ptrEvent.MeetingFlag = true;
			pCalendarData.AddEvent(ptrEvent);

			// Create recurrent event
			dtNow = DateTime.Now;
			CalendarEvent ptrMasterEvent = pCalendarData.CreateEvent();
			//Do Not set start and end times for master event
			//ptrMasterEvent.StartTime = dtNow;
			//dtNow = dtNow.AddHours(1);
			//ptrMasterEvent.EndTime = dtNow;
			ptrMasterEvent.Subject = "Subject - Recurrence event - lunch";
			ptrMasterEvent.Location = "Location - [Recurrence location]";
			ptrMasterEvent.Label = (int)Label.MustAttend;
			//ptrMasterEvent.AllDayEvent = true;
			//ptrMasterEvent->SetReminderMinutesBeforeStart(10);
			ptrMasterEvent.MeetingFlag = true;

			CalendarRecurrencePattern ptrRecPatt = ptrMasterEvent.CreateRecurrence();	
			DateTime dtStart = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 13, 8, 0);
			DateTime dtRecurencePattern_Start = new DateTime(DateTime.Now.Ticks);
			//  duration
			ptrRecPatt.StartTime = dtStart;
			ptrRecPatt.DurationMinutes = 90;
			//  period
			dtRecurencePattern_Start = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
			ptrRecPatt.StartDate = dtRecurencePattern_Start;

			dtRecurencePattern_Start = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 28);
			ptrRecPatt.EndDate = dtRecurencePattern_Start;
			//ptrRecPatt.EndMethod = CalendarPatternEnd.xtpCalendarPatternEndNoDate;
			// pattern
			//CalendarRecurrencePatternOptions RecurenceOptions = ptrRecPatt.Options;
			ptrRecPatt.Options.RecurrenceType = CalendarRecurrenceType.xtpCalendarRecurrenceDaily;

			//Daily Recurrence Options
			ptrRecPatt.Options.DailyEveryWeekDayOnly = false;
			ptrRecPatt.Options.DailyIntervalDays = 3;

			//ptrRecPatt.Options = RecurenceOptions;
			ptrMasterEvent.UpdateRecurrence(ptrRecPatt);
			pCalendarData.AddEvent(ptrMasterEvent);


			//Monthly Nth
			ptrMasterEvent = wndCalendarControl.DataProvider.CreateEvent();
			ptrRecPatt = ptrMasterEvent.CreateRecurrence();
			ptrMasterEvent.Subject = "Monthly Nth Recurrence";
			ptrMasterEvent.Location = "Park";
			//Do Not set start and end times for master event
			//ptrMasterEvent.StartTime = DateTime.Now;
			//ptrMasterEvent.EndTime = DateTime.Now.AddHours(3);
			ptrMasterEvent.Label = (int)Label.Personal;

			ptrRecPatt.StartTime = new DateTime(DateTime.Now.Ticks);
			ptrRecPatt.DurationMinutes = 90;
			ptrRecPatt.StartDate = new DateTime(DateTime.Now.Ticks);
			ptrRecPatt.EndDate = ptrRecPatt.StartDate.AddYears(6);

			ptrRecPatt.Options.RecurrenceType = CalendarRecurrenceType.xtpCalendarRecurrenceMonthNth;
			ptrRecPatt.Options.MonthNthIntervalMonths = 1;
			ptrRecPatt.Options.MonthNthWhichDay = 3;
			ptrRecPatt.Options.MonthNthWhichDayMask = CalendarWeekDay.xtpCalendarDayFriday;

            ptrMasterEvent.UpdateRecurrence(ptrRecPatt);
            pCalendarData.AddEvent(ptrMasterEvent);
            
            //Yearly Nth
			ptrMasterEvent = wndCalendarControl.DataProvider.CreateEvent();
			ptrRecPatt = ptrMasterEvent.CreateRecurrence();
			ptrMasterEvent.Subject = "Yearly Nth Recurrence - Anniverserary";
			ptrMasterEvent.Location = "Nice Restaurant";
			//Do Not set start and end times for master event
			//ptrMasterEvent.StartTime = DateTime.Now.AddHours(-5);
			//ptrMasterEvent.EndTime = DateTime.Now.AddHours(-2);
			ptrMasterEvent.Label = (int)Label.Anniverserary;

			ptrRecPatt.StartTime = new DateTime(DateTime.Now.Ticks);
			ptrRecPatt.DurationMinutes = 270;
			ptrRecPatt.StartDate = new DateTime(DateTime.Now.Ticks).AddYears(-2);
			ptrRecPatt.EndDate = ptrRecPatt.StartDate.AddYears(6);

			ptrRecPatt.Options.RecurrenceType = CalendarRecurrenceType.xtpCalendarRecurrenceYearNth;
			ptrRecPatt.Options.YearNthMonthOfYear = DateTime.Now.Month;
			ptrRecPatt.Options.YearNthWhichDay = 2;
			ptrRecPatt.Options.YearNthWhichDayMask = (int)CalendarWeekDay.xtpCalendarDayTuesday;

			ptrMasterEvent.UpdateRecurrence(ptrRecPatt);
			pCalendarData.AddEvent(ptrMasterEvent);



			//Saves events to TestEvents.xml file
			pCalendarData.Save();
			wndCalendarControl.Populate();
		}

		private void mnuContextNewEvent_Click(object sender, System.EventArgs e)
		{
			frmEventProperties = new frmEventProperties();
				
			frmEventProperties.CreateNewEvent();

			frmEventProperties.ShowDialog(this);			
		}

		private void mnuContextEditEvent_Click(object sender, System.EventArgs e)
		{
			frmEventProperties = new frmEventProperties();

			if (ContextEvent.RecurrenceState != CalendarEventRecurrenceState.xtpCalendarRecurrenceNotRecurring) 
			{
				frmOpenRecurringItem = new frmOpenRecurringItem();
				frmOpenRecurringItem.SetInformationLabel(ContextEvent.Subject);

				frmOpenRecurringItem.ShowDialog(this);

				if (frmOpenRecurringItem.OpenRecurringItemAnswer == -1) 
				{
					frmEventProperties = null;
					return;
				}				
				
				if (frmOpenRecurringItem.OpenRecurringItemAnswer == 2) 
				{
					ContextEvent = ContextEvent.RecurrencePattern.MasterEvent;
				}
				else 
				{
					ContextEvent = ContextEvent.CloneEvent();

					if (ContextEvent.RecurrenceState == CalendarEventRecurrenceState.xtpCalendarRecurrenceOccurrence) {
						ContextEvent.MakeAsRException();
					}					
				}
			}
				
			frmEventProperties.ModifyEvent(ContextEvent);

			frmEventProperties.ShowDialog(this);	
		}

		
		private void mnuContextDeleteEvent_Click(object sender, System.EventArgs e)
		{
			if(ContextEvent == null) {
				return;
			}
				
			Boolean bDeleted = false;

			if (ContextEvent.RecurrenceState == CalendarEventRecurrenceState.xtpCalendarRecurrenceOccurrence ||
				ContextEvent.RecurrenceState == CalendarEventRecurrenceState.xtpCalendarRecurrenceException) 
			{
				frmOpenRecurringItem = new frmOpenRecurringItem();
				frmOpenRecurringItem.SetForDelete(ContextEvent.Subject);

				frmOpenRecurringItem.ShowDialog(this);

				if (frmOpenRecurringItem.OpenRecurringItemAnswer == -1) 
				{
					ContextEvent = null;
					return;
				}				
				
				if (frmOpenRecurringItem.OpenRecurringItemAnswer == 2) 
				{
					// Series					
					wndCalendarControl.DataProvider.DeleteEvent(ContextEvent.RecurrencePattern.MasterEvent);								
					bDeleted = true;
				}
			}

			if (!bDeleted)
			{
				wndCalendarControl.DataProvider.DeleteEvent(ContextEvent);				
			}
			wndCalendarControl.Populate();
			wndCalendarControl.RedrawControl();			
		}


		private void CalendarControl_MouseMoveEvent(object sender, AxXtremeCalendarControl._DCalendarControlEvents_MouseMoveEvent e)
		{
			CalendarHitTestInfo HitTest;
			HitTest = wndCalendarControl.ActiveView.HitTest();

			System.Diagnostics.Debug.WriteLine("X: " + e.x + " Y: " + e.y);

			if (HitTest.ViewEvent != null) 
			{
				System.Diagnostics.Debug.WriteLine("MouseMove. HitTest = " + HitTest.ViewEvent.Event.Subject);
			}
		}

		private void CalendarControl_MouseDownEvent(object sender, AxXtremeCalendarControl._DCalendarControlEvents_MouseDownEvent e)
		{
			if(e.button == 2)
			{
				CalendarHitTestInfo HitTest;
				HitTest = wndCalendarControl.ActiveView.HitTest();

				Point MousePosition;
				MousePosition = new Point(e.x + wndCalendarControl.Left, e.y + wndCalendarControl.Top);

				if (HitTest.ViewEvent != null) 
				{
					System.Diagnostics.Debug.WriteLine("Right-Clicked On: " + HitTest.ViewEvent.Event.Subject);

					ContextEvent = HitTest.ViewEvent.Event;
					contextMenu.MenuItems[0].Visible = false;
					contextMenu.Show(this, MousePosition);
					contextMenu.MenuItems[0].Visible = true;
				}
				else if(HitTest.HitCode == CalendarHitTestCode.xtpCalendarHitTestDayViewTimeScale)
				{
					ContextMenu ContextPopup = new ContextMenu(new MenuItem[] {this.mnuChangeTimeScalePopup.CloneMenu()});					
					ContextPopup.Show(this, MousePosition);
				}
				else if (HitTest.HitCode != CalendarHitTestCode.xtpCalendarHitTestUnknown)
				{
					contextMenu.MenuItems[1].Visible = false;
					contextMenu.MenuItems[2].Visible = false;
					contextMenu.Show(this, MousePosition);
					contextMenu.MenuItems[1].Visible = true;
					contextMenu.MenuItems[2].Visible = true;
				}
			}
		}

		private void mnuCalendarChangeTimeZone_Click(object sender, System.EventArgs e)
		{
			frmTimeZone = new frmTimeZone();
				
			frmTimeZone.ShowDialog(this);			
		}

		private void cmdTimeZone_Click(object sender, System.EventArgs e)
		{
			mnuCalendarChangeTimeZone_Click(this, new System.EventArgs());
		}

		private void CalendarControl_DblClick(object sender, System.EventArgs e)
		{
			CalendarHitTestInfo HitTest;
			HitTest = wndCalendarControl.ActiveView.HitTest();

			CalendarEvents Events;
			if (HitTest.HitCode != CalendarHitTestCode.xtpCalendarHitTestUnknown)
			{
				Events = wndCalendarControl.DataProvider.RetrieveDayEvents(HitTest.ViewDay.Date);	
			}

			if (HitTest.ViewEvent == null)
			{
				mnuContextNewEvent_Click(this, new System.EventArgs());
			}
			else
			{
				ContextEvent = HitTest.ViewEvent.Event;
				mnuContextEditEvent_Click(this, new System.EventArgs());
			}
		}

		private void mnuDatePicker_Click()
		{
		
		}
		
	}
}
