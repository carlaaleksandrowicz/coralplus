using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using XtremeCalendarControl;

namespace CalendarControl
{
	/// <summary>
	/// Summary description for RecurrenceDialog.
	/// </summary>
	public class frmRecurrenceDialog : System.Windows.Forms.Form
	{
		private System.Windows.Forms.GroupBox gbxAppointmentTime;
		private System.Windows.Forms.Label lblAppointmentStart;
		private System.Windows.Forms.ComboBox cmbAppointmentStartTime;
		private System.Windows.Forms.Label lblAppointmentEndTime;
		private System.Windows.Forms.ComboBox cmbAppointmentEndTime;
		private System.Windows.Forms.Label lblAppointmentduration;
		private System.Windows.Forms.ComboBox cmbAppointmentDuration;
		private System.Windows.Forms.GroupBox gbxRecurrencePattern;
		private System.Windows.Forms.RadioButton optDaily;
		private System.Windows.Forms.RadioButton optWeekly;
		private System.Windows.Forms.RadioButton optMonthly;
		private System.Windows.Forms.RadioButton optYearly;
		private System.Windows.Forms.GroupBox gbxRangeOfRecurrence;
		private System.Windows.Forms.Label lblRangeStart;
		private System.Windows.Forms.DateTimePicker dpStartDate;
		private System.Windows.Forms.RadioButton optNoEndDate;
		private System.Windows.Forms.RadioButton optEndAfter;
		private System.Windows.Forms.RadioButton optEndBy;
		private System.Windows.Forms.TextBox txtOccurences;
		private System.Windows.Forms.Label lblOccurences;
		private System.Windows.Forms.DateTimePicker dpEndBy;
		private System.Windows.Forms.Button cmdOK;
		private System.Windows.Forms.Button cmdCancel;
		private System.Windows.Forms.Button cmdRemoveRecurrence;
		private System.Windows.Forms.Panel pnlWeekly;
		private System.Windows.Forms.CheckBox chkWeeklyMonday;
		private System.Windows.Forms.CheckBox chkWeeklySunday;
		private System.Windows.Forms.CheckBox chkWeeklySaturday;
		private System.Windows.Forms.CheckBox chkWeeklyFriday;
		private System.Windows.Forms.CheckBox chkWeeklyThursday;
		private System.Windows.Forms.CheckBox chkWeeklyWednesday;
		private System.Windows.Forms.CheckBox chkWeeklyTuesday;
		private System.Windows.Forms.Label lblWeeklyWeekOn;
		private System.Windows.Forms.Label lblWeeklyRecurEvery;
		private System.Windows.Forms.Panel pnlDaily;
		private System.Windows.Forms.RadioButton optDailyEveryWorkDay;
		private System.Windows.Forms.Label optDailyDays;
		private System.Windows.Forms.TextBox txtDailyEveryDay;
		private System.Windows.Forms.RadioButton optDailyEvery;
		private System.Windows.Forms.Panel pnlMonthly;
		private System.Windows.Forms.Label lblmonthlyMonthsMonths;
		private System.Windows.Forms.TextBox txtMonthlyOfEveryTheMonths;
		private System.Windows.Forms.Label lblMonthlyOfEveryMonth;
		private System.Windows.Forms.ComboBox cmbMonthlyDayOfWeek;
		private System.Windows.Forms.ComboBox cmbMonthlyDay;
		private System.Windows.Forms.RadioButton optMonthlyThe;
		private System.Windows.Forms.Label lblMonthlyMonths;
		private System.Windows.Forms.TextBox txtMonthlyEveryMonth;
		private System.Windows.Forms.Label lblMonthlyOfEvery;
		private System.Windows.Forms.ComboBox cmbMonthlyDayOfMonth;
		private System.Windows.Forms.RadioButton optMonthlyDay;
		private System.Windows.Forms.Panel pnlYearly;
		private System.Windows.Forms.ComboBox cmbYearlyTheMonth;
		private System.Windows.Forms.ComboBox cmbYearlyDate;
		private System.Windows.Forms.Label lblYearlyOf;
		private System.Windows.Forms.ComboBox cmbYearlyTheDay;
		private System.Windows.Forms.ComboBox cmdYearlyThePartOfWeek;
		private System.Windows.Forms.RadioButton optYearlyThe;
		private System.Windows.Forms.ComboBox cmbYearlyEveryDate;
		private System.Windows.Forms.RadioButton optYearlyDay;
		private System.Windows.Forms.TextBox txtWeeklyRecurEveryWeek;

		public CalendarRecurrencePatternOptions RecurrenceEventOptions;
		public CalendarEvent MyEvent = null;

		DateTime BeginSelection, EndSelection;
		Boolean AllDay;
		private System.Windows.Forms.PictureBox pictureBox1;

		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public frmRecurrenceDialog()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeControls();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		private void InitializeControls()
		{
			BeginSelection = EndSelection = DateTime.Now;
			AllDay = false;

			frmMain.Instance.wndCalendarControl.ActiveView.GetSelection(ref BeginSelection, ref EndSelection, ref AllDay);

			DateTime dtHours = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0);

			for ( int cnt = 0; cnt < 48; cnt++ ) 
			{        		
				cmbAppointmentStartTime.Items.Insert(cnt, dtHours.ToShortTimeString());			
				dtHours = dtHours.AddMinutes(30);
			}

			cmbAppointmentStartTime.Text = BeginSelection.ToShortTimeString();
			
			cmbAppointmentDuration.Items.Add("0 Minutes");
			cmbAppointmentDuration.Items.Add("5 Minutes");
			cmbAppointmentDuration.Items.Add("10 Minutes");
			cmbAppointmentDuration.Items.Add("15 Minutes");
			cmbAppointmentDuration.Items.Add("30 Minutes");
			cmbAppointmentDuration.Items.Add("60 Minutes (1 Hour)");
			cmbAppointmentDuration.Items.Add("120 Minutes (2 Hour)");
			cmbAppointmentDuration.Items.Add("180 Minutes (3 Hour)");
			cmbAppointmentDuration.Items.Add("240 Minutes (4 Hour)");

			cmbAppointmentDuration.Items.Add(System.Convert.ToString(24 * 60) + " Minutes (1 Day)");
			cmbAppointmentDuration.Items.Add(System.Convert.ToString(2 * 24 * 60) + " Minutes (2 Day)");

			cmbAppointmentDuration.SelectedIndex = 3;
			UpdateEndTimeCombo();

			dpStartDate.Value = BeginSelection;
			dpEndBy.Value = EndSelection;
			txtOccurences.Text = "10";
			optNoEndDate.Checked = true;

			//Daily Initialization
			DailyInitialization();

			//Weekly Initialization
			WeeklyInitialization();

			//Monthly Initialization
			MonthlyInitialization();

			//Yearly Initialization
			YearlyInitialization();
		}


		private void SafeSelectIndex(System.Windows.Forms.ComboBox cmbCMB, int nIndex)
		{
			int nIndexSafe = System.Math.Max(0, nIndex);
			nIndexSafe = System.Math.Min(cmbCMB.Items.Count-1, nIndexSafe);

			if (nIndexSafe >= 0) 
			{
				cmbCMB.SelectedIndex = nIndexSafe;
			}
		}

		private void DailyInitialization()
		{
			if (txtDailyEveryDay.Text == "")
			{
				txtDailyEveryDay.Text = "1";
			}
			
			if ((optDailyEveryWorkDay.Checked = false) & (optDailyEvery.Checked = false)) 
			{
				optDailyEvery.Checked = true;
			}
		}

		private void MonthlyInitialization()
		{
			frmMain.Instance.wndCalendarControl.ActiveView.GetSelection(ref BeginSelection, ref EndSelection, ref AllDay);

			if (cmbMonthlyDayOfMonth.Items.Count == 0) 
			{
				for ( int cnt = 1; cnt < 32; cnt++ ) 
				{        		
					cmbMonthlyDayOfMonth.Items.Insert(cnt - 1, cnt);
				}

				SafeSelectIndex(cmbMonthlyDayOfMonth, BeginSelection.Day + 1);
			}

			if (cmbMonthlyDay.Items.Count == 0)
			{
				cmbMonthlyDay.Items.Insert(0, "first");
				cmbMonthlyDay.Items.Insert(1, "second");
				cmbMonthlyDay.Items.Insert(2, "third");
				cmbMonthlyDay.Items.Insert(3, "fourth");
				cmbMonthlyDay.Items.Insert(4, "last");

				SafeSelectIndex(cmbMonthlyDay, 0);
			}

			if (cmbMonthlyDayOfWeek.Items.Count == 0)
			{
				/*int t = 0;
				foreach (string Week in Enum.GetNames(typeof(WeekDayMask)))
				{
					cmbMonthlyDayOfWeek.Items.Insert(t, Week);	
					t++;
				}*/
				cmbMonthlyDayOfWeek.Items.Insert(0, "Day");
				cmbMonthlyDayOfWeek.Items.Insert(1, "WeekDay");
				cmbMonthlyDayOfWeek.Items.Insert(2, "WeekendDay");
				cmbMonthlyDayOfWeek.Items.Insert(3, "Sunday");
				cmbMonthlyDayOfWeek.Items.Insert(4, "Monday");
				cmbMonthlyDayOfWeek.Items.Insert(5, "Tuesday");
				cmbMonthlyDayOfWeek.Items.Insert(6, "Wednesday");
				cmbMonthlyDayOfWeek.Items.Insert(7, "Thursday");
				cmbMonthlyDayOfWeek.Items.Insert(8, "Friday");
				cmbMonthlyDayOfWeek.Items.Insert(9, "Saturday");

				SafeSelectIndex(cmbMonthlyDayOfWeek, (int)BeginSelection.DayOfWeek + 3);
			}

			if (txtMonthlyEveryMonth.Text == "")
			{
				txtMonthlyEveryMonth.Text = "1";
			}

			if (txtMonthlyOfEveryTheMonths.Text == "")
			{
				txtMonthlyOfEveryTheMonths.Text = "1";
			}
		}

		public enum WeekDayMask
		{
			Day			= CalendarWeekDay.xtpCalendarDayAllWeek,	// All week mask.
			WeekDay     = CalendarWeekDay.xtpCalendarDayMo_Fr,		// Monday to Friday mask.
			WeekendDay  = CalendarWeekDay.xtpCalendarDaySaSu,		// Saturday, Sunday mask.
			Sunday		= CalendarWeekDay.xtpCalendarDaySaturday,   // Sunday
			Monday		= CalendarWeekDay.xtpCalendarDayMonday,     // Monday
			Tuesday     = CalendarWeekDay.xtpCalendarDayTuesday,    // Tuesday
			Wednesday   = CalendarWeekDay.xtpCalendarDayWednesday,  // Wednesday
			Thursday    = CalendarWeekDay.xtpCalendarDayThursday,   // Thursday
			Friday      = CalendarWeekDay.xtpCalendarDayFriday,     // Friday
			Saturday    = CalendarWeekDay.xtpCalendarDaySaturday,	// Saturday
		}

		private void YearlyInitialization()
		{
			frmMain.Instance.wndCalendarControl.ActiveView.GetSelection(ref BeginSelection, ref EndSelection, ref AllDay);

			if (cmbYearlyDate.Items.Count == 0) 
			{
				for ( int cnt = 1; cnt < 32; cnt++ ) 
				{        		
					cmbYearlyDate.Items.Insert(cnt - 1, cnt);
				}

				SafeSelectIndex(cmbYearlyDate, BeginSelection.Day + 1);
			}

			if (cmdYearlyThePartOfWeek.Items.Count == 0)
			{
				cmdYearlyThePartOfWeek.Items.Insert(0, "first");
				cmdYearlyThePartOfWeek.Items.Insert(1, "second");
				cmdYearlyThePartOfWeek.Items.Insert(2, "third");
				cmdYearlyThePartOfWeek.Items.Insert(3, "fourth");
				cmdYearlyThePartOfWeek.Items.Insert(4, "last");

				cmdYearlyThePartOfWeek.SelectedIndex = 0;
			}

			if (cmbYearlyTheDay.Items.Count == 0)
			{
				/*
				int t = 0;
				foreach (string Week in Enum.GetNames(typeof(frmMain.WorkWeekDay)))
				{
					cmbYearlyTheDay.Items.Insert(t, Week);	
					t++;
				}*/

				cmbYearlyTheDay.Items.Insert(0, "Day");
				cmbYearlyTheDay.Items.Insert(1, "WeekDay");
				cmbYearlyTheDay.Items.Insert(2, "WeekendDay");
				cmbYearlyTheDay.Items.Insert(3, "Sunday");
				cmbYearlyTheDay.Items.Insert(4, "Monday");
				cmbYearlyTheDay.Items.Insert(5, "Tuesday");
				cmbYearlyTheDay.Items.Insert(6, "Wednesday");
				cmbYearlyTheDay.Items.Insert(7, "Thursday");
				cmbYearlyTheDay.Items.Insert(8, "Friday");
				cmbYearlyTheDay.Items.Insert(9, "Saturday");

				SafeSelectIndex(cmbYearlyTheDay, (int)BeginSelection.DayOfWeek + 3);
			}

			if (cmbYearlyEveryDate.Items.Count == 0)
			{
				int k = 0;
				foreach (string Week in Enum.GetNames(typeof(MonthOfYear)))
				{
					cmbYearlyEveryDate.Items.Insert(k, Week);	
					cmbYearlyTheMonth.Items.Insert(k, Week);	
					k++;
				}

				SafeSelectIndex(cmbYearlyEveryDate, (int)BeginSelection.Month);
				SafeSelectIndex(cmbYearlyTheMonth, (int)BeginSelection.Month);
			}

		}

		private void WeeklyInitialization()
		{
			SetDayOfWeek();
			txtWeeklyRecurEveryWeek.Text = "1";
		}

		public enum	MonthOfYear
		{
			January = 0,
			February = 1,
			March = 2,
			April = 3,
			May = 4,
			June = 5,
			July = 6,
			August = 7,
			September = 8,
			October = 9,
			November = 10,
			December = 11
		}

		public void Weekly()
		{
			pnlDaily.Visible = false;
			pnlMonthly.Visible = false;
			pnlYearly.Visible = false;
			pnlWeekly.Visible = true;

			optWeekly.Checked = true;

			WeeklyInitialization();
			SetPattern(CalendarPatternEnd.xtpCalendarPatternEndNoDate, true);
			UpdateAppointmentTime();

			dpStartDate.Value = BeginSelection;
			dpEndBy.Value = EndSelection;

			if (MyEvent != null)
			{
				if (MyEvent.RecurrenceState != CalendarEventRecurrenceState.xtpCalendarRecurrenceNotRecurring) 
				{
					CheckDayOfWeek(MyEvent.RecurrencePattern.Options);
					txtWeeklyRecurEveryWeek.Text = MyEvent.RecurrencePattern.Options.WeeklyIntervalWeeks.ToString();
					SetPattern(MyEvent.RecurrencePattern.EndMethod, false);
					txtOccurences.Text = MyEvent.RecurrencePattern.EndAfterOccurrences.ToString();
					dpStartDate.Value = MyEvent.RecurrencePattern.StartDate;
				}
			}
		}

		private void UpdateAppointmentTime()
		{
			if (MyEvent.RecurrenceState == CalendarEventRecurrenceState.xtpCalendarRecurrenceMaster)
			{				
				cmbAppointmentStartTime.Text = MyEvent.RecurrencePattern.StartTime.ToShortTimeString();

				cmbAppointmentDuration.Text = MyEvent.RecurrencePattern.DurationMinutes.ToString() + " Minutes";
			}
			else
			{
				frmMain.Instance.wndCalendarControl.ActiveView.GetSelection(ref BeginSelection, ref EndSelection, ref AllDay);
				
				cmbAppointmentStartTime.Text = BeginSelection.ToShortTimeString();

				try {
					TimeSpan spDuration = EndSelection - BeginSelection;
					cmbAppointmentDuration.Text = spDuration.Minutes.ToString() + " Minutes";
				}
				catch {
					SafeSelectIndex(cmbAppointmentDuration, 3);
				}
			}
			
			UpdateEndTimeCombo();
		}

		private void SetDayOfWeek()
		{
			chkWeeklySunday.Checked = ((int)BeginSelection.DayOfWeek == (int)DayOfWeek.Sunday ? true : false);
			chkWeeklyMonday.Checked = ((int)BeginSelection.DayOfWeek == (int)DayOfWeek.Monday ? true : false);
			chkWeeklyTuesday.Checked = ((int)BeginSelection.DayOfWeek == (int)DayOfWeek.Tuesday ? true : false);
			chkWeeklyWednesday.Checked = ((int)BeginSelection.DayOfWeek == (int)DayOfWeek.Wednesday ? true : false);
			chkWeeklyThursday.Checked = ((int)BeginSelection.DayOfWeek == (int)DayOfWeek.Thursday ? true : false);
			chkWeeklyFriday.Checked = ((int)BeginSelection.DayOfWeek == (int)DayOfWeek.Friday ? true : false);
			chkWeeklySaturday.Checked = ((int)BeginSelection.DayOfWeek == (int)DayOfWeek.Saturday ? true : false);
		}

		private void CheckDayOfWeek(CalendarRecurrencePatternOptions Opt)
		{
			CalendarWeekDay WeekMask = Opt.WeeklyDayOfWeekMask;

			chkWeeklySunday.Checked = ((WeekMask & CalendarWeekDay.xtpCalendarDaySunday) == CalendarWeekDay.xtpCalendarDaySunday ? true : false);
			chkWeeklyMonday.Checked = ((WeekMask & CalendarWeekDay.xtpCalendarDayMonday) == CalendarWeekDay.xtpCalendarDayMonday ? true : false);
			chkWeeklyTuesday.Checked = ((WeekMask & CalendarWeekDay.xtpCalendarDayTuesday) == CalendarWeekDay.xtpCalendarDayTuesday ? true : false);
			chkWeeklyWednesday.Checked = ((WeekMask & CalendarWeekDay.xtpCalendarDayWednesday) == CalendarWeekDay.xtpCalendarDayWednesday ? true : false);
			chkWeeklyThursday.Checked = ((WeekMask & CalendarWeekDay.xtpCalendarDayThursday) == CalendarWeekDay.xtpCalendarDayThursday ? true : false);
			chkWeeklyFriday.Checked = ((WeekMask & CalendarWeekDay.xtpCalendarDayFriday) == CalendarWeekDay.xtpCalendarDayFriday ? true : false);
			chkWeeklySaturday.Checked = ((WeekMask & CalendarWeekDay.xtpCalendarDaySaturday) == CalendarWeekDay.xtpCalendarDaySaturday ? true : false);
		}

		private void UpdateEndTimeCombo()
		{
			try
			{
				string[] arDuration = cmbAppointmentDuration.Text.Split(' ');
				int nDuration = System.Convert.ToInt32(arDuration[0]);
				DateTime EndTime = System.Convert.ToDateTime(cmbAppointmentStartTime.Text);
				EndTime = EndTime.AddMinutes(nDuration);
				cmbAppointmentEndTime.Text = EndTime.ToShortTimeString();
			}
			catch{
				cmbAppointmentEndTime.Text = "error";				
			}
		}

		private void SetPattern(CalendarPatternEnd Pattern, Boolean NewRecurrence)
		{ 
			switch((int)Pattern) 
			{
			case (int)CalendarPatternEnd.xtpCalendarPatternEndNoDate:
				optNoEndDate.Checked = true;
				break;
			case (int)CalendarPatternEnd.xtpCalendarPatternEndDate:
				optEndBy.Checked = true;
				break;
			case (int)CalendarPatternEnd.xtpCalendarPatternEndAfterOccurrences:
				optEndAfter.Checked = true;
				break;
			}

			if (NewRecurrence) 
			{
				txtOccurences.Text = "10";
				frmMain.Instance.wndCalendarControl.ActiveView.GetSelection(ref BeginSelection, ref EndSelection, ref AllDay);
				dpEndBy.Value = EndSelection;
			}
			else
			{
				txtOccurences.Text = MyEvent.RecurrencePattern.EndAfterOccurrences.ToString();
				dpEndBy.Value = MyEvent.RecurrencePattern.EndDate;
			}
		}

		public void Monthly()
		{
			pnlDaily.Visible = false;
			pnlYearly.Visible = false;
			pnlWeekly.Visible = false;
			pnlMonthly.Visible = true;

			optMonthly.Checked = true;
			optMonthlyDay.Checked = true;

			MonthlyInitialization();
			SetPattern(CalendarPatternEnd.xtpCalendarPatternEndNoDate, true);
			UpdateAppointmentTime();

			frmMain.Instance.wndCalendarControl.ActiveView.GetSelection(ref BeginSelection, ref EndSelection, ref AllDay);
			dpStartDate.Value = BeginSelection;
			dpEndBy.Value = BeginSelection.AddDays(Convert.ToInt32(txtMonthlyEveryMonth.Text) * 31);

			if (MyEvent != null)
			{
				if (MyEvent.RecurrenceState != CalendarEventRecurrenceState.xtpCalendarRecurrenceNotRecurring) 
				{
					txtMonthlyEveryMonth.Text = MyEvent.RecurrencePattern.Options.MonthlyIntervalMonths.ToString();
					SafeSelectIndex(cmbMonthlyDayOfMonth, MyEvent.RecurrencePattern.Options.MonthlyDayOfMonth - 1);
					SetPattern(MyEvent.RecurrencePattern.EndMethod, false);
					
					txtOccurences.Text = MyEvent.RecurrencePattern.EndAfterOccurrences.ToString();
					dpStartDate.Value = MyEvent.RecurrencePattern.StartDate;
				}
			}
		}

		public void MonthlyNth()
		{
			pnlDaily.Visible = false;
			pnlYearly.Visible = false;
			pnlWeekly.Visible = false;
			pnlMonthly.Visible = true;

			optMonthly.Checked = true;
			optMonthlyThe.Checked = true;

			MonthlyInitialization();
			SetPattern(CalendarPatternEnd.xtpCalendarPatternEndNoDate, true);
			UpdateAppointmentTime();

			frmMain.Instance.wndCalendarControl.ActiveView.GetSelection(ref BeginSelection, ref EndSelection, ref AllDay);
			dpStartDate.Value = BeginSelection;
			dpEndBy.Value = BeginSelection.AddDays(Convert.ToInt32(txtMonthlyOfEveryTheMonths.Text) * 31);

			if (MyEvent != null) 
			{
				if (MyEvent.RecurrenceState != CalendarEventRecurrenceState.xtpCalendarRecurrenceNotRecurring) 
				{
					txtMonthlyOfEveryTheMonths.Text = MyEvent.RecurrencePattern.Options.MonthNthIntervalMonths.ToString();
					SafeSelectIndex(cmbMonthlyDay, MyEvent.RecurrencePattern.Options.MonthNthWhichDay - 1);
					SafeSelectIndex(cmbMonthlyDayOfWeek, GetDayMaskIndex(MyEvent.RecurrencePattern.Options, "Month"));

					SetPattern(MyEvent.RecurrencePattern.EndMethod, false);

					txtOccurences.Text = MyEvent.RecurrencePattern.EndAfterOccurrences.ToString();
					dpStartDate.Value = MyEvent.RecurrencePattern.StartDate;
				}
			}
		}

		private int GetDayMaskIndex(CalendarRecurrencePatternOptions Opt, String MaskType)
		{
			CalendarWeekDay Mask = 0;

			if (MaskType == "Month") // MonthNth Mask
			{
				Mask = Opt.MonthNthWhichDayMask;
			}
			else // YearNth Mask
			{
				Mask = (CalendarWeekDay)Opt.YearNthWhichDayMask;
			}

			int MaskIndex = -1;

			MaskIndex = ((Mask & CalendarWeekDay.xtpCalendarDayAllWeek) == CalendarWeekDay.xtpCalendarDayAllWeek ? 0 : -1);
			if (MaskIndex != -1) {return MaskIndex;}
			MaskIndex = ((Mask & CalendarWeekDay.xtpCalendarDayMo_Fr) == CalendarWeekDay.xtpCalendarDayMo_Fr ? 1 : -1);
			if (MaskIndex != -1) {return MaskIndex;}
			MaskIndex = ((Mask & CalendarWeekDay.xtpCalendarDaySaSu) == CalendarWeekDay.xtpCalendarDaySaSu ? 2 : -1);
			if (MaskIndex != -1) {return MaskIndex;}
			MaskIndex = ((Mask & CalendarWeekDay.xtpCalendarDaySunday) == CalendarWeekDay.xtpCalendarDaySunday ? 3 : -1);
			if (MaskIndex != -1) {return MaskIndex;}
			MaskIndex = ((Mask & CalendarWeekDay.xtpCalendarDayMonday) == CalendarWeekDay.xtpCalendarDayMonday ? 4 : -1);
			if (MaskIndex != -1) {return MaskIndex;}
			MaskIndex = ((Mask & CalendarWeekDay.xtpCalendarDayTuesday) == CalendarWeekDay.xtpCalendarDayTuesday ? 5 : -1);
			if (MaskIndex != -1) {return MaskIndex;}
			MaskIndex = ((Mask & CalendarWeekDay.xtpCalendarDayWednesday) == CalendarWeekDay.xtpCalendarDayWednesday ? 6 : -1);
			if (MaskIndex != -1) {return MaskIndex;}
			MaskIndex = ((Mask & CalendarWeekDay.xtpCalendarDayThursday) == CalendarWeekDay.xtpCalendarDayThursday ? 7 : -1);
			if (MaskIndex != -1) {return MaskIndex;}
			MaskIndex = ((Mask & CalendarWeekDay.xtpCalendarDayFriday) == CalendarWeekDay.xtpCalendarDayFriday ? 8 : -1);
			if (MaskIndex != -1) {return MaskIndex;}
			MaskIndex = ((Mask & CalendarWeekDay.xtpCalendarDaySaturday) == CalendarWeekDay.xtpCalendarDaySaturday ? 9 : -1);

			return MaskIndex;
		}

		public void Daily()
		{
			pnlMonthly.Visible = false;
			pnlYearly.Visible = false;
			pnlWeekly.Visible = false;
			pnlDaily.Visible = true;

			optDaily.Checked = true;

			DailyInitialization();
			SetPattern(CalendarPatternEnd.xtpCalendarPatternEndNoDate, true);
			UpdateAppointmentTime();

			optDailyEvery.Checked = true;

			frmMain.Instance.wndCalendarControl.ActiveView.GetSelection(ref BeginSelection, ref EndSelection, ref AllDay);
			dpStartDate.Value = BeginSelection;
			dpEndBy.Value = EndSelection.AddDays(Convert.ToInt32(txtDailyEveryDay.Text) * 9);		

			if (MyEvent != null)
			{
				if (MyEvent.RecurrenceState != CalendarEventRecurrenceState.xtpCalendarRecurrenceNotRecurring) 
				{
					if (MyEvent.RecurrencePattern.Options.DailyEveryWeekDayOnly)
					{
						optDailyEveryWorkDay.Checked = true;
						txtDailyEveryDay.Text = "1";
						SetPattern(MyEvent.RecurrencePattern.EndMethod, false);

						dpEndBy.Value = EndSelection.AddDays(Convert.ToInt32(txtOccurences.Text));		
					}
					else
					{
						optDailyEvery.Checked = true;
						txtDailyEveryDay.Text = MyEvent.RecurrencePattern.Options.DailyIntervalDays.ToString();
						SetPattern(MyEvent.RecurrencePattern.EndMethod, false);
						
						txtOccurences.Text = MyEvent.RecurrencePattern.EndAfterOccurrences.ToString();
						dpEndBy.Value = EndSelection.AddDays(Convert.ToInt32(txtDailyEveryDay.Text) * 9);
					}
				}
			}
		}

		public void Yearly()
		{
			pnlDaily.Visible = false;
			pnlMonthly.Visible = false;
			pnlWeekly.Visible = false;
			pnlYearly.Visible = true;

			optYearly.Checked = true;

			optYearlyDay.Checked = true;

			YearlyInitialization();
			SetPattern(CalendarPatternEnd.xtpCalendarPatternEndNoDate, true);
			UpdateAppointmentTime();

			frmMain.Instance.wndCalendarControl.ActiveView.GetSelection(ref BeginSelection, ref EndSelection, ref AllDay);
			dpStartDate.Value = BeginSelection;
			dpEndBy.Value = BeginSelection.AddYears(Convert.ToInt32(txtOccurences.Text));

			if (MyEvent != null)
			{
				if (MyEvent.RecurrenceState != CalendarEventRecurrenceState.xtpCalendarRecurrenceNotRecurring) 
				{
					cmbYearlyDate.Text = Convert.ToString(MyEvent.RecurrencePattern.Options.YearlyDayOfMonth);
					SafeSelectIndex(cmbYearlyEveryDate, MyEvent.RecurrencePattern.Options.YearlyMonthOfYear - 1);
					
					dpStartDate.Value = MyEvent.RecurrencePattern.StartDate;
					SetPattern(MyEvent.RecurrencePattern.EndMethod, false);

					txtOccurences.Text = MyEvent.RecurrencePattern.EndAfterOccurrences.ToString();
				}
			}
		}

		public void YearlyNth()
		{
			pnlDaily.Visible = false;
			pnlMonthly.Visible = false;
			pnlWeekly.Visible = false;
			pnlYearly.Visible = true;

			optYearly.Checked = true;

			optYearlyThe.Checked = true;

			YearlyInitialization();
			SetPattern(CalendarPatternEnd.xtpCalendarPatternEndNoDate, true);
			UpdateAppointmentTime();

			frmMain.Instance.wndCalendarControl.ActiveView.GetSelection(ref BeginSelection, ref EndSelection, ref AllDay);
			dpStartDate.Value = BeginSelection;
			dpEndBy.Value = BeginSelection.AddYears(1);

			if (MyEvent != null)
			{
				if (MyEvent.RecurrenceState != CalendarEventRecurrenceState.xtpCalendarRecurrenceNotRecurring) 
				{
					SafeSelectIndex(cmdYearlyThePartOfWeek, MyEvent.RecurrencePattern.Options.YearNthWhichDay - 1);
					SafeSelectIndex(cmbYearlyTheDay, GetDayMaskIndex(MyEvent.RecurrencePattern.Options, "Year"));
					SafeSelectIndex(cmbYearlyTheMonth, MyEvent.RecurrencePattern.Options.YearNthMonthOfYear - 1);
					
					dpStartDate.Value = MyEvent.RecurrencePattern.StartDate;
					SetPattern(MyEvent.RecurrencePattern.EndMethod, false);

					txtOccurences.Text = MyEvent.RecurrencePattern.EndAfterOccurrences.ToString();
				}
			}
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.gbxAppointmentTime = new System.Windows.Forms.GroupBox();
			this.cmbAppointmentDuration = new System.Windows.Forms.ComboBox();
			this.lblAppointmentduration = new System.Windows.Forms.Label();
			this.cmbAppointmentEndTime = new System.Windows.Forms.ComboBox();
			this.lblAppointmentEndTime = new System.Windows.Forms.Label();
			this.cmbAppointmentStartTime = new System.Windows.Forms.ComboBox();
			this.lblAppointmentStart = new System.Windows.Forms.Label();
			this.gbxRecurrencePattern = new System.Windows.Forms.GroupBox();
			this.pictureBox1 = new System.Windows.Forms.PictureBox();
			this.pnlYearly = new System.Windows.Forms.Panel();
			this.cmbYearlyTheMonth = new System.Windows.Forms.ComboBox();
			this.cmbYearlyDate = new System.Windows.Forms.ComboBox();
			this.lblYearlyOf = new System.Windows.Forms.Label();
			this.cmbYearlyTheDay = new System.Windows.Forms.ComboBox();
			this.cmdYearlyThePartOfWeek = new System.Windows.Forms.ComboBox();
			this.optYearlyThe = new System.Windows.Forms.RadioButton();
			this.cmbYearlyEveryDate = new System.Windows.Forms.ComboBox();
			this.optYearlyDay = new System.Windows.Forms.RadioButton();
			this.pnlWeekly = new System.Windows.Forms.Panel();
			this.txtWeeklyRecurEveryWeek = new System.Windows.Forms.TextBox();
			this.chkWeeklyMonday = new System.Windows.Forms.CheckBox();
			this.chkWeeklySunday = new System.Windows.Forms.CheckBox();
			this.chkWeeklySaturday = new System.Windows.Forms.CheckBox();
			this.chkWeeklyFriday = new System.Windows.Forms.CheckBox();
			this.chkWeeklyThursday = new System.Windows.Forms.CheckBox();
			this.chkWeeklyWednesday = new System.Windows.Forms.CheckBox();
			this.chkWeeklyTuesday = new System.Windows.Forms.CheckBox();
			this.lblWeeklyWeekOn = new System.Windows.Forms.Label();
			this.lblWeeklyRecurEvery = new System.Windows.Forms.Label();
			this.pnlDaily = new System.Windows.Forms.Panel();
			this.optDailyEveryWorkDay = new System.Windows.Forms.RadioButton();
			this.optDailyDays = new System.Windows.Forms.Label();
			this.txtDailyEveryDay = new System.Windows.Forms.TextBox();
			this.optDailyEvery = new System.Windows.Forms.RadioButton();
			this.optYearly = new System.Windows.Forms.RadioButton();
			this.optMonthly = new System.Windows.Forms.RadioButton();
			this.optWeekly = new System.Windows.Forms.RadioButton();
			this.optDaily = new System.Windows.Forms.RadioButton();
			this.pnlMonthly = new System.Windows.Forms.Panel();
			this.lblmonthlyMonthsMonths = new System.Windows.Forms.Label();
			this.txtMonthlyOfEveryTheMonths = new System.Windows.Forms.TextBox();
			this.lblMonthlyOfEveryMonth = new System.Windows.Forms.Label();
			this.cmbMonthlyDayOfWeek = new System.Windows.Forms.ComboBox();
			this.cmbMonthlyDay = new System.Windows.Forms.ComboBox();
			this.optMonthlyThe = new System.Windows.Forms.RadioButton();
			this.lblMonthlyMonths = new System.Windows.Forms.Label();
			this.txtMonthlyEveryMonth = new System.Windows.Forms.TextBox();
			this.lblMonthlyOfEvery = new System.Windows.Forms.Label();
			this.cmbMonthlyDayOfMonth = new System.Windows.Forms.ComboBox();
			this.optMonthlyDay = new System.Windows.Forms.RadioButton();
			this.gbxRangeOfRecurrence = new System.Windows.Forms.GroupBox();
			this.dpEndBy = new System.Windows.Forms.DateTimePicker();
			this.lblOccurences = new System.Windows.Forms.Label();
			this.txtOccurences = new System.Windows.Forms.TextBox();
			this.optEndBy = new System.Windows.Forms.RadioButton();
			this.optEndAfter = new System.Windows.Forms.RadioButton();
			this.optNoEndDate = new System.Windows.Forms.RadioButton();
			this.dpStartDate = new System.Windows.Forms.DateTimePicker();
			this.lblRangeStart = new System.Windows.Forms.Label();
			this.cmdOK = new System.Windows.Forms.Button();
			this.cmdCancel = new System.Windows.Forms.Button();
			this.cmdRemoveRecurrence = new System.Windows.Forms.Button();
			this.gbxAppointmentTime.SuspendLayout();
			this.gbxRecurrencePattern.SuspendLayout();
			this.pnlYearly.SuspendLayout();
			this.pnlWeekly.SuspendLayout();
			this.pnlDaily.SuspendLayout();
			this.pnlMonthly.SuspendLayout();
			this.gbxRangeOfRecurrence.SuspendLayout();
			this.SuspendLayout();
			// 
			// gbxAppointmentTime
			// 
			this.gbxAppointmentTime.Controls.AddRange(new System.Windows.Forms.Control[] {
																							 this.cmbAppointmentDuration,
																							 this.lblAppointmentduration,
																							 this.cmbAppointmentEndTime,
																							 this.lblAppointmentEndTime,
																							 this.cmbAppointmentStartTime,
																							 this.lblAppointmentStart});
			this.gbxAppointmentTime.Location = new System.Drawing.Point(8, 8);
			this.gbxAppointmentTime.Name = "gbxAppointmentTime";
			this.gbxAppointmentTime.Size = new System.Drawing.Size(560, 64);
			this.gbxAppointmentTime.TabIndex = 0;
			this.gbxAppointmentTime.TabStop = false;
			this.gbxAppointmentTime.Text = "Appointment Time";
			// 
			// cmbAppointmentDuration
			// 
			this.cmbAppointmentDuration.Location = new System.Drawing.Point(376, 32);
			this.cmbAppointmentDuration.Name = "cmbAppointmentDuration";
			this.cmbAppointmentDuration.Size = new System.Drawing.Size(168, 21);
			this.cmbAppointmentDuration.TabIndex = 5;
			this.cmbAppointmentDuration.TextChanged += new System.EventHandler(this.cmbAppointmentDuration_TextChanged);
			this.cmbAppointmentDuration.SelectedIndexChanged += new System.EventHandler(this.cmbAppointmentDuration_SelectedIndexChanged);
			// 
			// lblAppointmentduration
			// 
			this.lblAppointmentduration.Location = new System.Drawing.Point(320, 32);
			this.lblAppointmentduration.Name = "lblAppointmentduration";
			this.lblAppointmentduration.Size = new System.Drawing.Size(56, 24);
			this.lblAppointmentduration.TabIndex = 4;
			this.lblAppointmentduration.Text = "Duration:";
			this.lblAppointmentduration.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// cmbAppointmentEndTime
			// 
			this.cmbAppointmentEndTime.Enabled = false;
			this.cmbAppointmentEndTime.Location = new System.Drawing.Point(192, 32);
			this.cmbAppointmentEndTime.Name = "cmbAppointmentEndTime";
			this.cmbAppointmentEndTime.Size = new System.Drawing.Size(80, 21);
			this.cmbAppointmentEndTime.TabIndex = 3;
			// 
			// lblAppointmentEndTime
			// 
			this.lblAppointmentEndTime.Location = new System.Drawing.Point(154, 32);
			this.lblAppointmentEndTime.Name = "lblAppointmentEndTime";
			this.lblAppointmentEndTime.Size = new System.Drawing.Size(30, 23);
			this.lblAppointmentEndTime.TabIndex = 2;
			this.lblAppointmentEndTime.Text = "End:";
			this.lblAppointmentEndTime.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// cmbAppointmentStartTime
			// 
			this.cmbAppointmentStartTime.Location = new System.Drawing.Point(56, 32);
			this.cmbAppointmentStartTime.Name = "cmbAppointmentStartTime";
			this.cmbAppointmentStartTime.Size = new System.Drawing.Size(80, 21);
			this.cmbAppointmentStartTime.TabIndex = 1;
			this.cmbAppointmentStartTime.TextChanged += new System.EventHandler(this.cmbAppointmentStartTime_TextChanged);
			this.cmbAppointmentStartTime.SelectedIndexChanged += new System.EventHandler(this.cmbAppointmentStartTime_SelectedIndexChanged);
			// 
			// lblAppointmentStart
			// 
			this.lblAppointmentStart.Location = new System.Drawing.Point(16, 32);
			this.lblAppointmentStart.Name = "lblAppointmentStart";
			this.lblAppointmentStart.Size = new System.Drawing.Size(40, 23);
			this.lblAppointmentStart.TabIndex = 0;
			this.lblAppointmentStart.Text = "Start:";
			this.lblAppointmentStart.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// gbxRecurrencePattern
			// 
			this.gbxRecurrencePattern.Controls.AddRange(new System.Windows.Forms.Control[] {
																							   this.pictureBox1,
																							   this.pnlYearly,
																							   this.pnlWeekly,
																							   this.pnlDaily,
																							   this.optYearly,
																							   this.optMonthly,
																							   this.optWeekly,
																							   this.optDaily,
																							   this.pnlMonthly});
			this.gbxRecurrencePattern.Location = new System.Drawing.Point(8, 80);
			this.gbxRecurrencePattern.Name = "gbxRecurrencePattern";
			this.gbxRecurrencePattern.Size = new System.Drawing.Size(560, 304);
			this.gbxRecurrencePattern.TabIndex = 1;
			this.gbxRecurrencePattern.TabStop = false;
			this.gbxRecurrencePattern.Text = "Recurrence Pattern:";
			// 
			// pictureBox1
			// 
			this.pictureBox1.BackColor = System.Drawing.SystemColors.AppWorkspace;
			this.pictureBox1.Location = new System.Drawing.Point(88, 16);
			this.pictureBox1.Name = "pictureBox1";
			this.pictureBox1.Size = new System.Drawing.Size(2, 104);
			this.pictureBox1.TabIndex = 12;
			this.pictureBox1.TabStop = false;
			// 
			// pnlYearly
			// 
			this.pnlYearly.Controls.AddRange(new System.Windows.Forms.Control[] {
																					this.cmbYearlyTheMonth,
																					this.cmbYearlyDate,
																					this.lblYearlyOf,
																					this.cmbYearlyTheDay,
																					this.cmdYearlyThePartOfWeek,
																					this.optYearlyThe,
																					this.cmbYearlyEveryDate,
																					this.optYearlyDay});
			this.pnlYearly.Location = new System.Drawing.Point(96, 232);
			this.pnlYearly.Name = "pnlYearly";
			this.pnlYearly.Size = new System.Drawing.Size(400, 72);
			this.pnlYearly.TabIndex = 11;
			this.pnlYearly.Visible = false;
			// 
			// cmbYearlyTheMonth
			// 
			this.cmbYearlyTheMonth.Location = new System.Drawing.Point(272, 40);
			this.cmbYearlyTheMonth.Name = "cmbYearlyTheMonth";
			this.cmbYearlyTheMonth.Size = new System.Drawing.Size(121, 21);
			this.cmbYearlyTheMonth.TabIndex = 16;
			// 
			// cmbYearlyDate
			// 
			this.cmbYearlyDate.Location = new System.Drawing.Point(192, 8);
			this.cmbYearlyDate.Name = "cmbYearlyDate";
			this.cmbYearlyDate.Size = new System.Drawing.Size(121, 21);
			this.cmbYearlyDate.TabIndex = 15;
			// 
			// lblYearlyOf
			// 
			this.lblYearlyOf.Location = new System.Drawing.Point(256, 40);
			this.lblYearlyOf.Name = "lblYearlyOf";
			this.lblYearlyOf.Size = new System.Drawing.Size(16, 16);
			this.lblYearlyOf.TabIndex = 14;
			this.lblYearlyOf.Text = "of";
			// 
			// cmbYearlyTheDay
			// 
			this.cmbYearlyTheDay.Location = new System.Drawing.Point(136, 39);
			this.cmbYearlyTheDay.Name = "cmbYearlyTheDay";
			this.cmbYearlyTheDay.Size = new System.Drawing.Size(121, 21);
			this.cmbYearlyTheDay.TabIndex = 13;
			// 
			// cmdYearlyThePartOfWeek
			// 
			this.cmdYearlyThePartOfWeek.Location = new System.Drawing.Point(56, 39);
			this.cmdYearlyThePartOfWeek.Name = "cmdYearlyThePartOfWeek";
			this.cmdYearlyThePartOfWeek.Size = new System.Drawing.Size(121, 21);
			this.cmdYearlyThePartOfWeek.TabIndex = 12;
			// 
			// optYearlyThe
			// 
			this.optYearlyThe.Location = new System.Drawing.Point(8, 39);
			this.optYearlyThe.Name = "optYearlyThe";
			this.optYearlyThe.Size = new System.Drawing.Size(48, 24);
			this.optYearlyThe.TabIndex = 11;
			this.optYearlyThe.Text = "The";
			this.optYearlyThe.CheckedChanged += new System.EventHandler(this.optYearlyThe_CheckedChanged);
			// 
			// cmbYearlyEveryDate
			// 
			this.cmbYearlyEveryDate.Location = new System.Drawing.Point(56, 7);
			this.cmbYearlyEveryDate.Name = "cmbYearlyEveryDate";
			this.cmbYearlyEveryDate.Size = new System.Drawing.Size(121, 21);
			this.cmbYearlyEveryDate.TabIndex = 10;
			// 
			// optYearlyDay
			// 
			this.optYearlyDay.Location = new System.Drawing.Point(8, 15);
			this.optYearlyDay.Name = "optYearlyDay";
			this.optYearlyDay.Size = new System.Drawing.Size(48, 24);
			this.optYearlyDay.TabIndex = 9;
			this.optYearlyDay.Text = "Day";
			this.optYearlyDay.CheckedChanged += new System.EventHandler(this.optYearlyDay_CheckedChanged);
			// 
			// pnlWeekly
			// 
			this.pnlWeekly.Controls.AddRange(new System.Windows.Forms.Control[] {
																					this.txtWeeklyRecurEveryWeek,
																					this.chkWeeklyMonday,
																					this.chkWeeklySunday,
																					this.chkWeeklySaturday,
																					this.chkWeeklyFriday,
																					this.chkWeeklyThursday,
																					this.chkWeeklyWednesday,
																					this.chkWeeklyTuesday,
																					this.lblWeeklyWeekOn,
																					this.lblWeeklyRecurEvery});
			this.pnlWeekly.Location = new System.Drawing.Point(96, 72);
			this.pnlWeekly.Name = "pnlWeekly";
			this.pnlWeekly.Size = new System.Drawing.Size(360, 88);
			this.pnlWeekly.TabIndex = 8;
			this.pnlWeekly.Visible = false;
			// 
			// txtWeeklyRecurEveryWeek
			// 
			this.txtWeeklyRecurEveryWeek.Location = new System.Drawing.Point(84, 3);
			this.txtWeeklyRecurEveryWeek.MaxLength = 4;
			this.txtWeeklyRecurEveryWeek.Name = "txtWeeklyRecurEveryWeek";
			this.txtWeeklyRecurEveryWeek.TabIndex = 29;
			this.txtWeeklyRecurEveryWeek.Text = "";
			this.txtWeeklyRecurEveryWeek.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			this.txtWeeklyRecurEveryWeek.WordWrap = false;
			// 
			// chkWeeklyMonday
			// 
			this.chkWeeklyMonday.Location = new System.Drawing.Point(8, 40);
			this.chkWeeklyMonday.Name = "chkWeeklyMonday";
			this.chkWeeklyMonday.Size = new System.Drawing.Size(64, 16);
			this.chkWeeklyMonday.TabIndex = 28;
			this.chkWeeklyMonday.Text = "Monday";
			// 
			// chkWeeklySunday
			// 
			this.chkWeeklySunday.Location = new System.Drawing.Point(152, 60);
			this.chkWeeklySunday.Name = "chkWeeklySunday";
			this.chkWeeklySunday.Size = new System.Drawing.Size(104, 20);
			this.chkWeeklySunday.TabIndex = 27;
			this.chkWeeklySunday.Text = "Sunday";
			// 
			// chkWeeklySaturday
			// 
			this.chkWeeklySaturday.Location = new System.Drawing.Point(80, 60);
			this.chkWeeklySaturday.Name = "chkWeeklySaturday";
			this.chkWeeklySaturday.Size = new System.Drawing.Size(72, 20);
			this.chkWeeklySaturday.TabIndex = 26;
			this.chkWeeklySaturday.Text = "Saturday";
			// 
			// chkWeeklyFriday
			// 
			this.chkWeeklyFriday.Location = new System.Drawing.Point(8, 60);
			this.chkWeeklyFriday.Name = "chkWeeklyFriday";
			this.chkWeeklyFriday.Size = new System.Drawing.Size(64, 20);
			this.chkWeeklyFriday.TabIndex = 25;
			this.chkWeeklyFriday.Text = "Friday";
			// 
			// chkWeeklyThursday
			// 
			this.chkWeeklyThursday.Location = new System.Drawing.Point(248, 40);
			this.chkWeeklyThursday.Name = "chkWeeklyThursday";
			this.chkWeeklyThursday.Size = new System.Drawing.Size(104, 16);
			this.chkWeeklyThursday.TabIndex = 24;
			this.chkWeeklyThursday.Text = "Thursday";
			// 
			// chkWeeklyWednesday
			// 
			this.chkWeeklyWednesday.Location = new System.Drawing.Point(152, 40);
			this.chkWeeklyWednesday.Name = "chkWeeklyWednesday";
			this.chkWeeklyWednesday.Size = new System.Drawing.Size(88, 16);
			this.chkWeeklyWednesday.TabIndex = 23;
			this.chkWeeklyWednesday.Text = "Wednesday";
			// 
			// chkWeeklyTuesday
			// 
			this.chkWeeklyTuesday.Location = new System.Drawing.Point(80, 40);
			this.chkWeeklyTuesday.Name = "chkWeeklyTuesday";
			this.chkWeeklyTuesday.Size = new System.Drawing.Size(104, 16);
			this.chkWeeklyTuesday.TabIndex = 22;
			this.chkWeeklyTuesday.Text = "Tuesday";
			// 
			// lblWeeklyWeekOn
			// 
			this.lblWeeklyWeekOn.Location = new System.Drawing.Point(192, 8);
			this.lblWeeklyWeekOn.Name = "lblWeeklyWeekOn";
			this.lblWeeklyWeekOn.Size = new System.Drawing.Size(96, 16);
			this.lblWeeklyWeekOn.TabIndex = 21;
			this.lblWeeklyWeekOn.Text = "Week(s) On:";
			// 
			// lblWeeklyRecurEvery
			// 
			this.lblWeeklyRecurEvery.Location = new System.Drawing.Point(8, 8);
			this.lblWeeklyRecurEvery.Name = "lblWeeklyRecurEvery";
			this.lblWeeklyRecurEvery.Size = new System.Drawing.Size(80, 16);
			this.lblWeeklyRecurEvery.TabIndex = 19;
			this.lblWeeklyRecurEvery.Text = "Recur Every";
			// 
			// pnlDaily
			// 
			this.pnlDaily.Controls.AddRange(new System.Windows.Forms.Control[] {
																				   this.optDailyEveryWorkDay,
																				   this.optDailyDays,
																				   this.txtDailyEveryDay,
																				   this.optDailyEvery});
			this.pnlDaily.Location = new System.Drawing.Point(96, 16);
			this.pnlDaily.Name = "pnlDaily";
			this.pnlDaily.TabIndex = 9;
			this.pnlDaily.Visible = false;
			// 
			// optDailyEveryWorkDay
			// 
			this.optDailyEveryWorkDay.Location = new System.Drawing.Point(8, 32);
			this.optDailyEveryWorkDay.Name = "optDailyEveryWorkDay";
			this.optDailyEveryWorkDay.TabIndex = 21;
			this.optDailyEveryWorkDay.Text = "Every Work Day";
			this.optDailyEveryWorkDay.CheckedChanged += new System.EventHandler(this.optDailyEveryWorkDay_CheckedChanged);
			// 
			// optDailyDays
			// 
			this.optDailyDays.Location = new System.Drawing.Point(120, 8);
			this.optDailyDays.Name = "optDailyDays";
			this.optDailyDays.Size = new System.Drawing.Size(48, 24);
			this.optDailyDays.TabIndex = 20;
			this.optDailyDays.Text = "Day(s)";
			this.optDailyDays.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtDailyEveryDay
			// 
			this.txtDailyEveryDay.Location = new System.Drawing.Point(64, 8);
			this.txtDailyEveryDay.Name = "txtDailyEveryDay";
			this.txtDailyEveryDay.Size = new System.Drawing.Size(56, 20);
			this.txtDailyEveryDay.TabIndex = 19;
			this.txtDailyEveryDay.Text = "";
			this.txtDailyEveryDay.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			this.txtDailyEveryDay.WordWrap = false;
			// 
			// optDailyEvery
			// 
			this.optDailyEvery.Location = new System.Drawing.Point(8, 8);
			this.optDailyEvery.Name = "optDailyEvery";
			this.optDailyEvery.TabIndex = 18;
			this.optDailyEvery.Text = "Every";
			this.optDailyEvery.CheckedChanged += new System.EventHandler(this.optDailyEvery_CheckedChanged);
			// 
			// optYearly
			// 
			this.optYearly.Location = new System.Drawing.Point(16, 92);
			this.optYearly.Name = "optYearly";
			this.optYearly.Size = new System.Drawing.Size(64, 24);
			this.optYearly.TabIndex = 3;
			this.optYearly.Text = "Yearly";
			this.optYearly.CheckedChanged += new System.EventHandler(this.optYearly_CheckedChanged);
			// 
			// optMonthly
			// 
			this.optMonthly.Location = new System.Drawing.Point(16, 68);
			this.optMonthly.Name = "optMonthly";
			this.optMonthly.Size = new System.Drawing.Size(64, 24);
			this.optMonthly.TabIndex = 2;
			this.optMonthly.Text = "Monthly";
			this.optMonthly.CheckedChanged += new System.EventHandler(this.optMonthly_CheckedChanged);
			// 
			// optWeekly
			// 
			this.optWeekly.Location = new System.Drawing.Point(16, 44);
			this.optWeekly.Name = "optWeekly";
			this.optWeekly.Size = new System.Drawing.Size(64, 24);
			this.optWeekly.TabIndex = 1;
			this.optWeekly.Text = "Weekly";
			this.optWeekly.CheckedChanged += new System.EventHandler(this.optWeekly_CheckedChanged);
			// 
			// optDaily
			// 
			this.optDaily.Location = new System.Drawing.Point(16, 20);
			this.optDaily.Name = "optDaily";
			this.optDaily.Size = new System.Drawing.Size(64, 24);
			this.optDaily.TabIndex = 0;
			this.optDaily.Text = "Daily";
			this.optDaily.CheckedChanged += new System.EventHandler(this.optDaily_CheckedChanged);
			// 
			// pnlMonthly
			// 
			this.pnlMonthly.Controls.AddRange(new System.Windows.Forms.Control[] {
																					 this.lblmonthlyMonthsMonths,
																					 this.txtMonthlyOfEveryTheMonths,
																					 this.lblMonthlyOfEveryMonth,
																					 this.cmbMonthlyDayOfWeek,
																					 this.cmbMonthlyDay,
																					 this.optMonthlyThe,
																					 this.lblMonthlyMonths,
																					 this.txtMonthlyEveryMonth,
																					 this.lblMonthlyOfEvery,
																					 this.cmbMonthlyDayOfMonth,
																					 this.optMonthlyDay});
			this.pnlMonthly.Location = new System.Drawing.Point(96, 160);
			this.pnlMonthly.Name = "pnlMonthly";
			this.pnlMonthly.Size = new System.Drawing.Size(448, 72);
			this.pnlMonthly.TabIndex = 10;
			this.pnlMonthly.Visible = false;
			// 
			// lblmonthlyMonthsMonths
			// 
			this.lblmonthlyMonthsMonths.Location = new System.Drawing.Point(392, 40);
			this.lblmonthlyMonthsMonths.Name = "lblmonthlyMonthsMonths";
			this.lblmonthlyMonthsMonths.Size = new System.Drawing.Size(56, 24);
			this.lblmonthlyMonthsMonths.TabIndex = 10;
			this.lblmonthlyMonthsMonths.Text = "Month(s)";
			this.lblmonthlyMonthsMonths.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtMonthlyOfEveryTheMonths
			// 
			this.txtMonthlyOfEveryTheMonths.Location = new System.Drawing.Point(288, 40);
			this.txtMonthlyOfEveryTheMonths.MaxLength = 4;
			this.txtMonthlyOfEveryTheMonths.Name = "txtMonthlyOfEveryTheMonths";
			this.txtMonthlyOfEveryTheMonths.TabIndex = 9;
			this.txtMonthlyOfEveryTheMonths.Text = "";
			this.txtMonthlyOfEveryTheMonths.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			this.txtMonthlyOfEveryTheMonths.WordWrap = false;
			// 
			// lblMonthlyOfEveryMonth
			// 
			this.lblMonthlyOfEveryMonth.Location = new System.Drawing.Point(240, 40);
			this.lblMonthlyOfEveryMonth.Name = "lblMonthlyOfEveryMonth";
			this.lblMonthlyOfEveryMonth.Size = new System.Drawing.Size(48, 24);
			this.lblMonthlyOfEveryMonth.TabIndex = 8;
			this.lblMonthlyOfEveryMonth.Text = "of Every";
			this.lblMonthlyOfEveryMonth.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// cmbMonthlyDayOfWeek
			// 
			this.cmbMonthlyDayOfWeek.Location = new System.Drawing.Point(136, 40);
			this.cmbMonthlyDayOfWeek.Name = "cmbMonthlyDayOfWeek";
			this.cmbMonthlyDayOfWeek.Size = new System.Drawing.Size(96, 21);
			this.cmbMonthlyDayOfWeek.TabIndex = 7;
			// 
			// cmbMonthlyDay
			// 
			this.cmbMonthlyDay.Location = new System.Drawing.Point(56, 40);
			this.cmbMonthlyDay.Name = "cmbMonthlyDay";
			this.cmbMonthlyDay.Size = new System.Drawing.Size(72, 21);
			this.cmbMonthlyDay.TabIndex = 6;
			// 
			// optMonthlyThe
			// 
			this.optMonthlyThe.Location = new System.Drawing.Point(8, 40);
			this.optMonthlyThe.Name = "optMonthlyThe";
			this.optMonthlyThe.Size = new System.Drawing.Size(48, 24);
			this.optMonthlyThe.TabIndex = 5;
			this.optMonthlyThe.Text = "The";
			this.optMonthlyThe.CheckedChanged += new System.EventHandler(this.optMonthlyThe_CheckedChanged);
			// 
			// lblMonthlyMonths
			// 
			this.lblMonthlyMonths.Location = new System.Drawing.Point(264, 8);
			this.lblMonthlyMonths.Name = "lblMonthlyMonths";
			this.lblMonthlyMonths.Size = new System.Drawing.Size(56, 24);
			this.lblMonthlyMonths.TabIndex = 4;
			this.lblMonthlyMonths.Text = "Month(s)";
			this.lblMonthlyMonths.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtMonthlyEveryMonth
			// 
			this.txtMonthlyEveryMonth.Location = new System.Drawing.Point(160, 8);
			this.txtMonthlyEveryMonth.MaxLength = 4;
			this.txtMonthlyEveryMonth.Name = "txtMonthlyEveryMonth";
			this.txtMonthlyEveryMonth.TabIndex = 3;
			this.txtMonthlyEveryMonth.Text = "";
			this.txtMonthlyEveryMonth.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// lblMonthlyOfEvery
			// 
			this.lblMonthlyOfEvery.Location = new System.Drawing.Point(112, 8);
			this.lblMonthlyOfEvery.Name = "lblMonthlyOfEvery";
			this.lblMonthlyOfEvery.Size = new System.Drawing.Size(48, 24);
			this.lblMonthlyOfEvery.TabIndex = 2;
			this.lblMonthlyOfEvery.Text = "of Every";
			this.lblMonthlyOfEvery.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// cmbMonthlyDayOfMonth
			// 
			this.cmbMonthlyDayOfMonth.Location = new System.Drawing.Point(56, 8);
			this.cmbMonthlyDayOfMonth.Name = "cmbMonthlyDayOfMonth";
			this.cmbMonthlyDayOfMonth.Size = new System.Drawing.Size(56, 21);
			this.cmbMonthlyDayOfMonth.TabIndex = 1;
			// 
			// optMonthlyDay
			// 
			this.optMonthlyDay.Location = new System.Drawing.Point(8, 8);
			this.optMonthlyDay.Name = "optMonthlyDay";
			this.optMonthlyDay.Size = new System.Drawing.Size(48, 24);
			this.optMonthlyDay.TabIndex = 0;
			this.optMonthlyDay.Text = "Day";
			this.optMonthlyDay.CheckedChanged += new System.EventHandler(this.optMonthlyDay_CheckedChanged);
			// 
			// gbxRangeOfRecurrence
			// 
			this.gbxRangeOfRecurrence.Controls.AddRange(new System.Windows.Forms.Control[] {
																							   this.dpEndBy,
																							   this.lblOccurences,
																							   this.txtOccurences,
																							   this.optEndBy,
																							   this.optEndAfter,
																							   this.optNoEndDate,
																							   this.dpStartDate,
																							   this.lblRangeStart});
			this.gbxRangeOfRecurrence.Location = new System.Drawing.Point(8, 392);
			this.gbxRangeOfRecurrence.Name = "gbxRangeOfRecurrence";
			this.gbxRangeOfRecurrence.Size = new System.Drawing.Size(560, 88);
			this.gbxRangeOfRecurrence.TabIndex = 2;
			this.gbxRangeOfRecurrence.TabStop = false;
			this.gbxRangeOfRecurrence.Text = "Range of Recurrence";
			// 
			// dpEndBy
			// 
			this.dpEndBy.Format = System.Windows.Forms.DateTimePickerFormat.Short;
			this.dpEndBy.Location = new System.Drawing.Point(304, 56);
			this.dpEndBy.Name = "dpEndBy";
			this.dpEndBy.Size = new System.Drawing.Size(104, 20);
			this.dpEndBy.TabIndex = 7;
			// 
			// lblOccurences
			// 
			this.lblOccurences.Location = new System.Drawing.Point(360, 32);
			this.lblOccurences.Name = "lblOccurences";
			this.lblOccurences.Size = new System.Drawing.Size(80, 24);
			this.lblOccurences.TabIndex = 6;
			this.lblOccurences.Text = "Occurrences";
			this.lblOccurences.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtOccurences
			// 
			this.txtOccurences.Location = new System.Drawing.Point(304, 32);
			this.txtOccurences.MaxLength = 4;
			this.txtOccurences.Name = "txtOccurences";
			this.txtOccurences.Size = new System.Drawing.Size(48, 20);
			this.txtOccurences.TabIndex = 5;
			this.txtOccurences.Text = "";
			this.txtOccurences.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			this.txtOccurences.WordWrap = false;
			// 
			// optEndBy
			// 
			this.optEndBy.Location = new System.Drawing.Point(224, 56);
			this.optEndBy.Name = "optEndBy";
			this.optEndBy.TabIndex = 4;
			this.optEndBy.Text = "End By:";
			// 
			// optEndAfter
			// 
			this.optEndAfter.Location = new System.Drawing.Point(224, 32);
			this.optEndAfter.Name = "optEndAfter";
			this.optEndAfter.TabIndex = 3;
			this.optEndAfter.Text = "End After:";
			this.optEndAfter.CheckedChanged += new System.EventHandler(this.optEndAfter_CheckedChanged);
			// 
			// optNoEndDate
			// 
			this.optNoEndDate.Location = new System.Drawing.Point(224, 16);
			this.optNoEndDate.Name = "optNoEndDate";
			this.optNoEndDate.Size = new System.Drawing.Size(104, 16);
			this.optNoEndDate.TabIndex = 2;
			this.optNoEndDate.Text = "No End Date";
			// 
			// dpStartDate
			// 
			this.dpStartDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
			this.dpStartDate.Location = new System.Drawing.Point(56, 32);
			this.dpStartDate.Name = "dpStartDate";
			this.dpStartDate.Size = new System.Drawing.Size(144, 20);
			this.dpStartDate.TabIndex = 1;
			// 
			// lblRangeStart
			// 
			this.lblRangeStart.Location = new System.Drawing.Point(16, 32);
			this.lblRangeStart.Name = "lblRangeStart";
			this.lblRangeStart.Size = new System.Drawing.Size(40, 24);
			this.lblRangeStart.TabIndex = 0;
			this.lblRangeStart.Text = "Start:";
			this.lblRangeStart.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// cmdOK
			// 
			this.cmdOK.Location = new System.Drawing.Point(200, 488);
			this.cmdOK.Name = "cmdOK";
			this.cmdOK.TabIndex = 3;
			this.cmdOK.Text = "OK";
			this.cmdOK.Click += new System.EventHandler(this.cmdOK_Click);
			// 
			// cmdCancel
			// 
			this.cmdCancel.Location = new System.Drawing.Point(296, 488);
			this.cmdCancel.Name = "cmdCancel";
			this.cmdCancel.TabIndex = 4;
			this.cmdCancel.Text = "Cancel";
			this.cmdCancel.Click += new System.EventHandler(this.cmdCancel_Click);
			// 
			// cmdRemoveRecurrence
			// 
			this.cmdRemoveRecurrence.Enabled = false;
			this.cmdRemoveRecurrence.Location = new System.Drawing.Point(432, 488);
			this.cmdRemoveRecurrence.Name = "cmdRemoveRecurrence";
			this.cmdRemoveRecurrence.Size = new System.Drawing.Size(136, 23);
			this.cmdRemoveRecurrence.TabIndex = 5;
			this.cmdRemoveRecurrence.Text = "Remove Recurrence";
			this.cmdRemoveRecurrence.Click += new System.EventHandler(this.cmdRemoveRecurrence_Click);
			// 
			// frmRecurrenceDialog
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(578, 519);
			this.Controls.AddRange(new System.Windows.Forms.Control[] {
																		  this.cmdRemoveRecurrence,
																		  this.cmdCancel,
																		  this.cmdOK,
																		  this.gbxRangeOfRecurrence,
																		  this.gbxRecurrencePattern,
																		  this.gbxAppointmentTime});
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "frmRecurrenceDialog";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Appointment Recurrence";
			this.gbxAppointmentTime.ResumeLayout(false);
			this.gbxRecurrencePattern.ResumeLayout(false);
			this.pnlYearly.ResumeLayout(false);
			this.pnlWeekly.ResumeLayout(false);
			this.pnlDaily.ResumeLayout(false);
			this.pnlMonthly.ResumeLayout(false);
			this.gbxRangeOfRecurrence.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion


		private void optWeekly_CheckedChanged(object sender, System.EventArgs e)
		{
			if (optWeekly.Checked) 
			{
				Weekly();
			}
		}

		private void optDaily_CheckedChanged(object sender, System.EventArgs e)
		{
			if (optDaily.Checked) 
			{
				Daily();
			}
		}

		private void optMonthly_CheckedChanged(object sender, System.EventArgs e)
		{
			if (optMonthly.Checked) 
			{
				Monthly();
			}
		}

		private void optYearly_CheckedChanged(object sender, System.EventArgs e)
		{
			if (optYearly.Checked) 
			{
				Yearly();
			}
		}

		private void optDailyEvery_CheckedChanged(object sender, System.EventArgs e)
		{
			if (optDailyEvery.Checked) 
			{
				frmMain.Instance.wndCalendarControl.ActiveView.GetSelection(ref BeginSelection, ref EndSelection, ref AllDay);
				dpEndBy.Value = EndSelection.AddDays(Convert.ToInt32(txtDailyEveryDay.Text) * 9);		
			}
		}

		private void optDailyEveryWorkDay_CheckedChanged(object sender, System.EventArgs e)
		{
			if (optDailyEveryWorkDay.Checked) 
			{
				frmMain.Instance.wndCalendarControl.ActiveView.GetSelection(ref BeginSelection, ref EndSelection, ref AllDay);
				dpEndBy.Value = EndSelection.AddDays(Convert.ToInt32(txtOccurences.Text));		
			}		
		}

		private void optEndAfter_CheckedChanged(object sender, System.EventArgs e)
		{
			if (optDailyEveryWorkDay.Checked != true) 
			{
				txtOccurences.Text = "10";
			}			
		}

		private void optMonthlyThe_CheckedChanged(object sender, System.EventArgs e)
		{
			if (optMonthlyThe.Checked == true) 
			{
				MonthlyNth();
			}			
		}

		private void optMonthlyDay_CheckedChanged(object sender, System.EventArgs e)
		{
			if (optMonthlyDay.Checked == true) 
			{
				Monthly();
			}			
		}

		private void optYearlyThe_CheckedChanged(object sender, System.EventArgs e)
		{
			if (optYearlyThe.Checked == true) 
			{
				YearlyNth();
			}
		}

		private void optYearlyDay_CheckedChanged(object sender, System.EventArgs e)
		{
			if (optYearlyDay.Checked == true) 
			{
				Yearly();
			}		
		}

		private void cmdCancel_Click(object sender, System.EventArgs e)
		{
			DialogResult = DialogResult.Cancel;
			this.Close();
		}

		private void cmdRemoveRecurrence_Click(object sender, System.EventArgs e)
		{
			if (MyEvent.RecurrenceState == CalendarEventRecurrenceState.xtpCalendarRecurrenceException ||
				MyEvent.RecurrenceState == CalendarEventRecurrenceState.xtpCalendarRecurrenceOccurrence)
			{
				MyEvent.SetRExceptionDeleted(true);
			}
			else
			{
				MyEvent.RemoveRecurrence();
			}

			DialogResult = DialogResult.OK;
			this.Close();
		}

		private int SetDayMask(int nIndex)
		{
			switch(nIndex) 
			{
			case 0:
				return (int)CalendarWeekDay.xtpCalendarDayAllWeek;
			case 1:
				return (int)CalendarWeekDay.xtpCalendarDayMo_Fr;
			case 2:
				return (int)CalendarWeekDay.xtpCalendarDaySaSu;
			case 3:
				return (int)CalendarWeekDay.xtpCalendarDaySunday;
			case 4:
				return (int)CalendarWeekDay.xtpCalendarDayMonday;
			case 5:
				return (int)CalendarWeekDay.xtpCalendarDayTuesday;
			case 6:
				return (int)CalendarWeekDay.xtpCalendarDayWednesday;
			case 7:
				return (int)CalendarWeekDay.xtpCalendarDayThursday;
			case 8:
				return (int)CalendarWeekDay.xtpCalendarDayFriday;
			case 9:
				return (int)CalendarWeekDay.xtpCalendarDaySaturday;
			default:
				return -1;
			}
		}

		private void cmdOK_Click(object sender, System.EventArgs e)
		{   		
			CalendarRecurrencePattern ptrRecPatt;
			if (MyEvent.RecurrenceState == CalendarEventRecurrenceState.xtpCalendarRecurrenceNotRecurring)
			{
				ptrRecPatt = MyEvent.CreateRecurrence();
			}
			else
			{
				ptrRecPatt = MyEvent.RecurrencePattern;
				System.Diagnostics.Debug.Assert(ptrRecPatt != null, "Recurrence pattern must be not null!");
			}

			if (ptrRecPatt.Exceptions.Count > 0)
			{
				DialogResult dlgRes = MessageBox.Show("Any exceptions associated with this recurring appointment will be lost. Is this OK?", 
					"Recurrence Exceptions", MessageBoxButtons.OKCancel);
				if (dlgRes == DialogResult.Cancel) {
					return;
				}
				ptrRecPatt.RemoveAllExceptions();
			}
			
			// Appointment start Time
			try 
			{
				ptrRecPatt.StartTime = System.Convert.ToDateTime(cmbAppointmentStartTime.Text);
			} 
			catch {
				ptrRecPatt.StartTime = DateTime.Now;
			}

			//  Recurrence Duration Time
			try {
				string[] arDuration = cmbAppointmentDuration.Text.Split(' ');
				ptrRecPatt.DurationMinutes = System.Convert.ToInt32(arDuration[0]);
			} 
			catch{
				ptrRecPatt.DurationMinutes = 0;
			}

			// Recurrence Range
			ptrRecPatt.StartDate = dpStartDate.Value;

			//Set EndMethod, this is how and when the Occurrence will end
			if (optNoEndDate.Checked) 
			{
				ptrRecPatt.EndMethod = CalendarPatternEnd.xtpCalendarPatternEndNoDate;
			}
			else if (optEndAfter.Checked) 
			{
				ptrRecPatt.EndAfterOccurrences = Convert.ToInt32(txtOccurences.Text);
			}
			else
			{
				ptrRecPatt.EndDate = dpEndBy.Value;					
			}

			//Get Recurrence Options
			if (optDaily.Checked) 
			{
				ptrRecPatt.Options.RecurrenceType = CalendarRecurrenceType.xtpCalendarRecurrenceDaily;
				ptrRecPatt.Options.DailyEveryWeekDayOnly = (optDailyEveryWorkDay.Checked ? true : false);
				if (ptrRecPatt.Options.DailyEveryWeekDayOnly)
				{
					ptrRecPatt.Options.DailyIntervalDays = 1;
				}
				else
				{
					ptrRecPatt.Options.DailyIntervalDays = Convert.ToInt32(txtDailyEveryDay.Text);
				}
			}
			else if (optWeekly.Checked) 
			{
				ptrRecPatt.Options.RecurrenceType = CalendarRecurrenceType.xtpCalendarRecurrenceWeekly;
				ptrRecPatt.Options.WeeklyIntervalWeeks = Convert.ToInt32(txtWeeklyRecurEveryWeek.Text);
				
				ptrRecPatt.Options.WeeklyDayOfWeekMask = 0;

				CalendarWeekDay WeekMask = 0;
				WeekMask = (chkWeeklySunday.Checked ? (WeekMask | CalendarWeekDay.xtpCalendarDaySunday) : WeekMask );
				WeekMask = (chkWeeklyMonday.Checked ? (WeekMask | CalendarWeekDay.xtpCalendarDayMonday) : WeekMask );
				WeekMask = (chkWeeklyTuesday.Checked ? (WeekMask | CalendarWeekDay.xtpCalendarDayTuesday) : WeekMask );
				WeekMask = (chkWeeklyWednesday.Checked ? (WeekMask | CalendarWeekDay.xtpCalendarDayWednesday) : WeekMask );
				WeekMask = (chkWeeklyThursday.Checked ? (WeekMask | CalendarWeekDay.xtpCalendarDayThursday) : WeekMask );
				WeekMask = (chkWeeklyFriday.Checked ? (WeekMask | CalendarWeekDay.xtpCalendarDayFriday) : WeekMask );
				WeekMask = (chkWeeklySaturday.Checked ? (WeekMask | CalendarWeekDay.xtpCalendarDaySaturday) : WeekMask );

				ptrRecPatt.Options.WeeklyDayOfWeekMask = WeekMask;
			}
			else if (optMonthly.Checked) 
			{				
				if (optMonthlyDay.Checked)
				{
					ptrRecPatt.Options.RecurrenceType = CalendarRecurrenceType.xtpCalendarRecurrenceMonthly;
					ptrRecPatt.Options.MonthlyIntervalMonths = Convert.ToInt32(txtMonthlyEveryMonth.Text);
					ptrRecPatt.Options.MonthlyDayOfMonth = cmbMonthlyDayOfMonth.SelectedIndex + 1;
				}
				else
				{
					ptrRecPatt.Options.RecurrenceType = CalendarRecurrenceType.xtpCalendarRecurrenceMonthNth;
					ptrRecPatt.Options.MonthNthIntervalMonths = Convert.ToInt32(txtMonthlyOfEveryTheMonths.Text);
					ptrRecPatt.Options.MonthNthWhichDay = cmbMonthlyDay.SelectedIndex + 1;
					ptrRecPatt.Options.MonthNthWhichDayMask = (XtremeCalendarControl.CalendarWeekDay)SetDayMask(cmbMonthlyDayOfWeek.SelectedIndex);
				}

			}
			else 
			{
				if (optYearlyDay.Checked)
				{
					ptrRecPatt.Options.RecurrenceType = CalendarRecurrenceType.xtpCalendarRecurrenceYearly;
					ptrRecPatt.Options.YearlyDayOfMonth = Convert.ToInt32(cmbYearlyDate.Text);
					ptrRecPatt.Options.YearlyMonthOfYear = cmbYearlyEveryDate.SelectedIndex + 1;
				}
				else
				{
					ptrRecPatt.Options.RecurrenceType = CalendarRecurrenceType.xtpCalendarRecurrenceYearNth;
					ptrRecPatt.Options.YearNthMonthOfYear = cmbYearlyTheMonth.SelectedIndex + 1;
					ptrRecPatt.Options.YearNthWhichDay = cmdYearlyThePartOfWeek.SelectedIndex + 1;
					ptrRecPatt.Options.YearNthWhichDayMask = SetDayMask(cmbYearlyTheDay.SelectedIndex);
				}
			}

			//Update Event with new Recurrence
			MyEvent.UpdateRecurrence(ptrRecPatt);
			DialogResult = DialogResult.OK;
			this.Close();
		}
	
		protected override void OnLoad(EventArgs e)
		{			
			base.OnLoad (e);
			
			pnlYearly.Top = pnlDaily.Top;
			pnlWeekly.Top = pnlDaily.Top;
			pnlMonthly.Top = pnlDaily.Top;

			int nRowHeight = optYearly.Height;
			gbxRecurrencePattern.Height = optYearly.Bottom + nRowHeight;
			gbxRangeOfRecurrence.Top = gbxRecurrencePattern.Bottom + nRowHeight;

			cmdOK.Top = gbxRangeOfRecurrence.Bottom + 2* nRowHeight;
			cmdCancel.Top = cmdOK.Top;
			cmdRemoveRecurrence.Top = cmdOK.Top;

			Height = cmdOK.Bottom + 3 * nRowHeight;
			CenterToParent();

			if (MyEvent.RecurrenceState != CalendarEventRecurrenceState.xtpCalendarRecurrenceNotRecurring) 
			{
				if (MyEvent.RecurrencePattern.Options.RecurrenceType == CalendarRecurrenceType.xtpCalendarRecurrenceDaily) 
				{
					Daily();
				}
				else if (MyEvent.RecurrencePattern.Options.RecurrenceType == CalendarRecurrenceType.xtpCalendarRecurrenceMonthly) 
				{
					Monthly();
				}
				else if (MyEvent.RecurrencePattern.Options.RecurrenceType == CalendarRecurrenceType.xtpCalendarRecurrenceMonthNth) 
				{
					MonthlyNth();
				}
				else if (MyEvent.RecurrencePattern.Options.RecurrenceType == CalendarRecurrenceType.xtpCalendarRecurrenceWeekly) 
				{
					Weekly();
				}
				else if (MyEvent.RecurrencePattern.Options.RecurrenceType == CalendarRecurrenceType.xtpCalendarRecurrenceYearly) 
				{
					Yearly();
				}
				else if (MyEvent.RecurrencePattern.Options.RecurrenceType == CalendarRecurrenceType.xtpCalendarRecurrenceYearNth) 
				{
					YearlyNth();
				}
			}
			else
			{
				//By Default, Show Weekly View
				Weekly();
			}

			if (MyEvent.RecurrenceState != CalendarEventRecurrenceState.xtpCalendarRecurrenceNotRecurring) 
			{
				cmdRemoveRecurrence.Enabled = true;
			}
			else
			{
				cmdRemoveRecurrence.Enabled = false;
			}
		}

		private void cmbAppointmentStartTime_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			UpdateEndTimeCombo();		
		}

		private void cmbAppointmentStartTime_TextChanged(object sender, System.EventArgs e)
		{
			UpdateEndTimeCombo();
		}

		private void cmbAppointmentDuration_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			UpdateEndTimeCombo();
		}

		private void cmbAppointmentDuration_TextChanged(object sender, System.EventArgs e)
		{
			UpdateEndTimeCombo();
		}


		
	}
}
