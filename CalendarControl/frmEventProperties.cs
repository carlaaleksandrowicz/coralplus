using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using XtremeCalendarControl;

namespace CalendarControl
{
	public enum	Label
	{
		None = 0,
		Important = 1,
		Business = 2,
		Personal = 3,
		Vacation = 4,
		MustAttend = 5,
		TravelRequired = 6,
		NeedsPreparation = 7,
		Birthday = 8,
		Anniverserary = 9,
		PhoneCall = 10
	}

	/// <summary>
	/// Summary description for EventProperties.
	/// </summary>
	public class frmEventProperties : System.Windows.Forms.Form
	{
		private class CalReminderMinutes
		{
            public Int32 m_nMinutes;
            public String m_strMinutes;

            public CalReminderMinutes(Int32 nMinutes, String strMinutes)
			{
				m_nMinutes = nMinutes;
				m_strMinutes = strMinutes;
			}

            public CalReminderMinutes()
            {
				m_nMinutes = 0;
                m_strMinutes = "";	
			}

			public override String ToString() 
			{
				return m_strMinutes;
			}				
		}

		private System.Windows.Forms.Label lblSubject;
		private System.Windows.Forms.TextBox txtSubject;
		private System.Windows.Forms.Label lblLocation;
		private System.Windows.Forms.TextBox txtLocation;
		private System.Windows.Forms.Label lblLabel;
		private System.Windows.Forms.ComboBox cmbLabel;
		private System.Windows.Forms.Label lblShowTimeAs;
		private System.Windows.Forms.ComboBox cmbShowTimeAs;
		private System.Windows.Forms.Button cmdRecurrence;
		private System.Windows.Forms.Button cmdOK;
		private System.Windows.Forms.Button cmdCancel;
		private System.Windows.Forms.TextBox txtDescription;
		public CalendarEvent EditingEvent;
		public Boolean IsNewEvent = true;
		frmRecurrenceDialog frmRecurrenceDialog = null;		
		static public frmEventProperties Instance;
		public System.Windows.Forms.Panel pnlTimes;
		private System.Windows.Forms.ComboBox cmbEndTime;
		private System.Windows.Forms.ComboBox cmbStartTime;
		private System.Windows.Forms.CheckBox chkMeeting;
		private System.Windows.Forms.CheckBox chkPrivate;
		private System.Windows.Forms.CheckBox chkAllDayEvent;
		private System.Windows.Forms.DateTimePicker dpEndTimeDate;
		private System.Windows.Forms.DateTimePicker dpStartTimeDate;
		private System.Windows.Forms.Label lblEndTime;
		private System.Windows.Forms.Label lblStartTime;
		internal System.Windows.Forms.ComboBox cmbReminder;
		internal System.Windows.Forms.CheckBox chkReminder;
		public System.Windows.Forms.Label Line5;
		public System.Windows.Forms.Label Line4;
		public System.Windows.Forms.Label Line3;

		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public frmEventProperties()
		{
			//
			// Required for Windows Form Designer support
			//
			Instance = this;
			InitializeComponent();
			InitializeControls();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		private void InitializeControls()
		{
			cmbLabel.Items.Insert(0, "None");
			cmbLabel.Items.Insert(1, "Important");
			cmbLabel.Items.Insert(2, "Business");
			cmbLabel.Items.Insert(3, "Personal");
			cmbLabel.Items.Insert(4, "Vacation");
			cmbLabel.Items.Insert(5, "Must Attend");
			cmbLabel.Items.Insert(6, "Travel Required");
			cmbLabel.Items.Insert(7, "Needs Preparation");
			cmbLabel.Items.Insert(8, "Birthday");
			cmbLabel.Items.Insert(9, "Anniverserary");
			cmbLabel.Items.Insert(10, "Phone Call");
			cmbLabel.SelectedIndex = 0;

			cmbShowTimeAs.Items.Insert(0, "Free");
			cmbShowTimeAs.Items.Insert(1, "Tentative");
			cmbShowTimeAs.Items.Insert(2, "Busy");
			cmbShowTimeAs.Items.Insert(3, "Out of Office");
			cmbShowTimeAs.SelectedIndex = 0;

			DateTime dtHours = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0);

			for ( int cnt = 0; cnt < 48; cnt++ ) 
			{        		
				cmbStartTime.Items.Insert(cnt, dtHours.ToShortTimeString());
				cmbEndTime.Items.Insert(cnt, dtHours.ToShortTimeString());	
			
				dtHours = dtHours.AddMinutes(30);
			}

			cmbStartTime.SelectedIndex = 10;
			cmbEndTime.SelectedIndex = 11;

		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.lblSubject = new System.Windows.Forms.Label();
			this.txtSubject = new System.Windows.Forms.TextBox();
			this.lblLocation = new System.Windows.Forms.Label();
			this.txtLocation = new System.Windows.Forms.TextBox();
			this.lblLabel = new System.Windows.Forms.Label();
			this.cmbLabel = new System.Windows.Forms.ComboBox();
			this.lblShowTimeAs = new System.Windows.Forms.Label();
			this.cmbShowTimeAs = new System.Windows.Forms.ComboBox();
			this.cmdRecurrence = new System.Windows.Forms.Button();
			this.cmdOK = new System.Windows.Forms.Button();
			this.cmdCancel = new System.Windows.Forms.Button();
			this.txtDescription = new System.Windows.Forms.TextBox();
			this.pnlTimes = new System.Windows.Forms.Panel();
			this.cmbEndTime = new System.Windows.Forms.ComboBox();
			this.cmbStartTime = new System.Windows.Forms.ComboBox();
			this.chkAllDayEvent = new System.Windows.Forms.CheckBox();
			this.dpEndTimeDate = new System.Windows.Forms.DateTimePicker();
			this.dpStartTimeDate = new System.Windows.Forms.DateTimePicker();
			this.lblEndTime = new System.Windows.Forms.Label();
			this.lblStartTime = new System.Windows.Forms.Label();
			this.chkMeeting = new System.Windows.Forms.CheckBox();
			this.chkPrivate = new System.Windows.Forms.CheckBox();
			this.cmbReminder = new System.Windows.Forms.ComboBox();
			this.chkReminder = new System.Windows.Forms.CheckBox();
			this.Line5 = new System.Windows.Forms.Label();
			this.Line4 = new System.Windows.Forms.Label();
			this.Line3 = new System.Windows.Forms.Label();
			this.pnlTimes.SuspendLayout();
			this.SuspendLayout();
			// 
			// lblSubject
			// 
			this.lblSubject.Location = new System.Drawing.Point(16, 16);
			this.lblSubject.Name = "lblSubject";
			this.lblSubject.Size = new System.Drawing.Size(56, 16);
			this.lblSubject.TabIndex = 0;
			this.lblSubject.Text = "Subject:";
			// 
			// txtSubject
			// 
			this.txtSubject.Location = new System.Drawing.Point(72, 16);
			this.txtSubject.Name = "txtSubject";
			this.txtSubject.Size = new System.Drawing.Size(392, 20);
			this.txtSubject.TabIndex = 1;
			this.txtSubject.Text = "";
			// 
			// lblLocation
			// 
			this.lblLocation.Location = new System.Drawing.Point(16, 48);
			this.lblLocation.Name = "lblLocation";
			this.lblLocation.Size = new System.Drawing.Size(56, 16);
			this.lblLocation.TabIndex = 2;
			this.lblLocation.Text = "Location:";
			// 
			// txtLocation
			// 
			this.txtLocation.Location = new System.Drawing.Point(72, 40);
			this.txtLocation.Name = "txtLocation";
			this.txtLocation.Size = new System.Drawing.Size(392, 20);
			this.txtLocation.TabIndex = 3;
			this.txtLocation.Text = "";
			// 
			// lblLabel
			// 
			this.lblLabel.Location = new System.Drawing.Point(488, 24);
			this.lblLabel.Name = "lblLabel";
			this.lblLabel.Size = new System.Drawing.Size(80, 16);
			this.lblLabel.TabIndex = 4;
			this.lblLabel.Text = "Label:";
			// 
			// cmbLabel
			// 
			this.cmbLabel.Location = new System.Drawing.Point(576, 16);
			this.cmbLabel.Name = "cmbLabel";
			this.cmbLabel.Size = new System.Drawing.Size(121, 21);
			this.cmbLabel.TabIndex = 5;
			// 
			// lblShowTimeAs
			// 
			this.lblShowTimeAs.Location = new System.Drawing.Point(488, 56);
			this.lblShowTimeAs.Name = "lblShowTimeAs";
			this.lblShowTimeAs.Size = new System.Drawing.Size(80, 16);
			this.lblShowTimeAs.TabIndex = 15;
			this.lblShowTimeAs.Text = "Show Time As:";
			// 
			// cmbShowTimeAs
			// 
			this.cmbShowTimeAs.Location = new System.Drawing.Point(576, 48);
			this.cmbShowTimeAs.Name = "cmbShowTimeAs";
			this.cmbShowTimeAs.Size = new System.Drawing.Size(121, 21);
			this.cmbShowTimeAs.TabIndex = 16;
			// 
			// cmdRecurrence
			// 
			this.cmdRecurrence.Location = new System.Drawing.Point(624, 152);
			this.cmdRecurrence.Name = "cmdRecurrence";
			this.cmdRecurrence.TabIndex = 17;
			this.cmdRecurrence.Text = "Recurrence...";
			this.cmdRecurrence.Click += new System.EventHandler(this.cmdRecurrence_Click);
			// 
			// cmdOK
			// 
			this.cmdOK.Location = new System.Drawing.Point(264, 272);
			this.cmdOK.Name = "cmdOK";
			this.cmdOK.TabIndex = 18;
			this.cmdOK.Text = "OK";
			this.cmdOK.Click += new System.EventHandler(this.cmdOK_Click);
			// 
			// cmdCancel
			// 
			this.cmdCancel.Location = new System.Drawing.Point(360, 272);
			this.cmdCancel.Name = "cmdCancel";
			this.cmdCancel.TabIndex = 19;
			this.cmdCancel.Text = "Cancel";
			this.cmdCancel.Click += new System.EventHandler(this.cmdCancel_Click);
			// 
			// txtDescription
			// 
			this.txtDescription.Location = new System.Drawing.Point(8, 184);
			this.txtDescription.Multiline = true;
			this.txtDescription.Name = "txtDescription";
			this.txtDescription.ScrollBars = System.Windows.Forms.ScrollBars.Both;
			this.txtDescription.Size = new System.Drawing.Size(688, 64);
			this.txtDescription.TabIndex = 20;
			this.txtDescription.Text = "";
			// 
			// pnlTimes
			// 
			this.pnlTimes.Controls.AddRange(new System.Windows.Forms.Control[] {
																				   this.cmbEndTime,
																				   this.cmbStartTime,
																				   this.chkAllDayEvent,
																				   this.dpEndTimeDate,
																				   this.dpStartTimeDate,
																				   this.lblEndTime,
																				   this.lblStartTime});
			this.pnlTimes.Location = new System.Drawing.Point(8, 72);
			this.pnlTimes.Name = "pnlTimes";
			this.pnlTimes.Size = new System.Drawing.Size(456, 72);
			this.pnlTimes.TabIndex = 23;
			// 
			// cmbEndTime
			// 
			this.cmbEndTime.Location = new System.Drawing.Point(196, 40);
			this.cmbEndTime.Name = "cmbEndTime";
			this.cmbEndTime.Size = new System.Drawing.Size(121, 21);
			this.cmbEndTime.TabIndex = 31;
			// 
			// cmbStartTime
			// 
			this.cmbStartTime.Location = new System.Drawing.Point(196, 8);
			this.cmbStartTime.Name = "cmbStartTime";
			this.cmbStartTime.Size = new System.Drawing.Size(121, 21);
			this.cmbStartTime.TabIndex = 30;
			// 
			// chkAllDayEvent
			// 
			this.chkAllDayEvent.Location = new System.Drawing.Point(344, 8);
			this.chkAllDayEvent.Name = "chkAllDayEvent";
			this.chkAllDayEvent.TabIndex = 27;
			this.chkAllDayEvent.Text = "All Day Event";
			this.chkAllDayEvent.CheckedChanged += new System.EventHandler(this.chkAllDayEvent_CheckedChanged);
			// 
			// dpEndTimeDate
			// 
			this.dpEndTimeDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
			this.dpEndTimeDate.Location = new System.Drawing.Point(76, 40);
			this.dpEndTimeDate.Name = "dpEndTimeDate";
			this.dpEndTimeDate.Size = new System.Drawing.Size(116, 20);
			this.dpEndTimeDate.TabIndex = 26;
			// 
			// dpStartTimeDate
			// 
			this.dpStartTimeDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
			this.dpStartTimeDate.Location = new System.Drawing.Point(76, 8);
			this.dpStartTimeDate.Name = "dpStartTimeDate";
			this.dpStartTimeDate.Size = new System.Drawing.Size(116, 20);
			this.dpStartTimeDate.TabIndex = 25;
			// 
			// lblEndTime
			// 
			this.lblEndTime.Location = new System.Drawing.Point(4, 40);
			this.lblEndTime.Name = "lblEndTime";
			this.lblEndTime.TabIndex = 24;
			this.lblEndTime.Text = "End Time:";
			// 
			// lblStartTime
			// 
			this.lblStartTime.Location = new System.Drawing.Point(4, 8);
			this.lblStartTime.Name = "lblStartTime";
			this.lblStartTime.TabIndex = 23;
			this.lblStartTime.Text = "Start Time:";
			// 
			// chkMeeting
			// 
			this.chkMeeting.Location = new System.Drawing.Point(488, 104);
			this.chkMeeting.Name = "chkMeeting";
			this.chkMeeting.Size = new System.Drawing.Size(104, 16);
			this.chkMeeting.TabIndex = 29;
			this.chkMeeting.Text = " Meeting";
			// 
			// chkPrivate
			// 
			this.chkPrivate.Location = new System.Drawing.Point(488, 80);
			this.chkPrivate.Name = "chkPrivate";
			this.chkPrivate.Size = new System.Drawing.Size(104, 16);
			this.chkPrivate.TabIndex = 28;
			this.chkPrivate.Text = "Private";
			// 
			// cmbReminder
			// 
			this.cmbReminder.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cmbReminder.Location = new System.Drawing.Point(88, 152);
			this.cmbReminder.Name = "cmbReminder";
			this.cmbReminder.Size = new System.Drawing.Size(121, 21);
			this.cmbReminder.TabIndex = 36;
			// 
			// chkReminder
			// 
			this.chkReminder.Location = new System.Drawing.Point(16, 152);
			this.chkReminder.Name = "chkReminder";
			this.chkReminder.Size = new System.Drawing.Size(72, 24);
			this.chkReminder.TabIndex = 35;
			this.chkReminder.Text = "Reminder";
			this.chkReminder.CheckedChanged += new System.EventHandler(this.chkReminder_CheckedChanged);
			// 
			// Line5
			// 
			this.Line5.BackColor = System.Drawing.SystemColors.ControlDark;
			this.Line5.Location = new System.Drawing.Point(480, 144);
			this.Line5.Name = "Line5";
			this.Line5.Size = new System.Drawing.Size(216, 1);
			this.Line5.TabIndex = 38;
			// 
			// Line4
			// 
			this.Line4.BackColor = System.Drawing.SystemColors.ControlDark;
			this.Line4.Location = new System.Drawing.Point(480, 72);
			this.Line4.Name = "Line4";
			this.Line4.Size = new System.Drawing.Size(216, 1);
			this.Line4.TabIndex = 39;
			// 
			// Line3
			// 
			this.Line3.BackColor = System.Drawing.SystemColors.ControlDark;
			this.Line3.Location = new System.Drawing.Point(480, 16);
			this.Line3.Name = "Line3";
			this.Line3.Size = new System.Drawing.Size(1, 128);
			this.Line3.TabIndex = 37;
			// 
			// frmEventProperties
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(706, 311);
			this.Controls.AddRange(new System.Windows.Forms.Control[] {
																		  this.Line5,
																		  this.Line4,
																		  this.Line3,
																		  this.cmbReminder,
																		  this.chkReminder,
																		  this.txtDescription,
																		  this.txtLocation,
																		  this.txtSubject,
																		  this.cmdCancel,
																		  this.cmdOK,
																		  this.cmdRecurrence,
																		  this.cmbShowTimeAs,
																		  this.lblShowTimeAs,
																		  this.cmbLabel,
																		  this.lblLabel,
																		  this.lblLocation,
																		  this.lblSubject,
																		  this.pnlTimes,
																		  this.chkPrivate,
																		  this.chkMeeting});
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "frmEventProperties";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Event Properties";
			this.Closed += new System.EventHandler(this.frmEventProperties_Closed);
			this.pnlTimes.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		public void CreateNewEvent()
		{
			EditingEvent = frmMain.Instance.wndCalendarControl.DataProvider.CreateEvent();
			IsNewEvent = true;

			DateTime BeginSelection, EndSelection;
			Boolean AllDay;
			
			BeginSelection = EndSelection = DateTime.Now;
			AllDay = false;

			frmMain.Instance.wndCalendarControl.ActiveView.GetSelection(ref BeginSelection, ref EndSelection, ref AllDay);

			SetStartEnd(BeginSelection, EndSelection, AllDay);
			chkAllDayEvent.Checked = (AllDay ? true : false);

			txtSubject.Text = "New Event";

			cmbShowTimeAs.SelectedIndex = (AllDay ? 0 : 2);
			cmbLabel.SelectedIndex = 0;

			frmMain.Instance.ContextEvent = null;
		}

		public void SetStartEnd(DateTime BeginSelection, DateTime EndSelection, Boolean AllDay)
		{
			DateTime StartDate, StartTime, EndDate, EndTime;

			StartDate = BeginSelection.Date;
			StartTime = BeginSelection;

			EndDate = EndSelection.Date;
			EndTime = EndSelection;

			if(AllDay)
			{
				cmbStartTime.Visible = false;
				cmbEndTime.Visible = false;
			}

			dpStartTimeDate.Value = StartDate;
			if (StartTime.Minute < 30)
			{
				cmbStartTime.SelectedIndex = (int)(StartTime.Hour * 2);
			}
			else
			{
				cmbStartTime.SelectedIndex = (int)(StartTime.Hour * 2 + 1);
			}

			//UpdateEndTimeCombo();

			dpEndTimeDate.Value = EndDate;
			if (EndTime.Minute < 30)
			{
				cmbEndTime.SelectedIndex = (int)(EndTime.Hour * 2);
			}
			else
			{
				cmbEndTime.SelectedIndex = (int)(EndTime.Hour * 2 + 1);
			}
		}

//		private void UpdateEndTimeCombo()
//		{
//			cmbEndTime.Items.Clear();
//
//			//18000000000 Ticks = 30 Minutes
//			DateTime BeginTime = new DateTime(cmbStartTime.SelectedIndex * 18000000000);
//
//			cmbEndTime.Items.Add(BeginTime.ToShortTimeString() + " (0 minutes)");
//			BeginTime = BeginTime.AddMinutes(30);
//			cmbEndTime.Items.Add(BeginTime.ToShortTimeString() + " (30 minutes)");
//			BeginTime = BeginTime.AddMinutes(30);
//			cmbEndTime.Items.Add(BeginTime.ToShortTimeString() + " (1 hour)");
//
//			for (int i = 3; i <= 47; i++)
//			{
//				BeginTime = BeginTime.AddMinutes(30);
//				if (i % 2 > 0)
//				{
//					cmbEndTime.Items.Add(BeginTime.ToShortTimeString() + " (" + i / 2 + ".5 hours)");
//				}
//				else
//				{
//					cmbEndTime.Items.Add(BeginTime.ToShortTimeString() + " (" + i / 2 + " hours)");
//				}
//			}
//
//			cmbEndTime.SelectedIndex = 1;
//		}

		private void chkAllDayEvent_CheckedChanged(object sender, System.EventArgs e)
		{
			cmbEndTime.Visible = (chkAllDayEvent.Checked ? false : true);
			cmbStartTime.Visible = (chkAllDayEvent.Checked ? false : true);
		}

		private void cmbStartTime_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			//UpdateEndTimeCombo();
		}

		private void cmdCancel_Click(object sender, System.EventArgs e)
		{
			EditingEvent = null;
			frmMain.Instance.ContextEvent = null;
			this.Close();
		}

		private void dpStartTimeDate_ValueChanged(object sender, System.EventArgs e)
		{
			if (dpStartTimeDate.Value.CompareTo(dpEndTimeDate.Value) > 0) 
			{
				dpEndTimeDate.Value = dpStartTimeDate.Value;
			}
		}

		private void dpEndTimeDate_ValueChanged(object sender, System.EventArgs e)
		{
			if (dpEndTimeDate.Value.CompareTo(dpStartTimeDate.Value) < 0) 
			{
				dpStartTimeDate.Value = dpEndTimeDate.Value;
			}		
		}

		private void cmdOK_Click(object sender, System.EventArgs e)
		{
			if(EditingEvent.RecurrenceState == CalendarEventRecurrenceState.xtpCalendarRecurrenceNotRecurring ||
				EditingEvent.RecurrenceState == CalendarEventRecurrenceState.xtpCalendarRecurrenceException)
			{
				DateTime StartTime, EndTime;

				StartTime = dpStartTimeDate.Value;
				EndTime = dpEndTimeDate.Value;

				StartTime = StartTime.AddHours(cmbStartTime.SelectedIndex / 2);
				EndTime = EndTime.AddHours(cmbEndTime.SelectedIndex / 2);

				if (cmbStartTime.SelectedIndex % 2 > 0)
				{
					StartTime = StartTime.AddMinutes(30);
				}

				if (cmbEndTime.SelectedIndex % 2 > 0)
				{
					EndTime = EndTime.AddMinutes(30);
				}		
				
				EditingEvent.StartTime = StartTime;
				EditingEvent.EndTime = EndTime;
			}

			EditingEvent.Subject = txtSubject.Text;
			EditingEvent.Location = txtLocation.Text;
			EditingEvent.Body = txtDescription.Text;
			
			EditingEvent.AllDayEvent = (chkAllDayEvent.Checked ? true : false);
			EditingEvent.Label = cmbLabel.SelectedIndex;
			EditingEvent.BusyStatus = (CalendarEventBusyStatus)cmbShowTimeAs.SelectedIndex;

			EditingEvent.PrivateFlag = (chkPrivate.Checked ? true : false);
			EditingEvent.MeetingFlag = (chkMeeting.Checked ? true : false);

			EditingEvent.Reminder = chkReminder.Checked;
			if (EditingEvent.Reminder)
			{
				CalReminderMinutes pRmd = (CalReminderMinutes)cmbReminder.SelectedItem;
				if (pRmd != null)
				{
					EditingEvent.ReminderMinutesBeforeStart = pRmd.m_nMinutes;
				}
			}

			if (IsNewEvent) {				
				frmMain.Instance.wndCalendarControl.DataProvider.AddEvent(EditingEvent);
			}
			else {
				frmMain.Instance.wndCalendarControl.DataProvider.ChangeEvent(EditingEvent);
			}

			EditingEvent = null;
			frmMain.Instance.ContextEvent = null;

			frmMain.Instance.wndCalendarControl.Populate();
			frmMain.Instance.wndCalendarControl.RedrawControl();

			this.Close();

			frmMain.Instance.Activate();
		}

		public void ModifyEvent(CalendarEvent ModEvent)
		{
			IsNewEvent = false;

			txtSubject.Text = ModEvent.Subject;
			txtDescription.Text = ModEvent.Body;
			txtLocation.Text = ModEvent.Location;

			chkAllDayEvent.Checked = (ModEvent.AllDayEvent ? true : false);
			cmbLabel.SelectedIndex = ModEvent.Label;
			cmbShowTimeAs.SelectedIndex = (int)ModEvent.BusyStatus;

			chkPrivate.Checked = (ModEvent.PrivateFlag ? true : false);
			chkMeeting.Checked = (ModEvent.MeetingFlag ? true : false);

			if(ModEvent.RecurrenceState == CalendarEventRecurrenceState.xtpCalendarRecurrenceNotRecurring ||
				ModEvent.RecurrenceState == CalendarEventRecurrenceState.xtpCalendarRecurrenceException)
			{
				SetStartEnd(ModEvent.StartTime, ModEvent.EndTime, ModEvent.AllDayEvent);
			}

			EditingEvent = ModEvent;

			if (ModEvent.Subject != "")
			{
				this.Text = ModEvent.Subject + " - Event";
			}
		}

		private void cmdRecurrence_Click(object sender, System.EventArgs e)
		{
			frmRecurrenceDialog = new frmRecurrenceDialog();

			if (EditingEvent.RecurrenceState == CalendarEventRecurrenceState.xtpCalendarRecurrenceOccurrence ||
				EditingEvent.RecurrenceState == CalendarEventRecurrenceState.xtpCalendarRecurrenceException) 
			{
				EditingEvent = EditingEvent.RecurrencePattern.MasterEvent;
			}
			frmRecurrenceDialog.MyEvent = EditingEvent.CloneEvent();

			if (frmRecurrenceDialog.ShowDialog(this) == DialogResult.OK) 
			{
				EditingEvent = frmRecurrenceDialog.MyEvent;

				pnlTimes.Visible = EditingEvent.RecurrenceState != CalendarEventRecurrenceState.xtpCalendarRecurrenceMaster;
			}
		}

		private void frmEventProperties_Closed(object sender, System.EventArgs e)
		{
			frmMain.Instance.Activate();
		}
	
		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad (e);

			if (EditingEvent != null)
			{
				if (EditingEvent.RecurrenceState == CalendarEventRecurrenceState.xtpCalendarRecurrenceMaster)
				{
					pnlTimes.Visible = false;
				}
			}
			else
			{
				pnlTimes.Visible = true;
			}

			//-----------------------------------------------------------------
            cmbReminder.Items.Add(new CalReminderMinutes(0, "0 minutes"));
            cmbReminder.Items.Add(new CalReminderMinutes(1, "1 minute"));
            cmbReminder.Items.Add(new CalReminderMinutes(5, "5 minutes"));
            cmbReminder.Items.Add(new CalReminderMinutes(10, "10 minutes"));
            cmbReminder.Items.Add(new CalReminderMinutes(15, "15 minutes"));
            cmbReminder.Items.Add(new CalReminderMinutes(30, "30 minutes"));

            cmbReminder.Items.Add(new CalReminderMinutes(60, "1 hour"));
            cmbReminder.Items.Add(new CalReminderMinutes(120, "2 hour"));
            cmbReminder.Items.Add(new CalReminderMinutes(180, "3 hour"));
            cmbReminder.Items.Add(new CalReminderMinutes(60 * 12, "12 hour"));

            cmbReminder.Items.Add(new CalReminderMinutes(60 * 24, "1 day"));
            cmbReminder.Items.Add(new CalReminderMinutes(60 * 24 * 2, "2 days"));
            cmbReminder.Items.Add(new CalReminderMinutes(60 * 24 * 3, "3 days"));

            cmbReminder.Items.Add(new CalReminderMinutes(60 * 24 * 7, "1 week"));
            cmbReminder.Items.Add(new CalReminderMinutes(60 * 24 * 7 * 2, "2 weeks"));

            chkReminder.Checked = EditingEvent.Reminder;
            chkReminder_CheckedChanged(this, new System.EventArgs());

            Boolean bRmdExist = false;
			CalReminderMinutes pRmd = null;
			for (int i = 0; i < cmbReminder.Items.Count; i++ )
			{
				pRmd = (CalReminderMinutes)cmbReminder.Items[i];

				if (pRmd.m_nMinutes == EditingEvent.ReminderMinutesBeforeStart)
				{
					bRmdExist = true;
					break;
				}
			}

            if (!bRmdExist)
			{
                pRmd = new CalReminderMinutes();
                pRmd.m_nMinutes = EditingEvent.ReminderMinutesBeforeStart;
                pRmd.m_strMinutes = EditingEvent.ReminderMinutesBeforeStart.ToString() + " minutes";
				cmbReminder.Items.Add(pRmd);
			}

            cmbReminder.SelectedItem = pRmd;
		}

		private void chkReminder_CheckedChanged(object sender, System.EventArgs e)
		{
			cmbReminder.Enabled = chkReminder.Checked;
		}

	}
}
