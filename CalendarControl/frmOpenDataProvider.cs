using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace CalendarControl
{
	public enum DataProviderType 
	{
		dpMemory = 1,
		dpDB,
		dpMAPI,
	}
	public enum DataProviderTypeCustom 
	{
		dpSQLServer = 10,
	}
	/// <summary>
	/// Summary description for frmOpenDataProvider.
	/// </summary>
	public class frmOpenDataProvider : System.Windows.Forms.Form
	{
		protected String m_strConnectionString;
		protected int	 m_eDataProviderType;

		private System.Windows.Forms.GroupBox groupBoxDP_mem;
		private System.Windows.Forms.RadioButton radioBtnUseDP_mem;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Label labelDPfile_mem;
		private System.Windows.Forms.RadioButton radioBtnUseDP_mdb;
		private System.Windows.Forms.RadioButton radioBtnUseDP_SQLsrv;
		private System.Windows.Forms.Button btnOK;
		private System.Windows.Forms.Button btnCancel;
		private System.Windows.Forms.TextBox textDPfile_mem;
		private System.Windows.Forms.GroupBox groupBoxDP_mdb;
		private System.Windows.Forms.Label labelDPfile_mdb;
		private System.Windows.Forms.TextBox textDPfile_mdb;
		private System.Windows.Forms.GroupBox groupBoxDP_mapi;
		private System.Windows.Forms.Label labelDPfile_mapi;
		private System.Windows.Forms.RadioButton radioBtnUseDP_mapi;
		private System.Windows.Forms.GroupBox groupBoxDP_SQLSrv;
		private System.Windows.Forms.Label labelDPfile_SQLsrv;
		private System.Windows.Forms.TextBox textDPfile_SQLsrv;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public frmOpenDataProvider()
		{
			m_strConnectionString = "";
			m_eDataProviderType = (int)DataProviderType.dpMemory;
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.groupBoxDP_mem = new System.Windows.Forms.GroupBox();
            this.labelDPfile_mem = new System.Windows.Forms.Label();
            this.textDPfile_mem = new System.Windows.Forms.TextBox();
            this.radioBtnUseDP_mem = new System.Windows.Forms.RadioButton();
            this.radioBtnUseDP_mdb = new System.Windows.Forms.RadioButton();
            this.groupBoxDP_mdb = new System.Windows.Forms.GroupBox();
            this.labelDPfile_mdb = new System.Windows.Forms.Label();
            this.textDPfile_mdb = new System.Windows.Forms.TextBox();
            this.groupBoxDP_mapi = new System.Windows.Forms.GroupBox();
            this.labelDPfile_mapi = new System.Windows.Forms.Label();
            this.radioBtnUseDP_mapi = new System.Windows.Forms.RadioButton();
            this.radioBtnUseDP_SQLsrv = new System.Windows.Forms.RadioButton();
            this.groupBoxDP_SQLSrv = new System.Windows.Forms.GroupBox();
            this.labelDPfile_SQLsrv = new System.Windows.Forms.Label();
            this.textDPfile_SQLsrv = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnOK = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.groupBoxDP_mem.SuspendLayout();
            this.groupBoxDP_mdb.SuspendLayout();
            this.groupBoxDP_mapi.SuspendLayout();
            this.groupBoxDP_SQLSrv.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBoxDP_mem
            // 
            this.groupBoxDP_mem.Controls.Add(this.labelDPfile_mem);
            this.groupBoxDP_mem.Controls.Add(this.textDPfile_mem);
            this.groupBoxDP_mem.Location = new System.Drawing.Point(24, 40);
            this.groupBoxDP_mem.Name = "groupBoxDP_mem";
            this.groupBoxDP_mem.Size = new System.Drawing.Size(472, 64);
            this.groupBoxDP_mem.TabIndex = 0;
            this.groupBoxDP_mem.TabStop = false;
            // 
            // labelDPfile_mem
            // 
            this.labelDPfile_mem.Location = new System.Drawing.Point(8, 16);
            this.labelDPfile_mem.Name = "labelDPfile_mem";
            this.labelDPfile_mem.Size = new System.Drawing.Size(100, 16);
            this.labelDPfile_mem.TabIndex = 1;
            this.labelDPfile_mem.Text = "Data file:";
            // 
            // textDPfile_mem
            // 
            this.textDPfile_mem.Location = new System.Drawing.Point(8, 32);
            this.textDPfile_mem.Name = "textDPfile_mem";
            this.textDPfile_mem.Size = new System.Drawing.Size(456, 20);
            this.textDPfile_mem.TabIndex = 0;
            this.textDPfile_mem.Text = "textBox1";
            // 
            // radioBtnUseDP_mem
            // 
            this.radioBtnUseDP_mem.Enabled = false;
            this.radioBtnUseDP_mem.Location = new System.Drawing.Point(8, 16);
            this.radioBtnUseDP_mem.Name = "radioBtnUseDP_mem";
            this.radioBtnUseDP_mem.Size = new System.Drawing.Size(216, 24);
            this.radioBtnUseDP_mem.TabIndex = 1;
            this.radioBtnUseDP_mem.Text = "Use Memory data provider";
            this.radioBtnUseDP_mem.CheckedChanged += new System.EventHandler(this.radioBtnUseDP_mem_CheckedChanged);
            // 
            // radioBtnUseDP_mdb
            // 
            this.radioBtnUseDP_mdb.Enabled = false;
            this.radioBtnUseDP_mdb.Location = new System.Drawing.Point(8, 120);
            this.radioBtnUseDP_mdb.Name = "radioBtnUseDP_mdb";
            this.radioBtnUseDP_mdb.Size = new System.Drawing.Size(216, 24);
            this.radioBtnUseDP_mdb.TabIndex = 1;
            this.radioBtnUseDP_mdb.Text = "Use Data Base data provider (Access)";
            this.radioBtnUseDP_mdb.CheckedChanged += new System.EventHandler(this.radioBtnUseDP_mdb_CheckedChanged);
            // 
            // groupBoxDP_mdb
            // 
            this.groupBoxDP_mdb.Controls.Add(this.labelDPfile_mdb);
            this.groupBoxDP_mdb.Controls.Add(this.textDPfile_mdb);
            this.groupBoxDP_mdb.Location = new System.Drawing.Point(24, 144);
            this.groupBoxDP_mdb.Name = "groupBoxDP_mdb";
            this.groupBoxDP_mdb.Size = new System.Drawing.Size(472, 64);
            this.groupBoxDP_mdb.TabIndex = 0;
            this.groupBoxDP_mdb.TabStop = false;
            // 
            // labelDPfile_mdb
            // 
            this.labelDPfile_mdb.Location = new System.Drawing.Point(8, 16);
            this.labelDPfile_mdb.Name = "labelDPfile_mdb";
            this.labelDPfile_mdb.Size = new System.Drawing.Size(208, 16);
            this.labelDPfile_mdb.TabIndex = 1;
            this.labelDPfile_mdb.Text = "Data Base file (*.mdb):";
            // 
            // textDPfile_mdb
            // 
            this.textDPfile_mdb.Location = new System.Drawing.Point(8, 32);
            this.textDPfile_mdb.Name = "textDPfile_mdb";
            this.textDPfile_mdb.Size = new System.Drawing.Size(456, 20);
            this.textDPfile_mdb.TabIndex = 0;
            this.textDPfile_mdb.Text = "textBox1";
            // 
            // groupBoxDP_mapi
            // 
            this.groupBoxDP_mapi.Controls.Add(this.labelDPfile_mapi);
            this.groupBoxDP_mapi.Location = new System.Drawing.Point(24, 248);
            this.groupBoxDP_mapi.Name = "groupBoxDP_mapi";
            this.groupBoxDP_mapi.Size = new System.Drawing.Size(472, 40);
            this.groupBoxDP_mapi.TabIndex = 0;
            this.groupBoxDP_mapi.TabStop = false;
            // 
            // labelDPfile_mapi
            // 
            this.labelDPfile_mapi.Location = new System.Drawing.Point(8, 16);
            this.labelDPfile_mapi.Name = "labelDPfile_mapi";
            this.labelDPfile_mapi.Size = new System.Drawing.Size(456, 16);
            this.labelDPfile_mapi.TabIndex = 1;
            this.labelDPfile_mapi.Text = "User default connection settings are used.";
            // 
            // radioBtnUseDP_mapi
            // 
            this.radioBtnUseDP_mapi.Location = new System.Drawing.Point(8, 224);
            this.radioBtnUseDP_mapi.Name = "radioBtnUseDP_mapi";
            this.radioBtnUseDP_mapi.Size = new System.Drawing.Size(216, 24);
            this.radioBtnUseDP_mapi.TabIndex = 1;
            this.radioBtnUseDP_mapi.Text = "Use MAPI data provider";
            this.radioBtnUseDP_mapi.CheckedChanged += new System.EventHandler(this.radioBtnUseDP_mapi_CheckedChanged);
            // 
            // radioBtnUseDP_SQLsrv
            // 
            this.radioBtnUseDP_SQLsrv.Enabled = false;
            this.radioBtnUseDP_SQLsrv.Location = new System.Drawing.Point(8, 312);
            this.radioBtnUseDP_SQLsrv.Name = "radioBtnUseDP_SQLsrv";
            this.radioBtnUseDP_SQLsrv.Size = new System.Drawing.Size(216, 24);
            this.radioBtnUseDP_SQLsrv.TabIndex = 1;
            this.radioBtnUseDP_SQLsrv.Text = "Use SQL Server data provider";
            this.radioBtnUseDP_SQLsrv.CheckedChanged += new System.EventHandler(this.radioBtnUseDP_SQLsrv_CheckedChanged);
            // 
            // groupBoxDP_SQLSrv
            // 
            this.groupBoxDP_SQLSrv.Controls.Add(this.labelDPfile_SQLsrv);
            this.groupBoxDP_SQLSrv.Controls.Add(this.textDPfile_SQLsrv);
            this.groupBoxDP_SQLSrv.Location = new System.Drawing.Point(24, 336);
            this.groupBoxDP_SQLSrv.Name = "groupBoxDP_SQLSrv";
            this.groupBoxDP_SQLSrv.Size = new System.Drawing.Size(472, 64);
            this.groupBoxDP_SQLSrv.TabIndex = 0;
            this.groupBoxDP_SQLSrv.TabStop = false;
            // 
            // labelDPfile_SQLsrv
            // 
            this.labelDPfile_SQLsrv.Location = new System.Drawing.Point(8, 16);
            this.labelDPfile_SQLsrv.Name = "labelDPfile_SQLsrv";
            this.labelDPfile_SQLsrv.Size = new System.Drawing.Size(100, 16);
            this.labelDPfile_SQLsrv.TabIndex = 1;
            this.labelDPfile_SQLsrv.Text = "Connection string:";
            // 
            // textDPfile_SQLsrv
            // 
            this.textDPfile_SQLsrv.Location = new System.Drawing.Point(8, 32);
            this.textDPfile_SQLsrv.Name = "textDPfile_SQLsrv";
            this.textDPfile_SQLsrv.Size = new System.Drawing.Size(456, 20);
            this.textDPfile_SQLsrv.TabIndex = 0;
            this.textDPfile_SQLsrv.Text = "textBox1";
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.ForeColor = System.Drawing.Color.Blue;
            this.label5.ImageAlign = System.Drawing.ContentAlignment.TopRight;
            this.label5.Location = new System.Drawing.Point(392, 24);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(100, 16);
            this.label5.TabIndex = 2;
            this.label5.Text = "Built-in";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label6.ForeColor = System.Drawing.Color.Blue;
            this.label6.ImageAlign = System.Drawing.ContentAlignment.TopRight;
            this.label6.Location = new System.Drawing.Point(392, 128);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(100, 16);
            this.label6.TabIndex = 2;
            this.label6.Text = "Built-in";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label7.ForeColor = System.Drawing.Color.Blue;
            this.label7.ImageAlign = System.Drawing.ContentAlignment.TopRight;
            this.label7.Location = new System.Drawing.Point(392, 232);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(100, 16);
            this.label7.TabIndex = 2;
            this.label7.Text = "Built-in";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label8
            // 
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.label8.ImageAlign = System.Drawing.ContentAlignment.TopRight;
            this.label8.Location = new System.Drawing.Point(392, 320);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(100, 16);
            this.label8.TabIndex = 2;
            this.label8.Text = "custom";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Location = new System.Drawing.Point(8, 304);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(488, 2);
            this.panel1.TabIndex = 3;
            // 
            // btnOK
            // 
            this.btnOK.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOK.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnOK.Location = new System.Drawing.Point(168, 416);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 4;
            this.btnOK.Text = "OK";
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(256, 416);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 5;
            this.btnCancel.Text = "Cancel";
            // 
            // frmOpenDataProvider
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(504, 445);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.radioBtnUseDP_mem);
            this.Controls.Add(this.groupBoxDP_mem);
            this.Controls.Add(this.radioBtnUseDP_mdb);
            this.Controls.Add(this.groupBoxDP_mdb);
            this.Controls.Add(this.groupBoxDP_mapi);
            this.Controls.Add(this.radioBtnUseDP_mapi);
            this.Controls.Add(this.radioBtnUseDP_SQLsrv);
            this.Controls.Add(this.groupBoxDP_SQLSrv);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label8);
            this.Name = "frmOpenDataProvider";
            this.Text = "Open Data Provider";
            this.Closing += new System.ComponentModel.CancelEventHandler(this.frmOpenDataProvider_Closing);
            this.Load += new System.EventHandler(this.frmOpenDataProvider_Load);
            this.groupBoxDP_mem.ResumeLayout(false);
            this.groupBoxDP_mem.PerformLayout();
            this.groupBoxDP_mdb.ResumeLayout(false);
            this.groupBoxDP_mdb.PerformLayout();
            this.groupBoxDP_mapi.ResumeLayout(false);
            this.groupBoxDP_SQLSrv.ResumeLayout(false);
            this.groupBoxDP_SQLSrv.PerformLayout();
            this.ResumeLayout(false);

		}
		#endregion


		private void EnableControls(int eDPType)
		{
			labelDPfile_mem.Enabled = (eDPType == (int)DataProviderType.dpMemory);
			textDPfile_mem.Enabled = (eDPType == (int)DataProviderType.dpMemory);
			
			labelDPfile_mdb.Enabled = (eDPType == (int)DataProviderType.dpDB);
			textDPfile_mdb.Enabled = (eDPType == (int)DataProviderType.dpDB);
						
			labelDPfile_mapi.Enabled = (eDPType == (int)DataProviderType.dpMAPI);
						
			labelDPfile_SQLsrv.Enabled = (eDPType == (int)DataProviderTypeCustom.dpSQLServer);
			textDPfile_SQLsrv.Enabled = (eDPType == (int)DataProviderTypeCustom.dpSQLServer);
		}

		private void frmOpenDataProvider_Load(object sender, System.EventArgs e)
		{
            //EnableControls((int)DataProviderType.dpMemory);
            //radioBtnUseDP_mem.Checked = true;
            radioBtnUseDP_mapi.Checked = true;

            textDPfile_mem.Text = (String)Application.UserAppDataRegistry.GetValue("DataSource_mem", "events.xml");
			textDPfile_mdb.Text = (String)Application.UserAppDataRegistry.GetValue("DataSource_mdb", "events.mdb");
			textDPfile_SQLsrv.Text = (String)Application.UserAppDataRegistry.GetValue("DataSource_SQLsrv", "Provider=SQLOLEDB.1;...");
		}

		private void frmOpenDataProvider_Closing(object sender, System.ComponentModel.CancelEventArgs e)
		{
			Application.UserAppDataRegistry.SetValue("DataSource_mem", textDPfile_mem.Text);
			Application.UserAppDataRegistry.SetValue("DataSource_mdb", textDPfile_mdb.Text);
			Application.UserAppDataRegistry.SetValue("DataSource_SQLsrv", textDPfile_SQLsrv.Text);
		}		

		private void radioBtnUseDP_mem_CheckedChanged(object sender, System.EventArgs e)
		{
			if (radioBtnUseDP_mem.Checked) 
			{
				EnableControls((int)DataProviderType.dpMemory);
			}
		}

		private void radioBtnUseDP_mdb_CheckedChanged(object sender, System.EventArgs e)
		{
			if (radioBtnUseDP_mdb.Checked) 
			{
				EnableControls((int)DataProviderType.dpDB);
			}
		}

		private void radioBtnUseDP_mapi_CheckedChanged(object sender, System.EventArgs e)
		{
			if (radioBtnUseDP_mapi.Checked) 
			{
				EnableControls((int)DataProviderType.dpMAPI);
			}
		}

		private void radioBtnUseDP_SQLsrv_CheckedChanged(object sender, System.EventArgs e)
		{
			if (radioBtnUseDP_SQLsrv.Checked) 
			{
				EnableControls((int)DataProviderTypeCustom.dpSQLServer);
			}
		}

		private void btnOK_Click(object sender, System.EventArgs e)
		{
			m_eDataProviderType = (int)DataProviderType.dpMemory;
			m_strConnectionString = "";		
			
			if (radioBtnUseDP_mem.Checked) 
			{
				m_eDataProviderType = (int)DataProviderType.dpMemory;				
				m_strConnectionString = "Provider=XML;Data Source='" + textDPfile_mem.Text + "';Encoding=iso-8859-1;";
			}
			else if (radioBtnUseDP_mdb.Checked) 
			{
				m_eDataProviderType = (int)DataProviderType.dpDB;
				m_strConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source='" + textDPfile_mdb.Text + "';";
			}
			else if (radioBtnUseDP_mapi.Checked) 
			{
				m_eDataProviderType = (int)DataProviderType.dpMAPI;
				m_strConnectionString = "Provider=MAPI;";
			}
			else if (radioBtnUseDP_SQLsrv.Checked) 
			{
				m_eDataProviderType = (int)DataProviderTypeCustom.dpSQLServer;
				m_strConnectionString = textDPfile_SQLsrv.Text;
			}
			else {
				System.Diagnostics.Debug.Assert(false);
				DialogResult = DialogResult.None;
			}					
		}
		
		public String GetConnectionString() 
		{
			return m_strConnectionString;
		}

		public int GetDataProviderType() 
		{
			return m_eDataProviderType;
		}
		
	}
}
