using System;
using XtremeCalendarControl;

namespace CalendarControl
{	
	/// <summary>
	/// Summary description for providerSQLServer.
	/// </summary>
	public class SQLDataHelper
	{		
		private AxXtremeCalendarControl.AxCalendarControl m_pCalendar;

		public SQLDataHelper(AxXtremeCalendarControl.AxCalendarControl pCalendar)
		{			
			m_pCalendar = pCalendar;			
		}

		//======================================================================
		private String RoundNum2D(int nNumber)
		{			
			System.Diagnostics.Debug.Assert(nNumber < 100);

			System.String strNumber = Convert.ToString(nNumber);
			if(nNumber < 10)
			{
				strNumber = "0" + strNumber;
			}

			return strNumber;
		}

		private Boolean ToBool(int nValue)
		{
			return nValue != 0 ? true : false;
		}

		public String MakeSQLDateTime(DateTime dtDate )
		{
			//' "{ts'%Y-%m-%d %H:%M:%S'}"

			String strDate;

			strDate = "{ts'" + dtDate.Year + "-";
			strDate += RoundNum2D(dtDate.Month) + "-";
			strDate += RoundNum2D(dtDate.Day) + " ";

			strDate += RoundNum2D(dtDate.Hour) + ":";
			strDate += RoundNum2D(dtDate.Minute) + ":";
			strDate += RoundNum2D(dtDate.Second) + "'}";

			return strDate;
		}

		public String MakeSQLstr(String strString)
		{
			String strSQL = strString;

			strSQL = strSQL.Replace("\\", "\\\\");
			strSQL = strSQL.Replace("\"", "\\\"");
			    
			strSQL = "\"" + strSQL + "\"";
			    
			return strSQL;
		}
    
		
		public CalendarEvent CreateEventFromData(System.Data.OleDb.OleDbDataReader pEventData) 
		{
			return _CreateEventFromData(pEventData, false);
		}

		protected CalendarEvent _CreateEventFromData(System.Data.OleDb.OleDbDataReader pEventData, Boolean bRException) 
		{	
			try
			{			
				CalendarEvent pEvent = null;
				int nEventID;
				        
				if (pEventData.IsClosed /*|| !pEventData.HasRows*/) 
				{
					return null;
				}			
				        
				nEventID = pEventData.GetInt32(pEventData.GetOrdinal("EventID"));
				pEvent = m_pCalendar.DataProvider.CreateEventEx(nEventID);
				    
				if(pEvent == null) 
				{
					return null;
				}			
				        
				pEvent.Subject = pEventData.GetString(pEventData.GetOrdinal("Subject"));
				pEvent.Location = pEventData.GetString(pEventData.GetOrdinal("Location"));
				pEvent.Body = pEventData.GetString(pEventData.GetOrdinal("Body"));
				
				pEvent.MeetingFlag = ToBool(pEventData.GetInt32(pEventData.GetOrdinal("IsMeeting")));
				pEvent.PrivateFlag = ToBool(pEventData.GetInt32(pEventData.GetOrdinal("IsPrivate")));
				    
				pEvent.Label = pEventData.GetInt32(pEventData.GetOrdinal("LabelID"));
				pEvent.BusyStatus = (CalendarEventBusyStatus)pEventData.GetInt32(pEventData.GetOrdinal("BusyStatus"));
				pEvent.Importance = (CalendarEventImportance)pEventData.GetInt32(pEventData.GetOrdinal("ImportanceLevel"));
				    
				pEvent.StartTime = pEventData.GetDateTime(pEventData.GetOrdinal("StartDateTime"));
				pEvent.EndTime = pEventData.GetDateTime(pEventData.GetOrdinal("EndDateTime"));
				        
				pEvent.AllDayEvent = ToBool(pEventData.GetInt32(pEventData.GetOrdinal("IsAllDayEvent")));

				pEvent.Reminder = ToBool(pEventData.GetInt32(pEventData.GetOrdinal("IsReminder")));
				pEvent.ReminderMinutesBeforeStart = pEventData.GetInt32(pEventData.GetOrdinal("ReminderMinutesBeforeStart"));
				pEvent.ReminderSoundFile = pEventData.GetString(pEventData.GetOrdinal("RemainderSoundFile"));
				    
				pEvent.CustomProperties.LoadFromString(pEventData.GetString(pEventData.GetOrdinal("CustomPropertiesXMLData")));
				pEvent.CustomIcons.LoadFromString(pEventData.GetString(pEventData.GetOrdinal("CustomIconsIDs")));

				pEvent.ScheduleID = pEventData.GetInt32(pEventData.GetOrdinal("ScheduleID"));
				    
				if(bRException)
				{
					pEvent.MakeAsRException();

					pEvent.RExceptionStartTimeOrig = pEventData.GetDateTime(pEventData.GetOrdinal("RExceptionStartTimeOrig"));
					pEvent.RExceptionEndTimeOrig = pEventData.GetDateTime(pEventData.GetOrdinal("RExceptionEndTimeOrig"));
					pEvent.RExceptionDeleted = ToBool(pEventData.GetInt32(pEventData.GetOrdinal("ISRecurrenceExceptionDeleted")));
				}
				    
				if(!bRException)
				{			        
					//	"process_RecurrenceState" and "process_RecurrencePatternID" properties
					//	 are used to process master events.
					//	
					//	 If they are set and RecurrenceStaie is Master Data provider will
					//	 fier DoReadRPattern event and make event as Master.
					//	 And it will also generate occurrences for RetrieveDayEvents method.
					//	
					//	 These properties are temporary and they will be removed by data provider.
					//	
					//	 If these properties are not set data provider expect that master event
					//	 is already compleated - CreateRecurrence method is called and
					//	 Recurrence pattern is set.
					//	
					//	 This mechanism is useful for DB data providers, when events and patterns
					//	 are stored separately (in tables).
					//	 But if events stored in some memory collection or array
					//	 it should not be used because master event store recurrence pattern inside.
		
					pEvent.CustomProperties["process_RecurrenceState"] = pEventData.GetValue(pEventData.GetOrdinal("RecurrenceState"));
					pEvent.CustomProperties["process_RecurrencePatternID"] = pEventData.GetValue(pEventData.GetOrdinal("RecurrencePatternID"));
				}
				    
				return pEvent;    	
			}
			catch(System.Exception e)
			{
				System.Diagnostics.Debug.WriteLine("EXCEPTION! SQLDataHelper.CreateEventFromRS: " + e.Message);
			}

            return null;			
		}

		public void PutEventToData(CalendarEvent pEvent, System.Data.DataRow pEventDRow)
		{
			try
			{			    
				pEventDRow["Subject"] = pEvent.Subject;
				pEventDRow["Location"] = pEvent.Location;
				pEventDRow["Body"] = pEvent.Body;
				    
				pEventDRow["IsMeeting"] = pEvent.MeetingFlag ? 1 : 0;
				pEventDRow["IsPrivate"] = pEvent.PrivateFlag ? 1 : 0;
				    
				pEventDRow["LabelID"] = pEvent.Label;
				pEventDRow["BusyStatus"] = pEvent.BusyStatus;
				pEventDRow["ImportanceLevel"] = pEvent.Importance;
				    
				pEventDRow["StartDateTime"] = pEvent.StartTime;
				pEventDRow["EndDateTime"] = pEvent.EndTime;
				        
				pEventDRow["IsAllDayEvent"] = pEvent.AllDayEvent ? 1 : 0;

				pEventDRow["IsReminder"] = pEvent.Reminder ? 1 : 0;
				pEventDRow["ReminderMinutesBeforeStart"] = pEvent.ReminderMinutesBeforeStart;
				pEventDRow["RemainderSoundFile"] = pEvent.ReminderSoundFile;
				 
				pEventDRow["RecurrenceState"] = pEvent.RecurrenceState;
			    
				if(pEvent.RecurrenceState == CalendarEventRecurrenceState.xtpCalendarRecurrenceMaster ||
					pEvent.RecurrenceState == CalendarEventRecurrenceState.xtpCalendarRecurrenceException)
				{			        
					pEventDRow["RecurrencePatternID"] = pEvent.RecurrencePattern.Id;
				}
				else 
				{
					pEventDRow["RecurrencePatternID"] = 0;
				}
			    
				pEventDRow["RExceptionStartTimeOrig"] = pEvent.RExceptionStartTimeOrig;
				pEventDRow["RExceptionEndTimeOrig"] = pEvent.RExceptionEndTimeOrig;
				pEventDRow["ISRecurrenceExceptionDeleted"] = pEvent.RExceptionDeleted ? 1 : 0;
			        
				pEventDRow["CustomPropertiesXMLData"] = pEvent.CustomProperties.SaveToString();
				pEventDRow["CustomIconsIDs"] = pEvent.CustomIcons.SaveToString();
				
				pEventDRow["ScheduleID"] = pEvent.ScheduleID;
			}
			catch(System.Exception e)
			{
				System.Diagnostics.Debug.WriteLine("EXCEPTION! SQLDataHelper.PutEventToData: " + e.Message);
			}
		}

		public CalendarRecurrencePattern CreateRPatternFromData(System.Data.OleDb.OleDbDataReader pRPatternData, 
																System.Data.OleDb.OleDbConnection pOleDbConn)
		{
			try
			{	
				CalendarRecurrencePattern pRPattern = null;
				int nPatternID;
				        
				if (pRPatternData.IsClosed /*|| !pRPatternData.HasRows*/) 
				{
					return null;
				}
				        
				nPatternID = pRPatternData.GetInt32(pRPatternData.GetOrdinal("RecurrencePatternID"));
				pRPattern = m_pCalendar.DataProvider.CreateRecurrencePattern(nPatternID);
				    
				if(pRPattern == null) 
				{
					return null;
				}			

				pRPattern.MasterEventId = pRPatternData.GetInt32(pRPatternData.GetOrdinal("MasterEventID"));
				    
				pRPattern.StartTime = pRPatternData.GetDateTime(pRPatternData.GetOrdinal("EventStartTime"));
				pRPattern.DurationMinutes = pRPatternData.GetInt32(pRPatternData.GetOrdinal("EventDuration"));
				    
				pRPattern.StartDate = pRPatternData.GetDateTime(pRPatternData.GetOrdinal("PatternStartDate"));
				    
				pRPattern.EndMethod = (CalendarPatternEnd)pRPatternData.GetInt32(pRPatternData.GetOrdinal("PatternEndMethod"));
				    
				if(pRPattern.EndMethod == CalendarPatternEnd.xtpCalendarPatternEndDate) 
				{
					pRPattern.EndDate = pRPatternData.GetDateTime(pRPatternData.GetOrdinal("PatternEndDate"));
				}
				else if(pRPattern.EndMethod == CalendarPatternEnd.xtpCalendarPatternEndAfterOccurrences) 
				{
					pRPattern.EndAfterOccurrences = pRPatternData.GetInt32(pRPatternData.GetOrdinal("PatternEndAfterOccurrences"));
				}				
				else {
					System.Diagnostics.Debug.Assert(pRPattern.EndMethod == CalendarPatternEnd.xtpCalendarPatternEndNoDate);
				}
				
				pRPattern.Options.Data1 = pRPatternData.GetInt32(pRPatternData.GetOrdinal("OptionsData1"));
				pRPattern.Options.Data2 = pRPatternData.GetInt32(pRPatternData.GetOrdinal("OptionsData2"));
				pRPattern.Options.Data3 = pRPatternData.GetInt32(pRPatternData.GetOrdinal("OptionsData3"));
				pRPattern.Options.Data4 = pRPatternData.GetInt32(pRPatternData.GetOrdinal("OptionsData4"));
				    
				pRPattern.CustomProperties.LoadFromString(pRPatternData.GetString(pRPatternData.GetOrdinal("CustomPropertiesXMLData")));
				          
				pRPatternData.Close();

				ReadRPatternExceptions(pRPattern, pOleDbConn);
				        
				return pRPattern;
			}
			catch(System.Exception e)
			{
				System.Diagnostics.Debug.WriteLine("EXCEPTION! SQLDataHelper.CreateRPatternFromData: " + e.Message);
			}

            return null;
		}
	
		public void PutRPatternToData(CalendarRecurrencePattern pRPattern, System.Data.DataRow pRPatternDRow)
		{
			try
			{
				pRPatternDRow["MasterEventID"] = pRPattern.MasterEventId;

				pRPatternDRow["EventStartTime"] = pRPattern.StartTime;
				pRPatternDRow["EventDuration"] = pRPattern.DurationMinutes;

				pRPatternDRow["PatternStartDate"] = pRPattern.StartDate;

				pRPatternDRow["PatternEndMethod"] = pRPattern.EndMethod;
				pRPatternDRow["PatternEndDate"] = pRPattern.EndDate;
				pRPatternDRow["PatternEndAfterOccurrences"] = pRPattern.EndAfterOccurrences;

				pRPatternDRow["OptionsData1"] = pRPattern.Options.Data1;
				pRPatternDRow["OptionsData2"] = pRPattern.Options.Data2;
				pRPatternDRow["OptionsData3"] = pRPattern.Options.Data3;
				pRPatternDRow["OptionsData4"] = pRPattern.Options.Data4;

				pRPatternDRow["CustomPropertiesXMLData"] = pRPattern.CustomProperties.SaveToString();
			}
			catch(System.Exception e)
			{
				System.Diagnostics.Debug.WriteLine("EXCEPTION! SQLDataHelper.PutRPatternToData: " + e.Message);
			}
		}

		protected void ReadRPatternExceptions( CalendarRecurrencePattern pPattern, 
											System.Data.OleDb.OleDbConnection pOleDbConn)
		{
			if (pPattern == null || pOleDbConn == null) 
			{
				System.Diagnostics.Debug.Assert(false);
				return;
			}

			System.Data.OleDb.OleDbDataReader oleReader = null;

			try
			{
				int nPatternID = pPattern.Id;
				String strSQL = "SELECT * FROM CalendarEvents WHERE \r\n";
				strSQL += " RecurrenceState = " + Convert.ToString((int)CalendarEventRecurrenceState.xtpCalendarRecurrenceException);
				strSQL += " AND RecurrencePatternID = " + Convert.ToString(nPatternID);
				
				System.Data.OleDb.OleDbCommand oleDbCommand = pOleDbConn.CreateCommand();
				oleDbCommand.CommandText = strSQL;

				oleReader = oleDbCommand.ExecuteReader();

				while(oleReader.Read())
				{					
					CalendarEvent axEvent = _CreateEventFromData(oleReader, true);
					if (axEvent != null) 
					{
						pPattern.SetException(axEvent);
					}
				}
			}
			catch(System.Exception ex)
			{
				System.Diagnostics.Debug.WriteLine("EXCEPTION! SQLDataHelper.ReadRPatternExceptions: " + ex.Message);
			}

			//================================================
			if(oleReader != null && !oleReader.IsClosed) 
			{
				oleReader.Close();
			}
		}

		
		public String MakeRetrieveDayEventsSQL(DateTime dtDay) 
		{
			int nYear, nMonth, nDay;
			String strSQL;

			nYear = dtDay.Year;
			nMonth = dtDay.Month;
			nDay = dtDay.Day;

			strSQL = "SELECT * FROM CalendarEvents WHERE \r\n";

			strSQL += " ( RecurrenceState = " +  Convert.ToString((int)CalendarEventRecurrenceState.xtpCalendarRecurrenceNotRecurring) + " OR \r\n";
			strSQL += "   RecurrenceState = " +  Convert.ToString((int)CalendarEventRecurrenceState.xtpCalendarRecurrenceMaster) + ") AND \r\n";

			strSQL += "( YEAR(StartDateTime) < " + Convert.ToString(nYear) + "\r\n";
			strSQL += "  OR ( YEAR(StartDateTime) = " + Convert.ToString(nYear) + " AND \r\n";
			strSQL += "       ( MONTH(StartDateTime) < " + Convert.ToString(nMonth) + " OR \r\n";
			strSQL += "         MONTH(StartDateTime) = " + Convert.ToString(nMonth) + " AND \r\n";
			strSQL += "         DAY(StartDateTime) <= " + Convert.ToString(nDay) + "\r\n";

			strSQL += "     ) ) ) AND \r\n";

			strSQL += "( YEAR(EndDateTime) > " + Convert.ToString(nYear) + "\r\n";
			strSQL += "  OR ( YEAR(EndDateTime) = " + Convert.ToString(nYear) + " AND \r\n";
			strSQL += "       ( MONTH(EndDateTime) > " + Convert.ToString(nMonth) + " OR \r\n";
			strSQL += "         MONTH(EndDateTime) = " + Convert.ToString(nMonth) + " AND \r\n";
			strSQL += "         DAY(EndDateTime) >= " + Convert.ToString(nDay) + "\r\n";
			strSQL += "     ) ) ) \r\n";

			//System.Diagnostics.Debug.Write(strSQL);

			return strSQL;
		}

		public String MakeGetUpcomingEventsSQL(DateTime dtFrom, int nPeriodMinutes, Boolean bOptimized)
		{
			// The SQL script below can be created in 2 ways - simple or optimized.
			//
			// * The simple script is read all events from DB.
			//   RemindersManager will filter events and use only events with reminder 
			//   which will be Fired untill dtFrom + PeriodMinutes. 
			//   This can be very slow operation, 
			//   but it is called no so often - once per 2 hours by default.
			// 
			// * The optimized script is filter events instead of reminder manager.
			//   This can significantly improve performance.
			//
			// The simple variant can be useful for some debug purposes.
			// 
			// NOTE: generally dtFrom is Now.
			
			String strRState_no = Convert.ToString((int)CalendarEventRecurrenceState.xtpCalendarRecurrenceNotRecurring);
			String strRState_master = Convert.ToString((int)CalendarEventRecurrenceState.xtpCalendarRecurrenceMaster);
			String strRState_exception = Convert.ToString((int)CalendarEventRecurrenceState.xtpCalendarRecurrenceException);

			String strSQL = "SELECT * FROM CalendarEvents WHERE \r\n";

			strSQL += " (RecurrenceState = " + strRState_no + " OR \r\n";			
			strSQL += "  RecurrenceState = " + strRState_master + ") \r\n";

			//----------------------------------------------------------
			if(bOptimized)
			{
				DateTime dtUntil = dtFrom.AddMinutes(nPeriodMinutes);
				String strUntil = MakeSQLDateTime(dtUntil);
				
				strSQL += " AND \r\n";
				strSQL += " ( IsReminder <> 0 OR \r\n";
				strSQL += "     ( RecurrenceState = " + strRState_master + " AND \r\n";
				strSQL += "       RecurrencePatternID IN ";
				strSQL += "         ( SELECT RecurrencePatternID  FROM CalendarEvents \r\n";
				strSQL += "           WHERE ";
				strSQL += "             RecurrenceState = "  + strRState_exception + " AND \r\n";
				strSQL += "             IsReminder <> 0 AND ";
				strSQL += "				DATEADD(\"n\", -1 * ReminderMinutesBeforeStart, StartDateTime) <= " + strUntil + "\r\n";
				strSQL += "         ) \r\n";
				strSQL += "     ) \r\n";
				strSQL += " ) AND \r\n";
				strSQL += " DATEADD(\"n\", -1 * ReminderMinutesBeforeStart, StartDateTime) <= " + strUntil;
			}

			return strSQL;
		}

		//public String MakeUpdateEventSQL(/*pEvent As CalendarEvent*/)
		//{
		//	return "";
		/*
			Dim strSQL As String

			strSQL = "UPDATE CalendarEvents SET " & vbCrLf
			strSQL = strSQL + "Subject = " & MakeSQLstr(pEvent.Subject) & ", " & vbCrLf
			strSQL = strSQL + "Location = " & MakeSQLstr(pEvent.Location) & ", " & vbCrLf
			strSQL = strSQL + "Body = " & MakeSQLstr(pEvent.Body) & ", " & vbCrLf

			strSQL = strSQL + "IsMeeting = " & IIf(pEvent.MeetingFlag, 1, 0) & ", " & vbCrLf
			strSQL = strSQL + "IsPrivate = " & IIf(pEvent.PrivateFlag, 1, 0) & ", " & vbCrLf

			strSQL = strSQL + "LabelID = " & pEvent.Label & ", " & vbCrLf
			strSQL = strSQL + "BusyStatus = " & pEvent.BusyStatus & ", " & vbCrLf
			strSQL = strSQL + "ImportanceLevel = " & pEvent.Importance & ", " & vbCrLf

			strSQL = strSQL + "StartDateTime = " & MakeSQLDateTime(pEvent.StartTime) & ", " & vbCrLf
			strSQL = strSQL + "EndDateTime = " & MakeSQLDateTime(pEvent.EndTime) & ", " & vbCrLf

			strSQL = strSQL + "IsAllDayEvent = " & IIf(pEvent.AllDayEvent, 1, 0) & ", " & vbCrLf

			strSQL = strSQL + "IsReminder = " & IIf(pEvent.Reminder, 1, 0) & ", " & vbCrLf
																	strSQL = strSQL + "ReminderMinutesBeforeStart = " & pEvent.ReminderMinutesBeforeStart & ", " & vbCrLf
																	strSQL = strSQL + "RemainderSoundFile = " & MakeSQLstr(pEvent.ReminderSoundFile) & ", " & vbCrLf

			strSQL = strSQL + "RecurrenceState = " & pEvent.RecurrenceState & ", " & vbCrLf

			If pEvent.RecurrenceState = xtpCalendarRecurrenceMaster Or _
			pEvent.RecurrenceState = xtpCalendarRecurrenceException Then

			strSQL = strSQL + "RecurrencePatternID = " & pEvent.RecurrencePattern.Id & ", " & vbCrLf
			Else
			strSQL = strSQL + "RecurrencePatternID = 0, " & vbCrLf
			End If

			strSQL = strSQL + "RExceptionStartTimeOrig = " & MakeSQLDateTime(pEvent.RExceptionStartTimeOrig) & ", " & vbCrLf
			strSQL = strSQL + "RExceptionEndTimeOrig = " & MakeSQLDateTime(pEvent.RExceptionEndTimeOrig) & ", " & vbCrLf
			strSQL = strSQL + "ISRecurrenceExceptionDeleted = " & IIf(pEvent.RExceptionDeleted, 1, 0) & ", " & vbCrLf

			strSQL = strSQL + "CustomPropertiesXMLData = " & MakeSQLstr(pEvent.CustomProperties.SaveToString) & " " & vbCrLf

			strSQL = strSQL + "WHERE EventID = " & pEvent.Id & ";"

			''--------------------------------------------
			MakeUpdateEventSQL = strSQL
									 */
		//}

		//public String MakeCreateEventSQL(CalendarEvent pEvent)
		//{	
		//	return "";																						 
		/*
		 * Dim strSQL As String
		strSQL = "INSERT INTO CalendarEvents ("
		strSQL = strSQL + "Subject, "
		strSQL = strSQL + "Location, "
		strSQL = strSQL + "Body, "
       
		strSQL = strSQL + "IsMeeting, "
		strSQL = strSQL + "IsPrivate, "
    
		strSQL = strSQL + "LabelID, "
		strSQL = strSQL + "BusyStatus, "
		strSQL = strSQL + "ImportanceLevel, "
    
		strSQL = strSQL + "StartDateTime, "
		strSQL = strSQL + "EndDateTime, "
        
		strSQL = strSQL + "IsAllDayEvent, "

		strSQL = strSQL + "IsReminder, "
		strSQL = strSQL + "ReminderMinutesBeforeStart, "
		strSQL = strSQL + "RemainderSoundFile, "
            
		strSQL = strSQL + "RecurrenceState, "
		strSQL = strSQL + "RecurrencePatternID, "
    
		strSQL = strSQL + "RExceptionStartTimeOrig, "
		strSQL = strSQL + "RExceptionEndTimeOrig, "
		strSQL = strSQL + "ISRecurrenceExceptionDeleted, "
    
		strSQL = strSQL + "CustomPropertiesXMLData) " & vbCrLf

		strSQL = strSQL + "VALUES("

		strSQL = strSQL + MakeSQLstr(pEvent.Subject) & ", " & vbCrLf
		strSQL = strSQL + MakeSQLstr(pEvent.Location) & ", " & vbCrLf
		strSQL = strSQL + MakeSQLstr(pEvent.Body) & ", " & vbCrLf

		strSQL = strSQL & IIf(pEvent.MeetingFlag, 1, 0) & ", " & vbCrLf
		strSQL = strSQL & IIf(pEvent.PrivateFlag, 1, 0) & ", " & vbCrLf

		strSQL = strSQL & pEvent.Label & ", " & vbCrLf
		strSQL = strSQL & pEvent.BusyStatus & ", " & vbCrLf
		strSQL = strSQL & pEvent.Importance & ", " & vbCrLf

		strSQL = strSQL & MakeSQLDateTime(pEvent.StartTime) & ", " & vbCrLf
		strSQL = strSQL & MakeSQLDateTime(pEvent.EndTime) & ", " & vbCrLf

		strSQL = strSQL & IIf(pEvent.AllDayEvent, 1, 0) & ", " & vbCrLf

		strSQL = strSQL & IIf(pEvent.Reminder, 1, 0) & ", " & vbCrLf
		strSQL = strSQL & pEvent.ReminderMinutesBeforeStart & ", " & vbCrLf
		strSQL = strSQL & MakeSQLstr(pEvent.ReminderSoundFile) & ", " & vbCrLf

		strSQL = strSQL & pEvent.RecurrenceState & ", " & vbCrLf

		If pEvent.RecurrenceState = xtpCalendarRecurrenceMaster Or _
		pEvent.RecurrenceState = xtpCalendarRecurrenceException Then

		strSQL = strSQL & pEvent.RecurrencePattern.Id & ", " & vbCrLf
		Else
		strSQL = strSQL + "0, " & vbCrLf
		End If

		strSQL = strSQL & MakeSQLDateTime(pEvent.RExceptionStartTimeOrig) & ", " & vbCrLf
		strSQL = strSQL & MakeSQLDateTime(pEvent.RExceptionEndTimeOrig) & ", " & vbCrLf
		strSQL = strSQL & IIf(pEvent.RExceptionDeleted, 1, 0) & ", " & vbCrLf

		strSQL = strSQL & MakeSQLstr(pEvent.CustomProperties.SaveToString) & "); " & vbCrLf

		''--------------------------------------------
		MakeCreateEventSQL = strSQL
		*/
		//}
		

		//public String MakeUpdatePatternSQL(CalendarRecurrencePattern pRPattern)
		//{
		//	return "";
			/*

			Dim strSQL As String

			strSQL = "UPDATE CalendarRecurrencePatterns SET " & vbCrLf

			strSQL = strSQL + "MasterEventID = " & pRPattern.MasterEventId & ", " & vbCrLf

			strSQL = strSQL + "EventStartTime = " & MakeSQLDateTime(pRPattern.StartTime) & ", " & vbCrLf
			strSQL = strSQL + "EventDuration = " & pRPattern.DurationMinutes & ", " & vbCrLf

			strSQL = strSQL + "PatternStartDate = " & MakeSQLDateTime(pRPattern.StartDate) & ", " & vbCrLf

			strSQL = strSQL + "PatternEndMethod = " & pRPattern.EndMethod & ", " & vbCrLf
			strSQL = strSQL + "PatternEndDate = " & MakeSQLDateTime(pRPattern.EndDate) & ", " & vbCrLf
			strSQL = strSQL + "PatternEndAfterOccurrences = " & pRPattern.EndAfterOccurrences & ", " & vbCrLf

			strSQL = strSQL + "OptionsData1 = " & pRPattern.Options.Data1 & ", " & vbCrLf
			strSQL = strSQL + "OptionsData2 = " & pRPattern.Options.Data2 & ", " & vbCrLf
			strSQL = strSQL + "OptionsData3 = " & pRPattern.Options.Data3 & ", " & vbCrLf
			strSQL = strSQL + "OptionsData4 = " & pRPattern.Options.Data4 & ", " & vbCrLf

			strSQL = strSQL + "CustomPropertiesXMLData = " & MakeSQLstr(pRPattern.CustomProperties.SaveToString) & " " & vbCrLf

			strSQL = strSQL + " WHERE RecurrencePatternID = " & pRPattern.Id

			MakeUpdatePatternSQL = strSQL
			*/
		//}

		//public String MakeCreatePatternSQL(CalendarRecurrencePattern pRPattern) 
		//{
			/*
					Dim strSQL As String

					strSQL = "INSERT INTO CalendarRecurrencePatterns ("

					strSQL = strSQL + "MasterEventID, " & vbCrLf

					strSQL = strSQL + "EventStartTime, " & vbCrLf
					strSQL = strSQL + "EventDuration, " & vbCrLf

					strSQL = strSQL + "PatternStartDate, " & vbCrLf

					strSQL = strSQL + "PatternEndMethod, " & vbCrLf
					strSQL = strSQL + "PatternEndDate, " & vbCrLf
					strSQL = strSQL + "PatternEndAfterOccurrences, " & vbCrLf

					strSQL = strSQL + "OptionsData1, " & vbCrLf
					strSQL = strSQL + "OptionsData2, " & vbCrLf
					strSQL = strSQL + "OptionsData3, " & vbCrLf
					strSQL = strSQL + "OptionsData4, " & vbCrLf

					strSQL = strSQL + "CustomPropertiesXMLData) " & vbCrLf

					'--------
					strSQL = strSQL + "VALUES("

					strSQL = strSQL & pRPattern.MasterEventId & ", " & vbCrLf

					strSQL = strSQL & MakeSQLDateTime(pRPattern.StartTime) & ", " & vbCrLf
					strSQL = strSQL & pRPattern.DurationMinutes & ", " & vbCrLf

					strSQL = strSQL & MakeSQLDateTime(pRPattern.StartDate) & ", " & vbCrLf

					strSQL = strSQL & pRPattern.EndMethod & ", " & vbCrLf
					strSQL = strSQL & MakeSQLDateTime(pRPattern.EndDate) & ", " & vbCrLf
					strSQL = strSQL & pRPattern.EndAfterOccurrences & ", " & vbCrLf

					strSQL = strSQL & pRPattern.Options.Data1 & ", " & vbCrLf
					strSQL = strSQL & pRPattern.Options.Data2 & ", " & vbCrLf
					strSQL = strSQL & pRPattern.Options.Data3 & ", " & vbCrLf
					strSQL = strSQL & pRPattern.Options.Data4 & ", " & vbCrLf

					strSQL = strSQL & MakeSQLstr(pRPattern.CustomProperties.SaveToString) & "); "

					MakeCreatePatternSQL = strSQL
					*/
		//}

	}
}
