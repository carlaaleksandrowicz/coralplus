using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using XtremeCalendarControl;

namespace CalendarControl
{
	/// <summary>
	/// Summary description for TimeZone.
	/// </summary>
	public class frmTimeZone : System.Windows.Forms.Form
	{
		private System.Windows.Forms.GroupBox gbxCurrentTimeZone;
		private System.Windows.Forms.Label lblCurrentLabel;
		private System.Windows.Forms.TextBox txtCurrentLabel;
		private System.Windows.Forms.Label lblCurrentTimeZone;
		private System.Windows.Forms.ComboBox cmbCurrentTimeZone;
		private System.Windows.Forms.CheckBox chkCurrentAutomatic;
		private System.Windows.Forms.CheckBox chkShowAnAdditionalTimeZone;
		private System.Windows.Forms.GroupBox gbxAdditionalTimeZone;
		private System.Windows.Forms.CheckBox chkAdditionalAutomatic;
		private System.Windows.Forms.ComboBox cmbAdditionalTimeZone;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox txtAdditionalLabel;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Button cmdOK;
		private System.Windows.Forms.Button cmdCancel;
		private CalendarTimeZones g_pTimeZones;

		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public frmTimeZone()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeControls();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		private void InitializeControls()
		{
			CalendarTimeZone tziCurrent;
			CalendarTimeZone tzi2;
			Boolean bIsScale2Visible;

			tziCurrent = frmMain.Instance.wndCalendarControl.Options.GetCurrentTimeZone();
			tzi2 = frmMain.Instance.wndCalendarControl.Options.GetScale2TimeZone();
			g_pTimeZones = frmMain.Instance.wndCalendarControl.Options.EnumAllTimeZones();
			bIsScale2Visible = frmMain.Instance.wndCalendarControl.Options.DayViewScale2Visible;

			txtCurrentLabel.Text = frmMain.Instance.wndCalendarControl.Options.DayViewScaleLabel;
			txtAdditionalLabel.Text = frmMain.Instance.wndCalendarControl.Options.DayViewScale2Label;

			cmbCurrentTimeZone.Items.Insert(0, tziCurrent.DisplayString);
			cmbCurrentTimeZone.SelectedIndex = 0;

			chkCurrentAutomatic.Visible = IsAutoAdjustDaylight_Exists(tziCurrent);
			if (IsAutoAdjustDaylight_Checked(tziCurrent))
			{
				chkCurrentAutomatic.Checked = true;
			}
			else
			{
				chkCurrentAutomatic.Checked = false;
			}

			//cmbCurrentTimeZone.Enabled = false;
			//chkCurrentAutomatic.Enabled = false;

			//==========================================================
			int nIndex, nCurrentIndex;

			nIndex = 0;
			nCurrentIndex = g_pTimeZones.Count / 2;

			for(nIndex = 0; nIndex < g_pTimeZones.Count; nIndex++)
			{
				cmbAdditionalTimeZone.Items.Insert(nIndex, g_pTimeZones[nIndex].DisplayString);
				System.Diagnostics.Debug.WriteLine(g_pTimeZones[nIndex].DisplayString);

				if (tzi2 != null)
				{
					if (tzi2.IsEqual(g_pTimeZones[nIndex]))
					{
						nCurrentIndex = nIndex;
					}
				}
			}

			cmbAdditionalTimeZone.SelectedIndex = nCurrentIndex;

			if (bIsScale2Visible)
			{
				chkShowAnAdditionalTimeZone.Checked = true;
			}
			else
			{
				chkShowAnAdditionalTimeZone.Checked = false;
			}

			chkAdditionalAutomatic.Enabled = (IsAutoAdjustDaylight_Exists(tzi2) & bIsScale2Visible);
			if (IsAutoAdjustDaylight_Checked(tzi2))
			{
				chkAdditionalAutomatic.Checked = true;
			}
			else
			{
				chkAdditionalAutomatic.Checked = false;
			}
		}

		private Boolean IsAutoAdjustDaylight_Exists(CalendarTimeZone pTimeZone)
		{
			if (pTimeZone.DaylightDate.wMonth != 0 & pTimeZone.StandardDate.wMonth != 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		private Boolean IsAutoAdjustDaylight_Checked(CalendarTimeZone pTimeZone)
		{
			if (pTimeZone.DaylightBias != 0 | pTimeZone.StandardBias  != 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.gbxCurrentTimeZone = new System.Windows.Forms.GroupBox();
			this.chkCurrentAutomatic = new System.Windows.Forms.CheckBox();
			this.cmbCurrentTimeZone = new System.Windows.Forms.ComboBox();
			this.lblCurrentTimeZone = new System.Windows.Forms.Label();
			this.txtCurrentLabel = new System.Windows.Forms.TextBox();
			this.lblCurrentLabel = new System.Windows.Forms.Label();
			this.chkShowAnAdditionalTimeZone = new System.Windows.Forms.CheckBox();
			this.gbxAdditionalTimeZone = new System.Windows.Forms.GroupBox();
			this.chkAdditionalAutomatic = new System.Windows.Forms.CheckBox();
			this.cmbAdditionalTimeZone = new System.Windows.Forms.ComboBox();
			this.label1 = new System.Windows.Forms.Label();
			this.txtAdditionalLabel = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.cmdOK = new System.Windows.Forms.Button();
			this.cmdCancel = new System.Windows.Forms.Button();
			this.gbxCurrentTimeZone.SuspendLayout();
			this.gbxAdditionalTimeZone.SuspendLayout();
			this.SuspendLayout();
			// 
			// gbxCurrentTimeZone
			// 
			this.gbxCurrentTimeZone.Controls.AddRange(new System.Windows.Forms.Control[] {
																							 this.chkCurrentAutomatic,
																							 this.cmbCurrentTimeZone,
																							 this.lblCurrentTimeZone,
																							 this.txtCurrentLabel,
																							 this.lblCurrentLabel});
			this.gbxCurrentTimeZone.Location = new System.Drawing.Point(8, 8);
			this.gbxCurrentTimeZone.Name = "gbxCurrentTimeZone";
			this.gbxCurrentTimeZone.Size = new System.Drawing.Size(392, 104);
			this.gbxCurrentTimeZone.TabIndex = 0;
			this.gbxCurrentTimeZone.TabStop = false;
			this.gbxCurrentTimeZone.Text = "Current Time Zone";
			// 
			// chkCurrentAutomatic
			// 
			this.chkCurrentAutomatic.Location = new System.Drawing.Point(24, 80);
			this.chkCurrentAutomatic.Name = "chkCurrentAutomatic";
			this.chkCurrentAutomatic.Size = new System.Drawing.Size(304, 16);
			this.chkCurrentAutomatic.TabIndex = 4;
			this.chkCurrentAutomatic.Text = "Automatically Adjust Clock for Daylight Saving Changes";
			// 
			// cmbCurrentTimeZone
			// 
			this.cmbCurrentTimeZone.Enabled = false;
			this.cmbCurrentTimeZone.Location = new System.Drawing.Point(88, 48);
			this.cmbCurrentTimeZone.Name = "cmbCurrentTimeZone";
			this.cmbCurrentTimeZone.Size = new System.Drawing.Size(296, 21);
			this.cmbCurrentTimeZone.TabIndex = 3;
			// 
			// lblCurrentTimeZone
			// 
			this.lblCurrentTimeZone.Location = new System.Drawing.Point(16, 56);
			this.lblCurrentTimeZone.Name = "lblCurrentTimeZone";
			this.lblCurrentTimeZone.Size = new System.Drawing.Size(64, 16);
			this.lblCurrentTimeZone.TabIndex = 2;
			this.lblCurrentTimeZone.Text = "Time Zone:";
			// 
			// txtCurrentLabel
			// 
			this.txtCurrentLabel.Location = new System.Drawing.Point(88, 24);
			this.txtCurrentLabel.Name = "txtCurrentLabel";
			this.txtCurrentLabel.Size = new System.Drawing.Size(64, 20);
			this.txtCurrentLabel.TabIndex = 1;
			this.txtCurrentLabel.Text = "";
			// 
			// lblCurrentLabel
			// 
			this.lblCurrentLabel.Location = new System.Drawing.Point(16, 24);
			this.lblCurrentLabel.Name = "lblCurrentLabel";
			this.lblCurrentLabel.Size = new System.Drawing.Size(40, 16);
			this.lblCurrentLabel.TabIndex = 0;
			this.lblCurrentLabel.Text = "Label:";
			// 
			// chkShowAnAdditionalTimeZone
			// 
			this.chkShowAnAdditionalTimeZone.Location = new System.Drawing.Point(8, 124);
			this.chkShowAnAdditionalTimeZone.Name = "chkShowAnAdditionalTimeZone";
			this.chkShowAnAdditionalTimeZone.Size = new System.Drawing.Size(184, 16);
			this.chkShowAnAdditionalTimeZone.TabIndex = 1;
			this.chkShowAnAdditionalTimeZone.Text = "Show an Additional Time Zone";
			this.chkShowAnAdditionalTimeZone.CheckedChanged += new System.EventHandler(this.chkShowAnAdditionalTimeZone_CheckedChanged);
			// 
			// gbxAdditionalTimeZone
			// 
			this.gbxAdditionalTimeZone.Controls.AddRange(new System.Windows.Forms.Control[] {
																								this.chkAdditionalAutomatic,
																								this.cmbAdditionalTimeZone,
																								this.label1,
																								this.txtAdditionalLabel,
																								this.label2});
			this.gbxAdditionalTimeZone.Enabled = false;
			this.gbxAdditionalTimeZone.Location = new System.Drawing.Point(8, 146);
			this.gbxAdditionalTimeZone.Name = "gbxAdditionalTimeZone";
			this.gbxAdditionalTimeZone.Size = new System.Drawing.Size(392, 104);
			this.gbxAdditionalTimeZone.TabIndex = 2;
			this.gbxAdditionalTimeZone.TabStop = false;
			// 
			// chkAdditionalAutomatic
			// 
			this.chkAdditionalAutomatic.Location = new System.Drawing.Point(24, 80);
			this.chkAdditionalAutomatic.Name = "chkAdditionalAutomatic";
			this.chkAdditionalAutomatic.Size = new System.Drawing.Size(304, 16);
			this.chkAdditionalAutomatic.TabIndex = 4;
			this.chkAdditionalAutomatic.Text = "Automatically Adjust Clock for Daylight Saving Changes";
			// 
			// cmbAdditionalTimeZone
			// 
			this.cmbAdditionalTimeZone.Location = new System.Drawing.Point(88, 48);
			this.cmbAdditionalTimeZone.Name = "cmbAdditionalTimeZone";
			this.cmbAdditionalTimeZone.Size = new System.Drawing.Size(296, 21);
			this.cmbAdditionalTimeZone.TabIndex = 3;
			this.cmbAdditionalTimeZone.SelectedIndexChanged += new System.EventHandler(this.cmbAdditionalTimeZone_SelectedIndexChanged);
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(16, 56);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(64, 16);
			this.label1.TabIndex = 2;
			this.label1.Text = "Time Zone:";
			// 
			// txtAdditionalLabel
			// 
			this.txtAdditionalLabel.Location = new System.Drawing.Point(88, 24);
			this.txtAdditionalLabel.Name = "txtAdditionalLabel";
			this.txtAdditionalLabel.Size = new System.Drawing.Size(64, 20);
			this.txtAdditionalLabel.TabIndex = 1;
			this.txtAdditionalLabel.Text = "";
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(16, 24);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(40, 16);
			this.label2.TabIndex = 0;
			this.label2.Text = "Label:";
			// 
			// cmdOK
			// 
			this.cmdOK.Location = new System.Drawing.Point(128, 264);
			this.cmdOK.Name = "cmdOK";
			this.cmdOK.Size = new System.Drawing.Size(72, 24);
			this.cmdOK.TabIndex = 3;
			this.cmdOK.Text = "OK";
			this.cmdOK.Click += new System.EventHandler(this.cmdOK_Click);
			// 
			// cmdCancel
			// 
			this.cmdCancel.Location = new System.Drawing.Point(224, 264);
			this.cmdCancel.Name = "cmdCancel";
			this.cmdCancel.Size = new System.Drawing.Size(72, 24);
			this.cmdCancel.TabIndex = 4;
			this.cmdCancel.Text = "Cancel";
			this.cmdCancel.Click += new System.EventHandler(this.cmdCancel_Click);
			// 
			// frmTimeZone
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(416, 293);
			this.Controls.AddRange(new System.Windows.Forms.Control[] {
																		  this.cmdCancel,
																		  this.cmdOK,
																		  this.gbxAdditionalTimeZone,
																		  this.chkShowAnAdditionalTimeZone,
																		  this.gbxCurrentTimeZone});
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "frmTimeZone";
			this.Text = "Time Zone";
			this.gbxCurrentTimeZone.ResumeLayout(false);
			this.gbxAdditionalTimeZone.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		private void chkShowAnAdditionalTimeZone_CheckedChanged(object sender, System.EventArgs e)
		{
			gbxAdditionalTimeZone.Enabled = chkShowAnAdditionalTimeZone.Checked;
			cmbAdditionalTimeZone_SelectedIndexChanged(this, new System.EventArgs());
		}

		private void cmdCancel_Click(object sender, System.EventArgs e)
		{
			this.Close();
		}

		private void cmdOK_Click(object sender, System.EventArgs e)
		{
			frmMain.Instance.wndCalendarControl.Options.DayViewScaleLabel = txtCurrentLabel.Text;
			frmMain.Instance.wndCalendarControl.Options.DayViewScale2Label = txtAdditionalLabel.Text;

			if (chkShowAnAdditionalTimeZone.Checked)
			{
				frmMain.Instance.wndCalendarControl.Options.DayViewScale2Visible = true;
			}
			else
			{
				frmMain.Instance.wndCalendarControl.Options.DayViewScale2Visible = false;
			}

			CalendarTimeZone tziScale2;
			tziScale2 = g_pTimeZones[cmbAdditionalTimeZone.SelectedIndex];

			if (chkAdditionalAutomatic.Checked)
			{
				tziScale2.StandardBias = 0;
				tziScale2.DaylightBias = 0;
			}

			frmMain.Instance.wndCalendarControl.Options.SetScale2TimeZone(tziScale2);

			frmMain.Instance.wndCalendarControl.Populate();

			this.Close();
		}

		private void cmbAdditionalTimeZone_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			CalendarTimeZone tziScale2;
			tziScale2 = g_pTimeZones[cmbAdditionalTimeZone.SelectedIndex];

			chkAdditionalAutomatic.Enabled = IsAutoAdjustDaylight_Exists(tziScale2);

			if (IsAutoAdjustDaylight_Checked(tziScale2))
			{
				chkAdditionalAutomatic.Checked = true;
			}
			else
			{
				chkAdditionalAutomatic.Checked = false;
			}
		}
	}
}
