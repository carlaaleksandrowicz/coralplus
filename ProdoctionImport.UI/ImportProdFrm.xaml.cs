﻿using Coral.Prod.Managment;
using Coral.Prod.Managment.ProdoctionCompany;
using Coral.Prod.Managment.ProfoctionFormat.genral;
using CoralBusinessLogics;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace ProdoctionImport.UI
{
    /// <summary>
    /// Interaction logic for ImportProdFrm.xaml
    /// </summary>
    public partial class ImportProdFrm : Window
    {
        //private string _prodFileName = string.Empty;
        string clientDirPath = null;
        private object ins;

        public bool UpdateAgent { get; set; }
        public Agent AgentSelected { get; set; }
        public Company CompanySelected { get; set; }
        public Insurance InsuranceSelected { get; set; }
        public string _prodFileName { get; set; }
        public ImportProdFrm()
        {
            InitializeComponent();
        }

        public ImportProdFrm(string prodFileName)
        {
            InitializeComponent();
            _prodFileName = prodFileName;
            lblProdFileName.Content = _prodFileName;
        }

        public ImportProdFrm(Agent agentSelected, Company company, Insurance insurance, string fileName)
        {
            InitializeComponent();
            AgentSelected = agentSelected;
            CompanySelected = company;
            InsuranceSelected = insurance;
            _prodFileName = fileName;
        }

        private void OnLoded(object sender, RoutedEventArgs e)
        {

            string strAppDir = System.AppDomain.CurrentDomain.BaseDirectory;
            string path = strAppDir + @"Coral Files\ClientsPath.txt";

            if (File.Exists(path))
            {
                clientDirPath = File.ReadAllText(path);
            }
            else
            {
                MessageBox.Show("לא נמצא תיקיית הלקוחות. נא הגדר בהגדרות ראשוניות את נתיב התיקייה");
                Close();
                return;
            }

            InitialProdoction.SetCompany("כלל");
            InitialProdoction.SetCompany("מגדל");
            InitialProdoction.SetCompany("שומרה");
            InitialProdoction.SetCompany("מנורה");
            InitialProdoction.SetCompany("פניקס");
            InitialProdoction.SetCompany("הכשרה");
            InitialProdoction.SetCompany("שלמה");

            InitialProdoction.GetAgentID();

            LoadAllCompany();
            LoadAllInsurence();
            LoadAllAgent();

            if (CompanySelected != null)
            {
                var items = cbxCompany.Items;
                foreach (var item in items)
                {
                    Company com = (Company)item;
                    if (com.CompanyID == CompanySelected.CompanyID)
                    {
                        cbxCompany.SelectedItem = item;
                        break;
                    }
                }
            }
            if (InsuranceSelected != null)
            {
                var items = cbxInsurenseType.Items;
                foreach (var item in items)
                {
                    Insurance ins = (Insurance)item;
                    if (ins.InsuranceID == InsuranceSelected.InsuranceID)
                    {
                        cbxInsurenseType.SelectedItem = item;
                        break;
                    }
                }
            }
            if (AgentSelected != null)
            {
                var items = cbxAgent.Items;
                foreach (var item in items)
                {
                    Agent ag = (Agent)item;
                    if (ag.AgentID == AgentSelected.AgentID)
                    {
                        cbxAgent.SelectedItem = item;
                        break;
                    }
                }
            }
            if (!string.IsNullOrEmpty(_prodFileName))
            {
                lblProdFileName.Content = _prodFileName;
            }
        }

        private void btnFileDialog_Click(object sender, RoutedEventArgs e)
        {
            // Create OpenFileDialog 
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();



            // Set filter for file extension and default file extension 
            dlg.DefaultExt = "Zip";
            dlg.Filter = "Zip|*.zip|exe file (*.exe)|*.exe|7zip (*.7zip)|*.7zip";


            // Display OpenFileDialog by calling ShowDialog method 
            Nullable<bool> result = dlg.ShowDialog();


            // Get the selected file name and display in a TextBox 
            if (result == true)
            {
                // Open document 
                _prodFileName = dlg.FileName;
                lblProdFileName.Content = _prodFileName;
            }
        }

        private void LoadAllCompany()
        {
            CompaniesLogic cl = new CompaniesLogic();
            var companiesCollection = cl.GetAllCompanies();


            cbxCompany.ItemsSource = companiesCollection;
            cbxCompany.DisplayMemberPath = "CompanyName";
        }

        private void LoadAllInsurence()
        {
            InsurancesLogic il = new InsurancesLogic();
            var insurancesCollection = il.GetAllInsurances();


            cbxInsurenseType.ItemsSource = insurancesCollection.Where(i => i.InsuranceName != "שגויים");
            cbxInsurenseType.DisplayMemberPath = "InsuranceName";
        }

        private void LoadAllAgent()
        {
            AgentsLogic al = new AgentsLogic();
            var agentCollection = al.GetAllAgents();

            cbxAgent.ItemsSource = agentCollection;
            cbxAgent.DisplayMemberPath = "FullName";
        }

        private void cancelOperation_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void btnImportProd_Click(object sender, RoutedEventArgs e)
        {
           // loading.IsBusy = true;

            Task.Factory.StartNew(() =>
            {
                Dispatcher.Invoke(DispatcherPriority.Normal, new Action(() =>
                {
                    //loading.BusyContent = "מושך נתונים";
                }));

                try
                {
                    ProductionUpload();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
               ).ContinueWith((task) =>
               {
                  // loading.IsBusy = false;
                   //this.Close();
               }, TaskScheduler.FromCurrentSynchronizationContext()
               );
        }

        private void ProductionUpload()
        {
            //Company company = null;
            //Insurance insurance = null;
            
            bool isStringEmpty = string.IsNullOrWhiteSpace(_prodFileName);
            object com = null;
            object ins = null;
            object ag = null;
            bool isAgEnable = true;
            this.Dispatcher.Invoke(() =>
            {
                com = cbxCompany.SelectedItem;
                ins = cbxInsurenseType.SelectedItem;
                ag = cbxAgent.SelectedItem;
                //isAgEnable = cbxAgent.IsEnabled;
            });

            if (com != null && ins != null && ag != null && !isStringEmpty)
            {

                AgentSelected = (ag as Agent);

                CompanySelected = (com as Company);

                InsuranceSelected = (ins as Insurance);

                string[] agentNumbers = AgentSelected.AgentNumbers.Where(n => n.CompanyID == CompanySelected.CompanyID && n.InsuranceID == InsuranceSelected.InsuranceID).Select(n => n.Number).ToArray();

                if (agentNumbers.Count() == 0)
                {
                    UpdateAgent = true;
                    MessageBox.Show("נא עדכן את מספרי הסוכן לצורך משיכת פרודוקציה");
                    this.Dispatcher.Invoke(() =>
                    {
                        Close();
                    });
                    return;
                }

                ProdoctionCompanyBase prodoctionCompany = ProdocationCompanyFactory.Factory(CompanySelected.CompanyName, _prodFileName, InsuranceSelected.InsuranceName);

                if (isAgEnable && AgentSelected != null)
                {
                    int agentID = AgentSelected.AgentID;
                    prodoctionCompany.ProdoctionFormat.SetAgentID(agentID);
                }

                else if (!prodoctionCompany.ProdoctionFormat.IsAgentDetected(CompanySelected.CompanyID, InsuranceSelected.InsuranceID))
                {
                    cbxAgent.IsEnabled = true;
                    MessageBox.Show("סוכן לא זוהה באופן אוטוטמטי אנא בחר סוכן");
                    return;
                }

                try
                {
                    prodoctionCompany.ProdoctionFormat.ConvertToDB(CompanySelected.CompanyID);
                    MessageBox.Show("משיכת הפרודוקציה בוצעה בהצלחה");
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "", MessageBoxButton.OK, MessageBoxImage.None, MessageBoxResult.None, System.Windows.MessageBoxOptions.RightAlign);
                    return;
                }


            }
            else
            {
                throw new Exception("חובה להזין את כל הפרמטרים");
                //MessageBox.Show("חובה להזין את כל הפרמטרים");                
            }

        }
    }
}
