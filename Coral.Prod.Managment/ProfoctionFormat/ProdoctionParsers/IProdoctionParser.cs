﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coral.Prod.Managment.ProfoctionFormat.ProdoctionParsers
{
    public interface IProdoctionParser
    {
       
        void ParseProdoction(string directoryName);

        void ConvertToDB(int companyId);

        bool IsAgentDetected(int companyId, int insurenceID);

        void SetAgentID(int agentID);
    }
}
