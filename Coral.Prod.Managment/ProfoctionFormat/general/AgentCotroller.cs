﻿using CoralBusinessLogics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coral.Prod.Managment.ProfoctionFormat.general
{
    public class AgentCotroller
    {

        public static void UpdateAgentIfNeeded(int companyId, int insurenceID, string agentNumberSTR, int agentId)
        {

            int agentIDFound;
            if (!IsAgentDetected(companyId, insurenceID, agentNumberSTR, out agentIDFound))
            {

                AgentNumber agentNumber = new AgentNumber();
                agentNumber.CompanyID = companyId;
                agentNumber.InsuranceID = insurenceID;
                agentNumber.AgentID = agentId;
                agentNumber.Number = agentNumberSTR;

                using (var dbCtx = new Coral_DB_Entities())
                {
                    dbCtx.AgentNumbers.Add(agentNumber);
                    dbCtx.SaveChanges();
                }
            }
        }

        public static bool IsAgentDetected(int companyId, int insurenceID, string agentNumberSTR, out int agentIDFound)
        {
            bool agentFound = false;
            agentIDFound = 0;
            using (var dbCtx = new Coral_DB_Entities())
            {
                var agentNumberInContext = dbCtx.AgentNumbers.FirstOrDefault(c => c.CompanyID == companyId && c.InsuranceID == insurenceID && c.Number == agentNumberSTR);

                if (agentNumberInContext != null)
                {
                    agentIDFound = agentNumberInContext.AgentID;
                    agentFound = true;
                }
            }


            return agentFound;
        }
    }
}
