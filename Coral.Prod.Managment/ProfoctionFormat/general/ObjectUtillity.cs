﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Coral.Prod.Managment.ProfoctionFormat.general
{
    public class ObjectUtillity
    {

        public static void UpdateAccordingToType<T>(JToken itemPol, T updatedObject, JToken x, string key, PropertyInfo property)
        {
            if (((JProperty)(x)).Value.Type == JTokenType.Integer)
            {
                property.SetValue(updatedObject, itemPol.Value<int>(key));
            }
            else if (((JProperty)(x)).Value.Type == JTokenType.String)
            {
                property.SetValue(updatedObject, itemPol.Value<string>(key));
            }
            else if (((JProperty)(x)).Value.Type == JTokenType.Date)
            {
                property.SetValue(updatedObject, itemPol.Value<DateTime>(key));
            }
            else if (((JProperty)(x)).Value.Type == JTokenType.Float)
            {
                property.SetValue(updatedObject, itemPol.Value<Double>(key));
            }
            else if (((JProperty)(x)).Value.Type == JTokenType.Boolean)
            {
                property.SetValue(updatedObject, itemPol.Value<bool>(key));
            }
        }

        public static void makeStringEmptyAllNullValue<T>(T updatedObject)
        {
            PropertyInfo[] propertyInfo = typeof(T).GetProperties();
            foreach (var itemProperty in propertyInfo)
            {
                var propertyType = itemProperty.PropertyType;
                if (!propertyType.IsGenericType)
                {

                    if (itemProperty.GetValue(updatedObject) == null)
                    {
                        if (propertyType == typeof(String))
                            itemProperty.SetValue(updatedObject, String.Empty);
                    }
                }
            }
        }



    }
}
