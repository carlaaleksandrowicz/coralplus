﻿using CoralBusinessLogics;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Coral.Prod.Managment.ProfoctionFormat.general
{


    public class ClientController
    {
        // add client if not exsist
        public static Dictionary<string, Client> AddClientCollection(JArray clientCollection, int agentId, int categoryClientID, int clientTypeID)
        {
            Dictionary<string, Client> clientDic = new Dictionary<string, Client>();

            foreach (JToken itemClient in clientCollection)
            {

                var client = AddClient(itemClient, agentId, categoryClientID, clientTypeID);
                string id = client.IdNumber;

      
                if (itemClient["IdNumberRef"] != null)
                {
                    id = (string)itemClient["IdNumberRef"];
                }

                if (!clientDic.ContainsKey(id.ToString()))
                {
                    clientDic.Add(id.ToString(), client);
                }
            }

            return clientDic;
        }
        private static Client AddClient(JToken itemClient, int agentID, int categoryID, int clientTypeID)
        {
            Client client = new Client();
            try
            {

                foreach (var x in itemClient)
                {

                    var key = ((JProperty)(x)).Name;
                    var property = typeof(Client).GetProperty(key);
                    try
                    {

                        if (key == "IdNumberRef")
                        {
                            continue;
                        }

                        if (key == "IdNumber" || key == "ZipCode")
                        {
                            property.SetValue(client, itemClient.Value<int>(key).ToString());
                        }


                        else if (key == "BirthDate")
                        {
                            property.SetValue(client, ParseBirthDate(itemClient.Value<int>(key)));
                        }

                        else if (key == "MaritalStatus")
                        {
                            client.MaritalStatus = itemClient.Value<string>(key).ToString();
                        }

                        else if (((JProperty)(x)).Value.Type == JTokenType.Integer)
                        {
                            property.SetValue(client, itemClient.Value<int>(key));
                        }
                        else if (((JProperty)(x)).Value.Type == JTokenType.String)
                        {
                            property.SetValue(client, itemClient.Value<string>(key));
                        }
                        else if (((JProperty)(x)).Value.Type == JTokenType.Date)
                        {
                            property.SetValue(client, itemClient.Value<DateTime>(key));
                        }
                        else if (((JProperty)(x)).Value.Type == JTokenType.Float)
                        {
                            property.SetValue(client, itemClient.Value<Double>(key));
                        }
                        else if (((JProperty)(x)).Value.Type == JTokenType.Boolean)
                        {
                            property.SetValue(client, itemClient.Value<bool>(key));
                        }
                    }
                    catch (Exception ex)
                    {

                    }

                }

                client.PrincipalAgentID = agentID;
                client.SecundaryAgentID = agentID;
                client.CategoryID = categoryID;
                client.ClientTypeID = clientTypeID;
                client.OpenDate = DateTime.Now;


                PropertyInfo[] propertyInfo = typeof(Client).GetProperties();
                foreach (var itemProperty in propertyInfo)
                {
                    var propertyType = itemProperty.PropertyType;
                    if (!propertyType.IsGenericType)
                    {

                        if (itemProperty.GetValue(client) == null)
                        {
                            if (propertyType == typeof(String))
                                itemProperty.SetValue(client, String.Empty);
                        }
                    }
                }

                using (var dbCtx = new Coral_DB_Entities())
                {
                    string idNumWhithCero = "0" + client.IdNumber;
                    var clientInContext = dbCtx.Clients.FirstOrDefault(c => c.IdNumber == client.IdNumber);
                    if (clientInContext == null)
                    {
                        clientInContext = dbCtx.Clients.FirstOrDefault(c => c.IdNumber == idNumWhithCero);
                        if (clientInContext == null)
                        {
                            dbCtx.Clients.Add(client);
                            dbCtx.SaveChanges();
                            AddClientFolder(client.ClientID);
                        }
                    }



                }
            }
            catch (Exception ex)
            {

            }
            using (var dbCtx = new Coral_DB_Entities())
            {
                var clientInContext = dbCtx.Clients.FirstOrDefault(c => c.IdNumber == client.IdNumber);
                if (clientInContext == null)
                {
                    clientInContext = dbCtx.Clients.FirstOrDefault(c => c.IdNumber == "0" + client.IdNumber);
                }
                return clientInContext;
            }
        }

        private static void AddClientFolder(int clientID)
        {
            string strAppDir = System.AppDomain.CurrentDomain.BaseDirectory;
            string path = File.ReadAllText(strAppDir + @"Coral Files\ClientsPath.txt");
            if (Directory.Exists(path))
            {
                string clientPath = path + @"\" + clientID + @"\כללי";
                if (!Directory.Exists(clientPath))
                {
                    Directory.CreateDirectory(clientPath);
                }
            }

        }


        #region ParseMethod
        private static DateTime ParseBirthDate(int val)
        {
            var dateStr = val.ToString();
            DateTime ret;
            if (dateStr.Length > 6)
            {
                dateStr = dateStr.PadLeft(8, '0');
                ret = DateTime.ParseExact(string.Format("{0}", dateStr), "ddMMyyyy", CultureInfo.InvariantCulture);

                return ret;
            }
            else
            {
                dateStr = dateStr.PadLeft(6, '0');
                ret = DateTime.ParseExact(string.Format("{0}", dateStr), "yyMMdd", CultureInfo.InvariantCulture);
            }



            return ret;
        }

        #endregion
    }
}
