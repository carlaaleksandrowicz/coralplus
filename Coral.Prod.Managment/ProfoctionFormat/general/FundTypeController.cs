﻿using CoralBusinessLogics;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coral.Prod.Managment.ProfoctionFormat.general
{
    public class FundTypeController
    {
        public static Dictionary<int, string> AddFundTypesCollection(JArray fundTypesCollection, Dictionary<int, int> fundInsurnceDic)
        {
            Dictionary<int, string> fundTypesDic = new Dictionary<int, string>();

            foreach (JToken itemFundType in fundTypesCollection)
            {
                int lifeInsurnceTypeProdID = itemFundType["InsurnceCode"].Value<int>();

                if (fundInsurnceDic.ContainsKey(lifeInsurnceTypeProdID))
                {
                    fundTypesDic.Add(lifeInsurnceTypeProdID, itemFundType["InsurnceDesc"].Value<string>());
              
                }
            }

            return fundTypesDic;
        }


    }
}
