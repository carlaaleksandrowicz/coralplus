﻿using CoralBusinessLogics;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coral.Prod.Managment.ProfoctionFormat.general
{
    public class HealthInsuranceTypeController
    {
        public static Dictionary<int, HealthInsuranceType> AddHealthInsuranceTypesCollection(JArray inuranceHealthTypesCollection, int companyID)
        {
            Dictionary<int, HealthInsuranceType> insuranceTypesDic = new Dictionary<int, HealthInsuranceType>();

            foreach (JToken itemInsuranceType in inuranceHealthTypesCollection)
            {
                string HealthInsurnceTypeProdID = itemInsuranceType["PolicyCodeType"].Value<string>();
                var HealthInsuranceType = AddHealthInsurnceType(itemInsuranceType, companyID);
                insuranceTypesDic.Add(int.Parse(HealthInsurnceTypeProdID), HealthInsuranceType);
            }

            return insuranceTypesDic;
        }

        private static HealthInsuranceType AddHealthInsurnceType(JToken HealthInsurnceType, int companyID)
        {
            HealthIndustriesLogic HealthIndustryLogic = new HealthIndustriesLogic();

            string HealthInsurnceTypeName = HealthInsurnceType["PolicyTypeDesc"].Value<string>();
            string HealthInsurnceTypeProdID = HealthInsurnceType["PolicyCodeType"].Value<string>();

            try
            {
                HealthIndustryLogic.InsertHealthInsuranceType(HealthInsurnceTypeName, true, companyID);
            }
            catch
            {

            }

            using (var dbCtx = new Coral_DB_Entities())
            {
                var HealthInsuranceType = dbCtx.HealthInsuranceTypes.FirstOrDefault(i => i.HealthInsuranceTypeName == HealthInsurnceTypeName);
                return HealthInsuranceType;
            }
        }
    }
}
