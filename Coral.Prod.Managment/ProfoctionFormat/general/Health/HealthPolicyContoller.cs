﻿using Coral.Prod.Managment.ProdoctionCompany.Company.Clal.LifeHealth.resources;
using CoralBusinessLogics;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Coral.Prod.Managment.ProfoctionFormat.general
{
    public class HealthPolicyContoller
    {
        private Dictionary<string, Client> _clientDics = new Dictionary<string, Client>();
        private Dictionary<int, HealthInsuranceType> _healthInsuranceTypeDics = new Dictionary<int, HealthInsuranceType>();
        private Dictionary<string, List<HealthCoverage>> _healthCoverageDic = new Dictionary<string, List<HealthCoverage>>();

        public HealthPolicyContoller(Dictionary<string, Client> clientDics, Dictionary<int, HealthInsuranceType> healthInsuranceTypeDics, Dictionary<string, List<HealthCoverage>> healthCoverageDic)
        {
            _clientDics = clientDics;
            _healthInsuranceTypeDics = healthInsuranceTypeDics;
            _healthCoverageDic = healthCoverageDic;
        }

        public void AddHealthPolicyCollection(JArray healthhPolicyArr, int insurnceId, int companyID)
        {
            foreach (var healthPol in healthhPolicyArr)
            {
                AddHealthPolicy(healthPol, insurnceId, companyID);
            }
        }

        private DateTime? GetLastDateCovvarage(ICollection<HealthCoverage> HealthCoverages)
        {
            DateTime? max = null;
            foreach (var item in HealthCoverages)
            {
                if (item.EndDate.HasValue)
                {
                    if (max == null || item.EndDate > max)
                    {
                        max = item.EndDate;
                    }
                }

            }

            return max;


        }

        private void AddHealthPolicy(JToken itemPol, int insuranceID, int companyID)
        {

            HealthPolicy healthPolicie = new HealthPolicy();

            #region import all data
            foreach (var x in itemPol)
            {
                var key = ((JProperty)(x)).Name;
                var property = typeof(HealthPolicy).GetProperty(key);

                if (property == null) continue;
                if (key == "Hatzmada")
                {
                    property.SetValue(healthPolicie, ParseHatzmada(itemPol.Value<string>(key)));
                }

                else if (key == "PaymentMethod")
                {
                    property.SetValue(healthPolicie, ParsePaymentMethod(itemPol.Value<string>(key)));
                }

                else if (key == "BankName")
                {
                    property.SetValue(healthPolicie, itemPol.Value<int>(key).ToString());
                }

                else if (key == "AccountNumber")
                {
                    property.SetValue(healthPolicie, itemPol.Value<int>(key).ToString());
                }

                else if (key == "StartDate")
                {
                    property.SetValue(healthPolicie, ParseStartDate(itemPol.Value<int>(key)));
                }

                else if (key == "TotalMonthlyPayment")
                {
                    property.SetValue(healthPolicie, (decimal)(itemPol.Value<int>(key)));
                }

                else if (key == "PrimaryIndex")
                {
                    property.SetValue(healthPolicie, itemPol.Value<int>(key).ToString());
                }
                else if (key == "PolicyNumber")
                {
                    healthPolicie.PolicyNumber = itemPol.Value<int>(key).ToString();
                }
                else if (key == "HealthInsuranceTypeID")
                {
                    int val = itemPol.Value<int>(key);
                    property.SetValue(healthPolicie, _healthInsuranceTypeDics[val].HealthInsuranceTypeID);
                }
                else if (key == "PrincipalAgentNumberID")
                {
                    healthPolicie.PrincipalAgentNumberID = null;
                }
                else if (key == "CompanyID")
                {
                    healthPolicie.CompanyID = itemPol.Value<int>(key);
                }
                else if (key == "ClientID")
                {
                    healthPolicie.ClientID = itemPol.Value<int>(key);
                }

                else
                {
                    ObjectUtillity.UpdateAccordingToType<HealthPolicy>(itemPol, healthPolicie, x, key, property);
                }

            }
            #endregion

            #region set all refernce value
            healthPolicie.OpenDate = DateTime.Now;
            healthPolicie.EndDate = null;
            healthPolicie.PrincipalAgentNumberID = null;
            healthPolicie.SubAgentNumberID = null;
            healthPolicie.InsuranceID = insuranceID;
            healthPolicie.CompanyID = companyID;
            healthPolicie.ClientID = _clientDics[healthPolicie.ClientID.ToString()].ClientID;

            if (_healthCoverageDic.ContainsKey(healthPolicie.PolicyNumber))
            {
                healthPolicie.HealthCoverages = _healthCoverageDic[healthPolicie.PolicyNumber];
                healthPolicie.EndDate = GetLastDateCovvarage(healthPolicie.HealthCoverages);
            }
            #endregion

            #region set all null value
            ObjectUtillity.makeStringEmptyAllNullValue(healthPolicie);
            #endregion

            #region saveData 
            using (var dbCtx = new Coral_DB_Entities())
            {
                var clientInContext = dbCtx.HealthPolicies.FirstOrDefault(c => c.PolicyNumber == healthPolicie.PolicyNumber);
                if (clientInContext == null)
                {
                    dbCtx.HealthPolicies.Add(healthPolicie);
                }

                dbCtx.SaveChanges();
            }
            #endregion

        }

        #region ParseMethod
        private string ParsePaymentMethod(string val)
        {
            switch (val)
            {
                case "1":
                    return ClalApexLifeBritutResources.PaymentMethod1;

                case "2":
                    return ClalApexLifeBritutResources.PaymentMethod2;

                case "3":
                    return ClalApexLifeBritutResources.PaymentMethod3;

                case "4":
                    return ClalApexLifeBritutResources.PaymentMethod4;

                case "5":
                    return ClalApexLifeBritutResources.PaymentMethod4;

                default:
                    return string.Empty;

            }

        }

        private bool ParseHatzmada(string val)
        {
            return val != "0";
        }

        private DateTime ParseStartDate(int val)
        {
            var dateStr = val.ToString().PadLeft(4, '0');

            DateTime ret = DateTime.ParseExact(string.Format("20{0}01", dateStr), "yyyyMMdd", CultureInfo.InvariantCulture);

            if (ret > DateTime.Today)
            {
                ret = DateTime.ParseExact(string.Format("19{0}01", dateStr), "yyyyMMdd", CultureInfo.InvariantCulture);
            }

            return ret;


        }

        private DateTime ParseBirthDate(int val)
        {
            var dateStr = val.ToString().PadLeft(6, '0');
            var ret = DateTime.ParseExact(string.Format("{0}", dateStr), "yyMMdd", CultureInfo.InvariantCulture);

            return ret;
        }

        private DateTime ParseCovDateDate(int val)
        {
            var dateStr = val.ToString().PadLeft(6, '0');
            var ret = DateTime.ParseExact(string.Format("{0}", dateStr), "yyMMdd", CultureInfo.InvariantCulture);
            if (DateTime.Today > ret)
            {
                ret = DateTime.ParseExact(string.Format("20{0}", dateStr), "yyyyMMdd", CultureInfo.InvariantCulture);
            }

            return ret;
        }

        #endregion
    }


}
