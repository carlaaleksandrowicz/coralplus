﻿using CoralBusinessLogics;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Coral.Prod.Managment.ProfoctionFormat.general
{
    public class PolicyOwnerController
    {
        // add client if not exsist
        public static Dictionary<string, PolicyOwner> AddPolicyOwnerControllerCollection(JArray policyOwnerCollection, int insuranceID, int companyID)
        {
            Dictionary<string, PolicyOwner> policyOwnerDic = new Dictionary<string, PolicyOwner>();

            foreach (JToken itemPolicyOwner in policyOwnerCollection)
            {
                var policyOwner = AddPolicyOwner(itemPolicyOwner, insuranceID, companyID);
                if (!policyOwnerDic.ContainsKey(itemPolicyOwner["PolicyOwnerNumber"].ToString()))
                {
                    policyOwnerDic.Add(itemPolicyOwner["PolicyOwnerNumber"].ToString(), policyOwner);
                }
            }

            return policyOwnerDic;
        }
        private static PolicyOwner AddPolicyOwner(JToken itemPolicyOwner, int insuranceID, int companyID)
        {
            PolicyOwner policyOwner = new PolicyOwner();
            string policyOwnerNumber = itemPolicyOwner["PolicyOwnerNumber"].ToString();
            try
            {

                foreach (var x in itemPolicyOwner)
                {

                    var key = ((JProperty)(x)).Name;
                    var property = typeof(PolicyOwner).GetProperty(key);
                    try
                    {

                        if (key == "PolicyOwnerNumber")
                        {
                            continue;
                        }

                        else
                        {
                            ObjectUtillity.UpdateAccordingToType<PolicyOwner>(itemPolicyOwner, policyOwner, x, key, property);
                        }
                    }
                    catch (Exception ex)
                    {

                    }

                }

                ObjectUtillity.makeStringEmptyAllNullValue<PolicyOwner>(policyOwner);
                policyOwner.IdNumber = policyOwnerNumber;
                FactoryNumber fn = new FactoryNumber();
                fn.InsuranceID = insuranceID;
                fn.CompanyID = companyID;
                fn.Number = policyOwnerNumber;

                policyOwner.FactoryNumbers.Add(fn);



                using (var dbCtx = new Coral_DB_Entities())
                {
                    var factoryNumbersContext = dbCtx.FactoryNumbers.FirstOrDefault(f => f.CompanyID == companyID && f.InsuranceID == insuranceID && f.Number == policyOwnerNumber);

                    if (factoryNumbersContext == null)
                    {
                        //Add Student object into Students DBset
                        dbCtx.PolicyOwners.Add(policyOwner);
                    }

                    // call SaveChanges method to save student into database
                    dbCtx.SaveChanges();


                }
            }
            catch (Exception ex)
            {

            }
            using (var dbCtx = new Coral_DB_Entities())
            {
                var factoryNumbersContext = dbCtx.FactoryNumbers.FirstOrDefault(f => f.CompanyID == companyID && f.InsuranceID == insuranceID && f.Number == policyOwnerNumber);
                return (factoryNumbersContext.PolicyOwner);
            }
        }


        #region ParseMethod
        private static DateTime ParseBirthDate(int val)
        {
            var dateStr = val.ToString().PadLeft(6, '0');
            var ret = DateTime.ParseExact(string.Format("{0}", dateStr), "yyMMdd", CultureInfo.InvariantCulture);

            return ret;
        }

        #endregion
    }
}
