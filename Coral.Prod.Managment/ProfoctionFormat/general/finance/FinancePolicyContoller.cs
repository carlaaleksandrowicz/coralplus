﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CoralBusinessLogics;
using Newtonsoft.Json.Linq;
using System.Globalization;
using Coral.Prod.Managment.ProfoctionFormat.genral;

namespace Coral.Prod.Managment.ProfoctionFormat.general.Elementry
{

    public class FinancePolicyContoller
    {


        //   private Dictionary<int, LifeInsuranceType> _lifeInsuranceTypeDic = new Dictionary<int, LifeInsuranceType>();

        public FinancePolicyContoller()
        {

        }

        public void AddFinancePolicyCollection(JArray collection)
        {
            foreach (var itemPol in collection)
            {
                AddFinancePolicy(itemPol);
            }
        }

        private void AddFinancePolicy(JToken itemPol)
        {
            FinancePolicy financePolicy = new FinancePolicy();
            List<string> ls = new List<string>();
            #region import all data
            foreach (var x in itemPol)
            {
                var key = ((JProperty)(x)).Name;
                var property = typeof(FinancePolicy).GetProperty(key);
                try
                {
                    if (property == null)
                    {
                        ls.Add(key);
                        continue;
                    }

                    else if (key == "OpenDate" || key == "StartDate" || key == "StartDateEmployee")
                    {
                        property.SetValue(financePolicy, ParseStartDate(itemPol.Value<string>(key), (key == "StartDate")));
                    }
                    else if (key == "AdministrationFeesEmployee" || key == "AggregateFeesEmployee" || key == "WorkerDeposit" || key == "ReportedSalary" || key == "EmployerDeposit" || key == "DepositAmountEmployee")
                    {
                        property.SetValue(financePolicy, (decimal)(itemPol.Value<double>(key)));
                    }

                    else
                    {
                        ObjectUtillity.UpdateAccordingToType<FinancePolicy>(itemPol, financePolicy, x, key, property);
                    }

                }
                catch
                {
                    ls.Add(key);
                    continue;
                }

            }
            #endregion

            #region set all refernce value
            financePolicy.PrincipalAgentNumberID = null;
            financePolicy.SubAgentNumberID = null;
            financePolicy.ProgramID = InitialProdoction.GetFinanceProgram("כללי", financePolicy.ProgramTypeID.Value, financePolicy.CompanyID.Value);




            #endregion

            #region set all null value
            ObjectUtillity.makeStringEmptyAllNullValue(financePolicy);
            #endregion

            #region saveData 
            using (var dbCtx = new Coral_DB_Entities())
            {
                var clientInContext = dbCtx.FinancePolicies.FirstOrDefault(c => c.AssociateNumber == financePolicy.AssociateNumber);

                if (clientInContext == null)
                {

                    dbCtx.FinancePolicies.Add(financePolicy);
                    dbCtx.SaveChanges();
                }


            }
            #endregion

        }



        private DateTime? ParseStartDate(string val, bool isStartDate = true)
        {
            DateTime ret = new DateTime();
            try
            {

                ret = DateTime.ParseExact(string.Format("{0}", val), "yyyyMMdd", CultureInfo.InvariantCulture);


            }
            catch
            {
                return null;
            }
            return ret;


        }
    }

}
