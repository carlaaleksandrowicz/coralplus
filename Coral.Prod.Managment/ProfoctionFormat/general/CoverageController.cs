﻿using CoralBusinessLogics;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Coral.Prod.Managment.ProfoctionFormat.general
{
    public class CoverageController
    {
        public static Dictionary<string, List<LifeCoverage>> AddLifeCoverageCollection(JArray lifeCoverageCollection)
        {
            Dictionary<string, List<LifeCoverage>> dLifeCoverageDictionary = new Dictionary<string, List<LifeCoverage>>();

            foreach (JToken itemCov in lifeCoverageCollection)
            {
                LifeCoverage lifeCoverage = ConvertLifeCoverage(itemCov);
                if (dLifeCoverageDictionary.ContainsKey(lifeCoverage.LifePolicyID.ToString()))
                {
                    dLifeCoverageDictionary[lifeCoverage.LifePolicyID.ToString()].Add(lifeCoverage);
                }
                else
                {
                    List<LifeCoverage> l = new List<LifeCoverage>();
                    l.Add(lifeCoverage);
                    dLifeCoverageDictionary.Add(lifeCoverage.LifePolicyID.ToString(), l);
                }
            }

            return dLifeCoverageDictionary;
        }

        public static Dictionary<string, List<ElementaryCoverage>> AddElementryCoverageCollection(JArray elemntryCoverageCollection, int elementryCoverageTypeID)
        {

            Dictionary<string, List<ElementaryCoverage>> coverageDictionary = new Dictionary<string, List<ElementaryCoverage>>();

            foreach (JToken itemCov in elemntryCoverageCollection)
            {
                //string[] coverageKey = new string[] { itemCov["ElementaryPolicyID"].ToString(),itemCov["Addition"].ToString() };


                ElementaryCoverage elementryCoverage = ConvertElemntryCoverage(itemCov, elementryCoverageTypeID);

                if (coverageDictionary.ContainsKey(itemCov["ElementaryPolicyID"].ToString()))
                {
                    coverageDictionary[itemCov["ElementaryPolicyID"].ToString()].Add(elementryCoverage);
                }
                else
                {
                    List<ElementaryCoverage> l = new List<ElementaryCoverage>();
                    l.Add(elementryCoverage);
                    coverageDictionary.Add(itemCov["ElementaryPolicyID"].ToString(), l);
                }
            }

            return coverageDictionary;
        }


        public static Dictionary<string, List<HealthCoverage>> AddHealthCoverageCollection(JArray lifeCoverageCollection, int healthCoverageTypeID)
        {
            Dictionary<string, List<HealthCoverage>> dHealthCoverageDictionary = new Dictionary<string, List<HealthCoverage>>();

            foreach (JToken itemCov in lifeCoverageCollection)
            {
                HealthCoverage healthCoverage = ConvertHealthCoverage(itemCov, healthCoverageTypeID);
                if (dHealthCoverageDictionary.ContainsKey(healthCoverage.HealthPolicyID.ToString()))
                {
                    dHealthCoverageDictionary[healthCoverage.HealthPolicyID.ToString()].Add(healthCoverage);
                }
                else
                {
                    List<HealthCoverage> l = new List<HealthCoverage>();
                    l.Add(healthCoverage);
                    dHealthCoverageDictionary.Add(healthCoverage.HealthPolicyID.ToString(), l);
                }
            }

            return dHealthCoverageDictionary;
        }

        private static LifeCoverage ConvertLifeCoverage(JToken covrageItem)
        {

            LifeCoverage lifeCoverage = new LifeCoverage();
            foreach (var x in covrageItem)
            {
                var key = ((JProperty)(x)).Name;
                var property = typeof(LifeCoverage).GetProperty(key);

                if (property == null) continue;


                if (key == "StartDate")
                {
                    property.SetValue(lifeCoverage, ParseBirthDate(covrageItem.Value<int>(key)));
                }

                else if (key == "EndDate")
                {
                    property.SetValue(lifeCoverage, ParseBirthDate(covrageItem.Value<int>(key)));
                }



                else if (key == "Status")
                {
                    var val = covrageItem.Value<int>(key);
                    if (val == 1)
                    {
                        property.SetValue(lifeCoverage, true);
                    }
                    else
                    {
                        property.SetValue(lifeCoverage, false);
                    }
                }

                else if (key == "InsuranceAmount" || key == "MedicalAddition" || key == "ProfessionalAddition" || key == "IndexedAmount" || key == "MonthlyPremuim" || key == "IndexedPremium")
                {
                    property.SetValue(lifeCoverage, (decimal)(covrageItem.Value<int>(key)));
                }
                           


                else if (((JProperty)(x)).Value.Type == JTokenType.Integer)
                {
                    property.SetValue(lifeCoverage, covrageItem.Value<int>(key));
                }
                else if (((JProperty)(x)).Value.Type == JTokenType.String)
                {
                    property.SetValue(lifeCoverage, covrageItem.Value<string>(key));
                }
                else if (((JProperty)(x)).Value.Type == JTokenType.Date)
                {
                    property.SetValue(lifeCoverage, covrageItem.Value<DateTime>(key));
                }
                else if (((JProperty)(x)).Value.Type == JTokenType.Float)
                {
                    property.SetValue(lifeCoverage, covrageItem.Value<Double>(key));
                }
                else if (((JProperty)(x)).Value.Type == JTokenType.Boolean)
                {
                    property.SetValue(lifeCoverage, covrageItem.Value<bool>(key));
                }

            }

            lifeCoverage.LifeCoverageTypeID = covrageItem.Value<int>("LifeCoverageTypeID");

            #region set all null value
            PropertyInfo[] propertyInfo = typeof(LifeCoverage).GetProperties();
            foreach (var itemProperty in propertyInfo)
            {
                var propertyType = itemProperty.PropertyType;
                if (!propertyType.IsGenericType)
                {

                    if (itemProperty.GetValue(lifeCoverage) == null)
                    {
                        if (propertyType == typeof(String))
                            itemProperty.SetValue(lifeCoverage, String.Empty);
                    }
                }
            }
            #endregion

            return lifeCoverage;


        }

        private static ElementaryCoverage ConvertElemntryCoverage(JToken covrageItem, int elementryCoverageTypeID)
        {

            ElementaryCoverage elementaryCoverage = new ElementaryCoverage();

            int premiumMark = covrageItem["premiumMark"].Value<int>();
            int CoverageAmountMark = covrageItem["CoverageAmountMark"].Value<int>();
            int PorcentageMark = covrageItem["PorcentageMark"].Value<int>();

            foreach (var x in covrageItem)
            {
                var key = ((JProperty)(x)).Name;
                var property = typeof(ElementaryCoverage).GetProperty(key);

                 

                if (key == "premiumMark" || key == "PorcentageMark" || key == "CoverageAmountMark")
                {
                    continue;
                }

                else if (property == null) continue;

                else if (key == "premium")
                {
                    decimal val = (decimal)(covrageItem.Value<double>(key));
                    if (premiumMark == 0)
                    {
                        property.SetValue(elementaryCoverage, val);
                    }
                    else
                    {
                        property.SetValue(elementaryCoverage, -1 * val);
                    }
                }
                else if (key == "CoverageAmount")
                {
                    decimal val = (decimal)(covrageItem.Value<double>(key));
                    if (CoverageAmountMark == 0)
                    {
                        property.SetValue(elementaryCoverage, val);
                    }
                    else
                    {
                        property.SetValue(elementaryCoverage, -1 * val);
                    }
                }
                else if (key == "Porcentage")
                {
                    decimal val = (decimal)(covrageItem.Value<double>(key));
                    if (PorcentageMark == 0)
                    {
                        property.SetValue(elementaryCoverage, val);
                    }
                    else
                    {
                        property.SetValue(elementaryCoverage, -1 * val);
                    }
                
                }

                else if (key == "ElementaryPolicyID")
                {
                    continue;
                }


                else if (((JProperty)(x)).Value.Type == JTokenType.Integer)
                {
                    property.SetValue(elementaryCoverage, covrageItem.Value<int>(key));
                }
                else if (((JProperty)(x)).Value.Type == JTokenType.String)
                {
                    property.SetValue(elementaryCoverage, covrageItem.Value<string>(key));
                }
                else if (((JProperty)(x)).Value.Type == JTokenType.Date)
                {
                    property.SetValue(elementaryCoverage, covrageItem.Value<DateTime>(key));
                }
                else if (((JProperty)(x)).Value.Type == JTokenType.Float)
                {
                    property.SetValue(elementaryCoverage, covrageItem.Value<Double>(key));
                }
                else if (((JProperty)(x)).Value.Type == JTokenType.Boolean)
                {
                    property.SetValue(elementaryCoverage, covrageItem.Value<bool>(key));
                }

            }





            elementaryCoverage.ElementaryCoverageTypeID = elementryCoverageTypeID;

            #region set all null value
            PropertyInfo[] propertyInfo = typeof(ElementaryCoverage).GetProperties();
            foreach (var itemProperty in propertyInfo)
            {
                var propertyType = itemProperty.PropertyType;
                if (!propertyType.IsGenericType)
                {

                    if (itemProperty.GetValue(elementaryCoverage) == null)
                    {
                        if (propertyType == typeof(String))
                            itemProperty.SetValue(elementaryCoverage, String.Empty);
                    }
                }
            }
            #endregion

            return elementaryCoverage;


        }

        private static HealthCoverage ConvertHealthCoverage(JToken covrageItem, int HealthCoverageTypeID)
        {

            HealthCoverage healthCoverage = new HealthCoverage();
            foreach (var x in covrageItem)
            {
                var key = ((JProperty)(x)).Name;
                var property = typeof(HealthCoverage).GetProperty(key);



                if (key == "StartDate")
                {
                    property.SetValue(healthCoverage, ParseBirthDate(covrageItem.Value<int>(key)));
                }

                else if (key == "EndDate")
                {
                    property.SetValue(healthCoverage, ParseCovDateDate(covrageItem.Value<int>(key)));
                }



                else if (key == "Status")
                {
                    var val = covrageItem.Value<int>(key);
                    if (val == 1)
                    {
                        property.SetValue(healthCoverage, true);
                    }
                    else
                    {
                        property.SetValue(healthCoverage, false);
                    }
                }

                else if (key == "InsuranceAmount" || key == "MedicalAddition" || key == "ProfessionalAddition" || key == "IndexedAmount" || key == "MonthlyPremuim" || key == "IndexedPremium")
                {
                    property.SetValue(healthCoverage, (decimal)(covrageItem.Value<int>(key)));
                }



                else if (((JProperty)(x)).Value.Type == JTokenType.Integer)
                {
                    property.SetValue(healthCoverage, covrageItem.Value<int>(key));
                }
                else if (((JProperty)(x)).Value.Type == JTokenType.String)
                {
                    property.SetValue(healthCoverage, covrageItem.Value<string>(key));
                }
                else if (((JProperty)(x)).Value.Type == JTokenType.Date)
                {
                    property.SetValue(healthCoverage, covrageItem.Value<DateTime>(key));
                }
                else if (((JProperty)(x)).Value.Type == JTokenType.Float)
                {
                    property.SetValue(healthCoverage, covrageItem.Value<Double>(key));
                }
                else if (((JProperty)(x)).Value.Type == JTokenType.Boolean)
                {
                    property.SetValue(healthCoverage, covrageItem.Value<bool>(key));
                }

            }

            healthCoverage.HealthCoverageTypeID = HealthCoverageTypeID;

            #region set all null value
            PropertyInfo[] propertyInfo = typeof(HealthCoverage).GetProperties();
            foreach (var itemProperty in propertyInfo)
            {
                var propertyType = itemProperty.PropertyType;
                if (!propertyType.IsGenericType)
                {

                    if (itemProperty.GetValue(healthCoverage) == null)
                    {
                        if (propertyType == typeof(String))
                            itemProperty.SetValue(healthCoverage, String.Empty);
                    }
                }
            }
            #endregion

            return healthCoverage;

        }

        #region ParseMethod
        /*
        private static DateTime ParseBirthDate(int val)
        {
            var dateStr = val.ToString().PadLeft(8, '0');
            var ret = DateTime.ParseExact(string.Format("{0}", dateStr), "ddMMyyyy", CultureInfo.InvariantCulture);

            return ret;
        }
        */
        private static DateTime ParseBirthDate(int val)
        {
            var dateStr = val.ToString();
            DateTime ret;
            if (dateStr.Length > 6)
            {
                dateStr = dateStr.PadLeft(8, '0');
                ret = DateTime.ParseExact(string.Format("{0}", dateStr), "ddMMyyyy", CultureInfo.InvariantCulture);

                return ret;
            }
            else
            {
                dateStr = dateStr.PadLeft(6, '0');
                ret = DateTime.ParseExact(string.Format("{0}", dateStr), "yyMMdd", CultureInfo.InvariantCulture);
            }    
                
           

            return ret;
        }

        private static DateTime ParseCovDateDate(int val)
        {
            var dateStr = val.ToString().PadLeft(6, '0');
            var ret = DateTime.ParseExact(string.Format("{0}", dateStr), "yyMMdd", CultureInfo.InvariantCulture);
            if (DateTime.Today > ret)
            {
                ret = DateTime.ParseExact(string.Format("20{0}", dateStr), "yyyyMMdd", CultureInfo.InvariantCulture);
            }

            return ret;
        }
        #endregion
    }
}
