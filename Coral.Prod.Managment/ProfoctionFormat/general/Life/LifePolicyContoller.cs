﻿using Coral.Prod.Managment.ProdoctionCompany.Company.Clal.LifeHealth.resources;
using Coral.Prod.Managment.ProfoctionFormat.genral;
using CoralBusinessLogics;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coral.Prod.Managment.ProfoctionFormat.general.Life
{
    public class LifePolicyContoller
    {
        private Dictionary<string, List<LifeCoverage>> _lifeCoverageDic = new Dictionary<string, List<LifeCoverage>>();
        private Dictionary<int, string> _fundsDic = new Dictionary<int, string>();
        private Dictionary<string, Client> _clientDics = new Dictionary<string, Client>();
        private Dictionary<string, PolicyOwner> _policyOwnerDic = new Dictionary<string, PolicyOwner>();

        public LifePolicyContoller(Dictionary<string, List<LifeCoverage>> lifeCoverageDic, Dictionary<int, string> fundsDic, Dictionary<string, Client> clientDics, Dictionary<string, PolicyOwner> policyOwnerDic)
        {
            _lifeCoverageDic = lifeCoverageDic;
            _fundsDic = fundsDic;
            _clientDics = clientDics;
            _policyOwnerDic = policyOwnerDic;
        }

        public void AddLifePolicyCollection(JArray healthhPolicyArr, int companyId, int lifeIndustryID, int insurenceID)
        {
            foreach (var itemPol in healthhPolicyArr)
            {
                AddLifePolicy(itemPol, companyId, lifeIndustryID, insurenceID);
            }

        }
        private void AddLifePolicy(JToken itemPol, int companyID, int lifeIndustryID, int insurenceID)
        {

            LifePolicy lifePolicy = new LifePolicy();



            ManagerPolicyChanx managerPolicyChanx = new ManagerPolicyChanx();
            Risk risk = new Risk();
            PensionLifeIndustry pensionLifeIndustry = new PensionLifeIndustry();

            lifePolicy.ManagerPolicyChanges.Add(managerPolicyChanx);

            #region import all data
            foreach (var x in itemPol)
            {
                try
                {
                    var key = ((JProperty)(x)).Name;
                    var property = typeof(LifePolicy).GetProperty(key);

                    //life manager policy
                    if (key.StartsWith("ManagerPolicyChanx."))
                    {
                        string keyManagerPolicyChanx = key.Split('.')[1];
                        var propertyManagerPolicyChanx = typeof(ManagerPolicyChanx).GetProperty(keyManagerPolicyChanx);
                        try
                        {
                            propertyManagerPolicyChanx.SetValue(managerPolicyChanx, (decimal)(itemPol.Value<int>(key)));
                        }
                        catch
                        {
                            propertyManagerPolicyChanx.SetValue(managerPolicyChanx, itemPol.Value<int>(key));
                        }
                    }
                    else if (key.StartsWith("Risk."))
                    {
                        string keyRisk = key.Split('.')[1];
                        var propertyManagerPolicyChanx = typeof(Risk).GetProperty(keyRisk);
                        try
                        {
                            propertyManagerPolicyChanx.SetValue(risk, (decimal)(itemPol.Value<int>(key)));
                        }
                        catch
                        {
                            propertyManagerPolicyChanx.SetValue(risk, itemPol.Value<int>(key));
                        }
                    }

                    else if (key.StartsWith("PensionLifeIndustry."))
                    {
                        string keyPensionLifeIndustry = key.Split('.')[1];
                        var propertyManagerPolicyChanx = typeof(PensionLifeIndustry).GetProperty(keyPensionLifeIndustry);
                        try
                        {
                            propertyManagerPolicyChanx.SetValue(pensionLifeIndustry, (decimal)(itemPol.Value<int>(key)));
                        }
                        catch
                        {
                            propertyManagerPolicyChanx.SetValue(pensionLifeIndustry, itemPol.Value<int>(key));
                        }
                    }

                    else if (property == null) continue;

                    else if (key == "Hatzmada")
                    {
                        property.SetValue(lifePolicy, ParseHatzmada(itemPol.Value<string>(key)));
                    }

                    else if (key == "PaymentMethod")
                    {
                        property.SetValue(lifePolicy, ParsePaymentMethod(itemPol.Value<string>(key)));
                    }

                    else if (key == "BankPayment")
                    {
                        property.SetValue(lifePolicy, itemPol.Value<int>(key).ToString());
                    }

                    else if (key == "AccountNumberPayment")
                    {
                        property.SetValue(lifePolicy, itemPol.Value<int>(key).ToString());
                    }

                    else if (key == "StartDate")
                    {
                        property.SetValue(lifePolicy, ParseStartDate(itemPol.Value<int>(key)));
                    }

                    else if (key == "TotalMonthlyPayment")
                    {
                        property.SetValue(lifePolicy, (decimal)(itemPol.Value<int>(key)));
                    }

                    else if (key == "PrimaryIndex")
                    {
                        property.SetValue(lifePolicy, itemPol.Value<int>(key).ToString());
                    }
                    else if (key == "PolicyNumber")
                    {
                        property.SetValue(lifePolicy, itemPol.Value<int>(key).ToString());
                    }
                    else if (key == "PolicyOwnerID")
                    {
                        continue;
                    }

                    
                    else if (key == "FundTypeID")
                    {
                        var fundValue = itemPol.Value<int>(key);
                        if (_fundsDic.ContainsKey(fundValue))
                        {
                            var f = InitialProdoction.AddFundType(_fundsDic[fundValue], companyID, lifeIndustryID, insurenceID);
                            lifePolicy.FundTypeID = f.FundTypeID;
                        }
                        else
                        {
                            var f = InitialProdoction.AddFundType(_fundsDic.First().Value, companyID, lifeIndustryID, insurenceID);
                            lifePolicy.FundTypeID = f.FundTypeID;
                        }
                    }
                    else
                    {
                        ObjectUtillity.UpdateAccordingToType<LifePolicy>(itemPol, lifePolicy, x, key, property);
                    }

                }
                catch (Exception ex)
                {
                    continue;
                }

            }
            #endregion

            #region set all refernce value
            lifePolicy.OpenDate = DateTime.Now;
            lifePolicy.EndDate = null;
            lifePolicy.PrincipalAgentNumberID = null;
            lifePolicy.SubAgentNumberID = null;
            lifePolicy.Addition = 0;
            lifePolicy.CompanyID = companyID;
            lifePolicy.ClientID = _clientDics[lifePolicy.ClientID.ToString()].ClientID;
            lifePolicy.LifeIndustryID = lifeIndustryID;
            /*
            if (_policyOwnerDic.ContainsKey(itemPol["PolicyOwnerID"].ToString()))
            {
                lifePolicy.PolicyOwnerID = _policyOwnerDic[itemPol["PolicyOwnerID"].ToString()].PolicyOwnerID;
            }
            */
      

            if (_lifeCoverageDic.ContainsKey(lifePolicy.PolicyNumber))
            {
                lifePolicy.LifeCoverages = _lifeCoverageDic[lifePolicy.PolicyNumber];

                lifePolicy.EndDate = GetLastDateCovvarage(lifePolicy.LifeCoverages);
            }

            //מנהלים
            if (lifePolicy.InsuranceID == 1)
            {
                lifePolicy.ManagerPolicyChanges.Add(managerPolicyChanx);
            }
            //ריסק
            else if (lifePolicy.InsuranceID == 2)
            {
                lifePolicy.Risks.Add(risk);
            }
            //פנסיה
            else if (lifePolicy.InsuranceID == 5)
            {
                lifePolicy.PensionLifeIndustry = pensionLifeIndustry;
            }

            #endregion

            #region set all null value
            ObjectUtillity.makeStringEmptyAllNullValue(lifePolicy);
            #endregion

            #region saveData 
            using (var dbCtx = new Coral_DB_Entities())
            {


                var clientInContext = dbCtx.LifePolicies.FirstOrDefault(c => c.PolicyNumber == lifePolicy.PolicyNumber);
                if (clientInContext == null)
                {
                    dbCtx.LifePolicies.Add(lifePolicy);
                }

                try
                {
                    dbCtx.SaveChanges();

                }
                catch (Exception ex)
                {

                }
                #endregion

            }
        }

        private DateTime? GetLastDateCovvarage(ICollection<LifeCoverage> LifeCoverages)
        {
            DateTime? max = null;
            foreach (var item in LifeCoverages)
            {
                if (item.EndDate.HasValue)
                {
                    if (max == null || item.EndDate > max)
                    {
                        max = item.EndDate;
                    }
                }

            }

            return max;


        }


        #region parseMethods
        private string ParsePaymentMethod(string val)
        {
            switch (val)
            {
                case "1":
                    return ClalApexLifeBritutResources.PaymentMethod1;

                case "2":
                    return ClalApexLifeBritutResources.PaymentMethod2;

                case "3":
                    return ClalApexLifeBritutResources.PaymentMethod3;

                case "4":
                    return ClalApexLifeBritutResources.PaymentMethod4;

                case "5":
                    return ClalApexLifeBritutResources.PaymentMethod4;

                default:
                    return string.Empty;

            }

        }

        private bool ParseHatzmada(string val)
        {
            return val != "0";
        }

        private DateTime ParseStartDate(int val)
        {
            var dateStr = val.ToString();
            DateTime ret;
            if (dateStr.Length > 6)
            {
                dateStr = dateStr.PadLeft(8, '0');
                ret = DateTime.ParseExact(string.Format("{0}", dateStr), "ddMMyyyy", CultureInfo.InvariantCulture);

                return ret;
            }
            else
            {
                dateStr = dateStr.PadLeft(6, '0');
                ret = DateTime.ParseExact(string.Format("{0}", dateStr), "yyMMdd", CultureInfo.InvariantCulture);
            }



            return ret;


        }
        #endregion
    }
}
