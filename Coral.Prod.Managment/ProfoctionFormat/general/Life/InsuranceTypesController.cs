﻿using CoralBusinessLogics;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coral.Prod.Managment.ProfoctionFormat.general
{
    public class InsuranceTypesController
    {
        public static Dictionary<int, LifeInsuranceType> AddInsuranceLifeTypesCollection(JArray inuranceLifeTypesCollection, int lifeIndustryID)
        {
            Dictionary<int, LifeInsuranceType> insuranceTypesDic = new Dictionary<int, LifeInsuranceType>();

            foreach (JToken itemInsuranceType in inuranceLifeTypesCollection)
            {
                string lifeInsurnceTypeProdID = itemInsuranceType["PolicyCodeType"].Value<string>();
                var lifeInsuranceType = AddLifeInsurnceType(itemInsuranceType, lifeIndustryID);
                insuranceTypesDic.Add(int.Parse(lifeInsurnceTypeProdID), lifeInsuranceType);
            }

            return insuranceTypesDic;
            
        }

        private static LifeInsuranceType AddLifeInsurnceType(JToken lifeInsurnceType, int lifeIndustryID)
        {
            LifeIndustryLogic lifeIndustryLogic = new LifeIndustryLogic();

            string lifeInsurnceTypeName = lifeInsurnceType["PolicyTypeDesc"].Value<string>();
            string lifeInsurnceTypeProdID = lifeInsurnceType["PolicyCodeType"].Value<string>();

            try
            {
                lifeIndustryLogic.InsertLifeInsuranceType(lifeInsurnceTypeName, lifeIndustryID, true);
            }
            catch
            {

            }

            using (var dbCtx = new Coral_DB_Entities())
            {
                var lifeInsuranceType = dbCtx.LifeInsuranceTypes.FirstOrDefault(i => i.LifeInsuranceTypeName == lifeInsurnceTypeName);
                return lifeInsuranceType;
            }
        }
    }
}
