﻿using CoralBusinessLogics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coral.Prod.Managment.ProfoctionFormat.genral
{
    public static class InitialProdoction
    {
        public static int SetCompany(string companyName)
        {
            int companyID;
            using (var dbCtx = new Coral_DB_Entities())
            {
                try
                {
                    CompaniesLogic col = new CompaniesLogic();
                    col.InsertCompany(companyName);
                }
                catch
                {

                }


                var companyInContext = dbCtx.Companies.FirstOrDefault(c => c.CompanyName == companyName);
                companyID = companyInContext.CompanyID;

                return companyID;
            }

        }

        public static int GetLifeCoverageTypeID(int lifeIndustryID, string covrageTypeName, string covrageCode)
        {
            int lifeCoverageTypeID = 0;

            CoveragesLogic cl = new CoveragesLogic();

            using (var dbCtx = new Coral_DB_Entities())
            {
                try
                {
                    cl.InsertLifeCoverageType(lifeIndustryID, covrageCode, covrageTypeName, true);
                }
                catch
                {

                }

                lifeCoverageTypeID = dbCtx.LifeCoverageTypes.FirstOrDefault(ct => ct.LifeCoverageTypeName == covrageTypeName && ct.LifeCoverageCode == covrageCode && ct.LifeIndustryID == lifeIndustryID).LifeCoverageTypeID;
            }
            return lifeCoverageTypeID;
        }

        public static int GetLifeCovDef(int LifeIndustryID)
        {
            return InitialProdoction.GetLifeCoverageTypeID(LifeIndustryID, "כללי", "000");
        }

        public static int GetElementryCoverageTypeID(int industryID, string covrageTypeName, string covrageCode)
        {
            int elementaryCoverageTypeID = 0;

            CoveragesLogic cl = new CoveragesLogic();

            using (var dbCtx = new Coral_DB_Entities())
            {
                try
                {
                    cl.InsertElementaryCoverageType(industryID, covrageCode, covrageTypeName, true);
                }
                catch
                {

                }

                elementaryCoverageTypeID = dbCtx.ElementaryCoverageTypes.FirstOrDefault().ElementaryCoverageTypeID;
            }
            return elementaryCoverageTypeID;
        }


        public static int GetFinanceProgram(string ProgramName, int ProgramTypeId, int comapnyID)
        {
            int financeProgramID = 0;
            FinanceIndustryLogic fl = new FinanceIndustryLogic();



            using (var dbCtx = new Coral_DB_Entities())
            {
                try
                {
                    fl.InsertFinanceProgram(ProgramName, ProgramTypeId, comapnyID, true);
                }
                catch
                {

                }

                financeProgramID = dbCtx.FinancePrograms.FirstOrDefault().ProgramID;
            }
            return financeProgramID;
        }


        public static int GetElementaryInsuranceTypeID(int industryID, string name)
        {
            int elementaryInsuranceTypeID = 0;

            IndustriesLogic cl = new IndustriesLogic();

            using (var dbCtx = new Coral_DB_Entities())
            {
                try
                {
                    cl.InsertInsuranceType(name, industryID, true);
                }
                catch
                {

                }

                elementaryInsuranceTypeID = dbCtx.ElementaryInsuranceTypes.FirstOrDefault(l => l.ElementaryInsuranceTypeName == name&&l.InsuranceIndustryID==industryID).ElementaryInsuranceTypeID;
            }
            return elementaryInsuranceTypeID;
        }


        public static int GetLifeIndustryID(string lifeIndustryName)
        {
            int insurnceID;
            using (var dbCtx = new Coral_DB_Entities())
            {
                var lifeIndustryContext = dbCtx.LifeIndustries.FirstOrDefault(i => i.LifeIndustryName == lifeIndustryName);
                insurnceID = lifeIndustryContext.LifeIndustryID;
            }
            return insurnceID;
        }

        public static int GetElementryIndustryID(string industryName)
        {

            int insurnceID;
            using (var dbCtx = new Coral_DB_Entities())
            {
                var industryContext = dbCtx.InsuranceIndustries.FirstOrDefault(i => i.InsuranceIndustryName == industryName);
                insurnceID = industryContext.InsuranceIndustryID;
            }
            return insurnceID;
        }

        public static int GetAgentID()
        {
            int agentID;
            AgentsLogic d = new AgentsLogic();
            try
            {

                d.InsertAgent(true, null, "Elkayam", "akiva", "043194877", DateTime.Today, "aa", "ss", "039655838", "3", string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, true);

            }
            catch
            {

            }

            using (var dbCtx = new Coral_DB_Entities())
            {
                agentID = dbCtx.Agents.FirstOrDefault().AgentID;
            }
            return agentID;
        }

        public static int GetCategoryClientID(string clientCategory)
        {
            int categoryClientID;
            ClientsLogic cl = new ClientsLogic();
            try
            {
                cl.InsertCategory(clientCategory, true);
            }
            catch
            {

            }
            using (var dbCtx = new Coral_DB_Entities())
            {
                categoryClientID = dbCtx.Categories.FirstOrDefault().CategoryID;
            }

            return categoryClientID;
        }

        public static FundType AddFundType(string fundName, int companyID, int lifeIndustryID, int lnsuranceID)
        {
            FundsLogic fundsLogic = new FundsLogic();


            try
            {
                fundsLogic.InsertFundType(lnsuranceID, lifeIndustryID, companyID, fundName, true);
            }
            catch
            {

            }

            using (var dbCtx = new Coral_DB_Entities())
            {
                FundType fundItem = dbCtx.FundTypes.FirstOrDefault(i => i.FundTypeName == fundName);
                return fundItem;

            }
        }

        public static int GetInsurnceID(string insuranceName, int companyID)
        {
            int insurnceID;
            using (var dbCtx = new Coral_DB_Entities())
            {
                var insuranceInContext = dbCtx.Insurances.FirstOrDefault(i => i.InsuranceName == insuranceName);
                insurnceID = insuranceInContext.InsuranceID;

                var item = dbCtx.InsuranceCompanies.FirstOrDefault(i => (i.CompanyID == companyID && i.InsuranceID == insurnceID));
                if (item == null)
                {
                    InsuranceCompany ic = new InsuranceCompany();
                    ic.CompanyID = companyID;
                    ic.InsuranceID = insurnceID;
                    ic.Status = true;
                    dbCtx.InsuranceCompanies.Add(ic);
                    dbCtx.SaveChanges();
                }
            }
            return insurnceID;
        }
        public static int GetClientTypeID(string clientTypeName)
        {
            int clientTypeID;

            using (var dbCtx = new Coral_DB_Entities())
            {
                var clientTypeContext = dbCtx.ClientTypes.FirstOrDefault(i => i.ClientTypeName == clientTypeName);
                clientTypeID = clientTypeContext.ClientTypeID;
            }

            return clientTypeID;
        }

        public static int GetHealthCoverageTypeID()
        {
            int healthCoverageTypeID = 0;

            CoveragesLogic cl = new CoveragesLogic();

            using (var dbCtx = new Coral_DB_Entities())
            {

                try
                {
                    cl.InsertHealthCoverageType("01", "כיסוי בסיסי", true);
                }
                catch
                {

                }

                healthCoverageTypeID = dbCtx.HealthCoverageTypes.FirstOrDefault().HealthCoverageTypeID;
            }
            return healthCoverageTypeID;
        }

    }
}
