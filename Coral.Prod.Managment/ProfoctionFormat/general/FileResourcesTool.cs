﻿using CoralBusinessLogics;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Coral.Prod.Managment.ProfoctionFormat.general
{
    public static class FileResourcesTool
    {

        public static string SaveFileAsUTF(string fileName, Encoding encoding)
        {

            string newFileName = Path.GetTempFileName();
            if(File.Exists(newFileName))
            {
                File.Delete(newFileName);

            }

            StreamReader reader = new StreamReader(fileName, Encoding.GetEncoding(862));

            using (var writer = new StreamWriter(newFileName, true, Encoding.GetEncoding(862)))
            {
                var str = reader.ReadToEnd();
                var encodeHebrew = encode(str, encoding);
                writer.Write(encodeHebrew);

            }

            reader.Close();

            return newFileName;
        }
        
        private static string encode(string str, Encoding encoding)
        {
            Encoding defaultEncoding = Encoding.GetEncoding(862);
            byte[] bytes = defaultEncoding.GetBytes(str);
            Encoding encoding2 = encoding;
            string hebrewString2 = encoding2.GetString(bytes);
            return hebrewString2;
        }

        public static string ExtractEmbededResource(string companyName,string type, string extName)
        {
            string preFix = string.Format("Coral.Prod.Managment.ProdoctionCompany.Company.{0}.{1}.tables.table{2}.json", companyName,type, extName.ToLower());
            string talbeFileName = Path.GetTempFileName();

            if (File.Exists(talbeFileName))
            {
                File.Delete(talbeFileName);

            }

            var stream = Assembly.GetExecutingAssembly().GetManifestResourceStream(preFix);
            if (stream == null) return string.Empty;

            StreamReader reader = new StreamReader(stream);

            using (var writer = new StreamWriter(talbeFileName, false, Encoding.UTF8))
            {
                writer.Write(reader.ReadToEnd());

            }

            reader.Close();

            return talbeFileName;
        }



    }
}
