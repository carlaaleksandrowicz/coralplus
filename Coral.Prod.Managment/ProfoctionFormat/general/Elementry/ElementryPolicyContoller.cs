﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CoralBusinessLogics;
using Newtonsoft.Json.Linq;
using System.Globalization;
using System.IO;

namespace Coral.Prod.Managment.ProfoctionFormat.general.Elementry
{

    public class ElementryPolicyContoller
    {
        private Dictionary<string, List<ElementaryCoverage>> _elementryCoverageDic = new Dictionary<string, List<ElementaryCoverage>>();
        private JArray clients = new JArray();
        private bool isMenoraProd;
        private string policyNumMenoraCasco = "";

        //   private Dictionary<int, LifeInsuranceType> _lifeInsuranceTypeDic = new Dictionary<int, LifeInsuranceType>();

        public ElementryPolicyContoller(Dictionary<string, List<ElementaryCoverage>> elementryCoverageDic, JArray clients, bool isMenoraProduction)
        {
            _elementryCoverageDic = elementryCoverageDic;
            this.clients = clients;
            isMenoraProd = isMenoraProduction;
        }

        public void AddElementryPolicyCollection(JArray collection)
        {
            foreach (var itemPol in collection)
            {
                //policyNumMenoraCasco = "";
                if (isMenoraProd)
                {
                    string policyNum = itemPol["ElementaryPolicyID"].ToString();
                    if (policyNum.Substring(0, 4) == "0073")
                    {
                        var policy = collection.FirstOrDefault(p => (p["ElementaryPolicyID"].ToString()).Substring(4) == policyNum.Substring(4) && (p["ElementaryPolicyID"].ToString()).Substring(0, 4) != "0073");
                        if (policy != null)
                        {
                            policyNumMenoraCasco = policy["ElementaryPolicyID"].ToString();
                        }
                        else
                        {
                            if (itemPol["Addition"].ToString() != "0")
                            {
                                string policyNumberFromDb = GetPolicyNumberFromDb(policyNum.Substring(4));

                                if (policyNumberFromDb != null)
                                {
                                    policyNumMenoraCasco = policyNumberFromDb;
                                }
                            }
                        }
                    }
                }
                AddElementryPolicy(itemPol);
            }
        }

        private string GetPolicyNumberFromDb(string policyNumWithoutIndustry)
        {
            using (var dbCtx = new Coral_DB_Entities())
            {
                var policy = dbCtx.ElementaryPolicies.FirstOrDefault(p => p.PolicyNumber.Substring(4) == policyNumWithoutIndustry);
                if (policy != null)
                {
                    return policy.PolicyNumber;
                }
                else
                {
                    return null;
                }
            }
        }

        private void AddElementryPolicy(JToken itemPol)
        {
            ElementaryPolicy elementaryPolicy = new ElementaryPolicy();
            List<string> ls = new List<string>();
            #region import all data
            foreach (var x in itemPol)
            {
                var key = ((JProperty)(x)).Name;
                var property = typeof(ElementaryPolicy).GetProperty(key);
                try
                {
                    if (property == null)
                    {
                        ls.Add(key);
                        continue;
                    }

                    else if (key == "StartDate" || key == "EndDate" || key == "PaymentDate")
                    {
                        property.SetValue(elementaryPolicy, ParseStartDate(itemPol.Value<string>(key), (key == "StartDate")));
                    }

                    else if (key == "PaymentMethod")
                    {
                        property.SetValue(elementaryPolicy, ParsePaymentMethod(itemPol.Value<int>(key)));
                    }

                    else if (key == "Currency" || key == "BankName" || key == "BankBranchNumber")
                    {
                        property.SetValue(elementaryPolicy, itemPol.Value<int>(key).ToString());
                    }

                    else if (key == "Credit" || key == "PremiumNeto1" || key == "TotalFees" || key == "PremiumNeto4" ||
                        key == "TotalPremium" || key == "InscriptionFees" || key == "ProjectionFees" ||
                         key == "PolicyFees" || key == "StampsFees" || key == "HandlingFees")
                    {
                        property.SetValue(elementaryPolicy, (decimal)itemPol.Value<Double>(key));
                    }

                    else
                    {
                        ObjectUtillity.UpdateAccordingToType<ElementaryPolicy>(itemPol, elementaryPolicy, x, key, property);
                    }

                }
                catch
                {
                    ls.Add(key);
                    continue;
                }

            }
            #endregion

            #region set all refernce value
            elementaryPolicy.OpenDate = DateTime.Now;
            elementaryPolicy.PrincipalAgentNumberID = null;
            elementaryPolicy.SubAgentNumberID = null;
            elementaryPolicy.PaymentDate = null;
            elementaryPolicy.InsuranceIndustryID = (int)itemPol["InsuranceIndustryID"];
            elementaryPolicy.RenewalStatusID = (new RenewalsLogic()).GetRenewalRenewalStatusId("לא טופל");
            //    elementaryPolicy.EndDate = null;

            if (itemPol["car"] != null)
            {
                elementaryPolicy.CarPolicy = AddCarPolicy(itemPol["car"]);
            }

            if (itemPol["ApartmentPolicy"] != null)
            {
                elementaryPolicy.ApartmentPolicy = AddApartmentPolicy(itemPol["ApartmentPolicy"]);
            }

            if (itemPol["BusinessPolicy"] != null)
            {
                elementaryPolicy.BusinessPolicy = AddBusinessPolicy(itemPol["BusinessPolicy"]);
            }

            elementaryPolicy.PolicyNumber = itemPol["ElementaryPolicyID"].ToString();
            if (_elementryCoverageDic.ContainsKey(itemPol["ElementaryPolicyID"].ToString()))
            {
                elementaryPolicy.ElementaryCoverages = _elementryCoverageDic[itemPol["ElementaryPolicyID"].ToString()];

                foreach (var item in elementaryPolicy.ElementaryCoverages.Where(c=>c.ElementaryCoverageID != 0||c.ElementaryPolicyID != 0))
                {
                    item.ElementaryCoverageID = 0;
                    item.ElementaryPolicyID = 0;
                    item.ElementaryPolicy = null;
                }
            }
            elementaryPolicy.ElementaryPolicyID = 0;
            #endregion

            #region set all null value
            ObjectUtillity.makeStringEmptyAllNullValue(elementaryPolicy);
            #endregion

            #region saveData 
            using (var dbCtx = new Coral_DB_Entities())
            {
                elementaryPolicy.PolicyNumber = elementaryPolicy.PolicyNumber.TrimStart('0');
                if (policyNumMenoraCasco != "")
                {
                    elementaryPolicy.PolicyNumber = policyNumMenoraCasco.TrimStart('0');
                    policyNumMenoraCasco = "";
                }                
                var policyInContext = dbCtx.ElementaryPolicies.FirstOrDefault(c => c.PolicyNumber == elementaryPolicy.PolicyNumber && c.Addition == elementaryPolicy.Addition && c.InsuranceIndustryID == elementaryPolicy.InsuranceIndustryID);
                if (policyInContext == null)
                {
                    dbCtx.ElementaryPolicies.Add(elementaryPolicy);
                    dbCtx.SaveChanges();
                    ElementaryPolicy policyInCtx = dbCtx.ElementaryPolicies.Include("InsuranceIndustry").Include("Client").FirstOrDefault(p => p.ElementaryPolicyID == elementaryPolicy.ElementaryPolicyID);
                    string city = "";
                    string street = "";
                    string homeNum = "";
                    foreach (var client in clients)
                    {
                        if (client.SelectToken("IdNumber").ToString() == policyInCtx.Client.IdNumber)
                        {
                            homeNum = client.SelectToken("HomeNumber").ToString();
                            city = client.SelectToken("City").ToString();
                            street = client.SelectToken("Street").ToString();
                            break;
                        }
                    }
                    AddAddressToPolicy(city, street, homeNum, policyInCtx);
                    AddPolicyFolder(policyInCtx);
                }
            }
            #endregion

        }

        private void AddAddressToPolicy(string city, string street, string homeNumber, ElementaryPolicy policy)
        {
            if (policy.InsuranceIndustry.InsuranceIndustryName == "דירה")
            {
                using (var dbCtx = new Coral_DB_Entities())
                {
                    var aptPolicyInContext = dbCtx.ApartmentPolicies.FirstOrDefault(p => p.ElementaryPolicyID == policy.ElementaryPolicyID);
                    if (aptPolicyInContext == null)
                    {
                        ApartmentPolicy newAptPolicy = new ApartmentPolicy() { City = city, Street = street, HomeNumber = homeNumber };
                        dbCtx.ApartmentPolicies.Add(newAptPolicy);
                    }
                    else
                    {
                        aptPolicyInContext.City = city;
                        aptPolicyInContext.Street = street;
                        aptPolicyInContext.HomeNumber = homeNumber;
                    }
                    dbCtx.SaveChanges();
                }
            }
            else if (policy.InsuranceIndustry.InsuranceIndustryName == "עסק")
            {
                using (var dbCtx = new Coral_DB_Entities())
                {
                    var businessPolicyInContext = dbCtx.BusinessPolicies.FirstOrDefault(p => p.ElementaryPolicyID == policy.ElementaryPolicyID);
                    if (businessPolicyInContext == null)
                    {
                        BusinessPolicy newBusinessPolicy = new BusinessPolicy() { City = city, Street = street, HomeNumber = homeNumber };
                        dbCtx.BusinessPolicies.Add(newBusinessPolicy);
                    }
                    else
                    {
                        businessPolicyInContext.City = city;
                        businessPolicyInContext.Street = street;
                        businessPolicyInContext.HomeNumber = homeNumber;
                    }
                    dbCtx.SaveChanges();
                }
            }
        }

        private void AddPolicyFolder(ElementaryPolicy elementaryPolicy)
        {
            string strAppDir = System.AppDomain.CurrentDomain.BaseDirectory;
            string path = File.ReadAllText(strAppDir + @"Coral Files\ClientsPath.txt");
            if (Directory.Exists(path))
            {
                string clientPath = path + @"\" + elementaryPolicy.ClientID;
                if (!Directory.Exists(clientPath))
                {
                    Directory.CreateDirectory(clientPath);
                }
                string policyPath;
                if (elementaryPolicy.InsuranceIndustry.InsuranceIndustryName.Contains("רכב"))
                {
                    policyPath = clientPath + @"\אלמנטרי\רכב\" + elementaryPolicy.InsuranceIndustry.InsuranceIndustryName + " " + elementaryPolicy.PolicyNumber;
                }
                else if (elementaryPolicy.InsuranceIndustry.InsuranceIndustryName == "דירה")
                {
                    policyPath = clientPath + @"\אלמנטרי\דירה\" + elementaryPolicy.InsuranceIndustry.InsuranceIndustryName + " " + elementaryPolicy.PolicyNumber;
                }
                else if (elementaryPolicy.InsuranceIndustry.InsuranceIndustryName == "עסק")
                {
                    policyPath = clientPath + @"\אלמנטרי\עסק\" + elementaryPolicy.InsuranceIndustry.InsuranceIndustryName + " " + elementaryPolicy.PolicyNumber;
                }
                else
                {
                    policyPath = clientPath + @"\כללי\" + elementaryPolicy.InsuranceIndustry.InsuranceIndustryName + " " + elementaryPolicy.PolicyNumber;
                }
                if (!Directory.Exists(@policyPath))
                {
                    Directory.CreateDirectory(@policyPath);
                }
            }
        }


        private CarPolicy AddCarPolicy(JToken carPolObject)
        {
            List<string> ls = new List<string>();
            CarPolicy newcarPol = new CarPolicy();
            #region import all data
            foreach (var x in carPolObject)
            {
                var key = ((JProperty)(x)).Name;
                var property = typeof(CarPolicy).GetProperty(key);
                try
                {
                    if (property == null)
                    {
                        ls.Add(key);
                        continue;
                    }
                    else if (key == "Weight" || key == "PremiumNeto1" || key == "TotalFees" || key == "TotalPremium")
                    {
                        property.SetValue(newcarPol, (decimal)carPolObject.Value<Double>(key));
                    }
                    else if (key == "RegistrationNumber" || key == "Engine" || key == "ModelCode")
                    {
                        property.SetValue(newcarPol, carPolObject.Value<int>(key).ToString());
                    }
                    else
                    {
                        ObjectUtillity.UpdateAccordingToType<CarPolicy>(((JObject)carPolObject), newcarPol, x, key, property);
                    }

                }
                catch
                {
                    ls.Add(key);
                    continue;
                }

            }
            ObjectUtillity.makeStringEmptyAllNullValue(newcarPol);
            return newcarPol;
            #endregion
        }





        private BusinessPolicy AddBusinessPolicy(JToken businessPolicyObj)
        {
            List<string> ls = new List<string>();
            BusinessPolicy newbusinessPolicyObj = new BusinessPolicy();
            #region import all data
            foreach (var x in businessPolicyObj)
            {
                var key = ((JProperty)(x)).Name;
                var property = typeof(BusinessPolicy).GetProperty(key);
                try
                {
                    if (property == null)
                    {
                        ls.Add(key);
                        continue;
                    }
                    else
                    {
                        ObjectUtillity.UpdateAccordingToType<BusinessPolicy>(businessPolicyObj, newbusinessPolicyObj, x, key, property);
                    }

                }
                catch
                {
                    ls.Add(key);
                    continue;
                }

            }
            ObjectUtillity.makeStringEmptyAllNullValue(newbusinessPolicyObj);
            return newbusinessPolicyObj;
            #endregion
        }
        private ApartmentPolicy AddApartmentPolicy(JToken apartmentPolicyObj)
        {
            List<string> ls = new List<string>();
            ApartmentPolicy newApartmentPolicy = new ApartmentPolicy();

            #region import all data
            foreach (var x in apartmentPolicyObj)
            {
                var key = ((JProperty)(x)).Name;
                var property = typeof(ApartmentPolicy).GetProperty(key);
                try
                {
                    if (property == null)
                    {
                        ls.Add(key);
                        continue;
                    }

                    else
                    {
                        ObjectUtillity.UpdateAccordingToType<ApartmentPolicy>(apartmentPolicyObj, newApartmentPolicy, x, key, property);
                    }

                }
                catch
                {
                    ls.Add(key);
                    continue;
                }

            }
            ObjectUtillity.makeStringEmptyAllNullValue(newApartmentPolicy);
            return newApartmentPolicy;
            #endregion
        }


        private DateTime ParseStartDate(string val, bool isStartDate = true)
        {
            DateTime ret = new DateTime();
            try

            {
                var dateStr = val.ToString().PadLeft(4, '0');

                ret = DateTime.ParseExact(string.Format("20{0}", dateStr), "yyyyMMdd", CultureInfo.InvariantCulture);

                if (isStartDate && ret > DateTime.Today)
                {
                    ret = DateTime.ParseExact(string.Format("19{0}", dateStr), "yyyyMMdd", CultureInfo.InvariantCulture);
                }

            }
            catch
            {

            }
            return ret;


        }



        private string ParsePaymentMethod(int val)
        {
            string ret = string.Empty;
            if (val == 1)
            {
                ret = "מזומן";
            }
            else if (val == 2)
            {
                ret = "תשלומים";
            }
            return ret;
        }
    }

}
