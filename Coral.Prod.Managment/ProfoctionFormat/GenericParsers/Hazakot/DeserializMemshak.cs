﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Coral.Prod.Managment.ProfoctionFormat.GenericParsers.Hazakot
{
    public static class DeserializMemshak
    {
        static public Mimshak Deserialize(string file)
        {
            Stream streamResource;
            Mimshak mimshak = null;
            try
            {
                streamResource = new FileStream(file, FileMode.Open);


                if (streamResource != null)
                {
                    var xSerializer = new XmlSerializer(typeof(Mimshak));
                    mimshak = (Mimshak)xSerializer.Deserialize(streamResource);
                }

                streamResource.Close();

            }
            catch (Exception ex)
            {

            }
            return mimshak;
        }
    }
}
