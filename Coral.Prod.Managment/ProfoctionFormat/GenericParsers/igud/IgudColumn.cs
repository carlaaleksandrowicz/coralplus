﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Coral.Prod.Managment.ProfoctionFormat.GenericParsers.igud
{
    public enum FieldType { integerType, stringType, floatType, dateTimeType };

    public class IgudColumn
    {

        private const string patternDecimalMutiChar = @"[9]\(\d.*\)";
        private const string patternDecimalMutiCharwithDot = @"[9]\(\d.*\)V[9]\(\d.*\)";
        private const string patternDecimalSinglechar = "9";
        private const string patternDecimaldual = "99";
        private const string patternDecimalThreeniar = "999";
        private const string patternDate = "YYMMDD";
        private const string patternStrSingle = "X";
        private const string patternStrMulti = @"[X]\(\d.*\)";
        private const string patternMulti = @"\(\d.*\)";



        public string fieldName;
        public string typeL;
        public string refField;

        private FieldType _type;
        private int _length;
        private int _dotLocation;




        public void parseTypeAndLength()
        {
            if (string.CompareOrdinal(patternDecimalSinglechar, typeL) == 0)
            {
                _type = FieldType.integerType;
                _length = 1;
            }

            else if (string.CompareOrdinal(patternDecimaldual, typeL) == 0)
            {
                _type = FieldType.integerType;
                _length = 2;
            }

            else if (string.CompareOrdinal(patternStrSingle, typeL) == 0)
            {
                _type = FieldType.stringType;
                _length = 1;
            }
            else if (string.CompareOrdinal(patternDecimalThreeniar, typeL) == 0)
            {
                _type = FieldType.integerType;
                _length = 3;
            }
            else if (string.CompareOrdinal(patternDate, typeL) == 0)
            {
                _type = FieldType.dateTimeType;
                _length = 6;
            }

            else if (Regex.IsMatch(typeL, patternDecimalMutiCharwithDot))
            {
                _type = FieldType.floatType;
                _length = getLength(typeL);
            }

            else if (Regex.IsMatch(typeL, patternDecimalMutiChar))
            {
                _type = FieldType.integerType;
                _length = getLength(typeL);
            }
            else if (Regex.IsMatch(typeL, patternStrMulti))
            {
                _type = FieldType.stringType;
                _length = getLength(typeL);
            }
        }


        private int getLength(string typeAndLength)
        {
            if (Regex.IsMatch(typeL, patternDecimalMutiCharwithDot))
            {
                var split = typeAndLength.Split('V');
                int ret = 0;
                foreach (var item in split)
                {
                    var regmatch1 = Regex.Match(item, patternMulti);
                    var valStr = regmatch1.Value.Replace("(", string.Empty).Replace(")", string.Empty);
                    ret = ret + int.Parse(valStr);
                    _dotLocation = int.Parse(valStr);

                }
                return ret;
            }

            var regmatch = Regex.Match(typeAndLength, patternMulti);
            var val = regmatch.Value.Replace("(", string.Empty).Replace(")", string.Empty);
            int retvalue = int.Parse(val);

            
            return retvalue;
        }


        public FieldType Type
        {
            get
            {
                return _type;

            }

        }


        public int DotLocation
        {
            get
            {
                return _dotLocation;
            }

        }

        public int Length
        {
            get
            {
                return _length;

            }

        }
    }
}
