﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Coral.Prod.Managment.ProfoctionFormat
{
    public class ApexRefactorUtil
    {
        public static void RefactorClients(JArray Clients)
        {
            foreach (var itemClient in Clients)
            {
                SplitFirstNameLastName(itemClient);

                RefactorStreetAndHomeNumber(itemClient);

            }

        }

        private static void RefactorStreetAndHomeNumber(JToken itemClient)
        {
            var street = itemClient["Street"].ToString();

            char[] digits = street.Where(c => Char.IsDigit(c) || c == '/').ToArray();
            char[] letters = street.Where(c => Char.IsLetter(c) || Char.IsWhiteSpace(c)).ToArray();

            itemClient["HomeNumber"] = new string(digits);
            itemClient["HomeNumber"] = Reverse(itemClient["HomeNumber"].ToString());
            itemClient["Street"] = new string(letters);

            //var split = street.Split(' ');
            //if (split.Length >= 2)
            //{
            //    itemClient["HomeNumber"] = TruncateLongString(split[split.Length - 1].Trim(), 10);
                

            //    itemClient["Street"] = street.Remove(street.Length - itemClient["HomeNumber"].ToString().Length, itemClient["HomeNumber"].ToString().Length).Trim();
            //}
        }

        public static string Reverse(string s)
        {
            char[] charArray = s.ToCharArray();
            Array.Reverse(charArray);
            return new string(charArray);
        }

        public static string TruncateLongString(string str, int maxLength)
        {
            return str.Substring(0, Math.Min(str.Length, maxLength));
        }


        private static void SplitFirstNameLastName(JToken itemClient)
        {
            var lastNameVar = itemClient["LastName"].ToString();
            var split = lastNameVar.Split(' ');

            if (split.Length >= 2)
            {
                itemClient["FirstName"] = split[split.Length - 1].Trim();
                itemClient["LastName"] = lastNameVar.Remove(lastNameVar.Length - itemClient["FirstName"].ToString().Length, itemClient["FirstName"].ToString().Length).Trim();
            }
        }
    }
}
