﻿using Coral.Prod.Managment.ProfoctionFormat.ProdoctionParsers;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coral.Prod.Managment.ProfoctionFormat
{
    public abstract class ProdoctionFormatBase
    {
        #region Members
        protected string _rootDir;
        protected string _insurenseName;
        protected DirectoryInfo[] _prodoctionDirectories;
        protected string _prodoctionFormat;
        protected int _agentID;
        protected IProdoctionParser _prodoctionParser;
        protected bool _isValid = true;


        #endregion

        #region c'tor
        public ProdoctionFormatBase(string rootDirectory, string insurenseName)
        {
            _rootDir = rootDirectory;
            _insurenseName = insurenseName;

        }
        #endregion


        public void ConvertToDB(int companyID)
        {
            _prodoctionParser.ConvertToDB(companyID);
        }
        public void SetAgentID(int agentID)
        {
            _prodoctionParser.SetAgentID(agentID);
        }

        public bool IsValid()
        {
            return _isValid;
        }

        public bool IsAgentDetected(int companyID, int insurenceID)
        {
            return _prodoctionParser.IsAgentDetected(companyID, insurenceID);
        }
        protected abstract IProdoctionParser ProdoctionParserFactory(string name);




    }
}
