﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coral.Prod.Managment.Utils
{
    public static class Dicrctory
    {
        public static DirectoryInfo GetLeafDir(string rootDir)
        {
            DirectoryInfo di = new DirectoryInfo(rootDir);
            if (di.GetDirectories().Count() == 0)
            {
                return di;
            }

            else
            {
                foreach (var dirItem in di.GetDirectories())
                {
                    return GetLeafDir(dirItem.FullName);

                }
            }
            return di;

        }
    }
}
