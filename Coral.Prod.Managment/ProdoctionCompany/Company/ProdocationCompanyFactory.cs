﻿using Coral.Prod.Managment.ProdoctionCompany.Company;
using Coral.Prod.Managment.ProfoctionFormat;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coral.Prod.Managment.ProdoctionCompany
{
    public static class ProdocationCompanyFactory
    {
        public static ProdoctionCompanyBase Factory(string companyName, string fileName,string insurenseName)
        {
            switch (companyName)
            {
                case "כלל":
                    return new ProdoctionCompanyClal(fileName, insurenseName);

                case "מגדל":
                    return new ProdoctionCompanyMigdal(fileName, insurenseName);

                case "שומרה":
                    return new ProdoctionCompanyShomera(fileName, insurenseName);
                case "מנורה":
                    return new ProdoctionCompanyMenora(fileName, insurenseName);
                case "פניקס":
                    return new ProdoctionCompanyFenix(fileName, insurenseName);
                case "הכשרה":
                    return new ProdoctionCompanyHachshara(fileName, insurenseName);
                case "שלמה":
                    return new ProdoctionCompanyShlomo(fileName, insurenseName);
                case "הראל":
                    return new ProdoctionCompanyHarel(fileName, insurenseName);
                default:
                    return null;

            }
        }
    }
}


