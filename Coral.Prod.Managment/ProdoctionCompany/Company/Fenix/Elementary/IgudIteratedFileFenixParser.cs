﻿using Coral.Prod.Managment.ProfoctionFormat.general;
using Coral.Prod.Managment.ProfoctionFormat.GenericParsers.igud;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coral.Prod.Managment.ProfoctionFormat.GenericParsers.Igud
{
    public class IgudIteratedFileFenixParser
    {
        #region members
        private string _tableFile = string.Empty;
        private string _productionFile = string.Empty;

        public JArray ResultSet = new JArray();


        #endregion

        public IgudIteratedFileFenixParser(string productionFile, bool reverse)
        {
            _productionFile = productionFile;
            parseData(reverse);
        }

        private static List<IgudColumn> readTableFile(string tableFile)
        {
            List<IgudColumn> tableFields;
            using (StreamReader r = new StreamReader(tableFile))
            {
                string json = r.ReadToEnd();
                tableFields = JsonConvert.DeserializeObject<List<IgudColumn>>(json);
                foreach (var item in tableFields)
                {
                    item.parseTypeAndLength();
                }
            }
            return tableFields;
        }

        private void parseData(bool reverse)
        {
            string line;

            // Read the file and display it line by line.
            System.IO.StreamReader file =
               new System.IO.StreamReader(_productionFile, Encoding.GetEncoding(862), true);

            string recordPrefix = string.Empty;
            JObject obj = null;
            while ((line = file.ReadLine()) != null)
            {
                var valuePrefix = line.Substring(0, 35);
                if (valuePrefix != recordPrefix)
                {
                    if (obj != null)
                    {
                        ResultSet.Add(obj);
                    }
                    recordPrefix = valuePrefix;
                    obj = new JObject();
                    obj.Add("prefix", new JObject());
                    ReadRecord(0, line, (JObject)obj["prefix"], ".prefix", reverse);
                }

                parseLine(obj, line, reverse);
            }

            file.Close();

        }


        private void parseLine(JObject obj, string str, bool reverse)
        {
            try
            {
                var valueRecordType = str.Substring(35, 2);
                if (obj[valueRecordType] == null)
                {
                    JArray jarr = new JArray();
                    obj.Add(valueRecordType, jarr);
                }
                JObject newObj = new JObject();
                ((JArray)obj[valueRecordType]).Add(newObj);

                ReadRecord(0, str, newObj, "." + valueRecordType, reverse);
            }
            catch(Exception ex)
            {


            }
        }


        private static void ReadRecord(int charIndex, string str, JObject obj, string RecordType, bool reverse)
        {
            string tableName = FileResourcesTool.ExtractEmbededResource("Fenix", "Elementary", RecordType);
            List<IgudColumn> tableFields = readTableFile(tableName);
            if (File.Exists(tableName))
            {
                File.Delete(tableName);
            }

            if (RecordType == ".30" || RecordType == ".35")
            {
                charIndex = 37;

                int recordLength = 13;

                // var value = str.Substring(38, recordLength);
                while (charIndex + recordLength < str.Length)
                {
                    JObject recordObj = new JObject();
                    foreach (var field in tableFields)
                    {
                        var value = str.Substring(charIndex, field.Length);

                        if (field.Type == FieldType.integerType)
                        {
                            int val;
                            int.TryParse(value.Trim(), out val);

                            recordObj.Add(field.refField, val);
                        }
                        else if (field.Type == FieldType.floatType)
                        {
                            var doubleval = Double.Parse(value.Insert(value.Length - field.DotLocation, "."));
                            recordObj.Add(field.refField, doubleval);
                        }

                        charIndex = charIndex + field.Length;
                    }
                    obj.Add(recordObj["CodeSum"].ToString(), recordObj);
                }
                return;
            }

            StringBuilder sbComments = new StringBuilder();
            foreach (var field in tableFields)
            {
                var value = str.Substring(charIndex, field.Length);
                try
                {
                    if (!string.IsNullOrWhiteSpace(field.refField))
                    {
                        if (field.Type == FieldType.integerType)
                        {
                            int val;
                            int.TryParse(value.Trim(), out val);

                            obj.Add(field.refField, val);

                        }
                        else if (field.Type == FieldType.stringType)
                        {
                            obj.Add(field.refField, Reverse(value.Trim(), reverse));
                        }
                        else if (field.Type == FieldType.dateTimeType)
                        {
                            obj.Add(field.refField, value.Trim());
                            //  retDictionary.Add(field.fieldName, DateTime.ParseExact(value.Trim(), "YYMMDD", CultureInfo.InvariantCulture));
                        }
                        else if (field.Type == FieldType.floatType)
                        {
                            var doubleval = Double.Parse(value.Insert(value.Length + 1 - field.DotLocation, "."));
                            obj.Add(field.refField, doubleval);
                        }


                    }
                }
                catch(Exception ex)
                {


                }
                
                
                sbComments.AppendLine(string.Format("{0}:{1}", field.fieldName, value.ToString()));
                
                charIndex = charIndex + field.Length;
            }

            //if (obj["Comments"] == null)
            //{
            //    obj.Add("Comments", string.Empty);
            //}

           // obj["Comments"] = obj["Comments"] + "/n" + sbComments.ToString();


        }
        public static string Reverse(string s, bool reverse)
        {
            if (IsDigitsOnly(s)) return (s);
            if (!reverse) return (s);
            try
            {
                char[] charArray = s.ToCharArray();
                Array.Reverse(charArray);
                return new string(charArray);
            }
            catch (Exception ex)
            {

            }

            return s;
        }

        static bool IsDigitsOnly(string str)
        {
            foreach (char c in str)
            {
                if (c < '0' || c > '9')
                    return false;
            }

            return true;
        }
    }
}

