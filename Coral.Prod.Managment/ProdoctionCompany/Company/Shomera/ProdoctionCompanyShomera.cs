﻿using Coral.Prod.Managment.ProfoctionFormat;
using Coral.Prod.Managment.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coral.Prod.Managment.ProdoctionCompany.Company
{
    public class ProdoctionCompanyShomera : ProdoctionCompanyBase
    {

        public ProdoctionCompanyShomera(string file, string insurenseName)
        {
            this._prodoctionFormat = LoadFile(file, insurenseName);
        }



        protected override ProdoctionFormatBase LoadFile(string fileName, string insurenseName)
        {
            string destDir = Path.Combine(Path.GetTempPath(), Path.GetRandomFileName());
            Zip.ExtractFile(fileName, destDir);
            return new ProdoctionFormatShomera(destDir, insurenseName);
        }


    }
}
