﻿using Coral.Prod.Managment.ProfoctionFormat.general;
using Coral.Prod.Managment.ProfoctionFormat.general.Elementry;
using Coral.Prod.Managment.ProfoctionFormat.general.Life;
using Coral.Prod.Managment.ProfoctionFormat.GenericParsers;
using Coral.Prod.Managment.ProfoctionFormat.GenericParsers.Igud;
using Coral.Prod.Managment.ProfoctionFormat.genral;
using CoralBusinessLogics;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Coral.Prod.Managment.ProfoctionFormat.ProdoctionParsers
{
    public class Clal_IgudElementry : IProdoctionParser
    {
        private Dictionary<string, List<ElementaryCoverage>> _elementaryCoverageDic = new Dictionary<string, List<ElementaryCoverage>>();
        private Dictionary<int, FundType> _fundsDic = new Dictionary<int, FundType>();
        private Dictionary<string, Client> _clientDics = new Dictionary<string, Client>();
        private Dictionary<int, ElementaryInsuranceType> _elementaryInsuranceTypeDic = new Dictionary<int, ElementaryInsuranceType>();
        //  private Dictionary<string, decimal> _permuimSum = new Dictionary<string, decimal>();
        JObject _elmentryPolicyObject;
        int _agentId;
        void IProdoctionParser.ParseProdoction(string directoryName)
        {
            JObject ret = new JObject();

            DirectoryInfo di = new DirectoryInfo(directoryName);
            foreach (var fileItem in di.GetFiles())
            {

                string SaveFileAsUtf8 = FileResourcesTool.SaveFileAsUTF(fileItem.FullName, Encoding.GetEncoding(862));

                IgudIteratedFileParser fileParser = new IgudIteratedFileParser(SaveFileAsUtf8, true);
                ret.Add(fileItem.Extension, fileParser.ResultSet);
                File.Delete(SaveFileAsUtf8);
            }
            _elmentryPolicyObject = ret;
        }


        public void ConvertToDB(int companyId)
        {

            JObject elmentryPolicyObject = _elmentryPolicyObject;
            AgentsLogic al = new AgentsLogic();
            var agent = al.GetAllAgents().FirstOrDefault(a => a.AgentID == _agentId);
            bool isAgentNumberRecognized = false;
            string number = "";
            foreach (var item in elmentryPolicyObject)
            {
                number = item.Value.First.First.First["PrincipalAgentNumberID"].ToString();
                if (number != null && agent.AgentNumbers.FirstOrDefault(n => n.Number == number && n.CompanyID == companyId) != null)
                {
                    isAgentNumberRecognized = true;
                    break;
                }
            }
            if (isAgentNumberRecognized == false)
            {
                string message = "";
                if (number != "0" && number != "")
                {
                    message = string.Format("לא ניתן להמשיך בתהליך המשיכה\n מספר הסוכן {0}, המופיע בקובץ פרודוקציה, לא מתאים לחברה, לסוג הביטוח או לסוכן שהוזן", number);
                }
                else
                {
                    message = string.Format("לא ניתן להמשיך בתהליך המשיכה\n קובץ הפרודוקציה לא מתאים לפרמטרים שהוזנו");
                }
                throw new Exception(message);
            }
            int insurnceId = InitialProdoction.GetInsurnceID("אלמנטרי", companyId);
            int IndustryID = InitialProdoction.GetElementryIndustryID("רכב");
            int elementryCoverageTypeID = InitialProdoction.GetElementryCoverageTypeID(IndustryID,"כללי","001");
            int agentId = _agentId;
            int categoryClientID = InitialProdoction.GetCategoryClientID("לקוחות");
            int clientTypeID = InitialProdoction.GetClientTypeID("לקוחות");

            JArray clientsArr = new JArray();
            JArray elementaryCoverageArr = new JArray();

            listClients(elmentryPolicyObject, clientsArr);
            ApexRefactorUtil.RefactorClients(clientsArr);
            _clientDics = ClientController.AddClientCollection(clientsArr, agentId, categoryClientID, clientTypeID);

            listElementaryCoverage(elmentryPolicyObject, elementaryCoverageArr);
            JArray elementryPol_Arr = CreateElementryPolArray(elmentryPolicyObject, companyId, insurnceId);

            _elementaryCoverageDic = CoverageController.AddElementryCoverageCollection(elementaryCoverageArr, elementryCoverageTypeID);
            ElementryPolicyContoller elementryPolicyContoller = new ElementryPolicyContoller(_elementaryCoverageDic, clientsArr,false);
            elementryPolicyContoller.AddElementryPolicyCollection(elementryPol_Arr);

            string agentNumberSTR = elementryPol_Arr[0].Value<int>("PrincipalAgentNumberID").ToString();
            UpdateAgentIfNeeded(companyId, insurnceId, agentNumberSTR);
        }

        private static void listElementaryCoverage(JObject elmentryPolicyObject, JArray elementaryCoverageArr)
        {
            StringBuilder sb = new StringBuilder();
            JArray obj = (JArray)elmentryPolicyObject[".ZEL"];
            foreach (var item in obj)
            {
                JArray coverage_arr = item["20"] as JArray;
                if (coverage_arr != null && !string.IsNullOrWhiteSpace(item["prefix"]["ElementaryPolicyID"].ToString()))
                    foreach (JObject covitem in coverage_arr)
                    {
                        covitem.Add("ElementaryPolicyID", item["prefix"]["ElementaryPolicyID"].ToString());
                        covitem.Add("Addition", (int)item["prefix"]["Addition"]);
                        if (covitem["Comments"] != null && covitem["Comments"].ToString() != "0")
                        {
                            covitem["Comments"] = Regex.Replace(covitem["Comments"].ToString(), @"\d+", m => new string(m.Value.Reverse().ToArray()));

                            covitem["Comments"] = Regex.Replace(covitem["Comments"].ToString(), @"\w*\d\w*", m => DigitsRevrese(m.Value));

                            covitem["Comments"] = Regex.Replace(covitem["Comments"].ToString(), @"\d*\/\d*\/\d*", m => DateRevrese(m.Value));

                            covitem["Comments"] = Regex.Replace(covitem["Comments"].ToString(), @"\d*,\d*", m => BigNumber(m.Value));

                            

                            sb.AppendLine(covitem["Comments"].ToString());
                        }
                        elementaryCoverageArr.Add(covitem);
                    }
              //  File.AppendAllText(@"c:\old1.5.log", sb.ToString());
            }
        }

        private static string DateRevrese(string str)
        {
            try
            {
                DateTime a = DateTime.ParseExact(str, "yyyy/MM/dd", CultureInfo.InvariantCulture);

                return a.ToShortDateString();
            }
            catch
            {

                return str;
            }
        }

        private static string BigNumber(string str)
        {

            if(str.Length > 4)
            {
                string ret = string.Empty;
                var split = str.Split(',');

                for (int i = 0; i < split.Length; i++)
                {
                    ret = split[i] + "," + ret;
                }
                ret.Remove(split.Length - 1);

                return ret;
            }
            return str;
        }

        private static string DigitsRevrese(string str)
        {
            char[] charArray = str.ToCharArray();
            string word = string.Empty;
            string number = string.Empty;
            for (int i = 0; i < charArray.Length; i++)
            {
                if (charArray[i] < '0' || charArray[i] > '9' || charArray[i] == '\\' || charArray[i] == '/' || charArray[i] == ',')
                {
                    word += charArray[i];
                }
                else
                {
                    number += charArray[i];
                }
            }
            if(string.IsNullOrWhiteSpace(word))
            {
               // var s = new string(number.Reverse().ToArray());
                return number;
            }
            else if(string.IsNullOrWhiteSpace(number))
            {
                return word;
            }

            
           

            return string.Format("{1} {0}", number, word);
        }

      
        private static void listClients(JObject elmentryPolicyObject, JArray clientsArr)
        {
            JArray obj = (JArray)elmentryPolicyObject[".ZEL"];
            foreach (var item in obj)
            {
                JArray clients_arr = item["02"] as JArray;
                foreach (var clientitem in clients_arr)
                {
                    clientsArr.Add(clientitem);
                }

            }
        }


        private void SetPermuimSum(JArray permuimSum_arr, JObject polObject)
        {
            if (permuimSum_arr == null) return;
            JObject permuimSumitemObject = (JObject)permuimSum_arr.First();

            foreach (var innerData in permuimSumitemObject)
            {
                string codeSumStr = innerData.Key;
                var val = innerData.Value as JObject;

                int codeSum = val["CodeSum"].Value<int>();
                decimal totalSum = (decimal)val["TotalSum"].Value<Double>();
                int sign = (int)val["MarkSum"].Value<int>();
                if (sign == 0)
                {
                    sign = 1;
                }
                else
                {
                    sign = -1;
                }

                totalSum = sign * totalSum;

                
              //  polObject["Comments"] = polObject["Comments"] + string.Format("code:{0} , val :{1}", codeSum, totalSum);
                switch (codeSum)
                {
                    case 1:
                        polObject.Add("PremiumNeto1", totalSum);
                       
                        break;
                    case 2:
                        polObject.Add("PremiumNeto2", totalSum);
                        break;
                    case 3:
                        polObject.Add("PremiumNeto3", totalSum);
                        break;
                    case 4:
                        polObject.Add("InscriptionFees", totalSum);
                        break;
                    case 5:
                        polObject.Add("ProjectionFees", totalSum);
                        break;
                    case 6:
                        polObject.Add("StampsFees", totalSum);
                        break;
                    case 7:
                        polObject.Add("PolicyFees", totalSum);
                        break;
                    case 8:
                        polObject.Add("HandlingFees", totalSum);
                        break;
                    case 9:
                        polObject.Add("Credit", totalSum);
                        break;
                    case 20:
                        polObject.Add("PremiumNeto4", totalSum);
                        break;
                    case 15:
                        polObject.Add("TotalPremium", totalSum);
                        break;
                    default:
                        break;
                }
            }
        }

        private JArray CreateElementryPolArray(JObject elmentryPolicyObject, int companyId, int insurnceId)
        {
            JArray elementryPolArr = new JArray();
            JArray obj = (JArray)elmentryPolicyObject[".ZEL"];
            Dictionary<string[], JObject> elementryPolDic = new Dictionary<string[], JObject>();
            foreach (var item in obj)
            {
                JObject prefix_arr = item["prefix"] as JObject;
                string elementaryPolicyIDWithInd = prefix_arr["ElementaryPolicyID"].ToString();
                //string induestryIDtest = elementaryPolicyIDWithInd.Substring(0, 4);
                string induestryID = prefix_arr["IndustryID"].ToString();
                string elementaryPolicyID;
                if (elementaryPolicyIDWithInd.Count() > 9)
                {
                    elementaryPolicyID = elementaryPolicyIDWithInd.Remove(0, 4).TrimStart('0');
                    prefix_arr["ElementaryPolicyID"] = elementaryPolicyID;
                }
                else
                {
                    elementaryPolicyID = elementaryPolicyIDWithInd.TrimStart('0');                    
                }
                int elementaryIndustryID = getInduestryID(int.Parse(induestryID));
                int elementaryInsuranceTypeID = InitialProdoction.GetElementaryInsuranceTypeID(elementaryIndustryID, induestryID.ToString());

                string[] policyKey = new string[] { elementaryPolicyID, (item.First.First)["Addition"].ToString(), elementaryIndustryID.ToString() };
                if (!elementryPolDic.ContainsKey(policyKey))
                {
                    JObject polObject_base = new JObject();
                    JArray arr_07 = item["07"] as JArray;
                    JArray arr_30 = item["30"] as JArray;
                    JArray arr_35 = item["35"] as JArray;
                    JArray arr_01 = item["01"] as JArray;

                    JArray arr_10 = item["10"] as JArray;

                    JArray arr_02 = item["02"] as JArray;
                    string IdNumber = (arr_02.First() as JObject)["IdNumber"].ToString();
                    if (_clientDics.ContainsKey(IdNumber) || _clientDics.ContainsKey("0" + IdNumber))
                    {
                        polObject_base.Add("ClientID", _clientDics[IdNumber].ClientID);
                    }
                    polObject_base.Add("CompanyID", companyId);

                    polObject_base.Add("InsuranceIndustryID", elementaryIndustryID);
                    polObject_base.Add("InsuranceID", insurnceId);
                    polObject_base.Add("ElementaryInsuranceTypeID", elementaryInsuranceTypeID);

                    SetPermuimSum(arr_30, polObject_base);
                    SetPermuimSum(arr_35, polObject_base);


                    MergeObjectArr(polObject_base, arr_01);

                    if (arr_10 != null)
                    {
                        polObject_base.Add("car", arr_10.First());
                    }

                    if (elementaryIndustryID == 4)
                    {
                        polObject_base.Add("ApartmentPolicy", GetElementryPol());
                    }

                    if (elementaryIndustryID == 3)
                    {
                        polObject_base.Add("BusinessPolicy", GetBusinessPol());
                    }


                    MergeObject(polObject_base, prefix_arr);
                    MergeObjectArr(polObject_base, arr_07);



                    elementryPolDic.Add(policyKey, polObject_base);
                    elementryPolArr.Add(polObject_base);
                }
            }
            return elementryPolArr;
        }


        private static JObject GetElementryPol()
        {
            JObject ret = new JObject();
            ret.Add("City", "NA");
            ret.Add("Street", "NA");
            ret.Add("HomeNumber", "NA");
            ret.Add("ApartmentNumber", "NA");
            ret.Add("ZipCode", "NA");
            ret.Add("Floor", "NA");
            ret.Add("TotalBuildingFloorsNumber", "NA");

            return ret;
        }


        private static JObject GetBusinessPol()
        {
            JObject ret = new JObject();
            ret.Add("BusinessOwner", "NA");
            ret.Add("BusinessDescription", "NA");
            ret.Add("HomeNumber", "NA");
            ret.Add("BusinessName", "NA");
            ret.Add("City", "NA");
            ret.Add("Street", "NA");
            

            return ret;
        }





        private static JObject GetPol()
        {
            JObject ret = new JObject();
            ret.Add("City", "NA");
            ret.Add("Street", "NA");
            ret.Add("HomeNumber", "NA");
            ret.Add("ApartmentNumber", "NA");
            ret.Add("ZipCode", "NA");
            ret.Add("Floor", "NA");
            ret.Add("TotalBuildingFloorsNumber", "NA");

            return ret;
        }

        private static int getInduestryID(int id)
        {
            //רכב
            if (id == 16 || id == 31 || id == 19 || id == 17 || id == 22)
            {
                return 1;
            }
            // רכב חובה
            else if (id == 14)
            {
                return 2;
            }
            //דירה
            else if (id == 24 || id == 84)
            {
                return 4;
            }
            //עסק
            else if (id == 79 || id == 89 || id == 91 || id == 86 || id == 50)
            {
                return 3;
            }

            return 7;

        }

        private static void MergeObjectArr(JObject obj1, JArray obj2)
        {
            if (obj2 == null) return;
            foreach (var itemObj2 in obj2)
            {
                MergeObject(obj1, (JObject)itemObj2);

            }
        }

        private static void MergeObject(JObject obj1, JObject itemObj2)
        {
            if (itemObj2 == null) return;
            foreach (var innerData in itemObj2)
            {
                if (obj1[innerData.Key] == null)
                {
                    obj1.Add(innerData.Key, innerData.Value);
                }
            }
        }

        private void UpdateAgentIfNeeded(int companyId, int insurenceID, string agentNumberSTR)
        {

            if (!IsAgentDetected(companyId, insurenceID))
            {

                AgentCotroller.UpdateAgentIfNeeded(companyId, insurenceID, agentNumberSTR, _agentId);

            }
        }

        public bool IsAgentDetected(int companyId, int insurenceID)
        {
            JArray elementryPol_Arr = CreateElementryPolArray(_elmentryPolicyObject, companyId, insurenceID);

            bool agentFound = false;


            if (elementryPol_Arr.Count > 0)
            {
                string agentNumberSTR = elementryPol_Arr[0].Value<int>("PrincipalAgentNumberID").ToString();

                int agentIDFound;
                if (AgentCotroller.IsAgentDetected(companyId, insurenceID, agentNumberSTR, out agentIDFound))
                {
                    _agentId = agentIDFound;
                    return true;
                }
            }
            return agentFound;
        }

        public void SetAgentID(int agentID)
        {
            _agentId = agentID;
        }
    }
}
