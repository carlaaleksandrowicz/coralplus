﻿using Coral.Prod.Managment.ProfoctionFormat.general;
using Coral.Prod.Managment.ProfoctionFormat.general.Life;
using Coral.Prod.Managment.ProfoctionFormat.GenericParsers;
using Coral.Prod.Managment.ProfoctionFormat.genral;
using CoralBusinessLogics;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Coral.Prod.Managment.ProfoctionFormat.ProdoctionParsers
{
    public class ClalApexLife : IProdoctionParser
    {
        private Dictionary<string, List<LifeCoverage>> _lifeCoverageDic = new Dictionary<string, List<LifeCoverage>>();
        private Dictionary<string, PolicyOwner> _policyOwnerDic = new Dictionary<string, PolicyOwner>();
        private Dictionary<int, string> _fundsDic = new Dictionary<int, string>();
        private Dictionary<string, Client> _clientDics = new Dictionary<string, Client>();
        private Dictionary<int, int> _fund_induestryDic = new Dictionary<int, int>();
        //private Dictionary<int, LifeInsuranceType> _lifeInsuranceTypeDic = new Dictionary<int, LifeInsuranceType>();
        private JObject _induestryMaping;
        private JObject _lifePolicyObject;
        private int _agentId;
        Dictionary<int, JToken> _polObjectDic = new Dictionary<int, JToken>();



        void IProdoctionParser.ParseProdoction(string directoryName)
        {
            JObject ret = new JObject();

            DirectoryInfo di = new DirectoryInfo(directoryName);
            foreach (var fileItem in di.GetFiles())
            {
                string tableName = FileResourcesTool.ExtractEmbededResource("Clal", "LifeHealth.Life", fileItem.Extension);
                if (!string.IsNullOrWhiteSpace(tableName))
                {
                    string SaveFileAsUtf8 = FileResourcesTool.SaveFileAsUTF(fileItem.FullName, Encoding.GetEncoding(862));

                    ApexIteratedFileParser fileParser = new ApexIteratedFileParser(tableName, SaveFileAsUtf8, true);

                    ret.Add(fileItem.Extension, fileParser.ResultSet);

                    if (fileItem.Extension == ".POL")
                    {
                        ApexIteratedFileParser fileParserMiror = new ApexIteratedFileParser(tableName, SaveFileAsUtf8, true, true);

                        ret.Add(fileItem.Extension + "M", fileParserMiror.ResultSet);
                    }
                }
                else
                {
                    tableName = FileResourcesTool.ExtractEmbededResource("Clal", "LifeHealth.Life", fileItem.Extension + "1");
                    if (!string.IsNullOrWhiteSpace(tableName))
                    {
                        string SaveFileAsUtf8 = FileResourcesTool.SaveFileAsUTF(fileItem.FullName, Encoding.GetEncoding(862));

                        ApexIteratedFileParser fileParser = new ApexIteratedFileParser(tableName, SaveFileAsUtf8, true);
                        ret.Add(fileItem.Extension, fileParser.ResultSet);
                    }
                }

            }

            string tableNameSgp_induestry = FileResourcesTool.ExtractEmbededResource("Clal", "LifeHealth.Life", ".sgp_induestry");
            string tableNameSgp_induestryContent = File.ReadAllText(tableNameSgp_induestry);
            _induestryMaping = JObject.Parse(tableNameSgp_induestryContent);




            _lifePolicyObject = ret;
        }

        public void ConvertToDB(int companyId)
        {
            JObject lifePolicyObject = _lifePolicyObject;

            int insurnceId = InitialProdoction.GetInsurnceID("חיים", companyId);
            int lifeIndustryID = InitialProdoction.GetLifeIndustryID("פרט");

            int agentId = _agentId;
            int categoryClientID = InitialProdoction.GetCategoryClientID("לקוחות");
            int clientTypeID = InitialProdoction.GetClientTypeID("לקוחות");
            
            JArray polObject = (JArray)_lifePolicyObject[".POL"];
            JArray polObjectMirror = (JArray)_lifePolicyObject[".POLM"];
            Dictionary<int, JToken> polObjectMirrorDic = new Dictionary<int, JToken>();

            foreach (var item in polObjectMirror)
            {
                int polNumber = item.Value<int>("PolicyNumber");
                polObjectMirrorDic.Add(polNumber, item);

            }
            JArray clientsArr = (JArray)lifePolicyObject[".MEV"];
            JArray fundTypeArr = (JArray)lifePolicyObject[".SGB"];
            ApexRefactorUtil.RefactorClients(clientsArr);

            JArray covrageArr = (JArray)lifePolicyObject[".TAR"];

            JArray polictOwnerArr = (JArray)lifePolicyObject[".MAS"];

            RenderPolicyArr(polObject);
            _policyOwnerDic = PolicyOwnerController.AddPolicyOwnerControllerCollection(polictOwnerArr, insurnceId, companyId);


            RenderCovArr(covrageArr, polObjectMirrorDic);
            _lifeCoverageDic = CoverageController.AddLifeCoverageCollection(covrageArr);


            _clientDics = ClientController.AddClientCollection(clientsArr, agentId, categoryClientID, clientTypeID);

            _fundsDic = FundTypeController.AddFundTypesCollection(fundTypeArr, _fund_induestryDic);

            LifePolicyContoller lifePolicyContoller = new LifePolicyContoller(_lifeCoverageDic, _fundsDic, _clientDics, _policyOwnerDic);

            lifePolicyContoller.AddLifePolicyCollection((JArray)lifePolicyObject[".POL"], companyId, lifeIndustryID, insurnceId);



            string agentNumberSTR = polObject[0].Value<int>("PrincipalAgentNumberID").ToString();

            UpdateAgentIfNeeded(companyId, insurnceId, agentNumberSTR);
        }


        private void RenderPolicyArr(JArray polObject_arr)
        {
            foreach (var polItem in polObject_arr)
            {
                string induestryName = _induestryMaping[polItem["InsuranceID"].ToString()].ToString();
                int insuranceID = InitialProdoction.GetLifeIndustryID(induestryName);
                polItem["InsuranceID"] = insuranceID;
                polItem["LifeIndustryID"] = insuranceID;
                if (!_fund_induestryDic.ContainsKey((int)polItem["FundTypeID"]))
                {
                   
                    _fund_induestryDic.Add((int)polItem["FundTypeID"], insuranceID);
                }
                int polNumber = polItem.Value<int>("PolicyNumber");

                _polObjectDic.Add(polNumber, polItem);
            }
        }

        private void RenderCovArr(JArray covrageArr, Dictionary<int, JToken> polObjectMirrorDic)
        {
            foreach (var covItem in covrageArr)
            {
                try
                {
                    double insurenceAmount = covItem.Value<double>("InsuranceAmount");
                    int lifeCoverageTypeID = covItem.Value<int>("LifeCoverageTypeID");

                    int lifePolicyNumber = covItem.Value<int>("LifePolicyID");

                    if ((polObjectMirrorDic.ContainsKey(lifePolicyNumber)))
                    {
                        JToken itemPol = polObjectMirrorDic[lifePolicyNumber];

                        int LifeIndustryID = _polObjectDic[lifePolicyNumber].Value<int>("LifeIndustryID");

                       
                        covItem["LifeCoverageTypeID"] = InitialProdoction.GetLifeCovDef(LifeIndustryID);
                        foreach (var x in itemPol)
                        {
                            try
                            {
                                var key = ((JProperty)(x)).Name;
                                if (((JProperty)(x)).Value.Type == JTokenType.Integer)
                                {
                                    int val = itemPol.Value<int>(key);
                                    if (val == insurenceAmount)
                                    {
                                        covItem["LifeCoverageTypeID"] = InitialProdoction.GetLifeCoverageTypeID(LifeIndustryID, key, lifeCoverageTypeID.ToString());


                                    }
                                }
                            }
                            catch
                            {


                            }
                        }
                    }
                }
                catch
                {


                }
            }
        }

        private void UpdateAgentIfNeeded(int companyId, int insurenceID, string agentNumberSTR)
        {

            if (!IsAgentDetected(companyId, insurenceID))
            {

                AgentCotroller.UpdateAgentIfNeeded(companyId, insurenceID, agentNumberSTR, _agentId);

            }
        }

        public bool IsAgentDetected(int companyId, int insurenceID)
        {
            if (_lifePolicyObject.Count > 0)
            {
                JArray polObject = (JArray)_lifePolicyObject[".POL"];

                string agentNumberSTR = polObject[0].Value<int>("PrincipalAgentNumberID").ToString();

                int agentIDFound;
                if (AgentCotroller.IsAgentDetected(companyId, insurenceID, agentNumberSTR, out agentIDFound))
                {
                    _agentId = agentIDFound;
                    return true;
                }
            }


            return false;
        }

        public void SetAgentID(int agentID)
        {
            _agentId = agentID;
        }


    }
}
