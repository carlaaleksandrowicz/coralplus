﻿using Coral.Prod.Managment.ProfoctionFormat.GenericParsers.apex;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coral.Prod.Managment.ProfoctionFormat.GenericParsers
{
    public class ApexIteratedFileParser
    {
        #region members
        private string _tableFile = string.Empty;
        private string _productionFile = string.Empty;
        List<ApexColumn> tableFields;
        public JArray ResultSet = new JArray();


        #endregion

        public ApexIteratedFileParser(string tableFile, string productionFile, bool reverse, bool mirror = false)
        {
            _tableFile = tableFile;
            _productionFile = productionFile;
            readTableFile(_tableFile);
            parseData(reverse, mirror);


        }

        private void readTableFile(string tableFile)
        {
            using (StreamReader r = new StreamReader(tableFile))
            {
                string json = r.ReadToEnd();
                tableFields = JsonConvert.DeserializeObject<List<ApexColumn>>(json);
                foreach (var item in tableFields)
                {
                    item.parseTypeAndLength();
                }
            }
        }

        private void parseData(bool reverse ,bool mirror = false)
        {
            string line;

            // Read the file and display it line by line.
            System.IO.StreamReader file =
               new System.IO.StreamReader(_productionFile, Encoding.GetEncoding(862), true);

            while ((line = file.ReadLine()) != null)
            {
                if (!mirror)
                {
                    ResultSet.Add(parseLine(line, reverse));
                }
                else
                {
                    ResultSet.Add(parseLineMirror(line, reverse));
                }
            }

            file.Close();

        }


        private JObject parseLine(string str, bool reverse)
        {
            JObject obj = new JObject();

            StringBuilder sbComments = new StringBuilder();
            int charIndex = 0;
            foreach (var field in tableFields)
            {
                var value = str.Substring(charIndex, field.Length);

                if (!string.IsNullOrWhiteSpace(field.refField))
                {
                    var refFiledArr = field.refField.Split(':');
                    foreach (var refFielditem in refFiledArr)
                    {
                        if (refFielditem == "Comments")
                        {
                            sbComments.AppendLine(string.Format("{1}:{0}", field.fieldName, Reverse(value.Trim(), reverse)));
                        }

                        else if (refFielditem.Contains(".Comments"))
                        {
                            if (obj[refFielditem] == null)
                            {
                                obj.Add(refFielditem, string.Empty);
                            }

                            obj[refFielditem] = obj[refFielditem] + string.Format("{1}:{0}", field.fieldName, Reverse(value.Trim(), reverse));
                        }
                        else if (field.Type == FieldType.integerType)
                        {
                            int val;
                            int.TryParse(value.Trim(), out val);
                            if (obj[refFielditem] != null)
                            {
                                if (val != 0)
                                {
                                    obj[refFielditem] = val;
                                }
                            }
                            else
                            {
                                obj.Add(refFielditem, val);
                            }
                        }
                        else if (field.Type == FieldType.stringType)
                        {
                            obj.Add(refFielditem, Reverse(value.Trim(), reverse));
                        }
                        else if (field.Type == FieldType.dateTimeType)
                        {
                            obj.Add(refFielditem, value.Trim());
                            //  retDictionary.Add(field.fieldName, DateTime.ParseExact(value.Trim(), "YYMMDD", CultureInfo.InvariantCulture));
                        }
                        else if (field.Type == FieldType.floatType)
                        {
                            var doubleval = Double.Parse(value.Insert(value.Length - field.DotLocation, "."));
                            obj.Add(field.refField, doubleval);
                        }


                    }

                }

                sbComments.AppendLine(string.Format("{1}:{0}", field.fieldName, value.ToString()));

                charIndex = charIndex + field.Length;
            }
            obj.Add("Comments", sbComments.ToString());

            return obj;

        }

        private JObject parseLineMirror(string str, bool reverse)
        {
            JObject obj = new JObject();


            int charIndex = 0;
            foreach (var field in tableFields)
            {
                var value = str.Substring(charIndex, field.Length);

                if (field.Type == FieldType.integerType)
                {
                    int val;
                    int.TryParse(value.Trim(), out val);
                    if (obj[field.fieldName] != null)
                    {
                        if (val != 0)
                        {
                            obj[field.fieldName] = val;
                        }
                    }
                    else
                    {
                        obj.Add(field.fieldName, val);
                    }
                }
                else if (field.Type == FieldType.stringType)
                {
                    obj.Add(field.fieldName, Reverse(value.Trim(), reverse));
                }
                else if (field.Type == FieldType.dateTimeType)
                {
                    obj.Add(field.fieldName, value.Trim());
                    //  retDictionary.Add(field.fieldName, DateTime.ParseExact(value.Trim(), "YYMMDD", CultureInfo.InvariantCulture));
                }
                else if (field.Type == FieldType.floatType)
                {
                    var doubleval = Double.Parse(value.Insert(value.Length - field.DotLocation, "."));
                    obj.Add(field.refField, doubleval);
                }




                charIndex = charIndex + field.Length;
            }


            return obj;

        }
        public static string Reverse(string s, bool reverse)
        {
            if (IsDigitsOnly(s)) return (s);
            if (!reverse) return (s);
            try
            {
                char[] charArray = s.ToCharArray();
                Array.Reverse(charArray);
                return new string(charArray);
            }
            catch (Exception ex)
            {

            }

            return s;
        }

        static bool IsDigitsOnly(string str)
        {
            foreach (char c in str)
            {
                if (c < '0' || c > '9')
                    return false;
            }

            return true;
        }
    }
}

