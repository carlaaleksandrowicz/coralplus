﻿using Coral.Prod.Managment.ProfoctionFormat.general;
using Coral.Prod.Managment.ProfoctionFormat.GenericParsers;
using Coral.Prod.Managment.ProfoctionFormat.genral;
using CoralBusinessLogics;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Coral.Prod.Managment.ProfoctionFormat.ProdoctionParsers
{
    public class ClalApexLifeBritut : IProdoctionParser
    {
        private Dictionary<string, Client> _clientDics = new Dictionary<string, Client>();
        private Dictionary<int, HealthInsuranceType> _healthInsuranceTypeDics = new Dictionary<int, HealthInsuranceType>();
        private Dictionary<string, List<HealthCoverage>> _healthCoverageDic = new Dictionary<string, List<HealthCoverage>>();
        JObject _healthPolicyObject;
        int _agentId;

        void IProdoctionParser.ParseProdoction(string directoryName)
        {
            JObject ret = new JObject();

            DirectoryInfo di = new DirectoryInfo(directoryName);
            foreach (var fileItem in di.GetFiles())
            {
                string tableName = FileResourcesTool.ExtractEmbededResource("Clal","LifeHealth.Health", fileItem.Extension);
                if (!string.IsNullOrWhiteSpace(tableName))
                {
                    string SaveFileAsUtf8 = FileResourcesTool.SaveFileAsUTF(fileItem.FullName, Encoding.GetEncoding(1255));

                    ApexIteratedFileParser fileParser = new ApexIteratedFileParser(tableName, SaveFileAsUtf8, false);
                    ret.Add(fileItem.Extension, fileParser.ResultSet);
                }

                if (File.Exists(tableName))
                {
                    File.Delete(tableName);
                }

            }

            _healthPolicyObject = ret;
        }


        public void ConvertToDB(int companyId)
        {
            JObject healthPolicyObject = _healthPolicyObject;

            int insurnceId = InitialProdoction.GetInsurnceID("בריאות", companyId);
            int agentId = _agentId;
            int categoryClientID = InitialProdoction.GetCategoryClientID("לקוחות");
            int clientTypeID = InitialProdoction.GetClientTypeID("לקוחות");
            int healthCoverageType = InitialProdoction.GetHealthCoverageTypeID();


            JArray clientsArr = (JArray)healthPolicyObject[".MEV"];
            ApexRefactorUtil.RefactorClients(clientsArr);



            _clientDics = ClientController.AddClientCollection(clientsArr, agentId, categoryClientID, clientTypeID);
            _healthCoverageDic = CoverageController.AddHealthCoverageCollection((JArray)healthPolicyObject[".TAR"], healthCoverageType);
            _healthInsuranceTypeDics = HealthInsuranceTypeController.AddHealthInsuranceTypesCollection((JArray)healthPolicyObject[".SGP"], companyId);

            HealthPolicyContoller healthPolicyContoller = new HealthPolicyContoller(_clientDics, _healthInsuranceTypeDics, _healthCoverageDic);
            healthPolicyContoller.AddHealthPolicyCollection((JArray)healthPolicyObject[".POL"], insurnceId, companyId);

            JArray polObject = (JArray)healthPolicyObject[".POL"];

            string agentNumberSTR = polObject[0].Value<int>("PrincipalAgentNumberID").ToString();

            UpdateAgentIfNeeded(companyId, insurnceId, agentNumberSTR);
        }


        private void UpdateAgentIfNeeded(int companyId, int insurenceID, string agentNumberSTR)
        {

            if (!IsAgentDetected(companyId, insurenceID))
            {

                AgentCotroller.UpdateAgentIfNeeded(companyId, insurenceID, agentNumberSTR, _agentId);

            }
        }

        public bool IsAgentDetected(int companyId, int insurenceID)
        {
            if (_healthPolicyObject.Count > 0)
            {
                JArray polObject = (JArray)_healthPolicyObject[".POL"];

                string agentNumberSTR = polObject[0].Value<int>("PrincipalAgentNumberID").ToString();

                int agentIDFound;
                if (AgentCotroller.IsAgentDetected(companyId, insurenceID, agentNumberSTR, out agentIDFound))
                {
                    _agentId = agentIDFound;
                    return true;
                }
            }


            return false;
        }

        public void SetAgentID(int agentID)
        {
            _agentId = agentID;
        }
    }
}
