﻿using Coral.Prod.Managment.ProfoctionFormat.general;
using Coral.Prod.Managment.ProfoctionFormat.general.Elementry;
using Coral.Prod.Managment.ProfoctionFormat.general.Life;
using Coral.Prod.Managment.ProfoctionFormat.GenericParsers;
using Coral.Prod.Managment.ProfoctionFormat.GenericParsers.Hazakot;
using Coral.Prod.Managment.ProfoctionFormat.genral;
using CoralBusinessLogics;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Coral.Prod.Managment.ProfoctionFormat.ProdoctionParsers
{
    public class ClalMemshakHazakot : IProdoctionParser
    {

        //  private Dictionary<int, FundType> _fundsDic = new Dictionary<int, FundType>();
        private Dictionary<string, Client> _clientDics = new Dictionary<string, Client>();
        //  private Dictionary<int, LifeInsuranceType> _lifeInsuranceTypeDic = new Dictionary<int, LifeInsuranceType>();
        List<Mimshak> _mimshakList;
        int _agentId;
        void IProdoctionParser.ParseProdoction(string directoryName)
        {
            List<Mimshak> ret = new List<Mimshak>();
            JObject retObject = new JObject();
            DirectoryInfo di = new DirectoryInfo(directoryName);
            foreach (var fileItem in di.GetFiles())
            {
                if (fileItem.Extension.ToLower() == ".dat")
                {
                    ret.Add(DeserializMemshak.Deserialize(fileItem.FullName));
                }
            }
            _mimshakList = ret;
        }


        public void ConvertToDB(int companyId)
        {
            List<Mimshak> mimshakList = _mimshakList;

            int insurnceId = InitialProdoction.GetInsurnceID("פיננסים", companyId);


            int agentId = _agentId;
            int categoryClientID = InitialProdoction.GetCategoryClientID("לקוחות");
            int clientTypeID = InitialProdoction.GetClientTypeID("לקוחות");
            

            foreach (var itemMimshak in mimshakList)
            {
                JArray clientsArr = ParseClientArr(itemMimshak);

                _clientDics = ClientController.AddClientCollection(clientsArr, agentId, categoryClientID, clientTypeID);
                JArray financePolArr = ParsePolicy(itemMimshak, companyId, insurnceId);

                FinancePolicyContoller fpc = new FinancePolicyContoller();
                fpc.AddFinancePolicyCollection(financePolArr);
            }


        }

        private JArray ParseClientArr(Mimshak MimshakObject)
        {
            JArray clientsArr = new JArray();
            foreach (var yeshutYatzran in MimshakObject.YeshutYatzran)
            {
                foreach (var mutzar in yeshutYatzran.Mutzarim)
                {
                    var yeshutLakoach = mutzar.NetuneiMutzar.YeshutLakoach;
                    JObject newClients = new JObject();
                    newClients.Add("FirstName", yeshutLakoach.SHEMPRATI);
                    newClients.Add("LastName", yeshutLakoach.SHEMMISHPACHA);
                    newClients.Add("BirthDate", yeshutLakoach.TAARICHLEYDA.Remove(0, 2));
                    newClients.Add("IdNumber", yeshutLakoach.MISPARZIHUYLAKOACH);
                    newClients.Add("City", yeshutLakoach.SHEMYISHUV);
                    newClients.Add("Street", yeshutLakoach.SHEMRECHOV);
                    newClients.Add("HomeNumber", yeshutLakoach.MISPARBAIT);
                    newClients.Add("ZipCode", yeshutLakoach.MIKUD.ToString());
                    newClients.Add("Email", yeshutLakoach.EMAIL);
                    newClients.Add("CellPhone", yeshutLakoach.MISPARCELLULARI);
                    clientsArr.Add(newClients);
                }

            }

            return clientsArr;
        }




        //[FinanceProgramTypes] - תיק השקעות , קרן השתלמות , קופת גמל
        private JArray ParsePolicy(Mimshak MimshakObject, int companyID, int insurnceID)
        {
            JArray FinancePolicysArr = new JArray();
            foreach (var yeshutYatzran in MimshakObject.YeshutYatzran)
            {

                foreach (var mutzar in yeshutYatzran.Mutzarim)
                {
                    var YeshutMaasik = mutzar.NetuneiMutzar.YeshutMaasik.First();
                    JObject newfinancePolicyTemplate = new JObject();

                    newfinancePolicyTemplate.Add("PolicyOwnerIdNumber", YeshutMaasik.MISPARMEZAHEMAASIK);
                    newfinancePolicyTemplate.Add("ManagerName", YeshutMaasik.SHEMMAASIK);
                    newfinancePolicyTemplate.Add("PolicyOwnerAddress", YeshutMaasik.SHEMRECHOV + " " + YeshutMaasik.SHEMYISHUV);
                    newfinancePolicyTemplate.Add("PolicyOwnerFax", YeshutMaasik.MISPARFAX);
                    newfinancePolicyTemplate.Add("PolicyOwnerCellphone", YeshutMaasik.MISPARCELLULARI);
                    newfinancePolicyTemplate.Add("PolicyOwnerEmail", YeshutMaasik.EMAIL);
                    newfinancePolicyTemplate.Add("PolicyOwnerPhone", YeshutMaasik.MISPARTELEPHONEKAVI);

                    newfinancePolicyTemplate.Add("ProgramTypeID", ParseProgramType(mutzar.NetuneiMutzar.SUGMUTZAR));

                    var clientID = int.Parse(mutzar.NetuneiMutzar.YeshutLakoach.MISPARZIHUYLAKOACH);
                    newfinancePolicyTemplate.Add("ClientID", _clientDics[clientID.ToString()].ClientID);
                    newfinancePolicyTemplate.Add("CompanyID", companyID);
                    newfinancePolicyTemplate.Add("InsuranceID", insurnceID);


                    foreach (var itemPolisa in mutzar.HeshbonotOPolisot)
                    {
                        JObject newfinancePolicy = new JObject();

                        MergeObject(newfinancePolicy, newfinancePolicyTemplate);
                        newfinancePolicy.Add("OpenDate", itemPolisa.TAARICHHITZTARFUTMUTZAR);
                        newfinancePolicy.Add("StartDate", itemPolisa.TAARICHHITZTARFUTRISHON);
                        newfinancePolicy.Add("AssociateStatus", ParseAssociateStatus(itemPolisa.SUGTOCHNITOCHESHBON));
                        newfinancePolicy.Add("PaymentMethod", ParsePaymentMethod(itemPolisa.PirteiTaktziv.First().NetuneiGvia.KODEMTZAEITASHLUM));
                        newfinancePolicy.Add("AssociateNumber", itemPolisa.MISPARPOLISAOHESHBON);
                        newfinancePolicy.Add("ReportedSalary", itemPolisa.PirteiTaktziv.First().PirteiHaasaka.SACHARPOLISA);
                        newfinancePolicy.Add("StartDateEmployee", itemPolisa.PirteiTaktziv.First().PirteiHaasaka.TAARICHTCHILATTASHLUM);
                        newfinancePolicy.Add("AdministrationFeesEmployee", itemPolisa.PirteiTaktziv.First().PerutHotzaot.HotzaotBafoalLehodeshDivoach.SHEURDMEINIHULHAFKADA);
                        newfinancePolicy.Add("AggregateFeesEmployee", itemPolisa.PirteiTaktziv.First().PerutHotzaot.HotzaotBafoalLehodeshDivoach.SHEURDMEINIHULTZVIRA);
                        try
                        {
                            if (itemPolisa.PirteiTaktziv.First().PerutHafrashotLePolisa.First(a => a.SUGHAMAFKID == 1) != null)
                            {
                                newfinancePolicy.Add("WorkerDeposit", itemPolisa.PirteiTaktziv.First().PerutHafrashotLePolisa.First(a => a.SUGHAMAFKID == 1).ACHUZHAFRASHA);
                            }
                        }
                        catch
                        {

                        }
                        try
                        {
                            var s = itemPolisa.PirteiTaktziv.First().PerutHafrashotLePolisa.First(a => a.SUGHAMAFKID == 2);
                            if (s != null)
                            {
                                newfinancePolicy.Add("EmployerDeposit", itemPolisa.PirteiTaktziv.First().PerutHafrashotLePolisa.First(a => a.SUGHAMAFKID == 2).ACHUZHAFRASHA);
                                newfinancePolicy.Add("DepositAmountEmployee", itemPolisa.PirteiTaktziv.First().PerutHafrashotLePolisa.First(a => a.SUGHAMAFKID == 2).SCHUMHAFRASHA);
                            }
                        }
                        catch
                        { }

                        FinancePolicysArr.Add(newfinancePolicy);
                    }

                }
            }
            return FinancePolicysArr;

        }


        private static void MergeObject(JObject obj1, JObject itemObj2)
        {
            if (itemObj2 == null) return;
            foreach (var innerData in itemObj2)
            {
                if (obj1[innerData.Key] == null)
                {
                    obj1.Add(innerData.Key, innerData.Value);
                }
            }
        }
        private string ParseAssociateStatus(int status)
        {
            string ret = string.Empty;
            switch (status)
            {
                case 1:
                    ret = "שכיר";
                    break;
                case 2:
                    ret = "עצמאי";
                    break;
                case 3:
                    ret = "שכיר בעל שליטה";
                    break;
                default:
                    ret = "חבר קיבוץ/עמית שיתופי";
                    break;
            }

            return ret;

        }

        private int ParseProgramType(int status)
        {
            string ret = string.Empty;
            int programTypeID = 1;
            switch (status)
            {
                case 3:
                    ret = "קופת גמל";
                    break;
                case 4:
                    ret = "קרן השתלמות";
                    break;
                case 2:
                    ret = "קרן השתלמות";
                    break;
                case 1:
                    ret = "קרן השתלמות";
                    break;


                default:
                    ret = "שגויים";
                    break;

            }

            using (Coral_DB_Entities context = new Coral_DB_Entities())
            {
                var programType = context.FinanceProgramTypes.First(i => i.ProgramTypeName == ret);
                if (programType != null)
                {
                    programTypeID = programType.ProgramTypeID;
                }
            }

            return programTypeID;

        }

        private string ParsePaymentMethod(int status)
        {
            string ret = string.Empty;
            switch (status)
            {
                case 1:
                    ret = "העברה בנקאית";
                    break;
                case 2:
                    ret = "הוראת קבע";
                    break;
                case 3:
                    ret = "כרטיס אשראי";
                    break;
                case 4:
                    ret = "מזומן";
                    break;
                default:
                    ret = "אחר";
                    break;
            }

            return ret;

        }


        private void UpdateAgentIfNeeded(int companyId, int insurenceID, string agentNumberSTR)
        {

            if (!IsAgentDetected(companyId, insurenceID))
            {

                AgentCotroller.UpdateAgentIfNeeded(companyId, insurenceID, agentNumberSTR, _agentId);

            }
        }

        public bool IsAgentDetected(int companyId, int insurenceID)
        {
            return false;
            // throw new NotImplementedException();
        }

        public void SetAgentID(int agentID)
        {
            _agentId = agentID;
        }
    }
}
