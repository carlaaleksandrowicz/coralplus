﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Coral.Prod.Managment.ProfoctionFormat.ProdoctionParsers;
using Coral.Prod.Managment.ProfoctionFormat.genral;

namespace Coral.Prod.Managment.ProfoctionFormat
{
    class ProdoctionFormatHarel : ProdoctionFormatBase
    {
        public ProdoctionFormatHarel(string rootDirectory, string insurenseName) : base(rootDirectory, insurenseName)
        {
            DirectoryInfo leav = Utils.Dicrctory.GetLeafDir(rootDirectory);
            DirectoryInfo rootDirectoryDI = new DirectoryInfo(rootDirectory);

            this._prodoctionDirectories = leav.Parent.GetDirectories();
            this._prodoctionFormat = leav.Parent.Name;


            this._prodoctionParser = ProdoctionParserFactory(_insurenseName);
            int companyId = InitialProdoction.SetCompany("הראל");
            if (rootDirectoryDI.GetDirectories().Count() == 0)
            {
                _prodoctionParser.ParseProdoction(rootDirectory);
            }
            else
            {
                foreach (var item in this._prodoctionDirectories)
                {
                    _prodoctionParser.ParseProdoction(item.FullName);
                }
            }
        }

        private bool DetectProblemWithInsurence()
        {

            return false;
        }

        protected override IProdoctionParser ProdoctionParserFactory(string name)
        {
            switch (name)
            {
                case "בריאות":
                    //return new ClalApexLifeBritut();

                case "חיים":
                    //return new ClalApexLife();

                case "פיננסים":
                    //return new ClalMemshakHazakot();

                case "אלמנטרי":
                    return new Harel_IgudElementry();

                default:
                    return null;

            }
        }
    }
}
