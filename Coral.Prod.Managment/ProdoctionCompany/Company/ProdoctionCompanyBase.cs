﻿using Coral.Prod.Managment.ProfoctionFormat;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coral.Prod.Managment
{
    public abstract class ProdoctionCompanyBase
    {
        #region members
        protected ProdoctionFormatBase _prodoctionFormat;
        #endregion

        protected abstract ProdoctionFormatBase LoadFile(string fileName, string insurenseName);


        public ProdoctionFormatBase ProdoctionFormat
        {
            get
            {
                return _prodoctionFormat;
            }

        }

    }
}
