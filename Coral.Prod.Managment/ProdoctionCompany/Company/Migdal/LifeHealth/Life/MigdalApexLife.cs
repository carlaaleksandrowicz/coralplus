﻿using Coral.Prod.Managment.ProfoctionFormat;
using Coral.Prod.Managment.ProfoctionFormat.general;
using Coral.Prod.Managment.ProfoctionFormat.general.Life;
using Coral.Prod.Managment.ProfoctionFormat.GenericParsers;
using Coral.Prod.Managment.ProfoctionFormat.genral;
using Coral.Prod.Managment.ProfoctionFormat.ProdoctionParsers;
using CoralBusinessLogics;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Coral.Prod.Managment.ProfoctionCompany.Company.Migdal.LifeHealth.Life
{
    public class MigdalApexLife : IProdoctionParser
    {
        private Dictionary<string, List<LifeCoverage>> _lifeCoverageDic = new Dictionary<string, List<LifeCoverage>>();
        private Dictionary<string, PolicyOwner> _policyOwnerDic = new Dictionary<string, PolicyOwner>();
        private Dictionary<int, string> _fundsDic = new Dictionary<int, string>();
        private Dictionary<string, Client> _clientDics = new Dictionary<string, Client>();
        private Dictionary<int, int> _fund_induestryDic = new Dictionary<int, int>();
        //private Dictionary<int, LifeInsuranceType> _lifeInsuranceTypeDic = new Dictionary<int, LifeInsuranceType>();
        private JObject _covTypeMaping;
        private JObject _induestryMaping;
        private JObject _lifePolicyObject;
        private int _agentId;
        Dictionary<int, JToken> _polObjectDic = new Dictionary<int, JToken>();



        void IProdoctionParser.ParseProdoction(string directoryName)
        {
            JObject ret = new JObject();

            DirectoryInfo di = new DirectoryInfo(directoryName);
            foreach (var fileItem in di.GetFiles())
            {
                string tableName = FileResourcesTool.ExtractEmbededResource("Migdal", "LifeHealth.Life", "." + fileItem.Name);
                if (!string.IsNullOrWhiteSpace(tableName))
                {
                    string SaveFileAsUtf8 = FileResourcesTool.SaveFileAsUTF(fileItem.FullName, Encoding.GetEncoding("ISO-8859-8"));

                    ApexIteratedFileParser fileParser = new ApexIteratedFileParser(tableName, SaveFileAsUtf8, true);

                    ret.Add(fileItem.Name, fileParser.ResultSet);

                    if (fileItem.Name.ToLower() == "LIFE.MBT".ToLower())
                    {
                        ApexIteratedFileParser fileParserMiror = new ApexIteratedFileParser(tableName, SaveFileAsUtf8, true, true);

                        ret.Add(fileItem.Name + "M", fileParserMiror.ResultSet);
                    }
                }
                else
                {
                    //tableName = FileResourcesTool.ExtractEmbededResource("Migdal", "LifeHealth.Life", fileItem.Name + "1");
                    //if (!string.IsNullOrWhiteSpace(tableName))
                    //{
                    //    string SaveFileAsUtf8 = FileResourcesTool.SaveFileAsUTF(fileItem.FullName, Encoding.GetEncoding(862));

                    //    ApexIteratedFileParser fileParser = new ApexIteratedFileParser(tableName, SaveFileAsUtf8, true);
                    //    ret.Add(fileItem.Name, fileParser.ResultSet);
                    //}
                }

            }

            string tableNameSgp_induestry = FileResourcesTool.ExtractEmbededResource("Migdal", "LifeHealth.Life", ".sgp_induestry");


            string tableNameSgp_induestryContent = File.ReadAllText(tableNameSgp_induestry);
            _induestryMaping = JObject.Parse(tableNameSgp_induestryContent);

            string tableCovType = FileResourcesTool.ExtractEmbededResource("Migdal", "LifeHealth.Life", ".covType");
            string tableCovType_Content = File.ReadAllText(tableCovType);
            _covTypeMaping = JObject.Parse(tableCovType_Content);




            _lifePolicyObject = ret;
        }

        public void ConvertToDB(int companyId)
        {
            JObject lifePolicyObject = _lifePolicyObject;

            int insurnceId = InitialProdoction.GetInsurnceID("חיים", companyId);
            int lifeIndustryID = InitialProdoction.GetLifeIndustryID("פרט");
            int agentId = _agentId;
            int categoryClientID = InitialProdoction.GetCategoryClientID("לקוחות");
            int clientTypeID = InitialProdoction.GetClientTypeID("לקוחות");

            JArray polObject = (JArray)_lifePolicyObject["LIFE.MBT"];
            JArray polObjectMirror = (JArray)_lifePolicyObject["LIFE.MBTM"];
            Dictionary<int, JToken> polObjectMirrorDic = new Dictionary<int, JToken>();

            foreach (var item in polObjectMirror)
            {
                int polNumber = item.Value<int>("PolicyNumber");
                if (!polObjectMirrorDic.ContainsKey(polNumber))
                {
                    polObjectMirrorDic.Add(polNumber, item);
                }
            }



            JObject jo_fund = new JObject();
            jo_fund.Add("InsurnceCode", 0);
            jo_fund.Add("InsurnceDesc", "כללי");


            JArray clientsArr = (JArray)lifePolicyObject["PERSON.MBT"];
            JArray fundTypeArr = (JArray)lifePolicyObject[".SGB"];
            fundTypeArr = new JArray();
            fundTypeArr.Add(jo_fund);
            ApexRefactorUtil.RefactorClients(clientsArr);

            JArray covrageArr = (JArray)lifePolicyObject["COVRLIFE.MBT"];

            JArray polictOwnerArr = (JArray)lifePolicyObject["COMPANY.MBT"];

            RenderPolicyArr(polObject, insurnceId);
            _policyOwnerDic = PolicyOwnerController.AddPolicyOwnerControllerCollection(polictOwnerArr, insurnceId, companyId);


            RenderCovArr(covrageArr, polObjectMirrorDic, lifeIndustryID);
            _lifeCoverageDic = CoverageController.AddLifeCoverageCollection(covrageArr);


            _clientDics = ClientController.AddClientCollection(clientsArr, agentId, categoryClientID, clientTypeID);

            if (fundTypeArr != null)
            {
                _fundsDic = FundTypeController.AddFundTypesCollection(fundTypeArr, _fund_induestryDic);
            }

            LifePolicyContoller lifePolicyContoller = new LifePolicyContoller(_lifeCoverageDic, _fundsDic, _clientDics, _policyOwnerDic);

            lifePolicyContoller.AddLifePolicyCollection((JArray)lifePolicyObject["LIFE.MBT"], companyId, lifeIndustryID, insurnceId);



            string agentNumberSTR = polObject[0].Value<int>("PrincipalAgentNumberID").ToString();

            UpdateAgentIfNeeded(companyId, insurnceId, agentNumberSTR);
        }


        private void RenderPolicyArr(JArray polObject_arr, int insuranceID)
        {
            foreach (var polItem in polObject_arr)
            {
                string induestryName = _induestryMaping[polItem["InsuranceID"].ToString()].ToString();
                int LifeIndustryID = InitialProdoction.GetLifeIndustryID(induestryName);
                polItem["InsuranceID"] = insuranceID;
                polItem["LifeIndustryID"] = LifeIndustryID;
                var monthlyPayment = polItem["TotalMonthlyPayment"].ToString().Split('|');

                try
                {
                    polItem["TotalMonthlyPayment"] = double.Parse(monthlyPayment[monthlyPayment.Length - 1]);
                }
                catch
                {
                    polItem["TotalMonthlyPayment"] = 0;
                }
                if (_fund_induestryDic != null && polItem["FundTypeID"] != null)
                {
                    if (!_fund_induestryDic.ContainsKey((int)polItem["FundTypeID"]))
                    {

                        _fund_induestryDic.Add((int)polItem["FundTypeID"], insuranceID);
                    }
                }
                int polNumber = polItem.Value<int>("PolicyNumber");

                _polObjectDic.Add(polNumber, polItem);
            }
        }

        private void RenderCovArr(JArray covrageArr, Dictionary<int, JToken> polObjectMirrorDic, int LifeIndustryID)
        {
            foreach (var covItem in covrageArr)
            {
                try
                {
                    double insurenceAmount = covItem.Value<double>("InsuranceAmount");
                    int lifeCoverageTypeID = covItem.Value<int>("LifeCoverageTypeID");



                    if (_covTypeMaping[lifeCoverageTypeID.ToString()] != null)
                    {
                        covItem["LifeCoverageTypeID"] = InitialProdoction.GetLifeCoverageTypeID(LifeIndustryID, _covTypeMaping[lifeCoverageTypeID.ToString()].ToString(), lifeCoverageTypeID.ToString());
                    }
                    else
                    {
                        covItem["LifeCoverageTypeID"] = InitialProdoction.GetLifeCoverageTypeID(LifeIndustryID, "כללי", "000");
                    }

                }
                catch
                {

                }
            }
        }

        private void UpdateAgentIfNeeded(int companyId, int insurenceID, string agentNumberSTR)
        {

            if (!IsAgentDetected(companyId, insurenceID))
            {

                AgentCotroller.UpdateAgentIfNeeded(companyId, insurenceID, agentNumberSTR, _agentId);

            }
        }

        public bool IsAgentDetected(int companyId, int insurenceID)
        {
            if (_lifePolicyObject.Count > 0)
            {
                JArray polObject = (JArray)_lifePolicyObject["LIFE.MBT"];

                string agentNumberSTR = polObject[0].Value<int>("PrincipalAgentNumberID").ToString();

                int agentIDFound;
                if (AgentCotroller.IsAgentDetected(companyId, insurenceID, agentNumberSTR, out agentIDFound))
                {
                    _agentId = agentIDFound;
                    return true;
                }
            }


            return false;
        }

        public void SetAgentID(int agentID)
        {
            _agentId = agentID;
        }


    }
}
