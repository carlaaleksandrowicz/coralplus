﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Coral.Prod.Managment.ProfoctionFormat.ProdoctionParsers;
using Coral.Prod.Managment.ProfoctionFormat.genral;
using Coral.Prod.Managment.ProfoctionCompany.Company.Migdal.LifeHealth.Life;
using Coral.Prod.Managment.ProfoctionCompany.Company.Migdal.LifeHealth.Health;

namespace Coral.Prod.Managment.ProfoctionFormat
{
    class ProdoctionFormatMigdal : ProdoctionFormatBase
    {
        public ProdoctionFormatMigdal(string rootDirectory, string insurenseName) : base(rootDirectory, insurenseName)
        {
            DirectoryInfo leav = Utils.Dicrctory.GetLeafDir(rootDirectory);
            DirectoryInfo rootDirectoryDI = new DirectoryInfo(rootDirectory);

            this._prodoctionDirectories = leav.Parent.GetDirectories();
            this._prodoctionFormat = leav.Parent.Name;


            this._prodoctionParser = ProdoctionParserFactory(_insurenseName);
            int companyId = InitialProdoction.SetCompany("מגדל");
            if (rootDirectoryDI.GetDirectories().Count() == 0)
            {
                _prodoctionParser.ParseProdoction(rootDirectory);
            }
            else
            {
                foreach (var item in this._prodoctionDirectories)
                {
                    _prodoctionParser.ParseProdoction(item.FullName);
                }
            }
        }

        private bool DetectProblemWithInsurence()
        {

            return false;
        }

        protected override IProdoctionParser ProdoctionParserFactory(string name)
        {
            switch (name)
            {
                case "בריאות":
                    return new MigdalApexLifeBritut();

                case "חיים":
                    return new MigdalApexLife();

                //case "פיננסים":
                //    return new ClalMemshakHazakot();

                case "אלמנטרי":
                    return new Migdal_IgudElementry();

                default:
                    return null;

            }
        }
    }
}
