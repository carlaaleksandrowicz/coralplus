﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using InsertData;


namespace InsertDataUI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();            
        }

        private void btnStart_Click(object sender, RoutedEventArgs e)
        {
            //string toSimpleStr = @"Data Source=SERVER\SQLEXPRESS;Initial Catalog=Coral_DB;Integrated Security=True;";
            string toSimpleStr = @"Data Source=.\SQLEXPRESS2;Initial Catalog=Coral_DB;Integrated Security=True;";

            string toEFStr = string.Format(@"metadata=res://*/DataModel.csdl|res://*/DataModel.ssdl|res://*/DataModel.msl;provider=System.Data.SqlClient;provider connection string=""{0}MultipleActiveResultSets=True;App=EntityFramework""",
                                            toSimpleStr);

            InsertDataDLL insertData = new InsertDataDLL(toEFStr);
            try
            {
                insertData.Start(chbAddFirstUser.IsChecked);
                MessageBox.Show("העברת הנתונים עבר בהצלחה");
                this.Close();
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
    }
}
