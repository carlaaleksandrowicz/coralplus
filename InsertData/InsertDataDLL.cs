﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace InsertData
{

    public class InsertDataDLL
    {
        public string ToEFConStr { get; set; }
        public InsertDataDLL(string toConStr)
        {
            ToEFConStr = toConStr;
        }

        public void Start(bool? addUser)
        {
            CreateRoles(new string[] { "מנהל ראשי", "מנהל", "עובד" }, true);
            InsertClientTypes(new string[] { "לקוחות", "פוטנציאליים", "אלפון", "לא פעילים" });
            InsertInsurances(new string[] { "אלמנטרי", "חיים", "בריאות", "פיננסים", "נסיעות לחו''ל", "תאונות אישיות", "עובדים זרים" });
            InsertElementaryIndustries(new string[] { "רכב", "רכב חובה", "עסק", "דירה", "רכב - חבילה", "שגויים" });
            InsertLifeIndustries(new string[] { "מנהלים", "ריסק", "משכנתא", "פרט", "פנסיה", "שגויים" });
            InsertFinanceProgramTypes(new string[] { "קרן השתלמות", "קופת גמל", "תיק השקעות", "שגויים" });
            InsertForeingWorkersIndustries(new string[] { "עובדים זרים", "תיירים", "שגויים" });
            InsertRenewalStatuses(new RenewalStatus[] { new RenewalStatus { RenewalStatusName = "לא חודש", Status = true, Color = "Gray" }, new RenewalStatus { RenewalStatusName = "חודש", Status = true, Color = "Green" }, new RenewalStatus { RenewalStatusName = "בתהליך", Status = true, Color = "Yellow" }, new RenewalStatus { RenewalStatusName = "לא טופל", Status = true, Color = "Red" }, new RenewalStatus { RenewalStatusName = "פתוח לעבודה", Status = true, Color = "Orange" } });
            InsertInsuredTypes(new string[] { "ראשי", "בן/בת זוג", "ילד/ילדה" });
            if (addUser == true)
            {
                InsertUser();
            }
        }

        private void InsertUser()
        {
            using (Coral_DBEntities1 context = new Coral_DBEntities1(ToEFConStr))
            {
                try
                {
                    User admin = new User()
                    {
                        IsCalendarPermission = true,
                        IsCreateClientPermission = true,
                        IsElementaryPermission = true,
                        IsClientProfilePermission = true,
                        IsFinancePermission = true,
                        IsForeingWorkersPermission = true,
                        IsHealthPermission = true,
                        IsLifePermission = true,
                        IsOffersPermission = true,
                        IsPersonalAccidentsPermission = true,
                        IsRenewalsPermission = true,
                        IsReportsPermission = true,
                        IsScanPermission = true,
                        IsTasksPermission = true,
                        IsTravelPermission = true,
                        Password = "1122",
                        Status = true,
                        Usermame = "admin"
                    };

                    var roles = context.Roles.ToList();
                    foreach (var item in roles)
                    {
                        admin.Roles.Add(item);
                    }
                    context.Users.Add(admin);
                    context.SaveChanges();
                }
                catch (Exception e)
                {
                    throw new Exception(e.Message);
                }
            }
        }

        private void InsertInsuredTypes(string[] types)
        {
            using (Coral_DBEntities1 context = new Coral_DBEntities1(ToEFConStr))
            {
                try
                {
                    foreach (var type in types)
                    {
                        InsuredType insuredType = new InsuredType { InsuredTypeName = type };
                        context.InsuredTypes.Add(insuredType);
                    }
                    context.SaveChanges();
                }
                catch (Exception e)
                {
                    throw new Exception(e.Message);
                }
            }
        }

        private void InsertForeingWorkersIndustries(string[] industries)
        {
            using (Coral_DBEntities1 context = new Coral_DBEntities1(ToEFConStr))
            {
                try
                {
                    foreach (var item in industries)
                    {
                        ForeingWorkersIndustry industry = new ForeingWorkersIndustry { ForeingWorkersIndustryName = item };
                        context.ForeingWorkersIndustries.Add(industry);
                    }
                    context.SaveChanges();
                }
                catch (Exception e)
                {
                    throw new Exception(e.Message);
                }
            }
        }

        private void InsertRenewalStatuses(RenewalStatus[] renewalStatus)
        {
            using (Coral_DBEntities1 context = new Coral_DBEntities1(ToEFConStr))
            {
                try
                {
                    foreach (var item in renewalStatus)
                    {
                        context.RenewalStatuses.Add(item);
                    }
                    context.SaveChanges();
                }
                catch (Exception e)
                {
                    throw new Exception(e.Message);
                }
            }
        }

        private void InsertFinanceProgramTypes(string[] programTypes)
        {
            using (Coral_DBEntities1 context = new Coral_DBEntities1(ToEFConStr))
            {
                try
                {
                    foreach (var item in programTypes)
                    {
                        FinanceProgramType program = new FinanceProgramType { ProgramTypeName = item };
                        context.FinanceProgramTypes.Add(program);
                    }
                    context.SaveChanges();
                }
                catch (Exception e)
                {
                    throw new Exception(e.Message);
                }
            }
        }

        public void CreateRoles(string[] roleNames, bool status)
        {
            using (Coral_DBEntities1 context = new Coral_DBEntities1(ToEFConStr))
            {
                try
                {
                    foreach (var name in roleNames)
                    {
                        Role role = new Role { RoleName = name, Status = status };
                        context.Roles.Add(role);
                    }
                    context.SaveChanges();
                }
                catch (Exception e)
                {
                    throw new Exception(e.Message);
                }
            }
        }

        public void InsertClientTypes(string[] types)
        {
            using (Coral_DBEntities1 context = new Coral_DBEntities1(ToEFConStr))
            {
                try
                {
                    foreach (var type in types)
                    {
                        ClientType clientType = new ClientType { ClientTypeName = type };
                        context.ClientTypes.Add(clientType);
                    }
                    context.SaveChanges();
                }
                catch (Exception e)
                {
                    throw new Exception(e.Message);
                }
            }
        }

        public void InsertInsurances(string[] insurances)
        {
            using (Coral_DBEntities1 context = new Coral_DBEntities1(ToEFConStr))
            {
                try
                {
                    foreach (var name in insurances)
                    {
                        Insurance insurance = new Insurance { InsuranceName = name };
                        context.Insurances.Add(insurance);
                    }
                    context.SaveChanges();
                }
                catch (Exception e)
                {
                    throw new Exception(e.Message);
                }
            }
        }

        public void InsertElementaryIndustries(string[] industries)
        {
            using (Coral_DBEntities1 context = new Coral_DBEntities1(ToEFConStr))
            {
                try
                {
                    foreach (var item in industries)
                    {
                        InsuranceIndustry industry = new InsuranceIndustry { InsuranceIndustryName = item };
                        context.InsuranceIndustries.Add(industry);
                    }
                    context.SaveChanges();
                }
                catch (Exception e)
                {
                    throw new Exception(e.Message);
                }
            }
        }

        public void InsertLifeIndustries(string[] industries)
        {
            using (Coral_DBEntities1 context = new Coral_DBEntities1(ToEFConStr))
            {
                try
                {
                    foreach (var item in industries)
                    {
                        LifeIndustry industry = new LifeIndustry { LifeIndustryName = item };
                        context.LifeIndustries.Add(industry);
                    }
                    context.SaveChanges();
                }
                catch (Exception e)
                {
                    throw new Exception(e.Message);
                }
            }
        }



    }
}
