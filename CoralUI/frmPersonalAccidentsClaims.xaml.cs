﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using CoralBusinessLogics;
using System.IO;
using System.Windows.Media;
using System.Windows.Documents;

namespace CoralUI
{
    /// <summary>
    /// Interaction logic for frmPersonalAccidentsClaims.xaml
    /// </summary>
    public partial class frmPersonalAccidentsClaims : Window
    {
        Client client = null;
        PersonalAccidentsPolicy policy = null;
        User user;
        PersonalAccidentsClaimsLogic claimLogic = new PersonalAccidentsClaimsLogic();
        InsurancesLogic insuranceLogic = new InsurancesLogic();
        InputsValidations validations = new InputsValidations();
        List<PersonalAccidentsWitness> witnesses = new List<PersonalAccidentsWitness>();
        List<PersonalAccidentsClaimTracking> trackings = new List<PersonalAccidentsClaimTracking>();
        TrackingsLogics trackingLogic = new TrackingsLogics();
        private PersonalAccidentsClaim personalAccidentsClaim = null;
        ListViewSettings lv = new ListViewSettings();
        string path = "";
        string newPath = "";
        string clientDirPath = "";
        string[] claimPaths = null;
        List<object> nullErrorList = null;
        InputsValidations validation = new InputsValidations();
        string[] personalAccidentsPaths = null;
        private bool isChanges=false;
        bool confirmBeforeClosing = true;
        bool cancelClose = false;
        PersonalAccidentsWitness witnessSelected;


        public frmPersonalAccidentsClaims(Client policyClient, PersonalAccidentsPolicy policyClaim, User userAccount)
        {
            InitializeComponent();
            client = policyClient;
            policy = policyClaim;
            user = userAccount;
        }

        public frmPersonalAccidentsClaims(PersonalAccidentsClaim personalAccidentsClaim, Client client, User user)
        {
            InitializeComponent();
            this.personalAccidentsClaim = personalAccidentsClaim;
            this.client = client;
            this.user = user;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            string strAppDir = System.AppDomain.CurrentDomain.BaseDirectory;
            string path = strAppDir + @"Coral Files\ClientsPath.txt";

            if (File.Exists(path))
            {
                clientDirPath = File.ReadAllText(path);
            }
            else
            {
                MessageBox.Show("לא נמצא תיקיית הלקוחות. נא בדוק בהגדרות ראשוניות את נתיב התיקייה");
                Close();
                return;
            }
            
            if (personalAccidentsClaim != null)
            {
                personalAccidentsPaths = Directory.GetDirectories(clientDirPath + @"/" + client.ClientID + @"/תאונות אישיות", "*" + personalAccidentsClaim.PersonalAccidentsPolicy.PolicyNumber + "*");
            }
            else if (policy != null)
            {
                personalAccidentsPaths = Directory.GetDirectories(clientDirPath + @"/" + client.ClientID + @"/תאונות אישיות", "*" + policy.PolicyNumber + "*");
            }
            //if (personalAccidentsPaths.Count() > 0)
            //{
            //    claimPaths = Directory.GetDirectories(personalAccidentsPaths[0], "תביעות*");
            //    if (claimPaths.Count() > 0)
            //    {
            //        path = claimPaths[0];
            //    }
            //}
            cbClaimTypeBinding();
            cbClaimConditionBinding();
            lblNameSpace.Content = client.FirstName + " " + client.LastName;
            lblCellPhoneSpace.Content = client.CellPhone;
            lblEmailSpace.Content = client.Email;
            lblIdSpace.Content = client.IdNumber;
            lblPhoneSpace.Content = client.PhoneHome;
            if (policy != null)
            {
                //lblInsuranceTypeSpace.Content = policy.PersonalAccidentsIndustry.PersonalAccidentsIndustryName + " " + policy.FundType.FundTypeName;
                lblPhoneSpace.Content = client.PhoneHome;
                lblStartDateSpace.Content = ((DateTime)policy.StartDate).Date;
                lblCompanySpace.Content = policy.Company.CompanyName;
                dpOpenDate.SelectedDate = DateTime.Now;
                txtPolicyNumber.Text = policy.PolicyNumber;
            }
            else
            {
                //lblInsuranceTypeSpace.Content = personalAccidentsClaim.PersonalAccidentsPolicy.PersonalAccidentsIndustry.PersonalAccidentsIndustryName + " " + personalAccidentsClaim.PersonalAccidentsPolicy.FundType.FundTypeName;
                lblStartDateSpace.Content = ((DateTime)personalAccidentsClaim.PersonalAccidentsPolicy.StartDate).Date;
                lblCompanySpace.Content = personalAccidentsClaim.PersonalAccidentsPolicy.Company.CompanyName;
                dpOpenDate.SelectedDate = (DateTime)personalAccidentsClaim.OpenDate;
                txtPolicyNumber.Text = personalAccidentsClaim.PersonalAccidentsPolicy.PolicyNumber;
                //var claimsTypes = cbClaimType.Items;
                //foreach (var item in claimsTypes)
                //{
                //    PersonalAccidentsClaimType claimType = (PersonalAccidentsClaimType)item;
                //    if (claimType.PersonalAccidentsClaimTypeID == personalAccidentsClaim.PersonalAccidentsClaimTypeID)
                //    {
                //        cbClaimType.SelectedItem = item;
                //        break;
                //    }
                //}
                if (personalAccidentsClaim.PersonalAccidentsClaimTypeID != null)
                {
                    SelectItemInCb((int)personalAccidentsClaim.PersonalAccidentsClaimTypeID);
                }
                txtClaimNumber.Text = personalAccidentsClaim.ClaimNumber;
                if (personalAccidentsClaim.ClaimStatus == false)
                {
                    cbStatus.SelectedIndex = 1;
                }
                if (personalAccidentsClaim.PersonalAccidentsClaimConditionID != null)
                {
                    SelectConditionItemInCb((int)personalAccidentsClaim.PersonalAccidentsClaimConditionID);
                }
                dpDeliveryDateToCompany.SelectedDate = personalAccidentsClaim.DeliveredToCompanyDate;
                dpMoneyReceivedDate.SelectedDate = personalAccidentsClaim.MoneyReceivedDate;
                txtAmountClaimed.Text = validations.ConvertDecimalToString(personalAccidentsClaim.ClaimAmount);
                txtAmountReceived.Text = validations.ConvertDecimalToString(personalAccidentsClaim.AmountReceived);
                dpEventDate.SelectedDate = personalAccidentsClaim.EventDateAndTime;
                tpEventHour.Value = personalAccidentsClaim.EventDateAndTime;
                txtEventPlace.Text = personalAccidentsClaim.EventPlace;
                txtEventDescription.Text = personalAccidentsClaim.EventDescription;
                witnesses = claimLogic.GetWitnessesByClaim(personalAccidentsClaim.PersonalAccidentsClaimID);
                dgWitnessesBinding();
                trackings = trackingLogic.GetAllPersonalAccidentsClaimTrackingsByClaimID(personalAccidentsClaim.PersonalAccidentsClaimID);
                dgTrackingsBinding();
            }

        }


        private void cbClaimTypeBinding()
        {
            cbClaimType.ItemsSource = claimLogic.GetActivePersonalAccidentsClaimTypes();
            cbClaimType.DisplayMemberPath = "ClaimTypeName";
        }

        private void cbClaimConditionBinding()
        {
            cbClaimCondition.ItemsSource = claimLogic.GetActivePersonalAccidentsClaimConditions();
            cbClaimCondition.DisplayMemberPath = "Description";
        }

        private void btnUpdateClaimTypeTable_Click(object sender, RoutedEventArgs e)
        {
            int? selectedItemId = null;
            if (cbClaimType.SelectedItem != null)
            {
                selectedItemId = ((PersonalAccidentsClaimType)cbClaimType.SelectedItem).PersonalAccidentsClaimTypeID;
            }
            frmUpdateTable updateClaimTypesTable = new frmUpdateTable(new PersonalAccidentsClaimType());
            updateClaimTypesTable.ShowDialog();
            cbClaimTypeBinding();
            if (updateClaimTypesTable.ItemAdded != null)
            {
                SelectItemInCb(((PersonalAccidentsClaimType)updateClaimTypesTable.ItemAdded).PersonalAccidentsClaimTypeID);
            }
            else if (selectedItemId != null)
            {
                SelectItemInCb((int)selectedItemId);
            }
        }

        private void SelectItemInCb(int id)
        {
            var items = cbClaimType.Items;
            foreach (var item in items)
            {
                PersonalAccidentsClaimType parsedItem = (PersonalAccidentsClaimType)item;
                if (parsedItem.PersonalAccidentsClaimTypeID == id)
                {
                    cbClaimType.SelectedItem = item;
                    break;
                }
            }
        }

        private void SelectConditionItemInCb(int id)
        {
            var items = cbClaimCondition.Items;
            foreach (var item in items)
            {
                PersonalAccidentsClaimCondition parsedItem = (PersonalAccidentsClaimCondition)item;
                if (parsedItem.PersonalAccidentsClaimConditionID == id)
                {
                    cbClaimCondition.SelectedItem = item;
                    break;
                }
            }
        }

        private void btnUpdateClaimConditionTable_Click(object sender, RoutedEventArgs e)
        {
            int? selectedItemId = null;
            if (cbClaimCondition.SelectedItem != null)
            {
                selectedItemId = ((PersonalAccidentsClaimCondition)cbClaimCondition.SelectedItem).PersonalAccidentsClaimConditionID;
            }
            frmUpdateTable updateClaimConditionsTable = new frmUpdateTable(new PersonalAccidentsClaimCondition());
            updateClaimConditionsTable.ShowDialog();
            cbClaimConditionBinding();
            if (updateClaimConditionsTable.ItemAdded != null)
            {
                SelectConditionItemInCb(((PersonalAccidentsClaimCondition)updateClaimConditionsTable.ItemAdded).PersonalAccidentsClaimConditionID);
            }
            else if (selectedItemId != null)
            {
                SelectConditionItemInCb((int)selectedItemId);
            }

        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            if (nullErrorList != null && nullErrorList.Count > 0)
            {
                foreach (var item in nullErrorList)
                {
                    if (item is TextBox)
                    {
                        ((TextBox)item).ClearValue(BorderBrushProperty);
                    }
                    else if (item is ComboBox)
                    {
                        ((ComboBox)item).ClearValue(BorderBrushProperty);
                    }
                    else if (item is DatePicker)
                    {
                        ((DatePicker)item).ClearValue(BorderBrushProperty);
                    }
                }
            }
            nullErrorList = validation.InputNullValidation(new object[] { dpEventDate });
            if (nullErrorList.Count > 0)
            {
                cancelClose = true;
                tbItemGeneral.Focus();
                MessageBox.Show("נא למלאות שדות חובה", "", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (dpMoneyReceivedDate.SelectedDate != null && dpDeliveryDateToCompany.SelectedDate != null)
            {
                if (dpMoneyReceivedDate.SelectedDate < dpDeliveryDateToCompany.SelectedDate)
                {
                    cancelClose = true;
                    MessageBox.Show("תאריך משלוח חייב להיות קטן מתאריך קבלת הכסף", "", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
            }
            int? claimTypeId = null;
            if (cbClaimType.SelectedItem != null)
            {
                claimTypeId = ((PersonalAccidentsClaimType)cbClaimType.SelectedItem).PersonalAccidentsClaimTypeID;
            }
            int? claimConditionId = null;
            if (cbClaimCondition.SelectedItem != null)
            {
                claimConditionId = ((PersonalAccidentsClaimCondition)cbClaimCondition.SelectedItem).PersonalAccidentsClaimConditionID;
            }
            bool status = true;
            if (cbStatus.SelectedIndex == 1)
            {
                status = false;
            }
            DateTime? eventDateTime = null;
            if (tpEventHour.Value != null)
            {
                DateTime eventTime = Convert.ToDateTime(tpEventHour.Value.ToString());
                eventDateTime = new DateTime(((DateTime)dpEventDate.SelectedDate).Year, ((DateTime)dpEventDate.SelectedDate).Month, ((DateTime)dpEventDate.SelectedDate).Day, eventTime.Hour, eventTime.Minute, eventTime.Second, eventTime.Millisecond);
            }
            else
            {
                if (dpEventDate.SelectedDate != null)
                {
                    eventDateTime = (DateTime)dpEventDate.SelectedDate;
                }
            }

            FillEmptyTextBoxes(new TextBox[] { txtAmountClaimed, txtAmountReceived });
            try
            {
                int claimId = 0;
                if (personalAccidentsClaim == null)
                {
                    claimId = claimLogic.InsertPersonalAccidentsClaim(policy.PersonalAccidentsPolicyID, claimTypeId, claimConditionId, status, txtClaimNumber.Text, (DateTime)dpOpenDate.SelectedDate, dpDeliveryDateToCompany.SelectedDate, dpMoneyReceivedDate.SelectedDate, decimal.Parse(txtAmountClaimed.Text), decimal.Parse(txtAmountReceived.Text), eventDateTime, txtEventPlace.Text, txtEventDescription.Text);
                    if (personalAccidentsPaths.Count()>0)
                    {
                        path = personalAccidentsPaths[0] + @"/תביעה " + ((DateTime)dpEventDate.SelectedDate).ToString("dd-MM-yyyy");// + " " + policy.PersonalAccidentsIndustry.PersonalAccidentsIndustryName;
                    }
                }
                else
                {
                    claimId = personalAccidentsClaim.PersonalAccidentsClaimID;
                    claimLogic.UpdatePersonalAccidentsClaim(claimId, claimTypeId, claimConditionId, status, txtClaimNumber.Text, dpDeliveryDateToCompany.SelectedDate, dpMoneyReceivedDate.SelectedDate, decimal.Parse(txtAmountClaimed.Text), decimal.Parse(txtAmountReceived.Text), eventDateTime, txtEventPlace.Text, txtEventDescription.Text);
                    if (personalAccidentsPaths.Count() > 0)
                    {
                        newPath = personalAccidentsPaths[0] + @"/תביעה " + ((DateTime)dpEventDate.SelectedDate).ToString("dd-MM-yyyy");// + " " + personalAccidentsClaim.PersonalAccidentsPolicy.PersonalAccidentsIndustry.PersonalAccidentsIndustryName;
                        path = personalAccidentsPaths[0] + @"/תביעה " + ((DateTime)personalAccidentsClaim.EventDateAndTime).ToString("dd-MM-yyyy");// + " " + personalAccidentsClaim.PersonalAccidentsPolicy.PersonalAccidentsIndustry.PersonalAccidentsIndustryName;
                    }
                }
                SaveUnsavedWitness();

                if (witnesses.Count > 0)
                {
                    claimLogic.InsertWitnesses(witnesses, claimId);
                }
                if (trackings.Count > 0)
                {
                    trackingLogic.InsertPersonalAccidentsClaimTrackings(trackings, claimId, user.UserID);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "", MessageBoxButton.OK, MessageBoxImage.Error);
            }

            if (personalAccidentsClaim == null)
            {
                if (path != null && path != "")
                {
                    if (!Directory.Exists(@path))
                    {
                        Directory.CreateDirectory(@path);
                    }
                }
                else
                {
                    System.Windows.Forms.MessageBox.Show("לא ניתן ליצור את תיקיית התביעה");
                }
            }
            else
            {
                if (path != "")
                {
                    if (Directory.Exists(path) && newPath != "")
                    {
                        if (path != newPath)
                        {
                            try
                            {
                                Directory.Move(path, newPath);
                            }
                            catch (Exception)
                            {
                                System.Windows.Forms.MessageBox.Show("לא ניתן למצוא את נתיב התיקייה");
                            }
                        }
                    }
                }
                else
                {
                    if (newPath != "" && newPath != null)
                    {
                        Directory.CreateDirectory(newPath);
                    }
                }
            }

            if (sender is Button)
            {
                Button btn = (Button)sender;
                if (btn.Name == "btnSave")
                {
                    confirmBeforeClosing = false;
                    Close();
                }
            }
        }

        private void SaveUnsavedWitness()
        {
            if (txtWitnessName.Text != "" || txtWitnessAddress.Text != "" || txtWitnessPhone.Text != "" || txtWitnessCellPhone.Text != "")
            {
                btnAddWitness_Click(this, new RoutedEventArgs());
            }
        }

        public void FillEmptyTextBoxes(TextBox[] inputs)
        {
            foreach (var item in inputs)
            {
                if (item.Text == "")
                {
                    item.Text = "0";
                }
            }
        }

        private void txtAmountClaimed_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (sender is TextBox)
            {
                TextBox txtBox = (TextBox)sender;
                txtBox.ClearValue(BorderBrushProperty);
                if (txtBox.Text != "")
                {
                    try
                    {
                        decimal reusult = validations.ConvertStringToDecimal(txtBox.Text);
                    }
                    catch (Exception ex)
                    {
                        txtBox.BorderBrush = System.Windows.Media.Brushes.Red;
                        System.Windows.Forms.MessageBox.Show(ex.Message);
                        txtBox.Text = "";
                    }
                }
            }
        }

        private void dpEventDate_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            if (dpEventDate.SelectedDate != null)
            {
                tpEventHour.IsEnabled = true;
            }
            else
            {
                tpEventHour.IsEnabled = false;
                tpEventHour.Value = null;
            }
        }

        private void btnAddWitness_Click(object sender, RoutedEventArgs e)
        {
            if (witnessSelected == null)
            {
                PersonalAccidentsWitness witness = new PersonalAccidentsWitness() { WitnessName = txtWitnessName.Text, WittnessAddress = txtWitnessAddress.Text, WitnessPhone = txtWitnessPhone.Text, WitnessCellPhone = txtWitnessCellPhone.Text };
                witnesses.Add(witness);
            }
            else
            {
                witnessSelected.WitnessCellPhone = txtWitnessCellPhone.Text;
                witnessSelected.WitnessName = txtWitnessName.Text;
                witnessSelected.WitnessPhone = txtWitnessPhone.Text;
                witnessSelected.WittnessAddress = txtWitnessAddress.Text;
            }
            dgWitnessesBinding();
            dgWitnesses.UnselectAll();
            txtWitnessAddress.Clear();
            txtWitnessCellPhone.Clear();
            txtWitnessName.Clear();
            txtWitnessPhone.Clear();
        }

        private void dgWitnessesBinding()
        {
            dgWitnesses.ItemsSource = witnesses.ToList();
            lv.AutoSizeColumns(dgWitnesses.View);
        }

        private void dgWitnesses_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            witnessSelected = (PersonalAccidentsWitness)dgWitnesses.SelectedItem;
            if (witnessSelected != null)
            {
                txtWitnessName.Text = witnessSelected.WitnessName;
                txtWitnessAddress.Text = witnessSelected.WittnessAddress;
                txtWitnessPhone.Text = witnessSelected.WitnessPhone;
                txtWitnessCellPhone.Text = witnessSelected.WitnessCellPhone;
            }
        }

        private void dgWitnesses_MouseDown(object sender, MouseButtonEventArgs e)
        {
            dgWitnesses.UnselectAll();
            txtWitnessName.Clear();
            txtWitnessAddress.Clear();
            txtWitnessPhone.Clear();
            txtWitnessCellPhone.Clear();
        }

        private void dgTrackings_MouseDown(object sender, MouseButtonEventArgs e)
        {
            dgTrackings.UnselectAll();
        }

        private void btnNewTracking_Click(object sender, RoutedEventArgs e)
        {
            frmTracking newTrackingWindow = new frmTracking(client, policy, user);
            newTrackingWindow.ShowDialog();
            if (newTrackingWindow.trackingContent != null)
            {
                trackings.Add(new PersonalAccidentsClaimTracking { TrackingContent = newTrackingWindow.trackingContent, Date = DateTime.Now, User = user });
                dgTrackingsBinding();
            }
        }

        private void dgTrackingsBinding()
        {
            dgTrackings.ItemsSource = trackings.OrderByDescending(t => t.Date);
            lv.AutoSizeColumns(dgTrackings.View);
        }

        private void btnDeleteTracking_Click(object sender, RoutedEventArgs e)
        {
            if (dgTrackings.SelectedItem == null)
            {
                MessageBox.Show("נא לסמן מעקב בטבלה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            if (MessageBox.Show("למחוק מעקב", "אישור", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                if (((PersonalAccidentsClaimTracking)dgTrackings.SelectedItem).PersonalAccidentsClaimTrackingID != 0)
                {
                    trackingLogic.DeletePersonalAccidentsClaimTracking((PersonalAccidentsClaimTracking)dgTrackings.SelectedItem);
                }
                trackings.Remove((PersonalAccidentsClaimTracking)dgTrackings.SelectedItem);
                dgTrackingsBinding();
            }
            else
            {
                dgTrackings.UnselectAll();
            }
        }

        private void btnUpdateTracking_Click(object sender, RoutedEventArgs e)
        {
            if (dgTrackings.SelectedItem == null)
            {
                MessageBox.Show("נא לסמן מעקב בטבלה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            PersonalAccidentsClaimTracking trackingToUpdate = (PersonalAccidentsClaimTracking)dgTrackings.SelectedItem;
            frmTracking updateTrackingWindow = new frmTracking(trackingToUpdate, client, policy, user);
            updateTrackingWindow.ShowDialog();
            trackingToUpdate.TrackingContent = updateTrackingWindow.trackingContent;
            dgTrackingsBinding();
            dgTrackings.UnselectAll();
        }

        private void dgTrackings_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            DependencyObject src = (DependencyObject)(e.OriginalSource);
            while (!(src is Control))
            {
                if (!(src is Visual))
                {
                    break;
                }
                src = VisualTreeHelper.GetParent(src);
            }

            if ((src is Control && src.GetType() == typeof(ListViewItem)) || src.GetType() == typeof(Run))
            {
                if (dgTrackings.SelectedItem == null)
                {
                    PersonalAccidentsClaimTracking trackingToUpdate = (PersonalAccidentsClaimTracking)dgTrackings.SelectedItem;
                    frmTracking updateTrackingWindow = new frmTracking(trackingToUpdate, client, policy, user);
                    updateTrackingWindow.ShowDialog();
                    trackingToUpdate.TrackingContent = updateTrackingWindow.trackingContent;
                    dgTrackingsBinding();
                    dgTrackings.UnselectAll();
                }
            }
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void GroupBox_GotFocus(object sender, RoutedEventArgs e)
        {
            isChanges = true;
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (confirmBeforeClosing && isChanges)
            {
                if (MessageBox.Show("האם ברצונך לשמור שינוים שבצעת", "אישור", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    btnSave_Click(sender, new RoutedEventArgs());
                    if (cancelClose)
                    {
                        e.Cancel = true;
                        cancelClose = false;
                    }
                }
            }
        }
    }
}


