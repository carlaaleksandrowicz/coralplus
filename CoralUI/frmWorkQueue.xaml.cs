﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using CoralBusinessLogics;
using System.Globalization;

namespace CoralUI
{
    /// <summary>
    /// Interaction logic for frmWorkQueue.xaml
    /// </summary>
    public partial class frmWorkQueue : Window
    {
        Client client = null;
        User user = null;
        Request requestToUpdate = null;
        ElementaryPolicy elementaryPolicy = null;
        LifePolicy lifePolicy = null;
        WorkTasksLogic workTasksLogic = new WorkTasksLogic();
        InsurancesLogic insuranceLogic = new InsurancesLogic();
        PoliciesLogic policiyLogic = new PoliciesLogic();
        UserLogic usersLogic = new UserLogic();
        ClientsLogic clientLogic = new ClientsLogic();
        List<RequestTracking> trackings = new List<RequestTracking>();
        TrackingsLogics trackingLogic = new TrackingsLogics();
        //int requestId;
        Reminders remindersClass;
        public static event EventHandler RequestCountChange;
        LifePoliciesLogic lifePoliciyLogic = new LifePoliciesLogic();
        HealthPolicy healthPolicy = null;
        HealthPoliciesLogic healthPoliciyLogic = new HealthPoliciesLogic();
        ListViewSettings lv = new ListViewSettings();
        FinancePolicy financePolicy = null;
        FinancePoliciesLogic financePolicyLogic = new FinancePoliciesLogic();
        PersonalAccidentsPolicy personalAccidentsPolicy = null;
        PersonalAccidentsLogic personalAccidentsLogic = new PersonalAccidentsLogic();
        TravelPolicy travelPolicy = null;
        TravelLogic travelLogic = new TravelLogic();
        ForeingWorkersPolicy foreingWorkersPolicy = null;
        ForeingWorkersLogic foreingWorkersLogic = new ForeingWorkersLogic();
        bool confirmBeforeClosing = true;
        bool cancelClose = false;
        private bool isChanges = false;
        public int RequestID { get; set; }

        public frmWorkQueue()
        {
            InitializeComponent();
        }
        public frmWorkQueue(Client clientMessage, User userWorkQueue, object policy)
        {
            InitializeComponent();
            client = clientMessage;
            user = userWorkQueue;
            if (policy is ElementaryPolicy)
            {
                elementaryPolicy = (ElementaryPolicy)policy;
            }
            else if (policy is LifePolicy)
            {
                lifePolicy = (LifePolicy)policy;
            }
            else if (policy is HealthPolicy)
            {
                healthPolicy = (HealthPolicy)policy;
            }
            else if (policy is FinancePolicy)
            {
                financePolicy = (FinancePolicy)policy;
            }
            else if (policy is PersonalAccidentsPolicy)
            {
                personalAccidentsPolicy = (PersonalAccidentsPolicy)policy;
            }
            else if (policy is TravelPolicy)
            {
                travelPolicy = (TravelPolicy)policy;
            }
            else if (policy is ForeingWorkersPolicy)
            {
                foreingWorkersPolicy = (ForeingWorkersPolicy)policy;
            }
        }
        public frmWorkQueue(Client clientMessage, User userWorkQueue, object policy, string trackingContent)
        {
            InitializeComponent();
            client = clientMessage;
            user = userWorkQueue;
            if (policy is ElementaryPolicy)
            {
                elementaryPolicy = (ElementaryPolicy)policy;
            }
            else if (policy is LifePolicy)
            {
                lifePolicy = (LifePolicy)policy;
            }
            else if (policy is HealthPolicy)
            {
                healthPolicy = (HealthPolicy)policy;
            }
            else if (policy is FinancePolicy)
            {
                financePolicy = (FinancePolicy)policy;
            }
            else if (policy is PersonalAccidentsPolicy)
            {
                personalAccidentsPolicy = (PersonalAccidentsPolicy)policy;
            }
            else if (policy is TravelPolicy)
            {
                travelPolicy = (TravelPolicy)policy;
            }
            else if (policy is ForeingWorkersPolicy)
            {
                foreingWorkersPolicy = (ForeingWorkersPolicy)policy;
            }
            txtContactDescription.Text = trackingContent;
        }

        public frmWorkQueue(Request request)
        {
            InitializeComponent();
            requestToUpdate = request;
            client = requestToUpdate.Client;
            user = requestToUpdate.User1;
            if (requestToUpdate.ElementaryPolicy != null)
            {
                elementaryPolicy = requestToUpdate.ElementaryPolicy;
            }
            else if (requestToUpdate.LifePolicy != null)
            {
                lifePolicy = requestToUpdate.LifePolicy;
            }
            else if (requestToUpdate.HealthPolicy is HealthPolicy)
            {
                healthPolicy = requestToUpdate.HealthPolicy;
            }
            else if (requestToUpdate.FinancePolicy is FinancePolicy)
            {
                financePolicy = requestToUpdate.FinancePolicy;
            }
            else if (requestToUpdate.PersonalAccidentsPolicy != null)
            {
                personalAccidentsPolicy = requestToUpdate.PersonalAccidentsPolicy;
            }
            else if (requestToUpdate.TravelPolicy != null)
            {
                travelPolicy = requestToUpdate.TravelPolicy;
            }
            else if (requestToUpdate.ForeingWorkersPolicy != null)
            {
                foreingWorkersPolicy = requestToUpdate.ForeingWorkersPolicy;
            }

        }
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            cbClientsBinding();
            dpContactDate.SelectedDate = DateTime.Now;
            cbTaskCategoryBinding();
            cbPolicyNumberBinding();
            usersBinding();
            var users = cbOpenBy.Items;
            foreach (var item in users)
            {
                User userToSelect = (User)item;
                if (userToSelect.UserID == user.UserID)
                {
                    cbOpenBy.SelectedItem = item;
                    cbUserInCharge.SelectedItem = item;
                    break;
                }
            }
            if (elementaryPolicy != null)
            {
                var cbItems = cbPolicyNumber.Items;
                foreach (var item in cbItems)
                {
                    ElementaryPolicy cbPolicy = (ElementaryPolicy)item;
                    if (cbPolicy.PolicyNumber == elementaryPolicy.PolicyNumber)
                    {
                        cbPolicyNumber.SelectedItem = item;
                        break;
                    }
                }
            }
            else if (lifePolicy != null)
            {
                var cbItems = cbPolicyNumber.Items;
                foreach (var item in cbItems)
                {
                    LifePolicy cbPolicy = (LifePolicy)item;
                    if (cbPolicy.PolicyNumber == lifePolicy.PolicyNumber)
                    {
                        cbPolicyNumber.SelectedItem = item;
                        break;
                    }
                }
            }
            else if (healthPolicy != null)
            {
                var cbItems = cbPolicyNumber.Items;
                foreach (var item in cbItems)
                {
                    HealthPolicy cbPolicy = (HealthPolicy)item;
                    if (cbPolicy.PolicyNumber == healthPolicy.PolicyNumber)
                    {
                        cbPolicyNumber.SelectedItem = item;
                        break;
                    }
                }
            }
            else if (financePolicy != null)
            {
                var cbItems = cbPolicyNumber.Items;
                foreach (var item in cbItems)
                {
                    FinancePolicy cbPolicy = (FinancePolicy)item;
                    if (cbPolicy.AssociateNumber == financePolicy.AssociateNumber)
                    {
                        cbPolicyNumber.SelectedItem = item;
                        break;
                    }
                }
            }
            else if (personalAccidentsPolicy != null)
            {
                var cbItems = cbPolicyNumber.Items;
                foreach (var item in cbItems)
                {
                    PersonalAccidentsPolicy cbPolicy = (PersonalAccidentsPolicy)item;
                    if (cbPolicy.PolicyNumber == personalAccidentsPolicy.PolicyNumber)
                    {
                        cbPolicyNumber.SelectedItem = item;
                        break;
                    }
                }
            }
            else if (travelPolicy != null)
            {
                var cbItems = cbPolicyNumber.Items;
                foreach (var item in cbItems)
                {
                    TravelPolicy cbPolicy = (TravelPolicy)item;
                    if (cbPolicy.PolicyNumber == travelPolicy.PolicyNumber)
                    {
                        cbPolicyNumber.SelectedItem = item;
                        break;
                    }
                }
            }
            else if (foreingWorkersPolicy != null)
            {
                var cbItems = cbPolicyNumber.Items;
                foreach (var item in cbItems)
                {
                    ForeingWorkersPolicy cbPolicy = (ForeingWorkersPolicy)item;
                    if (cbPolicy.PolicyNumber == foreingWorkersPolicy.PolicyNumber)
                    {
                        cbPolicyNumber.SelectedItem = item;
                        break;
                    }
                }
            }
            if (client == null)
            {
                btnAttach.IsEnabled = false;
                lblClientNameAndId.Content = "";
            }
            else
            {
                string clientDetails;
                if (client.CellPhone != null && client.CellPhone != "")
                {
                    clientDetails = string.Format("{0} {1}  ת.ז: {2} נייד: {3}", client.FirstName, client.LastName, client.IdNumber, client.CellPhone);
                }
                else
                {
                    clientDetails = string.Format("{0} {1}  ת.ז: {2}", client.FirstName, client.LastName, client.IdNumber);
                }
                lblClientNameAndId.Content = clientDetails;
                var cbClientsItems = cbClientName.Items;
                foreach (var item in cbClientsItems)
                {
                    Client clientItem = (Client)item;
                    if (clientItem.ClientID == client.ClientID)
                    {
                        cbClientName.SelectedItem = item;
                        cbClientName.IsEnabled = false;
                        break;
                    }
                }

                //lblNameSpace.Content = client.FirstName + " " + client.LastName;
                //lblIdSpace.Content = client.IdNumber;
                //lblPhoneSpace.Content = client.PhoneHome;
                //lblCellPhoneSpace.Content = client.CellPhone;
                //lblEmailSpace.Content = client.Email;
            }
            if (requestToUpdate == null)
            {
                dpOpenDate.SelectedDate = DateTime.Now;
                cbTaskStatus.SelectedIndex = 0;
            }
            else
            {
                dpOpenDate.SelectedDate = requestToUpdate.OpenDate;

                if (requestToUpdate.RequestCategoryID != null)
                {
                    SelectItemInCb((int)requestToUpdate.RequestCategoryID);
                }
                if (requestToUpdate.RequestStatus == true)
                {
                    cbTaskStatus.SelectedIndex = 0;
                }
                else
                {
                    cbTaskStatus.SelectedIndex = 1;
                }

                if (requestToUpdate.RequestConditionID != null)
                {
                    SelectConditionItemInCb((int)requestToUpdate.RequestConditionID);
                }
                foreach (var item in users)
                {
                    User userToSelect = (User)item;
                    if (userToSelect.UserID == requestToUpdate.HandledByUserID)
                    {
                        cbUserInCharge.SelectedItem = item;
                        //break;
                    }
                    if (userToSelect.UserID==requestToUpdate.OpenedByUserID)
                    {
                        cbOpenBy.SelectedItem = item;
                    }
                }
                chbIsTheRequesterTheClient.IsChecked = requestToUpdate.IsRequesterClient;
                txtRequesterLastName.Text = requestToUpdate.RequesterLastName;
                txtRequesterFirstName.Text = requestToUpdate.RequesterFirstName;
                txtRequesterId.Text = requestToUpdate.RequesterIdNumber;
                txtRequesterPhone.Text = requestToUpdate.RequesterPhone;
                txtRequesterCellPhone.Text = requestToUpdate.RequesterCellPhone;
                txtRequesterFax.Text = requestToUpdate.RequesterFax;
                txtRequesterEmail.Text = requestToUpdate.RequesterEmail;
                txtRequesterAddress.Text = requestToUpdate.RequesterAddress;
                dpContactDate.SelectedDate = requestToUpdate.RequesteDate;
                if (requestToUpdate.RequestHour != null)
                {
                    tpContactHour.Value = Convert.ToDateTime(requestToUpdate.RequestHour.ToString());
                }
                dpDueDate.SelectedDate = requestToUpdate.RequestDueDate;
                dpClosingDate.SelectedDate = requestToUpdate.RequestClosingDate;
                txtSubject.Text = requestToUpdate.RequestSubject;
                txtContactDescription.Text = requestToUpdate.RequestDescription;
                txtHandlerLastName.Text = requestToUpdate.HandlerLastName;
                txtHandlerFirstName.Text = requestToUpdate.HandlerFirstName;
                txtHandlerCompany.Text = requestToUpdate.HandlerCompanyName;
                txtHandlerPhone.Text = requestToUpdate.HandlerPhone;
                txtHandlerCellPhone.Text = requestToUpdate.HandlerCellPhone;
                txtHandlerFax.Text = requestToUpdate.HandlerFax;
                txtHandlerEmail.Text = requestToUpdate.HandlerEmail;
                txtHandlerAddress.Text = requestToUpdate.HandlerAddress;
                txtHandlerZipCode.Text = requestToUpdate.HandlerZipCode;
                dpHandlingOpenDate.SelectedDate = requestToUpdate.HandlerOpenDate;
                dpHandlingClosingDate.SelectedDate = requestToUpdate.HandlerClosingDate;
                DateTime? requestDateTime = null;
                if (requestToUpdate.IsReminder == true)
                {
                    //if (requestToUpdate.ReminderHour != null)
                    //{
                    DateTime eventTime = Convert.ToDateTime(requestToUpdate.ReminderHour.ToString());
                    requestDateTime = new DateTime(((DateTime)requestToUpdate.ReminderDate).Year, ((DateTime)requestToUpdate.ReminderDate).Month, ((DateTime)requestToUpdate.ReminderDate).Day, eventTime.Hour, eventTime.Minute, eventTime.Second, eventTime.Millisecond);
                    //}
                    //else
                    //{
                    //    if (dpReminderDate.SelectedDate != null)
                    //    {
                    //        requestDateTime = (DateTime)dpReminderDate.SelectedDate;
                    //    }
                    //}
                    if (requestDateTime > DateTime.Now)
                    {
                        chbIsReminder.IsChecked = requestToUpdate.IsReminder;
                        dpReminderDate.SelectedDate = requestToUpdate.ReminderDate;
                        if (requestToUpdate.ReminderHour != null)
                        {
                            tpReminderTime.Value = Convert.ToDateTime(requestToUpdate.ReminderHour.ToString());
                        }
                    }
                }
                trackings = trackingLogic.GetAllRequestTrackingsByRequestID(requestToUpdate.RequestID);
                dgTrackingsBinding();
            }
        }

        private void cbClientName_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            client = (Client)cbClientName.SelectedItem;
            txtClientId.Text = client.IdNumber;
            if (client.CellPhone != null)
            {
                txtClientPhone.Text = client.CellPhone;
            }
            else if (client.PhoneHome != null)
            {
                txtClientPhone.Text = client.PhoneHome;
            }
            else
            {
                txtClientPhone.Text = client.PhoneWork;
            }
            txtClientEmail.Text = client.Email;
        }
        private void cbClientsBinding()
        {
            var clients = clientLogic.GetAllClients();
            cbClientName.ItemsSource = clients.OrderBy(c => c.LastName);
        }
        private void usersBinding()
        {
            var users = usersLogic.GetActiveUsers();
            cbOpenBy.ItemsSource = users;
            cbOpenBy.DisplayMemberPath = "Usermame";
            cbUserInCharge.ItemsSource = users;
            cbUserInCharge.DisplayMemberPath = "Usermame";
        }

        private void cbPolicyNumberBinding()
        {
            if (elementaryPolicy != null)
            {
                cbPolicyNumber.ItemsSource = policiyLogic.GetAllActiveElementaryPoliciesByClient(client);
            }
            else if (lifePolicy != null)
            {
                cbPolicyNumber.ItemsSource = lifePoliciyLogic.GetAllLifePoliciesByClient(client);
            }
            else if (healthPolicy != null)
            {
                cbPolicyNumber.ItemsSource = healthPoliciyLogic.GetAllHealthPoliciesByClient(client);
            }
            else if (financePolicy != null)
            {
                cbPolicyNumber.ItemsSource = financePolicyLogic.GetAllFinancePoliciesByClient(client);
            }
            else if (personalAccidentsPolicy != null)
            {
                cbPolicyNumber.ItemsSource = personalAccidentsLogic.GetAllPersonalAccidentsPoliciesByClient(client);
            }
            else if (travelPolicy != null)
            {
                cbPolicyNumber.ItemsSource = travelLogic.GetAllTravelPoliciesByClient(client);
            }
            else if (foreingWorkersPolicy != null)
            {
                cbPolicyNumber.ItemsSource = foreingWorkersLogic.GetAllForeingWorkersPoliciesByClient(client);
            }
        }

        private void cbTaskCategoryBinding()
        {
            int? insuranceId = null;
            if (elementaryPolicy != null)
            {
                insuranceId = insuranceLogic.GetInsuranceID("אלמנטרי");
            }
            else if (lifePolicy != null)
            {
                insuranceId = insuranceLogic.GetInsuranceID("חיים");
            }
            else if (healthPolicy != null)
            {
                insuranceId = insuranceLogic.GetInsuranceID("בריאות");
            }
            else if (financePolicy != null)
            {
                insuranceId = insuranceLogic.GetInsuranceID("פיננסים");
            }
            else if (personalAccidentsPolicy != null)
            {
                insuranceId = insuranceLogic.GetInsuranceID("תאונות אישיות");
            }
            else if (travelPolicy != null)
            {
                insuranceId = insuranceLogic.GetInsuranceID("נסיעות לחו''ל");
            }
            else if (foreingWorkersPolicy != null)
            {
                insuranceId = insuranceLogic.GetInsuranceID("עובדים זרים");
            }
            cbTaskCategory.ItemsSource = workTasksLogic.GetActiveRequestCategoriesByInsuranceId(insuranceId);
            cbTaskCategory.DisplayMemberPath = "RequestCategoryName";
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            dpDueDate.ClearValue(BorderBrushProperty);
            txtSubject.ClearValue(BorderBrushProperty);
            if (txtSubject.Text == "")
            {
                txtSubject.BorderBrush = Brushes.Red;
                MessageBox.Show("נא להזין נושא הפניה", "שדה חובה", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                cancelClose = true;
                return;
            }
            if (dpDueDate.SelectedDate == null)
            {
                dpDueDate.BorderBrush = Brushes.Red;
                MessageBox.Show("נא להזין תאריך יעד", "שדה חובה", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                cancelClose = true;
                return;
            }
            if (chbIsReminder.IsChecked == true)
            {
                if (dpReminderDate.SelectedDate == null || tpReminderTime.Value == null)
                {
                    MessageBox.Show("נא להזין תאריך ושעת התזכורת", "שדה חובה", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    cancelClose = true;
                    return;
                }
                DateTime reminderResetTime = Convert.ToDateTime(tpReminderTime.Value.ToString());
                DateTime reminderDateTime = new DateTime(((DateTime)dpReminderDate.SelectedDate).Year, ((DateTime)dpReminderDate.SelectedDate).Month, ((DateTime)dpReminderDate.SelectedDate).Day, reminderResetTime.Hour, reminderResetTime.Minute, reminderResetTime.Second, reminderResetTime.Millisecond);
                if (reminderDateTime <= DateTime.Now)
                {
                    MessageBox.Show("נא להזין זמן גדול מעכשיו", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    cancelClose = true;
                    return;
                }
            }

            int? clientId = null;
            if (client != null)
            {
                clientId = client.ClientID;
            }
            int? categoryId = null;
            if (cbTaskCategory.SelectedItem != null)
            {
                categoryId = ((RequestCategory)cbTaskCategory.SelectedItem).RequestCategoryID;
            }
            int? elementaryPolicyId = null;
            if (cbPolicyNumber.SelectedItem is ElementaryPolicy)
            {
                elementaryPolicyId = ((ElementaryPolicy)cbPolicyNumber.SelectedItem).ElementaryPolicyID;
            }
            int? lifePolicyId = null;
            if (cbPolicyNumber.SelectedItem is LifePolicy)
            {
                lifePolicyId = ((LifePolicy)cbPolicyNumber.SelectedItem).LifePolicyID;
            }
            int? healthPolicyId = null;
            if (cbPolicyNumber.SelectedItem is HealthPolicy)
            {
                healthPolicyId = ((HealthPolicy)cbPolicyNumber.SelectedItem).HealthPolicyID;
            }
            int? financePolicyId = null;
            if (cbPolicyNumber.SelectedItem is FinancePolicy)
            {
                financePolicyId = ((FinancePolicy)cbPolicyNumber.SelectedItem).FinancePolicyID;
            }
            int? personalAccidentsPolicyId = null;
            if (cbPolicyNumber.SelectedItem is PersonalAccidentsPolicy)
            {
                personalAccidentsPolicyId = ((PersonalAccidentsPolicy)cbPolicyNumber.SelectedItem).PersonalAccidentsPolicyID;
            }
            int? travelPolicyId = null;
            if (cbPolicyNumber.SelectedItem is TravelPolicy)
            {
                travelPolicyId = ((TravelPolicy)cbPolicyNumber.SelectedItem).TravelPolicyID;
            }
            int? foreingWorkersPolicyId = null;
            if (cbPolicyNumber.SelectedItem is ForeingWorkersPolicy)
            {
                foreingWorkersPolicyId = ((ForeingWorkersPolicy)cbPolicyNumber.SelectedItem).ForeingWorkersPolicyID;
            }
            bool? status = true;
            if (cbTaskStatus.SelectedIndex == 1)
            {
                status = false;
            }
            int? conditionId = null;
            if (cbTaskCondition.SelectedItem != null)
            {
                conditionId = ((RequestCondition)cbTaskCondition.SelectedItem).RequestConditionID;
            }
            int? openById = null;
            if (cbOpenBy.SelectedItem != null)
            {
                openById = ((User)cbOpenBy.SelectedItem).UserID;
            }
            int? handledById = null;
            if (cbUserInCharge.SelectedItem != null)
            {
                handledById = ((User)cbUserInCharge.SelectedItem).UserID;
            }
            TimeSpan? contactTime = null;

            if (tpContactHour.Value != null)
            {
                contactTime = ((DateTime)tpContactHour.Value).TimeOfDay;
            }
            TimeSpan? reminderTime = null;

            if (tpReminderTime.Value != null)
            {
                reminderTime = ((DateTime)tpReminderTime.Value).TimeOfDay;
            }
            DateTime openDate = (DateTime)dpOpenDate.SelectedDate;
            try
            {
                if (requestToUpdate == null)
                {
                    RequestID = workTasksLogic.InsertRequest(clientId, openDate, categoryId, elementaryPolicyId, lifePolicyId, healthPolicyId, financePolicyId, personalAccidentsPolicyId, travelPolicyId, foreingWorkersPolicyId, status, conditionId, openById, handledById, chbIsTheRequesterTheClient.IsChecked, txtRequesterLastName.Text, txtRequesterFirstName.Text, txtRequesterId.Text, txtRequesterPhone.Text, txtRequesterCellPhone.Text, txtRequesterFax.Text, txtRequesterEmail.Text, txtRequesterAddress.Text, dpContactDate.SelectedDate, contactTime, txtSubject.Text, txtContactDescription.Text, dpDueDate.SelectedDate, dpClosingDate.SelectedDate, txtHandlerCompany.Text, txtHandlerLastName.Text, txtHandlerFirstName.Text, txtHandlerPhone.Text, txtHandlerCellPhone.Text, txtHandlerFax.Text, txtHandlerEmail.Text, txtHandlerAddress.Text, txtHandlerZipCode.Text, dpHandlingOpenDate.SelectedDate, dpHandlingClosingDate.SelectedDate, chbIsReminder.IsChecked, dpReminderDate.SelectedDate, reminderTime);
                    if (chbIsReminder.IsChecked == true)
                    {
                        remindersClass = new Reminders(user);
                        remindersClass.AddReminderTimer(RequestID, (DateTime)dpReminderDate.SelectedDate, (TimeSpan)reminderTime);
                    }
                }
                else
                {
                    RequestID = workTasksLogic.UpdateRequest(requestToUpdate.RequestID, clientId, categoryId, elementaryPolicyId, lifePolicyId, healthPolicyId, financePolicyId, personalAccidentsPolicyId, travelPolicyId, foreingWorkersPolicyId, status, conditionId, openById, handledById, chbIsTheRequesterTheClient.IsChecked, txtRequesterLastName.Text, txtRequesterFirstName.Text, txtRequesterId.Text, txtRequesterPhone.Text, txtRequesterCellPhone.Text, txtRequesterFax.Text, txtRequesterEmail.Text, txtRequesterAddress.Text, dpContactDate.SelectedDate, contactTime, txtSubject.Text, txtContactDescription.Text, dpDueDate.SelectedDate, dpClosingDate.SelectedDate, txtHandlerCompany.Text, txtHandlerLastName.Text, txtHandlerFirstName.Text, txtHandlerPhone.Text, txtHandlerCellPhone.Text, txtHandlerFax.Text, txtHandlerEmail.Text, txtHandlerAddress.Text, txtHandlerZipCode.Text, dpHandlingOpenDate.SelectedDate, dpHandlingClosingDate.SelectedDate, chbIsReminder.IsChecked, dpReminderDate.SelectedDate, reminderTime);
                    if (chbIsReminder.IsChecked == true && (requestToUpdate.ReminderDate != dpReminderDate.SelectedDate || requestToUpdate.ReminderHour != reminderTime))
                    {
                        remindersClass = new Reminders(user);
                        remindersClass.UpdateReminderTimer(RequestID, (DateTime)dpReminderDate.SelectedDate, (TimeSpan)reminderTime);
                    }
                }
                if (trackings.Count > 0)
                {
                    trackingLogic.InsertRequestTrackings(trackings, RequestID, user.UserID);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
            finally
            {
                OnRequestCountChange();
            }

            if (sender is Button)
            {
                Button btn = (Button)sender;
                if (btn.Name == "btnSave")
                {
                    confirmBeforeClosing = false;
                    Close();
                }
            }
        }

        private void OnRequestCountChange()
        {
            if (RequestCountChange != null)
            {
                RequestCountChange(this, new EventArgs());
            }
        }

        private void btnUpdateTaskCategoryTable_Click(object sender, RoutedEventArgs e)
        {
            int? insuranceID = null;
            if (elementaryPolicy != null)
            {
                insuranceID = insuranceLogic.GetInsuranceID("אלמנטרי");
            }
            else if (lifePolicy != null)
            {
                insuranceID = insuranceLogic.GetInsuranceID("חיים");
            }
            else if (healthPolicy != null)
            {
                insuranceID = insuranceLogic.GetInsuranceID("בריאות");
            }
            else if (financePolicy != null)
            {
                insuranceID = insuranceLogic.GetInsuranceID("פיננסים");
            }
            else if (personalAccidentsPolicy != null)
            {
                insuranceID = insuranceLogic.GetInsuranceID("תאונות אישיות");
            }
            else if (travelPolicy != null)
            {
                insuranceID = insuranceLogic.GetInsuranceID("נסיעות לחו''ל");
            }
            else if (foreingWorkersPolicy != null)
            {
                insuranceID = insuranceLogic.GetInsuranceID("עובדים זרים");
            }
            int? selectedItemId = null;
            if (cbTaskCategory.SelectedItem != null)
            {
                selectedItemId = ((RequestCategory)cbTaskCategory.SelectedItem).RequestCategoryID;
            }
            RequestCategory category = new RequestCategory() { InsuranceID = insuranceID };
            frmUpdateTable updateCategoriesTable = new frmUpdateTable(category);
            updateCategoriesTable.ShowDialog();
            cbTaskCategoryBinding();
            if (updateCategoriesTable.ItemAdded != null)
            {
                SelectItemInCb(((RequestCategory)updateCategoriesTable.ItemAdded).RequestCategoryID);
            }
            else if (selectedItemId != null)
            {
                SelectItemInCb((int)selectedItemId);
            }
        }

        private void SelectItemInCb(int id)
        {
            var items = cbTaskCategory.Items;
            foreach (var item in items)
            {
                RequestCategory parsedItem = (RequestCategory)item;
                if (parsedItem.RequestCategoryID == id)
                {
                    cbTaskCategory.SelectedItem = item;
                    break;
                }
            }
        }

        private void btnUpdateTaskConditionTable_Click(object sender, RoutedEventArgs e)
        {
            if (cbTaskCategory.SelectedItem == null)
            {
                MessageBox.Show("נא לבחור קטגוריית המשימה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            int? selectedItemId = null;
            if (cbTaskCondition.SelectedItem != null)
            {
                selectedItemId = ((RequestCondition)cbTaskCondition.SelectedItem).RequestConditionID;
            }
            int categoryId = ((RequestCategory)cbTaskCategory.SelectedItem).RequestCategoryID;
            RequestCondition condition = new RequestCondition() { RequestCategoryID = categoryId };
            frmUpdateTable updateConditionsTable = new frmUpdateTable(condition);
            updateConditionsTable.ShowDialog();
            cbTaskConditionBinding();
            if (updateConditionsTable.ItemAdded != null)
            {
                SelectConditionItemInCb(((RequestCondition)updateConditionsTable.ItemAdded).RequestConditionID);
            }
            else if (selectedItemId != null)
            {
                SelectConditionItemInCb((int)selectedItemId);
            }
        }

        private void SelectConditionItemInCb(int id)
        {
            var items = cbTaskCondition.Items;
            foreach (var item in items)
            {
                RequestCondition parsedItem = (RequestCondition)item;
                if (parsedItem.RequestConditionID == id)
                {
                    cbTaskCondition.SelectedItem = item;
                    break;
                }
            }
        }

        private void cbTaskConditionBinding()
        {
            if (cbTaskCategory.SelectedItem != null)
            {
                cbTaskCondition.ItemsSource = workTasksLogic.GetActiveRequestConditionsByCategory(((RequestCategory)cbTaskCategory.SelectedItem).RequestCategoryID);
                cbTaskCondition.DisplayMemberPath = "RequestConditionName";
            }
            else
            {
                cbTaskCondition.ItemsSource = null;
            }
        }

        private void chbIsTheRequesterTheClient_Checked(object sender, RoutedEventArgs e)
        {
            if (cbClientName.SelectedItem == null)
            {
                MessageBox.Show("נא לבחור לקוח מרשימת הלקוחות", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                (sender as CheckBox).IsChecked = false;
                return;
            }
            txtRequesterLastName.Text = client.LastName;
            txtRequesterFirstName.Text = client.FirstName;
            txtRequesterId.Text = client.IdNumber;
            txtRequesterPhone.Text = client.PhoneHome;
            txtRequesterCellPhone.Text = client.CellPhone;
            txtRequesterFax.Text = client.Fax;
            txtRequesterEmail.Text = client.Email;
            txtRequesterAddress.Text = client.Street + " " + client.HomeNumber + ", " + client.City;
        }

        private void chbIsTheRequesterTheClient_Unchecked(object sender, RoutedEventArgs e)
        {
            txtRequesterLastName.Clear();
            txtRequesterFirstName.Clear();
            txtRequesterId.Clear();
            txtRequesterPhone.Clear();
            txtRequesterCellPhone.Clear();
            txtRequesterFax.Clear();
            txtRequesterEmail.Clear();
            txtRequesterAddress.Clear();
        }

        private void cbPolicyNumber_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cbPolicyNumber.SelectedItem is ElementaryPolicy)
            {
                txtInsuranceType.Text = ((ElementaryPolicy)cbPolicyNumber.SelectedItem).ElementaryInsuranceType.ElementaryInsuranceTypeName;
            }
            else if (cbPolicyNumber.SelectedItem is LifePolicy)
            {
                txtInsuranceType.Text = ((LifePolicy)cbPolicyNumber.SelectedItem).FundType.FundTypeName;
            }
            else if (cbPolicyNumber.SelectedItem is HealthPolicy)
            {
                txtInsuranceType.Text = ((HealthPolicy)cbPolicyNumber.SelectedItem).HealthInsuranceType.HealthInsuranceTypeName;
            }
            else if (cbPolicyNumber.SelectedItem is FinancePolicy)
            {
                txtInsuranceType.Text = ((FinancePolicy)cbPolicyNumber.SelectedItem).FinanceProgram.ProgramName;
            }
            else if (cbPolicyNumber.SelectedItem is ForeingWorkersPolicy)
            {
                txtInsuranceType.Text = ((ForeingWorkersPolicy)cbPolicyNumber.SelectedItem).ForeingWorkersInsuranceType.ForeingWorkersInsuranceTypeName;
            }
            //else if (cbPolicyNumber.SelectedItem is PersonalAccidentsPolicy)
            //{
            //    txtInsuranceType.Text = ((PersonalAccidentsPolicy)cbPolicyNumber.SelectedItem).FinanceProgram.ProgramName;
            //}
        }

        private void cbTaskCondition_DropDownOpened(object sender, EventArgs e)
        {
            if (cbTaskCategory.SelectedItem == null)
            {
                MessageBox.Show("נא לבחור קטגוריית המשימה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
        }

        private void cbTaskCategory_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            cbTaskConditionBinding();
        }

        private void btnAttach_Click(object sender, RoutedEventArgs e)
        {
            frmScan2 archiveWindow = new frmScan2(client, cbPolicyNumber.SelectedItem, null, user);
            archiveWindow.ShowDialog();
        }

        private void chbIsReminder_Checked(object sender, RoutedEventArgs e)
        {
            dpReminderDate.IsEnabled = true;
            tpReminderTime.IsEnabled = true;
        }

        private void chbIsReminder_Unchecked(object sender, RoutedEventArgs e)
        {
            dpReminderDate.IsEnabled = false;
            dpReminderDate.SelectedDate = null;
            tpReminderTime.IsEnabled = false;
            tpReminderTime.Value = null;
        }

        private void btnNewTracking_Click(object sender, RoutedEventArgs e)
        {
            frmTracking newTrackingWindow = new frmTracking(true);
            newTrackingWindow.ShowDialog();
            if (newTrackingWindow.trackingContent != null)
            {
                trackings.Add(new RequestTracking { TrackingContent = newTrackingWindow.trackingContent, Date = DateTime.Now, User = user });
                dgTrackingsBinding();
            }
        }

        private void dgTrackingsBinding()
        {
            dgTrackings.ItemsSource = trackings.OrderByDescending(t => t.Date);
            lv.AutoSizeColumns(dgTrackings.View);
        }

        private void btnDeleteTracking_Click(object sender, RoutedEventArgs e)
        {
            if (dgTrackings.SelectedItem == null)
            {
                MessageBox.Show("נא לסמן מעקב בטבלה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            if (MessageBox.Show("למחוק מעקב", "אישור", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                if (((RequestTracking)dgTrackings.SelectedItem).RequestTrackingID != 0)
                {
                    trackingLogic.DeleteRequestTracking((RequestTracking)dgTrackings.SelectedItem);
                }
                trackings.Remove((RequestTracking)dgTrackings.SelectedItem);
                dgTrackingsBinding();
            }
            else
            {
                dgTrackings.UnselectAll();
            }
        }

        private void dgTrackings_MouseDown(object sender, MouseButtonEventArgs e)
        {
            dgTrackings.UnselectAll();
        }

        private void btnUpdateTracking_Click(object sender, RoutedEventArgs e)
        {
            if (dgTrackings.SelectedItem == null)
            {
                MessageBox.Show("נא לסמן מעקב בטבלה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            RequestTracking trackingToUpdate = (RequestTracking)dgTrackings.SelectedItem;
            frmTracking updateTrackingWindow = new frmTracking(trackingToUpdate);
            updateTrackingWindow.ShowDialog();
            trackingToUpdate.TrackingContent = updateTrackingWindow.trackingContent;
            dgTrackingsBinding();
            dgTrackings.UnselectAll();
        }

        private void dgTrackings_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            DependencyObject src = (DependencyObject)(e.OriginalSource);
            while (!(src is Control))
            {
                if (!(src is Visual))
                {
                    break;
                }
                src = VisualTreeHelper.GetParent(src);
            }
            if ((src is Control && src.GetType() == typeof(ListViewItem)) || src.GetType() == typeof(Run))
            {
                if (dgTrackings.SelectedItem != null)
                {
                    RequestTracking trackingToUpdate = (RequestTracking)dgTrackings.SelectedItem;
                    frmTracking updateTrackingWindow = new frmTracking(trackingToUpdate);
                    updateTrackingWindow.ShowDialog();
                    trackingToUpdate.TrackingContent = updateTrackingWindow.trackingContent;
                    dgTrackingsBinding();
                    dgTrackings.UnselectAll();
                }
            }
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (confirmBeforeClosing&&isChanges)
            {
                if (MessageBox.Show("האם ברצונך לשמור שינוים שבצעת", "אישור", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    btnSave_Click(sender, new RoutedEventArgs());
                    if (cancelClose)
                    {
                        e.Cancel = true;
                        cancelClose = false;
                    }
                }
            }
        }

        
        private void txtRequesterLastName_GotFocus(object sender, RoutedEventArgs e)
        {
            isChanges = true;
        }

       
    }
}
