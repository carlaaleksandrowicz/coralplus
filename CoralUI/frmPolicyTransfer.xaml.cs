﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using CoralBusinessLogics;

namespace CoralUI
{
    /// <summary>
    /// Interaction logic for frmPolicyTransfer.xaml
    /// </summary>
    public partial class frmPolicyTransfer : Window
    {
        object policy;
        ClientsLogic clientsLogic = new ClientsLogic();
        List<Client> clients = new List<Client>();
        ElementaryPolicy elementaryPolicy;
        PoliciesLogic elementaryLogic = new PoliciesLogic();
        TravelPolicy travelPolicy;
        TravelLogic travelLogic = new TravelLogic();
        FinancePolicy financePolicy;
        FinancePoliciesLogic financeLogic = new FinancePoliciesLogic();
        HealthPolicy healthPolicy;
        HealthPoliciesLogic healthLogic = new HealthPoliciesLogic();
        LifePolicy lifePolicy;
        LifePoliciesLogic lifeLogic = new LifePoliciesLogic();
        PersonalAccidentsPolicy personalAccidentsPolicy;
        PersonalAccidentsLogic personalAccidentsLogic = new PersonalAccidentsLogic();
        ForeingWorkersPolicy foreingWorkersPolicy;
        ForeingWorkersLogic foreingWorkersLogic = new ForeingWorkersLogic();


        public frmPolicyTransfer(object policy)
        {
            InitializeComponent();
            this.policy = policy;
        }
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            dgClientsBinding();
            if (policy is ElementaryPolicy)
            {
                elementaryPolicy = (ElementaryPolicy)policy;
                txtClientLastName.Text = elementaryPolicy.Client.LastName;
                txtClientFirstName.Text = elementaryPolicy.Client.FirstName;
                txtClientId.Text = elementaryPolicy.Client.IdNumber;
                txtInsurance.Text = elementaryPolicy.Insurance.InsuranceName;
                txtPolicyNumber.Text = elementaryPolicy.PolicyNumber;
                txtIndustry.Text = elementaryPolicy.InsuranceIndustry.InsuranceIndustryName;
            }
            else if (policy is TravelPolicy)
            {
                travelPolicy = (TravelPolicy)policy;
                txtClientLastName.Text = travelPolicy.Client.LastName;
                txtClientFirstName.Text = travelPolicy.Client.FirstName;
                txtClientId.Text = travelPolicy.Client.IdNumber;
                txtInsurance.Text = travelPolicy.Insurance.InsuranceName;
                txtPolicyNumber.Text = travelPolicy.PolicyNumber;                
            }
            else if (policy is ForeingWorkersPolicy)
            {
                foreingWorkersPolicy = (ForeingWorkersPolicy)policy;
                txtClientLastName.Text = foreingWorkersPolicy.Client.LastName;
                txtClientFirstName.Text = foreingWorkersPolicy.Client.FirstName;
                txtClientId.Text = foreingWorkersPolicy.Client.IdNumber;
                txtInsurance.Text = foreingWorkersPolicy.Insurance.InsuranceName;
                txtPolicyNumber.Text = foreingWorkersPolicy.PolicyNumber;
                txtIndustry.Text = foreingWorkersPolicy.ForeingWorkersIndustry.ForeingWorkersIndustryName;
            }
            else if (policy is HealthPolicy)
            {
                healthPolicy = (HealthPolicy)policy;
                txtClientLastName.Text = healthPolicy.Client.LastName;
                txtClientFirstName.Text = healthPolicy.Client.FirstName;
                txtClientId.Text = healthPolicy.Client.IdNumber;
                txtInsurance.Text = healthPolicy.Insurance.InsuranceName;
                txtPolicyNumber.Text = healthPolicy.PolicyNumber;
            }
            else if (policy is LifePolicy)
            {
                lifePolicy = (LifePolicy)policy;
                txtClientLastName.Text = lifePolicy.Client.LastName;
                txtClientFirstName.Text = lifePolicy.Client.FirstName;
                txtClientId.Text = lifePolicy.Client.IdNumber;
                txtInsurance.Text = lifePolicy.Insurance.InsuranceName;
                txtPolicyNumber.Text = lifePolicy.PolicyNumber;
                txtIndustry.Text = lifePolicy.LifeIndustry.LifeIndustryName;
            }
            else if (policy is PersonalAccidentsPolicy)
            {
                personalAccidentsPolicy = (PersonalAccidentsPolicy)policy;
                txtClientLastName.Text = personalAccidentsPolicy.Client.LastName;
                txtClientFirstName.Text = personalAccidentsPolicy.Client.FirstName;
                txtClientId.Text = personalAccidentsPolicy.Client.IdNumber;
                txtInsurance.Text = personalAccidentsPolicy.Insurance.InsuranceName;
                txtPolicyNumber.Text = personalAccidentsPolicy.PolicyNumber;
            }
            else if (policy is FinancePolicy)
            {
                financePolicy = (FinancePolicy)policy;
                txtClientLastName.Text = financePolicy.Client.LastName;
                txtClientFirstName.Text = financePolicy.Client.FirstName;
                txtClientId.Text = financePolicy.Client.IdNumber;
                txtInsurance.Text = financePolicy.Insurance.InsuranceName;
                txtPolicyNumber.Text = financePolicy.AssociateNumber;
                txtIndustry.Text = financePolicy.FinanceProgramType.ProgramTypeName;
            }
        }

        private void dgClientsBinding()
        {
            clients = clientsLogic.GetAllClientsByType("לקוחות");
            dgClients.ItemsSource = clients;
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void dgClients_MouseDown(object sender, MouseButtonEventArgs e)
        {
            dgClients.UnselectAll();
        }

        private void txtClientSearch_TextChanged(object sender, TextChangedEventArgs e)
        {
            string key = txtClientSearch.Text;
            List<Client> searchResults;
            searchResults = clients.Where(c => c.LastName.StartsWith(key) || c.FirstName.StartsWith(key) || c.IdNumber.StartsWith(key) || (c.LastName + " " + c.FirstName).StartsWith(key) || (c.FirstName + " " + c.LastName).StartsWith(key)).ToList();
            dgClients.ItemsSource = searchResults;
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            if (dgClients.SelectedItem == null)
            {
                MessageBox.Show("נא לסמן את הלקוח שברצונך להעביר את הפוליסה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            try
            {
                if (elementaryPolicy != null)
                {
                    elementaryLogic.TransferPolicyToClient(elementaryPolicy, (Client)dgClients.SelectedItem);
                }
                else if (travelPolicy != null)
                {
                    travelLogic.TransferPolicyToClient(travelPolicy, (Client)dgClients.SelectedItem);
                }
                else if (financePolicy != null)
                {
                    financeLogic.TransferPolicyToClient(financePolicy, (Client)dgClients.SelectedItem);
                }
                else if (healthPolicy != null)
                {
                    healthLogic.TransferPolicyToClient(healthPolicy, (Client)dgClients.SelectedItem);
                }
                else if (lifePolicy != null)
                {
                    lifeLogic.TransferPolicyToClient(lifePolicy, (Client)dgClients.SelectedItem);
                }
                else if (personalAccidentsPolicy != null)
                {
                    personalAccidentsLogic.TransferPolicyToClient(personalAccidentsPolicy, (Client)dgClients.SelectedItem);
                }
                else if (foreingWorkersPolicy != null)
                {
                    foreingWorkersLogic.TransferPolicyToClient(foreingWorkersPolicy, (Client)dgClients.SelectedItem);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                Close();
            }
        }
    }
}
