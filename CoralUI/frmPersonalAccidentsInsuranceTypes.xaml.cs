﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using CoralBusinessLogics;

namespace CoralUI
{
    /// <summary>
    /// Interaction logic for frmPersonalAccidentsInsuranceTypes.xaml
    /// </summary>
    /// 
    public partial class frmPersonalAccidentsInsuranceTypes : Window
    {
        InputsValidations validations = new InputsValidations();
        PersonalAccidentsInsuranceTypesLogic insuranceTypesLogic = new PersonalAccidentsInsuranceTypesLogic();
        Company company;
        public PersonalAccidentsInsuranceType typeAdded;
        private bool isChanges = false;
        private bool cancelClose = false;


        public frmPersonalAccidentsInsuranceTypes(InsuranceCompany insuranceCompany)
        {
            InitializeComponent();
            company = insuranceCompany.Company;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            txtCompany.Text = company.CompanyName;
            dgInsuranceTypesBinding();
        }


        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (sender is TextBox)
            {
                TextBox txtBox = (TextBox)sender;
                txtBox.ClearValue(BorderBrushProperty);
                if (txtBox.Text != "")
                {
                    try
                    {
                        decimal reusult = validations.ConvertStringToDecimal(txtBox.Text);
                    }
                    catch (Exception ex)
                    {
                        txtBox.BorderBrush = System.Windows.Media.Brushes.Red;
                        System.Windows.Forms.MessageBox.Show(ex.Message);
                        txtBox.Text = "";
                    }
                }
            }
        }

        private void btnAddInsuranceType_Click(object sender, RoutedEventArgs e)
        {
            if (txtInsuranceTypeName.Text == "")
            {
                cancelClose = true;
                MessageBox.Show("שם התכנית הינו שדה חובה");
                return;
            }
            bool status = true;
            if (cbStatus.SelectedIndex == 1)
            {
                status = false;
            }
            decimal? deathAmount = null;
            if (txtDeathAmount.Text != "")
            {
                deathAmount = decimal.Parse(txtDeathAmount.Text);
            }
            decimal? discapacityAmount = null;
            if (txtDiscapacityAmount.Text != "")
            {
                discapacityAmount = decimal.Parse(txtDiscapacityAmount.Text);
            }
            decimal? fracturesAndBurnsAmount = null;
            if (txtFracturesAndBurnsAmount.Text != "")
            {
                fracturesAndBurnsAmount = decimal.Parse(txtFracturesAndBurnsAmount.Text);
            }
            decimal? hospitalizationAmount = null;
            if (txtHospitalizationAmount.Text != "")
            {
                hospitalizationAmount = decimal.Parse(txtHospitalizationAmount.Text);
            }
            decimal? siudAmount = null;
            if (txtSiudAmount.Text != "")
            {
                siudAmount = decimal.Parse(txtSiudAmount.Text);
            }
            typeAdded = insuranceTypesLogic.InsertPersonalAccidentsInsuranceType(txtInsuranceTypeName.Text, status, company.CompanyID, deathAmount, discapacityAmount, fracturesAndBurnsAmount, hospitalizationAmount, siudAmount);
            dgInsuranceTypesBinding();
            ClearInputs();
        }

        private void ClearInputs()
        {
            txtInsuranceTypeName.Clear();

            cbStatus.SelectedIndex = 0;
            txtDeathAmount.Clear();
            txtDiscapacityAmount.Clear();
            txtFracturesAndBurnsAmount.Clear();
            txtHospitalizationAmount.Clear();
            txtSiudAmount.Clear();
        }

        private void dgInsuranceTypesBinding()
        {
            dgInsuranceTypes.ItemsSource = insuranceTypesLogic.GetPersonalAccidentsInsuranceTypesByCompany(company.CompanyID);
        }

        private void btnDeleteInsuranceType_Click(object sender, RoutedEventArgs e)
        {

            if (dgInsuranceTypes.SelectedItem == null)
            {
                MessageBox.Show("נא לסמן סוג ביטוח", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            if (MessageBox.Show("למחוק סוג ביטוח", "אישור", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                insuranceTypesLogic.DeleteInsuranceType((PersonalAccidentsInsuranceType)dgInsuranceTypes.SelectedItem);
                dgInsuranceTypesBinding();
            }
            else
            {
                dgInsuranceTypes.UnselectAll();
            }
        }

        private void dgInsuranceTypes_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (dgInsuranceTypes.SelectedItem == null)
            {
                txtInsuranceTypeName.IsEnabled = true;
                ClearInputs();
            }
            else
            {
                DisplayInputs();
            }
        }

        private void DisplayInputs()
        {
            if (dgInsuranceTypes.SelectedItem == null)
            {
                return;
            }
            PersonalAccidentsInsuranceType type = (PersonalAccidentsInsuranceType)dgInsuranceTypes.SelectedItem;
            txtCompany.Text = type.Company.CompanyName;
            txtInsuranceTypeName.Text = type.PersonalAccidentsInsuranceTypeName;
            txtInsuranceTypeName.IsEnabled = false;
            if (type.Status == false)
            {
                cbStatus.SelectedIndex = 1;
            }
            if (type.DeathByAccidentAmount != null)
            {
                txtDeathAmount.Text = ((decimal)type.DeathByAccidentAmount).ToString("0.##");
            }
            else
            {
                txtDeathAmount.Clear();
            }
            if (type.IncapacityByAccidentAmount != null)
            {
                txtDiscapacityAmount.Text = ((decimal)type.IncapacityByAccidentAmount).ToString("0.##");
            }
            else
            {
                txtDiscapacityAmount.Clear();
            }
            if (type.FracturesAndBurnsByAccidentAmount != null)
            {
                txtFracturesAndBurnsAmount.Text = ((decimal)type.FracturesAndBurnsByAccidentAmount).ToString("0.##");
            }
            else
            {
                txtFracturesAndBurnsAmount.Clear();
            }
            if (type.CompensationForHospitalizationAmount != null)
            {
                txtHospitalizationAmount.Text = ((decimal)type.CompensationForHospitalizationAmount).ToString("0.##");
            }
            else
            {
                txtHospitalizationAmount.Clear();
            }
            if (type.SiudByAccidentAmount != null)
            {
                txtSiudAmount.Text = ((decimal)type.SiudByAccidentAmount).ToString("0.##");
            }
            else
            {
                txtSiudAmount.Clear();
            }
        }

        private void dgInsuranceTypes_MouseDown(object sender, MouseButtonEventArgs e)
        {
            dgInsuranceTypes.UnselectAll();
        }

        private void btnUpdateInsuranceType_Click(object sender, RoutedEventArgs e)
        {
            if (dgInsuranceTypes.SelectedItem == null)
            {
                MessageBox.Show("נא לסמן סוג ביטוח", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            bool status = true;
            if (cbStatus.SelectedIndex == 1)
            {
                status = false;
            }
            decimal? deathAmount = null;
            if (txtDeathAmount.Text != "")
            {
                deathAmount = decimal.Parse(txtDeathAmount.Text);
            }
            decimal? discapacityAmount = null;
            if (txtDiscapacityAmount.Text != "")
            {
                discapacityAmount = decimal.Parse(txtDiscapacityAmount.Text);
            }
            decimal? fracturesAndBurnsAmount = null;
            if (txtFracturesAndBurnsAmount.Text != "")
            {
                fracturesAndBurnsAmount = decimal.Parse(txtFracturesAndBurnsAmount.Text);
            }
            decimal? hospitalizationAmount = null;
            if (txtHospitalizationAmount.Text != "")
            {
                hospitalizationAmount = decimal.Parse(txtHospitalizationAmount.Text);
            }
            decimal? siudAmount = null;
            if (txtSiudAmount.Text != "")
            {
                siudAmount = decimal.Parse(txtSiudAmount.Text);
            }
            typeAdded = insuranceTypesLogic.UpdatePersonalAccidentsInsuranceTypeStatus((PersonalAccidentsInsuranceType)dgInsuranceTypes.SelectedItem, status, company.CompanyID, deathAmount, discapacityAmount, fracturesAndBurnsAmount, hospitalizationAmount, siudAmount);
            dgInsuranceTypesBinding();
            ClearInputs();
        }

        private void GroupBox_GotFocus(object sender, RoutedEventArgs e)
        {
            isChanges = true;
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (isChanges)
            {
                if (MessageBox.Show("האם ברצונך לשמור שינוים שבצעת", "אישור", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    if (dgInsuranceTypes.SelectedItem == null)
                    {
                        btnAddInsuranceType_Click(sender, new RoutedEventArgs());
                    }
                    else
                    {
                        btnUpdateInsuranceType_Click(sender, new RoutedEventArgs());
                    }
                    if (cancelClose)
                    {
                        e.Cancel = true;
                        cancelClose = false;
                    }
                }
            }
        }
    }
}
