﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.IO;
using GdPicture10;
using System.Drawing;
using System.Windows.Interop;
using CoralBusinessLogics;
using System.Security;
using System.Runtime.InteropServices;

namespace CoralUI
{
    /// <summary>
    /// Interaction logic for frmSearchResults.xaml
    /// </summary>
    public partial class frmSearchResults : Window
    {
        List<DirectoryInfo> folders = null;
        List<FileInfo> files = null;
        List<Result> results = new List<Result>();
        Client client = null;
        ClientsLogic clientLogic = new ClientsLogic();
        string rootPath;
        string clientsPath;
        Email email = new Email();
        public frmSearchResults(List<DirectoryInfo> dirResults, List<FileInfo> fileResults, string searchKey, Client archClient)
        {
            InitializeComponent();
            this.SizeToContent = SizeToContent.Manual;
            folders = dirResults;
            files = fileResults;
            client = archClient;
            if (searchKey != null)
            {
                txtGeneralSearch.Text = searchKey;
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            cbChooseClient.ItemsSource = clientLogic.GetAllClients();

            string strAppDir = System.AppDomain.CurrentDomain.BaseDirectory;
            string path = strAppDir + @"Coral Files\ClientsPath.txt";

            if (!File.Exists(path))
            {
                MessageBox.Show("לא נמצא תיקיית הלקוחות. נא בדוק בהגדרות ראשוניות את נתיב התיקייה");
                Close();
                return;
            }
            clientsPath = File.ReadAllText(path);
            //rootPath = clientsPath + @"\" + clientArchive.ClientID;
            m_GDViewer.ZoomMode = ViewerZoomMode.ZoomModeWidthViewer;
            if (folders.Count == 0 && files.Count == 0)
            {
                System.Windows.Forms.MessageBox.Show("לא נמצא תוצאות מתאימות");
            }
            else
            {
                DisplayResults(folders, files);
                dgSearchResultsBinding();
            }
            
        }

        private void Window_ContentRendered(object sender, EventArgs e)
        {
            System.Windows.Forms.MessageBox.Show("החיפוש הסתיים");
        }
        private List<Result> DisplayResults(List<DirectoryInfo> folders, List<FileInfo> files)
        {
            foreach (DirectoryInfo item in folders)
            {
                var addUriSource = new Uri(@"/Images/folder.png", UriKind.Relative);
                Result result = new Result();
                result.DocIcon = new BitmapImage(addUriSource);
                result.DocPath = item.FullName;
                results.Add(result);
            }
            foreach (FileInfo item in files)
            {
                if (File.Exists(item.FullName))
                {
                    string iconPath = @item.FullName;

                    //Icon icon = System.Drawing.Icon.ExtractAssociatedIcon(iconPath);
                    Icon icon = ExtractAssociatedIcon(iconPath);
                    ImageSource img = Imaging.CreateBitmapSourceFromHIcon(icon.Handle, Int32Rect.Empty, BitmapSizeOptions.FromEmptyOptions());
                    Result result = new Result() { DocIcon = img, DocPath = item.FullName };
                    results.Add(result);
                }
            }
            return results;
        }

        public static Icon ExtractAssociatedIcon(String filePath)
        {
            int index = 0;

            Uri uri;
            if (filePath == null)
            {
                throw new ArgumentException(String.Format("'{0}' is not valid for '{1}'", "null", "filePath"), "filePath");
            }
            try
            {
                uri = new Uri(filePath);
            }
            catch (UriFormatException)
            {
                filePath = System.IO.Path.GetFullPath(filePath);
                uri = new Uri(filePath);
            }
            //if (uri.IsUnc)
            //{
            //  throw new ArgumentException(String.Format("'{0}' is not valid for '{1}'", filePath, "filePath"), "filePath");
            //}
            if (uri.IsFile)
            {
                if (!File.Exists(filePath))
                {
                    //IntSecurity.DemandReadFileIO(filePath);
                    throw new FileNotFoundException(filePath);
                }

                StringBuilder iconPath = new StringBuilder(260);
                iconPath.Append(filePath);

                IntPtr handle = SafeNativeMethods.ExtractAssociatedIcon(new HandleRef(null, IntPtr.Zero), iconPath, ref index);
                if (handle != IntPtr.Zero)
                {
                    return System.Drawing.Icon.FromHandle(handle);
                }
            }
            return null;
        }


        /// <summary>
        /// This class suppresses stack walks for unmanaged code permission. 
        /// (System.Security.SuppressUnmanagedCodeSecurityAttribute is applied to this class.) 
        /// This class is for methods that are safe for anyone to call. 
        /// Callers of these methods are not required to perform a full security review to make sure that the 
        /// usage is secure because the methods are harmless for any caller.
        /// </summary>
        [SuppressUnmanagedCodeSecurity]
        internal static class SafeNativeMethods
        {
            [DllImport("shell32.dll", EntryPoint = "ExtractAssociatedIcon", CharSet = CharSet.Auto)]
            internal static extern IntPtr ExtractAssociatedIcon(HandleRef hInst, StringBuilder iconPath, ref int index);
        }

        private void dgSearchResultsBinding()
        {
            dgSearchResults.ItemsSource = results.ToList();
        }

        public void OnHyperlinkClick(object sender, RoutedEventArgs e)
        {
            m_GDViewer.ZoomMode = ViewerZoomMode.ZoomModeWidthViewer;
            m_GDViewer.DisplayFromFile(((Result)dgSearchResults.SelectedItem).DocPath);
            ShowCurrentPage();
        }

        private void btnZoomFitToViewer_Click(object sender, RoutedEventArgs e)
        {
            m_GDViewer.ZoomMode = ViewerZoomMode.ZoomModeFitToViewer;
        }

        private void btnZoom100_Click(object sender, RoutedEventArgs e)
        {
            m_GDViewer.Zoom = 1;
        }

        private void zoomSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            m_GDViewer.Zoom = zoomSlider.Value;
        }

        private void m_GDViewer_AfterZoomChange()
        {
            zoomSlider.Value = m_GDViewer.Zoom;
        }

        private void btnRotateRight_Click(object sender, RoutedEventArgs e)
        {
            m_GDViewer.Rotate(RotateFlipType.Rotate90FlipNone);
            if (m_GDViewer.GetStat() != GdPictureStatus.OK)
            {
                MessageBox.Show("Error: " + m_GDViewer.GetStat());
            }
        }

        private void btnRotateLeft_Click(object sender, RoutedEventArgs e)
        {
            m_GDViewer.Rotate(RotateFlipType.Rotate270FlipNone);
            if (m_GDViewer.GetStat() != GdPictureStatus.OK)
            {
                MessageBox.Show("Error: " + m_GDViewer.GetStat());
            }
        }

        private void btnFocus_Click(object sender, RoutedEventArgs e)
        {
            m_GDViewer.MouseMode = ViewerMouseMode.MouseModeAreaSelection;
        }

        private void m_GDViewer_MouseUp(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            if (m_GDViewer.MouseMode == ViewerMouseMode.MouseModeAreaSelection)
            {
                m_GDViewer.ZoomRect();
                m_GDViewer.ClearRect();
                m_GDViewer.MouseMode = ViewerMouseMode.MouseModeDefault;
            }
        }

        private void btnCopyCurrentPage_Click(object sender, RoutedEventArgs e)
        {
            m_GDViewer.CopyToClipboard();
        }

        private void btnPrintCurrent_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                m_GDViewer.Print();
            }
            catch (Exception)
            {
                System.Windows.Forms.MessageBox.Show("לא ניתן להדפיס");
                return;
            }
        }

        private void btnPrintAll_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                m_GDViewer.PrintDialog();
            }
            catch (Exception)
            {
                System.Windows.Forms.MessageBox.Show("לא ניתן להדפיס");
                return;
            }
        }

        private void m_GDViewer_DocumentClosed()
        {
            ShowCurrentPage();
        }

        private void ShowCurrentPage()
        {
            if (m_GDViewer.CurrentPage != 0)
                lblCurrentPage.Content = m_GDViewer.CurrentPage.ToString() + " / " + m_GDViewer.PageCount;
            else
                lblCurrentPage.Content = "";
        }

        private void btnFirstPage_Click(object sender, RoutedEventArgs e)
        {
            if (m_GDViewer.DisplayFirstPage() != GdPictureStatus.OK)
                MessageBox.Show("Error : " + m_GDViewer.GetStat().ToString());
            else
                ShowCurrentPage();
        }

        private void btnPreviousPage_Click(object sender, RoutedEventArgs e)
        {
            if (m_GDViewer.DisplayPreviousPage() != GdPictureStatus.OK)
                MessageBox.Show("Error : " + m_GDViewer.GetStat().ToString());
            else
                ShowCurrentPage();
        }

        private void btnLastPage_Click(object sender, RoutedEventArgs e)
        {
            if (m_GDViewer.DisplayLastPage() != GdPictureStatus.OK)
                MessageBox.Show("Error : " + m_GDViewer.GetStat().ToString());
            else
                ShowCurrentPage();
        }

        private void btnNextPage_Click(object sender, RoutedEventArgs e)
        {
            if (m_GDViewer.DisplayNextPage() != GdPictureStatus.OK)
                MessageBox.Show("Error : " + m_GDViewer.GetStat().ToString());
            else
                ShowCurrentPage();
        }

        private void btnMoveToClient_Click(object sender, RoutedEventArgs e)
        {
            if (cbChooseClient.SelectedItem == null)
            {
                System.Windows.Forms.MessageBox.Show("נא בחר לקוח");
                return;
            }
            string sourcePath = ((Result)dgSearchResults.SelectedItem).DocPath;
            if (MessageBox.Show("העבר את הקובץ " + sourcePath + " ללקוח " + ((Client)cbChooseClient.SelectedItem).LastName + " " + ((Client)cbChooseClient.SelectedItem).FirstName, "אישור", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                try
                {
                    string fileOrDirName = System.IO.Path.GetFileName(sourcePath);
                    string destPath;
                    int index = 1;
                    do
                    {
                        if (index == 1)
                        {
                            destPath = clientsPath + @"\" + ((Client)cbChooseClient.SelectedItem).ClientID + @"\" + fileOrDirName;
                        }
                        else
                        {
                            destPath = clientsPath + @"\" + ((Client)cbChooseClient.SelectedItem).ClientID + @"\" + index + fileOrDirName;
                        }
                        index++;

                    } while (File.Exists(destPath));
                    //destPath = Directory.GetCurrentDirectory() + @"\Clients\" + ((Client)cbChooseClient.SelectedItem).ClientID + @"\" + fileOrDirName;
                    m_GDViewer.CloseDocument();
                    Directory.Move(sourcePath, destPath);
                    results.Remove((Result)dgSearchResults.SelectedItem);
                    dgSearchResultsBinding();
                }
                catch (Exception ex)
                {
                    System.Windows.Forms.MessageBox.Show(ex.Message);
                }
            }
        }

        private void btnSearch_Click(object sender, RoutedEventArgs e)
        {
            results.Clear();
            if (txtGeneralSearch.Text == "")
            {
                MessageBox.Show("נא להזין מילת חיפוש", "שגיאה", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            DirectoryInfo clientRoot;
            if (rbClientSearch.IsChecked == true)
            {
                clientRoot = new DirectoryInfo(clientsPath + @"\" + client.ClientID);
            }
            else
            {
                clientRoot=new DirectoryInfo(clientsPath);
            }
                List<DirectoryInfo> directories = clientRoot.GetDirectories("*.*", System.IO.SearchOption.AllDirectories).ToList();
                directories.Add(clientRoot);
                FileInfo[] files = null;
                List<DirectoryInfo> dirResults = new List<DirectoryInfo>();
                List<FileInfo> fileResults = new List<FileInfo>();

                foreach (DirectoryInfo dir in directories)
                {
                    if (rbFilesAndDirectories.IsChecked==true)
                    {
                        if (dir.Name.Contains(txtGeneralSearch.Text))
                        {
                            dirResults.Add(dir);
                        } 
                    }
                    files = dir.GetFiles("*" + txtGeneralSearch.Text + "*");
                    if (files != null)
                    {
                        foreach (FileInfo file in files)
                        {
                            fileResults.Add(file);
                        }
                    }
                }
                if (dirResults.Count == 0 && fileResults.Count == 0)
                {
                    System.Windows.Forms.MessageBox.Show("לא נמצא תוצאות מתאימות");
                }
                else
                {
                    DisplayResults(dirResults, fileResults);                    
                    System.Windows.Forms.MessageBox.Show("החיפוש הסתיים");
                }
                dgSearchResultsBinding();
            }

       

        private void Hyperlink_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {

        }

        private void Hyperlink_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {

        }

        private void btnSendMail_Click(object sender, RoutedEventArgs e)
        {
            var selectedFiles = dgSearchResults.SelectedItems;
            List<string> attachments = new List<string>();
            foreach (var item in selectedFiles)
            {
                if (!File.Exists(((Result)item).DocPath))
                {                    
                    continue;
                }
                attachments.Add(((Result)item).DocPath);
            }
            try
            {
                email.SendEmailFromOutlook("", "", null, null);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }

}
