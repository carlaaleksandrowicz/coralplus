﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Xceed.Wpf.Toolkit;

namespace CoralUI
{
    /// <summary>
    /// Interaction logic for frmNewMessage.xaml
    /// </summary>
    public partial class frmNewMessage : Window
    {
        public frmNewMessage()
        {
            InitializeComponent();
            DateTime dt = DateTime.Now;
            string dtFormat=String.Format("{0:F}", dt);
            lblDateTimeNow.Content = dtFormat;
            dtpickerReminder.Value= DateTime.Now;
        }

        private void chbIsReminder_Checked(object sender, RoutedEventArgs e)
        {
            dtpickerReminder.IsEnabled = true;
            cbWorker.IsEnabled = true;
        }

        private void chbIsReminder_Unchecked(object sender, RoutedEventArgs e)
        {
            dtpickerReminder.IsEnabled = false;
            cbWorker.IsEnabled = false;
        }
    }
}
