﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using CoralBusinessLogics;

namespace CoralUI
{
    /// <summary>
    /// Interaction logic for frmTravelInsuranceTypes.xaml
    /// </summary>
    public partial class frmTravelInsuranceTypes : Window
    {
        Company company;
        public TravelInsuranceType typeAdded;
        TravelInsuranceLogic insuranceTypesLogic = new TravelInsuranceLogic();
        private bool isChanges = false;
        private bool cancelClose = false;

        public frmTravelInsuranceTypes()
        {
            InitializeComponent();
        }

        public frmTravelInsuranceTypes(InsuranceCompany insuranceCompany)
        {
            InitializeComponent();
            company = insuranceCompany.Company;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            txtCompany.Text = company.CompanyName;
            dgInsuranceTypesBinding();
        }

        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void btnAddInsuranceType_Click(object sender, RoutedEventArgs e)
        {
            if (txtInsuranceTypeName.Text == "")
            {
                MessageBox.Show("שם התכנית הינו שדה חובה");
                cancelClose = true;
                return;
            }
            bool status = true;
            if (cbStatus.SelectedIndex == 1)
            {
                status = false;
            }
            typeAdded = insuranceTypesLogic.InsertTravelInsuranceType(txtInsuranceTypeName.Text, status, company.CompanyID, chbWinterSport.IsChecked, chbExtremeSport.IsChecked, chbSearchAndRescue.IsChecked, chbPregnancy.IsChecked, chbExistingMedicalCondition.IsChecked, chbBaggage.IsChecked, chbComputerAndTablet.IsChecked, chbPhoneAndGPS.IsChecked, chbBicycle.IsChecked, txtOther.Text, chbOther.IsChecked);
            dgInsuranceTypesBinding();
            ClearInputs();
            isChanges = false;
        }

        private void ClearInputs()
        {
            txtInsuranceTypeName.Clear();
            cbStatus.SelectedIndex = 0;
            chbWinterSport.IsChecked = false;
            chbExtremeSport.IsChecked = false;
            chbExistingMedicalCondition.IsChecked = false;
            chbSearchAndRescue.IsChecked = false;
            chbPregnancy.IsChecked = false;
            chbBaggage.IsChecked = false;
            chbBicycle.IsChecked = false;
            chbComputerAndTablet.IsChecked = false;
            chbPhoneAndGPS.IsChecked = false;
            chbOther.IsChecked = false;
            txtOther.Clear();
        }

        private void dgInsuranceTypesBinding()
        {
            dgInsuranceTypes.ItemsSource = insuranceTypesLogic.GetTravelInsuranceTypesByCompany(company.CompanyID);
        }

        private void btnDeleteInsuranceType_Click(object sender, RoutedEventArgs e)
        {
            if (dgInsuranceTypes.SelectedItem == null)
            {
                MessageBox.Show("נא לסמן סוג ביטוח", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            if (MessageBox.Show("למחוק סוג ביטוח", "אישור", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                insuranceTypesLogic.DeleteInsuranceType((TravelInsuranceType)dgInsuranceTypes.SelectedItem);
                dgInsuranceTypesBinding();
            }
            else
            {
                dgInsuranceTypes.UnselectAll();
            }
        }

        private void btnUpdateInsuranceType_Click(object sender, RoutedEventArgs e)
        {
            if (dgInsuranceTypes.SelectedItem == null)
            {
                MessageBox.Show("נא לסמן סוג ביטוח", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            bool status = true;
            if (cbStatus.SelectedIndex == 1)
            {
                status = false;
            }
            typeAdded = insuranceTypesLogic.UpdateTravelInsuranceTypeStatus((TravelInsuranceType)dgInsuranceTypes.SelectedItem, status, company.CompanyID, chbWinterSport.IsChecked, chbExtremeSport.IsChecked, chbSearchAndRescue.IsChecked, chbPregnancy.IsChecked, chbExistingMedicalCondition.IsChecked, chbBaggage.IsChecked, chbComputerAndTablet.IsChecked, chbPhoneAndGPS.IsChecked, chbBicycle.IsChecked, txtOther.Text, chbOther.IsChecked);
            dgInsuranceTypesBinding();
            ClearInputs();

        }

        private void dgInsuranceTypes_MouseDown(object sender, MouseButtonEventArgs e)
        {
            dgInsuranceTypes.UnselectAll();
        }

        private void dgInsuranceTypes_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (dgInsuranceTypes.SelectedItem == null)
            {
                txtInsuranceTypeName.IsEnabled = true;
                ClearInputs();
            }
            else
            {
                DisplayInputs();
            }
        }

        private void DisplayInputs()
        {
            if (dgInsuranceTypes.SelectedItem == null)
            {
                return;
            }
            TravelInsuranceType type = (TravelInsuranceType)dgInsuranceTypes.SelectedItem;
            txtCompany.Text = type.Company.CompanyName;
            txtInsuranceTypeName.Text = type.TravelInsuranceTypeName;
            txtInsuranceTypeName.IsEnabled = false;
            if (type.Status == false)
            {
                cbStatus.SelectedIndex = 1;
            }
            chbWinterSport.IsChecked = type.IsWinterSport;
            chbExtremeSport.IsChecked = type.IsExtremeSport;
            chbExistingMedicalCondition.IsChecked = type.IsExistingMedicalCondition;
            chbSearchAndRescue.IsChecked = type.IsSearchAndRescue;
            chbPregnancy.IsChecked = type.IsPregnancy;
            chbBaggage.IsChecked = type.IsBaggage;
            chbBicycle.IsChecked = type.IsBicycle;
            chbComputerAndTablet.IsChecked = type.IsComputerOrTablet;
            chbPhoneAndGPS.IsChecked = type.IsPhoneOrGPS;
            chbOther.IsChecked = type.IsOther;
            txtOther.Text = type.OtherName;
        }

        private void txtInsuranceTypeName_GotFocus(object sender, RoutedEventArgs e)
        {
            isChanges = true;
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (isChanges)
            {
                if (MessageBox.Show("האם ברצונך לשמור שינוים שבצעת", "אישור", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    if (dgInsuranceTypes.SelectedItem == null)
                    {
                        btnAddInsuranceType_Click(sender, new RoutedEventArgs());
                    }
                    else
                    {
                        btnUpdateInsuranceType_Click(sender, new RoutedEventArgs());
                    }
                    if (cancelClose)
                    {
                        e.Cancel = true;
                        cancelClose = false;
                    }
                }
            }
        }
    }
}
