﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using CoralBusinessLogics;

namespace CoralUI
{
    /// <summary>
    /// Interaction logic for frmReferedByClientList.xaml
    /// </summary>
    public partial class frmReferedByClientList : Window
    {
        private List<CoralBusinessLogics.Client> referedByClient;

        public frmReferedByClientList()
        {
            InitializeComponent();
        }

        public frmReferedByClientList(List<Client> referedByClient)
        {
            // TODO: Complete member initialization
            InitializeComponent();
            this.referedByClient = referedByClient;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            lvReferedByClient.ItemsSource = referedByClient;
        }

        
    }
}
