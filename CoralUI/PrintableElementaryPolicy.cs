﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoralUI
{
    [Serializable()]
    public class PrintableElementaryPolicy
    {
        public string ClientName { get; set; }
        public string ClientIdNumber { get; set; }
        public string PolicyNumber { get; set; }
        public int Addition { get; set; }
    }
}
