﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using CoralBusinessLogics;

namespace CoralUI
{

    /// <summary>
    /// Interaction logic for frmClaims.xaml
    /// </summary>
    public partial class frmClaims : Window
    {
        ClaimsLogic claimsLogic = new ClaimsLogic();
        ClientsLogic clientLogic = new ClientsLogic();
        IndustriesLogic industryLogic = new IndustriesLogic();
        InsurancesLogic insuranceLogic = new InsurancesLogic();
        User user;
        List<ElementaryClaim> elementaryClaims = new List<ElementaryClaim>();
        List<LifeClaim> lifeClaims = new List<LifeClaim>();
        List<HealthClaim> healthClaims = new List<HealthClaim>();
        List<PersonalAccidentsClaim> personalAccidentsClaims = new List<PersonalAccidentsClaim>();
        List<TravelClaim> travelClaims = new List<TravelClaim>();
        List<ForeingWorkersClaim> foreingWorkersClaims = new List<ForeingWorkersClaim>();
        LifeClaimsLogic lifeClaimsLogic = new LifeClaimsLogic();
        HealthClaimsLogic healthClaimLogic = new HealthClaimsLogic();
        LifeIndustryLogic lifeIndustryLogic = new LifeIndustryLogic();
        PersonalAccidentsClaimsLogic personalAccidentsClaimLogic = new PersonalAccidentsClaimsLogic();
        TravelClaimsLogic travelClaimLogic = new TravelClaimsLogic();
        ForeingWorkersClaimsLogic foreingWorkersClaimLogic = new ForeingWorkersClaimsLogic();
        ForeingWorkersIndutryLogic foreingWorkersIndustryLogic = new ForeingWorkersIndutryLogic();
        Email email = new Email();
        ExcelLogic excelLogic = new ExcelLogic();
        public frmClaims(User userAccount)
        {
            InitializeComponent();
            this.SizeToContent = SizeToContent.Manual;
            user = userAccount;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            cbClaimConditionBinding();
            cbClaimTypeBinding();
            cbIndustryBinding();
            cbCompanyBinding();
            cbClaimStatus.SelectedIndex = 0;
            SetPermissions();
        }

        private void SetPermissions()
        {
            if (user.IsElementaryPermission == false)
            {
                tbItemElementaryClaims.IsEnabled = false;
                dgClaims.ItemsSource = null;
                dgClaims.IsEnabled = false;
            }
            if (user.IsForeingWorkersPermission == false)
            {
                tbItemForeingWorkersClaims.IsEnabled = false;
            }
            if (user.IsHealthPermission == false)
            {
                tbItemHealthClaims.IsEnabled = false;
            }
            if (user.IsLifePermission == false)
            {
                tbItemLifeClaims.IsEnabled = false;
            }
            if (user.IsPersonalAccidentsPermission == false)
            {
                tbItemPersonalAccidentsClaims.IsEnabled = false;
            }
            if (user.IsScanPermission == false)
            {
                btnOpticArchive.IsEnabled = false;
            }
            if (user.IsTravelPermission == false)
            {
                tbItemTravelClaims.IsEnabled = false;
            }
        }

        private void cbCompanyBinding()
        {
            cbCompany.ItemsSource = insuranceLogic.GetAllCompanies();
            cbCompany.DisplayMemberPath = "CompanyName";
        }

        private void cbIndustryBinding()
        {
            if (tbItemElementaryClaims.IsSelected)
            {
                cbIndustry.ItemsSource = industryLogic.GetAllIndustries();
                cbIndustry.DisplayMemberPath = "InsuranceIndustryName";
                cbIndustry.IsEnabled = true;
            }
            else if (tbItemLifeClaims.IsSelected)
            {
                cbIndustry.ItemsSource = lifeIndustryLogic.GetAllLifeIndustries();
                cbIndustry.DisplayMemberPath = "LifeIndustryName";
                cbIndustry.IsEnabled = true;
            }
            else if (tbItemForeingWorkersClaims.IsSelected)
            {
                cbIndustry.ItemsSource = foreingWorkersIndustryLogic.GetAllForeingWorkersIndustries();
                cbIndustry.DisplayMemberPath = "ForeingWorkersIndustryName";
                cbIndustry.IsEnabled = true;
            }
            else
            {
                cbIndustry.IsEnabled = false;
            }
        }

        private void cbClaimTypeBinding()
        {
            if (tbItemElementaryClaims.IsSelected)
            {
                cbClaimType.ItemsSource = claimsLogic.GetAllElementaryClaimTypes();
            }
            else if (tbItemLifeClaims.IsSelected)
            {
                cbClaimType.ItemsSource = lifeClaimsLogic.GetAllLifeClaimTypes();
            }
            else if (tbItemHealthClaims.IsSelected)
            {
                cbClaimType.ItemsSource = healthClaimLogic.GetAllHealthClaimTypes();
            }
            else if (tbItemPersonalAccidentsClaims.IsSelected)
            {
                cbClaimType.ItemsSource = personalAccidentsClaimLogic.GetAllPersonalAccidentsClaimTypes();
            }
            else if (tbItemTravelClaims.IsSelected)
            {
                cbClaimType.ItemsSource = travelClaimLogic.GetAllTravelClaimTypes();
            }
            else if (tbItemForeingWorkersClaims.IsSelected)
            {
                cbClaimType.ItemsSource = foreingWorkersClaimLogic.GetAllForeingWorkersClaimTypes();
            }
        }

        private void cbClaimConditionBinding()
        {
            if (tbItemElementaryClaims.IsSelected)
            {
                cbClaimCondition.ItemsSource = claimsLogic.GetAllElementaryClaimConditions();
            }
            else if (tbItemLifeClaims.IsSelected)
            {
                cbClaimCondition.ItemsSource = lifeClaimsLogic.GetAllLifeClaimConditions();
            }
            else if (tbItemHealthClaims.IsSelected)
            {
                cbClaimCondition.ItemsSource = healthClaimLogic.GetAllHealthClaimConditions();
            }
            else if (tbItemPersonalAccidentsClaims.IsSelected)
            {
                cbClaimCondition.ItemsSource = personalAccidentsClaimLogic.GetAllPersonalAccidentsClaimConditions();
            }
            else if (tbItemTravelClaims.IsSelected)
            {
                cbClaimCondition.ItemsSource = travelClaimLogic.GetAllTravelClaimConditions();
            }
            else if (tbItemForeingWorkersClaims.IsSelected)
            {
                cbClaimCondition.ItemsSource = foreingWorkersClaimLogic.GetAllForeingWorkersClaimConditions();
            }
        }

        private void dgClaimsBinding()
        {
            cbClaimConditionBinding();
            cbClaimTypeBinding();
            cbIndustryBinding();
            ClearSearchInputs();
            RefreshClaims();
        }

        private void RefreshClaims()
        {
            int count = 0;
            string statusName = null;
            bool? status = null;
            if (cbClaimStatus.SelectedItem != null)
            {
                statusName = ((ComboBoxItem)cbClaimStatus.SelectedItem).Content.ToString();
                if (statusName == "פתוח")
                {
                    status = true;
                }
                else if (statusName == "סגור")
                {
                    status = false;
                }

            }
            if (tbItemElementaryClaims.IsSelected)
            {
                elementaryClaims = claimsLogic.GetAllElementaryClaims();
                if (status == null)
                {
                    dgClaims.ItemsSource = elementaryClaims;
                }
                else
                {
                    //elementaryClaims = elementaryClaims.Where(c => c.ClaimStatus == status).ToList();
                    //dgClaims.ItemsSource = elementaryClaims;
                    dgClaims.ItemsSource = elementaryClaims.Where(c => c.ClaimStatus == status).ToList();
                }
                //count = elementaryClaims.Count;
                count = ((List<ElementaryClaim>)dgClaims.ItemsSource).Count;
            }
            else if (tbItemLifeClaims.IsSelected)
            {
                lifeClaims = claimsLogic.GetAllLifeClaims();
                if (status == null)
                {
                    dgLifeClaims.ItemsSource = lifeClaims;
                }
                else
                {
                    //lifeClaims = lifeClaims.Where(c => c.ClaimStatus == status).ToList();
                    //dgLifeClaims.ItemsSource = lifeClaims;
                    dgLifeClaims.ItemsSource = lifeClaims.Where(c => c.ClaimStatus == status).ToList();
                }
                //count = lifeClaims.Count;
                count = ((List<LifeClaim>)dgLifeClaims.ItemsSource).Count;
            }
            else if (tbItemHealthClaims.IsSelected)
            {
                healthClaims = claimsLogic.GetAllHealthClaims();
                if (status == null)
                {
                    dgHealthClaims.ItemsSource = healthClaims;
                }
                else
                {
                    //healthClaims = healthClaims.Where(c => c.ClaimStatus == status).ToList();
                    //dgHealthClaims.ItemsSource = healthClaims;
                    dgHealthClaims.ItemsSource = healthClaims.Where(c => c.ClaimStatus == status).ToList();
                }
                count = ((List<HealthClaim>)dgHealthClaims.ItemsSource).Count;
            }
            else if (tbItemPersonalAccidentsClaims.IsSelected)
            {
                personalAccidentsClaims = claimsLogic.GetAllPersonalAccidentsClaims();
                if (status == null)
                {
                    dgPersonalAccidentsClaims.ItemsSource = personalAccidentsClaims;
                }
                else
                {
                    //personalAccidentsClaims = personalAccidentsClaims.Where(c => c.ClaimStatus == status).ToList();
                    //dgPersonalAccidentsClaims.ItemsSource = personalAccidentsClaims;
                    dgPersonalAccidentsClaims.ItemsSource = personalAccidentsClaims.Where(c => c.ClaimStatus == status).ToList();
                }
                count = ((List<PersonalAccidentsClaim>)dgPersonalAccidentsClaims.ItemsSource).Count;
            }
            else if (tbItemForeingWorkersClaims.IsSelected)
            {
                foreingWorkersClaims = claimsLogic.GetAllForeingWorkersClaims();
                if (status == null)
                {
                    dgForeingWorkersClaims.ItemsSource = foreingWorkersClaims;
                }
                else
                {
                    //foreingWorkersClaims = foreingWorkersClaims.Where(c => c.ClaimStatus == status).ToList();
                    //dgForeingWorkersClaims.ItemsSource = foreingWorkersClaims;
                    dgForeingWorkersClaims.ItemsSource = foreingWorkersClaims.Where(c => c.ClaimStatus == status).ToList();
                }
                count = ((List<ForeingWorkersClaim>)dgForeingWorkersClaims.ItemsSource).Count;
            }
            else if (tbItemTravelClaims.IsSelected)
            {
                travelClaims = travelClaimLogic.GetAllTravelClaims();
                if (status == null)
                {
                    dgTravelClaims.ItemsSource = travelClaims;
                }
                else
                {
                    //travelClaims = travelClaims.Where(c => c.ClaimStatus == status).ToList();
                    //dgTravelClaims.ItemsSource = travelClaims;
                    dgTravelClaims.ItemsSource = travelClaims.Where(c => c.ClaimStatus == status).ToList();
                }
                count = ((List<TravelClaim>)dgTravelClaims.ItemsSource).Count;
            }
            lblCount.Content = string.Format("סה''כ רשומות: {0}", count.ToString());
        }

        private void ClearSearchInputs()
        {
            cbClaimCondition.SelectedIndex = -1;
            cbClaimType.SelectedIndex = -1;
            cbIndustry.SelectedIndex = -1;
            cbCompany.SelectedIndex = -1;
            txtSearchClaim.Clear();
        }

        private void btnClientDetails_Click(object sender, RoutedEventArgs e)
        {
            ListView lv = null;
            object claimToSelect = null;
            Client client = GetClient(ref lv, ref claimToSelect);
            if (client == null)
            {
                MessageBox.Show("נא לסמן תביעה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            frmAddClient clientDetailsWindow = new frmAddClient(client, user);
            clientDetailsWindow.ShowDialog();
            RefreshClaims();
            DisplayClaimsByFilter();
            if (claimToSelect != null && lv != null)
            {
                SelectClaimInDg(claimToSelect, lv);
            }
        }

        private Client GetClient(ref ListView lv, ref object claimToSelect)
        {
            Client client = null;
            if (tbItemElementaryClaims.IsSelected)
            {
                if (dgClaims.SelectedItem == null)
                {
                    return null;
                    //MessageBox.Show("נא לסמן תביעה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                }
                else
                {
                    lv = dgClaims;
                    claimToSelect = dgClaims.SelectedItem;
                    client = clientLogic.GetClientByClientID(((ElementaryClaim)dgClaims.SelectedItem).ElementaryPolicy.ClientID);
                }
            }
            else if (tbItemLifeClaims.IsSelected)
            {
                if (dgLifeClaims.SelectedItem == null)
                {
                    return null;
                    //MessageBox.Show("נא לסמן תביעה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                }
                else
                {
                    lv = dgLifeClaims;
                    claimToSelect = dgLifeClaims.SelectedItem;
                    client = clientLogic.GetClientByClientID(((LifeClaim)dgLifeClaims.SelectedItem).LifePolicy.ClientID);
                }
            }
            else if (tbItemHealthClaims.IsSelected)
            {
                if (dgHealthClaims.SelectedItem == null)
                {
                    return null;
                    //MessageBox.Show("נא לסמן תביעה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                }
                else
                {
                    lv = dgHealthClaims;
                    claimToSelect = dgHealthClaims.SelectedItem;
                    client = clientLogic.GetClientByClientID(((HealthClaim)dgHealthClaims.SelectedItem).HealthPolicy.ClientID);
                }
            }
            else if (tbItemPersonalAccidentsClaims.IsSelected)
            {
                if (dgPersonalAccidentsClaims.SelectedItem == null)
                {
                    return null;
                    //MessageBox.Show("נא לסמן תביעה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                }
                else
                {
                    lv = dgPersonalAccidentsClaims;
                    claimToSelect = dgPersonalAccidentsClaims.SelectedItem;
                    client = clientLogic.GetClientByClientID(((PersonalAccidentsClaim)dgPersonalAccidentsClaims.SelectedItem).PersonalAccidentsPolicy.ClientID);
                }
            }
            else if (tbItemTravelClaims.IsSelected)
            {
                if (dgTravelClaims.SelectedItem == null)
                {
                    return null;
                    //MessageBox.Show("נא לסמן תביעה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                }
                else
                {
                    lv = dgTravelClaims;
                    claimToSelect = dgTravelClaims.SelectedItem;
                    client = clientLogic.GetClientByClientID(((TravelClaim)dgTravelClaims.SelectedItem).TravelPolicy.ClientID);
                }
            }
            else if (tbItemForeingWorkersClaims.IsSelected)
            {
                if (dgForeingWorkersClaims.SelectedItem == null)
                {
                    return null;
                    //MessageBox.Show("נא לסמן תביעה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                }
                else
                {
                    lv = dgForeingWorkersClaims;
                    claimToSelect = dgForeingWorkersClaims.SelectedItem;
                    client = clientLogic.GetClientByClientID(((ForeingWorkersClaim)dgForeingWorkersClaims.SelectedItem).ForeingWorkersPolicy.ClientID);
                }
            }

            return client;
        }

        private void btnPolicyDetails_Click(object sender, RoutedEventArgs e)
        {
            ListView lv = null;
            object claimToSelect = null;
            if (tbItemElementaryClaims.IsSelected)
            {
                if (dgClaims.SelectedItem == null)
                {
                    MessageBox.Show("נא לסמן תביעה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                }
                else
                {
                    lv = dgClaims;
                    claimToSelect = dgClaims.SelectedItem;
                    Client client = clientLogic.GetClientByClientID(((ElementaryClaim)dgClaims.SelectedItem).ElementaryPolicy.ClientID);
                    frmElementary policyDetails = new frmElementary(((ElementaryClaim)dgClaims.SelectedItem).ElementaryPolicy, user, false, client);
                    policyDetails.ShowDialog();
                }
            }
            else if (tbItemLifeClaims.IsSelected)
            {
                if (dgLifeClaims.SelectedItem == null)
                {
                    MessageBox.Show("נא לסמן תביעה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                }
                else
                {
                    lv = dgLifeClaims;
                    claimToSelect = dgLifeClaims.SelectedItem;
                    Client client = clientLogic.GetClientByClientID(((LifeClaim)dgLifeClaims.SelectedItem).LifePolicy.ClientID);
                    frmLifePolicy policyDetails = new frmLifePolicy(client, user, false, ((LifeClaim)dgLifeClaims.SelectedItem).LifePolicy);   //(((LifeClaim)dgLifeClaims.SelectedItem).LifePolicy, user, false, client);
                    policyDetails.ShowDialog();
                }
            }
            else if (tbItemHealthClaims.IsSelected)
            {
                if (dgHealthClaims.SelectedItem == null)
                {
                    MessageBox.Show("נא לסמן תביעה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                }
                else
                {
                    lv = dgHealthClaims;
                    claimToSelect = dgHealthClaims.SelectedItem;
                    Client client = clientLogic.GetClientByClientID(((HealthClaim)dgHealthClaims.SelectedItem).HealthPolicy.ClientID);
                    frmHealthPolicy policyDetails = new frmHealthPolicy(client, user, false, ((HealthClaim)dgHealthClaims.SelectedItem).HealthPolicy);   //(((LifeClaim)dgLifeClaims.SelectedItem).LifePolicy, user, false, client);
                    policyDetails.ShowDialog();
                }
            }
            else if (tbItemPersonalAccidentsClaims.IsSelected)
            {
                if (dgPersonalAccidentsClaims.SelectedItem == null)
                {
                    MessageBox.Show("נא לסמן תביעה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                }
                else
                {
                    lv = dgPersonalAccidentsClaims;
                    claimToSelect = dgPersonalAccidentsClaims.SelectedItem;
                    Client client = clientLogic.GetClientByClientID(((PersonalAccidentsClaim)dgPersonalAccidentsClaims.SelectedItem).PersonalAccidentsPolicy.ClientID);
                    frmPersonalAccidents policyDetails = new frmPersonalAccidents(client, user, false, ((PersonalAccidentsClaim)dgPersonalAccidentsClaims.SelectedItem).PersonalAccidentsPolicy);   //(((LifeClaim)dgLifeClaims.SelectedItem).LifePolicy, user, false, client);
                    policyDetails.ShowDialog();
                }
            }
            else if (tbItemForeingWorkersClaims.IsSelected)
            {
                if (dgForeingWorkersClaims.SelectedItem == null)
                {
                    MessageBox.Show("נא לסמן תביעה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                }
                else
                {
                    lv = dgForeingWorkersClaims;
                    claimToSelect = dgForeingWorkersClaims.SelectedItem;
                    Client client = clientLogic.GetClientByClientID(((ForeingWorkersClaim)dgForeingWorkersClaims.SelectedItem).ForeingWorkersPolicy.ClientID);
                    frmForeignWorkers policyDetails = new frmForeignWorkers(client, user, false, ((ForeingWorkersClaim)dgForeingWorkersClaims.SelectedItem).ForeingWorkersPolicy);   //(((LifeClaim)dgLifeClaims.SelectedItem).LifePolicy, user, false, client);
                    policyDetails.ShowDialog();
                }
            }
            else if (tbItemTravelClaims.IsSelected)
            {
                if (dgTravelClaims.SelectedItem == null)
                {
                    MessageBox.Show("נא לסמן תביעה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                }
                else
                {
                    lv = dgTravelClaims;
                    claimToSelect = dgTravelClaims.SelectedItem;
                    Client client = clientLogic.GetClientByClientID(((TravelClaim)dgTravelClaims.SelectedItem).TravelPolicy.ClientID);
                    frmTravel policyDetails = new frmTravel(client, user, false, ((TravelClaim)dgTravelClaims.SelectedItem).TravelPolicy);   //(((LifeClaim)dgLifeClaims.SelectedItem).LifePolicy, user, false, client);
                    policyDetails.ShowDialog();
                }
            }
            RefreshClaims();
            DisplayClaimsByFilter();
            if (lv != null && claimToSelect != null)
            {
                SelectClaimInDg(claimToSelect, lv);
            }
        }

        private void cbClaimStatus_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //dgClaimsBinding();
            DisplayClaimsByFilter();
        }

        private void DisplayClaimsByFilter()
        {
            int count = 0;
            string status = null;
            if (cbClaimStatus.SelectedItem != null)
            {
                status = ((ComboBoxItem)cbClaimStatus.SelectedItem).Content.ToString();
            }
            if (tbItemElementaryClaims.IsSelected)
            {
                dgClaims.ItemsSource = claimsLogic.GetElementaryClaimsByFilter(elementaryClaims, status, (ElementaryClaimCondition)cbClaimCondition.SelectedItem, (ElementaryClaimType)cbClaimType.SelectedItem, (InsuranceIndustry)cbIndustry.SelectedItem, (Company)cbCompany.SelectedItem, txtSearchClaim.Text);
                count = ((List<ElementaryClaim>)dgClaims.ItemsSource).Count;
            }
            else if (tbItemLifeClaims.IsSelected)
            {
                dgLifeClaims.ItemsSource = claimsLogic.GetLifeClaimsByFilter(lifeClaims, status, (LifeClaimCondition)cbClaimCondition.SelectedItem, (LifeClaimType)cbClaimType.SelectedItem, (LifeIndustry)cbIndustry.SelectedItem, (Company)cbCompany.SelectedItem, txtSearchClaim.Text);
                count = ((List<LifeClaim>)dgLifeClaims.ItemsSource).Count;
            }
            else if (tbItemHealthClaims.IsSelected)
            {
                dgHealthClaims.ItemsSource = claimsLogic.GetHealthClaimsByFilter(healthClaims, status, (HealthClaimCondition)cbClaimCondition.SelectedItem, (HealthClaimType)cbClaimType.SelectedItem, (Company)cbCompany.SelectedItem, txtSearchClaim.Text);
                count = ((List<HealthClaim>)dgHealthClaims.ItemsSource).Count;
            }
            else if (tbItemPersonalAccidentsClaims.IsSelected)
            {
                dgPersonalAccidentsClaims.ItemsSource = claimsLogic.GetPersonalAccidentsClaimsByFilter(personalAccidentsClaims, status, (PersonalAccidentsClaimCondition)cbClaimCondition.SelectedItem, (PersonalAccidentsClaimType)cbClaimType.SelectedItem, (Company)cbCompany.SelectedItem, txtSearchClaim.Text);
                count = ((List<PersonalAccidentsClaim>)dgPersonalAccidentsClaims.ItemsSource).Count;
            }
            else if (tbItemForeingWorkersClaims.IsSelected)
            {
                dgForeingWorkersClaims.ItemsSource = claimsLogic.GetForeingWorkersClaimsByFilter(foreingWorkersClaims, status, (ForeingWorkersClaimCondition)cbClaimCondition.SelectedItem, (ForeingWorkersClaimType)cbClaimType.SelectedItem, (ForeingWorkersIndustry)cbIndustry.SelectedItem, (Company)cbCompany.SelectedItem, txtSearchClaim.Text);
                count = ((List<ForeingWorkersClaim>)dgForeingWorkersClaims.ItemsSource).Count;
            }
            else if (tbItemTravelClaims.IsSelected)
            {
                dgTravelClaims.ItemsSource = claimsLogic.GetTravelClaimsByFilter(travelClaims, status, (TravelClaimCondition)cbClaimCondition.SelectedItem, (TravelClaimType)cbClaimType.SelectedItem, (Company)cbCompany.SelectedItem, txtSearchClaim.Text);
                count = ((List<TravelClaim>)dgTravelClaims.ItemsSource).Count;
            }
            lblCount.Content = string.Format("סה''כ רשומות: {0}", count.ToString());
        }

        private void txtSearchClaim_TextChanged(object sender, TextChangedEventArgs e)
        {
            DisplayClaimsByFilter();
        }

        private void btnClear_Click(object sender, RoutedEventArgs e)
        {
            ClearSearchInputs();
        }

        private void btnOpticArchive_Click(object sender, RoutedEventArgs e)
        {
            if (tbItemElementaryClaims.IsSelected)
            {
                if (dgClaims.SelectedItem == null)
                {
                    MessageBox.Show("נא לסמן תביעה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    return;
                }
                Client client = clientLogic.GetClientByClientID((((ElementaryClaim)dgClaims.SelectedItem).ElementaryPolicy).ClientID);
                //frmScan2 archiveWindow = new frmScan2(client, ((ElementaryClaim)dgClaims.SelectedItem).ElementaryPolicy, null,user);
                frmScan2 archiveWindow = new frmScan2(client, (object)dgClaims.SelectedItem, null, user);
                archiveWindow.ShowDialog();
            }
            else if (tbItemLifeClaims.IsSelected)
            {
                if (dgLifeClaims.SelectedItem == null)
                {
                    MessageBox.Show("נא לסמן תביעה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    return;
                }
                Client client = clientLogic.GetClientByClientID((((LifeClaim)dgLifeClaims.SelectedItem).LifePolicy).ClientID);
                frmScan2 archiveWindow = new frmScan2(client, (object)dgLifeClaims.SelectedItem, null, user);
                archiveWindow.ShowDialog();
            }
            else if (tbItemHealthClaims.IsSelected)
            {
                if (dgHealthClaims.SelectedItem == null)
                {
                    MessageBox.Show("נא לסמן תביעה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    return;
                }
                Client client = clientLogic.GetClientByClientID((((HealthClaim)dgHealthClaims.SelectedItem).HealthPolicy).ClientID);
                frmScan2 archiveWindow = new frmScan2(client, (object)dgHealthClaims.SelectedItem, null, user);
                archiveWindow.ShowDialog();
            }
            else if (tbItemTravelClaims.IsSelected)
            {
                if (dgTravelClaims.SelectedItem == null)
                {
                    MessageBox.Show("נא לסמן תביעה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    return;
                }
                Client client = clientLogic.GetClientByClientID((((TravelClaim)dgTravelClaims.SelectedItem).TravelPolicy).ClientID);
                frmScan2 archiveWindow = new frmScan2(client, (object)dgTravelClaims.SelectedItem, null, user);
                archiveWindow.ShowDialog();
            }
            else if (tbItemPersonalAccidentsClaims.IsSelected)
            {
                if (dgPersonalAccidentsClaims.SelectedItem == null)
                {
                    MessageBox.Show("נא לסמן תביעה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    return;
                }
                Client client = clientLogic.GetClientByClientID((((PersonalAccidentsClaim)dgPersonalAccidentsClaims.SelectedItem).PersonalAccidentsPolicy).ClientID);
                frmScan2 archiveWindow = new frmScan2(client, (object)dgPersonalAccidentsClaims.SelectedItem, null, user);
                archiveWindow.ShowDialog();
            }
            else if (tbItemForeingWorkersClaims.IsSelected)
            {
                if (dgForeingWorkersClaims.SelectedItem == null)
                {
                    MessageBox.Show("נא לסמן תביעה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    return;
                }
                Client client = clientLogic.GetClientByClientID((((ForeingWorkersClaim)dgForeingWorkersClaims.SelectedItem).ForeingWorkersPolicy).ClientID);
                frmScan2 archiveWindow = new frmScan2(client, (object)dgForeingWorkersClaims.SelectedItem, null, user);
                archiveWindow.ShowDialog();
            }
        }

        private void SelectClaimInDg(object claimToSelect, ListView lv)
        {
            var dgItems = lv.Items;
            foreach (var item in dgItems)
            {
                if (claimToSelect is ElementaryClaim)
                {
                    ElementaryClaim policy = (ElementaryClaim)item;
                    if (policy.ElementaryClaimID == ((ElementaryClaim)claimToSelect).ElementaryClaimID)
                    {
                        lv.SelectedItem = item;
                        lv.ScrollIntoView(item);
                        ListViewItem listViewItem = lv.ItemContainerGenerator.ContainerFromItem(lv.SelectedItem) as ListViewItem;
                        if (listViewItem != null)
                        {
                            listViewItem.Focus();
                        }
                        break;
                    }
                }
                else if (claimToSelect is ForeingWorkersClaim)
                {
                    ForeingWorkersClaim policy = (ForeingWorkersClaim)item;
                    if (policy.ForeingWorkersClaimID == ((ForeingWorkersClaim)claimToSelect).ForeingWorkersClaimID)
                    {
                        lv.SelectedItem = item;
                        lv.ScrollIntoView(item);
                        ListViewItem listViewItem = lv.ItemContainerGenerator.ContainerFromItem(lv.SelectedItem) as ListViewItem;
                        if (listViewItem != null)
                        {
                            listViewItem.Focus();
                        }
                        break;
                    }
                }
                else if (claimToSelect is LifeClaim)
                {
                    LifeClaim policy = (LifeClaim)item;
                    if (policy.LifeClaimID == ((LifeClaim)claimToSelect).LifeClaimID)
                    {
                        lv.SelectedItem = item;
                        lv.ScrollIntoView(item);
                        ListViewItem listViewItem = lv.ItemContainerGenerator.ContainerFromItem(lv.SelectedItem) as ListViewItem;
                        if (listViewItem != null)
                        {
                            listViewItem.Focus();
                        }
                        break;
                    }
                }
                else if (claimToSelect is HealthClaim)
                {
                    HealthClaim policy = (HealthClaim)item;
                    if (policy.HealthClaimID == ((HealthClaim)claimToSelect).HealthClaimID)
                    {
                        lv.SelectedItem = item;
                        lv.ScrollIntoView(item);
                        ListViewItem listViewItem = lv.ItemContainerGenerator.ContainerFromItem(lv.SelectedItem) as ListViewItem;
                        if (listViewItem != null)
                        {
                            listViewItem.Focus();
                        }
                        break;
                    }
                }
                else if (claimToSelect is TravelClaim)
                {
                    TravelClaim policy = (TravelClaim)item;
                    if (policy.TravelClaimID == ((TravelClaim)claimToSelect).TravelClaimID)
                    {
                        lv.SelectedItem = item;
                        lv.ScrollIntoView(item);
                        ListViewItem listViewItem = lv.ItemContainerGenerator.ContainerFromItem(lv.SelectedItem) as ListViewItem;
                        if (listViewItem != null)
                        {
                            listViewItem.Focus();
                        }
                        break;
                    }
                }
                else if (claimToSelect is PersonalAccidentsClaim)
                {
                    PersonalAccidentsClaim policy = (PersonalAccidentsClaim)item;
                    if (policy.PersonalAccidentsClaimID == ((PersonalAccidentsClaim)claimToSelect).PersonalAccidentsClaimID)
                    {
                        lv.SelectedItem = item;
                        lv.ScrollIntoView(item);
                        ListViewItem listViewItem = lv.ItemContainerGenerator.ContainerFromItem(lv.SelectedItem) as ListViewItem;
                        if (listViewItem != null)
                        {
                            listViewItem.Focus();
                        }
                        break;
                    }
                }
            }
        }

        private void btnClaimDetails_Click(object sender, RoutedEventArgs e)
        {
            object claimToSelect = null;
            ListView lv = null;

            if (tbItemElementaryClaims.IsSelected)
            {
                frmElementaryClaims updateElementaryClaim = null;
                if (dgClaims.SelectedItem == null)
                {
                    MessageBox.Show("נא לסמן את התביעה שברצונך לעדכן", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                }
                else
                {
                    claimToSelect = (ElementaryClaim)dgClaims.SelectedItem;
                    lv = dgClaims;
                    updateElementaryClaim = new frmElementaryClaims((ElementaryClaim)dgClaims.SelectedItem, ((ElementaryClaim)dgClaims.SelectedItem).ElementaryPolicy.Client, user);
                    updateElementaryClaim.ShowDialog();
                }
            }
            else if (tbItemLifeClaims.IsSelected)
            {
                frmLifeClaims updateLifeClaim = null;
                if (dgLifeClaims.SelectedItem == null)
                {
                    MessageBox.Show("נא לסמן את התביעה שברצונך לעדכן", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                }
                else
                {
                    claimToSelect = (LifeClaim)dgLifeClaims.SelectedItem;
                    lv = dgLifeClaims;

                    updateLifeClaim = new frmLifeClaims((LifeClaim)dgLifeClaims.SelectedItem, ((LifeClaim)dgLifeClaims.SelectedItem).LifePolicy.Client, user);
                    updateLifeClaim.ShowDialog();
                }
            }
            else if (tbItemHealthClaims.IsSelected)
            {
                frmHealthClaim updateHealthClaim = null;
                if (dgHealthClaims.SelectedItem == null)
                {
                    MessageBox.Show("נא לסמן את התביעה שברצונך לעדכן", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                }
                else
                {
                    claimToSelect = (HealthClaim)dgHealthClaims.SelectedItem;
                    lv = dgHealthClaims;
                    updateHealthClaim = new frmHealthClaim((HealthClaim)dgHealthClaims.SelectedItem, ((HealthClaim)dgHealthClaims.SelectedItem).HealthPolicy.Client, user);
                    updateHealthClaim.ShowDialog();
                }
            }
            else if (tbItemPersonalAccidentsClaims.IsSelected)
            {
                frmPersonalAccidentsClaims updatePersonalAccidentsClaim = null;
                if (dgPersonalAccidentsClaims.SelectedItem == null)
                {
                    MessageBox.Show("נא לסמן את התביעה שברצונך לעדכן", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                }
                else
                {
                    claimToSelect = (PersonalAccidentsClaim)dgPersonalAccidentsClaims.SelectedItem;
                    lv = dgPersonalAccidentsClaims;
                    updatePersonalAccidentsClaim = new frmPersonalAccidentsClaims((PersonalAccidentsClaim)dgPersonalAccidentsClaims.SelectedItem, ((PersonalAccidentsClaim)dgPersonalAccidentsClaims.SelectedItem).PersonalAccidentsPolicy.Client, user);
                    updatePersonalAccidentsClaim.ShowDialog();
                }

            }
            else if (tbItemForeingWorkersClaims.IsSelected)
            {
                frmForeingWorkersClaims updateForeingWorkersClaim = null;
                if (dgForeingWorkersClaims.SelectedItem == null)
                {
                    MessageBox.Show("נא לסמן את התביעה שברצונך לעדכן", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                }
                else
                {
                    claimToSelect = (ForeingWorkersClaim)dgForeingWorkersClaims.SelectedItem;
                    lv = dgForeingWorkersClaims;
                    updateForeingWorkersClaim = new frmForeingWorkersClaims((ForeingWorkersClaim)dgForeingWorkersClaims.SelectedItem, ((ForeingWorkersClaim)dgForeingWorkersClaims.SelectedItem).ForeingWorkersPolicy.Client, user);
                    updateForeingWorkersClaim.ShowDialog();
                }

            }
            else if (tbItemTravelClaims.IsSelected)
            {
                frmTravelClaims updateTravelClaim = null;
                if (dgTravelClaims.SelectedItem == null)
                {
                    MessageBox.Show("נא לסמן את התביעה שברצונך לעדכן", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                }
                else
                {
                    claimToSelect = (TravelClaim)dgTravelClaims.SelectedItem;
                    lv = dgTravelClaims;
                    updateTravelClaim = new frmTravelClaims((TravelClaim)dgTravelClaims.SelectedItem, ((TravelClaim)dgTravelClaims.SelectedItem).TravelPolicy.Client, user);
                    updateTravelClaim.ShowDialog();
                }
            }
            RefreshClaims();
            DisplayClaimsByFilter();
            if (claimToSelect != null && lv != null)
            {
                SelectClaimInDg(claimToSelect, lv);
            }
        }

        private void dgClaims_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            DependencyObject src = (DependencyObject)(e.OriginalSource);
            while (!(src is Control))
            {
                if (!(src is Visual))
                {
                    break;
                }
                src = VisualTreeHelper.GetParent(src);
            }

            if ((src is Control && src.GetType() == typeof(ListViewItem)) || src.GetType() == typeof(Run))
            {
                if (tbItemElementaryClaims.IsSelected)
                {
                    frmElementaryClaims updateElementaryClaim = null;
                    if (dgClaims.SelectedItem == null)
                    {
                        MessageBox.Show("נא לסמן את התביעה שברצונך לעדכן", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    }
                    else
                    {
                        updateElementaryClaim = new frmElementaryClaims((ElementaryClaim)dgClaims.SelectedItem, ((ElementaryClaim)dgClaims.SelectedItem).ElementaryPolicy.Client, user);
                        updateElementaryClaim.ShowDialog();
                    }
                }
                else if (tbItemLifeClaims.IsSelected)
                {
                    frmLifeClaims updateLifeClaim = null;
                    if (dgLifeClaims.SelectedItem == null)
                    {
                        MessageBox.Show("נא לסמן את התביעה שברצונך לעדכן", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    }
                    else
                    {
                        updateLifeClaim = new frmLifeClaims((LifeClaim)dgLifeClaims.SelectedItem, ((LifeClaim)dgLifeClaims.SelectedItem).LifePolicy.Client, user);
                        updateLifeClaim.ShowDialog();
                    }
                }
                else if (tbItemHealthClaims.IsSelected)
                {
                    frmHealthClaim updateHealthClaim = null;
                    if (dgHealthClaims.SelectedItem == null)
                    {
                        MessageBox.Show("נא לסמן את התביעה שברצונך לעדכן", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    }
                    else
                    {
                        updateHealthClaim = new frmHealthClaim((HealthClaim)dgHealthClaims.SelectedItem, ((HealthClaim)dgHealthClaims.SelectedItem).HealthPolicy.Client, user);
                        updateHealthClaim.ShowDialog();
                    }
                }
                else if (tbItemPersonalAccidentsClaims.IsSelected)
                {
                    frmPersonalAccidentsClaims updatePersonalAccidentsClaim = null;
                    if (dgPersonalAccidentsClaims.SelectedItem == null)
                    {
                        MessageBox.Show("נא לסמן את התביעה שברצונך לעדכן", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    }
                    else
                    {
                        updatePersonalAccidentsClaim = new frmPersonalAccidentsClaims((PersonalAccidentsClaim)dgPersonalAccidentsClaims.SelectedItem, ((PersonalAccidentsClaim)dgPersonalAccidentsClaims.SelectedItem).PersonalAccidentsPolicy.Client, user);
                        updatePersonalAccidentsClaim.ShowDialog();
                    }
                }
                else if (tbItemForeingWorkersClaims.IsSelected)
                {
                    frmForeingWorkersClaims updateForeingWorkersClaim = null;
                    if (dgForeingWorkersClaims.SelectedItem == null)
                    {
                        MessageBox.Show("נא לסמן את התביעה שברצונך לעדכן", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    }
                    else
                    {
                        updateForeingWorkersClaim = new frmForeingWorkersClaims((ForeingWorkersClaim)dgForeingWorkersClaims.SelectedItem, ((ForeingWorkersClaim)dgForeingWorkersClaims.SelectedItem).ForeingWorkersPolicy.Client, user);
                        updateForeingWorkersClaim.ShowDialog();
                    }
                }
                else if (tbItemTravelClaims.IsSelected)
                {
                    frmTravelClaims updateTravelClaim = null;
                    if (dgTravelClaims.SelectedItem == null)
                    {
                        MessageBox.Show("נא לסמן את התביעה שברצונך לעדכן", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    }
                    else
                    {
                        updateTravelClaim = new frmTravelClaims((TravelClaim)dgTravelClaims.SelectedItem, ((TravelClaim)dgTravelClaims.SelectedItem).TravelPolicy.Client, user);
                        updateTravelClaim.ShowDialog();
                    }
                }
                RefreshClaims();
                DisplayClaimsByFilter();
            }
        }

        private void tbClaims_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.Source is TabControl)
            {
                dgClaimsBinding();
            }
        }
        private void btnSendEmail_Click(object sender, RoutedEventArgs e)
        {
            ListView lv = null;
            object claimToSelect = null;
            Client client = GetClient(ref lv, ref claimToSelect);
            if (client == null)
            {
                MessageBox.Show("נא לסמן תביעה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            if (client.Email == "")
            {
                MessageBox.Show("אין ללקוח כתובת דוא''ל במערכת", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            string[] address = new string[] { client.Email };
            try
            {
                email.SendEmailFromOutlook("", "", null, address);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void mItemExcel_Click(object sender, RoutedEventArgs e)
        {
            excelLogic.ElementaryClaimsToExcel((List<ElementaryClaim>)dgClaims.ItemsSource);
        }

        private void mItemPersonalAccidentsExcel_Click(object sender, RoutedEventArgs e)
        {
            excelLogic.PersonalAccidentsClaimsToExcel((List<PersonalAccidentsClaim>)dgPersonalAccidentsClaims.ItemsSource);
        }

        private void mItemTravelExcel_Click(object sender, RoutedEventArgs e)
        {
            excelLogic.TravelClaimsToExcel((List<TravelClaim>)dgTravelClaims.ItemsSource);
        }

        private void mItemLifeExcel_Click(object sender, RoutedEventArgs e)
        {
            excelLogic.LifeClaimsToExcel((List<LifeClaim>)dgLifeClaims.ItemsSource);
        }

        private void mItemHealthExcel_Click(object sender, RoutedEventArgs e)
        {
            excelLogic.HealthClaimsToExcel((List<HealthClaim>)dgHealthClaims.ItemsSource);
        }

        private void mItemForeingWorkersExcel_Click(object sender, RoutedEventArgs e)
        {
            excelLogic.ForeingWorkersClaimsToExcel((List<ForeingWorkersClaim>)dgForeingWorkersClaims.ItemsSource);
        }

        private void dgClaims_MouseDown(object sender, MouseButtonEventArgs e)
        {
            dgClaims.UnselectAll();
        }

        private void dgPersonalAccidentsClaims_MouseDown(object sender, MouseButtonEventArgs e)
        {
            dgPersonalAccidentsClaims.UnselectAll();
        }

        private void dgTravelClaims_MouseDown(object sender, MouseButtonEventArgs e)
        {
            dgTravelClaims.UnselectAll();
        }

        private void dgLifeClaims_MouseDown(object sender, MouseButtonEventArgs e)
        {
            dgLifeClaims.UnselectAll();
        }

        private void dgHealthClaims_MouseDown(object sender, MouseButtonEventArgs e)
        {
            dgHealthClaims.UnselectAll();
        }

        private void dgForeingWorkersClaims_MouseDown(object sender, MouseButtonEventArgs e)
        {
            dgForeingWorkersClaims.UnselectAll();
        }

        private void mItemPrintElementary_Click(object sender, RoutedEventArgs e)
        {
            List<ClaimsReport> reportList = null;
            if (tbItemElementaryClaims.IsSelected)
            {
                reportList = ConvertToReportView((List<ElementaryClaim>)dgClaims.ItemsSource);
            }
            else if (tbItemForeingWorkersClaims.IsSelected)
            {
                reportList = ConvertToReportView((List<ForeingWorkersClaim>)dgForeingWorkersClaims.ItemsSource);
            }
            else if (tbItemHealthClaims.IsSelected)
            {
                reportList = ConvertToReportView((List<HealthClaim>)dgHealthClaims.ItemsSource);
            }
            else if (tbItemLifeClaims.IsSelected)
            {
                reportList = ConvertToReportView((List<LifeClaim>)dgLifeClaims.ItemsSource);
            }
            else if (tbItemPersonalAccidentsClaims.IsSelected)
            {
                reportList = ConvertToReportView((List<PersonalAccidentsClaim>)dgPersonalAccidentsClaims.ItemsSource);
            }
            else if (tbItemTravelClaims.IsSelected)
            {
                reportList = ConvertToReportView((List<TravelClaim>)dgTravelClaims.ItemsSource);
            }
            if (reportList != null)
            {
                try
                {
                    frmPrintPreview preview = new frmPrintPreview("ClaimsReport", reportList);
                    preview.ShowDialog();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private List<ClaimsReport> ConvertToReportView(object list)
        {
            List<ClaimsReport> reportList = new List<ClaimsReport>();
            if (list is List<ElementaryClaim>)
            {
                foreach (var item in (List<ElementaryClaim>)list)
                {
                    ClaimsReport reportItem = new ClaimsReport();
                    if (item.ElementaryPolicy.Client != null)
                    {
                        reportItem.ClientName = item.ElementaryPolicy.Client.LastName + " " + item.ElementaryPolicy.Client.FirstName;
                        reportItem.IdNumber = item.ElementaryPolicy.Client.IdNumber;
                    }
                    if (item.ElementaryPolicy.Company != null)
                    {
                        reportItem.CompanyName = item.ElementaryPolicy.Company.CompanyName;
                    }
                    if (item.ElementaryPolicy.InsuranceIndustry != null)
                    {
                        reportItem.Industry = item.ElementaryPolicy.InsuranceIndustry.InsuranceIndustryName;
                    }
                    reportItem.PolicyNumber = item.ElementaryPolicy.PolicyNumber;
                    if (item.ElementaryPolicy.CarPolicy != null)
                    {
                        reportItem.RegistrationNumber = item.ElementaryPolicy.CarPolicy.RegistrationNumber;
                    }
                    if (item.EventDate != null)
                    {
                        reportItem.EventDate = ((DateTime)item.EventDate).ToString("dd/MM/yyyy");
                    }
                    reportItem.ClaimNumber = item.ClaimNumber;
                    reportItem.ThirdPartyClaimNumber = item.ThirdPartyClaimNumber;
                    //reportItem.ThirdPartyCompanyName = item.CarThirdParty.Company.CompanyName;
                    reportItem.Header = string.Format(@"דו''ח תביעות {0}{1}", Environment.NewLine, lblCount.Content);

                    reportList.Add(reportItem);
                }
            }
            else if (list is List<LifeClaim>)
            {
                foreach (var item in (List<LifeClaim>)list)
                {
                    ClaimsReport reportItem = new ClaimsReport();
                    if (item.LifePolicy.Client != null)
                    {
                        reportItem.ClientName = item.LifePolicy.Client.LastName + " " + item.LifePolicy.Client.FirstName;
                        reportItem.IdNumber = item.LifePolicy.Client.IdNumber;
                    }
                    if (item.LifePolicy.Company != null)
                    {
                        reportItem.CompanyName = item.LifePolicy.Company.CompanyName;
                    }
                    if (item.LifePolicy.LifeIndustry != null)
                    {
                        reportItem.Industry = item.LifePolicy.LifeIndustry.LifeIndustryName;
                    }
                    reportItem.PolicyNumber = item.LifePolicy.PolicyNumber;
                    if (item.EventDateAndTime != null)
                    {
                        reportItem.EventDate = ((DateTime)item.EventDateAndTime).ToString("dd/MM/yyyy");
                    }
                    reportItem.ClaimNumber = item.ClaimNumber;
                    reportItem.Header = string.Format(@"דו''ח תביעות {0}{1}", Environment.NewLine, lblCount.Content);

                    reportList.Add(reportItem);
                }
            }
            else if (list is List<HealthClaim>)
            {
                foreach (var item in (List<HealthClaim>)list)
                {
                    ClaimsReport reportItem = new ClaimsReport();
                    if (item.HealthPolicy.Client != null)
                    {
                        reportItem.ClientName = item.HealthPolicy.Client.LastName + " " + item.HealthPolicy.Client.FirstName;
                        reportItem.IdNumber = item.HealthPolicy.Client.IdNumber;
                    }
                    if (item.HealthPolicy.Company != null)
                    {
                        reportItem.CompanyName = item.HealthPolicy.Company.CompanyName;
                    }
                    reportItem.PolicyNumber = item.HealthPolicy.PolicyNumber;
                    if (item.EventDateAndTime != null)
                    {
                        reportItem.EventDate = ((DateTime)item.EventDateAndTime).ToString("dd/MM/yyyy");
                    }
                    reportItem.ClaimNumber = item.ClaimNumber;
                    reportItem.Header = string.Format(@"דו''ח תביעות {0}{1}", Environment.NewLine, lblCount.Content);

                    reportList.Add(reportItem);
                }
            }
            else if (list is List<TravelClaim>)
            {
                foreach (var item in (List<TravelClaim>)list)
                {
                    ClaimsReport reportItem = new ClaimsReport();
                    if (item.TravelPolicy.Client != null)
                    {
                        reportItem.ClientName = item.TravelPolicy.Client.LastName + " " + item.TravelPolicy.Client.FirstName;
                        reportItem.IdNumber = item.TravelPolicy.Client.IdNumber;
                    }
                    if (item.TravelPolicy.Company != null)
                    {
                        reportItem.CompanyName = item.TravelPolicy.Company.CompanyName;
                    }
                    reportItem.PolicyNumber = item.TravelPolicy.PolicyNumber;
                    if (item.EventDateAndTime != null)
                    {
                        reportItem.EventDate = ((DateTime)item.EventDateAndTime).ToString("dd/MM/yyyy");
                    }
                    reportItem.ClaimNumber = item.ClaimNumber;
                    reportItem.Header = string.Format(@"דו''ח תביעות {0}{1}", Environment.NewLine, lblCount.Content);

                    reportList.Add(reportItem);
                }
            }
            else if (list is List<PersonalAccidentsClaim>)
            {
                foreach (var item in (List<PersonalAccidentsClaim>)list)
                {
                    ClaimsReport reportItem = new ClaimsReport();
                    if (item.PersonalAccidentsPolicy.Client != null)
                    {
                        reportItem.ClientName = item.PersonalAccidentsPolicy.Client.LastName + " " + item.PersonalAccidentsPolicy.Client.FirstName;
                        reportItem.IdNumber = item.PersonalAccidentsPolicy.Client.IdNumber;
                    }
                    if (item.PersonalAccidentsPolicy.Company != null)
                    {
                        reportItem.CompanyName = item.PersonalAccidentsPolicy.Company.CompanyName;
                    }
                    reportItem.PolicyNumber = item.PersonalAccidentsPolicy.PolicyNumber;
                    if (item.EventDateAndTime != null)
                    {
                        reportItem.EventDate = ((DateTime)item.EventDateAndTime).ToString("dd/MM/yyyy");
                    }
                    reportItem.ClaimNumber = item.ClaimNumber;
                    reportItem.Header = string.Format(@"דו''ח תביעות {0}{1}", Environment.NewLine, lblCount.Content);

                    reportList.Add(reportItem);
                }
            }
            else if (list is List<ForeingWorkersClaim>)
            {
                foreach (var item in (List<ForeingWorkersClaim>)list)
                {
                    ClaimsReport reportItem = new ClaimsReport();
                    if (item.ForeingWorkersPolicy.Client != null)
                    {
                        reportItem.ClientName = item.ForeingWorkersPolicy.Client.LastName + " " + item.ForeingWorkersPolicy.Client.FirstName;
                        reportItem.IdNumber = item.ForeingWorkersPolicy.Client.IdNumber;
                    }
                    if (item.ForeingWorkersPolicy.Company != null)
                    {
                        reportItem.CompanyName = item.ForeingWorkersPolicy.Company.CompanyName;
                    }
                    if (item.ForeingWorkersPolicy.ForeingWorkersIndustry != null)
                    {
                        reportItem.Industry = item.ForeingWorkersPolicy.ForeingWorkersIndustry.ForeingWorkersIndustryName;
                    }
                    reportItem.PolicyNumber = item.ForeingWorkersPolicy.PolicyNumber;
                    if (item.EventDateAndTime != null)
                    {
                        reportItem.EventDate = ((DateTime)item.EventDateAndTime).ToString("dd/MM/yyyy");
                    }
                    reportItem.ClaimNumber = item.ClaimNumber;
                    reportItem.Header = string.Format(@"דו''ח תביעות {0}{1}", Environment.NewLine, lblCount.Content);

                    reportList.Add(reportItem);
                }
            }
            return reportList;
        }
    }
}
