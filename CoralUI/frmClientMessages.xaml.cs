﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using CoralBusinessLogics;

namespace CoralUI
{
    /// <summary>
    /// Interaction logic for frmClientMessages.xaml
    /// </summary>
    public partial class frmClientMessages : Window
    {
        Client client;
        User user;
        WorkTasksLogic workTasksLogic = new WorkTasksLogic();
        PoliciesLogic policiesLogic = new PoliciesLogic();
        UserLogic userLogic = new UserLogic();
        RolesLogic rolesLogic = new RolesLogic();
        List<Request> allRequests;            
        List<Request> requestsByDueDate;
        List<Request> requestsByContactDate;
        int index;
        bool isUserWorkerOnly = false;
        ListViewSettings lv = new ListViewSettings();
        ExcelLogic excelLogic = new ExcelLogic();
        Email email = new Email();

        public frmClientMessages(Client tasksClient, User userAccount)
        {
            InitializeComponent();
            this.SizeToContent = SizeToContent.Manual;
            client = tasksClient;
            user = userAccount;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            
            lblClientName.Content = client.FirstName + " " + client.LastName;
            //clientRequests = workTasksLogic.GetAllRequestsByClient(client.ClientID, user);
            rbDisplayAll.IsChecked = true;
            cbUserInChargeBinding();
            cbTaskCategoryBinding();
            cbConditionBinding();
            
            //dateFilteredRequests = clientRequests;
            
            cbStatus.SelectedIndex = 0;
            if (user.IsScanPermission==false)
            {
                btnOpticArchive.IsEnabled = false;
            }
            //dgMessagesBinding();
        }

        private void cbUserInChargeBinding()
        {
            List<User> allUsers = (List<User>)userLogic.GetAllUsers();
            List<User> filteredUsers = (List<User>)userLogic.GetAllUsers();
            cbTransferTaskTo.ItemsSource = allUsers;
            cbTransferTaskTo.DisplayMemberPath = "Usermame";
            cbUserInCharge.ItemsSource = allUsers;
            cbUserInCharge.DisplayMemberPath = "Usermame";
            var users = cbUserInCharge.Items;
            foreach (var item in users)
            {
                bool isUserSelected = false;
                User userToSelect = (User)item;
                if (userToSelect.UserID == user.UserID && !isUserSelected)
                {
                    cbUserInCharge.SelectedItem = item;
                    isUserSelected = true;
                    index = cbUserInCharge.Items.IndexOf(item);
                }
                if (!rolesLogic.IsUserInRole(user.Usermame, "מנהל ראשי"))
                {
                    if (rolesLogic.IsUserInRole(user.Usermame, "מנהל"))
                    {
                        if (rolesLogic.IsUserInRole(userToSelect.Usermame, "מנהל ראשי"))
                        {
                            filteredUsers.RemoveAll(u => u.UserID == userToSelect.UserID);
                            cbUserInCharge.ItemsSource = filteredUsers;
                            users = cbUserInCharge.Items;
                            foreach (var userItem in users)
                            {

                                userToSelect = (User)userItem;
                                if (userToSelect.UserID == user.UserID)
                                {
                                    cbUserInCharge.SelectedItem = userItem;
                                    index = cbUserInCharge.Items.IndexOf(userItem);
                                }
                            }
                        }
                    }
                    else if (rolesLogic.IsUserInRole(user.Usermame, "עובד"))
                    {
                        cbUserInCharge.IsEnabled = false;
                        isUserWorkerOnly = true;
                    }
                }
            }
        }


        private void dgMessagesBinding()
        {
            List<Request> filteredRequests = new List<Request>();
            if (rbDueDate.IsChecked == true)
            {
                filteredRequests.AddRange(requestsByDueDate);
            }
            else if (rbContactDate.IsChecked == true)
            {
                filteredRequests.AddRange(requestsByContactDate);
            }
            else
            {
                filteredRequests.AddRange(allRequests);
            }
            if (cbUserInCharge.SelectedItem != null)
            {
                filteredRequests = filteredRequests.Where(r => r.HandledByUserID == ((User)cbUserInCharge.SelectedItem).UserID).ToList();
            }
            if (cbCondition.SelectedItem != null)
            {
                filteredRequests = filteredRequests.Where(r => r.RequestConditionID == ((RequestCondition)cbCondition.SelectedItem).RequestConditionID).ToList();
            }
            if (cbCategory.SelectedItem != null)
            {
                filteredRequests = filteredRequests.Where(r => r.RequestCategoryID == ((RequestCategory)cbCategory.SelectedItem).RequestCategoryID).ToList();
            }
            if (cbStatus.SelectedIndex == 0)
            {
                filteredRequests = filteredRequests.Where(r => r.RequestStatus == true).ToList();
            }
            else if (cbStatus.SelectedIndex == 1)
            {
                filteredRequests = filteredRequests.Where(r => r.RequestStatus == false).ToList();
            }
            if (txtSearchRequest.Text != "")
            {
                string searchKey = txtSearchRequest.Text;
                filteredRequests = filteredRequests.Where(r => (r.Client != null && (r.Client.FirstName.StartsWith(searchKey) || r.Client.LastName.StartsWith(searchKey) || r.Client.IdNumber.StartsWith(searchKey))) || (r.RequesterFirstName!=null&&r.RequesterFirstName.StartsWith(searchKey)) || (r.RequesterLastName != null && r.RequesterLastName.StartsWith(searchKey)) || (r.RequestSubject != null && r.RequestSubject.Contains(searchKey))).ToList();
            }
            dgMessages.ItemsSource = filteredRequests;
            if (dgMessages.ItemsSource != null)
            {
                int count = ((List<Request>)dgMessages.ItemsSource).Count;
                lblCount.Content = string.Format("סה''כ רשומות: {0}", count.ToString());
            }
            lv.AutoSizeColumns(dgMessages.View);

        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void btnOpticArchive_Click(object sender, RoutedEventArgs e)
        {
            if (dgMessages.SelectedItem == null)
            {
                MessageBox.Show("נא לסמן משימה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            object policy = null;
            if (((Request)dgMessages.SelectedItem).ElementaryPolicyID != null)
            {
                policy = policiesLogic.GetElementaryPolicyByPolicyId((int)((Request)dgMessages.SelectedItem).ElementaryPolicyID);
            }
            if (((Request)dgMessages.SelectedItem).LifePolicyID != null)
            {
                //policiesLogic.GetElementaryPolicyByPolicyId((int)((Request)dgMessages.SelectedItem).ElementaryPolicyID);
            }

            if (((Request)dgMessages.SelectedItem).FinancePolicyID != null)
            {
                //policiesLogic.GetElementaryPolicyByPolicyId((int)((Request)dgMessages.SelectedItem).ElementaryPolicyID);
            }

            //client = clientLogic.GetClientByClientID(((ElementaryPolicy)dgRenewals.SelectedItem).ClientID);
            frmScan2 archiveWindow = new frmScan2(client, policy, null, user);
            archiveWindow.ShowDialog();
        }

        private void btnAddMessage_Click(object sender, RoutedEventArgs e)
        {
            frmWorkQueue queueWindow = new frmWorkQueue(client, user, null);
            queueWindow.ShowDialog();
            if (queueWindow.RequestID != 0)
            {
                if (rbDueDate.IsChecked == true)
                {
                    GetRequestsByDueDate();
                }
                else if (rbContactDate.IsChecked == true)
                {
                    GetRequestsByContactDate();
                }
                else
                {
                    GetAllRequests();
                }

                SelectRequestInDg(queueWindow.RequestID, dgMessages);
            }
        }

        private void SelectRequestInDg(int requestToSelectId, ListView lv)
        {
            var dgItems = lv.Items;
            foreach (var item in dgItems)
            {
                Request request = (Request)item;
                if (request.RequestID == requestToSelectId)
                {
                    lv.SelectedItem = item;
                    lv.ScrollIntoView(item);
                    ListViewItem listViewItem = lv.ItemContainerGenerator.ContainerFromItem(lv.SelectedItem) as ListViewItem;
                    if (listViewItem != null)
                    {
                        listViewItem.Focus();
                    }
                    break;
                }
            }
        }


        private void btnUpdateMessage_Click(object sender, RoutedEventArgs e)
        {
            UpdateRequest();
        }

        private void UpdateRequest()
        {
            if (dgMessages.SelectedItem == null)
            {
                MessageBox.Show("נא לסמן משימה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            Request requestToSelect = (Request)dgMessages.SelectedItem;
            frmWorkQueue queueWindow = new frmWorkQueue((Request)dgMessages.SelectedItem);
            queueWindow.ShowDialog();
            if (rbDueDate.IsChecked == true)
            {
                GetRequestsByDueDate();
            }
            else if (rbContactDate.IsChecked == true)
            {
                GetRequestsByContactDate();
            }
            else
            {
                GetAllRequests();
            }
            SelectRequestInDg(requestToSelect.RequestID, dgMessages);
        }

        private void dgMessages_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            DependencyObject src = (DependencyObject)(e.OriginalSource);
            while (!(src is Control))
            {
                if (!(src is Visual))
                {
                    break;
                }
                src = VisualTreeHelper.GetParent(src);
            }

            if ((src is Control && src.GetType() == typeof(ListViewItem)) || src.GetType() == typeof(Run))
            {
                UpdateRequest();
            }
        }

        private void rbContactDate_Checked(object sender, RoutedEventArgs e)
        {
            dpContactDateFrom.IsEnabled = true;
            dpContactDateTo.IsEnabled = true;
            btnFindByContactDate.IsEnabled = true;
            dpContactDueDateFrom.IsEnabled = false;
            dpContactDueDateTo.IsEnabled = false;
            btnFindByDueDate.IsEnabled = false;

            if (dpContactDateFrom.SelectedDate != null && dpContactDateTo.SelectedDate != null)
            {
                GetRequestsByContactDate();
            }
        }

        private void GetRequestsByContactDate()
        {
            if (dpContactDateTo.SelectedDate == null || dpContactDateFrom.SelectedDate == null)
            {
                MessageBox.Show("נא להזין תאריך התחלה ותאריך סיום", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            if (dpContactDateTo.SelectedDate < dpContactDateFrom.SelectedDate)
            {
                MessageBox.Show("תאריך סיום חייב להיות גדול מתאריך התחלה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            requestsByContactDate = workTasksLogic.GetRequestsByContactDate(dpContactDateFrom.SelectedDate, dpContactDateTo.SelectedDate, user).Where(r => r.ClientID == client.ClientID).ToList(); 
            dgMessagesBinding();
        }
        

        private void ClearFilters()
        {
            //if (isUserWorkerOnly)
            //{
            cbUserInCharge.SelectedIndex = index;
            //}
            //else
            //{
            //    cbUserInCharge.SelectedIndex = -1;
            //}
            cbCategory.SelectedIndex = -1;
            cbCondition.SelectedIndex = -1;
            cbStatus.SelectedIndex = 0;
            txtSearchRequest.Clear();
        }

        private void rbDueDate_Checked(object sender, RoutedEventArgs e)
        {
            dpContactDateFrom.IsEnabled = false;
            dpContactDateTo.IsEnabled = false;
            btnFindByContactDate.IsEnabled = false;
            dpContactDueDateFrom.IsEnabled = true;
            dpContactDueDateTo.IsEnabled = true;
            btnFindByDueDate.IsEnabled = true;
            if (dpContactDueDateFrom.SelectedDate != null && dpContactDueDateTo.SelectedDate != null)
            {
                GetRequestsByDueDate();
            }
        }
        private void GetRequestsByDueDate()
        {
            if (dpContactDueDateTo.SelectedDate == null || dpContactDueDateFrom.SelectedDate == null)
            {
                MessageBox.Show("נא להזין תאריך התחלה ותאריך סיום", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            if (dpContactDueDateTo.SelectedDate < dpContactDueDateFrom.SelectedDate)
            {
                MessageBox.Show("תאריך סיום חייב להיות גדול מתאריך התחלה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            requestsByDueDate = workTasksLogic.GetRequestsByDueDate(dpContactDueDateFrom.SelectedDate, dpContactDueDateTo.SelectedDate, user).Where(r=>r.ClientID==client.ClientID).ToList();
            dgMessagesBinding();
        }

        
        private void rbDisplayAll_Checked(object sender, RoutedEventArgs e)
        {
            dpContactDateFrom.IsEnabled = false;
            dpContactDateTo.IsEnabled = false;
            btnFindByContactDate.IsEnabled = false;
            dpContactDueDateFrom.IsEnabled = false;
            dpContactDueDateTo.IsEnabled = false;
            btnFindByDueDate.IsEnabled = false;
            GetAllRequests();            
        }

        private void GetAllRequests()
        {
            allRequests = workTasksLogic.GetAllRequestsByClient(client.ClientID, user);
            dgMessagesBinding();
        }

        private void cbUserInCharge_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            dgMessagesBinding();
        }

        

        private void btnClear_Click(object sender, RoutedEventArgs e)
        {
            ClearFilters();            
        }

        private void btnFindByDueDate_Click(object sender, RoutedEventArgs e)
        {
            GetRequestsByDueDate();
        }

        private void btnFindByContactDate_Click(object sender, RoutedEventArgs e)
        {
            GetRequestsByContactDate();
        }

        private void txtSearchRequest_TextChanged(object sender, TextChangedEventArgs e)
        {
            dgMessagesBinding();
        }

        private void cbTaskCategoryBinding()
        {
            cbCategory.ItemsSource = workTasksLogic.GetAllRequestCategories();
        }

        private void cbConditionBinding()
        {
            cbCondition.ItemsSource = workTasksLogic.GetAllRequestConditions();
        }

        private void btnTransfer_Click(object sender, RoutedEventArgs e)
        {
            if (dgMessages.SelectedItems.Count == 0)
            {
                MessageBox.Show("נא לסמן משימות", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            if (cbTransferTaskTo.SelectedItem == null)
            {
                MessageBox.Show("נא לסמן משתמש אליו ברצונך להעביר משימות", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }

            List<Request> selectedTasks = dgMessages.SelectedItems.Cast<Request>().ToList();
            try
            {
                workTasksLogic.TransferRequests(selectedTasks, ((User)cbTransferTaskTo.SelectedItem).UserID);
                if (rbDueDate.IsChecked == true)
                {
                    GetRequestsByDueDate();
                }
                else if (rbContactDate.IsChecked == true)
                {
                    GetRequestsByContactDate();
                }
                else
                {
                    GetAllRequests();
                }
                cbTransferTaskTo.Text = "העבר אל";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "", MessageBoxButton.OK, MessageBoxImage.Error);
            }

        }

        private void dgMessages_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            dgMessages.UnselectAll();
        }

        private void btnRefresh_Click(object sender, RoutedEventArgs e)
        {
            if (rbDueDate.IsChecked == true)
            {
                GetRequestsByDueDate();
            }
            else if (rbContactDate.IsChecked == true)
            {
                GetRequestsByContactDate();
            }
            else
            {
                GetAllRequests();
            }
        }


        private void mItemExcel_Click(object sender, RoutedEventArgs e)
        {
            excelLogic.TasksToExcel((List<Request>)dgMessages.ItemsSource);
        }

        private void btnSendEmail_Click(object sender, RoutedEventArgs e)
        {            
            if (client != null)
            {
                if (client.Email == "")
                {
                    MessageBox.Show("אין ללקוח כתובת דוא''ל במערכת", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    return;
                }
                string[] address = new string[] { client.Email };
                try
                {
                    email.SendEmailFromOutlook("", "", null, address);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        private void btnExcel_Click(object sender, RoutedEventArgs e)
        {
            excelLogic.TasksToExcel((List<Request>)dgMessages.ItemsSource);
        }
    }
}
