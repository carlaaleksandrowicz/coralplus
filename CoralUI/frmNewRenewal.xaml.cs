﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using CoralBusinessLogics;


namespace CoralUI
{
    /// <summary>
    /// Interaction logic for frmNewRenewal.xaml
    /// </summary>
    public partial class frmNewRenewal : Window
    {
        RenewalsLogic renewalLogic = new RenewalsLogic();
        Client client;
        ElementaryPolicy policy;
        ElementaryPolicy hoba;
        ElementaryPolicy mekifPartG;
        ElementaryPolicy jabila;
        AllowedToDriveLogic allowedToDriveLogic = new AllowedToDriveLogic();
        //Renewal renewal = null;
        User user;
        List<ElementaryPolicy> policiesByRegistrationNumber;
        PoliciesLogic policiesLogics = new PoliciesLogic();
        InsurancesLogic insuranceLogic = new InsurancesLogic();
        IndustriesLogic industryLogic = new IndustriesLogic();
        ListViewSettings lv = new ListViewSettings();
        InputsValidations validation = new InputsValidations();
        bool? isJobaRenewed = null;
        bool? isMekifPartGRenewed = null;
        bool? isJabilaRenewed = null;
        frmRenewalTrackingManager trackingWindow;
        frmAddClient clientDetailsWindow;
        private List<ElementaryPolicy> hobaPolicies;
        private List<ElementaryPolicy> mekifPartGPolicies;
        private List<ElementaryPolicy> jabilaPolicies;
        List<object> nullErrorList = null;
        ClientsLogic clientLogic = new ClientsLogic();

        public frmNewRenewal(Client clientRenewal, ElementaryPolicy policyToRenew, User userAccount)
        {
            InitializeComponent();
            client = clientRenewal;
            user = userAccount;
            policy = policyToRenew;

        }

        private void TabControlSetting()
        {
            if (hoba == null)
            {
                tbItemHoba.IsEnabled = false;
            }
            if (mekifPartG == null)
            {
                tbItemMekifThirdParty.IsEnabled = false;
            }
            if (jabila == null)
            {
                tbItemJabila.IsEnabled = false;
            }
        }

        private void GetAllPoliciesToRenewByCar(ElementaryPolicy policy)
        {
            policiesByRegistrationNumber = policiesLogics.GetPoliciesByRegistrationNumberAndEndDate(policy.CarPolicy.RegistrationNumber, (DateTime)policy.EndDate);

            hoba = policiesByRegistrationNumber.LastOrDefault(p => p.InsuranceIndustry.InsuranceIndustryName == "רכב חובה");
            if (hoba != null)
            {
                dgQuotesBinding();

                hobaPolicies = policiesLogics.GetPoliciesByPolicyNumberAndIndustry(hoba.PolicyNumber, hoba.InsuranceIndustryID).ToList();
                cbHobaAddition.ItemsSource = hobaPolicies.ToList();
                cbHobaAddition.DisplayMemberPath = "Addition";
                SelectMaxAddition(cbHobaAddition, hobaPolicies);
                decimal? hobaTotalPremium = hobaPolicies.Sum(p => p.TotalPremium);
                lblHobaTotalPremium.Content = string.Format("חובה: {0} ש''ח", ((decimal)hobaTotalPremium).ToString("0.##"));
            }

            mekifPartG = policiesByRegistrationNumber.LastOrDefault(p => p.InsuranceIndustry.InsuranceIndustryName == "רכב" && p.ElementaryPolicyID == policy.ElementaryPolicyID);
            if (mekifPartG != null)
            {
                dgCarQuotesBinding();

                mekifPartGPolicies = policiesLogics.GetPoliciesByPolicyNumberAndIndustry(mekifPartG.PolicyNumber, mekifPartG.InsuranceIndustryID).ToList();
                cbMekifPartGAddition.ItemsSource = mekifPartGPolicies.ToList();
                cbMekifPartGAddition.DisplayMemberPath = "Addition";
                SelectMaxAddition(cbMekifPartGAddition, mekifPartGPolicies);
                decimal? mekifPartGTotalPremium = mekifPartGPolicies.Sum(p => p.TotalPremium);
                lblMekifTotalPremium.Content = string.Format("מקיף/צד ג: {0} ש''ח", ((decimal)mekifPartGTotalPremium).ToString("0.##"));
            }

            jabila = policiesByRegistrationNumber.LastOrDefault(p => p.InsuranceIndustry.InsuranceIndustryName == "רכב - חבילה");
            if (jabila != null)
            {
                dgJabilaQuotesBinding();

                jabilaPolicies = policiesLogics.GetPoliciesByPolicyNumberAndIndustry(jabila.PolicyNumber, jabila.InsuranceIndustryID).ToList();
                cbPackageAddition.ItemsSource = jabilaPolicies.ToList();
                cbPackageAddition.DisplayMemberPath = "Addition";
                SelectMaxAddition(cbPackageAddition, jabilaPolicies);
                decimal? packageTotalPremium = jabilaPolicies.Sum(p => p.TotalPremium);
                lblPackageTotalPremium.Content = string.Format("חבילה: {0} ש''ח", ((decimal)packageTotalPremium).ToString("0.##"));
            }
        }

        private void SelectMaxAddition(ComboBox cb, List<ElementaryPolicy> policies)
        {
            if (policies == null)
            {
                return;
            }
            int addition = 0;
            ElementaryPolicy policy = policies[0];
            foreach (var item in policies)
            {
                if (item.Addition > 0)
                {
                    addition = (int)item.Addition;
                    policy = item;
                }
            }
            var items = cb.Items;
            foreach (var item in items)
            {
                ElementaryPolicy tempItem = (ElementaryPolicy)item;
                if (tempItem.ElementaryPolicyID == policy.ElementaryPolicyID)
                {
                    cb.SelectedItem = item;
                    break;
                }
            }
        }


        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            GetAllPoliciesToRenewByCar(policy);
            TabControlSetting();
            cbCompanyBinding();
            cbNewCarPolicyTypeBinding(hoba, mekifPartG, jabila);
            cbDriversAgeBinding();
            SetClientDetails();
            txtLisenceNumber.Text = policy.CarPolicy.RegistrationNumber;
            txtModelCode.Text = policy.CarPolicy.ModelCode;
            txtManufacturer.Text = policy.CarPolicy.Manufacturer;
            txtYear.Text = policy.CarPolicy.Year.ToString();
            
            if (policy.IsMortgaged == true)
            {
                chbIsMortgaged.IsChecked = true;
                chbIsMortgaged.FontWeight = FontWeights.Bold;
            }
            if (policy.CarPolicy.AllowedToDriveID != null)
            {
                string allowedToDrive = (allowedToDriveLogic.GetAllowToDriveById((int)policy.CarPolicy.AllowedToDriveID)).Description;
                txtDriversAge.Text = allowedToDrive;
            }
            if (hoba != null)
            {
                isJobaRenewed = false;
                //txtHobaPolicy.Text = hoba.PolicyNumber;
                //txtHobaPremia.Text = validation.ConvertDecimalToString(hoba.TotalPremium);
                //txtHobaInsuranceCompany.Text = hoba.Company.CompanyName;
                RenewalStatus renewalStatus = renewalLogic.GetRenewalStatusByRenewalStatusId((int)hoba.RenewalStatusID);
                if (renewalStatus.RenewalStatusName == "לא טופל")
                {
                    string newStatus = "פתוח לעבודה";
                    hoba.RenewalStatusID = policiesLogics.UpdatePolicyRenewalStatus(hoba, newStatus);
                }
               /* else if (renewalStatus.RenewalStatusName == "חודש")
                {
                    cbHobaRenewalStatus.IsEnabled = false;
                    dgQuotes.IsEnabled = false;
                    dgQuotesOnly.IsEnabled = false;
                    isJobaRenewed = true;
                }*/
            }
            if (mekifPartG != null)
            {
                isMekifPartGRenewed = false;
                //txtMekifPartGPolicy.Text = mekifPartG.PolicyNumber;
                //txtMekifPartGPremia.Text = validation.ConvertDecimalToString(mekifPartG.TotalPremium);
                //txtMekifPartGInsuranceCompany.Text = mekifPartG.Company.CompanyName;
                RenewalStatus renewalStatus = renewalLogic.GetRenewalStatusByRenewalStatusId((int)mekifPartG.RenewalStatusID);
                if (renewalStatus.RenewalStatusName == "לא טופל")
                {
                    string newStatus = "פתוח לעבודה";
                    mekifPartG.RenewalStatusID = policiesLogics.UpdatePolicyRenewalStatus(mekifPartG, newStatus);
                }
                /*else if (renewalStatus.RenewalStatusName == "חודש")
                {
                    cbMekifRenewalStatus.IsEnabled = false;
                    dgCarQuotes.IsEnabled = false;
                    dgCarQuotesOnly.IsEnabled = false;
                    isMekifPartGRenewed = true;
                }*/
            }
            if (jabila != null)
            {
                isJabilaRenewed = false;
                //txtPackagePolicy.Text = jabila.PolicyNumber;
                //txtPackagePremia.Text = validation.ConvertDecimalToString(jabila.TotalPremium);
                //txtPackageInsuranceCompany.Text = jabila.Company.CompanyName;
                RenewalStatus renewalStatus = renewalLogic.GetRenewalStatusByRenewalStatusId((int)jabila.RenewalStatusID);
                if (renewalStatus.RenewalStatusName == "לא טופל")
                {
                    string newStatus = "פתוח לעבודה";
                    jabila.RenewalStatusID = policiesLogics.UpdatePolicyRenewalStatus(jabila, newStatus);
                }
                /*else if (renewalStatus.RenewalStatusName == "חודש")
                {
                    cbJabilaRenewalStatus.IsEnabled = false;
                    dgJabilaQuotes.IsEnabled = false;
                    dgJabilaQuotesOnly.IsEnabled = false;
                    isJabilaRenewed = true;
                }*/
            }
            if (isJobaRenewed != false && isMekifPartGRenewed != false && isJabilaRenewed != false)
            {
                gbQuotes.IsEnabled = false;
                btnPolicyRenewal.IsEnabled = false;
                btnDeleteQuote.IsEnabled = false;
            }
            cbRenewalStatusBinding();
            frmRenewalTrackingManager.TrackingsCountChange+=new EventHandler(renewalsWindow_TrackingsCountChange);

            lblTrackingCountBinding();

        }
        private void SetClientDetails()
        {
            txtClientName.Text = client.FirstName + " " + client.LastName;
            txtEmail.Text = client.Email;
            txtPhone.Text = client.PhoneHome;
            txtCellPhone.Text = client.CellPhone;
            string clientDetails;
            if (client.CellPhone != null && client.CellPhone != "")
            {
                clientDetails = string.Format("{0} {1}  ת.ז: {2} נייד: {3}", client.FirstName, client.LastName, client.IdNumber, client.CellPhone);
            }
            else
            {
                clientDetails = string.Format("{0} {1}  ת.ז: {2}", client.FirstName, client.LastName, client.IdNumber);
            }
            lblClientNameAndId.Content = clientDetails;
        }
        void renewalsWindow_TrackingsCountChange(object sender, EventArgs e)
        {
            lblTrackingCountBinding();
        }


        private void lblTrackingCountBinding()
        {
            lblTrackingCount.Content = (renewalLogic.GetTrackingsCountByPolicyId(policy.ElementaryPolicyID)).ToString();
        }

        private void cbDriversAgeBinding()
        {
            cbDriversAge.ItemsSource = allowedToDriveLogic.GetActiveAllowedToDrive();
            cbDriversAge.DisplayMemberPath = "Description";
        }

        private void btnUpdateDriversTable_Click(object sender, RoutedEventArgs e)
        {
            int? selectedItemId = null;
            if (cbDriversAge.SelectedItem != null)
            {
                selectedItemId = ((AllowedToDrive)cbDriversAge.SelectedItem).AllowedToDriveID;
            }
            frmUpdateTable updateAllowedToDriveTable = new frmUpdateTable(new AllowedToDrive());
            updateAllowedToDriveTable.ShowDialog();
            cbDriversAgeBinding();
            if (updateAllowedToDriveTable.ItemAdded != null)
            {
                SelectDriverItemInCb(((AllowedToDrive)updateAllowedToDriveTable.ItemAdded).AllowedToDriveID);
            }
            else if (selectedItemId != null)
            {
                SelectDriverItemInCb((int)selectedItemId);
            }
            //SelectCbItem(cbInsuranceCompany,);
        }

        private void SelectDriverItemInCb(int id)
        {
            var items = cbDriversAge.Items;
            foreach (var item in items)
            {
                AllowedToDrive parsedItem = (AllowedToDrive)item;
                if (parsedItem.AllowedToDriveID == id)
                {
                    cbDriversAge.SelectedItem = item;
                    break;
                }
            }
        }

        private void cbRenewalStatusBinding()
        {
            if (hoba != null)
            {
                cbHobaRenewalStatus.ItemsSource = renewalLogic.GetAllActiveRenewalStatuses();
                cbHobaRenewalStatus.DisplayMemberPath = "RenewalStatusName";
                var hobaRenewalStatuses = cbHobaRenewalStatus.Items;
                string hobaStatus = (renewalLogic.GetRenewalStatusByRenewalStatusId((int)hoba.RenewalStatusID)).RenewalStatusName;
                /*if (hobaStatus == "חודש")
                {
                    cbHobaRenewalStatus.IsEnabled = false;
                    dgQuotes.UnselectAll();
                    dgQuotes.IsEnabled = false;
                    dgQuotesOnly.UnselectAll();
                    dgQuotesOnly.IsEnabled = false;

                }*/
                foreach (var item in hobaRenewalStatuses)
                {
                    if (((RenewalStatus)item).RenewalStatusName == hobaStatus)
                    {
                        cbHobaRenewalStatus.SelectedItem = item;
                        break;
                    }
                }
            }
            if (mekifPartG != null)
            {
                cbMekifRenewalStatus.ItemsSource = renewalLogic.GetAllActiveRenewalStatuses();
                cbMekifRenewalStatus.DisplayMemberPath = "RenewalStatusName";
                var mekifRenewalStatuses = cbMekifRenewalStatus.Items;
                string mekifPartGStatus = (renewalLogic.GetRenewalStatusByRenewalStatusId((int)mekifPartG.RenewalStatusID)).RenewalStatusName;
                /*if (mekifPartGStatus == "חודש")
                {
                    cbMekifRenewalStatus.IsEnabled = false;
                    dgCarQuotes.UnselectAll();
                    dgCarQuotes.IsEnabled = false;
                    dgCarQuotesOnly.UnselectAll();
                    dgCarQuotesOnly.IsEnabled = false;
                }*/
                foreach (var item in mekifRenewalStatuses)
                {
                    if (((RenewalStatus)item).RenewalStatusName == mekifPartGStatus)
                    {
                        cbMekifRenewalStatus.SelectedItem = item;
                        break;
                    }
                }
            }
            if (jabila != null)
            {
                cbJabilaRenewalStatus.ItemsSource = renewalLogic.GetAllActiveRenewalStatuses();
                cbJabilaRenewalStatus.DisplayMemberPath = "RenewalStatusName";
                var jabilaRenewalStatuses = cbJabilaRenewalStatus.Items;
                string jabilaStatus = (renewalLogic.GetRenewalStatusByRenewalStatusId((int)jabila.RenewalStatusID)).RenewalStatusName;

                foreach (var item in jabilaRenewalStatuses)
                {
                    if (((RenewalStatus)item).RenewalStatusName == jabilaStatus)
                    {
                        cbJabilaRenewalStatus.SelectedItem = item;
                        break;
                    }
                }
                /*if (jabilaStatus == "חודש")
                {
                    cbJabilaRenewalStatus.IsEnabled = false;
                    dgJabilaQuotes.UnselectAll();
                    dgJabilaQuotes.IsEnabled = false;
                    dgJabilaQuotesOnly.UnselectAll();
                    dgJabilaQuotesOnly.IsEnabled = false;
                }*/
            }
        }

        private void cbNewCarPolicyTypeBinding(ElementaryPolicy hoba, ElementaryPolicy mekifPartG, ElementaryPolicy jabila)
        {
            List<InsuranceIndustry> industries = (industryLogic.GetAllIndustries()).Where(i => i.InsuranceIndustryName.Contains("רכב")).ToList();

            if (hoba == null)
            {
                industries.RemoveAll(i => i.InsuranceIndustryName == "רכב חובה");
            }
            if (mekifPartG == null)
            {
                industries.RemoveAll(i => i.InsuranceIndustryName == "רכב");
            }
            if (jabila == null)
            {
                industries.RemoveAll(i => i.InsuranceIndustryName == "רכב - חבילה");
            }
            cbNewCarPolicyType.ItemsSource = industries;
            cbNewCarPolicyType.DisplayMemberPath = "InsuranceIndustryName";
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        //private void btnAddTracking_Click(object sender, RoutedEventArgs e)
        //{
        //    frmTracking newTrackingWindow = new frmTracking(client, policy, user);
        //    newTrackingWindow.ShowDialog();
        //    if (newTrackingWindow.trackingContent != null)
        //    {
        //        DateTime date = DateTime.Now;
        //        if (hoba != null)
        //        {
        //            renewalLogic.InsertRenewalTracking(hoba.ElementaryPolicyID, user.UserID, date, newTrackingWindow.trackingContent);
        //        }
        //        if (mekifPartG != null)
        //        {
        //            renewalLogic.InsertRenewalTracking(mekifPartG.ElementaryPolicyID, user.UserID, date, newTrackingWindow.trackingContent);
        //        }
        //        if (jabila != null)
        //        {
        //            renewalLogic.InsertRenewalTracking(jabila.ElementaryPolicyID, user.UserID, date, newTrackingWindow.trackingContent);
        //        }
        //        //dgTrackingBinding();
        //    }
        //}

        //private void dgTrackingBinding()
        //{
        //    dgTracking.ItemsSource = renewalLogic.GetTrackingsByRenewalId(policy.ElementaryPolicyID);
        //    lv.AutoSizeColumns(dgTracking.View);
        //}

        //private void btnDeleteTracking_Click(object sender, RoutedEventArgs e)
        //{
        //    if (dgTracking.SelectedItem == null)
        //    {
        //        MessageBox.Show("נא לסמן מעקב", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
        //        return;
        //    }
        //    renewalLogic.DeleteTracking((RenewalTracking)dgTracking.SelectedItem);
        //    dgTrackingBinding();
        //}

        //private void btnUpdateTracking_Click(object sender, RoutedEventArgs e)
        //{
        //    if (dgTracking.SelectedItem == null)
        //    {
        //        MessageBox.Show("נא לסמן מעקב", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
        //        return;
        //    }
        //    frmTracking updateTrackingWindow = new frmTracking((RenewalTracking)dgTracking.SelectedItem, client, policy, user);
        //    updateTrackingWindow.ShowDialog();
        //    renewalLogic.UpdateRenewalTracking((RenewalTracking)dgTracking.SelectedItem, updateTrackingWindow.trackingContent);
        //    dgTrackingBinding();
        //    dgTracking.UnselectAll();
        //}


        private void cbCompanyBinding()
        {
            cbNewInsuranceCompany.ItemsSource = insuranceLogic.GetActiveCompaniesByInsurance("אלמנטרי");
            cbNewInsuranceCompany.DisplayMemberPath = "Company.CompanyName";
        }

        private void btnUpdateCompanyTable_Click(object sender, RoutedEventArgs e)
        {
            int? selectedItemId = null;
            if (cbNewInsuranceCompany.SelectedItem != null)
            {
                selectedItemId = ((InsuranceCompany)cbNewInsuranceCompany.SelectedItem).CompanyID;
            }
            int insuranceID = insuranceLogic.GetInsuranceID("אלמנטרי");
            InsuranceCompany company = new InsuranceCompany() { InsuranceID = insuranceID };
            frmUpdateTable updateCompaniesTable = new frmUpdateTable(company);
            updateCompaniesTable.ShowDialog();
            cbCompanyBinding();
            if (updateCompaniesTable.ItemAdded != null)
            {
                SelectItemInCb(((InsuranceCompany)updateCompaniesTable.ItemAdded).CompanyID);
            }
            else if (selectedItemId != null)
            {
                SelectItemInCb((int)selectedItemId);
            }
        }

        private void SelectItemInCb(int id)
        {
            var items = cbNewInsuranceCompany.Items;
            foreach (var item in items)
            {
                InsuranceCompany parsedItem = (InsuranceCompany)item;
                if (parsedItem.CompanyID == id)
                {
                    cbNewInsuranceCompany.SelectedItem = item;
                    break;
                }
            }
        }


        private void btnAddQuote_Click(object sender, RoutedEventArgs e)
        {
            //if (txtNewPremium.Text == "" || cbNewInsuranceCompany.SelectedItem == null || txtPolicyNumber.Text == "" || cbNewCarPolicyType.SelectedItem == null)
            //{
            //    MessageBox.Show("נא למלאות שדות חובה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            //    return;
            //}
            if (nullErrorList != null && nullErrorList.Count > 0)
            {
                foreach (var item in nullErrorList)
                {
                    if (item is TextBox)
                    {
                        ((TextBox)item).ClearValue(BorderBrushProperty);
                    }
                    else if (item is ComboBox)
                    {
                        ((ComboBox)item).ClearValue(BorderBrushProperty);
                    }                    
                }
            }

            //בדיקת שדות חובה
            nullErrorList = validation.InputNullValidation(new object[] { txtNewPremium,cbNewInsuranceCompany,txtPolicyNumber,cbNewCarPolicyType});
            if (nullErrorList.Count > 0)
            {
                //cancelClose = true;
                MessageBox.Show("נא למלאות שדות חובה", "", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            try
            {
                if (((InsuranceIndustry)cbNewCarPolicyType.SelectedItem).InsuranceIndustryName == "רכב חובה")
                {
                    if (renewalLogic.GetRenewalStatusByRenewalStatusId((int)hoba.RenewalStatusID).RenewalStatusName == "חודש")
                    {
                        MessageBox.Show("לא ניתן לבצע הצעה, הפוליסה חודשה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                        return;
                    }
                    renewalLogic.InsertCarRenewalQuote(hoba, txtPolicyNumber.Text, int.Parse(txtNewPremium.Text), (InsuranceCompany)cbNewInsuranceCompany.SelectedItem, (InsuranceIndustry)cbNewCarPolicyType.SelectedItem, (AllowedToDrive)cbDriversAge.SelectedItem, chbIsJabilaIncluded.IsChecked);
                    dgQuotesBinding();
                }
                else if (((InsuranceIndustry)cbNewCarPolicyType.SelectedItem).InsuranceIndustryName == "רכב")
                {
                    if (renewalLogic.GetRenewalStatusByRenewalStatusId((int)mekifPartG.RenewalStatusID).RenewalStatusName == "חודש")
                    {
                        MessageBox.Show("לא ניתן לבצע הצעה, הפוליסה חודשה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                        return;
                    }
                    renewalLogic.InsertCarRenewalQuote(mekifPartG, txtPolicyNumber.Text, int.Parse(txtNewPremium.Text), (InsuranceCompany)cbNewInsuranceCompany.SelectedItem, (InsuranceIndustry)cbNewCarPolicyType.SelectedItem, (AllowedToDrive)cbDriversAge.SelectedItem, chbIsJabilaIncluded.IsChecked);
                    dgCarQuotesBinding();
                }
                else if (((InsuranceIndustry)cbNewCarPolicyType.SelectedItem).InsuranceIndustryName == "רכב - חבילה")
                {
                    if (renewalLogic.GetRenewalStatusByRenewalStatusId((int)jabila.RenewalStatusID).RenewalStatusName == "חודש")
                    {
                        MessageBox.Show("לא ניתן לבצע הצעה, הפוליסה חודשה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                        return;
                    }
                    renewalLogic.InsertCarRenewalQuote(jabila, txtPolicyNumber.Text, int.Parse(txtNewPremium.Text), (InsuranceCompany)cbNewInsuranceCompany.SelectedItem, (InsuranceIndustry)cbNewCarPolicyType.SelectedItem, (AllowedToDrive)cbDriversAge.SelectedItem, chbIsJabilaIncluded.IsChecked);
                    dgJabilaQuotesBinding();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
            finally
            {
                txtPolicyNumber.Clear();
                txtNewPremium.Clear();
                cbNewInsuranceCompany.SelectedIndex = -1;
                cbNewCarPolicyType.SelectedIndex = -1;
                cbDriversAge.SelectedIndex = -1;
                chbIsJabilaIncluded.IsChecked = false;
            }
        }

        private void dgQuotesBinding()
        {
            dgQuotes.ItemsSource = renewalLogic.GetCarRenewalQuotesByRenewalId(hoba.ElementaryPolicyID);
            dgQuotesOnly.ItemsSource = renewalLogic.GetCarRenewalQuotesByRenewalId(hoba.ElementaryPolicyID);

        }
        private void dgCarQuotesBinding()
        {
            dgCarQuotes.ItemsSource = renewalLogic.GetCarRenewalQuotesByRenewalId(mekifPartG.ElementaryPolicyID);
            dgCarQuotesOnly.ItemsSource = renewalLogic.GetCarRenewalQuotesByRenewalId(mekifPartG.ElementaryPolicyID);
        }

        private void dgJabilaQuotesBinding()
        {
            dgJabilaQuotes.ItemsSource = renewalLogic.GetCarRenewalQuotesByRenewalId(jabila.ElementaryPolicyID);
            dgJabilaQuotesOnly.ItemsSource = renewalLogic.GetCarRenewalQuotesByRenewalId(jabila.ElementaryPolicyID);
        }


        private void btnPolicyRenewal_Click(object sender, RoutedEventArgs e)
        {
            frmElementary renewElementaryPolicy = null;
            //if (dgQuotes.SelectedItem != null)
            //{
            //    renewElementaryPolicy = new frmElementary(client, user, true, hoba, null, (CarRenewalQuote)dgQuotes.SelectedItem);
            //    renewElementaryPolicy.ShowDialog();
            //    if (renewElementaryPolicy.renewalAccepted)
            //    {
            //        hoba.RenewalStatusID = policiesLogics.UpdatePolicyRenewalStatus(hoba, "חודש");
            //    }
            //}
            if (dgQuotesOnly.SelectedItem != null)
            {
                renewElementaryPolicy = new frmElementary(client, user, true, hoba, null, (CarRenewalQuote)dgQuotesOnly.SelectedItem);
                renewElementaryPolicy.ShowDialog();
                if (renewElementaryPolicy.renewalAccepted)
                {
                    hoba.RenewalStatusID = policiesLogics.UpdatePolicyRenewalStatus(hoba, "חודש");
                }
            }
            //else if (dgCarQuotes.SelectedItem != null)
            //{
            //    renewElementaryPolicy = new frmElementary(client, user, true, mekifPartG, null, (CarRenewalQuote)dgCarQuotes.SelectedItem);
            //    renewElementaryPolicy.ShowDialog();
            //    if (renewElementaryPolicy.renewalAccepted)
            //    {
            //        mekifPartG.RenewalStatusID = policiesLogics.UpdatePolicyRenewalStatus(policy, "חודש");
            //    }
            //}
            else if (dgCarQuotesOnly.SelectedItem != null)
            {
                renewElementaryPolicy = new frmElementary(client, user, true, mekifPartG, null, (CarRenewalQuote)dgCarQuotesOnly.SelectedItem);
                renewElementaryPolicy.ShowDialog();
                if (renewElementaryPolicy.renewalAccepted)
                {
                    mekifPartG.RenewalStatusID = policiesLogics.UpdatePolicyRenewalStatus(mekifPartG, "חודש");
                }
            }
            //else if (dgJabilaQuotes.SelectedItem != null)
            //{
            //    renewElementaryPolicy = new frmElementary(client, user, true, jabila, null, (CarRenewalQuote)dgJabilaQuotes.SelectedItem);
            //    renewElementaryPolicy.ShowDialog();
            //    if (renewElementaryPolicy.renewalAccepted)
            //    {
            //        jabila.RenewalStatusID = policiesLogics.UpdatePolicyRenewalStatus(policy, "חודש");
            //    }
            //}
            else if (dgJabilaQuotesOnly.SelectedItem != null)
            {
                renewElementaryPolicy = new frmElementary(client, user, true, jabila, null, (CarRenewalQuote)dgJabilaQuotesOnly.SelectedItem);
                renewElementaryPolicy.ShowDialog();
                if (renewElementaryPolicy.renewalAccepted)
                {
                    jabila.RenewalStatusID = policiesLogics.UpdatePolicyRenewalStatus(jabila, "חודש");
                }
            }
            else
            {
                MessageBox.Show("נא לסמן הצעה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
            cbRenewalStatusBinding();
        }

        private void btnDeleteQuote_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (dgQuotes.SelectedItem != null)
                {
                    renewalLogic.DeleteCarQuote((CarRenewalQuote)dgQuotes.SelectedItem);
                    dgQuotesBinding();
                }
                else if (dgQuotesOnly.SelectedItem != null)
                {
                    renewalLogic.DeleteCarQuote((CarRenewalQuote)dgQuotesOnly.SelectedItem);
                    dgQuotesBinding();
                }
                else if (dgCarQuotes.SelectedItem != null)
                {
                    renewalLogic.DeleteCarQuote((CarRenewalQuote)dgCarQuotes.SelectedItem);
                    dgCarQuotesBinding();
                }
                else if (dgCarQuotesOnly.SelectedItem != null)
                {
                    renewalLogic.DeleteCarQuote((CarRenewalQuote)dgCarQuotesOnly.SelectedItem);
                    dgCarQuotesBinding();
                }
                else if (dgJabilaQuotes.SelectedItem != null)
                {
                    renewalLogic.DeleteCarQuote((CarRenewalQuote)dgJabilaQuotes.SelectedItem);
                    dgJabilaQuotesBinding();
                }
                else if (dgJabilaQuotesOnly.SelectedItem != null)
                {
                    renewalLogic.DeleteCarQuote((CarRenewalQuote)dgJabilaQuotesOnly.SelectedItem);
                    dgJabilaQuotesBinding();
                }
                else
                {
                    MessageBox.Show("נא לסמן הצעה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private void cbHobaRenewalStatus_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (this.cbHobaRenewalStatus.IsDropDownOpen)
            {
                //OPTIONAL:
                //Causes the combobox selection changed to not be fired again if anything
                //in the function below changes the selection (as in my weird case)
                //this.cbHobaRenewalStatus.IsDropDownOpen = false;

                //now put the code you want to fire when a user selects an option here
                hoba.RenewalStatusID = policiesLogics.UpdatePolicyRenewalStatus(hoba, ((RenewalStatus)cbHobaRenewalStatus.SelectedItem).RenewalStatusName);
            }

        }

        private void cbMekifRenewalStatus_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

            if (this.cbMekifRenewalStatus.IsDropDownOpen)
            {
                //OPTIONAL:
                //Causes the combobox selection changed to not be fired again if anything
                //in the function below changes the selection (as in my weird case)
                //this.cbMekifRenewalStatus.IsDropDownOpen = false;
                mekifPartG.RenewalStatusID = policiesLogics.UpdatePolicyRenewalStatus(mekifPartG, ((RenewalStatus)cbMekifRenewalStatus.SelectedItem).RenewalStatusName);
            }
        }

        private void cbJabilaRenewalStatus_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

            if (this.cbJabilaRenewalStatus.IsDropDownOpen)
            {
                //OPTIONAL:
                //Causes the combobox selection changed to not be fired again if anything
                //in the function below changes the selection (as in my weird case)
                //this.cbJabilaRenewalStatus.IsDropDownOpen = false;
                jabila.RenewalStatusID = policiesLogics.UpdatePolicyRenewalStatus(jabila, ((RenewalStatus)cbJabilaRenewalStatus.SelectedItem).RenewalStatusName);
            }
        }

        private void btnPolicyArchive_Click(object sender, RoutedEventArgs e)
        {
            frmScan2 archiveWindow = new frmScan2(client, hoba, null, user);
            archiveWindow.ShowDialog();
        }

        private void btnMekifPolicyArchive_Click(object sender, RoutedEventArgs e)
        {
            frmScan2 archiveWindow = new frmScan2(client, mekifPartG, null, user);
            archiveWindow.ShowDialog();
        }

        private void btnJabilaPolicyArchive_Click(object sender, RoutedEventArgs e)
        {
            frmScan2 archiveWindow = new frmScan2(client, jabila, null, user);
            archiveWindow.ShowDialog();
        }

        private void tbQuotes_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.Source is TabControl)
            {
                dgCarQuotes.UnselectAll();
                dgCarQuotesOnly.UnselectAll();
                dgJabilaQuotes.UnselectAll();
                dgJabilaQuotesOnly.UnselectAll();
                dgQuotes.UnselectAll();
                dgQuotesOnly.UnselectAll();
                if (tbItemDisplayAll.IsSelected)
                {
                    //btnPolicyRenewal.IsEnabled = false;
                }
                else
                {
                    //btnPolicyRenewal.IsEnabled = true;
                }
            }
        }

        private void txtNewPremium_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (sender is TextBox)
            {
                TextBox txtBox = (TextBox)sender;
                txtBox.ClearValue(BorderBrushProperty);
                if (txtBox.Text != "")
                {
                    try
                    {
                        decimal reusult = validation.ConvertStringToDecimal(txtBox.Text);
                    }
                    catch (Exception ex)
                    {
                        txtBox.BorderBrush = System.Windows.Media.Brushes.Red;
                        System.Windows.Forms.MessageBox.Show(ex.Message);
                        txtBox.Text = "";
                    }
                }
            }
        }

        private void btnCalculator_Click(object sender, RoutedEventArgs e)
        {
            System.Diagnostics.Process.Start("calc");
        }

        private void btnTracking_Click(object sender, RoutedEventArgs e)
        {
            trackingWindow = new frmRenewalTrackingManager(policy, policiesByRegistrationNumber, user);
            trackingWindow.Show();
            //lblTrackingCountBinding();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (trackingWindow != null && trackingWindow.IsLoaded)
            {
                trackingWindow.Close();
            }
            if (clientDetailsWindow != null && clientDetailsWindow.IsLoaded)
            {
                clientDetailsWindow.Close();
            }
        }

        private void btnClientDetails_Click(object sender, RoutedEventArgs e)
        {
            if (client != null)
            {
                clientDetailsWindow = new frmAddClient(client, user);
                clientDetailsWindow.ShowDialog();
                client = clientLogic.GetClientByClientID(client.ClientID);
                SetClientDetails();
            }
        }

        private void cbHobaAddition_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            SetHobaInfo((ElementaryPolicy)cbHobaAddition.SelectedItem);
        }

        private void SetHobaInfo(ElementaryPolicy selectedItem)
        {
            txtHobaPolicy.Text = selectedItem.PolicyNumber;
            txtHobaPremia.Text = validation.ConvertDecimalToString(selectedItem.TotalPremium);
            txtHobaInsuranceCompany.Text = selectedItem.Company.CompanyName;
        }

        private void cbMekifPartGAddition_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            SetMekifInfo((ElementaryPolicy)cbMekifPartGAddition.SelectedItem);
        }

        private void SetMekifInfo(ElementaryPolicy selectedItem)
        {
            txtMekifPartGPolicy.Text = selectedItem.PolicyNumber;
            txtMekifPartGPremia.Text = validation.ConvertDecimalToString(selectedItem.TotalPremium);
            txtMekifPartGInsuranceCompany.Text = selectedItem.Company.CompanyName;
        }

        private void cbPackageAddition_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            SetPackageInfo((ElementaryPolicy)cbPackageAddition.SelectedItem);
        }

        private void SetPackageInfo(ElementaryPolicy selectedItem)
        {
            txtPackagePolicy.Text = selectedItem.PolicyNumber;
            txtPackagePremia.Text = validation.ConvertDecimalToString(selectedItem.TotalPremium);
            txtPackageInsuranceCompany.Text = selectedItem.Company.CompanyName;
        }

        
    }
}
