﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using CoralBusinessLogics;
using System.IO;
using System.Windows.Media;
using System.Windows.Documents;

namespace CoralUI
{
    /// <summary>
    /// Interaction logic for frmElementaryClaims.xaml
    /// </summary>
    public partial class frmElementaryClaims : Window
    {
        Client client = null;
        ElementaryPolicy policy = null;
        ClaimsLogic claimLogic = new ClaimsLogic();
        InputsValidations validations = new InputsValidations();
        List<TextBox> errorList = null;
        int claimId;
        List<ElementaryWitness> witnesses = new List<ElementaryWitness>();
        List<ApartmentBusinessDamage> damages = new List<ApartmentBusinessDamage>();
        decimal? totalAmountClaimed = 0;
        List<CarThirdParty> carThirdParties = new List<CarThirdParty>();
        List<ElementaryClaimTracking> trackings = new List<ElementaryClaimTracking>();
        TrackingsLogics trackingLogic = new TrackingsLogics();
        User user;
        InsurancesLogic insuranceLogic = new InsurancesLogic();
        VehicleTypesLogic vehicleLogic = new VehicleTypesLogic();
        IndustriesLogic industryLogic = new IndustriesLogic();
        ElementaryClaim claim = null;
        ListViewSettings lv = new ListViewSettings();
        int thirdPartySelectedIndex;
        string path = "";
        string newPath = "";
        string clientDirPath = "";
        string[] industryPathDirs = null;
        List<object> nullErrorList = null;
        InputsValidations validation = new InputsValidations();
        private bool isChanges = false;
        bool confirmBeforeClosing = true;
        bool cancelClose = false;
        bool isThirdPartyUpdate = false;
        ApartmentBusinessDamage damageSelected;
        ElementaryWitness witnessSelected;

        public frmElementaryClaims(Client policyClient, ElementaryPolicy policyClaim, User userAccount)
        {
            InitializeComponent();
            this.SizeToContent = SizeToContent.Manual;
            client = policyClient;
            policy = policyClaim;
            user = userAccount;
        }

        public frmElementaryClaims(ElementaryClaim claimToUpdate, Client policyClient, User userAccount)
        {
            InitializeComponent();
            this.SizeToContent = SizeToContent.Manual;
            client = policyClient;
            claim = claimToUpdate;
            policy = claim.ElementaryPolicy;
            user = userAccount;
        }
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            string strAppDir = System.AppDomain.CurrentDomain.BaseDirectory;
            string path = strAppDir + @"Coral Files\ClientsPath.txt";

            if (File.Exists(path))
            {
                clientDirPath = File.ReadAllText(path);
            }
            else
            {
                MessageBox.Show("לא נמצא תיקיית הלקוחות. נא בדוק בהגדרות ראשוניות את נתיב התיקייה");
                Close();
                return;
            }

            //path = clientDirPath + @"/" + client.ClientID + "/אלמנטרי/"; //+ policy.PolicyNumber + " " + policy.InsuranceIndustry.InsuranceIndustryName + "/תביעות/" + policy.PolicyNumber + " " + policy.InsuranceIndustry.InsuranceIndustryName + @"/תביעה " + claim.EventDate.ToString() + " " + policy.InsuranceIndustry.InsuranceIndustryName;
            if (policy.InsuranceIndustry.InsuranceIndustryName == "דירה")
            {
                if (Directory.Exists(clientDirPath + @"/" + client.ClientID + @"/אלמנטרי/דירה"))
                {
                    industryPathDirs = Directory.GetDirectories(clientDirPath + @"/" + client.ClientID + @"/אלמנטרי/דירה", "*" + policy.PolicyNumber + "*");

                }
            }
            else if (policy.InsuranceIndustry.InsuranceIndustryName == "עסק")
            {
                if (Directory.Exists(clientDirPath + @"/" + client.ClientID + @"/אלמנטרי/עסק"))
                {
                    industryPathDirs = Directory.GetDirectories(clientDirPath + @"/" + client.ClientID + @"/אלמנטרי/עסק", "*" + policy.PolicyNumber + "*");
                }
            }
            else if (policy.InsuranceIndustry.InsuranceIndustryName.Contains("רכב"))
            {
                if (Directory.Exists(clientDirPath + @"\" + client.ClientID + @"\אלמנטרי\רכב"))
                {
                    industryPathDirs = Directory.GetDirectories(clientDirPath + @"\" + client.ClientID + @"\אלמנטרי\רכב", "*" + @policy.PolicyNumber + "*");
                }
            }
            //if (industryPathDirs.Count() > 0)
            //{
            //    string[] paths = Directory.GetDirectories(industryPathDirs[0], "תביעות*");
            //    if (paths.Count() > 0)
            //    {
            //        path = paths[0];
            //    }
            //}
            btnThirdParty1.IsEnabled = false;
            btnThirdParty2.IsEnabled = false;
            btnThirdParty3.IsEnabled = false;
            btnThirdParty4.IsEnabled = false;
            btnThirdParty5.IsEnabled = false;
            lblNameSpace.Content = client.FirstName + " " + client.LastName;
            lblCellPhoneSpace.Content = client.CellPhone;
            lblEmailSpace.Content = client.Email;
            lblEndDateSpace.Content = ((DateTime)policy.EndDate).Date;
            lblIdSpace.Content = client.IdNumber;
            lblInsuranceTypeSpace.Content = policy.InsuranceIndustry.InsuranceIndustryName + " " + policy.ElementaryInsuranceType.ElementaryInsuranceTypeName;
            lblPhoneSpace.Content = client.PhoneHome;
            lblStartDateSpace.Content = ((DateTime)policy.StartDate).Date;
            lblCompanySpace.Content = policy.Company.CompanyName;
            if (policy.CarPolicy != null)
            {
                lblCarNumber.Visibility = Visibility.Visible;
                lblCarNumberSpace.Content = policy.CarPolicy.RegistrationNumber;
            }
            dpOpenDate.SelectedDate = DateTime.Now;
            txtPolicyNumber.Text = policy.PolicyNumber;
            if (policy.InsuranceIndustry.InsuranceIndustryName.Contains("רכב"))
            {
                tbItemCar.Visibility = Visibility.Visible;
                tbItemCarThirdParty.Visibility = Visibility.Visible;
                tbItemAptBusiness.Visibility = Visibility.Collapsed;
            }
            else
            {
                tbItemAptBusiness.Visibility = Visibility.Visible;
                tbItemCar.Visibility = Visibility.Collapsed;
                tbItemCarThirdParty.Visibility = Visibility.Collapsed;
                txtAptBusinessTotalAmountClaimed.Text = "0";
            }
            cbClaimTypeBinding();
            cbClaimConditionBinding();
            cbCompanyBinding();
            cbVehicleTypeBinding();
            cbInsuranceTypeBinding();
            if (claim != null)
            {
                dpOpenDate.SelectedDate = claim.OpenDate;
                txtPolicyNumber.Text = claim.ElementaryPolicy.PolicyNumber;
                txtClaimNumber.Text = claim.ClaimNumber;
                txtThirdPartyClaimNumber.Text = claim.ThirdPartyClaimNumber;
                dpDeliveryDateToCompany.SelectedDate = claim.DeliveredToCompanyDate;
                dpMoneyReceivedDate.SelectedDate = claim.MoneyReceivedDate;
                if (claim.ClaimAmount != null)
                {
                    txtAmountClaimed.Text = ((decimal)claim.ClaimAmount).ToString("0.##");
                }
                if (claim.AmountReceived != null)
                {
                    txtAmountReceived.Text = ((decimal)claim.AmountReceived).ToString("0.##");
                }
                dpEventDate.SelectedDate = claim.EventDate;
                if (claim.EventTime != null)
                {
                    tpEventHour.Value = Convert.ToDateTime(claim.EventTime.ToString());
                }
                txtEventPlace.Text = claim.EventPlace;
                txtEventDescription.Text = claim.EventDescription;
                txtAppraiserName.Text = claim.AppraiserName;
                txtAppraiserAddress.Text = claim.AppraiserAddress;
                txtAppraiserPhone.Text = claim.AppraiserPhone;
                txtAppraiserCellPhone.Text = claim.AppraiserCellPhone;
                txtAppraiserFax.Text = claim.AppraiserFax;
                txtAppraiserEmail.Text = claim.AppraiserEmail;
                witnesses = claimLogic.GetWitnessesByClaim(claim.ElementaryClaimID);
                if (claim.ElementaryClaimTypeID != null)
                {
                    SelectClaimTypeItemInCb((int)claim.ElementaryClaimTypeID);
                }
                if (claim.ClaimStatus == true)
                {
                    cbStatus.SelectedIndex = 0;
                }
                else
                {
                    cbStatus.SelectedIndex = 1;
                }
                //var claimsConditions = cbClaimCondition.Items;
                //foreach (var item in claimsConditions)
                //{
                //    ElementaryClaimCondition claimCondition = (ElementaryClaimCondition)item;
                //    if (claimCondition.ElementaryClaimConditionID == claim.ElementaryClaimConditionID)
                //    {
                //        cbClaimCondition.SelectedItem = item;
                //        break;
                //    }
                //}
                if (claim.ElementaryClaimConditionID != null)
                {
                    SelectClaimConditionItemInCb((int)claim.ElementaryClaimConditionID);
                }
                dgWitnessesBinding();
                trackings = trackingLogic.GetAllClaimElementaryTrackingsByClaimID(claim.ElementaryClaimID);
                dgTrackingsBinding();
                if (policy.InsuranceIndustry.InsuranceIndustryName.Contains("רכב"))
                {
                    CarClaim carClaim = claimLogic.GetCarClaimByClaimID(claim.ElementaryClaimID);
                    if (carClaim.DamageAmount != null)
                    {
                        txtDamageByAppraiser.Text = ((decimal)carClaim.DamageAmount).ToString("0.##");
                    }
                    if (carClaim.AppraiserFees != null)
                    {
                        txtAppraiserFees.Text = ((decimal)carClaim.AppraiserFees).ToString("0.##");
                    }
                    if (carClaim.CarDepreciation != null)
                    {
                        txtCarDepreciation.Text = ((decimal)carClaim.CarDepreciation).ToString("0.##");
                    }
                    if (carClaim.OtherDamages != null)
                    {
                        txtOtherDamages.Text = ((decimal)carClaim.OtherDamages).ToString("0.##");
                    }
                    txtDamageByAppraiser_LostFocus(this, new RoutedEventArgs());
                    txtDamagesDescription.Text = carClaim.DamageDescription;
                    txtDriverLastName.Text = carClaim.DriverLastName;
                    txtDriverFirstName.Text = carClaim.DriverFirsName;
                    txtDriverId.Text = carClaim.DriverIdNumber;
                    dpDriverBirthDate.SelectedDate = carClaim.DriverBirthDate;
                    txtDriverRelationWithPolicyHolder.Text = carClaim.DriverProximityToPolicyHolder;
                    txtDriverAddress.Text = carClaim.DriverAddress;
                    txtDriverPhone.Text = carClaim.DriverPhone;
                    txtDriverCellPhone.Text = carClaim.DriverCellPhone;
                    txtDriverEmail.Text = carClaim.DriverEmail;
                    txtDriverLicenseNumber.Text = carClaim.DriverLicenseNumber;
                    dpDriverLicenseDate.SelectedDate = carClaim.DriverLicenseDate;
                    dpDriverLicenseValid.SelectedDate = carClaim.DriverLicenseValid;
                    txtDriverLicenseType.Text = carClaim.DriverLicenseType;
                    chbIsArrangementGarage.IsChecked = carClaim.IsGarageArrangement;
                    txtGarageName.Text = carClaim.GarageName;
                    txtGarageAddress.Text = carClaim.GarageAddress;
                    txtGaragePhone.Text = carClaim.GaragePhone;
                    txtGarageFax.Text = carClaim.GarageFax;
                    txtContactName.Text = carClaim.GarageContactName;
                    txtEmail.Text = carClaim.GarageEmail;
                    carThirdParties = claimLogic.GetThirdPartiesByClaimID(claim.ElementaryClaimID);
                    if (carThirdParties.Count > 0)
                    {
                        btnThirdParty1.IsEnabled = true;
                        DisplayThirdPartyDetails(0);
                    }
                    if (carThirdParties.Count > 1)
                    {
                        btnThirdParty2.IsEnabled = true;
                    }
                    if (carThirdParties.Count > 2)
                    {
                        btnThirdParty3.IsEnabled = true;
                    }
                    if (carThirdParties.Count > 3)
                    {
                        btnThirdParty4.IsEnabled = true;
                    }
                    if (carThirdParties.Count > 4)
                    {
                        btnThirdParty5.IsEnabled = true;
                    }
                    //dgCarThirdPartiesBinding();
                }
                else
                {
                    ApartmentBusinessClaim aptBusinessClaim = claimLogic.GetAptBusinessClaimByClaimID(claim.ElementaryClaimID);
                    damages = claimLogic.GetAllDamagesByAptOrBussinesClaim(claim.ElementaryClaimID);
                    dgAptBusinessCalimDetailsBinding();
                    if (aptBusinessClaim.TotalAmountClaimed != null)
                    {
                        txtAptBusinessTotalAmountClaimed.Text = ((decimal)aptBusinessClaim.TotalAmountClaimed).ToString("0.##");
                    }
                    if (aptBusinessClaim.TotalAmountClaimed != null)
                    {
                        totalAmountClaimed = (decimal)aptBusinessClaim.TotalAmountClaimed;
                    }
                    if (aptBusinessClaim.IsThirdPartyDamages == true)
                    {
                        chbAptBusinessIsThirdPartyDamage.IsChecked = true;
                        ApartmentBusinessThirdParty thirdParty = claimLogic.GetAptBusinessThirdPartyByClaimId(claim.ElementaryClaimID);
                        if (thirdParty != null)
                        {
                            txtAptBusinessDamagedName.Text = thirdParty.DamagedName;
                            txtAptBusinessDamagedId.Text = thirdParty.DamagedIdNumber;
                            txtAptBusinessDamagedAddress.Text = thirdParty.DamagedAddress;
                            txtAptBusinessDamagedPhone.Text = thirdParty.DamagedPhone;
                            txtAptBusinessDamagedCellPhone.Text = thirdParty.DamagedCellPhone;
                            txtAptBusinessDamagedEmail.Text = thirdParty.DamagedEmail;
                            txtAptBusinessThirdPartyDamageDescipion.Text = thirdParty.DamageDescription;
                            if (thirdParty.TotalAmoundClaimed != null)
                            {
                                txtAptBusinessThirdPartyTotalAmountClaimed.Text = ((decimal)thirdParty.TotalAmoundClaimed).ToString("0.##");
                            }
                            var companies = cbAptBusinessThirdPartyInsuranceCompany.Items;
                            foreach (var item in companies)
                            {
                                InsuranceCompany company = (InsuranceCompany)item;
                                if (company.CompanyID == thirdParty.DamagedCompanyID)
                                {
                                    cbAptBusinessThirdPartyInsuranceCompany.SelectedItem = item;
                                    break;
                                }
                            }
                        }
                    }
                }
            }
        }

        private void cbInsuranceTypeBinding()
        {
            //cbThirdPartyInsuranceType.ItemsSource = industryLogic.GetAllActiveThirdPartyInsuranceTypes();
            //cbThirdPartyInsuranceType.DisplayMemberPath = "ThirdPartyInsuranceTypeName";
            cbThirdPartyInsuranceType.ItemsSource = industryLogic.GetAllActiveCarInsuranceTypes();
            cbThirdPartyInsuranceType.DisplayMemberPath = "ElementaryInsuranceTypeName";
        }

        private void cbVehicleTypeBinding()
        {
            cbThirdPartyCarType.ItemsSource = vehicleLogic.GetAllActiveVehicleTypes();
            cbThirdPartyCarType.DisplayMemberPath = "Description";
        }

        private void cbCompanyBinding()
        {
            cbAptBusinessThirdPartyInsuranceCompany.ItemsSource = insuranceLogic.GetActiveCompaniesByInsurance("אלמנטרי");
            cbAptBusinessThirdPartyInsuranceCompany.DisplayMemberPath = "Company.CompanyName";
            cbThirdPartyInsuranceCompany.ItemsSource = insuranceLogic.GetActiveCompaniesByInsurance("אלמנטרי");
            cbThirdPartyInsuranceCompany.DisplayMemberPath = "Company.CompanyName";
        }

        private void chbAptBusinessIsThirdPartyDamage_Checked(object sender, RoutedEventArgs e)
        {
            gbAptBusinessThirdPartyDetails.IsEnabled = true;
        }

        private void chbAptBusinessIsThirdPartyDamage_Unchecked(object sender, RoutedEventArgs e)
        {
            gbAptBusinessThirdPartyDetails.IsEnabled = false;
        }

        private void btnUpdateClaimTypeTable_Click(object sender, RoutedEventArgs e)
        {
            int? selectedItemId = null;
            if (cbClaimType.SelectedItem != null)
            {
                selectedItemId = ((ElementaryClaimType)cbClaimType.SelectedItem).ElementaryClaimTypeID;
            }
            frmUpdateTable updateClaimTypesTable = new frmUpdateTable(new ElementaryClaimType() { InsuranceIndustryID = policy.InsuranceIndustryID });
            updateClaimTypesTable.ShowDialog();
            cbClaimTypeBinding();
            if (updateClaimTypesTable.ItemAdded != null)
            {
                SelectClaimTypeItemInCb(((ElementaryClaimType)updateClaimTypesTable.ItemAdded).ElementaryClaimTypeID);
            }
            else if (selectedItemId != null)
            {
                SelectClaimTypeItemInCb((int)selectedItemId);
            }
            //SelectCbItem(cbInsuranceCompany,);
        }

        private void SelectClaimTypeItemInCb(int id)
        {
            var items = cbClaimType.Items;
            foreach (var item in items)
            {
                ElementaryClaimType parsedItem = (ElementaryClaimType)item;
                if (parsedItem.ElementaryClaimTypeID == id)
                {
                    cbClaimType.SelectedItem = item;
                    break;
                }
            }
        }

        private void cbClaimTypeBinding()
        {
            cbClaimType.ItemsSource = claimLogic.GetActiveElementaryClaimTypesByIndustry(policy.InsuranceIndustryID);
            cbClaimType.DisplayMemberPath = "ClaimTypeName";
        }

        private void btnUpdateClaimConditionTable_Click(object sender, RoutedEventArgs e)
        {
            int? selectedItemId = null;
            if (cbClaimCondition.SelectedItem != null)
            {
                selectedItemId = ((ElementaryClaimCondition)cbClaimCondition.SelectedItem).ElementaryClaimConditionID;
            }
            frmUpdateTable updateClaimConditionsTable = new frmUpdateTable(new ElementaryClaimCondition() { InsuranceIndustryID = policy.InsuranceIndustryID });
            updateClaimConditionsTable.ShowDialog();
            cbClaimConditionBinding();
            if (updateClaimConditionsTable.ItemAdded != null)
            {
                SelectClaimConditionItemInCb(((ElementaryClaimCondition)updateClaimConditionsTable.ItemAdded).ElementaryClaimConditionID);
            }
            else if (selectedItemId != null)
            {
                SelectClaimConditionItemInCb((int)selectedItemId);
            }
            //SelectCbItem(cbInsuranceCompany,);
        }

        private void SelectClaimConditionItemInCb(int id)
        {
            var items = cbClaimCondition.Items;
            foreach (var item in items)
            {
                ElementaryClaimCondition parsedItem = (ElementaryClaimCondition)item;
                if (parsedItem.ElementaryClaimConditionID == id)
                {
                    cbClaimCondition.SelectedItem = item;
                    break;
                }
            }
        }

        private void cbClaimConditionBinding()
        {
            cbClaimCondition.ItemsSource = claimLogic.GetActiveElementaryClaimConditionsByIndustrie(policy.InsuranceIndustryID);
            cbClaimCondition.DisplayMemberPath = "Description";
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (nullErrorList != null && nullErrorList.Count > 0)
                {
                    foreach (var item in nullErrorList)
                    {
                        if (item is TextBox)
                        {
                            ((TextBox)item).ClearValue(BorderBrushProperty);
                        }
                        else if (item is ComboBox)
                        {
                            ((ComboBox)item).ClearValue(BorderBrushProperty);
                        }
                        else if (item is DatePicker)
                        {
                            ((DatePicker)item).ClearValue(BorderBrushProperty);
                        }
                    }
                }
                nullErrorList = validation.InputNullValidation(new object[] { dpEventDate });
                if (nullErrorList.Count > 0)
                {
                    tbItemGeneral.Focus();
                    MessageBox.Show("נא למלאות שדות חובה", "", MessageBoxButton.OK, MessageBoxImage.Error);
                    cancelClose = true;
                    return;
                }
                txtAmountClaimed.ClearValue(BorderBrushProperty);
                txtAmountReceived.ClearValue(BorderBrushProperty);
                errorList = validations.OnlyNumbersValidation(new TextBox[] { txtAmountClaimed, txtAmountReceived });
                if (errorList.Count > 0)
                {
                    tbItemGeneral.IsSelected = true;
                    MessageBox.Show("נא להזין מספרים או נקודה בלבד", "", MessageBoxButton.OK, MessageBoxImage.Error);
                    cancelClose = true;
                    return;
                }
                bool isClaimOpen = true;
                if (cbStatus.SelectedIndex == 1)
                {
                    isClaimOpen = false;
                }
                decimal? amounClaimed = null;
                if (txtAmountClaimed.Text != "")
                {
                    amounClaimed = decimal.Parse(txtAmountClaimed.Text);
                }
                decimal? amountRecived = null;
                if (txtAmountReceived.Text != "")
                {
                    amountRecived = decimal.Parse(txtAmountReceived.Text);
                }
                DateTime openDate = (DateTime)dpOpenDate.SelectedDate;
                TimeSpan? eventTime = null;
                if (tpEventHour.Value != null)
                {
                    eventTime = ((DateTime)tpEventHour.Value).TimeOfDay;
                }
                int? typeID = null;
                if (cbClaimType.SelectedItem != null)
                {
                    typeID = ((ElementaryClaimType)cbClaimType.SelectedItem).ElementaryClaimTypeID;
                }
                int? conditionID = null;
                if (cbClaimCondition.SelectedItem != null)
                {
                    conditionID = ((ElementaryClaimCondition)cbClaimCondition.SelectedItem).ElementaryClaimConditionID;
                }

                if (claim == null)
                {
                    claimId = claimLogic.InsertElementaryClaim(policy.ElementaryPolicyID, typeID, conditionID, isClaimOpen, txtClaimNumber.Text, openDate, txtThirdPartyClaimNumber.Text, dpDeliveryDateToCompany.SelectedDate, dpMoneyReceivedDate.SelectedDate, amounClaimed, amountRecived, dpEventDate.SelectedDate, eventTime, txtEventPlace.Text, txtEventDescription.Text, txtAppraiserName.Text, txtAppraiserAddress.Text, txtAppraiserPhone.Text, txtAppraiserCellPhone.Text, txtAppraiserFax.Text, txtAppraiserEmail.Text);
                    if (industryPathDirs.Count() > 0)
                    {
                        path = industryPathDirs[0] + @"/תביעה " + ((DateTime)dpEventDate.SelectedDate).ToString("dd-MM-yyyy") + " " + policy.InsuranceIndustry.InsuranceIndustryName;
                    }
                }
                else
                {
                    claimId = claimLogic.UpdateElementaryClaim(claim.ElementaryClaimID, typeID, conditionID, isClaimOpen, txtClaimNumber.Text, openDate, txtThirdPartyClaimNumber.Text, dpDeliveryDateToCompany.SelectedDate, dpMoneyReceivedDate.SelectedDate, amounClaimed, amountRecived, dpEventDate.SelectedDate, eventTime, txtEventPlace.Text, txtEventDescription.Text, txtAppraiserName.Text, txtAppraiserAddress.Text, txtAppraiserPhone.Text, txtAppraiserCellPhone.Text, txtAppraiserFax.Text, txtAppraiserEmail.Text);
                    if (industryPathDirs.Count() > 0)
                    {
                        newPath = industryPathDirs[0] + @"/תביעה " + ((DateTime)dpEventDate.SelectedDate).ToString("dd-MM-yyyy") + " " + policy.InsuranceIndustry.InsuranceIndustryName;
                        path = industryPathDirs[0] + @"/תביעה " + ((DateTime)claim.EventDate).ToString("dd-MM-yyyy") + " " + policy.InsuranceIndustry.InsuranceIndustryName;
                    }
                }
                SaveUnsavedWitness();
                if (witnesses.Count > 0)
                {
                    claimLogic.InsertWitnesses(witnesses, claimId);
                }
                if (policy.InsuranceIndustry.InsuranceIndustryName.Contains("רכב"))
                {
                    txtDamageByAppraiser.ClearValue(BorderBrushProperty);
                    txtAppraiserFees.ClearValue(BorderBrushProperty);
                    txtCarDepreciation.ClearValue(BorderBrushProperty);
                    txtOtherDamages.ClearValue(BorderBrushProperty);
                    errorList = validations.OnlyNumbersValidation(new TextBox[] { txtDamageByAppraiser, txtAppraiserFees, txtCarDepreciation, txtOtherDamages });
                    if (errorList.Count > 0)
                    {
                        tbItemCar.IsSelected = true;
                        MessageBox.Show("נא להזין מספרים או נקודה בלבד", "", MessageBoxButton.OK, MessageBoxImage.Error);
                        cancelClose = true;
                        return;
                    }
                    decimal? damagesByAppraiser = null;
                    if (txtDamageByAppraiser.Text != "")
                    {
                        damagesByAppraiser = decimal.Parse(txtDamageByAppraiser.Text);
                    }
                    decimal? appraiserFees = null;
                    if (txtAppraiserFees.Text != "")
                    {
                        appraiserFees = decimal.Parse(txtAppraiserFees.Text);
                    }
                    decimal? carDepreciation = null;
                    if (txtCarDepreciation.Text != "")
                    {
                        carDepreciation = decimal.Parse(txtCarDepreciation.Text);
                    }
                    decimal? otherDamages = null;
                    if (txtOtherDamages.Text != "")
                    {
                        otherDamages = decimal.Parse(txtOtherDamages.Text);
                    }
                    if (claim == null)
                    {
                        claimLogic.InsertCarClaim(claimId, damagesByAppraiser, appraiserFees, carDepreciation, otherDamages, txtDamagesDescription.Text, txtDriverLastName.Text, txtDriverFirstName.Text, txtDriverId.Text, dpDriverBirthDate.SelectedDate, txtDriverAddress.Text, txtDriverPhone.Text, txtDriverCellPhone.Text, txtDriverEmail.Text, txtDriverLicenseNumber.Text, dpDriverLicenseDate.SelectedDate, dpDriverLicenseValid.SelectedDate, txtDriverLicenseType.Text, txtDriverRelationWithPolicyHolder.Text, txtGarageName.Text, txtGarageAddress.Text, txtGaragePhone.Text, txtGarageFax.Text, txtEmail.Text, txtContactName.Text, chbIsArrangementGarage.IsChecked);

                    }
                    else
                    {
                        claimLogic.UpdateCarClaim(claimId, damagesByAppraiser, appraiserFees, carDepreciation, otherDamages, txtDamagesDescription.Text, txtDriverLastName.Text, txtDriverFirstName.Text, txtDriverId.Text, dpDriverBirthDate.SelectedDate, txtDriverAddress.Text, txtDriverPhone.Text, txtDriverCellPhone.Text, txtDriverEmail.Text, txtDriverLicenseNumber.Text, dpDriverLicenseDate.SelectedDate, dpDriverLicenseValid.SelectedDate, txtDriverLicenseType.Text, txtDriverRelationWithPolicyHolder.Text, txtGarageName.Text, txtGarageAddress.Text, txtGaragePhone.Text, txtGarageFax.Text, txtEmail.Text, txtContactName.Text, chbIsArrangementGarage.IsChecked);
                    }
                    SaveUnsavedThirdPary();
                    if (carThirdParties.Count > 0)
                    {
                        claimLogic.InsertCarThirdParties(claimId, carThirdParties);
                    }
                }
                else
                {
                    decimal? totalAmountClaimed = null;
                    if (txtAptBusinessTotalAmountClaimed.Text != "")
                    {
                        totalAmountClaimed = decimal.Parse(txtAptBusinessTotalAmountClaimed.Text);
                    }
                    if (claim == null)
                    {
                        claimLogic.InsertAptBusinessClaim(claimId, totalAmountClaimed, (bool)chbAptBusinessIsThirdPartyDamage.IsChecked);
                    }
                    else
                    {
                        claimLogic.UpdateAptBusinessClaim(claimId, totalAmountClaimed, (bool)chbAptBusinessIsThirdPartyDamage.IsChecked);
                    }
                    SaveUnsavedDamages();
                    if (damages.Count > 0)
                    {
                        claimLogic.InsertDamages(claimId, damages);
                    }
                    if ((bool)chbAptBusinessIsThirdPartyDamage.IsChecked)
                    {
                        //insert third party damages
                        txtAptBusinessThirdPartyTotalAmountClaimed.ClearValue(BorderBrushProperty);
                        errorList = validations.OnlyNumbersValidation(new TextBox[] { txtAptBusinessThirdPartyTotalAmountClaimed });
                        if (errorList.Count > 0)
                        {
                            MessageBox.Show("נא להזין מספרים או נקודה בלבד", "", MessageBoxButton.OK, MessageBoxImage.Error);
                            cancelClose = true;
                            return;
                        }
                        decimal? totalAmountClaimedByThirdParty = null;
                        if (txtAptBusinessThirdPartyTotalAmountClaimed.Text != "")
                        {
                            totalAmountClaimedByThirdParty = decimal.Parse(txtAptBusinessThirdPartyTotalAmountClaimed.Text);
                        }

                        if (claim == null)
                        {
                            claimLogic.InsertAptBussinesThirdPartyDetails(claimId, txtAptBusinessDamagedName.Text, txtAptBusinessDamagedId.Text, txtAptBusinessDamagedAddress.Text, txtAptBusinessDamagedPhone.Text, txtAptBusinessDamagedCellPhone.Text, txtAptBusinessDamagedEmail.Text, txtAptBusinessThirdPartyDamageDescipion.Text, totalAmountClaimedByThirdParty, (InsuranceCompany)cbAptBusinessThirdPartyInsuranceCompany.SelectedItem);
                        }
                        else
                        {
                            claimLogic.UpdateAptBussinesThirdPartyDetails(claimId, txtAptBusinessDamagedName.Text, txtAptBusinessDamagedId.Text, txtAptBusinessDamagedAddress.Text, txtAptBusinessDamagedPhone.Text, txtAptBusinessDamagedCellPhone.Text, txtAptBusinessDamagedEmail.Text, txtAptBusinessThirdPartyDamageDescipion.Text, totalAmountClaimedByThirdParty, (InsuranceCompany)cbAptBusinessThirdPartyInsuranceCompany.SelectedItem);
                        }
                    }
                }
                if (trackings.Count > 0)
                {
                    trackingLogic.InsertElementaryClaimTrackings(trackings, claimId, user.UserID);
                }
                //path = @"Clients/" + client.ClientID + "/אלמנטרי/דירה/" + txtPolicyNumber.Text;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "", MessageBoxButton.OK, MessageBoxImage.Error);
            }

            if (claim == null)
            {
                if (path != "")
                {
                    if (!Directory.Exists(@path))
                    {
                        Directory.CreateDirectory(@path);
                    }
                }
                else
                {
                    System.Windows.Forms.MessageBox.Show("לא ניתן ליצור את תיקיית התביעה");
                }
            }
            else
            {
                if (path != "")
                {
                    if (Directory.Exists(path) && newPath != "")
                    {
                        if (path != newPath)
                        {
                            try
                            {
                                Directory.Move(path, newPath);
                            }
                            catch (Exception)
                            {
                                System.Windows.Forms.MessageBox.Show("לא ניתן למצוא את נתיב התיקייה");
                            }
                        }
                    }
                }
                else
                {
                    if (newPath != "")
                    {
                        Directory.CreateDirectory(newPath);
                    }
                }
            }

            if (sender is Button)
            {
                Button btn = (Button)sender;
                if (btn.Name == "btnSave")
                {
                    confirmBeforeClosing = false;
                    Close();
                }
            }
        }

        private void SaveUnsavedWitness()
        {
            if (txtWitnessName.Text != "" || txtWitnessAddress.Text != "" || txtWitnessPhone.Text != "" || txtWitnessCellPhone.Text != "")
            {
                btnAddWitness_Click(this, new RoutedEventArgs());
            }
        }

        private void SaveUnsavedDamages()
        {
            if (txtPropertyAndDamageDescription.Text != "" || txtSurveyItemNumber.Text != "" || txtSurveyItemAmount.Text != "" || txtAptBusinessAmountClaimed.Text != "" || txtAptBusinessComments.Text != "")
            {
                btnAddItem_Click(this, new RoutedEventArgs());
            }
        }

        private void SaveUnsavedThirdPary()
        {
            if (txtThirdPartyCarNumber.Text != "" || cbThirdPartyCarType.SelectedItem != null || txtThirdPartyCarManufacturer.Text != "" || txtThirdPartyPolicyHolderLastName.Text != "" || txtThirdPartyPolicyHolderFirstName.Text != "" || txtThirdPartyPolicyHolderAddress.Text != "" || txtThirdPartyPolicyHolderId.Text != "" || txtThirdPartyPolicyHolderPhone.Text != "" || txtThirdPartyPolicyHolderCellPhone.Text != "" || txtThirdPartyPolicyNumber.Text != "" || cbThirdPartyInsuranceType.SelectedItem != null || cbThirdPartyInsuranceCompany.SelectedItem != null || txtThirdPartyAgentName.Text != "" || txtThirdPartyAgentPhone.Text != "" || txtThirdPartyAgentPhone2.Text != "" || txtThirdPartyAgentEmail.Text != "" || txtThirdPartyDriverLastName.Text != "" || txtThirdPartyDriverFirstName.Text != "" || txtThirdPartyDriverId.Text != "" || txtThirdPartyDriverAddress.Text != "" || txtThirdPartyDriverPhone.Text != "" || txtThirdPartyDriverCellPhone.Text != "" || chbIsHurtPeople.IsChecked == true || txtThirdPartyDamageDescription.Text != "")
            {
                if (!isThirdPartyUpdate)
                {
                    btnSaveThirdParty_Click(this, new RoutedEventArgs());
                }
                else
                {
                    btnUpdateThirdParty_Click(this, new RoutedEventArgs());
                }
            }
        }

        private void btnAddWitness_Click(object sender, RoutedEventArgs e)
        {
            if (witnessSelected == null)
            {
                ElementaryWitness witness = new ElementaryWitness() { WitnessName = txtWitnessName.Text, WittnessAddress = txtWitnessAddress.Text, WitnessPhone = txtWitnessPhone.Text, WitnessCellPhone = txtWitnessCellPhone.Text };
                witnesses.Add(witness);
            }
            else
            {
                witnessSelected.WitnessCellPhone = txtWitnessCellPhone.Text;
                witnessSelected.WitnessName = txtWitnessName.Text;
                witnessSelected.WitnessPhone = txtWitnessPhone.Text;
                witnessSelected.WittnessAddress = txtWitnessAddress.Text;
            }
            dgWitnessesBinding();
            dgWitnesses.UnselectAll();
            txtWitnessAddress.Clear();
            txtWitnessCellPhone.Clear();
            txtWitnessName.Clear();
            txtWitnessPhone.Clear();
        }

        private void dgWitnessesBinding()
        {
            dgWitnesses.ItemsSource = witnesses.ToList();
            lv.AutoSizeColumns(dgWitnesses.View);
        }

        private void btnAddItem_Click(object sender, RoutedEventArgs e)
        {
            txtSurveyItemAmount.ClearValue(BorderBrushProperty);
            txtAptBusinessAmountClaimed.ClearValue(BorderBrushProperty);
            errorList = validations.OnlyNumbersValidation(new TextBox[] { txtSurveyItemAmount, txtAptBusinessAmountClaimed });
            if (errorList.Count > 0)
            {
                MessageBox.Show("נא להזין מספרים או נקודה בלבד", "", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            decimal? surveyItemAmount = null;
            if (txtSurveyItemAmount.Text != "")
            {
                surveyItemAmount = decimal.Parse(txtSurveyItemAmount.Text);
            }
            decimal? amountClaimed = null;
            if (txtAptBusinessAmountClaimed.Text != "")
            {
                amountClaimed = decimal.Parse(txtAptBusinessAmountClaimed.Text);
            }
            if (damageSelected != null)
            {
                damageSelected.AmountClaimed = amountClaimed;
                damageSelected.Comments = txtAptBusinessComments.Text;
                damageSelected.DamageAndPropertyDescription = txtPropertyAndDamageDescription.Text;
                damageSelected.SurveyItemAmount = surveyItemAmount;
                damageSelected.SurveyItemNumber = txtSurveyItemNumber.Text;
                txtAptBusinessTotalAmountClaimed.Text = ((decimal)(damages.Where(d => d.AmountClaimed != null).Sum(d => d.AmountClaimed))).ToString("0.##");
            }
            else
            {
                ApartmentBusinessDamage damage = new ApartmentBusinessDamage() { DamageAndPropertyDescription = txtPropertyAndDamageDescription.Text, SurveyItemNumber = txtSurveyItemNumber.Text, SurveyItemAmount = surveyItemAmount, AmountClaimed = amountClaimed, Comments = txtAptBusinessComments.Text };
                damages.Add(damage);
                if (amountClaimed != null)
                {
                    totalAmountClaimed = totalAmountClaimed + (decimal)amountClaimed;
                }
                if (totalAmountClaimed != null)
                {
                    txtAptBusinessTotalAmountClaimed.Text = ((decimal)totalAmountClaimed).ToString("0.##");
                }
            }
            dgAptBusinessCalimDetailsBinding();

            txtPropertyAndDamageDescription.Clear();
            txtSurveyItemNumber.Clear();
            txtSurveyItemAmount.Clear();
            txtAptBusinessAmountClaimed.Clear();
            txtAptBusinessComments.Clear();
            damageSelected = null;
        }

        private void dgAptBusinessCalimDetailsBinding()
        {
            dgAptBusinessCalimDetails.ItemsSource = damages.ToList();
        }
        private void btnAddThirdParty_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btnNewTracking_Click(object sender, RoutedEventArgs e)
        {
            frmTracking newTrackingWindow = new frmTracking(client, policy, user);
            newTrackingWindow.ShowDialog();
            if (newTrackingWindow.trackingContent != null)
            {
                trackings.Add(new ElementaryClaimTracking { TrackingContent = newTrackingWindow.trackingContent, Date = DateTime.Now, User = user });
                dgTrackingsBinding();
            }
        }

        private void dgTrackingsBinding()
        {
            dgTrackings.ItemsSource = trackings.OrderByDescending(t => t.Date);
            lv.AutoSizeColumns(dgTrackings.View);
        }

        private void btnDeleteTracking_Click(object sender, RoutedEventArgs e)
        {
            if (dgTrackings.SelectedItem == null)
            {
                MessageBox.Show("נא לסמן מעקב בטבלה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            if (MessageBox.Show("למחוק מעקב", "אישור", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                if (((ElementaryClaimTracking)dgTrackings.SelectedItem).ElementaryClaimTrackingID != 0)
                {
                    trackingLogic.DeleteElementaryClaimTracking((ElementaryClaimTracking)dgTrackings.SelectedItem);
                }
                trackings.Remove((ElementaryClaimTracking)dgTrackings.SelectedItem);
                dgTrackingsBinding();
            }
            else
            {
                dgTrackings.UnselectAll();
            }
        }

        private void dgTrackings_MouseDown(object sender, MouseButtonEventArgs e)
        {
            dgTrackings.UnselectAll();
        }

        private void btnUpdateTracking_Click(object sender, RoutedEventArgs e)
        {
            if (dgTrackings.SelectedItem == null)
            {
                MessageBox.Show("נא לסמן מעקב בטבלה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            ElementaryClaimTracking trackingToUpdate = (ElementaryClaimTracking)dgTrackings.SelectedItem;
            frmTracking updateTrackingWindow = new frmTracking(trackingToUpdate, client, policy, user);
            updateTrackingWindow.ShowDialog();
            trackingToUpdate.TrackingContent = updateTrackingWindow.trackingContent;
            dgTrackingsBinding();
            dgTrackings.UnselectAll();
        }

        private void btnSaveThirdParty_Click(object sender, RoutedEventArgs e)
        {
            if (isThirdPartyUpdate)
            {
                btnUpdateThirdParty_Click(sender, e);
                return;
            }
            int? vehicleTypeId = null;
            if (cbThirdPartyCarType.SelectedItem != null)
            {
                vehicleTypeId = ((VehicleType)cbThirdPartyCarType.SelectedItem).VehicleTypeID;
            }
            int? companyId = null;
            if (cbThirdPartyInsuranceCompany.SelectedItem != null)
            {
                companyId = ((InsuranceCompany)cbThirdPartyInsuranceCompany.SelectedItem).CompanyID;
            }
            int? insuranceTypeId = null;
            if (cbThirdPartyInsuranceType.SelectedItem != null)
            {
                insuranceTypeId = ((ElementaryInsuranceType)cbThirdPartyInsuranceType.SelectedItem).ElementaryInsuranceTypeID;
            }
            CarThirdParty newThirdParty = new CarThirdParty()
            {
                RegistrationNumber = txtThirdPartyCarNumber.Text,
                VehicleTypeID = vehicleTypeId,
                Manufacturer = txtThirdPartyCarManufacturer.Text,
                CompanyID = companyId,
                ElementaryInsuranceTypeID = insuranceTypeId,
                PolicyNumber = txtThirdPartyPolicyNumber.Text,
                PolicyHolderName = txtThirdPartyPolicyHolderFirstName.Text + " " + txtThirdPartyPolicyHolderLastName.Text,
                PolicyHolderIdNumber = txtThirdPartyPolicyHolderId.Text,
                PolicyHolderAddress = txtThirdPartyPolicyHolderAddress.Text,
                PolicyHolderPhone = txtThirdPartyPolicyHolderPhone.Text,
                PolicyHolderCellPhone = txtThirdPartyPolicyHolderCellPhone.Text,
                AgentName = txtThirdPartyAgentName.Text,
                AgentPhone = txtThirdPartyAgentPhone.Text,
                AgentSecondPhone = txtThirdPartyAgentPhone2.Text,
                AgentEmail = txtThirdPartyAgentEmail.Text,
                DriverName = txtThirdPartyDriverFirstName.Text + " " + txtThirdPartyDriverLastName.Text,
                DriverIdNumber = txtThirdPartyDriverId.Text,
                DriverAddress = txtThirdPartyDriverAddress.Text,
                DriverPhone = txtThirdPartyDriverPhone.Text,
                DriverCellPhone = txtThirdPartyDriverCellPhone.Text,
                IsHurtPeople = chbIsHurtPeople.IsChecked,
                DamageDescription = txtThirdPartyDamageDescription.Text
            };

            carThirdParties.Add(newThirdParty);
            //dgCarThirdPartiesBinding();

            if (btnThirdParty1.IsEnabled == false)
            {
                btnThirdParty1.IsEnabled = true;
            }
            else if (btnThirdParty2.IsEnabled == false)
            {
                btnThirdParty2.IsEnabled = true;
            }
            else if (btnThirdParty3.IsEnabled == false)
            {
                btnThirdParty3.IsEnabled = true;
            }
            else if (btnThirdParty4.IsEnabled == false)
            {
                btnThirdParty4.IsEnabled = true;
            }
            else if (btnThirdParty5.IsEnabled == false)
            {
                btnThirdParty5.IsEnabled = true;
                //btnSaveThirdParty.IsEnabled = false;
            }
            ClearThirdPartyDetails();
        }

        private void ClearThirdPartyDetails()
        {
            isThirdPartyUpdate = false;
            txtThirdPartyCarNumber.Clear();
            cbThirdPartyCarType.SelectedIndex = -1;
            txtThirdPartyCarManufacturer.Clear();
            cbThirdPartyInsuranceCompany.SelectedIndex = -1;
            cbThirdPartyInsuranceType.SelectedIndex = -1;
            txtThirdPartyPolicyNumber.Clear();
            txtThirdPartyPolicyHolderFirstName.Clear();
            txtThirdPartyPolicyHolderLastName.Clear();
            txtThirdPartyPolicyHolderId.Clear();
            txtThirdPartyPolicyHolderAddress.Clear();
            txtThirdPartyPolicyHolderPhone.Clear();
            txtThirdPartyPolicyHolderCellPhone.Clear();
            txtThirdPartyAgentName.Clear();
            txtThirdPartyAgentPhone.Clear();
            txtThirdPartyAgentPhone2.Clear();
            txtThirdPartyAgentEmail.Clear();
            txtThirdPartyDriverFirstName.Clear();
            txtThirdPartyDriverLastName.Clear();
            txtThirdPartyDriverId.Clear();
            txtThirdPartyDriverAddress.Clear();
            txtThirdPartyDriverPhone.Clear();
            txtThirdPartyDriverCellPhone.Clear();
            chbIsHurtPeople.IsChecked = false;
            txtThirdPartyDamageDescription.Clear();
            //btnSaveThirdParty.IsEnabled = true;
            //btnUpdateThirdParty.IsEnabled = false;
            btnDeleteThirdParty.IsEnabled = false;
            //txtBtnSaveThirdParty.Text = "שמור צד ג";
        }

        //private void dgCarThirdPartiesBinding()
        //{
        //    dgCarThirdParties.ItemsSource = carThirdParties.ToList();
        //}

        //private void dgCarThirdParties_SelectionChanged(object sender, SelectionChangedEventArgs e)
        //{
        //    CarThirdParty thirdPartySelected = (CarThirdParty)dgCarThirdParties.SelectedItem;
        //    if (thirdPartySelected != null)
        //    {

        //        txtBtnSaveThirdParty.Text = "עדכן צד ג";
        //        txtThirdPartyCarNumber.Text = thirdPartySelected.RegistrationNumber;
        //        var vehicleTypes = cbThirdPartyCarType.Items;
        //        foreach (var item in vehicleTypes)
        //        {
        //            VehicleType vehicleType = (VehicleType)item;
        //            if (vehicleType.VehicleTypeID == thirdPartySelected.VehicleTypeID)
        //            {
        //                cbThirdPartyCarType.SelectedItem = item;
        //                break;
        //            }
        //        }
        //        txtThirdPartyCarManufacturer.Text = thirdPartySelected.Manufacturer;
        //        string[] name = thirdPartySelected.PolicyHolderName.Split(null);
        //        if (name.Length > 0)
        //        {
        //            txtThirdPartyPolicyHolderFirstName.Text = name[0];
        //        }
        //        if (name.Length > 1)
        //        {
        //            txtThirdPartyPolicyHolderLastName.Text = name[1];
        //        }
        //        txtThirdPartyPolicyHolderAddress.Text = thirdPartySelected.PolicyHolderAddress;
        //        txtThirdPartyPolicyHolderId.Text = thirdPartySelected.PolicyHolderIdNumber;
        //        txtThirdPartyPolicyHolderPhone.Text = thirdPartySelected.PolicyHolderPhone;
        //        txtThirdPartyPolicyHolderCellPhone.Text = thirdPartySelected.PolicyHolderCellPhone;
        //        txtThirdPartyPolicyNumber.Text = thirdPartySelected.PolicyNumber;
        //        var insuranceTypes = cbThirdPartyInsuranceType.Items;
        //        foreach (var item in insuranceTypes)
        //        {
        //            ElementaryInsuranceType insuranceType = (ElementaryInsuranceType)item;
        //            if (insuranceType.ElementaryInsuranceTypeID == thirdPartySelected.ElementaryInsuranceTypeID)
        //            {
        //                cbThirdPartyInsuranceType.SelectedItem = item;
        //                break;
        //            }
        //        }
        //        var companies = cbThirdPartyInsuranceCompany.Items;
        //        foreach (var item in companies)
        //        {
        //            InsuranceCompany company = (InsuranceCompany)item;
        //            if (company.CompanyID == thirdPartySelected.CompanyID)
        //            {
        //                cbThirdPartyInsuranceCompany.SelectedItem = item;
        //                break;
        //            }
        //        }
        //        txtThirdPartyAgentName.Text = thirdPartySelected.AgentName;
        //        txtThirdPartyAgentPhone.Text = thirdPartySelected.AgentPhone;
        //        txtThirdPartyAgentEmail.Text = thirdPartySelected.AgentEmail;
        //        string[] aptName = thirdPartySelected.PolicyHolderName.Split(null);
        //        if (aptName.Length > 0)
        //        {
        //            txtThirdPartyDriverFirstName.Text = aptName[0];
        //        }
        //        if (aptName.Length > 1)
        //        {
        //            txtThirdPartyDriverLastName.Text = aptName[1];
        //        }
        //        txtThirdPartyDriverId.Text = thirdPartySelected.DriverIdNumber;
        //        txtThirdPartyDriverAddress.Text = thirdPartySelected.DriverAddress;
        //        txtThirdPartyDriverPhone.Text = thirdPartySelected.DriverPhone;
        //        txtThirdPartyDriverCellPhone.Text = thirdPartySelected.DriverCellPhone;
        //        if (thirdPartySelected.IsHurtPeople == true)
        //        {
        //            chbIsHurtPeople.IsChecked = true;
        //        }
        //        txtThirdPartyDamageDescription.Text = thirdPartySelected.DamageDescription;
        //    }
        //}

        private void dgAptBusinessCalimDetails_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            damageSelected = (ApartmentBusinessDamage)dgAptBusinessCalimDetails.SelectedItem;
            if (damageSelected != null)
            {
                txtPropertyAndDamageDescription.Text = damageSelected.DamageAndPropertyDescription;
                txtSurveyItemNumber.Text = damageSelected.SurveyItemNumber;
                if (damageSelected.SurveyItemAmount != null)
                {
                    txtSurveyItemAmount.Text = ((decimal)damageSelected.SurveyItemAmount).ToString("0.##");
                }
                if (damageSelected.AmountClaimed != null)
                {
                    txtAptBusinessAmountClaimed.Text = ((decimal)damageSelected.AmountClaimed).ToString("0.##");
                }
                txtAptBusinessComments.Text = damageSelected.Comments;
            }
        }

        private void dgWitnesses_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            witnessSelected = (ElementaryWitness)dgWitnesses.SelectedItem;
            if (witnessSelected != null)
            {
                txtWitnessName.Text = witnessSelected.WitnessName;
                txtWitnessAddress.Text = witnessSelected.WittnessAddress;
                txtWitnessPhone.Text = witnessSelected.WitnessPhone;
                txtWitnessCellPhone.Text = witnessSelected.WitnessCellPhone;
            }
        }

        //private void dgCarThirdParties_MouseDown(object sender, MouseButtonEventArgs e)
        //{
        //    dgCarThirdParties.UnselectAll();
        //    ClearThirdPartyDetails();
        //}

        private void dgWitnesses_MouseDown(object sender, MouseButtonEventArgs e)
        {
            dgWitnesses.UnselectAll();
            txtWitnessName.Clear();
            txtWitnessAddress.Clear();
            txtWitnessPhone.Clear();
            txtWitnessCellPhone.Clear();
        }

        private void dpEventDate_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            if (dpEventDate.SelectedDate != null)
            {
                tpEventHour.IsEnabled = true;
            }
            else
            {
                tpEventHour.IsEnabled = false;
                tpEventHour.Value = null;
            }
        }

        private void dgTrackings_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            DependencyObject src = (DependencyObject)(e.OriginalSource);
            while (!(src is Control))
            {
                if (!(src is Visual))
                {
                    break;
                }
                src = VisualTreeHelper.GetParent(src);
            }

            if ((src is Control && src.GetType() == typeof(ListViewItem)) || src.GetType() == typeof(Run))
            {
                if (dgTrackings.SelectedItem != null)
                {
                    ElementaryClaimTracking trackingToUpdate = (ElementaryClaimTracking)dgTrackings.SelectedItem;
                    frmTracking updateTrackingWindow = new frmTracking(trackingToUpdate, client, policy, user);
                    updateTrackingWindow.ShowDialog();
                    trackingToUpdate.TrackingContent = updateTrackingWindow.trackingContent;
                    dgTrackingsBinding();
                    dgTrackings.UnselectAll();
                }
            }
        }

        private void btnThirdParty1_Click(object sender, RoutedEventArgs e)
        {
            thirdPartySelectedIndex = 0;
            DisplayThirdPartyDetails(0);
        }

        private void DisplayThirdPartyDetails(int index)
        {
            ClearThirdPartyDetails();
            CarThirdParty thirdPartySelected = carThirdParties[index];
            //btnSaveThirdParty.IsEnabled = false;
            //btnUpdateThirdParty.IsEnabled = true;
            btnDeleteThirdParty.IsEnabled = true;
            //txtBtnSaveThirdParty.Text = "עדכן צד ג";
            txtThirdPartyCarNumber.Text = thirdPartySelected.RegistrationNumber;
            var vehicleTypes = cbThirdPartyCarType.Items;
            foreach (var item in vehicleTypes)
            {
                VehicleType vehicleType = (VehicleType)item;
                if (vehicleType.VehicleTypeID == thirdPartySelected.VehicleTypeID)
                {
                    cbThirdPartyCarType.SelectedItem = item;
                    break;
                }
            }
            txtThirdPartyCarManufacturer.Text = thirdPartySelected.Manufacturer;
            string[] name = thirdPartySelected.PolicyHolderName.Split(null);
            if (name.Length > 0)
            {
                txtThirdPartyPolicyHolderFirstName.Text = name[0];
            }
            if (name.Length > 1)
            {
                txtThirdPartyPolicyHolderLastName.Text = name[1];
            }
            txtThirdPartyPolicyHolderAddress.Text = thirdPartySelected.PolicyHolderAddress;
            txtThirdPartyPolicyHolderId.Text = thirdPartySelected.PolicyHolderIdNumber;
            txtThirdPartyPolicyHolderPhone.Text = thirdPartySelected.PolicyHolderPhone;
            txtThirdPartyPolicyHolderCellPhone.Text = thirdPartySelected.PolicyHolderCellPhone;
            txtThirdPartyPolicyNumber.Text = thirdPartySelected.PolicyNumber;
            //var insuranceTypes = cbThirdPartyInsuranceType.Items;
            //foreach (var item in insuranceTypes)
            //{
            //    ElementaryInsuranceType insuranceType = (ElementaryInsuranceType)item;
            //    if (insuranceType.ElementaryInsuranceTypeID == thirdPartySelected.ElementaryInsuranceTypeID)
            //    {
            //        cbThirdPartyInsuranceType.SelectedItem = item;
            //        break;
            //    }
            //}
            if (thirdPartySelected.ElementaryInsuranceTypeID != null)
            {
                SelectItemInCb((int)thirdPartySelected.ElementaryInsuranceTypeID);
            }
            //var companies = cbThirdPartyInsuranceCompany.Items;
            //foreach (var item in companies)
            //{
            //    InsuranceCompany company = (InsuranceCompany)item;
            //    if (company.CompanyID == thirdPartySelected.CompanyID)
            //    {
            //        cbThirdPartyInsuranceCompany.SelectedItem = item;
            //        break;
            //    }
            //}
            if (thirdPartySelected.CompanyID != null)
            {
                SelectCompanyItemInCb((int)thirdPartySelected.CompanyID);
            }
            txtThirdPartyAgentName.Text = thirdPartySelected.AgentName;
            txtThirdPartyAgentPhone.Text = thirdPartySelected.AgentPhone;
            txtThirdPartyAgentPhone2.Text = thirdPartySelected.AgentSecondPhone;
            txtThirdPartyAgentEmail.Text = thirdPartySelected.AgentEmail;
            string[] aptName = thirdPartySelected.DriverName.Split(null);
            if (aptName.Length > 0)
            {
                txtThirdPartyDriverFirstName.Text = aptName[0];
            }
            if (aptName.Length > 1)
            {
                txtThirdPartyDriverLastName.Text = aptName[1];
            }
            txtThirdPartyDriverId.Text = thirdPartySelected.DriverIdNumber;
            txtThirdPartyDriverAddress.Text = thirdPartySelected.DriverAddress;
            txtThirdPartyDriverPhone.Text = thirdPartySelected.DriverPhone;
            txtThirdPartyDriverCellPhone.Text = thirdPartySelected.DriverCellPhone;
            if (thirdPartySelected.IsHurtPeople == true)
            {
                chbIsHurtPeople.IsChecked = true;
            }
            txtThirdPartyDamageDescription.Text = thirdPartySelected.DamageDescription;
            isThirdPartyUpdate = true;
        }

        private void btnThirdParty2_Click(object sender, RoutedEventArgs e)
        {
            thirdPartySelectedIndex = 1;
            DisplayThirdPartyDetails(1);
        }

        private void btnThirdParty3_Click(object sender, RoutedEventArgs e)
        {
            thirdPartySelectedIndex = 2;
            DisplayThirdPartyDetails(2);
        }

        private void btnThirdParty4_Click(object sender, RoutedEventArgs e)
        {
            thirdPartySelectedIndex = 3;
            DisplayThirdPartyDetails(3);
        }

        private void btnThirdParty5_Click(object sender, RoutedEventArgs e)
        {
            thirdPartySelectedIndex = 4;
            DisplayThirdPartyDetails(4);
        }

        private void btnClear_Click(object sender, RoutedEventArgs e)
        {
            ClearThirdPartyDetails();
        }

        private void btnDeleteThirdParty_Click(object sender, RoutedEventArgs e)
        {
            carThirdParties.Remove(carThirdParties[thirdPartySelectedIndex]);
            if (btnThirdParty5.IsEnabled == true)
            {
                btnThirdParty5.IsEnabled = false;
            }
            else if (btnThirdParty4.IsEnabled == true)
            {
                btnThirdParty4.IsEnabled = false;
            }
            else if (btnThirdParty3.IsEnabled == true)
            {
                btnThirdParty3.IsEnabled = false;
            }
            else if (btnThirdParty2.IsEnabled == true)
            {
                btnThirdParty2.IsEnabled = false;
            }
            else if (btnThirdParty1.IsEnabled == true)
            {
                btnThirdParty1.IsEnabled = false;
            }
            ClearThirdPartyDetails();
        }

        private void btnUpdateThirdParty_Click(object sender, RoutedEventArgs e)
        {
            int? vehicleTypeId = null;
            if (cbThirdPartyCarType.SelectedItem != null)
            {
                vehicleTypeId = ((VehicleType)cbThirdPartyCarType.SelectedItem).VehicleTypeID;
            }
            int? companyId = null;
            if (cbThirdPartyInsuranceCompany.SelectedItem != null)
            {
                companyId = ((InsuranceCompany)cbThirdPartyInsuranceCompany.SelectedItem).CompanyID;
            }
            int? insuranceTypeId = null;
            if (cbThirdPartyInsuranceType.SelectedItem != null)
            {
                insuranceTypeId = ((ElementaryInsuranceType)cbThirdPartyInsuranceType.SelectedItem).ElementaryInsuranceTypeID;
            }
            carThirdParties[thirdPartySelectedIndex].RegistrationNumber = txtThirdPartyCarNumber.Text;
            carThirdParties[thirdPartySelectedIndex].VehicleTypeID = vehicleTypeId;
            carThirdParties[thirdPartySelectedIndex].Manufacturer = txtThirdPartyCarManufacturer.Text;
            carThirdParties[thirdPartySelectedIndex].CompanyID = companyId;
            carThirdParties[thirdPartySelectedIndex].ElementaryInsuranceTypeID = insuranceTypeId;
            carThirdParties[thirdPartySelectedIndex].PolicyNumber = txtThirdPartyPolicyNumber.Text;
            carThirdParties[thirdPartySelectedIndex].PolicyHolderName = txtThirdPartyPolicyHolderFirstName.Text + " " + txtThirdPartyPolicyHolderLastName.Text;
            carThirdParties[thirdPartySelectedIndex].PolicyHolderIdNumber = txtThirdPartyPolicyHolderId.Text;
            carThirdParties[thirdPartySelectedIndex].PolicyHolderAddress = txtThirdPartyPolicyHolderAddress.Text;
            carThirdParties[thirdPartySelectedIndex].PolicyHolderPhone = txtThirdPartyPolicyHolderPhone.Text;
            carThirdParties[thirdPartySelectedIndex].PolicyHolderCellPhone = txtThirdPartyPolicyHolderCellPhone.Text;
            carThirdParties[thirdPartySelectedIndex].AgentName = txtThirdPartyAgentName.Text;
            carThirdParties[thirdPartySelectedIndex].AgentPhone = txtThirdPartyAgentPhone.Text;
            carThirdParties[thirdPartySelectedIndex].AgentSecondPhone = txtThirdPartyAgentPhone2.Text;
            carThirdParties[thirdPartySelectedIndex].AgentEmail = txtThirdPartyAgentEmail.Text;
            carThirdParties[thirdPartySelectedIndex].DriverName = txtThirdPartyDriverFirstName.Text + " " + txtThirdPartyDriverLastName.Text;
            carThirdParties[thirdPartySelectedIndex].DriverIdNumber = txtThirdPartyDriverId.Text;
            carThirdParties[thirdPartySelectedIndex].DriverAddress = txtThirdPartyDriverAddress.Text;
            carThirdParties[thirdPartySelectedIndex].DriverPhone = txtThirdPartyDriverPhone.Text;
            carThirdParties[thirdPartySelectedIndex].DriverCellPhone = txtThirdPartyDriverCellPhone.Text;
            carThirdParties[thirdPartySelectedIndex].IsHurtPeople = chbIsHurtPeople.IsChecked;
            carThirdParties[thirdPartySelectedIndex].DamageDescription = txtThirdPartyDamageDescription.Text;

            ClearThirdPartyDetails();
        }

        //private void chbIsArrangementGarage_Checked(object sender, RoutedEventArgs e)
        //{
        //    spGarageDetails.IsEnabled = true;
        //}

        //private void chbIsArrangementGarage_Unchecked(object sender, RoutedEventArgs e)
        //{
        //    spGarageDetails.IsEnabled = false;
        //    txtGarageName.Clear();
        //    txtGarageAddress.Clear();
        //    txtGaragePhone.Clear();
        //    txtGarageFax.Clear();
        //    txtContactName.Clear();
        //    txtEmail.Clear();
        //}

        private void btnUpdateCarTypeTable_Click(object sender, RoutedEventArgs e)
        {
            frmUpdateTable updateVehicleTypesTable = new frmUpdateTable(new VehicleType());
            updateVehicleTypesTable.ShowDialog();
            cbVehicleTypeBinding();
        }

        private void btnUpdateInsuranceTypeTable_Click(object sender, RoutedEventArgs e)
        {
            int? selectedItemId = null;
            if (cbThirdPartyInsuranceType.SelectedItem != null)
            {
                selectedItemId = ((ElementaryInsuranceType)cbThirdPartyInsuranceType.SelectedItem).ElementaryInsuranceTypeID;
            }
            frmUpdateTable updateInsuranceTypesTable = new frmUpdateTable(new ElementaryInsuranceType());
            updateInsuranceTypesTable.ShowDialog();
            cbInsuranceTypeBinding();
            if (updateInsuranceTypesTable.ItemAdded != null)
            {
                SelectItemInCb(((ElementaryInsuranceType)updateInsuranceTypesTable.ItemAdded).ElementaryInsuranceTypeID);
            }
            else if (selectedItemId != null)
            {
                SelectItemInCb((int)selectedItemId);
            }
        }

        //private void SelectItemInCb(int id)
        //{
        //    var items = cbThirdPartyInsuranceType.Items;
        //    foreach (var item in items)
        //    {
        //        ThirdPartyInsuranceType parsedItem = (ThirdPartyInsuranceType)item;
        //        if (parsedItem.ThirdPartyInsuranceTypeID == id)
        //        {
        //            cbThirdPartyInsuranceType.SelectedItem = item;
        //            break;
        //        }
        //    }
        //}

        private void SelectItemInCb(int id)
        {
            var items = cbThirdPartyInsuranceType.Items;
            foreach (var item in items)
            {
                ElementaryInsuranceType parsedItem = (ElementaryInsuranceType)item;
                if (parsedItem.ElementaryInsuranceTypeID == id)
                {
                    cbThirdPartyInsuranceType.SelectedItem = item;
                    break;
                }
            }
        }

        private void btnUpdateInsuranceCompanyTable_Click(object sender, RoutedEventArgs e)
        {
            int? selectedItemId = null;
            if (cbThirdPartyInsuranceCompany.SelectedItem != null)
            {
                selectedItemId = ((InsuranceCompany)cbThirdPartyInsuranceCompany.SelectedItem).CompanyID;
            }
            int insuranceID = insuranceLogic.GetInsuranceID("אלמנטרי");
            InsuranceCompany company = new InsuranceCompany() { InsuranceID = insuranceID };
            frmUpdateTable updateCompaniesTable = new frmUpdateTable(company);
            updateCompaniesTable.ShowDialog();
            cbCompanyBinding();
            if (updateCompaniesTable.ItemAdded != null)
            {
                SelectCompanyItemInCb(((InsuranceCompany)updateCompaniesTable.ItemAdded).CompanyID);
            }
            else if (selectedItemId != null)
            {
                SelectCompanyItemInCb((int)selectedItemId);
            }
        }

        private void SelectCompanyItemInCb(int id)
        {
            var items = cbThirdPartyInsuranceCompany.Items;
            foreach (var item in items)
            {
                InsuranceCompany parsedItem = (InsuranceCompany)item;
                if (parsedItem.CompanyID == id)
                {
                    cbThirdPartyInsuranceCompany.SelectedItem = item;
                    break;
                }
            }
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }



        private void cbClaimType_GotFocus(object sender, RoutedEventArgs e)
        {
            isChanges = true;
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (confirmBeforeClosing && isChanges)
            {
                if (MessageBox.Show("האם ברצונך לשמור שינוים שבצעת", "אישור", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    btnSave_Click(sender, new RoutedEventArgs());
                    if (cancelClose)
                    {
                        e.Cancel = true;
                        cancelClose = false;
                    }
                }
            }
        }

        private void txtDamageByAppraiser_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (sender is TextBox)
            {
                TextBox txtBox = (TextBox)sender;
                txtBox.ClearValue(BorderBrushProperty);
                if (txtBox.Text != "")
                {
                    try
                    {
                        decimal reusult = validation.ConvertStringToDecimal(txtBox.Text);

                    }
                    catch (Exception ex)
                    {
                        txtBox.BorderBrush = System.Windows.Media.Brushes.Red;
                        System.Windows.Forms.MessageBox.Show(ex.Message);
                        txtBox.Text = "";
                    }
                }
            }
        }

        private void txtDamageByAppraiser_LostFocus(object sender, RoutedEventArgs e)
        {
            decimal totalDamages = 0;
            if (txtDamageByAppraiser.Text != "")
            {
                totalDamages = totalDamages + decimal.Parse(txtDamageByAppraiser.Text);
            }
            if (txtAppraiserFees.Text != "")
            {
                totalDamages = totalDamages + decimal.Parse(txtAppraiserFees.Text);
            }
            if (txtCarDepreciation.Text != "")
            {
                totalDamages = totalDamages + decimal.Parse(txtCarDepreciation.Text);
            }
            if (txtOtherDamages.Text != "")
            {
                totalDamages = totalDamages + decimal.Parse(txtOtherDamages.Text);
            }
            txtTotalDamages.Text = totalDamages.ToString("0.##");
        }
    }
}



