﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Microsoft.SqlServer.Management.Smo;
using System.Data.SqlClient;

using System.Data;
using System.Configuration;
using System.IO.Compression;
using System.Windows.Threading;
using CoralBusinessLogics;
using System.Data.EntityClient;
using System.Data.Objects;

namespace CoralUI
{
    /// <summary>
    /// Interaction logic for frmBackup.xaml
    /// </summary>
    public partial class frmBackup : Window
    {
        string connection = "";
        string clientsPath = "";
        string offerPath = "";
        string backupPath = "";
        CoralBusinessLogics.User user;
        BackupLogic backupLogic = new BackupLogic();
        string strAppDir = "";
        string backupSavedPath="";

        public frmBackup(CoralBusinessLogics.User user)
        {
            InitializeComponent();
            this.user = user;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            connection = GetConnectionString();

            txtDatabaseConnection.Text = connection;

            strAppDir= System.AppDomain.CurrentDomain.BaseDirectory;
            clientsPath = strAppDir + @"Coral Files\ClientsPath.txt";
            offerPath = strAppDir + @"Coral Files\OffersPath.txt";
            backupPath= strAppDir + @"Coral Files\BackupPath.txt";

            if (File.Exists(clientsPath))
            {
                txtArchPath.Text = File.ReadAllText(clientsPath);
            }
            else
            {
                MessageBox.Show("נא הגדר ראשית את מיקום תיקיית לקוחות והצעות בחלון הגדרות ראשוניות");
                Close();
                return;
            }
            if (File.Exists(offerPath))
            {
                txtOffersPath.Text = File.ReadAllText(offerPath);
            }
            else
            {
                MessageBox.Show("נא הגדר ראשית את מיקום תיקיית לקוחות והצעות בחלון הגדרות ראשוניות");
                Close();
                return;
            }
            if (File.Exists(backupPath))
            {
                backupSavedPath=File.ReadAllText(backupPath);
                txtExportPath.Text = backupSavedPath;
            }
            dgExportLogBinding();
        }

        private string GetConnectionString()
        {
            ObjectContext context = new ObjectContext(ConfigurationManager.ConnectionStrings["Coral_DB_Entities"].ToString());
            EntityConnection entityConnection = context.Connection as EntityConnection;
            if (null != entityConnection)
            {
                return entityConnection.StoreConnection.ConnectionString;
            }
            else
                return null;
        }

        private void btnExport_Click(object sender, RoutedEventArgs e)
        {
            if (txtExportPath.Text == "")
            {
                MessageBox.Show("נא הגדר ראשית את הנתיב לייצוא הקובץ");
                return;
            }
            string path = txtExportPath.Text;
            if (!Directory.Exists(path))
            {
                MessageBox.Show("הנתיב לייצוא הקובץ לא קיים");
                return;
            }
            if (backupSavedPath!=path)
            {
                SaveBackupPath();
            }
            bool isBackupSucceeded = false;
            string zipPath = "";
            DateTime now = DateTime.Now;

            loading.IsBusy = true;

            Task.Factory.StartNew(() =>
            {
                Dispatcher.Invoke(DispatcherPriority.Normal, new Action(() =>
                {
                    loading.BusyContent = "מייצא קובץ נתונים";
                }));
                try
                {

                    string exportFolderPath = System.IO.Path.Combine(path, @"Coral Export " + now.ToString("dd-MM-yyyy hh.mm tt"));
                    Directory.CreateDirectory(exportFolderPath);

                    BackupService service = new BackupService(@connection, exportFolderPath);
                    service.BackupDatabase("Coral_DB");

                    string clientsTarget = exportFolderPath + @"\Clients";
                    string offersTarget = exportFolderPath + @"\Offers";
                    string clientsSource = "";
                    string offersSource = "";

                    this.Dispatcher.Invoke(() =>
                    {
                        clientsSource = txtArchPath.Text;
                        offersSource = txtOffersPath.Text;
                    });

                    DirectoryCopy(clientsSource, clientsTarget, true);
                    DirectoryCopy(offersSource, offersTarget, true);

                    zipPath = exportFolderPath + ".zip";
                    ZipFile.CreateFromDirectory(exportFolderPath, zipPath);
                    isBackupSucceeded = true;

                    Directory.Delete(exportFolderPath, true);


                    //this.Dispatcher.Invoke(() =>
                    //{
                    //    MessageBox.Show("קובץ ייצוא נתונים נוצר בהצלחה");
                    //});
                }
                catch (SqlException)
                {
                    MessageBox.Show("Please set: Control panel->administrative tools->services->SQL Server->Properties->log on as: local system account, allow service to interact with desktop");
                }
                catch (EntitySqlException)
                {
                    MessageBox.Show("Please set: Control panel->administrative tools->services->SQL Server->Properties->log on as: local system account, allow service to interact with desktop");
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                finally
                {
                    backupLogic.CreateBackupLog(now, zipPath, isBackupSucceeded, user.UserID);
                    this.Dispatcher.Invoke(() =>
                    {
                        dgExportLogBinding();
                        if (isBackupSucceeded)
                        {
                            MessageBox.Show("קובץ ייצוא נתונים נוצר בהצלחה");
                        }
                        else
                        {
                            MessageBox.Show("יצירת קובץ ייצוא נתונים נכשל");
                        }
                    });
                }
            }
            ).ContinueWith((task) =>
            {
                loading.IsBusy = false;
                //this.Close();
            }, TaskScheduler.FromCurrentSynchronizationContext()
            );

        }

        private void SaveBackupPath()
        {
            try
            {
                //string strAppDir = System.AppDomain.CurrentDomain.BaseDirectory;
                string coralFilePath = strAppDir + @"Coral Files";
                if (!Directory.Exists(coralFilePath))
                {
                    Directory.CreateDirectory(coralFilePath);
                }
                string backupPath = coralFilePath + @"\BackupPath.txt";
                if (File.Exists(backupPath))
                {
                    File.Delete(backupPath);
                }
                File.WriteAllText(backupPath, txtExportPath.Text);              
                
            }
            catch (Exception)
            {
                MessageBox.Show("לא ניתן לפתוח את התיקייה המבוקשת");
            }

        }

        private void dgExportLogBinding()
        {
            dgExportsLog.ItemsSource = backupLogic.GetAllBackupLogs();
        }


        private static void DirectoryCopy(string sourceDirName, string destDirName, bool copySubDirs)
        {
            // Get the subdirectories for the specified directory.
            DirectoryInfo dir = new DirectoryInfo(sourceDirName);

            if (!dir.Exists)
            {
                throw new DirectoryNotFoundException(
                    "תקיית המקור לא נמצא במערכת: "
                    + sourceDirName);
            }

            DirectoryInfo[] dirs = dir.GetDirectories();
            // If the destination directory doesn't exist, create it.
            if (!Directory.Exists(destDirName))
            {
                Directory.CreateDirectory(destDirName);
            }

            // Get the files in the directory and copy them to the new location.
            FileInfo[] files = dir.GetFiles();
            foreach (FileInfo file in files)
            {
                string temppath = System.IO.Path.Combine(destDirName, file.Name);
                file.CopyTo(temppath, false);
            }

            // If copying subdirectories, copy them and their contents to new location.
            if (copySubDirs)
            {
                foreach (DirectoryInfo subdir in dirs)
                {
                    string temppath = System.IO.Path.Combine(destDirName, subdir.Name);
                    DirectoryCopy(subdir.FullName, temppath, copySubDirs);
                }
            }
        }

        private void btnBrowse_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Forms.FolderBrowserDialog dlg = new System.Windows.Forms.FolderBrowserDialog();
            System.Windows.Forms.DialogResult result = dlg.ShowDialog();
            txtExportPath.Text = dlg.SelectedPath;
        }
    }
    public class BackupService
    {
        private readonly string _connectionString;
        private readonly string _backupFolderFullPath;
        private readonly string[] _systemDatabaseNames = { "master", "tempdb", "model", "msdb" };

        public BackupService(string connectionString, string backupFolderFullPath)
        {
            _connectionString = connectionString;
            _backupFolderFullPath = backupFolderFullPath;
        }

        public void BackupAllUserDatabases()
        {
            foreach (string databaseName in GetAllUserDatabases())
            {
                BackupDatabase(databaseName);
            }
        }

        public void BackupDatabase(string databaseName)
        {
            string filePath = BuildBackupPathWithFilename(databaseName);

            using (var connection = new SqlConnection(_connectionString))
            {
                var query = String.Format("BACKUP DATABASE [{0}] TO DISK='{1}'", databaseName, filePath);

                using (var command = new SqlCommand(query, connection))
                {
                    connection.Open();
                    command.ExecuteNonQuery();
                }
            }
        }

        private IEnumerable<string> GetAllUserDatabases()
        {
            var databases = new List<String>();

            DataTable databasesTable;

            using (var connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                databasesTable = connection.GetSchema("Databases");

                connection.Close();
            }

            foreach (DataRow row in databasesTable.Rows)
            {
                string databaseName = row["database_name"].ToString();

                if (_systemDatabaseNames.Contains(databaseName))
                    continue;

                databases.Add(databaseName);
            }

            return databases;
        }

        private string BuildBackupPathWithFilename(string databaseName)
        {
            string filename = string.Format("{0}-{1}.bak", databaseName, DateTime.Now.ToString("yyyy-MM-dd"));

            return System.IO.Path.Combine(_backupFolderFullPath, filename);
        }
    }
}
