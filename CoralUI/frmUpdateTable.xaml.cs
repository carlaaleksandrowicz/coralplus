﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using CoralBusinessLogics;

namespace CoralUI
{
    /// <summary>
    /// Interaction logic for frmUpdateTable.xaml
    /// </summary>
    public partial class frmUpdateTable : Window
    {

        ContactCategoriesLogic categoriesLogic;
        ClientsLogic clientCategoryLogic;
        InsurancesLogic insuranceLogic;
        IndustriesLogic industryLogic;
        StructureTypesLogic structureTypeLogic;
        AppraisersLogic appraiserLogic;
        VehicleTypesLogic vehicleLogic;
        AllowedToDriveLogic allowedToDriveLogic;
        CoveragesLogic coverageLogic;
        DocumentNamesLogic docNameLogic;
        RenewalsLogic renewalsLogic;
        ClaimsLogic claimLogic;
        LifeClaimsLogic lifeClaimLogic;
        WorkTasksLogic workTaskLogic;
        LifeIndustryLogic lifeIndustryLogic;
        InvestmentPlansLogic investmentPlanLogics;
        FundsLogic fundsLogics;
        InsuranceWaiversLogic insuranceWaiverLogics;
        RetirementPlansLogic retirementPlanLogic;
        object tableToDisplay;
        InsuranceCompany company;
        ElementaryInsuranceType elementaryInsuranceType;
        LifeInsuranceType lifeInsuranceType;
        ElementaryCoverageType elementaryCoverageType;
        ElementaryClaimType elementaryClaimType;
        ElementaryClaimCondition elementaryClaimCondition;
        RequestCategory requestCategory;
        RequestCondition requestCondition;
        LifeCoverageType lifeCoverageType;
        HealthIndustriesLogic healthInsuranceLogic;
        HealthInsuranceType healthInsuranceType;
        HealthClaimsLogic healthClaimLogic;
        ListViewSettings lv = new ListViewSettings();
        FinanceIndustryLogic financeIndustryLogic;
        CountriesLogic countriesLogic;
        TravelClaimsLogic travelClaimsLogic;
        private PersonalAccidentsClaimsLogic personalAccidentsClaimLogic;
        private ForeingWorkersIndutryLogic foreingWorkersIndustryLogic;
        private ForeingWorkersClaimsLogic foreingWorkersClaimLogic;
        StandardMoneyCollectionLogic moneyCollectionLogic;
        private bool isChanges=false;
        private bool cancelClose=false;

        public object ItemAdded { get; set; }
        public frmUpdateTable(object table)
        {
            InitializeComponent();
            tableToDisplay = table;
            cbStatus.SelectedIndex = 0;
            txtName.Focus();
        }

        //טעינת החלון
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (tableToDisplay is ContactCategory)
            {
                categoriesLogic = new ContactCategoriesLogic();
                lblTableUpdate.Content = "טבלת קטגוריות אנשי קשר";
                DataGridContactCategoriesBinding();
            }
            else if (tableToDisplay is MoneyCollectionType)
            {
                moneyCollectionLogic = new StandardMoneyCollectionLogic();
                lblTableUpdate.Content = "טבלת סוגי גבייה";
                DataGridMoneyCollectionTypesBinding();
            }
            else if (tableToDisplay is DangerousHobby)
            {
                clientCategoryLogic = new ClientsLogic();
                lblTableUpdate.Content = "טבלת תחביבים מסוכנים";
                DataGridHobbiesBinding();
            }
            else if (tableToDisplay is Country)
            {
                countriesLogic = new CountriesLogic();
                lblTableUpdate.Content = "טבלת מדינות יעד";
                DataGridCountriesBinding();
            }
            else if (tableToDisplay is ThirdPartyInsuranceType)
            {
                industryLogic = new IndustriesLogic();
                lblTableUpdate.Content = "טבלת סוגי ביטוח";
                DataGridThirdPartyInsuranceTypesBinding();
            }
            else if (tableToDisplay is Category)
            {
                clientCategoryLogic = new ClientsLogic();
                lblTableUpdate.Content = "טבלת קטגוריות";
                DataGridClientCategoriesBinding();
            }
            else if (tableToDisplay is InsuranceCompany)
            {
                insuranceLogic = new InsurancesLogic();
                lblTableUpdate.Content = "טבלת חברות ביטוח";
                DataGridInsuranceCompaniesBinding();
            }
            else if (tableToDisplay is ElementaryInsuranceType)
            {
                industryLogic = new IndustriesLogic();
                lblTableUpdate.Content = "טבלת סוגי ביטוח";
                DataGridElementaryInsuranceTypeBinding();
            }
            else if (tableToDisplay is ForeingWorkersInsuranceType)
            {
                foreingWorkersIndustryLogic = new ForeingWorkersIndutryLogic();
                lblTableUpdate.Content = "טבלת סוגי ביטוח";
                DataGridForeingWorkersInsuranceTypeBinding();
            }
            else if (tableToDisplay is FinanceProgram)
            {
                financeIndustryLogic = new FinanceIndustryLogic();
                lblTableUpdate.Content = "טבלת תכניות פיננסים";
                DataGridFinanceProgramsBinding();
            }
            else if (tableToDisplay is StructureType)
            {
                structureTypeLogic = new StructureTypesLogic();
                lblTableUpdate.Content = "טבלת סוגי מבנה";
                DataGridStructureTypeBinding();
            }
            else if (tableToDisplay is Appraiser)
            {
                appraiserLogic = new AppraisersLogic();
                lblTableUpdate.Content = "טבלת שמאים";
                DataGridAppraisersBinding();
            }
            else if (tableToDisplay is VehicleType)
            {
                vehicleLogic = new VehicleTypesLogic();
                lblTableUpdate.Content = "טבלת סוגי רכב";
                DataGridVehiclesBinding();
            }
            else if (tableToDisplay is AllowedToDrive)
            {
                allowedToDriveLogic = new AllowedToDriveLogic();
                lblTableUpdate.Content = "טבלת רשאים לנהוג";
                DataGridAllowedToDriveBinding();
            }
            else if (tableToDisplay is ElementaryCoverageType)
            {
                coverageLogic = new CoveragesLogic();
                lblTableUpdate.Content = "טבלת סוג כיסוים";
                DataGridElementaryCoverageTypesBinding();
            }
            else if (tableToDisplay is TravelCoverageType)
            {
                coverageLogic = new CoveragesLogic();
                lblTableUpdate.Content = "טבלת סוג כיסוים";
                DataGridTravelCoverageTypesBinding();
            }
            else if (tableToDisplay is ForeingWorkersCoverageType)
            {
                coverageLogic = new CoveragesLogic();
                lblTableUpdate.Content = "טבלת סוג כיסוים";
                DataGridForeingWorkersCoverageTypesBinding();
            }
            else if (tableToDisplay is LifeCoverageType)
            {
                coverageLogic = new CoveragesLogic();
                lblTableUpdate.Content = "טבלת סוג כיסוים";
                DataGridLifeCoverageTypesBinding();
            }
            else if (tableToDisplay is HealthCoverageType)
            {
                coverageLogic = new CoveragesLogic();
                lblTableUpdate.Content = "טבלת סוג כיסוים";
                DataGridHealthCoverageTypesBinding();
            }
            else if (tableToDisplay is PersonalAccidentsCoverageType)
            {
                coverageLogic = new CoveragesLogic();
                lblTableUpdate.Content = "טבלת סוג כיסוים";
                DataGridPersonalAccidentsCoverageTypesBinding();
            }
            else if (tableToDisplay is DocumentName)
            {
                docNameLogic = new DocumentNamesLogic();
                lblTableUpdate.Content = "טבלת שמות קובץ";
                DataGridDocNamesBinding();
            }
            else if (tableToDisplay is RenewalStatus)
            {
                renewalsLogic = new RenewalsLogic();
                lblTableUpdate.Content = "טבלת סטטוס חידושים";
                DataGridRenewalStatusBinding();
            }
            else if (tableToDisplay is ElementaryClaimType)
            {
                claimLogic = new ClaimsLogic();
                lblTableUpdate.Content = "טבלת סוגי תביעות";
                DataGridElementaryClaimTypesBinding();
            }
            else if (tableToDisplay is TravelClaimType)
            {
                travelClaimsLogic = new TravelClaimsLogic();
                lblTableUpdate.Content = "טבלת סוגי תביעות";
                DataGridTravelClaimTypesBinding();
            }
            else if (tableToDisplay is LifeClaimType)
            {
                lifeClaimLogic = new LifeClaimsLogic();
                lblTableUpdate.Content = "טבלת סוגי תביעות";
                DataGridLifeClaimTypesBinding();
            }
            else if (tableToDisplay is PersonalAccidentsClaimType)
            {
                personalAccidentsClaimLogic = new PersonalAccidentsClaimsLogic();
                lblTableUpdate.Content = "טבלת סוגי תביעות";
                DataGridPersonalAccidentsClaimTypesBinding();
            }
            else if (tableToDisplay is ForeingWorkersClaimType)
            {
                foreingWorkersClaimLogic = new ForeingWorkersClaimsLogic();
                lblTableUpdate.Content = "טבלת סוגי תביעות";
                DataGridForeingWorkersClaimTypesBinding();
            }
            else if (tableToDisplay is LifeClaimCondition)
            {
                lifeClaimLogic = new LifeClaimsLogic();
                lblTableUpdate.Content = "טבלת מצבי תביעות";
                DataGridLifeClaimConditionBinding();
            }
            else if (tableToDisplay is TravelClaimCondition)
            {
                travelClaimsLogic = new TravelClaimsLogic();
                lblTableUpdate.Content = "טבלת מצבי תביעות";
                DataGridTravelClaimConditionBinding();
            }
            else if (tableToDisplay is PersonalAccidentsClaimCondition)
            {
                personalAccidentsClaimLogic = new PersonalAccidentsClaimsLogic();
                lblTableUpdate.Content = "טבלת מצבי תביעות";
                DataGridPersonalAccidentsClaimConditionBinding();
            }
            else if (tableToDisplay is ForeingWorkersClaimCondition)
            {
                foreingWorkersClaimLogic = new ForeingWorkersClaimsLogic();
                lblTableUpdate.Content = "טבלת מצבי תביעות";
                DataGridForeingWorkersClaimConditionBinding();
            }
            else if (tableToDisplay is HealthClaimType)
            {
                healthClaimLogic = new HealthClaimsLogic();
                lblTableUpdate.Content = "טבלת סוגי תביעות";
                DataGridHealthClaimTypesBinding();
            }
            else if (tableToDisplay is HealthClaimCondition)
            {
                healthClaimLogic = new HealthClaimsLogic();
                lblTableUpdate.Content = "טבלת מצבי תביעות";
                DataGridHealthClaimConditionBinding();
            }
            else if (tableToDisplay is ElementaryClaimCondition)
            {
                claimLogic = new ClaimsLogic();
                lblTableUpdate.Content = "טבלת מצבי תביעה";
                DataGridElementaryClaimConditionsBinding();
            }
            else if (tableToDisplay is RequestCategory)
            {
                workTaskLogic = new WorkTasksLogic();
                lblTableUpdate.Content = "טבלת קטגוריות משימה";
                DataGridRequestCategoriesBinding();
            }
            else if (tableToDisplay is RequestCondition)
            {
                workTaskLogic = new WorkTasksLogic();
                lblTableUpdate.Content = "טבלת מצבי משימה";
                DataGridRequestConditionsBinding();
            }
            else if (tableToDisplay is LifeInsuranceType )
            {
                lifeIndustryLogic = new LifeIndustryLogic();
                lblTableUpdate.Content = "טבלת סוגי ביטוח";
                DataGridLifeInsuranceBinding();
            }
            else if (tableToDisplay is InvesmentPlan)
            {
                investmentPlanLogics = new InvestmentPlansLogic();
                lblTableUpdate.Content = "טבלת מסלולי השקעה";
                DataGridInvestmentPlanBinding();
            }
            else if (tableToDisplay is FundJoiningPlan)
            {
                fundsLogics = new FundsLogic();
                lblTableUpdate.Content = "טבלת מסלולי הצטרפות";
                DataGridJoiningPlansBinding();
            }
            else if (tableToDisplay is InsurancesWaiver)
            {
                insuranceWaiverLogics = new InsuranceWaiversLogic();
                lblTableUpdate.Content = "טבלת ויתורי ביטוח";
                DataGridInsuranceWaiversBinding();
            }
            else if (tableToDisplay is RetirementPlan)
            {
                retirementPlanLogic = new RetirementPlansLogic();
                lblTableUpdate.Content = "טבלת מסלולי פרישה";
                DataGridRetirementPlansBinding();
            }
            else if (tableToDisplay is FundType)
            {
                fundsLogics = new FundsLogic();
                lblTableUpdate.Content = "טבלת סוגי קופה";
                DataGridFundTypesBinding();
            }
            else if (tableToDisplay is FinanceFundType)
            {
                fundsLogics = new FundsLogic();
                lblTableUpdate.Content = "טבלת סוגי קופה";
                DataGridFinanceFundTypesBinding();
            }
            else if (tableToDisplay is HealthInsuranceType)
            {
                healthInsuranceLogic = new HealthIndustriesLogic();
                lblTableUpdate.Content = "טבלת סוגי ביטוח בריאות";
                DataGridHealthInsuranceTypesBinding();
            }
        }

        private void DataGridMoneyCollectionTypesBinding()
        {
            dgTableContent.ItemsSource = moneyCollectionLogic.GetAllMoneyCollectionTypes();
            dgColName.DisplayMemberBinding = new Binding("MoneyCollectionTypeName");
        }

        private void DataGridForeingWorkersClaimConditionBinding()
        {
            dgTableContent.ItemsSource = foreingWorkersClaimLogic.GetAllForeingWorkersClaimConditions();
            dgColName.DisplayMemberBinding = new Binding("Description");
        }

        private void DataGridForeingWorkersClaimTypesBinding()
        {
            dgTableContent.ItemsSource = foreingWorkersClaimLogic.GetAllForeingWorkersClaimTypes();
            dgColName.DisplayMemberBinding = new Binding("ClaimTypeName");
        }

        private void DataGridForeingWorkersCoverageTypesBinding()
        {
            dgTableContent.ItemsSource = coverageLogic.GetAllForeingWorkersCoverageTypes();
            dgColName.DisplayMemberBinding = new Binding("ForeingWorkersCoverageTypeName");
        }

        private void DataGridForeingWorkersInsuranceTypeBinding()
        {
            dgTableContent.ItemsSource = foreingWorkersIndustryLogic.GetForeingWorkersInsuranceTypesByIndustryAndCompany(((ForeingWorkersInsuranceType)tableToDisplay).ForeingWorkersIndustryID, ((ForeingWorkersInsuranceType)tableToDisplay).CompanyID);
            dgColName.DisplayMemberBinding = new Binding("ForeingWorkersInsuranceTypeName");

        }

        private void DataGridTravelClaimConditionBinding()
        {
            dgTableContent.ItemsSource = travelClaimsLogic.GetAllTravelClaimConditions();
            dgColName.DisplayMemberBinding = new Binding("Description");
        }

        private void DataGridTravelClaimTypesBinding()
        {
            dgTableContent.ItemsSource = travelClaimsLogic.GetAllTravelClaimTypes();
            dgColName.DisplayMemberBinding = new Binding("ClaimTypeName");
        }

        private void DataGridTravelCoverageTypesBinding()
        {
            dgTableContent.ItemsSource = coverageLogic.GetAllTravelCoverageTypes();
            dgColName.DisplayMemberBinding = new Binding("TravelCoverageTypeName");
        }

        private void DataGridCountriesBinding()
        {
            dgTableContent.ItemsSource = countriesLogic.GetAllCountries();
            dgColName.DisplayMemberBinding = new Binding("CountryName");
        }

        private void DataGridPersonalAccidentsClaimConditionBinding()
        {
            dgTableContent.ItemsSource = personalAccidentsClaimLogic.GetAllPersonalAccidentsClaimConditions();
            dgColName.DisplayMemberBinding = new Binding("Description");
        }

        private void DataGridPersonalAccidentsClaimTypesBinding()
        {
            dgTableContent.ItemsSource = personalAccidentsClaimLogic.GetAllPersonalAccidentsClaimTypes();
            dgColName.DisplayMemberBinding = new Binding("ClaimTypeName");
        }

        private void DataGridPersonalAccidentsCoverageTypesBinding()
        {
            dgTableContent.ItemsSource = coverageLogic.GetAllPersonalAccidentsCoverageTypes();
            dgColName.DisplayMemberBinding = new Binding("PersonalAccidentsCoverageTypeName");

        }

        private void DataGridFinanceFundTypesBinding()
        {
            dgTableContent.ItemsSource = fundsLogics.GetAllFinanceFundTypesByProgramTypeAndCompany(((FinanceFundType)tableToDisplay).ProgramTypeID, ((FinanceFundType)tableToDisplay).CompanyID);
            dgColName.DisplayMemberBinding = new Binding("FundTypeName");
        }

        private void DataGridFinanceProgramsBinding()
        {
            dgTableContent.ItemsSource = financeIndustryLogic.GetFinanceProgramsByProgramTypeAndCompany(((FinanceProgram)tableToDisplay).ProgramTypeID, ((FinanceProgram)tableToDisplay).CompanyID);
            dgColName.DisplayMemberBinding = new Binding("ProgramName");
        }

        private void DataGridThirdPartyInsuranceTypesBinding()
        {
            dgTableContent.ItemsSource = industryLogic.GetAllThirdPartyInsuranceTypes();
            dgColName.DisplayMemberBinding = new Binding("ThirdPartyInsuranceTypeName");
        }

        private void DataGridHobbiesBinding()
        {
            dgTableContent.ItemsSource = clientCategoryLogic.GetHobbies();
            dgColName.DisplayMemberBinding = new Binding("HobbyName");
        }

        private void DataGridHealthClaimConditionBinding()
        {
            dgTableContent.ItemsSource = healthClaimLogic.GetAllHealthClaimConditions();
            dgColName.DisplayMemberBinding = new Binding("Description");
            //dgColStatus.DisplayMemberBinding = new Binding("Status");
        }

        private void DataGridHealthClaimTypesBinding()
        {
            dgTableContent.ItemsSource = healthClaimLogic.GetAllHealthClaimTypes();
            dgColName.DisplayMemberBinding = new Binding("ClaimTypeName");
            //dgColStatus.DisplayMemberBinding = new Binding("Status");
        }

        private void DataGridHealthCoverageTypesBinding()
        {
            dgTableContent.ItemsSource = coverageLogic.GetAllHealthCoverageTypes();
            dgColName.DisplayMemberBinding = new Binding("HealthCoverageTypeName");
            //dgColStatus.DisplayMemberBinding = new Binding("Status");
        }

        private void DataGridHealthInsuranceTypesBinding()
        {
            healthInsuranceType=(HealthInsuranceType)tableToDisplay;
            dgTableContent.ItemsSource = healthInsuranceLogic.GetHealthInsuranceTypesByCompany(healthInsuranceType.CompanyID);
            dgColName.DisplayMemberBinding = new Binding("HealthInsuranceTypeName");
            //dgColStatus.DisplayMemberBinding = new Binding("Status");
        }

        private void DataGridLifeClaimConditionBinding()
        {
            dgTableContent.ItemsSource = lifeClaimLogic.GetAllLifeClaimConditions();
            dgColName.DisplayMemberBinding = new Binding("Description");
            //.DisplayMemberBinding = new Binding("Status");
        }

        private void DataGridLifeClaimTypesBinding()
        {
            dgTableContent.ItemsSource = lifeClaimLogic.GetAllLifeClaimTypes();
            dgColName.DisplayMemberBinding = new Binding("ClaimTypeName");
            //dgColStatus.DisplayMemberBinding = new Binding("Status");
        }

        private void DataGridLifeCoverageTypesBinding()
        {
            lifeCoverageType = (LifeCoverageType)tableToDisplay;
            dgTableContent.ItemsSource = coverageLogic.GetAllLifeCoverageTypesByIndustry(lifeCoverageType.LifeIndustryID);
            dgColName.DisplayMemberBinding = new Binding("LifeCoverageTypeName");
            //dgColStatus.DisplayMemberBinding = new Binding("Status");
        }

        private void DataGridFundTypesBinding()
        {
            dgTableContent.ItemsSource = fundsLogics.GetAllFundTypesByInsuranceAndCompany(((FundType)tableToDisplay).InsuranceID, (int)((FundType)tableToDisplay).LifeIndustryID, ((FundType)tableToDisplay).CompanyID);
            dgColName.DisplayMemberBinding = new Binding("FundTypeName");
           // dgColStatus.DisplayMemberBinding = new Binding("Status");
        }

        private void DataGridRetirementPlansBinding()
        {
            dgTableContent.ItemsSource = retirementPlanLogic.GetAllRetirementPlans();
            dgColName.DisplayMemberBinding = new Binding("RetirementPlanName");
            //dgColStatus.DisplayMemberBinding = new Binding("Status");
        }

        private void DataGridInsuranceWaiversBinding()
        {
            dgTableContent.ItemsSource = insuranceWaiverLogics.GetAllInsuranceWaivers();
            dgColName.DisplayMemberBinding = new Binding("InsurancesWaiverName");
            //dgColStatus.DisplayMemberBinding = new Binding("Status");
        }

        private void DataGridJoiningPlansBinding()
        {
            dgTableContent.ItemsSource = fundsLogics.GetAllJoiningPlans();
            dgColName.DisplayMemberBinding = new Binding("FundJoiningPlanName");
            //dgColStatus.DisplayMemberBinding = new Binding("Status");
        }

        private void DataGridInvestmentPlanBinding()
        {
            dgTableContent.ItemsSource = investmentPlanLogics.GetAllInvestmentPlans();
            dgColName.DisplayMemberBinding = new Binding("InvesmentPlanName");
            //dgColStatus.DisplayMemberBinding = new Binding("Status");
        }

        private void DataGridLifeInsuranceBinding()
        {
            lifeInsuranceType = (LifeInsuranceType)tableToDisplay;
            dgTableContent.ItemsSource = lifeIndustryLogic.GetLifeInsuranceTypesByIndustry(lifeInsuranceType.LifeIndustryID);
            dgColName.DisplayMemberBinding = new Binding("LifeInsuranceTypeName");
            //dgColStatus.DisplayMemberBinding = new Binding("Status");
        }

        private void DataGridRequestConditionsBinding()
        {
            requestCondition = (RequestCondition)tableToDisplay;
            dgTableContent.ItemsSource = workTaskLogic.GetAllRequestConditionsByCategory(requestCondition.RequestCategoryID);
            dgColName.DisplayMemberBinding = new Binding("RequestConditionName");
            //dgColStatus.DisplayMemberBinding = new Binding("Status");
        }

        private void DataGridRequestCategoriesBinding()
        {
            requestCategory = (RequestCategory)tableToDisplay;
            dgTableContent.ItemsSource = workTaskLogic.GetRequestCategoriesByInsuranceId(requestCategory.InsuranceID);
            dgColName.DisplayMemberBinding = new Binding("RequestCategoryName");
            //dgColStatus.DisplayMemberBinding = new Binding("Status");
        }

        private void DataGridElementaryClaimConditionsBinding()
        {
            elementaryClaimCondition = (ElementaryClaimCondition)tableToDisplay;
            dgTableContent.ItemsSource = claimLogic.GetAllElementaryClaimConditionsByIndustrie(elementaryClaimCondition.InsuranceIndustryID);
            dgColName.DisplayMemberBinding = new Binding("Description");
            //dgColStatus.DisplayMemberBinding = new Binding("Status");
        }

        private void DataGridElementaryClaimTypesBinding()
        {
            elementaryClaimType = (ElementaryClaimType)tableToDisplay;
            dgTableContent.ItemsSource = claimLogic.GetAllElementaryClaimTypesByIndustry(elementaryClaimType.InsuranceIndustryID);
            dgColName.DisplayMemberBinding = new Binding("ClaimTypeName");
            //dgColStatus.DisplayMemberBinding = new Binding("Status");
        }

        private void DataGridRenewalStatusBinding()
        {
            dgTableContent.ItemsSource = renewalsLogic.GetAllRenewalStatuses();
            dgColName.DisplayMemberBinding = new Binding("RenewalStatusName");
            //dgColStatus.DisplayMemberBinding = new Binding("Status");
        }

        private void DataGridDocNamesBinding()
        {
            dgTableContent.ItemsSource = docNameLogic.GetAllDocumentNames();
            dgColName.DisplayMemberBinding = new Binding("DocName");
            //dgColStatus.DisplayMemberBinding = new Binding("Status");
        }

        private void DataGridElementaryCoverageTypesBinding()
        {
            elementaryCoverageType = (ElementaryCoverageType)tableToDisplay;
            dgTableContent.ItemsSource = coverageLogic.GetAllElementaryCoverageTypesByIndustry(elementaryCoverageType.InsuranceIndustryID);
            dgColName.DisplayMemberBinding = new Binding("ElementaryCoverageTypeName");
            //dgColStatus.DisplayMemberBinding = new Binding("Status");
        }

        private void DataGridAllowedToDriveBinding()
        {
            dgTableContent.ItemsSource = allowedToDriveLogic.GetAllAllowedToDrive();
            dgColName.DisplayMemberBinding = new Binding("Description");
            //dgColStatus.DisplayMemberBinding = new Binding("Status");
        }

        private void DataGridVehiclesBinding()
        {
            dgTableContent.ItemsSource = vehicleLogic.GetAllVehicleTypes();
            dgColName.DisplayMemberBinding = new Binding("Description");
            //dgColStatus.DisplayMemberBinding = new Binding("Status");
        }

        private void DataGridAppraisersBinding()
        {
            dgTableContent.ItemsSource = appraiserLogic.GetAllAppraisers();
            dgColName.DisplayMemberBinding = new Binding("AppraiserName");
            //dgColStatus.DisplayMemberBinding = new Binding("Status");
        }

        private void DataGridStructureTypeBinding()
        {
            dgTableContent.ItemsSource = structureTypeLogic.GetAllStructures();
            dgColName.DisplayMemberBinding = new Binding("StructureTypeName");
           // dgColStatus.DisplayMemberBinding = new Binding("Status");
        }

        private void DataGridElementaryInsuranceTypeBinding()
        {
            elementaryInsuranceType = (ElementaryInsuranceType)tableToDisplay;
            dgTableContent.ItemsSource = industryLogic.GetAllInsuranceTypesByIndustry(elementaryInsuranceType.InsuranceIndustryID);
            dgColName.DisplayMemberBinding = new Binding("ElementaryInsuranceTypeName");
            //dgColStatus.DisplayMemberBinding = new Binding("Status");
        }

        private void DataGridInsuranceCompaniesBinding()
        {
            company = (InsuranceCompany)tableToDisplay;
            dgTableContent.ItemsSource = insuranceLogic.GetCompaniesByInsurance(company.InsuranceID);
            dgColName.DisplayMemberBinding = new Binding("Company.CompanyName");
            //dgColStatus.DisplayMemberBinding = new Binding("Status");
        }

        private void DataGridContactCategoriesBinding()
        {
            dgTableContent.ItemsSource = categoriesLogic.GetAllContactCategories();
            dgColName.DisplayMemberBinding = new Binding("ContactCategoryName");
            //dgColStatus.DisplayMemberBinding = new Binding("Status");
        }

        private void DataGridClientCategoriesBinding()
        {
            dgTableContent.ItemsSource = clientCategoryLogic.GetAllCategories();
            dgColName.DisplayMemberBinding = new Binding("CategoryName");
           // dgColStatus.DisplayMemberBinding = new Binding("Status");
        }

        private void btnAddUpdate_Click(object sender, RoutedEventArgs e)
        {
            txtName.ClearValue(BorderBrushProperty);
            if (txtName.Text == "")
            {
                txtName.BorderBrush = Brushes.Red;
                cancelClose = true;
                return;
            }

            bool IsActive = false;
            if (cbStatus.SelectedIndex == 0)
            {
                IsActive = true;
            }

            if (tableToDisplay is ContactCategory)
            {
                if (txtBtnAddCategory.Text != "עדכן רשומה בטבלה")
                {
                    try
                    {
                        ItemAdded=categoriesLogic.InsertCategory(txtName.Text, IsActive);
                        DataGridContactCategoriesBinding();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    }
                }
                else
                {
                    try
                    {
                        ContactCategory updatedCategory = categoriesLogic.UpdateCategoryStatus(txtName.Text, IsActive);
                        if (updatedCategory.Status==true)
                        {
                            ItemAdded = updatedCategory;
                        }
                        DataGridContactCategoriesBinding();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    }
                }
            }
            else if (tableToDisplay is Category)
            {
                if (txtBtnAddCategory.Text != "עדכן רשומה בטבלה")
                {
                    try
                    {
                        ItemAdded = clientCategoryLogic.InsertCategory(txtName.Text, IsActive);
                        DataGridClientCategoriesBinding();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    }
                }
                else
                {
                    try
                    {
                        Category updatedCategory=clientCategoryLogic.UpdateCategoryStatus(txtName.Text, IsActive);
                        if (updatedCategory.Status == true)
                        {
                            ItemAdded = updatedCategory;
                        }
                        DataGridClientCategoriesBinding();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    }
                }
            }
            else if (tableToDisplay is MoneyCollectionType)
            {
                if (txtBtnAddCategory.Text != "עדכן רשומה בטבלה")
                {
                    try
                    {
                        ItemAdded = moneyCollectionLogic.InsertMoneyCollectionType(txtName.Text, IsActive);
                        DataGridMoneyCollectionTypesBinding();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    }
                }
                else
                {
                    try
                    {
                        MoneyCollectionType updatedType = moneyCollectionLogic.UpdateMoneyCollectionTypeStatus(txtName.Text, IsActive);
                        if (updatedType.Status == true)
                        {
                            ItemAdded = updatedType;
                        }
                        DataGridMoneyCollectionTypesBinding();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    }
                }
            }

            else if (tableToDisplay is ThirdPartyInsuranceType)
            {
                if (txtBtnAddCategory.Text != "עדכן רשומה בטבלה")
                {
                    try
                    {
                        ItemAdded = industryLogic.InsertThirdPartyInsuranceType(txtName.Text, IsActive);
                        DataGridThirdPartyInsuranceTypesBinding();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    }
                }
                else
                {
                    try
                    {
                        ThirdPartyInsuranceType result= industryLogic.UpdateThirdPartyInsuranceTypeStatus(txtName.Text, IsActive);
                        if (result.Status == true)
                        {
                            ItemAdded = result;
                        }
                        DataGridThirdPartyInsuranceTypesBinding();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    }
                }
            }
            else if (tableToDisplay is FinanceProgram)
            {
                if (txtBtnAddCategory.Text != "עדכן רשומה בטבלה")
                {
                    try
                    {
                        ItemAdded = financeIndustryLogic.InsertFinanceProgram(txtName.Text, ((FinanceProgram)tableToDisplay).ProgramTypeID, ((FinanceProgram)tableToDisplay).CompanyID, IsActive);
                        DataGridFinanceProgramsBinding();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    }
                }
                else
                {
                    try
                    {
                        FinanceProgram result = financeIndustryLogic.UpdateFinanceProgram((FinanceProgram)dgTableContent.SelectedItem, IsActive);
                        if (result.Status == true)
                        {
                            ItemAdded = result;
                        }
                        DataGridFinanceProgramsBinding();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    }
                }
            }
            else if (tableToDisplay is ForeingWorkersInsuranceType)
            {
                if (txtBtnAddCategory.Text != "עדכן רשומה בטבלה")
                {
                    try
                    {
                        ItemAdded = foreingWorkersIndustryLogic.InsertForeingWorkersInsuranceType(txtName.Text, ((ForeingWorkersInsuranceType)tableToDisplay).ForeingWorkersIndustryID, ((ForeingWorkersInsuranceType)tableToDisplay).CompanyID, IsActive);
                        DataGridForeingWorkersInsuranceTypeBinding();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    }
                }
                else
                {
                    try
                    {
                        ForeingWorkersInsuranceType result = foreingWorkersIndustryLogic.UpdateForeingWorkersInsuranceType((ForeingWorkersInsuranceType)dgTableContent.SelectedItem, IsActive);
                        if (result.Status == true)
                        {
                            ItemAdded = result;
                        }
                        DataGridForeingWorkersInsuranceTypeBinding();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    }
                }
            }
            else if (tableToDisplay is DangerousHobby)
            {
                if (txtBtnAddCategory.Text != "עדכן רשומה בטבלה")
                {
                    try
                    {
                        ItemAdded = clientCategoryLogic.InsertHobby(txtName.Text, IsActive);
                        DataGridHobbiesBinding();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    }
                }
                else
                {
                    try
                    {
                        DangerousHobby result= clientCategoryLogic.UpdateHobbyName(txtName.Text, IsActive);
                        if (result.Status == true)
                        {
                            ItemAdded = result;
                        }
                        DataGridHobbiesBinding();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    }
                }
            }
            else if (tableToDisplay is Country)
            {
                if (txtBtnAddCategory.Text != "עדכן רשומה בטבלה")
                {
                    try
                    {
                        ItemAdded = countriesLogic.InsertCountry(txtName.Text, IsActive);
                        DataGridCountriesBinding();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    }
                }
                else
                {
                    try
                    {
                        Country result = countriesLogic.UpdateCountry(txtName.Text, IsActive);
                        if (result.Status == true)
                        {
                            ItemAdded = result;
                        }
                        DataGridCountriesBinding();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    }
                }
            }
            else if (tableToDisplay is LifeClaimType)
            {
                if (txtBtnAddCategory.Text != "עדכן רשומה בטבלה")
                {
                    try
                    {
                        ItemAdded = lifeClaimLogic.InsertLifeClaimType(txtName.Text, IsActive);
                        DataGridLifeClaimTypesBinding();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    }
                }
                else
                {
                    try
                    {
                        LifeClaimType result=lifeClaimLogic.UpdateLifeClaimTypeStatus(txtName.Text, IsActive);
                        if (result.Status == true)
                        {
                            ItemAdded = result;
                        }
                        DataGridLifeClaimTypesBinding();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    }
                }
            }
            else if (tableToDisplay is TravelClaimType)
            {
                if (txtBtnAddCategory.Text != "עדכן רשומה בטבלה")
                {
                    try
                    {
                        ItemAdded = travelClaimsLogic.InsertTravelClaimType(txtName.Text, IsActive);
                        DataGridTravelClaimTypesBinding();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    }
                }
                else
                {
                    try
                    {
                        TravelClaimType result = travelClaimsLogic.UpdateTravelClaimTypeStatus(txtName.Text, IsActive);
                        if (result.Status == true)
                        {
                            ItemAdded = result;
                        }
                        DataGridTravelClaimTypesBinding();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    }
                }
            }
            else if (tableToDisplay is PersonalAccidentsClaimType)
            {
                if (txtBtnAddCategory.Text != "עדכן רשומה בטבלה")
                {
                    try
                    {
                        ItemAdded = personalAccidentsClaimLogic.InsertPersonalAccidentsClaimType(txtName.Text, IsActive);
                        DataGridPersonalAccidentsClaimTypesBinding();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    }
                }
                else
                {
                    try
                    {
                        PersonalAccidentsClaimType result = personalAccidentsClaimLogic.UpdatePersonalAccidentsClaimTypeStatus(txtName.Text, IsActive);
                        if (result.Status == true)
                        {
                            ItemAdded = result;
                        }
                        DataGridPersonalAccidentsClaimTypesBinding();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    }
                }
            }
            else if (tableToDisplay is ForeingWorkersClaimType)
            {
                if (txtBtnAddCategory.Text != "עדכן רשומה בטבלה")
                {
                    try
                    {
                        ItemAdded = foreingWorkersClaimLogic.InsertForeingWorkersClaimType(txtName.Text, IsActive);
                        DataGridForeingWorkersClaimTypesBinding();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    }
                }
                else
                {
                    try
                    {
                        ForeingWorkersClaimType result = foreingWorkersClaimLogic.UpdateForeingWorkersClaimTypeStatus(txtName.Text, IsActive);
                        if (result.Status == true)
                        {
                            ItemAdded = result;
                        }
                        DataGridForeingWorkersClaimTypesBinding();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    }
                }
            }
            else if (tableToDisplay is HealthClaimType)
            {
                if (txtBtnAddCategory.Text != "עדכן רשומה בטבלה")
                {
                    try
                    {
                        ItemAdded = healthClaimLogic.InsertHealthClaimType(txtName.Text, IsActive);
                        DataGridHealthClaimTypesBinding();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    }
                }
                else
                {
                    try
                    {
                        HealthClaimType result=healthClaimLogic.UpdateHealthClaimTypeStatus(txtName.Text, IsActive);
                        if (result.Status == true)
                        {
                            ItemAdded = result;
                        }
                        DataGridHealthClaimTypesBinding();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    }
                }
            }
            else if (tableToDisplay is LifeClaimCondition)
            {
                if (txtBtnAddCategory.Text != "עדכן רשומה בטבלה")
                {
                    try
                    {
                        ItemAdded = lifeClaimLogic.InsertLifeClaimCondition(txtName.Text, IsActive);
                        DataGridLifeClaimConditionBinding();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    }
                }
                else
                {
                    try
                    {
                        LifeClaimCondition result=lifeClaimLogic.UpdateLifeClaimConditionStatus(txtName.Text, IsActive);
                        if (result.Status == true)
                        {
                            ItemAdded = result;
                        }
                        DataGridLifeClaimConditionBinding();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    }
                }
            }
            else if (tableToDisplay is TravelClaimCondition)
            {
                if (txtBtnAddCategory.Text != "עדכן רשומה בטבלה")
                {
                    try
                    {
                        ItemAdded = travelClaimsLogic.InsertTravelClaimCondition(txtName.Text, IsActive);
                        DataGridTravelClaimConditionBinding();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    }
                }
                else
                {
                    try
                    {
                        TravelClaimCondition result = travelClaimsLogic.UpdateTravelClaimConditionStatus(txtName.Text, IsActive);
                        if (result.Status == true)
                        {
                            ItemAdded = result;
                        }
                        DataGridTravelClaimConditionBinding();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    }
                }
            }
            else if (tableToDisplay is PersonalAccidentsClaimCondition)
            {
                if (txtBtnAddCategory.Text != "עדכן רשומה בטבלה")
                {
                    try
                    {
                        ItemAdded = personalAccidentsClaimLogic.InsertPersonalAccidentsClaimCondition(txtName.Text, IsActive);
                        DataGridPersonalAccidentsClaimConditionBinding();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    }
                }
                else
                {
                    try
                    {
                        PersonalAccidentsClaimCondition result = personalAccidentsClaimLogic.UpdatePersonalAccidentsClaimConditionStatus(txtName.Text, IsActive);
                        if (result.Status == true)
                        {
                            ItemAdded = result;
                        }
                        DataGridPersonalAccidentsClaimConditionBinding();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    }
                }
            }
            else if (tableToDisplay is ForeingWorkersClaimCondition)
            {
                if (txtBtnAddCategory.Text != "עדכן רשומה בטבלה")
                {
                    try
                    {
                        ItemAdded = foreingWorkersClaimLogic.InsertForeingWorkersClaimCondition(txtName.Text, IsActive);
                        DataGridForeingWorkersClaimConditionBinding();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    }
                }
                else
                {
                    try
                    {
                        ForeingWorkersClaimCondition result = foreingWorkersClaimLogic.UpdateForeingWorkersClaimConditionStatus(txtName.Text, IsActive);
                        if (result.Status == true)
                        {
                            ItemAdded = result;
                        }
                        DataGridForeingWorkersClaimConditionBinding();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    }
                }
            }
            else if (tableToDisplay is HealthClaimCondition)
            {
                if (txtBtnAddCategory.Text != "עדכן רשומה בטבלה")
                {
                    try
                    {
                        ItemAdded = healthClaimLogic.InsertHealthClaimCondition(txtName.Text, IsActive);
                        DataGridHealthClaimConditionBinding();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    }
                }
                else
                {
                    try
                    {
                       HealthClaimCondition result= healthClaimLogic.UpdateHealthClaimConditionStatus(txtName.Text, IsActive);
                       if (result.Status == true)
                       {
                           ItemAdded = result;
                       }
                       DataGridHealthClaimConditionBinding();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    }
                }
            }
            else if (tableToDisplay is InsuranceCompany)
            {
                if (txtBtnAddCategory.Text != "עדכן רשומה בטבלה")
                {
                    try
                    {
                        ItemAdded = insuranceLogic.InsertCompanyToInsurance(txtName.Text, company.InsuranceID, IsActive);
                        DataGridInsuranceCompaniesBinding();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    }
                }
                else
                {
                    try
                    {
                        company = (InsuranceCompany)dgTableContent.SelectedItem;
                        InsuranceCompany result=insuranceLogic.UpdateInsuranceCompanieStatus(company, IsActive);
                        if (result.Status == true)
                        {
                            ItemAdded = result;
                        }
                        DataGridInsuranceCompaniesBinding();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    }
                }
            }
            else if (tableToDisplay is ElementaryInsuranceType)
            {

                if (txtBtnAddCategory.Text != "עדכן רשומה בטבלה")
                {
                    try
                    {
                        ItemAdded= industryLogic.InsertInsuranceType(txtName.Text, elementaryInsuranceType.InsuranceIndustryID, IsActive);
                        DataGridElementaryInsuranceTypeBinding();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    }
                }
                else
                {
                    try
                    {
                        elementaryInsuranceType = (ElementaryInsuranceType)dgTableContent.SelectedItem;
                        ElementaryInsuranceType result=industryLogic.UpdateInsuranceTypeStatus(elementaryInsuranceType, IsActive);
                        if (result.Status == true)
                        {
                            ItemAdded = result;
                        }
                        DataGridElementaryInsuranceTypeBinding();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    }
                }
            }
            else if (tableToDisplay is StructureType)
            {
                if (txtBtnAddCategory.Text != "עדכן רשומה בטבלה")
                {
                    try
                    {
                        ItemAdded= structureTypeLogic.InsertStructureType(txtName.Text, IsActive);
                        DataGridStructureTypeBinding();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    }
                }
                else
                {
                    try
                    {
                        StructureType result=structureTypeLogic.UpdateStructureTypeStatus(txtName.Text, IsActive);
                        if (result.Status == true)
                        {
                            ItemAdded = result;
                        }
                        DataGridStructureTypeBinding();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    }
                }
            }
            else if (tableToDisplay is Appraiser)
            {
                if (txtBtnAddCategory.Text != "עדכן רשומה בטבלה")
                {
                    try
                    {
                        ItemAdded = appraiserLogic.InsertAppraiser(txtName.Text, IsActive);
                        DataGridAppraisersBinding();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    }
                }
                else
                {
                    try
                    {
                        Appraiser result=appraiserLogic.UpdateAppraiserStatus(txtName.Text, IsActive);
                        if (result.Status == true)
                        {
                            ItemAdded = result;
                        }
                        DataGridAppraisersBinding();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    }
                }
            }
            else if (tableToDisplay is VehicleType)
            {
                if (txtBtnAddCategory.Text != "עדכן רשומה בטבלה")
                {
                    try
                    {
                        ItemAdded= vehicleLogic.InsertVehicleType(txtName.Text, IsActive);
                        DataGridVehiclesBinding();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    }
                }
                else
                {
                    try
                    {
                        VehicleType result=vehicleLogic.UpdateVehicleTypeStatus(txtName.Text, IsActive);
                        if (result.Status == true)
                        {
                            ItemAdded = result;
                        }
                        DataGridVehiclesBinding();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    }
                }
            }
            else if (tableToDisplay is AllowedToDrive)
            {
                if (txtBtnAddCategory.Text != "עדכן רשומה בטבלה")
                {
                    try
                    {
                        ItemAdded= allowedToDriveLogic.InsertAllowedToDrive(txtName.Text, IsActive);
                        DataGridAllowedToDriveBinding();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    }
                }
                else
                {
                    try
                    {
                        AllowedToDrive result=allowedToDriveLogic.UpdateAllowedToDrive(txtName.Text, IsActive);
                        if (result.Status == true)
                        {
                            ItemAdded = result;
                        }
                        DataGridAllowedToDriveBinding();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    }
                }
            }
            else if (tableToDisplay is ElementaryCoverageType)
            {
                if (txtBtnAddCategory.Text != "עדכן רשומה בטבלה")
                {
                    try
                    {
                        ItemAdded= coverageLogic.InsertElementaryCoverageType(elementaryCoverageType.InsuranceIndustryID, null, txtName.Text, IsActive);
                        DataGridElementaryCoverageTypesBinding();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    }
                }
                else
                {
                    try
                    {
                        ElementaryCoverageType result=coverageLogic.UpdateElementaryCoverageType((ElementaryCoverageType)dgTableContent.SelectedItem, IsActive);
                        if (result.Status == true)
                        {
                            ItemAdded = result;
                        }
                        DataGridElementaryCoverageTypesBinding();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    }
                }
            }
            else if (tableToDisplay is HealthCoverageType)
            {
                if (txtBtnAddCategory.Text != "עדכן רשומה בטבלה")
                {
                    try
                    {
                        ItemAdded = coverageLogic.InsertHealthCoverageType(null, txtName.Text, IsActive);
                        DataGridHealthCoverageTypesBinding();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    }
                }
                else
                {
                    try
                    {
                        HealthCoverageType result=coverageLogic.UpdateHealthCoverageType((HealthCoverageType)dgTableContent.SelectedItem, IsActive);
                        if (result.Status == true)
                        {
                            ItemAdded = result;
                        }
                        DataGridHealthCoverageTypesBinding();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    }
                }
            }
            else if (tableToDisplay is TravelCoverageType)
            {
                if (txtBtnAddCategory.Text != "עדכן רשומה בטבלה")
                {
                    try
                    {
                        ItemAdded = coverageLogic.InsertTravelCoverageType(null, txtName.Text, IsActive);
                        DataGridTravelCoverageTypesBinding();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    }
                }
                else
                {
                    try
                    {
                        TravelCoverageType result = coverageLogic.UpdateTravelCoverageType((TravelCoverageType)dgTableContent.SelectedItem, IsActive);
                        if (result.Status == true)
                        {
                            ItemAdded = result;
                        }
                        DataGridTravelCoverageTypesBinding();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    }
                }
            }
            else if (tableToDisplay is ForeingWorkersCoverageType)
            {
                if (txtBtnAddCategory.Text != "עדכן רשומה בטבלה")
                {
                    try
                    {
                        ItemAdded = coverageLogic.InsertForeingWorkersCoverageType(null, txtName.Text, IsActive);
                        DataGridForeingWorkersCoverageTypesBinding();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    }
                }
                else
                {
                    try
                    {
                        ForeingWorkersCoverageType result = coverageLogic.UpdateForeingWorkersCoverageType((ForeingWorkersCoverageType)dgTableContent.SelectedItem, IsActive);
                        if (result.Status == true)
                        {
                            ItemAdded = result;
                        }
                        DataGridForeingWorkersCoverageTypesBinding();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    }
                }
            }
            else if (tableToDisplay is PersonalAccidentsCoverageType)
            {
                if (txtBtnAddCategory.Text != "עדכן רשומה בטבלה")
                {
                    try
                    {
                        ItemAdded = coverageLogic.InsertPersonalAccidentsCoverageType(null, txtName.Text, IsActive);
                        DataGridPersonalAccidentsCoverageTypesBinding();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    }
                }
                else
                {
                    try
                    {
                        PersonalAccidentsCoverageType result = coverageLogic.UpdatePersonalAccidentsCoverageType((PersonalAccidentsCoverageType)dgTableContent.SelectedItem, IsActive);
                        if (result.Status == true)
                        {
                            ItemAdded = result;
                        }
                        DataGridPersonalAccidentsCoverageTypesBinding();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    }
                }
            }
            else if (tableToDisplay is LifeCoverageType)
            {
                if (txtBtnAddCategory.Text != "עדכן רשומה בטבלה")
                {
                    try
                    {
                        ItemAdded=coverageLogic.InsertLifeCoverageType(lifeCoverageType.LifeIndustryID, null, txtName.Text, IsActive);
                        DataGridLifeCoverageTypesBinding();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    }
                }
                else
                {
                    try
                    {
                        LifeCoverageType result=coverageLogic.UpdateLifeCoverageType((LifeCoverageType)dgTableContent.SelectedItem, IsActive);
                        if (result.Status == true)
                        {
                            ItemAdded = result;
                        }
                        DataGridLifeCoverageTypesBinding();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    }
                }
            }
            else if (tableToDisplay is DocumentName)
            {
                if (txtBtnAddCategory.Text != "עדכן רשומה בטבלה")
                {
                    try
                    {
                        ItemAdded = docNameLogic.InsertDocumentName(txtName.Text, IsActive);
                        DataGridDocNamesBinding();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    }
                }
                else
                {
                    try
                    {
                        DocumentName result=docNameLogic.UpdateDocumentName(txtName.Text, IsActive);
                        if (result.Status == true)
                        {
                            ItemAdded = result;
                        }
                        DataGridDocNamesBinding();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    }
                }
            }
            else if (tableToDisplay is RenewalStatus)
            {
                if (txtBtnAddCategory.Text != "עדכן רשומה בטבלה")
                {
                    try
                    {
                        renewalsLogic.InsertRenewalStatus(txtName.Text, IsActive);
                        DataGridRenewalStatusBinding();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    }
                }
                else
                {
                    try
                    {
                        renewalsLogic.UpdateRenewalStatusStatus((RenewalStatus)dgTableContent.SelectedItem, IsActive);
                        DataGridRenewalStatusBinding();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    }
                }
            }
            else if (tableToDisplay is ElementaryClaimType)
            {
                if (txtBtnAddCategory.Text != "עדכן רשומה בטבלה")
                {
                    try
                    {
                        ItemAdded= claimLogic.InsertElementaryClaimType(elementaryClaimType.InsuranceIndustryID, txtName.Text, IsActive);
                        DataGridElementaryClaimTypesBinding();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    }
                }
                else
                {
                    try
                    {
                        ElementaryClaimType result=claimLogic.UpdateElementaryClaimTypeStatus((ElementaryClaimType)dgTableContent.SelectedItem, IsActive);
                        if (result.Status == true)
                        {
                            ItemAdded = result;
                        }
                        DataGridElementaryClaimTypesBinding();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    }
                }
            }
            else if (tableToDisplay is ElementaryClaimCondition)
            {
                if (txtBtnAddCategory.Text != "עדכן רשומה בטבלה")
                {
                    try
                    {
                        ItemAdded = claimLogic.InsertElementaryClaimCondition(elementaryClaimCondition.InsuranceIndustryID, txtName.Text, IsActive);
                        DataGridElementaryClaimConditionsBinding();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    }
                }
                else
                {
                    try
                    {
                        ElementaryClaimCondition result=claimLogic.UpdateElementaryClaimConditionStatus((ElementaryClaimCondition)dgTableContent.SelectedItem, IsActive);
                        if (result.Status == true)
                        {
                            ItemAdded = result;
                        }
                        DataGridElementaryClaimConditionsBinding();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    }
                }
            }
            else if (tableToDisplay is RequestCategory)
            {
                if (txtBtnAddCategory.Text != "עדכן רשומה בטבלה")
                {
                    try
                    {
                        ItemAdded = workTaskLogic.InsertRequestCategory(requestCategory.InsuranceID, txtName.Text, IsActive);
                        DataGridRequestCategoriesBinding();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    }
                }
                else
                {
                    try
                    {
                        RequestCategory result=workTaskLogic.UpdateRequestCategorystatus(((RequestCategory)dgTableContent.SelectedItem).RequestCategoryID, IsActive);
                        if (result.Status == true)
                        {
                            ItemAdded = result;
                        }
                        DataGridRequestCategoriesBinding();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    }
                }
            }
            else if (tableToDisplay is RequestCondition)
            {
                if (txtBtnAddCategory.Text != "עדכן רשומה בטבלה")
                {
                    try
                    {
                        ItemAdded = workTaskLogic.InsertRequestCondition(requestCondition.RequestCategoryID, txtName.Text, IsActive);
                        DataGridRequestConditionsBinding();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    }
                }
                else
                {
                    try
                    {
                        RequestCondition result=workTaskLogic.UpdateRequestConditionStatus(((RequestCondition)dgTableContent.SelectedItem).RequestConditionID, IsActive);
                        if (result.Status == true)
                        {
                            ItemAdded = result;
                        }
                        DataGridRequestConditionsBinding();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    }
                }
            }
            else if (tableToDisplay is LifeInsuranceType)
            {
                if (txtBtnAddCategory.Text != "עדכן רשומה בטבלה")
                {
                    try
                    {
                        lifeIndustryLogic.InsertLifeInsuranceType(txtName.Text, lifeInsuranceType.LifeIndustryID, IsActive);
                        DataGridLifeInsuranceBinding();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    }
                }
                else
                {
                    try
                    {
                        lifeInsuranceType=(LifeInsuranceType)dgTableContent.SelectedItem;
                        lifeIndustryLogic.UpdateLifeInsuranceTypeStatus(lifeInsuranceType, IsActive);
                        DataGridLifeInsuranceBinding();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    }
                }
            }
            else if (tableToDisplay is InvesmentPlan)
            {

                if (txtBtnAddCategory.Text != "עדכן רשומה בטבלה")
                {
                    try
                    {
                        ItemAdded=investmentPlanLogics.InsertInvestmentPlan(txtName.Text,IsActive);
                        DataGridInvestmentPlanBinding();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    }
                }
                else
                {
                    try
                    {
                        InvesmentPlan result=investmentPlanLogics.UpdatePlanName(((InvesmentPlan)dgTableContent.SelectedItem).InvesmentPlanName, IsActive);
                        if (result.Status == true)
                        {
                            ItemAdded = result;
                        }
                        DataGridInvestmentPlanBinding();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    }
                }
            }
            else if (tableToDisplay is FundJoiningPlan)
            {

                if (txtBtnAddCategory.Text != "עדכן רשומה בטבלה")
                {
                    try
                    {
                        ItemAdded=fundsLogics.InsertJoiningPlan(txtName.Text, IsActive);
                        DataGridJoiningPlansBinding();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    }
                }
                else
                {
                    try
                    {
                        FundJoiningPlan result=fundsLogics.UpdateJoiningPlanName(((FundJoiningPlan)dgTableContent.SelectedItem).FundJoiningPlanName, IsActive);
                        if (result.Status == true)
                        {
                            ItemAdded = result;
                        }
                        DataGridJoiningPlansBinding();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    }
                }
            }
            else if (tableToDisplay is InsurancesWaiver)
            {

                if (txtBtnAddCategory.Text != "עדכן רשומה בטבלה")
                {
                    try
                    {
                        ItemAdded = insuranceWaiverLogics.InsertInsuranceWaiver(txtName.Text, IsActive);                        
                        DataGridInsuranceWaiversBinding();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    }
                }
                else
                {
                    try
                    {
                        InsurancesWaiver result=insuranceWaiverLogics.UpdateInsuranceWaiverStatus(((InsurancesWaiver)dgTableContent.SelectedItem).InsurancesWaiverName, IsActive);
                        if (result.Status == true)
                        {
                            ItemAdded = result;
                        }
                        DataGridInsuranceWaiversBinding();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    }
                }
            }
            else if (tableToDisplay is RetirementPlan)
            {

                if (txtBtnAddCategory.Text != "עדכן רשומה בטבלה")
                {
                    try
                    {
                        ItemAdded = retirementPlanLogic.InsertRetirementPlans(txtName.Text, IsActive);
                        DataGridRetirementPlansBinding();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    }
                }
                else
                {
                    try
                    {
                        RetirementPlan result=retirementPlanLogic.UpdatePlanName(((RetirementPlan)dgTableContent.SelectedItem).RetirementPlanName, IsActive);
                        if (result.Status == true)
                        {
                            ItemAdded = result;
                        }
                        DataGridRetirementPlansBinding();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    }
                }
            }
            else if (tableToDisplay is FundType)
            {

                if (txtBtnAddCategory.Text != "עדכן רשומה בטבלה")
                {
                    try
                    {
                        ItemAdded=fundsLogics.InsertFundType(((FundType)tableToDisplay).InsuranceID, (int)((FundType)tableToDisplay).LifeIndustryID, ((FundType)tableToDisplay).CompanyID, txtName.Text, IsActive);                        
                        DataGridFundTypesBinding();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    }
                }
                else
                {
                    try
                    {
                        FundType result=fundsLogics.UpdateFundTypeStatus((FundType)dgTableContent.SelectedItem, IsActive);
                        if (result.Status == true)
                        {
                            ItemAdded = result;
                        }
                        DataGridFundTypesBinding();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    }
                }
            }
            else if (tableToDisplay is FinanceFundType)
            {

                if (txtBtnAddCategory.Text != "עדכן רשומה בטבלה")
                {
                    try
                    {
                        ItemAdded = fundsLogics.InsertFinanceFundType((int)((FinanceFundType)tableToDisplay).ProgramTypeID, ((FinanceFundType)tableToDisplay).CompanyID, txtName.Text, IsActive);
                        DataGridFinanceFundTypesBinding();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    }
                }
                else
                {
                    try
                    {
                        FinanceFundType result = fundsLogics.UpdateFinanceFundTypeStatus((FinanceFundType)dgTableContent.SelectedItem, IsActive);
                        if (result.Status == true)
                        {
                            ItemAdded = result;
                        }
                        DataGridFinanceFundTypesBinding();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    }
                }
            }
            else if (tableToDisplay is HealthInsuranceType)
            {

                if (txtBtnAddCategory.Text != "עדכן רשומה בטבלה")
                {
                    try
                    {
                        ItemAdded=healthInsuranceLogic.InsertHealthInsuranceType(txtName.Text, IsActive, healthInsuranceType.CompanyID);
                        DataGridHealthInsuranceTypesBinding();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    }
                }
                else
                {
                    try
                    {
                        HealthInsuranceType result = healthInsuranceLogic.UpdateHealthInsuranceTypeStatus((HealthInsuranceType)dgTableContent.SelectedItem, IsActive,healthInsuranceType.CompanyID);
                        if (result.Status == true)
                        {
                            ItemAdded = result;
                        }
                        DataGridHealthInsuranceTypesBinding();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    }
                }
            }
            if (dgTableContent.SelectedItem != null)
            {
                dgTableContent.UnselectAll();
            }
            else
            {
                CleanGroupBox();
            }
            isChanges = false;
        }

        //מנקה את פירטי הרשומה
        private void CleanGroupBox()
        {
            txtName.IsEnabled = true;
            txtBtnAddCategory.Text = "הוסף רשומה לטבלה";
            var addUriSource = new Uri(@"/IconsMind/Add_24px.png", UriKind.Relative);
            imgBtnAddCategory.Source = new BitmapImage(addUriSource);
            txtName.Clear();
            cbStatus.SelectedIndex = 0;
            txtName.ClearValue(BorderBrushProperty);
        }

        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void dgTableContent_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (dgTableContent.SelectedItem == null)
            //מנקה את פירטי הרשומה כאין רשומה מסומנת בטבלה
            {
                CleanGroupBox();
                return;
            }
            //ממלא את פירטי הרשומה לפי הרשומה המסומן
            txtBtnAddCategory.Text = "עדכן רשומה בטבלה";
            var updateUriSource = new Uri(@"/IconsMind/Edit_24px.png", UriKind.Relative);
            imgBtnAddCategory.Source = new BitmapImage(updateUriSource);
            if (tableToDisplay is ContactCategory)
            {
                ContactCategory categorySelected = (ContactCategory)dgTableContent.SelectedItem;
                txtName.Text = categorySelected.ContactCategoryName;
                txtName.IsEnabled = false;
                if (categorySelected.Status == true)
                {
                    cbStatus.SelectedIndex = 0;
                }
                else
                {
                    cbStatus.SelectedIndex = 1;
                }
            }
            else if (tableToDisplay is Category)
            {
                Category categorySelected = (Category)dgTableContent.SelectedItem;
                txtName.Text = categorySelected.CategoryName;
                txtName.IsEnabled = false;
                if (categorySelected.Status == true)
                {
                    cbStatus.SelectedIndex = 0;
                }
                else
                {
                    cbStatus.SelectedIndex = 1;
                }
            }
            else if (tableToDisplay is MoneyCollectionType)
            {
                MoneyCollectionType typeSelected = (MoneyCollectionType)dgTableContent.SelectedItem;
                txtName.Text = typeSelected.MoneyCollectionTypeName;
                txtName.IsEnabled = false;
                if (typeSelected.Status == true)
                {
                    cbStatus.SelectedIndex = 0;
                }
                else
                {
                    cbStatus.SelectedIndex = 1;
                }
            }
            else if (tableToDisplay is ThirdPartyInsuranceType)
            {
                ThirdPartyInsuranceType insuranceSelected = (ThirdPartyInsuranceType)dgTableContent.SelectedItem;
                txtName.Text = insuranceSelected.ThirdPartyInsuranceTypeName;
                txtName.IsEnabled = false;
                if (insuranceSelected.Status == true)
                {
                    cbStatus.SelectedIndex = 0;
                }
                else
                {
                    cbStatus.SelectedIndex = 1;
                }
            }
            else if (tableToDisplay is DangerousHobby)
            {
                DangerousHobby hobbySelected = (DangerousHobby)dgTableContent.SelectedItem;
                txtName.Text = hobbySelected.HobbyName;
                txtName.IsEnabled = false;
                if (hobbySelected.Status == true)
                {
                    cbStatus.SelectedIndex = 0;
                }
                else
                {
                    cbStatus.SelectedIndex = 1;
                }
            }
            else if (tableToDisplay is Country)
            {
                Country countrySelected = (Country)dgTableContent.SelectedItem;
                txtName.Text = countrySelected.CountryName;
                txtName.IsEnabled = false;
                if (countrySelected.Status == true)
                {
                    cbStatus.SelectedIndex = 0;
                }
                else
                {
                    cbStatus.SelectedIndex = 1;
                }
            }
            else if (tableToDisplay is LifeClaimType)
            {
                LifeClaimType claimTypeSelected = (LifeClaimType)dgTableContent.SelectedItem;
                txtName.Text = claimTypeSelected.ClaimTypeName;
                txtName.IsEnabled = false;
                if (claimTypeSelected.Status == true)
                {
                    cbStatus.SelectedIndex = 0;
                }
                else
                {
                    cbStatus.SelectedIndex = 1;
                }
            }
            else if (tableToDisplay is PersonalAccidentsClaimType)
            {
                PersonalAccidentsClaimType claimTypeSelected = (PersonalAccidentsClaimType)dgTableContent.SelectedItem;
                txtName.Text = claimTypeSelected.ClaimTypeName;
                txtName.IsEnabled = false;
                if (claimTypeSelected.Status == true)
                {
                    cbStatus.SelectedIndex = 0;
                }
                else
                {
                    cbStatus.SelectedIndex = 1;
                }
            }
            else if (tableToDisplay is ForeingWorkersClaimType)
            {
                ForeingWorkersClaimType claimTypeSelected = (ForeingWorkersClaimType)dgTableContent.SelectedItem;
                txtName.Text = claimTypeSelected.ClaimTypeName;
                txtName.IsEnabled = false;
                if (claimTypeSelected.Status == true)
                {
                    cbStatus.SelectedIndex = 0;
                }
                else
                {
                    cbStatus.SelectedIndex = 1;
                }
            }
            else if (tableToDisplay is TravelClaimType)
            {
                TravelClaimType claimTypeSelected = (TravelClaimType)dgTableContent.SelectedItem;
                txtName.Text = claimTypeSelected.ClaimTypeName;
                txtName.IsEnabled = false;
                if (claimTypeSelected.Status == true)
                {
                    cbStatus.SelectedIndex = 0;
                }
                else
                {
                    cbStatus.SelectedIndex = 1;
                }
            }
            else if (tableToDisplay is FinanceProgram)
            {
                FinanceProgram programSelected = (FinanceProgram)dgTableContent.SelectedItem;
                txtName.Text = programSelected.ProgramName;
                txtName.IsEnabled = false;
                if (programSelected.Status == true)
                {
                    cbStatus.SelectedIndex = 0;
                }
                else
                {
                    cbStatus.SelectedIndex = 1;
                }
            }
            else if (tableToDisplay is ForeingWorkersInsuranceType)
            {
                ForeingWorkersInsuranceType typeSelected = (ForeingWorkersInsuranceType)dgTableContent.SelectedItem;
                txtName.Text = typeSelected.ForeingWorkersInsuranceTypeName;
                txtName.IsEnabled = false;
                if (typeSelected.Status == true)
                {
                    cbStatus.SelectedIndex = 0;
                }
                else
                {
                    cbStatus.SelectedIndex = 1;
                }
            }
            else if (tableToDisplay is HealthClaimType)
            {
                HealthClaimType claimTypeSelected = (HealthClaimType)dgTableContent.SelectedItem;
                txtName.Text = claimTypeSelected.ClaimTypeName;
                txtName.IsEnabled = false;
                if (claimTypeSelected.Status == true)
                {
                    cbStatus.SelectedIndex = 0;
                }
                else
                {
                    cbStatus.SelectedIndex = 1;
                }
            }
            else if (tableToDisplay is LifeClaimCondition)
            {
                LifeClaimCondition claimConditionSelected = (LifeClaimCondition)dgTableContent.SelectedItem;
                txtName.Text = claimConditionSelected.Description;
                txtName.IsEnabled = false;
                if (claimConditionSelected.Status == true)
                {
                    cbStatus.SelectedIndex = 0;
                }
                else
                {
                    cbStatus.SelectedIndex = 1;
                }
            }
            else if (tableToDisplay is TravelClaimCondition)
            {
                TravelClaimCondition claimConditionSelected = (TravelClaimCondition)dgTableContent.SelectedItem;
                txtName.Text = claimConditionSelected.Description;
                txtName.IsEnabled = false;
                if (claimConditionSelected.Status == true)
                {
                    cbStatus.SelectedIndex = 0;
                }
                else
                {
                    cbStatus.SelectedIndex = 1;
                }
            }
            else if (tableToDisplay is PersonalAccidentsClaimCondition)
            {
                PersonalAccidentsClaimCondition claimConditionSelected = (PersonalAccidentsClaimCondition)dgTableContent.SelectedItem;
                txtName.Text = claimConditionSelected.Description;
                txtName.IsEnabled = false;
                if (claimConditionSelected.Status == true)
                {
                    cbStatus.SelectedIndex = 0;
                }
                else
                {
                    cbStatus.SelectedIndex = 1;
                }
            }
            else if (tableToDisplay is ForeingWorkersClaimCondition)
            {
                ForeingWorkersClaimCondition claimConditionSelected = (ForeingWorkersClaimCondition)dgTableContent.SelectedItem;
                txtName.Text = claimConditionSelected.Description;
                txtName.IsEnabled = false;
                if (claimConditionSelected.Status == true)
                {
                    cbStatus.SelectedIndex = 0;
                }
                else
                {
                    cbStatus.SelectedIndex = 1;
                }
            }
            else if (tableToDisplay is HealthClaimCondition)
            {
                HealthClaimCondition claimConditionSelected = (HealthClaimCondition)dgTableContent.SelectedItem;
                txtName.Text = claimConditionSelected.Description;
                txtName.IsEnabled = false;
                if (claimConditionSelected.Status == true)
                {
                    cbStatus.SelectedIndex = 0;
                }
                else
                {
                    cbStatus.SelectedIndex = 1;
                }
            }
            else if (tableToDisplay is InsuranceCompany)
            {
                InsuranceCompany companySelected = (InsuranceCompany)dgTableContent.SelectedItem;
                txtName.Text = companySelected.Company.CompanyName;
                txtName.IsEnabled = false;
                if (companySelected.Status == true)
                {
                    cbStatus.SelectedIndex = 0;
                }
                else
                {
                    cbStatus.SelectedIndex = 1;
                }
            }
            else if (tableToDisplay is ElementaryInsuranceType)
            {
                ElementaryInsuranceType insuranceTypeSelected = (ElementaryInsuranceType)dgTableContent.SelectedItem;
                txtName.Text = insuranceTypeSelected.ElementaryInsuranceTypeName;
                txtName.IsEnabled = false;
                if (insuranceTypeSelected.Status == true)
                {
                    cbStatus.SelectedIndex = 0;
                }
                else
                {
                    cbStatus.SelectedIndex = 1;
                }
            }
            else if (tableToDisplay is StructureType)
            {
                StructureType structureSelected = (StructureType)dgTableContent.SelectedItem;
                txtName.Text = structureSelected.StructureTypeName;
                txtName.IsEnabled = false;
                if (structureSelected.Status == true)
                {
                    cbStatus.SelectedIndex = 0;
                }
                else
                {
                    cbStatus.SelectedIndex = 1;
                }
            }
            else if (tableToDisplay is Appraiser)
            {
                Appraiser appraiserSelected = (Appraiser)dgTableContent.SelectedItem;
                txtName.Text = appraiserSelected.AppraiserName;
                txtName.IsEnabled = false;
                if (appraiserSelected.Status == true)
                {
                    cbStatus.SelectedIndex = 0;
                }
                else
                {
                    cbStatus.SelectedIndex = 1;
                }
            }
            else if (tableToDisplay is VehicleType)
            {
                VehicleType vehicleSelected = (VehicleType)dgTableContent.SelectedItem;
                txtName.Text = vehicleSelected.Description;
                txtName.IsEnabled = false;
                if (vehicleSelected.Status == true)
                {
                    cbStatus.SelectedIndex = 0;
                }
                else
                {
                    cbStatus.SelectedIndex = 1;
                }
            }
            else if (tableToDisplay is AllowedToDrive)
            {
                AllowedToDrive allowedToDriveSelected = (AllowedToDrive)dgTableContent.SelectedItem;
                txtName.Text = allowedToDriveSelected.Description;
                txtName.IsEnabled = false;
                if (allowedToDriveSelected.Status == true)
                {
                    cbStatus.SelectedIndex = 0;
                }
                else
                {
                    cbStatus.SelectedIndex = 1;
                }
            }
            else if (tableToDisplay is ElementaryCoverageType)
            {
                ElementaryCoverageType coverageSelected = (ElementaryCoverageType)dgTableContent.SelectedItem;
                txtName.Text = coverageSelected.ElementaryCoverageTypeName;
                txtName.IsEnabled = false;
                if (coverageSelected.Status == true)
                {
                    cbStatus.SelectedIndex = 0;
                }
                else
                {
                    cbStatus.SelectedIndex = 1;
                }
            }
            else if (tableToDisplay is HealthCoverageType)
            {
                HealthCoverageType coverageSelected = (HealthCoverageType)dgTableContent.SelectedItem;
                txtName.Text = coverageSelected.HealthCoverageTypeName;
                txtName.IsEnabled = false;
                if (coverageSelected.Status == true)
                {
                    cbStatus.SelectedIndex = 0;
                }
                else
                {
                    cbStatus.SelectedIndex = 1;
                }
            }
            else if (tableToDisplay is TravelCoverageType)
            {
                TravelCoverageType coverageSelected = (TravelCoverageType)dgTableContent.SelectedItem;
                txtName.Text = coverageSelected.TravelCoverageTypeName;
                txtName.IsEnabled = false;
                if (coverageSelected.Status == true)
                {
                    cbStatus.SelectedIndex = 0;
                }
                else
                {
                    cbStatus.SelectedIndex = 1;
                }
            }
            else if (tableToDisplay is ForeingWorkersCoverageType)
            {
                ForeingWorkersCoverageType coverageSelected = (ForeingWorkersCoverageType)dgTableContent.SelectedItem;
                txtName.Text = coverageSelected.ForeingWorkersCoverageTypeName;
                txtName.IsEnabled = false;
                if (coverageSelected.Status == true)
                {
                    cbStatus.SelectedIndex = 0;
                }
                else
                {
                    cbStatus.SelectedIndex = 1;
                }
            }
            else if (tableToDisplay is PersonalAccidentsCoverageType)
            {
                PersonalAccidentsCoverageType coverageSelected = (PersonalAccidentsCoverageType)dgTableContent.SelectedItem;
                txtName.Text = coverageSelected.PersonalAccidentsCoverageTypeName;
                txtName.IsEnabled = false;
                if (coverageSelected.Status == true)
                {
                    cbStatus.SelectedIndex = 0;
                }
                else
                {
                    cbStatus.SelectedIndex = 1;
                }
            }
            else if (tableToDisplay is LifeCoverageType)
            {
                LifeCoverageType coverageSelected = (LifeCoverageType)dgTableContent.SelectedItem;
                txtName.Text = coverageSelected.LifeCoverageTypeName;
                txtName.IsEnabled = false;
                if (coverageSelected.Status == true)
                {
                    cbStatus.SelectedIndex = 0;
                }
                else
                {
                    cbStatus.SelectedIndex = 1;
                }
            }
            else if (tableToDisplay is DocumentName)
            {
                DocumentName docNameSelected = (DocumentName)dgTableContent.SelectedItem;
                txtName.Text = docNameSelected.DocName;
                txtName.IsEnabled = false;
                if (docNameSelected.Status == true)
                {
                    cbStatus.SelectedIndex = 0;
                }
                else
                {
                    cbStatus.SelectedIndex = 1;
                }
            }
            else if (tableToDisplay is RenewalStatus)
            {
                RenewalStatus renewalStatusSelected = (RenewalStatus)dgTableContent.SelectedItem;
                txtName.Text = renewalStatusSelected.RenewalStatusName;
                txtName.IsEnabled = false;
                if (renewalStatusSelected.Status == true)
                {
                    cbStatus.SelectedIndex = 0;
                }
                else
                {
                    cbStatus.SelectedIndex = 1;
                }
            }
            else if (tableToDisplay is ElementaryClaimType)
            {
                ElementaryClaimType claimTypeSelected = (ElementaryClaimType)dgTableContent.SelectedItem;
                txtName.Text = claimTypeSelected.ClaimTypeName;
                txtName.IsEnabled = false;
                if (claimTypeSelected.Status == true)
                {
                    cbStatus.SelectedIndex = 0;
                }
                else
                {
                    cbStatus.SelectedIndex = 1;
                }
            }
            else if (tableToDisplay is ElementaryClaimCondition)
            {
                ElementaryClaimCondition claimConditionSelected = (ElementaryClaimCondition)dgTableContent.SelectedItem;
                txtName.Text = claimConditionSelected.Description;
                txtName.IsEnabled = false;
                if (claimConditionSelected.Status == true)
                {
                    cbStatus.SelectedIndex = 0;
                }
                else
                {
                    cbStatus.SelectedIndex = 1;
                }
            }
            else if (tableToDisplay is RequestCategory)
            {
                RequestCategory requestCategorySelected = (RequestCategory)dgTableContent.SelectedItem;
                txtName.Text = requestCategorySelected.RequestCategoryName;
                txtName.IsEnabled = false;
                if (requestCategorySelected.Status == true)
                {
                    cbStatus.SelectedIndex = 0;
                }
                else
                {
                    cbStatus.SelectedIndex = 1;
                }
            }
            else if (tableToDisplay is RequestCondition)
            {
                RequestCondition requestConditionSelected = (RequestCondition)dgTableContent.SelectedItem;
                txtName.Text = requestConditionSelected.RequestConditionName;
                txtName.IsEnabled = false;
                if (requestConditionSelected.Status == true)
                {
                    cbStatus.SelectedIndex = 0;
                }
                else
                {
                    cbStatus.SelectedIndex = 1;
                }
            }
            else if (tableToDisplay is LifeInsuranceType)
            {
                LifeInsuranceType insuranceTypeSelected = (LifeInsuranceType)dgTableContent.SelectedItem;
                txtName.Text = insuranceTypeSelected.LifeInsuranceTypeName;
                txtName.IsEnabled = false;
                if (insuranceTypeSelected.Status == true)
                {
                    cbStatus.SelectedIndex = 0;
                }
                else
                {
                    cbStatus.SelectedIndex = 1;
                }
            }
            else if (tableToDisplay is InvesmentPlan)
            {
                InvesmentPlan investmentPlanSelected = (InvesmentPlan)dgTableContent.SelectedItem;
                txtName.Text = investmentPlanSelected.InvesmentPlanName;
                txtName.IsEnabled = false;
                if (investmentPlanSelected.Status == true)
                {
                    cbStatus.SelectedIndex = 0;
                }
                else
                {
                    cbStatus.SelectedIndex = 1;
                }
            }
            else if (tableToDisplay is FundJoiningPlan)
            {
                FundJoiningPlan joiningPlanSelected = (FundJoiningPlan)dgTableContent.SelectedItem;
                txtName.Text = joiningPlanSelected.FundJoiningPlanName;
                txtName.IsEnabled = false;
                if (joiningPlanSelected.Status == true)
                {
                    cbStatus.SelectedIndex = 0;
                }
                else
                {
                    cbStatus.SelectedIndex = 1;
                }
            }
            else if (tableToDisplay is InsurancesWaiver)
            {
                InsurancesWaiver insuranceWaiverSelected = (InsurancesWaiver)dgTableContent.SelectedItem;
                txtName.Text = insuranceWaiverSelected.InsurancesWaiverName;
                txtName.IsEnabled = false;
                if (insuranceWaiverSelected.Status == true)
                {
                    cbStatus.SelectedIndex = 0;
                }
                else
                {
                    cbStatus.SelectedIndex = 1;
                }
            }
            else if (tableToDisplay is RetirementPlan)
            {
                RetirementPlan retirementPlanSelected = (RetirementPlan)dgTableContent.SelectedItem;
                txtName.Text = retirementPlanSelected.RetirementPlanName;
                txtName.IsEnabled = false;
                if (retirementPlanSelected.Status == true)
                {
                    cbStatus.SelectedIndex = 0;
                }
                else
                {
                    cbStatus.SelectedIndex = 1;
                }
            }
            else if (tableToDisplay is FundType)
            {
                FundType fundTypeSelected = (FundType)dgTableContent.SelectedItem;
                txtName.Text = fundTypeSelected.FundTypeName;
                txtName.IsEnabled = false;
                if (fundTypeSelected.Status == true)
                {
                    cbStatus.SelectedIndex = 0;
                }
                else
                {
                    cbStatus.SelectedIndex = 1;
                }
            }
            else if (tableToDisplay is HealthInsuranceType)
            {
                HealthInsuranceType healthTypeSelected = (HealthInsuranceType)dgTableContent.SelectedItem;
                txtName.Text = healthTypeSelected.HealthInsuranceTypeName;
                txtName.IsEnabled = false;
                if (healthTypeSelected.Status == true)
                {
                    cbStatus.SelectedIndex = 0;
                }
                else
                {
                    cbStatus.SelectedIndex = 1;
                }
            }
        }

        //מנקה את הטבלה מכל סימון        
        private void dgTableContent_MouseDown(object sender, MouseButtonEventArgs e)
        {
            dgTableContent.UnselectAll();
        }

        private void txtName_GotFocus(object sender, RoutedEventArgs e)
        {            
            isChanges = true;
        }

        private void txtName_TextChanged(object sender, TextChangedEventArgs e)
        {
            isChanges = true;
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (isChanges)
            {
                if (MessageBox.Show("האם ברצונך לשמור שינוים שבצעת", "אישור", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    btnAddUpdate_Click(sender, new RoutedEventArgs());
                    if (cancelClose)
                    {
                        e.Cancel = true;
                        cancelClose = false;
                    }
                }
            }
        }
    }
}
