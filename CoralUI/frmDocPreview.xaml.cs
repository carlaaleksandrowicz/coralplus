﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using CoralBusinessLogics;

namespace CoralUI
{
    /// <summary>
    /// Interaction logic for frmDocPreview.xaml
    /// </summary>
    public partial class frmDocPreview : Window
    {
        List<Client> clientsToPrint = null;
        System.Collections.ObjectModel.ObservableCollection<DataGridColumn> columnsToAdd = null;
        string tableTitle = null;
        public frmDocPreview(object list, System.Collections.ObjectModel.ObservableCollection<DataGridColumn> columns, string title)
        {
            InitializeComponent();
            this.SizeToContent = SizeToContent.Manual;
            if (list is List<Client>)
            {
                clientsToPrint = (List<Client>)list;                
            }
            tableTitle = title;
            columnsToAdd = columns;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            table_.Background = Brushes.White;
            //Next, TableColumn objects are created and added to the table's Columns collection, with some formatting applied.          
            int numberOfColumns = columnsToAdd.Count;
            for (int x = 0; x < numberOfColumns; x++)
            {
                table_.Columns.Add(new TableColumn());
            }
            //Next, a title row is created and added to the table with some formatting applied. 
            // Create and add an empty TableRowGroup to hold the table's Rows.
            table_.RowGroups.Add(new TableRowGroup());

            // Add the first (title) row.
            table_.RowGroups[0].Rows.Add(new TableRow());

            // Alias the current working row for easy reference.
            TableRow currentRow = table_.RowGroups[0].Rows[0];

            // Global formatting for the title row.
            currentRow.Background = Brushes.LightGray;
            currentRow.FontSize = 18;
            currentRow.FontWeight = System.Windows.FontWeights.Bold;

            // Add the header row with content, 
            currentRow.Cells.Add(new TableCell(new Paragraph(new Run(tableTitle))));
            // and set the row to span all the columns.
            currentRow.Cells[0].ColumnSpan = columnsToAdd.Count;

            //Next, a header row is created and added to the table, and the cells in the header row are created and populated with content.            
            table_.RowGroups[0].Rows.Add(new TableRow());
            currentRow = table_.RowGroups[0].Rows[1];

            // Global formatting for the header row.
            currentRow.FontSize = 12;
            currentRow.FontWeight = FontWeights.Bold;

            // Add cells with content to the header row.
            foreach (var item in columnsToAdd)
            {
                currentRow.Cells.Add(new TableCell(new Paragraph(new Run(item.Header.ToString()))));
            }

            //Next, rows for data are created and added to the table_, and the cells in those rows are created and populated with content.

            if (clientsToPrint != null)
            {
                for (int i = 0; i < clientsToPrint.Count; i++)
                {
                    table_.RowGroups[0].Rows.Add(new TableRow());
                    currentRow = table_.RowGroups[0].Rows[i + 2];
                    currentRow.FontSize = 12;
                    currentRow.FontWeight = FontWeights.Normal;
                    if (i % 2 == 0)
                        currentRow.Background = Brushes.LightGray;
                    else
                        currentRow.Background = Brushes.White;
                    currentRow.Cells.Add(new TableCell(new Paragraph(new Run(clientsToPrint[i].LastName))));
                    currentRow.Cells.Add(new TableCell(new Paragraph(new Run(clientsToPrint[i].FirstName))));
                    currentRow.Cells.Add(new TableCell(new Paragraph(new Run(clientsToPrint[i].IdNumber))));
                    DateTime birthDate = ((DateTime)clientsToPrint[i].BirthDate).Date;
                    currentRow.Cells.Add(new TableCell(new Paragraph(new Run(birthDate.ToString("d")))));
                    currentRow.Cells.Add(new TableCell(new Paragraph(new Run(clientsToPrint[i].City))));
                    currentRow.Cells.Add(new TableCell(new Paragraph(new Run(clientsToPrint[i].Street))));
                    currentRow.Cells.Add(new TableCell(new Paragraph(new Run(clientsToPrint[i].HomeNumber))));
                    currentRow.Cells.Add(new TableCell(new Paragraph(new Run(clientsToPrint[i].ApartmentNumber))));
                    currentRow.Cells.Add(new TableCell(new Paragraph(new Run(clientsToPrint[i].PhoneHome))));
                    currentRow.Cells.Add(new TableCell(new Paragraph(new Run(clientsToPrint[i].CellPhone))));
                    currentRow.Cells.Add(new TableCell(new Paragraph(new Run(clientsToPrint[i].PhoneWork))));
                    currentRow.Cells.Add(new TableCell(new Paragraph(new Run(clientsToPrint[i].Fax))));
                    currentRow.Cells.Add(new TableCell(new Paragraph(new Run(clientsToPrint[i].Email))));
                }
            }
        }
    }
}
