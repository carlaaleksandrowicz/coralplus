﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using CoralBusinessLogics;
using System.IO;
using System.Globalization;

namespace CoralUI
{
    /// <summary>
    /// Interaction logic for frmClientProfile.xaml
    /// </summary>
    public partial class frmClientProfile : Window
    {
        User user;
        Client client;
        ClientsLogic clientLogic = new ClientsLogic();
        List<Client> referedByClient;
        frmReferedByClientList win;
        frmClientImage imageWindow;
        PoliciesLogic elementaryLogic = new PoliciesLogic();
        ClaimsLogic elementaryClaimLogic = new ClaimsLogic();
        LifePoliciesLogic lifeLogic = new LifePoliciesLogic();
        LifeClaimsLogic lifeClaimLogic = new LifeClaimsLogic();
        ForeingWorkersLogic foreingWorkersLogic = new ForeingWorkersLogic();
        ForeingWorkersClaimsLogic foreingWorkersClaimLogic = new ForeingWorkersClaimsLogic();
        HealthPoliciesLogic healthLogic = new HealthPoliciesLogic();
        HealthClaimsLogic healthClaimLogic = new HealthClaimsLogic();
        FinancePoliciesLogic financeLogic = new FinancePoliciesLogic();
        PersonalAccidentsLogic personalAccidentsLogic = new PersonalAccidentsLogic();
        PersonalAccidentsClaimsLogic personalAccidentsClaimLogic = new PersonalAccidentsClaimsLogic();
        TravelLogic travelLogic = new TravelLogic();
        TravelClaimsLogic travelClaimLogic = new TravelClaimsLogic();
        decimal elementaryTotalPremium = 0;
        decimal elementaryLastYearPremium = 0;
        decimal elementary3YearPremium = 0;
        decimal elementary5YearPremium = 0;
        decimal elementaryTotalClaims = 0;
        decimal elementaryLastYearClaims = 0;
        decimal elementary3YearsClaims = 0;
        decimal elementary5YearsClaims = 0;
        decimal lifeTotalPremium = 0;
        decimal lifeLastYearPremium = 0;
        decimal life3YearPremium = 0;
        decimal life5YearPremium = 0;
        decimal lifeTotalClaims = 0;
        decimal lifeLastYearClaims = 0;
        decimal life3YearsClaims = 0;
        decimal life5YearsClaims = 0;
        decimal foreingWorkersTotalPremium = 0;
        decimal foreingWorkersLastYearPremium = 0;
        decimal foreingWorkers3YearPremium = 0;
        decimal foreingWorkers5YearPremium = 0;
        decimal foreingWorkersTotalClaims = 0;
        decimal foreingWorkersLastYearClaims = 0;
        decimal foreingWorkers3YearsClaims = 0;
        decimal foreingWorkers5YearsClaims = 0;
        decimal healthTotalPremium = 0;
        decimal healthLastYearPremium = 0;
        decimal health3YearPremium = 0;
        decimal health5YearPremium = 0;
        decimal healthTotalClaims = 0;
        decimal healthLastYearClaims = 0;
        decimal health3YearsClaims = 0;
        decimal health5YearsClaims = 0;
        decimal personalAccidentsTotalPremium = 0;
        decimal personalAccidentsLastYearPremium = 0;
        decimal personalAccidents3YearPremium = 0;
        decimal personalAccidents5YearPremium = 0;
        decimal personalAccidentsTotalClaims = 0;
        decimal personalAccidentsLastYearClaims = 0;
        decimal personalAccidents3YearsClaims = 0;
        decimal personalAccidents5YearsClaims = 0;
        decimal financeTotalPremium = 0;
        decimal financeLastYearPremium = 0;
        decimal finance3YearPremium = 0;
        decimal finance5YearPremium = 0;
        decimal travelTotalPremium = 0;
        decimal travelLastYearPremium = 0;
        decimal travel3YearPremium = 0;
        decimal travel5YearPremium = 0;
        decimal travelTotalClaims = 0;
        decimal travelLastYearClaims = 0;
        decimal travel3YearsClaims = 0;
        decimal travel5YearsClaims = 0;
        private string clientDirPath;
        private string clientGeneralPath;
        decimal kupatGuemelLastYearPremium = 0;
        int kupatGuemelLastYearCount = 0;
        decimal kupatGuemel3YearsPremium = 0;
        int kupatGuemel3YearsCount = 0;
        decimal kupatGuemel5YearsPremium = 0;
        int kupatGuemel5YearsCount = 0;
        decimal kupatGuemelPremium = 0;
        int kupatGuemelCount = 0;
        decimal kerenHishtalmutLastYearPremium = 0;
        int kerenHishtalmutLastYearCount = 0;
        decimal kerenHishtalmut3YearsPremium = 0;
        int kerenHishtalmut3YearsCount = 0;
        decimal kerenHishtalmut5YearsPremium = 0;
        int kerenHishtalmut5YearsCount = 0;
        decimal kerenHishtalmutPremium = 0;
        int kerenHishtalmutCount = 0;
        decimal invesmentPortfolioLastYearPremium = 0;
        int invesmentPortfolioLastYearCount = 0;
        decimal invesmentPortfolio3YearsPremium = 0;
        int invesmentPortfolio3YearsCount = 0;
        decimal invesmentPortfolio5YearsPremium = 0;
        int invesmentPortfolio5YearsCount = 0;
        decimal invesmentPortfolioPremium = 0;
        int invesmentPortfolioCount = 0;
        decimal managersLastYearPremium = 0;
        int managersLastYearCount = 0;
        decimal managers3YearsPremium = 0;
        int managers3YearsCount = 0;
        decimal managers5YearsPremium = 0;
        int managers5YearsCount = 0;
        decimal managersPremium = 0;
        int managersCount = 0;
        decimal fWorkersLastYearPremium = 0;
        int fWorkersLastYearCount = 0;
        decimal fWorkers3YearsPremium = 0;
        int fWorkers3YearsCount = 0;
        decimal fWorkers5YearsPremium = 0;
        int fWorkers5YearsCount = 0;
        decimal fWorkersPremium = 0;
        int fWorkersCount = 0;
        decimal touristsLastYearPremium = 0;
        int touristsLastYearCount = 0;
        decimal tourists3YearsPremium = 0;
        int tourists3YearsCount = 0;
        decimal tourists5YearsPremium = 0;
        int tourists5YearsCount = 0;
        decimal touristsPremium = 0;
        int touristsCount = 0;
        decimal mortgageLastYearPremium = 0;
        int mortgageLastYearCount = 0;
        decimal mortgage3YearsPremium = 0;
        int mortgage3YearsCount = 0;
        decimal mortgage5YearsPremium = 0;
        int mortgage5YearsCount = 0;
        decimal mortgagePremium = 0;
        int mortgageCount = 0;
        decimal privateLastYearPremium = 0;
        int privateLastYearCount = 0;
        decimal private3YearsPremium = 0;
        int private3YearsCount = 0;
        decimal private5YearsPremium = 0;
        int private5YearsCount = 0;
        decimal privatePremium = 0;
        int privateCount = 0;
        decimal pensionLastYearPremium = 0;
        int pensionLastYearCount = 0;
        decimal pension3YearsPremium = 0;
        int pension3YearsCount = 0;
        decimal pension5YearsPremium = 0;
        int pension5YearsCount = 0;
        decimal pensionPremium = 0;
        int pensionCount = 0;
        decimal riskLastYearPremium = 0;
        int riskLastYearCount = 0;
        decimal risk3YearsPremium = 0;
        int risk3YearsCount = 0;
        decimal risk5YearsPremium = 0;
        int risk5YearsCount = 0;
        decimal riskPremium = 0;
        int riskCount = 0;
        decimal jobaLastYearPremium = 0;
        int jobaLastYearCount = 0;
        decimal joba3YearsPremium = 0;
        int joba3YearsCount = 0;
        decimal joba5YearsPremium = 0;
        int joba5YearsCount = 0;
        decimal jobaPremium = 0;
        int jobaCount = 0;
        decimal carLastYearPremium = 0;
        int carLastYearCount = 0;
        decimal car3YearsPremium = 0;
        int car3YearsCount = 0;
        decimal car5YearsPremium = 0;
        int car5YearsCount = 0;
        decimal carPremium = 0;
        int carCount = 0;
        decimal jabilaLastYearPremium = 0;
        int jabilaLastYearCount = 0;
        decimal jabila3YearsPremium = 0;
        int jabila3YearsCount = 0;
        decimal jabila5YearsPremium = 0;
        int jabila5YearsCount = 0;
        decimal jabilaPremium = 0;
        int jabilaCount = 0;
        decimal aptLastYearPremium = 0;
        int aptLastYearCount = 0;
        decimal apt3YearsPremium = 0;
        int apt3YearsCount = 0;
        decimal apt5YearsPremium = 0;
        int apt5YearsCount = 0;
        decimal aptPremium = 0;
        int aptCount = 0;
        decimal businessLastYearPremium = 0;
        int businessLastYearCount = 0;
        decimal business3YearsPremium = 0;
        int business3YearsCount = 0;
        decimal business5YearsPremium = 0;
        int business5YearsCount = 0;
        decimal businessPremium = 0;
        int businessCount = 0;
        int personalAccidentsLastYearCount = 0;
        int personalAccidents3YearsCount = 0;
        int personalAccidents5YearsCount = 0;
        int personalAccidentsCount = 0;
        int travelLastYearCount = 0;
        int travel3YearsCount = 0;
        int travel5YearsCount = 0;
        int travelCount = 0;
        int healthLastYearCount = 0;
        int health3YearsCount = 0;
        int health5YearsCount = 0;
        int healthCount = 0;
        private Brush lightGreen;
        private Brush darkBlue;
        private Brush turkiz;
        
        private int elementaryClaimCount;
        private int lifeClaimsCount;
        private int healthClaimsCount;
        private int personalAccidentsClaimsCount;
        private int travelClaimsCount;
        private int foreingWorkersClaimCount;
        private int elementaryLastYearClaimsCount;
        private int elementary3YearsClaimsCount;
        private int elementary5YearsClaimsCount;
        private int lifeLastYearClaimsCount;
        private int life3YearsClaimsCount;
        private int life5YearsClaimsCount;
        private int foreingWorkersLastYearClaimsCount;
        private int foreingWorkers3YearsClaimsCount;
        private int foreingWorkers5YearsClaimsCount;
        private int healthLastYearClaimsCount;
        private int health3YearsClaimsCount;
        private int health5YearsClaimsCount;
        private int personalAccidentsLastYearClaimsCount;
        private int personalAccidents3YearsClaimsCount;
        private int personalAccidents5YearsClaimsCount;
        private int travelLastYearClaimsCount;
        private int travel3YearsClaimsCount;
        private int travel5YearsClaimsCount;
        private int elementaryOffersLastYear;
        private int personalAccidentsOffersLastYear;
        private int travelOffersLastYear;
        private int lifeOffersLastYear;
        private int healthOffersLastYear;
        private int financeOffersLastYear;
        private int foreingWorkersOffersLastYear;
        private int elementaryOffers3Years;
        private int personalAccidentsOffers3Years;
        private int travelOffers3Years;
        private int lifeOffers3Years;
        private int healthOffers3Years;
        private int financeOffers3Years;
        private int foreingWorkersOffers3Years;
        private int elementaryOffers5Years;
        private int personalAccidentsOffers5Years;
        private int travelOffers5Years;
        private int lifeOffers5Years;
        private int healthOffers5Years;
        private int financeOffers5Years;
        private int foreingWorkersOffers5Years;
        private int elementaryOffersAll;
        private int personalAccidentsOffersAll;
        private int travelOffersAll;
        private int lifeOffersAll;
        private int healthOffersAll;
        private int financeOffersAll;
        private int foreingWorkersOffersAll;

        public List<ElementaryPolicy> YearsJobaPolicies { get; private set; }

        public frmClientProfile(Client client, User user)
        {
            InitializeComponent();
            this.client = client;
            this.user = user;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {                 
            string strAppDir = System.AppDomain.CurrentDomain.BaseDirectory;
            string path = strAppDir + @"Coral Files\ClientsPath.txt";
            if (File.Exists(path))
            {
                clientDirPath = File.ReadAllText(path);
                clientGeneralPath = clientDirPath + @"\" + client.ClientID + @"\כללי";
            }
            else
            {
                MessageBox.Show("לא נמצא תיקיית הלקוחות. נא בדוק בהגדרות ראשוניות את נתיב התיקייה");
                Close();
                return;
            }

            BrushConverter bc = new BrushConverter();
            lightGreen = (Brush)bc.ConvertFrom("#FF66A503");
            darkBlue = (Brush)bc.ConvertFrom("#FF030F40");
            turkiz = (Brush)bc.ConvertFrom("#FF1695A3");            

            if (client != null)
            {
                RefreshClientDetails();
            }
        }

        private void RefreshClientDetails()
        {
            lblClientName.Content = client.FirstName + " " + client.LastName;
            lblName.Content = lblClientName.Content;
            lblNameClient.Content = lblClientName.Content;
            lblIdNumber.Content = client.IdNumber;
            if (client.BirthDate is DateTime)
            {
                lblBirthDate.Content = ((DateTime)client.BirthDate).ToString("dd-MM-yyyy");
                DateTime today = DateTime.Today;
                int age = today.Year - ((DateTime)client.BirthDate).Year;
                if (((DateTime)client.BirthDate) > today.AddYears(-age))
                    age--;
                lblAge.Content = age.ToString("0.##");
            }
            lblMaritalStatus.Content = client.MaritalStatus;
            lblAddress.Content = string.Format("{0} {1}, {2}", client.Street, client.HomeNumber, client.City);
            lblPhone.Content = client.PhoneHome;
            lblCellphone.Content = client.CellPhone;
            lblEmail.Content = client.Email;
            if (client.PicturePath != "" && client.PicturePath != null)
            {
                txtPictureText.Text = "";
            }
            PictureBinding(client.PicturePath);
            if (client.ReferedByClientID != null)
            {
                var referedBy = clientLogic.GetClientByClientID((int)client.ReferedByClientID);
                lblReferredBy.Content = referedBy.FirstName + " " + referedBy.LastName;

            }
            lblRelation.Content = client.ReferedByRelation;
            referedByClient = clientLogic.GetReferedByClient(client.ClientID);
            hypReferedByClientCount.Text = referedByClient.Count.ToString();
            GetAndDisplayPolicyInfo();
            txtComments.Text = client.Remarks;
        }

        private void GetAndDisplayPolicyInfo()
        {
            SetElementaryInfo();
            SetLifeInfo();
            SetHealthInfo();
            SetFinanceInfo();
            SetPersonalAccidentsInfo();
            SetTravelInfo();
            SetForeingWorkersInfo();
            SetOffersInfo();
            
            DisplayProfitabilityData();
        }

        private void SetOffersInfo()
        {
            var elementaryOffers = elementaryLogic.GetAllOffersByClient(client);
            var personalAccidentsOffers = personalAccidentsLogic.GetAllPersonalAccidentsOffersByClient(client);
            var travelOffers = travelLogic.GetAllTravelOffersByClient(client);
            var lifeOffers = lifeLogic.GetAllLifeOffersByClient(client);
            var healthOffers = healthLogic.GetAllHealthOffersByClient(client);
            var financeOffers = financeLogic.GetAllFinanceOffersByClient(client);
            var foreingWorkersOffers = foreingWorkersLogic.GetAllForeingWorkersOffersByClient(client);

            elementaryOffersLastYear=(elementaryOffers.Where(o=>o.EndDate> DateTime.Now).ToList()).Count;
            personalAccidentsOffersLastYear = (personalAccidentsOffers.Where(o => o.EndDate > DateTime.Now).ToList()).Count;
            travelOffersLastYear = (travelOffers.Where(o => o.EndDate > DateTime.Now).ToList()).Count;
            lifeOffersLastYear = (lifeOffers.Where(o => o.EndDate > DateTime.Now).ToList()).Count;
            healthOffersLastYear = (healthOffers.Where(o => o.EndDate > DateTime.Now).ToList()).Count;
            financeOffersLastYear = (financeOffers.Where(o => o.EndDate > DateTime.Now).ToList()).Count;
            foreingWorkersOffersLastYear = (foreingWorkersOffers.Where(o => o.EndDate > DateTime.Now).ToList()).Count;

            elementaryOffers3Years = (elementaryOffers.Where(o => o.EndDate > DateTime.Now.AddYears(-3)).ToList()).Count;
            personalAccidentsOffers3Years = (personalAccidentsOffers.Where(o => o.EndDate > DateTime.Now.AddYears(-3)).ToList()).Count;
            travelOffers3Years = (travelOffers.Where(o => o.EndDate > DateTime.Now.AddYears(-3)).ToList()).Count;
            lifeOffers3Years = (lifeOffers.Where(o => o.EndDate > DateTime.Now.AddYears(-3)).ToList()).Count;
            healthOffers3Years = (healthOffers.Where(o => o.EndDate > DateTime.Now.AddYears(-3)).ToList()).Count;
            financeOffers3Years = (financeOffers.Where(o => o.EndDate > DateTime.Now.AddYears(-3)).ToList()).Count;
            foreingWorkersOffers3Years = (foreingWorkersOffers.Where(o => o.EndDate > DateTime.Now.AddYears(-3)).ToList()).Count;

            elementaryOffers5Years = (elementaryOffers.Where(o => o.EndDate > DateTime.Now.AddYears(-5)).ToList()).Count;
            personalAccidentsOffers5Years = (personalAccidentsOffers.Where(o => o.EndDate > DateTime.Now.AddYears(-5)).ToList()).Count;
            travelOffers5Years = (travelOffers.Where(o => o.EndDate > DateTime.Now.AddYears(-5)).ToList()).Count;
            lifeOffers5Years = (lifeOffers.Where(o => o.EndDate > DateTime.Now.AddYears(-5)).ToList()).Count;
            healthOffers5Years = (healthOffers.Where(o => o.EndDate > DateTime.Now.AddYears(-5)).ToList()).Count;
            financeOffers5Years = (financeOffers.Where(o => o.EndDate > DateTime.Now.AddYears(-5)).ToList()).Count;
            foreingWorkersOffers5Years = (foreingWorkersOffers.Where(o => o.EndDate > DateTime.Now.AddYears(-5)).ToList()).Count;

            elementaryOffersAll = elementaryOffers.Count;
            personalAccidentsOffersAll = personalAccidentsOffers.Count;
            travelOffersAll = travelOffers.Count;
            lifeOffersAll = lifeOffers.Count;
            healthOffersAll = healthOffers.Count;
            financeOffersAll = financeOffers.Count;
            foreingWorkersOffersAll = foreingWorkersOffers.Count;
        }

        private void SetForeingWorkersInfo()
        {
            List<ForeingWorkersPolicy> lastYearForeingWorkersPolicies = null;
            List<ForeingWorkersPolicy> threeYearsForeingWorkersPolicies = null;
            List<ForeingWorkersPolicy> fiveYearsForeingWorkersPolicies = null;
            List<ForeingWorkersClaim> threeYearsForeingWorkersClaims = null;
            List<ForeingWorkersClaim> fiveYearsForeingWorkersClaims = null;
            List<ForeingWorkersClaim> lastYearForeingWorkersClaims = new List<ForeingWorkersClaim>();

            var foreingWorkersPolicies = foreingWorkersLogic.GetAllForeingWorkersPoliciesByClient(client);
            if (foreingWorkersPolicies != null && foreingWorkersPolicies.Count > 0)
            {
                foreingWorkersTotalPremium = (decimal)foreingWorkersPolicies.Sum(p => p.TotalPremium);
                lastYearForeingWorkersPolicies = foreingWorkersPolicies.Where(p => p.EndDate > DateTime.Now).ToList();
                if (lastYearForeingWorkersPolicies.Count > 0)
                {
                    foreingWorkersLastYearPremium = (decimal)lastYearForeingWorkersPolicies.Sum(p => p.TotalPremium);
                }
                threeYearsForeingWorkersPolicies = foreingWorkersPolicies.Where(p => p.EndDate >= DateTime.Now.AddYears(-3)).ToList();
                if (threeYearsForeingWorkersPolicies.Count > 0)
                {
                    foreingWorkers3YearPremium = (decimal)threeYearsForeingWorkersPolicies.Sum(p => p.TotalPremium);
                }
                fiveYearsForeingWorkersPolicies = foreingWorkersPolicies.Where(p => p.EndDate >= DateTime.Now.AddYears(-5)).ToList();
                if (fiveYearsForeingWorkersPolicies.Count > 0)
                {
                    foreingWorkers5YearPremium = (decimal)fiveYearsForeingWorkersPolicies.Sum(p => p.TotalPremium);
                }
            }

            var foreingWorkersClaims = foreingWorkersClaimLogic.GetAllForeingWorkersClaimsByClient(client.ClientID);
            foreingWorkersClaimCount = foreingWorkersClaims.Count;
            if (foreingWorkersClaims != null && foreingWorkersClaims.Count > 0)
            {
                var foreingWorkersAmountedClaims = foreingWorkersClaims.Where(c => c.AmountReceived != null);
                foreingWorkersTotalClaims = (decimal)foreingWorkersAmountedClaims.Sum(c => c.AmountReceived);
                //List<ForeingWorkersClaim> lastYearForeingWorkersClaims = new List<ForeingWorkersClaim>();
                if (lastYearForeingWorkersPolicies != null)
                {
                    foreach (var item in lastYearForeingWorkersPolicies)
                    {
                        var policyClaims = foreingWorkersClaims.Where(c => c.ForeingWorkersPolicyID == item.ForeingWorkersPolicyID).ToList();
                        if (policyClaims != null && policyClaims.Count() > 0)
                        {
                            foreach (var claim in policyClaims)
                            {
                                lastYearForeingWorkersClaims.Add(claim);
                            }
                        }
                    }
                }
                foreingWorkersLastYearClaimsCount = lastYearForeingWorkersClaims.Count;
                if (lastYearForeingWorkersClaims.Count > 0)
                {
                    var lastYearAmountedClaims = lastYearForeingWorkersClaims.Where(c => c.AmountReceived != null).ToList();
                    foreingWorkersLastYearClaims = (decimal)lastYearAmountedClaims.Sum(c => c.AmountReceived);
                }
                threeYearsForeingWorkersClaims = foreingWorkersClaims.Where(c => c.EventDateAndTime >= DateTime.Now.AddYears(-3)).ToList();
                foreingWorkers3YearsClaimsCount = threeYearsForeingWorkersClaims.Count;
                if (threeYearsForeingWorkersClaims.Count > 0)
                {
                    var threeYearsAmountedClaims = threeYearsForeingWorkersClaims.Where(c => c.AmountReceived != null).ToList();
                    foreingWorkers3YearsClaims = (decimal)threeYearsAmountedClaims.Sum(c => c.AmountReceived);
                }
                fiveYearsForeingWorkersClaims = foreingWorkersClaims.Where(c => c.EventDateAndTime >= DateTime.Now.AddYears(-5)).ToList();
                foreingWorkers5YearsClaimsCount = fiveYearsForeingWorkersClaims.Count;
                if (fiveYearsForeingWorkersClaims.Count > 0)
                {
                    var fiveYearsAmountedClaims = fiveYearsForeingWorkersClaims.Where(c => c.AmountReceived != null).ToList();
                    foreingWorkers5YearsClaims = (decimal)fiveYearsAmountedClaims.Sum(c => c.AmountReceived);
                }
            }

            if (lastYearForeingWorkersPolicies != null)
            {
                var lastYearFWorkersPolicies = lastYearForeingWorkersPolicies.Where(p => p.ForeingWorkersIndustry.ForeingWorkersIndustryName == "עובדים זרים").ToList();
                fWorkersLastYearCount = lastYearFWorkersPolicies.Count;
                lblForeingWorkersPoliciesCount.Content = fWorkersLastYearCount.ToString();
                fWorkersLastYearPremium = (decimal)(lastYearFWorkersPolicies.Sum(p => p.TotalPremium));
                lblForeingWorkersPremium.Content = fWorkersLastYearPremium.ToString("0.##");

                var lastYearTouristsPolicies = lastYearForeingWorkersPolicies.Where(p => p.ForeingWorkersIndustry.ForeingWorkersIndustryName == "תיירים").ToList();
                touristsLastYearCount = lastYearTouristsPolicies.Count;
                lblTouristsPoliciesCount.Content = touristsLastYearCount.ToString();
                touristsLastYearPremium = (decimal)(lastYearTouristsPolicies.Sum(p => p.TotalPremium));
                lblTouristsPremium.Content = touristsLastYearPremium.ToString("0.##");
            }
            if (lastYearForeingWorkersClaims != null)
            {
                var lastYearFWorkersClaims = lastYearForeingWorkersClaims.Where(c => c.ForeingWorkersPolicy.ForeingWorkersIndustry.ForeingWorkersIndustryName == "עובדים זרים").ToList();
                lblForeingWorkersClaimsCount.Content = lastYearFWorkersClaims.Count.ToString();
                lblForeingWorkersClaimsAmount.Content = ((decimal)(lastYearFWorkersClaims.Where(c => c.AmountReceived != null).Sum(c => c.AmountReceived))).ToString("0.##");

                var lastYearTouristsClaims = lastYearForeingWorkersClaims.Where(c => c.ForeingWorkersPolicy.ForeingWorkersIndustry.ForeingWorkersIndustryName == "תיירים").ToList();
                lblTouristsClaimsCount.Content = lastYearTouristsClaims.Count.ToString();
                lblTouristsClaimsAmount.Content = ((decimal)(lastYearTouristsClaims.Where(c => c.AmountReceived != null).Sum(c => c.AmountReceived))).ToString("0.##");
            }
            if (threeYearsForeingWorkersPolicies != null)
            {
                var threeYearsFWorkersPolicies = threeYearsForeingWorkersPolicies.Where(p => p.ForeingWorkersIndustry.ForeingWorkersIndustryName == "עובדים זרים").ToList();
                fWorkers3YearsCount = threeYearsFWorkersPolicies.Count;
                lblForeingWorkersPoliciesCount3.Content = fWorkers3YearsCount.ToString();
                fWorkers3YearsPremium = (decimal)(threeYearsFWorkersPolicies.Sum(p => p.TotalPremium));
                lblForeingWorkersPremium3.Content = fWorkers3YearsPremium.ToString("0.##");

                var threeYearsTouristsPolicies = threeYearsForeingWorkersPolicies.Where(p => p.ForeingWorkersIndustry.ForeingWorkersIndustryName == "תיירים").ToList();
                tourists3YearsCount = threeYearsTouristsPolicies.Count;
                lblTouristsPoliciesCount3.Content = tourists3YearsCount.ToString();
                tourists3YearsPremium = (decimal)(threeYearsTouristsPolicies.Sum(p => p.TotalPremium));
                lblTouristsPremium3.Content = tourists3YearsPremium.ToString("0.##");
            }
            if (threeYearsForeingWorkersClaims != null)
            {
                var threeYearsFWorkersClaims = threeYearsForeingWorkersClaims.Where(p => p.ForeingWorkersPolicy.ForeingWorkersIndustry.ForeingWorkersIndustryName == "עובדים זרים").ToList();
                lblForeingWorkersClaimsCount3.Content = threeYearsFWorkersClaims.Count.ToString();
                lblForeingWorkersClaimsAmount3.Content = ((decimal)(threeYearsFWorkersClaims.Where(c => c.AmountReceived != null).Sum(c => c.AmountReceived))).ToString("0.##");

                var threeYearsTouristsClaims = threeYearsForeingWorkersClaims.Where(c => c.ForeingWorkersPolicy.ForeingWorkersIndustry.ForeingWorkersIndustryName == "תיירים").ToList();
                lblTouristsClaimsCount3.Content = threeYearsTouristsClaims.Count.ToString();
                lblTouristsClaimsAmount3.Content = ((decimal)(threeYearsTouristsClaims.Where(c => c.AmountReceived != null).Sum(c => c.AmountReceived))).ToString("0.##");
            }
            if (fiveYearsForeingWorkersPolicies != null)
            {
                var fiveYearsFWorkersPolicies = fiveYearsForeingWorkersPolicies.Where(p => p.ForeingWorkersIndustry.ForeingWorkersIndustryName == "עובדים זרים").ToList();
                fWorkers5YearsCount = fiveYearsFWorkersPolicies.Count;
                lblForeingWorkersPoliciesCount5.Content = fWorkers5YearsCount.ToString();
                fWorkers5YearsPremium = (decimal)(fiveYearsFWorkersPolicies.Sum(p => p.TotalPremium));
                lblForeingWorkersPremium5.Content = fWorkers5YearsPremium.ToString("0.##");

                var fiveYearsTouristsPolicies = fiveYearsForeingWorkersPolicies.Where(p => p.ForeingWorkersIndustry.ForeingWorkersIndustryName == "תיירים").ToList();
                tourists5YearsCount = fiveYearsTouristsPolicies.Count;
                lblTouristsPoliciesCount5.Content = tourists5YearsCount.ToString();
                tourists5YearsPremium = (decimal)(fiveYearsTouristsPolicies.Sum(p => p.TotalPremium));
                lblTouristsPremium5.Content = tourists5YearsPremium.ToString("0.##");
            }
            if (fiveYearsForeingWorkersClaims != null)
            {
                var fiveYearsFWorkersClaims = fiveYearsForeingWorkersClaims.Where(p => p.ForeingWorkersPolicy.ForeingWorkersIndustry.ForeingWorkersIndustryName == "עובדים זרים").ToList();
                lblForeingWorkersClaimsCount5.Content = fiveYearsFWorkersClaims.Count.ToString();
                lblForeingWorkersClaimsAmount5.Content = ((decimal)(fiveYearsFWorkersClaims.Where(c => c.AmountReceived != null).Sum(c => c.AmountReceived))).ToString("0.##");

                var fiveYearsTouristsClaims = fiveYearsForeingWorkersClaims.Where(c => c.ForeingWorkersPolicy.ForeingWorkersIndustry.ForeingWorkersIndustryName == "תיירים").ToList();
                lblTouristsClaimsCount5.Content = fiveYearsTouristsClaims.Count.ToString();
                lblTouristsClaimsAmount5.Content = ((decimal)(fiveYearsTouristsClaims.Where(c => c.AmountReceived != null).Sum(c => c.AmountReceived))).ToString("0.##");
            }
            if (foreingWorkersPolicies != null)
            {
                var fWorkersPolicies = foreingWorkersPolicies.Where(p => p.ForeingWorkersIndustry.ForeingWorkersIndustryName == "עובדים זרים").ToList();
                fWorkersCount = fWorkersPolicies.Count;
                lblForeingWorkersPoliciesCountAll.Content = fWorkersCount.ToString();
                fWorkersPremium = (decimal)(fWorkersPolicies.Sum(p => p.TotalPremium));
                lblForeingWorkersPremiumAll.Content = fWorkersPremium.ToString("0.##");

                var touristsPolicies = foreingWorkersPolicies.Where(p => p.ForeingWorkersIndustry.ForeingWorkersIndustryName == "תיירים").ToList();
                touristsCount = touristsPolicies.Count;
                lblTouristsPoliciesCountAll.Content = touristsCount.ToString();
                touristsPremium = (decimal)(touristsPolicies.Sum(p => p.TotalPremium));
                lblTouristsPremiumAll.Content = touristsPremium.ToString("0.##");
            }
            if (foreingWorkersClaims != null)
            {
                var fWorkersClaims = foreingWorkersClaims.Where(p => p.ForeingWorkersPolicy.ForeingWorkersIndustry.ForeingWorkersIndustryName == "עובדים זרים").ToList();
                lblForeingWorkersClaimsCountAll.Content = fWorkersClaims.Count.ToString();
                lblForeingWorkersClaimsAmountAll.Content = ((decimal)(fWorkersClaims.Where(c => c.AmountReceived != null).Sum(c => c.AmountReceived))).ToString("0.##");

                var touristsClaims = foreingWorkersClaims.Where(c => c.ForeingWorkersPolicy.ForeingWorkersIndustry.ForeingWorkersIndustryName == "תיירים").ToList();
                lblTouristsClaimsCountAll.Content = touristsClaims.Count.ToString();
                lblTouristsClaimsAmountAll.Content = ((decimal)(touristsClaims.Where(c => c.AmountReceived != null).Sum(c => c.AmountReceived))).ToString("0.##");
            }
        }
        private void SetTravelInfo()
        {
            DateTime nowDate = DateTime.Now;
            var travelPolicies = travelLogic.GetAllTravelPoliciesByClient(client);

            if (travelPolicies != null && travelPolicies.Count > 0)
            {
                foreach (var item in travelPolicies)
                {
                    if (item.Premium != null)
                    {
                        decimal premium = (decimal)item.Premium;
                        //DateTime startDate = (DateTime)item.StartDate;
                        //int totalMonths = ((nowDate.Year - startDate.Year) * 12) + (nowDate.Month + 1) - startDate.Month;
                        travelTotalPremium = travelTotalPremium + premium;
                        travelCount++;
                        if (item.EndDate > nowDate)
                        {
                            //if (totalMonths <= 12)
                            //{
                            //    travelLastYearPremium = travelLastYearPremium + (premium * totalMonths);
                            //}
                            //else
                            //{
                            //    travelLastYearPremium = travelLastYearPremium + (premium * 12);
                            //}
                            travelLastYearPremium = travelLastYearPremium + premium;
                            travelLastYearCount++;
                        }
                        if (item.EndDate > nowDate.AddYears(-3))
                        {
                            //if (totalMonths <= 36)
                            //{
                            //    travel3YearPremium = travel3YearPremium + (premium * totalMonths);
                            //}
                            //else
                            //{
                            //    travel3YearPremium = travel3YearPremium + (premium * 36);
                            //}
                            travel3YearPremium = travel3YearPremium + premium;
                            travel3YearsCount++;
                        }
                        if (item.EndDate > nowDate.AddYears(-5))
                        {
                            //if (totalMonths <= 60)
                            //{
                            //    travel5YearPremium = travel5YearPremium + (premium * totalMonths);
                            //}
                            //else
                            //{
                            //    travel5YearPremium = travel5YearPremium + (premium * 60);
                            //}
                            travel5YearPremium = travel5YearPremium + premium;
                            travel5YearsCount++;
                        }
                    }

                }
                var travelClaims = travelClaimLogic.GetAllTravelClaimsByClient(client.ClientID);
                travelClaimsCount = travelClaims.Count;
                travelLastYearClaimsCount = travelClaims.Where(c => c.EventDateAndTime > nowDate.AddYears(-1)).Count();
                travel3YearsClaimsCount = travelClaims.Where(c => c.EventDateAndTime > nowDate.AddYears(-3)).Count();
                travel5YearsClaimsCount = travelClaims.Where(c => c.EventDateAndTime > nowDate.AddYears(-5)).Count();

                if (travelClaims != null && travelClaims.Count > 0)
                {
                    var travelAmountedClaims = travelClaims.Where(c => c.AmountReceived != null);
                    travelTotalClaims = (decimal)travelAmountedClaims.Sum(c => c.AmountReceived);
                    travelLastYearClaims = (decimal)(travelAmountedClaims.Where(c => c.EventDateAndTime > nowDate.AddYears(-1)).Sum(c => c.AmountReceived));
                    travel3YearsClaims = (decimal)(travelAmountedClaims.Where(c => c.EventDateAndTime > nowDate.AddYears(-3)).Sum(c => c.AmountReceived));
                    travel5YearsClaims = (decimal)(travelAmountedClaims.Where(c => c.EventDateAndTime > nowDate.AddYears(-5)).Sum(c => c.AmountReceived));

                    lblTravelClaimsCount.Content = travelClaims.Where(c => c.EventDateAndTime > nowDate.AddYears(-1)).Count().ToString();
                    lblTravelClaimsAmount.Content = travelLastYearClaims.ToString("0.##");
                    lblTravelClaimsCount3.Content = travelClaims.Where(c => c.EventDateAndTime > nowDate.AddYears(-3)).Count().ToString();
                    lblTravelClaimsAmount3.Content = travel3YearsClaims.ToString("0.##");
                    lblTravelClaimsCount5.Content = travelClaims.Where(c => c.EventDateAndTime > nowDate.AddYears(-5)).Count().ToString();
                    lblTravelClaimsAmount5.Content = travel5YearsClaims.ToString("0.##");
                    lblTravelClaimsCountAll.Content = travelClaims.Count.ToString();
                    lblTravelClaimsAmountAll.Content = travelTotalClaims.ToString("0.##");
                }

                lblTravelPoliciesCount.Content = travelLastYearCount.ToString();
                lblTravelPremium.Content = travelLastYearPremium.ToString("0.##");
                lblTravelPoliciesCount3.Content = travel3YearsCount.ToString();
                lblTravelPremium3.Content = travel3YearPremium.ToString("0.##");
                lblTravelPoliciesCount5.Content = travel5YearsCount.ToString();
                lblTravelPremium5.Content = travel5YearPremium.ToString("0.##");
                lblTravelPoliciesCountAll.Content = travelCount.ToString();
                lblTravelPremiumAll.Content = travelTotalPremium.ToString("0.##");
            }
        }


        private void SetPersonalAccidentsInfo()
        {
            DateTime nowDate = DateTime.Now;
            var personalAccidentsPolicies = personalAccidentsLogic.GetAllPersonalAccidentsPoliciesByClient(client);

            if (personalAccidentsPolicies != null && personalAccidentsPolicies.Count > 0)
            {
                foreach (var item in personalAccidentsPolicies)
                {
                    if (item.TotalPremium != null)
                    {
                        decimal premium = (decimal)item.TotalPremium;
                        DateTime startDate = (DateTime)item.StartDate;
                        int totalMonths = ((nowDate.Year - startDate.Year) * 12) + (nowDate.Month + 1) - startDate.Month;
                        personalAccidentsTotalPremium = personalAccidentsTotalPremium + (premium * totalMonths);
                        personalAccidentsCount++;
                        if (item.EndDate > nowDate)
                        {
                            if (totalMonths <= 12)
                            {
                                personalAccidentsLastYearPremium = personalAccidentsLastYearPremium + (premium * totalMonths);
                            }
                            else
                            {
                                personalAccidentsLastYearPremium = personalAccidentsLastYearPremium + (premium * 12);
                            }
                            personalAccidentsLastYearCount++;
                        }
                        if (item.EndDate > nowDate.AddYears(-3))
                        {
                            if (totalMonths <= 36)
                            {
                                personalAccidents3YearPremium = personalAccidents3YearPremium + (premium * totalMonths);
                            }
                            else
                            {
                                personalAccidents3YearPremium = personalAccidents3YearPremium + (premium * 36);
                            }
                            personalAccidents3YearsCount++;
                        }
                        if (item.EndDate > nowDate.AddYears(-5))
                        {
                            if (totalMonths <= 60)
                            {
                                personalAccidents5YearPremium = personalAccidents5YearPremium + (premium * totalMonths);
                            }
                            else
                            {
                                personalAccidents5YearPremium = personalAccidents5YearPremium + (premium * 60);
                            }
                            personalAccidents5YearsCount++;
                        }
                    }

                }
                var personalAccidentsClaims = personalAccidentsClaimLogic.GetAllPersonalAccidentsClaimsByClient(client.ClientID);
                personalAccidentsClaimsCount = personalAccidentsClaims.Count;
                personalAccidentsLastYearClaimsCount = personalAccidentsClaims.Where(c => c.EventDateAndTime > nowDate.AddYears(-1)).Count();
                personalAccidents3YearsClaimsCount = personalAccidentsClaims.Where(c => c.EventDateAndTime > nowDate.AddYears(-3)).Count();
                personalAccidents5YearsClaimsCount = personalAccidentsClaims.Where(c => c.EventDateAndTime > nowDate.AddYears(-5)).Count();

                if (personalAccidentsClaims != null && personalAccidentsClaims.Count > 0)
                {
                    var personalAccidentsAmountedClaims = personalAccidentsClaims.Where(c => c.AmountReceived != null);
                    personalAccidentsTotalClaims = (decimal)personalAccidentsAmountedClaims.Sum(c => c.AmountReceived);
                    personalAccidentsLastYearClaims = (decimal)(personalAccidentsAmountedClaims.Where(c => c.EventDateAndTime > nowDate.AddYears(-1)).Sum(c => c.AmountReceived));
                    personalAccidents3YearsClaims = (decimal)(personalAccidentsAmountedClaims.Where(c => c.EventDateAndTime > nowDate.AddYears(-3)).Sum(c => c.AmountReceived));
                    personalAccidents5YearsClaims = (decimal)(personalAccidentsAmountedClaims.Where(c => c.EventDateAndTime > nowDate.AddYears(-5)).Sum(c => c.AmountReceived));

                    lblPersonalAccidentsClaimsCount.Content = personalAccidentsClaims.Where(c => c.EventDateAndTime > nowDate.AddYears(-1)).Count().ToString();
                    lblPersonalAccidentsClaimsAmount.Content = personalAccidentsLastYearClaims.ToString("0.##");
                    lblPersonalAccidentsClaimsCount3.Content = personalAccidentsClaims.Where(c => c.EventDateAndTime > nowDate.AddYears(-3)).Count().ToString();
                    lblPersonalAccidentsClaimsAmount3.Content = personalAccidents3YearsClaims.ToString("0.##");
                    lblPersonalAccidentsClaimsCount5.Content = personalAccidentsClaims.Where(c => c.EventDateAndTime > nowDate.AddYears(-5)).Count().ToString();
                    lblPersonalAccidentsClaimsAmount5.Content = personalAccidents5YearsClaims.ToString("0.##");
                    lblPersonalAccidentsClaimsCountAll.Content = personalAccidentsClaims.Count.ToString();
                    lblPersonalAccidentsClaimsAmountAll.Content = personalAccidentsTotalClaims.ToString("0.##");
                }

                lblPersonalAccidentsPoliciesCount.Content = personalAccidentsLastYearCount.ToString();
                lblPersonalAccidentsPremium.Content = personalAccidentsLastYearPremium.ToString("0.##");
                lblPersonalAccidentsPoliciesCount3.Content = personalAccidents3YearsCount.ToString();
                lblPersonalAccidentsPremium3.Content = personalAccidents3YearPremium.ToString("0.##");
                lblPersonalAccidentsPoliciesCount5.Content = personalAccidents5YearsCount.ToString();
                lblPersonalAccidentsPremium5.Content = personalAccidents5YearPremium.ToString("0.##");
                lblPersonalAccidentsPoliciesCountAll.Content = personalAccidentsCount.ToString();
                lblPersonalAccidentsPremiumAll.Content = personalAccidentsTotalPremium.ToString("0.##");
            }
        }

        private void SetFinanceInfo()
        {
            DateTime nowDate = DateTime.Now;
            var financePolicies = financeLogic.GetAllFinancePoliciesByClient(client);


            if (financePolicies != null && financePolicies.Count > 0)
            {
                foreach (var item in financePolicies)
                {
                    if ((item.DepositAmountIndependent != null || item.DepositAmountEmployee != null))
                    {
                        decimal premium = 0;
                        if (item.DepositAmountIndependent == null)
                        {
                            premium = (decimal)item.DepositAmountEmployee;
                        }
                        else if (item.DepositAmountEmployee == null)
                        {
                            premium = (decimal)item.DepositAmountIndependent;
                        }
                        else
                        {
                            premium = (decimal)item.DepositAmountEmployee + (decimal)item.DepositAmountIndependent;
                        }
                        decimal yearPremium = 0;
                        decimal threeYearsPremium = 0;
                        decimal fiveYearsPremium = 0;
                        DateTime startDate = (DateTime)item.StartDate;
                        int totalMonths = ((nowDate.Year - startDate.Year) * 12) + (nowDate.Month + 1) - startDate.Month;
                        financeTotalPremium = financeTotalPremium + (premium * totalMonths);
                        if (item.FinanceProgramType.ProgramTypeName == "קופת גמל")
                        {
                            kupatGuemelPremium = kupatGuemelPremium + (premium * totalMonths);
                            kupatGuemelCount++;
                        }
                        else if (item.FinanceProgramType.ProgramTypeName == "קרן השתלמות")
                        {
                            kerenHishtalmutPremium = kerenHishtalmutPremium + (premium * totalMonths);
                            kerenHishtalmutCount++;
                        }
                        else if (item.FinanceProgramType.ProgramTypeName == "תיק השקעות")
                        {
                            invesmentPortfolioPremium = invesmentPortfolioPremium + (premium * totalMonths);
                            invesmentPortfolioCount++;
                        }
                        if (item.EndDate > nowDate)
                        {
                            if (totalMonths <= 12)
                            {
                                yearPremium = (premium * totalMonths);
                                financeLastYearPremium = financeLastYearPremium + yearPremium;
                            }
                            else
                            {
                                yearPremium = (premium * 12);
                                financeLastYearPremium = financeLastYearPremium + yearPremium;
                            }
                            if (item.FinanceProgramType.ProgramTypeName == "קופת גמל")
                            {
                                kupatGuemelLastYearPremium = kupatGuemelLastYearPremium + yearPremium;
                                kupatGuemelLastYearCount++;
                            }
                            else if (item.FinanceProgramType.ProgramTypeName == "קרן השתלמות")
                            {
                                kerenHishtalmutLastYearPremium = kerenHishtalmutLastYearPremium + yearPremium;
                                kerenHishtalmutLastYearCount++;
                            }
                            else if (item.FinanceProgramType.ProgramTypeName == "תיק השקעות")
                            {
                                invesmentPortfolioLastYearPremium = invesmentPortfolioLastYearPremium + yearPremium;
                                invesmentPortfolioLastYearCount++;
                            }
                        }
                        if (item.EndDate > nowDate.AddYears(-3))
                        {
                            if (totalMonths <= 36)
                            {
                                threeYearsPremium = (premium * totalMonths);
                                finance3YearPremium = finance3YearPremium + threeYearsPremium;
                            }
                            else
                            {
                                threeYearsPremium = (premium * 36);
                                finance3YearPremium = finance3YearPremium + threeYearsPremium;
                            }
                            if (item.FinanceProgramType.ProgramTypeName == "קופת גמל")
                            {
                                kupatGuemel3YearsPremium = kupatGuemel3YearsPremium + threeYearsPremium;
                                kupatGuemel3YearsCount++;
                            }
                            else if (item.FinanceProgramType.ProgramTypeName == "קרן השתלמות")
                            {
                                kerenHishtalmut3YearsPremium = kerenHishtalmut3YearsPremium + threeYearsPremium;
                                kerenHishtalmut3YearsCount++;
                            }
                            else if (item.FinanceProgramType.ProgramTypeName == "תיק השקעות")
                            {
                                invesmentPortfolio3YearsPremium = invesmentPortfolio3YearsPremium + threeYearsPremium;
                                invesmentPortfolio3YearsCount++;
                            }
                        }
                        if (item.EndDate > nowDate.AddYears(-5))
                        {
                            if (totalMonths <= 60)
                            {
                                fiveYearsPremium = (premium * totalMonths);
                                finance5YearPremium = finance5YearPremium + fiveYearsPremium;
                            }
                            else
                            {
                                fiveYearsPremium = (premium * 60);
                                finance5YearPremium = finance5YearPremium + fiveYearsPremium;
                            }
                            if (item.FinanceProgramType.ProgramTypeName == "קופת גמל")
                            {
                                kupatGuemel5YearsPremium = kupatGuemel5YearsPremium + fiveYearsPremium;
                                kupatGuemel5YearsCount++;
                            }
                            else if (item.FinanceProgramType.ProgramTypeName == "קרן השתלמות")
                            {
                                kerenHishtalmut5YearsPremium = kerenHishtalmut5YearsPremium + fiveYearsPremium;
                                kerenHishtalmut5YearsCount++;
                            }
                            else if (item.FinanceProgramType.ProgramTypeName == "תיק השקעות")
                            {
                                invesmentPortfolio5YearsPremium = invesmentPortfolio5YearsPremium + fiveYearsPremium;
                                invesmentPortfolio5YearsCount++;
                            }
                        }
                    }
                }
                lblKupatGuemelPoliciesCount.Content = kupatGuemelLastYearCount.ToString();
                lblKupatGuemelPremium.Content = kupatGuemelLastYearPremium.ToString("0.##");
                lblKupatGuemelPoliciesCount3.Content = kupatGuemel3YearsCount.ToString();
                lblKupatGuemelPremium3.Content = kupatGuemel3YearsPremium.ToString("0.##");
                lblKupatGuemelPoliciesCount5.Content = kupatGuemel5YearsCount.ToString();
                lblKupatGuemelPremium5.Content = kupatGuemel5YearsPremium.ToString("0.##");
                lblKupatGuemelPoliciesCountAll.Content = kupatGuemelCount.ToString();
                lblKupatGuemelPremiumAll.Content = kupatGuemelPremium.ToString("0.##");

                lblKerenHishtalmutPoliciesCount.Content = kerenHishtalmutLastYearCount.ToString();
                lblKerenHishtalmutPremium.Content = kerenHishtalmutLastYearPremium.ToString("0.##");
                lblKerenHishtalmutPoliciesCount3.Content = kerenHishtalmut3YearsCount.ToString();
                lblKerenHishtalmutPremium3.Content = kerenHishtalmut3YearsPremium.ToString("0.##");
                lblKerenHishtalmutPoliciesCount5.Content = kerenHishtalmut5YearsCount.ToString();
                lblKerenHishtalmutPremium5.Content = kerenHishtalmut5YearsPremium.ToString("0.##");
                lblKerenHishtalmutPoliciesCountAll.Content = kerenHishtalmutCount.ToString();
                lblKerenHishtalmutPremiumAll.Content = kerenHishtalmutPremium.ToString("0.##");

                lblInvesmentPortfolioPoliciesCount.Content = invesmentPortfolioLastYearCount.ToString();
                lblInvesmentPortfolioPremium.Content = invesmentPortfolioLastYearPremium.ToString("0.##");
                lblInvesmentPortfolioPoliciesCount3.Content = invesmentPortfolio3YearsCount.ToString();
                lblInvesmentPortfolioPremium3.Content = invesmentPortfolio3YearsPremium.ToString("0.##");
                lblInvesmentPortfolioPoliciesCount5.Content = invesmentPortfolio5YearsCount.ToString();
                lblInvesmentPortfolioPremium5.Content = invesmentPortfolio5YearsPremium.ToString("0.##");
                lblInvesmentPortfolioPoliciesCountAll.Content = invesmentPortfolioCount.ToString();
                lblInvesmentPortfolioPremiumAll.Content = invesmentPortfolioPremium.ToString("0.##");

            }
        }

        private void SetHealthInfo()
        {
            DateTime nowDate = DateTime.Now;
            var healthPolicies = healthLogic.GetAllHealthPoliciesByClient(client);

            if (healthPolicies != null && healthPolicies.Count > 0)
            {
                foreach (var item in healthPolicies)
                {
                    if (item.TotalMonthlyPayment != null)
                    {
                        decimal premium = (decimal)item.TotalMonthlyPayment;
                        DateTime startDate = (DateTime)item.StartDate;
                        int totalMonths = ((nowDate.Year - startDate.Year) * 12) + (nowDate.Month + 1) - startDate.Month;
                        healthTotalPremium = healthTotalPremium + (premium * totalMonths);
                        healthCount++;
                        if (item.EndDate > nowDate)
                        {
                            if (totalMonths <= 12)
                            {
                                healthLastYearPremium = healthLastYearPremium + (premium * totalMonths);
                            }
                            else
                            {
                                healthLastYearPremium = healthLastYearPremium + (premium * 12);
                            }
                            healthLastYearCount++;
                        }
                        if (item.EndDate > nowDate.AddYears(-3))
                        {
                            if (totalMonths <= 36)
                            {
                                health3YearPremium = health3YearPremium + (premium * totalMonths);
                            }
                            else
                            {
                                health3YearPremium = health3YearPremium + (premium * 36);
                            }
                            health3YearsCount++;
                        }
                        if (item.EndDate > nowDate.AddYears(-5))
                        {
                            if (totalMonths <= 60)
                            {
                                health5YearPremium = health5YearPremium + (premium * totalMonths);
                            }
                            else
                            {
                                health5YearPremium = health5YearPremium + (premium * 60);
                            }
                            health5YearsCount++;
                        }
                    }

                }
                var healthClaims = healthClaimLogic.GetAllHealthClaimsByClient(client.ClientID);
                healthClaimsCount = healthClaims.Count;
                healthLastYearClaimsCount = healthClaims.Where(c => c.EventDateAndTime > nowDate.AddYears(-1)).Count();
                health3YearsClaimsCount = healthClaims.Where(c => c.EventDateAndTime > nowDate.AddYears(-3)).Count();
                health5YearsClaimsCount = healthClaims.Where(c => c.EventDateAndTime > nowDate.AddYears(-5)).Count();

                if (healthClaims != null && healthClaims.Count > 0)
                {
                    var healthAmountedClaims = healthClaims.Where(c => c.AmountReceived != null);
                    healthTotalClaims = (decimal)healthAmountedClaims.Sum(c => c.AmountReceived);
                    healthLastYearClaims = (decimal)(healthAmountedClaims.Where(c => c.EventDateAndTime > nowDate.AddYears(-1)).Sum(c => c.AmountReceived));
                    health3YearsClaims = (decimal)(healthAmountedClaims.Where(c => c.EventDateAndTime > nowDate.AddYears(-3)).Sum(c => c.AmountReceived));
                    health5YearsClaims = (decimal)(healthAmountedClaims.Where(c => c.EventDateAndTime > nowDate.AddYears(-5)).Sum(c => c.AmountReceived));

                    lblHealthClaimsCount.Content = healthClaims.Where(c => c.EventDateAndTime > nowDate.AddYears(-1)).Count().ToString();
                    lblHealthClaimsAmount.Content = healthLastYearClaims.ToString("0.##");
                    lblHealthClaimsCount3.Content = healthClaims.Where(c => c.EventDateAndTime > nowDate.AddYears(-3)).Count().ToString();
                    lblHealthClaimsAmount3.Content = health3YearsClaims.ToString("0.##");
                    lblHealthClaimsCount5.Content = healthClaims.Where(c => c.EventDateAndTime > nowDate.AddYears(-5)).Count().ToString();
                    lblHealthClaimsAmount5.Content = health5YearsClaims.ToString("0.##");
                    lblHealthClaimsCountAll.Content = healthClaims.Count.ToString();
                    lblHealthClaimsAmountAll.Content = healthTotalClaims.ToString("0.##");
                }

                lblHealthPoliciesCount.Content = healthLastYearCount.ToString();
                lblHealthPremium.Content = healthLastYearPremium.ToString("0.##");
                lblHealthPoliciesCount3.Content = health3YearsCount.ToString();
                lblHealthPremium3.Content = health3YearPremium.ToString("0.##");
                lblHealthPoliciesCount5.Content = health5YearsCount.ToString();
                lblHealthPremium5.Content = health5YearPremium.ToString("0.##");
                lblHealthPoliciesCountAll.Content = healthCount.ToString();
                lblHealthPremiumAll.Content = healthTotalPremium.ToString("0.##");
            }
        }

        private void SetLifeInfo()
        {
            //List<LifePolicy> lastYearLifePolicies = null;
            DateTime nowDate = DateTime.Now;
            var lifePolicies = lifeLogic.GetAllLifePoliciesByClient(client);


            if (lifePolicies != null && lifePolicies.Count > 0)
            {
                foreach (var item in lifePolicies)
                {
                    if (item.TotalMonthlyPayment != null)
                    {
                        decimal premium = (decimal)item.TotalMonthlyPayment;
                        decimal yearPremium = 0;
                        decimal threeYearsPremium = 0;
                        decimal fiveYearsPremium = 0;
                        DateTime startDate = (DateTime)item.StartDate;
                        int totalMonths = ((nowDate.Year - startDate.Year) * 12) + (nowDate.Month + 1) - startDate.Month;
                        lifeTotalPremium = lifeTotalPremium + (premium * totalMonths);
                        if (item.LifeIndustry.LifeIndustryName == "מנהלים")
                        {
                            managersPremium = managersPremium + (premium * totalMonths);
                            managersCount++;
                        }
                        else if (item.LifeIndustry.LifeIndustryName == "משכנתא")
                        {
                            mortgagePremium = mortgagePremium + (premium * totalMonths);
                            mortgageCount++;
                        }
                        else if (item.LifeIndustry.LifeIndustryName == "פרט")
                        {
                            privatePremium = privatePremium + (premium * totalMonths);
                            privateCount++;
                        }
                        else if (item.LifeIndustry.LifeIndustryName == "פנסיה")
                        {
                            pensionPremium = pensionPremium + (premium * totalMonths);
                            pensionCount++;
                        }
                        else if (item.LifeIndustry.LifeIndustryName == "ריסק")
                        {
                            riskPremium = riskPremium + (premium * totalMonths);
                            riskCount++;
                        }
                        if (item.EndDate > nowDate)
                        {
                            if (totalMonths <= 12)
                            {
                                yearPremium = premium * totalMonths;
                                lifeLastYearPremium = lifeLastYearPremium + yearPremium;
                            }
                            else
                            {
                                yearPremium = premium * 12;
                                lifeLastYearPremium = lifeLastYearPremium + yearPremium;
                            }
                            if (item.LifeIndustry.LifeIndustryName == "מנהלים")
                            {
                                managersLastYearPremium = managersLastYearPremium + yearPremium;
                                managersLastYearCount++;
                            }
                            else if (item.LifeIndustry.LifeIndustryName == "משכנתא")
                            {
                                mortgageLastYearPremium = mortgageLastYearPremium + yearPremium;
                                mortgageLastYearCount++;
                            }
                            else if (item.LifeIndustry.LifeIndustryName == "פרט")
                            {
                                privateLastYearPremium = privateLastYearPremium + yearPremium;
                                privateLastYearCount++;
                            }
                            else if (item.LifeIndustry.LifeIndustryName == "פנסיה")
                            {
                                pensionLastYearPremium = pensionLastYearPremium + yearPremium;
                                pensionLastYearCount++;
                            }
                            else if (item.LifeIndustry.LifeIndustryName == "ריסק")
                            {
                                riskLastYearPremium = riskLastYearPremium + yearPremium;
                                riskLastYearCount++;
                            }
                        }
                        if (item.EndDate > nowDate.AddYears(-3))
                        {
                            if (totalMonths <= 36)
                            {
                                threeYearsPremium = premium * totalMonths;
                                life3YearPremium = life3YearPremium + threeYearsPremium;
                            }
                            else
                            {
                                threeYearsPremium = premium * 36;
                                life3YearPremium = life3YearPremium + threeYearsPremium;
                            }
                            if (item.LifeIndustry.LifeIndustryName == "מנהלים")
                            {
                                managers3YearsPremium = managers3YearsPremium + threeYearsPremium;
                                managers3YearsCount++;
                            }
                            else if (item.LifeIndustry.LifeIndustryName == "משכנתא")
                            {
                                mortgage3YearsPremium = mortgage3YearsPremium + threeYearsPremium;
                                mortgage3YearsCount++;
                            }
                            else if (item.LifeIndustry.LifeIndustryName == "פרט")
                            {
                                private3YearsPremium = private3YearsPremium + threeYearsPremium;
                                private3YearsCount++;
                            }
                            else if (item.LifeIndustry.LifeIndustryName == "פנסיה")
                            {
                                pension3YearsPremium = pension3YearsPremium + threeYearsPremium;
                                pension3YearsCount++;
                            }
                            else if (item.LifeIndustry.LifeIndustryName == "ריסק")
                            {
                                risk3YearsPremium = risk3YearsPremium + threeYearsPremium;
                                risk3YearsCount++;
                            }
                        }
                        if (item.EndDate > nowDate.AddYears(-5))
                        {
                            if (totalMonths <= 60)
                            {
                                fiveYearsPremium = premium * totalMonths;
                                life5YearPremium = life5YearPremium + fiveYearsPremium;
                            }
                            else
                            {
                                fiveYearsPremium = premium * 60;
                                life5YearPremium = life5YearPremium + fiveYearsPremium;
                            }
                            if (item.LifeIndustry.LifeIndustryName == "מנהלים")
                            {
                                managers5YearsPremium = managers5YearsPremium + fiveYearsPremium;
                                managers5YearsCount++;
                            }
                            else if (item.LifeIndustry.LifeIndustryName == "משכנתא")
                            {
                                mortgage5YearsPremium = mortgage5YearsPremium + fiveYearsPremium;
                                mortgage5YearsCount++;
                            }
                            else if (item.LifeIndustry.LifeIndustryName == "פרט")
                            {
                                private5YearsPremium = private5YearsPremium + fiveYearsPremium;
                                private5YearsCount++;
                            }
                            else if (item.LifeIndustry.LifeIndustryName == "פנסיה")
                            {
                                pension5YearsPremium = pension5YearsPremium + fiveYearsPremium;
                                pension5YearsCount++;
                            }
                            else if (item.LifeIndustry.LifeIndustryName == "ריסק")
                            {
                                risk5YearsPremium = risk5YearsPremium + fiveYearsPremium;
                                risk5YearsCount++;
                            }
                        }
                    }
                }
                var lifeClaims = lifeClaimLogic.GetAllLifeClaimsByClient(client.ClientID);
                lifeClaimsCount = lifeClaims.Count;
                lifeLastYearClaimsCount = lifeClaims.Where(c => c.EventDateAndTime > nowDate.AddYears(-1)).Count();
                life3YearsClaimsCount = lifeClaims.Where(c => c.EventDateAndTime > nowDate.AddYears(-3)).Count();
                life5YearsClaimsCount = lifeClaims.Where(c => c.EventDateAndTime > nowDate.AddYears(-5)).Count();

                if (lifeClaims != null && lifeClaims.Count > 0)
                {
                    var lifeAmountedClaims = lifeClaims.Where(c => c.AmountReceived != null);
                    lifeTotalClaims = (decimal)lifeAmountedClaims.Sum(c => c.AmountReceived);
                    lifeLastYearClaims = (decimal)(lifeAmountedClaims.Where(c => c.EventDateAndTime > nowDate.AddYears(-1)).Sum(c => c.AmountReceived));
                    life3YearsClaims = (decimal)(lifeAmountedClaims.Where(c => c.EventDateAndTime > nowDate.AddYears(-3)).Sum(c => c.AmountReceived));
                    life5YearsClaims = (decimal)(lifeAmountedClaims.Where(c => c.EventDateAndTime > nowDate.AddYears(-5)).Sum(c => c.AmountReceived));

                    var managersClaims = lifeClaims.Where(c => c.LifePolicy.LifeIndustry.LifeIndustryName == "מנהלים");
                    lblManagersClaimsAmount.Content = ((decimal)(managersClaims.Where(c => c.EventDateAndTime > nowDate.AddYears(-1) && c.AmountReceived != null).Sum(c => c.AmountReceived))).ToString("0.##");
                    lblManagersClaimsCount.Content = managersClaims.Where(c => c.EventDateAndTime > nowDate.AddYears(-1)).Count().ToString();
                    lblManagersClaimsAmount3.Content = ((decimal)(managersClaims.Where(c => c.EventDateAndTime > nowDate.AddYears(-3) && c.AmountReceived != null).Sum(c => c.AmountReceived))).ToString("0.##");
                    lblManagersClaimsCount3.Content = managersClaims.Where(c => c.EventDateAndTime > nowDate.AddYears(-3)).Count().ToString();
                    lblManagersClaimsAmount5.Content = ((decimal)(managersClaims.Where(c => c.EventDateAndTime > nowDate.AddYears(-5) && c.AmountReceived != null).Sum(c => c.AmountReceived))).ToString("0.##");
                    lblManagersClaimsCount5.Content = managersClaims.Where(c => c.EventDateAndTime > nowDate.AddYears(-5)).Count().ToString();
                    lblManagersClaimsAmountAll.Content = ((decimal)(managersClaims.Where(c => c.AmountReceived != null).Sum(c => c.AmountReceived))).ToString("0.##");
                    lblManagersClaimsCountAll.Content = managersClaims.Count().ToString();

                    var mortgageClaims = lifeClaims.Where(c => c.LifePolicy.LifeIndustry.LifeIndustryName == "משכנתא");
                    lblMortgageClaimsAmount.Content = ((decimal)(mortgageClaims.Where(c => c.EventDateAndTime > nowDate.AddYears(-1) && c.AmountReceived != null).Sum(c => c.AmountReceived))).ToString("0.##");
                    lblMortgageClaimsCount.Content = mortgageClaims.Where(c => c.EventDateAndTime > nowDate.AddYears(-1)).Count().ToString();
                    lblMortgageClaimsAmount3.Content = ((decimal)(mortgageClaims.Where(c => c.EventDateAndTime > nowDate.AddYears(-3) && c.AmountReceived != null).Sum(c => c.AmountReceived))).ToString("0.##");
                    lblMortgageClaimsCount3.Content = mortgageClaims.Where(c => c.EventDateAndTime > nowDate.AddYears(-3)).Count().ToString();
                    lblMortgageClaimsAmount5.Content = ((decimal)(mortgageClaims.Where(c => c.EventDateAndTime > nowDate.AddYears(-5) && c.AmountReceived != null).Sum(c => c.AmountReceived))).ToString("0.##");
                    lblMortgageClaimsCount5.Content = mortgageClaims.Where(c => c.EventDateAndTime > nowDate.AddYears(-5)).Count().ToString();
                    lblMortgageClaimsAmountAll.Content = ((decimal)(mortgageClaims.Where(c => c.AmountReceived != null).Sum(c => c.AmountReceived))).ToString("0.##");
                    lblMortgageClaimsCountAll.Content = mortgageClaims.Count().ToString();

                    var privateClaims = lifeClaims.Where(c => c.LifePolicy.LifeIndustry.LifeIndustryName == "פרט");
                    lblPrivateClaimsAmount.Content = ((decimal)(privateClaims.Where(c => c.EventDateAndTime > nowDate.AddYears(-1) && c.AmountReceived != null).Sum(c => c.AmountReceived))).ToString("0.##");
                    lblPrivateClaimsCount.Content = privateClaims.Where(c => c.EventDateAndTime > nowDate.AddYears(-1)).Count().ToString();
                    lblPrivateClaimsAmount3.Content = ((decimal)(privateClaims.Where(c => c.EventDateAndTime > nowDate.AddYears(-3) && c.AmountReceived != null).Sum(c => c.AmountReceived))).ToString("0.##");
                    lblPrivateClaimsCount3.Content = privateClaims.Where(c => c.EventDateAndTime > nowDate.AddYears(-3)).Count().ToString();
                    lblPrivateClaimsAmount5.Content = ((decimal)(privateClaims.Where(c => c.EventDateAndTime > nowDate.AddYears(-5) && c.AmountReceived != null).Sum(c => c.AmountReceived))).ToString("0.##");
                    lblPrivateClaimsCount5.Content = privateClaims.Where(c => c.EventDateAndTime > nowDate.AddYears(-5)).Count().ToString();
                    lblPrivateClaimsAmountAll.Content = ((decimal)(privateClaims.Where(c => c.AmountReceived != null).Sum(c => c.AmountReceived))).ToString("0.##");
                    lblPrivateClaimsCountAll.Content = privateClaims.Count().ToString();

                    var pensionClaims = lifeClaims.Where(c => c.LifePolicy.LifeIndustry.LifeIndustryName == "פנסיה");
                    lblPensionClaimsAmount.Content = ((decimal)(pensionClaims.Where(c => c.EventDateAndTime > nowDate.AddYears(-1) && c.AmountReceived != null).Sum(c => c.AmountReceived))).ToString("0.##");
                    lblPensionClaimsCount.Content = pensionClaims.Where(c => c.EventDateAndTime > nowDate.AddYears(-1)).Count().ToString();
                    lblPensionClaimsAmount3.Content = ((decimal)(pensionClaims.Where(c => c.EventDateAndTime > nowDate.AddYears(-3) && c.AmountReceived != null).Sum(c => c.AmountReceived))).ToString("0.##");
                    lblPensionClaimsCount3.Content = pensionClaims.Where(c => c.EventDateAndTime > nowDate.AddYears(-3)).Count().ToString();
                    lblPensionClaimsAmount5.Content = ((decimal)(pensionClaims.Where(c => c.EventDateAndTime > nowDate.AddYears(-5) && c.AmountReceived != null).Sum(c => c.AmountReceived))).ToString("0.##");
                    lblPensionClaimsCount5.Content = pensionClaims.Where(c => c.EventDateAndTime > nowDate.AddYears(-5)).Count().ToString();
                    lblPensionClaimsAmountAll.Content = ((decimal)(pensionClaims.Where(c => c.AmountReceived != null).Sum(c => c.AmountReceived))).ToString("0.##");
                    lblPensionClaimsCountAll.Content = pensionClaims.Count().ToString();

                    var riskClaims = lifeClaims.Where(c => c.LifePolicy.LifeIndustry.LifeIndustryName == "פנסיה");
                    lblRisksClaimsAmount.Content = ((decimal)(riskClaims.Where(c => c.EventDateAndTime > nowDate.AddYears(-1) && c.AmountReceived != null).Sum(c => c.AmountReceived))).ToString("0.##");
                    lblRisksClaimsCount.Content = riskClaims.Where(c => c.EventDateAndTime > nowDate.AddYears(-1)).Count().ToString();
                    lblRisksClaimsAmount3.Content = ((decimal)(riskClaims.Where(c => c.EventDateAndTime > nowDate.AddYears(-3) && c.AmountReceived != null).Sum(c => c.AmountReceived))).ToString("0.##");
                    lblRisksClaimsCount3.Content = riskClaims.Where(c => c.EventDateAndTime > nowDate.AddYears(-3)).Count().ToString();
                    lblRisksClaimsAmount5.Content = ((decimal)(riskClaims.Where(c => c.EventDateAndTime > nowDate.AddYears(-5) && c.AmountReceived != null).Sum(c => c.AmountReceived))).ToString("0.##");
                    lblRisksClaimsCount5.Content = riskClaims.Where(c => c.EventDateAndTime > nowDate.AddYears(-5)).Count().ToString();
                    lblRisksClaimsAmountAll.Content = ((decimal)(riskClaims.Where(c => c.AmountReceived != null).Sum(c => c.AmountReceived))).ToString("0.##");
                    lblRisksClaimsCountAll.Content = riskClaims.Count().ToString();
                }
            }

            lblManagersPoliciesCount.Content = managersLastYearCount.ToString();
            lblManagersPremia.Content = managersLastYearPremium.ToString("0.##");
            lblManagersPoliciesCount3.Content = managers3YearsCount.ToString();
            lblManagersPremia3.Content = managers3YearsPremium.ToString("0.##");
            lblManagersPoliciesCount5.Content = managers5YearsCount.ToString();
            lblManagersPremia5.Content = managers5YearsPremium.ToString("0.##");
            lblManagersPoliciesCountAll.Content = managersCount.ToString();
            lblManagersPremiaAll.Content = managersPremium.ToString("0.##");

            lblMortgagePoliciesCount.Content = mortgageLastYearCount.ToString();
            lblMortgagePremia.Content = mortgageLastYearPremium.ToString("0.##");
            lblMortgagePoliciesCount3.Content = mortgage3YearsCount.ToString();
            lblMortgagePremia3.Content = mortgage3YearsPremium.ToString("0.##");
            lblMortgagePoliciesCount5.Content = mortgage5YearsCount.ToString();
            lblMortgagePremia5.Content = mortgage5YearsPremium.ToString("0.##");
            lblMortgagePoliciesCountAll.Content = mortgageCount.ToString();
            lblMortgagePremiaAll.Content = mortgagePremium.ToString("0.##");

            lblPrivatePoliciesCount.Content = privateLastYearCount.ToString();
            lblPrivatePremia.Content = privateLastYearPremium.ToString("0.##");
            lblPrivatePoliciesCount3.Content = private3YearsCount.ToString();
            lblPrivatePremia3.Content = private3YearsPremium.ToString("0.##");
            lblPrivatePoliciesCount5.Content = private5YearsCount.ToString();
            lblPrivatePremia5.Content = private5YearsPremium.ToString("0.##");
            lblPrivatePoliciesCountAll.Content = privateCount.ToString();
            lblPrivatePremiaAll.Content = privatePremium.ToString("0.##");

            lblPensionPoliciesCount.Content = pensionLastYearCount.ToString();
            lblPensionPremia.Content = pensionLastYearPremium.ToString("0.##");
            lblPensionPoliciesCount3.Content = pension3YearsCount.ToString();
            lblPensionPremia3.Content = pension3YearsPremium.ToString("0.##");
            lblPensionPoliciesCount5.Content = pension5YearsCount.ToString();
            lblPensionPremia5.Content = pension5YearsPremium.ToString("0.##");
            lblPensionPoliciesCountAll.Content = pensionCount.ToString();
            lblPensionPremiaAll.Content = pensionPremium.ToString("0.##");

            lblRisksPoliciesCount.Content = riskLastYearCount.ToString();
            lblRisksPremia.Content = riskLastYearPremium.ToString("0.##");
            lblRisksPoliciesCount3.Content = risk3YearsCount.ToString();
            lblRisksPremia3.Content = risk3YearsPremium.ToString("0.##");
            lblRisksPoliciesCount5.Content = risk5YearsCount.ToString();
            lblRisksPremia5.Content = risk5YearsPremium.ToString("0.##");
            lblRisksPoliciesCountAll.Content = riskCount.ToString();
            lblRisksPremiaAll.Content = riskPremium.ToString("0.##");
        }

        private void SetElementaryInfo()
        {
            List<ElementaryPolicy> lastYearElementaryPolicies = null;
            List<ElementaryPolicy> threeYearsElementaryPolicies = null;
            List<ElementaryPolicy> fiveYearsElementaryPolicies = null;
            List<ElementaryClaim> threeYearsElementaryClaims = null;
            List<ElementaryClaim> fiveYearsElementaryClaims = null;
            List<ElementaryClaim> lastYearElementaryClaims = new List<ElementaryClaim>();

            var elementaryPolicies = elementaryLogic.GetAllElementaryPoliciesByClient(client);
            if (elementaryPolicies != null && elementaryPolicies.Count > 0)
            {
                elementaryTotalPremium = (decimal)elementaryPolicies.Sum(p => p.TotalPremium);
                lastYearElementaryPolicies = elementaryPolicies.Where(p => p.EndDate > DateTime.Now).ToList();
                if (lastYearElementaryPolicies.Count > 0)
                {
                    elementaryLastYearPremium = (decimal)lastYearElementaryPolicies.Sum(p => p.TotalPremium);
                }
                threeYearsElementaryPolicies = elementaryPolicies.Where(p => p.EndDate >= DateTime.Now.AddYears(-3)).ToList();
                if (threeYearsElementaryPolicies.Count > 0)
                {
                    elementary3YearPremium = (decimal)threeYearsElementaryPolicies.Sum(p => p.TotalPremium);
                }
                fiveYearsElementaryPolicies = elementaryPolicies.Where(p => p.EndDate >= DateTime.Now.AddYears(-5)).ToList();
                if (fiveYearsElementaryPolicies.Count > 0)
                {
                    elementary5YearPremium = (decimal)fiveYearsElementaryPolicies.Sum(p => p.TotalPremium);
                }
            }

            var elementaryClaims = elementaryClaimLogic.GetAllElementaryClaimsByClient(client.ClientID);
            elementaryClaimCount = elementaryClaims.Count;
            if (elementaryClaims != null && elementaryClaims.Count > 0)
            {
                var elementaryAmountedClaims = elementaryClaims.Where(c => c.AmountReceived != null);
                elementaryTotalClaims = (decimal)elementaryAmountedClaims.Sum(c => c.AmountReceived);
                //List<ElementaryClaim> lastYearElementaryClaims = new List<ElementaryClaim>();
                if (lastYearElementaryPolicies != null)
                {
                    foreach (var item in lastYearElementaryPolicies)
                    {
                        var policyClaims = elementaryClaims.Where(c => c.ElementaryPolicyID == item.ElementaryPolicyID);
                        if (policyClaims != null && policyClaims.Count() > 0)
                        {
                            foreach (var claim in policyClaims)
                            {
                                lastYearElementaryClaims.Add(claim);
                            }
                        }
                    }
                }
                elementaryLastYearClaimsCount = lastYearElementaryClaims.Count;
                if (lastYearElementaryClaims.Count > 0)
                {
                    var lastYearAmountedClaims = lastYearElementaryClaims.Where(c => c.AmountReceived != null).ToList();
                    elementaryLastYearClaims = (decimal)lastYearAmountedClaims.Sum(c => c.AmountReceived);
                }
                threeYearsElementaryClaims = elementaryClaims.Where(c => c.EventDate >= DateTime.Now.AddYears(-3)).ToList();
                elementary3YearsClaimsCount = threeYearsElementaryClaims.Count;
                if (threeYearsElementaryClaims.Count > 0)
                {
                    var threeYearsAmountedClaims = threeYearsElementaryClaims.Where(c => c.AmountReceived != null).ToList();
                    elementary3YearsClaims = (decimal)threeYearsAmountedClaims.Sum(c => c.AmountReceived);
                }
                fiveYearsElementaryClaims = elementaryClaims.Where(c => c.EventDate >= DateTime.Now.AddYears(-5)).ToList();
                elementary5YearsClaimsCount = fiveYearsElementaryClaims.Count;
                if (fiveYearsElementaryClaims.Count > 0)
                {
                    var fiveYearsAmountedClaims = fiveYearsElementaryClaims.Where(c => c.AmountReceived != null).ToList();
                    elementary5YearsClaims = (decimal)fiveYearsAmountedClaims.Sum(c => c.AmountReceived);
                }
            }

            if (lastYearElementaryPolicies != null)
            {
                var lastYearJobaPolicies = lastYearElementaryPolicies.Where(p => p.InsuranceIndustry.InsuranceIndustryName == "רכב חובה").ToList();
                jobaLastYearCount = lastYearJobaPolicies.Count;
                lblChobaCarPoliciesCount.Content = jobaLastYearCount.ToString();
                jobaLastYearPremium = (decimal)(lastYearJobaPolicies.Sum(p => p.TotalPremium));
                lblChobaCarPremia.Content = jobaLastYearPremium.ToString("0.##");

                var lastYearCarPolicies = lastYearElementaryPolicies.Where(p => p.InsuranceIndustry.InsuranceIndustryName == "רכב").ToList();
                carLastYearCount = lastYearCarPolicies.Count;
                lblMekifCarPoliciesCount.Content = carLastYearCount.ToString();
                carLastYearPremium = (decimal)(lastYearCarPolicies.Sum(p => p.TotalPremium));
                lblMekifCarPremia.Content = carLastYearPremium.ToString("0.##");

                var lastYearJabilaPolicies = lastYearElementaryPolicies.Where(p => p.InsuranceIndustry.InsuranceIndustryName == "רכב - חבילה").ToList();
                jabilaLastYearCount = lastYearJabilaPolicies.Count;
                lblJabilaCarPoliciesCount.Content = jabilaLastYearCount.ToString();
                jabilaLastYearPremium = (decimal)(lastYearJabilaPolicies.Sum(p => p.TotalPremium));
                lblJabilaCarPremia.Content = jabilaLastYearPremium.ToString("0.##");

                var lastYearAptPolicies = lastYearElementaryPolicies.Where(p => p.InsuranceIndustry.InsuranceIndustryName == "דירה").ToList();
                aptLastYearCount = lastYearAptPolicies.Count;
                lblApartmentPoliciesCount.Content = aptLastYearCount.ToString();
                aptLastYearPremium = (decimal)(lastYearAptPolicies.Sum(p => p.TotalPremium));
                lblApartmentPremia.Content = aptLastYearPremium.ToString("0.##");

                var lastYearBusinessPolicies = lastYearElementaryPolicies.Where(p => p.InsuranceIndustry.InsuranceIndustryName == "עסק").ToList();
                businessLastYearCount = lastYearBusinessPolicies.Count;
                lblBusinessPoliciesCount.Content = businessLastYearCount.ToString();
                businessLastYearPremium = (decimal)(lastYearBusinessPolicies.Sum(p => p.TotalPremium));
                lblBusinessPremia.Content = businessLastYearPremium.ToString("0.##");

            }
            if (lastYearElementaryClaims != null)
            {
                var lastYearJobaClaims = lastYearElementaryClaims.Where(c => c.ElementaryPolicy.InsuranceIndustry.InsuranceIndustryName == "רכב חובה").ToList();
                lblChobaCarClaimsCount.Content = lastYearJobaClaims.Count.ToString();
                lblChobaCarClaimsAmount.Content = ((decimal)(lastYearJobaClaims.Where(c => c.AmountReceived != null).Sum(c => c.AmountReceived))).ToString("0.##");

                var lastYearCarClaims = lastYearElementaryClaims.Where(c => c.ElementaryPolicy.InsuranceIndustry.InsuranceIndustryName == "רכב").ToList();
                lblMekifCarClaimsCount.Content = lastYearCarClaims.Count.ToString();
                lblMekifCarClaimsAmount.Content = ((decimal)(lastYearCarClaims.Where(c => c.AmountReceived != null).Sum(c => c.AmountReceived))).ToString("0.##");

                var lastYearJabilaClaims = lastYearElementaryClaims.Where(c => c.ElementaryPolicy.InsuranceIndustry.InsuranceIndustryName == "רכב - חבילה").ToList();
                lblJabilaCarClaimsCount.Content = lastYearJabilaClaims.Count.ToString();
                lblJabilaCarClaimsAmount.Content = ((decimal)(lastYearJabilaClaims.Where(c => c.AmountReceived != null).Sum(c => c.AmountReceived))).ToString("0.##");

                var lastYearAptClaims = lastYearElementaryClaims.Where(c => c.ElementaryPolicy.InsuranceIndustry.InsuranceIndustryName == "דירה").ToList();
                lblApartmentClaimsCount.Content = lastYearAptClaims.Count.ToString();
                lblApartmentClaimsAmount.Content = ((decimal)(lastYearAptClaims.Where(c => c.AmountReceived != null).Sum(c => c.AmountReceived))).ToString("0.##");

                var lastYearBusinessClaims = lastYearElementaryClaims.Where(c => c.ElementaryPolicy.InsuranceIndustry.InsuranceIndustryName == "עסק").ToList();
                lblBusinessClaimsCount.Content = lastYearBusinessClaims.Count.ToString();
                lblBusinessClaimsAmount.Content = ((decimal)(lastYearBusinessClaims.Where(c => c.AmountReceived != null).Sum(c => c.AmountReceived))).ToString("0.##");

            }
            if (threeYearsElementaryPolicies != null)
            {
                var threeYearsJobaPolicies = threeYearsElementaryPolicies.Where(p => p.InsuranceIndustry.InsuranceIndustryName == "רכב חובה").ToList();
                joba3YearsCount = threeYearsJobaPolicies.Count;
                lblChobaCarPoliciesCount3.Content = joba3YearsCount.ToString();
                joba3YearsPremium = (decimal)(threeYearsJobaPolicies.Sum(p => p.TotalPremium));
                lblChobaCarPremia3.Content = joba3YearsPremium.ToString("0.##");

                var threeYearsCarPolicies = threeYearsElementaryPolicies.Where(p => p.InsuranceIndustry.InsuranceIndustryName == "רכב").ToList();
                car3YearsCount = threeYearsCarPolicies.Count;
                lblMekifCarPoliciesCount3.Content = car3YearsCount.ToString();
                car3YearsPremium = (decimal)(threeYearsCarPolicies.Sum(p => p.TotalPremium));
                lblMekifCarPremia3.Content = car3YearsPremium.ToString("0.##");

                var threeYearsJabilaPolicies = threeYearsElementaryPolicies.Where(p => p.InsuranceIndustry.InsuranceIndustryName == "רכב - חבילה").ToList();
                jabila3YearsCount = threeYearsJabilaPolicies.Count;
                lblJabilaCarPoliciesCount3.Content = jabila3YearsCount.ToString();
                jabila3YearsPremium = (decimal)(threeYearsJabilaPolicies.Sum(p => p.TotalPremium));
                lblJabilaCarPremia3.Content = jabila3YearsPremium.ToString("0.##");

                var threeYearsAptPolicies = threeYearsElementaryPolicies.Where(p => p.InsuranceIndustry.InsuranceIndustryName == "דירה").ToList();
                apt3YearsCount = threeYearsAptPolicies.Count;
                lblApartmentPoliciesCount3.Content = apt3YearsCount.ToString();
                apt3YearsPremium = (decimal)(threeYearsAptPolicies.Sum(p => p.TotalPremium));
                lblApartmentPremia3.Content = apt3YearsPremium.ToString("0.##");

                var threeYearsBusinessPolicies = threeYearsElementaryPolicies.Where(p => p.InsuranceIndustry.InsuranceIndustryName == "עסק").ToList();
                business3YearsCount = threeYearsBusinessPolicies.Count;
                lblBusinessPoliciesCount3.Content = business3YearsCount.ToString();
                business3YearsPremium = (decimal)(threeYearsBusinessPolicies.Sum(p => p.TotalPremium));
                lblBusinessPremia3.Content = business3YearsPremium.ToString("0.##");

            }
            if (threeYearsElementaryClaims != null)
            {
                var threeYearsJobaClaims = threeYearsElementaryClaims.Where(c => c.ElementaryPolicy.InsuranceIndustry.InsuranceIndustryName == "רכב חובה").ToList();
                lblChobaCarClaimsCount3.Content = threeYearsJobaClaims.Count.ToString();
                lblChobaCarClaimsAmount3.Content = ((decimal)(threeYearsJobaClaims.Where(c => c.AmountReceived != null).Sum(c => c.AmountReceived))).ToString("0.##");

                var threeYearsCarClaims = threeYearsElementaryClaims.Where(c => c.ElementaryPolicy.InsuranceIndustry.InsuranceIndustryName == "רכב").ToList();
                lblMekifCarClaimsCount3.Content = threeYearsCarClaims.Count.ToString();
                lblMekifCarClaimsAmount3.Content = ((decimal)(threeYearsCarClaims.Where(c => c.AmountReceived != null).Sum(c => c.AmountReceived))).ToString("0.##");

                var threeYearsJabilaClaims = threeYearsElementaryClaims.Where(c => c.ElementaryPolicy.InsuranceIndustry.InsuranceIndustryName == "רכב - חבילה").ToList();
                lblJabilaCarClaimsCount3.Content = threeYearsJabilaClaims.Count.ToString();
                lblJabilaCarClaimsAmount3.Content = ((decimal)(threeYearsJabilaClaims.Where(c => c.AmountReceived != null).Sum(c => c.AmountReceived))).ToString("0.##");

                var threeYearsAptClaims = threeYearsElementaryClaims.Where(c => c.ElementaryPolicy.InsuranceIndustry.InsuranceIndustryName == "דירה").ToList();
                lblApartmentClaimsCount3.Content = threeYearsAptClaims.Count.ToString();
                lblApartmentClaimsAmount3.Content = ((decimal)(threeYearsAptClaims.Where(c => c.AmountReceived != null).Sum(c => c.AmountReceived))).ToString("0.##");

                var threeYearsBusinessClaims = threeYearsElementaryClaims.Where(c => c.ElementaryPolicy.InsuranceIndustry.InsuranceIndustryName == "עסק").ToList();
                lblBusinessClaimsCount3.Content = threeYearsBusinessClaims.Count.ToString();
                lblBusinessClaimsAmount3.Content = ((decimal)(threeYearsBusinessClaims.Where(c => c.AmountReceived != null).Sum(c => c.AmountReceived))).ToString("0.##");

            }
            if (fiveYearsElementaryPolicies != null)
            {
                var fiveYearsJobaPolicies = fiveYearsElementaryPolicies.Where(p => p.InsuranceIndustry.InsuranceIndustryName == "רכב חובה").ToList();
                joba5YearsCount = fiveYearsJobaPolicies.Count;
                lblChobaCarPoliciesCount5.Content = joba5YearsCount.ToString();
                joba5YearsPremium = (decimal)(fiveYearsJobaPolicies.Sum(p => p.TotalPremium));
                lblChobaCarPremia5.Content = joba5YearsPremium.ToString("0.##");

                var fiveYearsCarPolicies = fiveYearsElementaryPolicies.Where(p => p.InsuranceIndustry.InsuranceIndustryName == "רכב").ToList();
                car5YearsCount = fiveYearsCarPolicies.Count;
                lblMekifCarPoliciesCount5.Content = car5YearsCount.ToString();
                car5YearsPremium = (decimal)(fiveYearsCarPolicies.Sum(p => p.TotalPremium));
                lblMekifCarPremia5.Content = car5YearsPremium.ToString("0.##");

                var fiveYearsJabilaPolicies = fiveYearsElementaryPolicies.Where(p => p.InsuranceIndustry.InsuranceIndustryName == "רכב - חבילה").ToList();
                jabila5YearsCount = fiveYearsJabilaPolicies.Count;
                lblJabilaCarPoliciesCount5.Content = jabila5YearsCount.ToString();
                jabila5YearsPremium = (decimal)(fiveYearsJabilaPolicies.Sum(p => p.TotalPremium));
                lblJabilaCarPremia5.Content = jabila5YearsPremium.ToString("0.##");

                var fiveYearsAptPolicies = fiveYearsElementaryPolicies.Where(p => p.InsuranceIndustry.InsuranceIndustryName == "דירה").ToList();
                apt5YearsCount = fiveYearsAptPolicies.Count;
                lblApartmentPoliciesCount5.Content = apt5YearsCount.ToString();
                apt5YearsPremium = (decimal)(fiveYearsAptPolicies.Sum(p => p.TotalPremium));
                lblApartmentPremia5.Content = apt5YearsPremium.ToString("0.##");

                var fiveYearsBusinessPolicies = fiveYearsElementaryPolicies.Where(p => p.InsuranceIndustry.InsuranceIndustryName == "עסק").ToList();
                business5YearsCount = fiveYearsBusinessPolicies.Count;
                lblBusinessPoliciesCount5.Content = business5YearsCount.ToString();
                business5YearsPremium = (decimal)(fiveYearsBusinessPolicies.Sum(p => p.TotalPremium));
                lblBusinessPremia5.Content = business5YearsPremium.ToString("0.##");

            }
            if (fiveYearsElementaryClaims != null)
            {
                var fiveYearsJobaClaims = fiveYearsElementaryClaims.Where(c => c.ElementaryPolicy.InsuranceIndustry.InsuranceIndustryName == "רכב חובה").ToList();
                lblChobaCarClaimsCount5.Content = fiveYearsJobaClaims.Count.ToString();
                lblChobaCarClaimsAmount5.Content = ((decimal)(fiveYearsJobaClaims.Where(c => c.AmountReceived != null).Sum(c => c.AmountReceived))).ToString("0.##");

                var fiveYearsCarClaims = fiveYearsElementaryClaims.Where(c => c.ElementaryPolicy.InsuranceIndustry.InsuranceIndustryName == "רכב").ToList();
                lblMekifCarClaimsCount5.Content = fiveYearsCarClaims.Count.ToString();
                lblMekifCarClaimsAmount5.Content = ((decimal)(fiveYearsCarClaims.Where(c => c.AmountReceived != null).Sum(c => c.AmountReceived))).ToString("0.##");

                var fiveYearsJabilaClaims = fiveYearsElementaryClaims.Where(c => c.ElementaryPolicy.InsuranceIndustry.InsuranceIndustryName == "רכב - חבילה").ToList();
                lblJabilaCarClaimsCount5.Content = fiveYearsJabilaClaims.Count.ToString();
                lblJabilaCarClaimsAmount5.Content = ((decimal)(fiveYearsJabilaClaims.Where(c => c.AmountReceived != null).Sum(c => c.AmountReceived))).ToString("0.##");

                var fiveYearsAptClaims = fiveYearsElementaryClaims.Where(c => c.ElementaryPolicy.InsuranceIndustry.InsuranceIndustryName == "דירה").ToList();
                lblApartmentClaimsCount5.Content = fiveYearsAptClaims.Count.ToString();
                lblApartmentClaimsAmount5.Content = ((decimal)(fiveYearsAptClaims.Where(c => c.AmountReceived != null).Sum(c => c.AmountReceived))).ToString("0.##");

                var fiveYearsBusinessClaims = fiveYearsElementaryClaims.Where(c => c.ElementaryPolicy.InsuranceIndustry.InsuranceIndustryName == "עסק").ToList();
                lblBusinessClaimsCount5.Content = fiveYearsBusinessClaims.Count.ToString();
                lblBusinessClaimsAmount5.Content = ((decimal)(fiveYearsBusinessClaims.Where(c => c.AmountReceived != null).Sum(c => c.AmountReceived))).ToString("0.##");

            }
            if (elementaryPolicies != null)
            {
                var jobaPolicies = elementaryPolicies.Where(p => p.InsuranceIndustry.InsuranceIndustryName == "רכב חובה").ToList();
                jobaCount = jobaPolicies.Count;
                lblChobaCarPoliciesCountAll.Content = jobaCount.ToString();
                jobaPremium = (decimal)(jobaPolicies.Sum(p => p.TotalPremium));
                lblChobaCarPremiaAll.Content = jobaPremium.ToString("0.##");

                var carPolicies = elementaryPolicies.Where(p => p.InsuranceIndustry.InsuranceIndustryName == "רכב").ToList();
                carCount = carPolicies.Count;
                lblMekifCarPoliciesCountAll.Content = carCount.ToString();
                carPremium = (decimal)(carPolicies.Sum(p => p.TotalPremium));
                lblMekifCarPremiaAll.Content = carPremium.ToString("0.##");

                var jabilaPolicies = elementaryPolicies.Where(p => p.InsuranceIndustry.InsuranceIndustryName == "רכב - חבילה").ToList();
                jabilaCount = jabilaPolicies.Count;
                lblJabilaCarPoliciesCountAll.Content = jabilaCount.ToString();
                jabilaPremium = (decimal)(jabilaPolicies.Sum(p => p.TotalPremium));
                lblJabilaCarPremiaAll.Content = jabilaPremium.ToString("0.##");

                var aptPolicies = elementaryPolicies.Where(p => p.InsuranceIndustry.InsuranceIndustryName == "דירה").ToList();
                aptCount = aptPolicies.Count;
                lblApartmentPoliciesCountAll.Content = aptCount.ToString();
                aptPremium = (decimal)(aptPolicies.Sum(p => p.TotalPremium));
                lblApartmentPremiaAll.Content = aptPremium.ToString("0.##");

                var businessPolicies = elementaryPolicies.Where(p => p.InsuranceIndustry.InsuranceIndustryName == "עסק").ToList();
                businessCount = businessPolicies.Count;
                lblBusinessPoliciesCountAll.Content = businessCount.ToString();
                businessPremium = (decimal)(businessPolicies.Sum(p => p.TotalPremium));
                lblBusinessPremiaAll.Content = businessPremium.ToString("0.##");
            }
            if (elementaryClaims != null)
            {
                var jobaClaims = elementaryClaims.Where(c => c.ElementaryPolicy.InsuranceIndustry.InsuranceIndustryName == "רכב חובה").ToList();
                lblChobaCarClaimsCountAll.Content = jobaClaims.Count.ToString();
                lblChobaCarClaimsAmountAll.Content = ((decimal)(jobaClaims.Where(c => c.AmountReceived != null).Sum(c => c.AmountReceived))).ToString("0.##");

                var carClaims = elementaryClaims.Where(c => c.ElementaryPolicy.InsuranceIndustry.InsuranceIndustryName == "רכב").ToList();
                lblMekifCarClaimsCountAll.Content = carClaims.Count.ToString();
                lblMekifCarClaimsAmountAll.Content = ((decimal)(carClaims.Where(c => c.AmountReceived != null).Sum(c => c.AmountReceived))).ToString("0.##");

                var jabilaClaims = elementaryClaims.Where(c => c.ElementaryPolicy.InsuranceIndustry.InsuranceIndustryName == "רכב - חבילה").ToList();
                lblJabilaCarClaimsCountAll.Content = jabilaClaims.Count.ToString();
                lblJabilaCarClaimsAmountAll.Content = ((decimal)(jabilaClaims.Where(c => c.AmountReceived != null).Sum(c => c.AmountReceived))).ToString("0.##");

                var aptClaims = elementaryClaims.Where(c => c.ElementaryPolicy.InsuranceIndustry.InsuranceIndustryName == "דירה").ToList();
                lblApartmentClaimsCountAll.Content = aptClaims.Count.ToString();
                lblApartmentClaimsAmountAll.Content = ((decimal)(aptClaims.Where(c => c.AmountReceived != null).Sum(c => c.AmountReceived))).ToString("0.##");

                var businessClaims = elementaryClaims.Where(c => c.ElementaryPolicy.InsuranceIndustry.InsuranceIndustryName == "עסק").ToList();
                lblBusinessClaimsCountAll.Content = businessClaims.Count.ToString();
                lblBusinessClaimsAmountAll.Content = ((decimal)(businessClaims.Where(c => c.AmountReceived != null).Sum(c => c.AmountReceived))).ToString("0.##");

            }
        }

        private void ClearClientChart()
        {
            ttElementary.Content = "אין ללקוח פוליסות אלמנטרי";
            ttElementary.Foreground = Brushes.Red;
            ttApartment.Content = "אין ללקוח פוליסות דירה";
            ttApartment.Foreground = Brushes.Red;
            ttBusiness.Content = "אין ללקוח פוליסות עסק";
            ttBusiness.Foreground = Brushes.Red;
            ttCar.Content = "אין ללקוח פוליסות רכב";
            ttCar.Foreground = Brushes.Red;
            ttFinance.Content = "אין ללקוח פוליסות פיננסים";
            ttFinance.Foreground = Brushes.Red;
            ttHealth.Content = "אין ללקוח פוליסות בריאות";
            ttHealth.Foreground = Brushes.Red;
            ttInvesmentPortfolio.Content = "אין ללקוח תיק השקעות";
            ttInvesmentPortfolio.Foreground = Brushes.Red;
            ttKerenHishtalmut.Content = "אין ללקוח קרן השתלמות";
            ttKerenHishtalmut.Foreground = Brushes.Red;
            ttKupatGuemel.Content = "אין ללקוח קופת גמל";
            ttKupatGuemel.Foreground = Brushes.Red;
            ttLife.Content = "אין ללקוח פוליסות חיים";
            ttLife.Foreground = Brushes.Red;
            ttManagers.Content = "אין ללקוח פוליסות מנהלים";
            ttManagers.Foreground = Brushes.Red;
            ttMortgage.Content = "אין ללקוח פוליסות משכנתא";
            ttMortgage.Foreground = Brushes.Red;
            ttPension.Content = "אין ללקוח פוליסות פנסיה";
            ttPension.Foreground = Brushes.Red;
            ttPersonalAccidents.Content = "אין ללקוח פוליסות תאונות אישיות";
            ttPersonalAccidents.Foreground = Brushes.Red;
            ttPrivate.Content = "אין ללקוח פוליסות פרט";
            ttPrivate.Foreground = Brushes.Red;
            ttRisks.Content = "אין ללקוח פוליסות ריסק";
            ttRisks.Foreground = Brushes.Red;
            ttTravel.Content = "אין ללקוח פוליסות חו''ל";
            ttTravel.Foreground = Brushes.Red;
            ttForeingWorkers.Content = "אין ללקוח פוליסות עובדים זרים";
            ttForeingWorkers.Foreground = Brushes.Red;
            ttElementaryClaims.Content = "אין ללקוח תביעות אלמנטרי";
            ttElementaryClaims.ClearValue(ForegroundProperty);
            ttLifeClaims.Content = "אין ללקוח תביעות חיים";
            ttLifeClaims.ClearValue(ForegroundProperty);
            ttHealthClaims.Content = "אין ללקוח תביעות בריאות";
            ttHealthClaims.ClearValue(ForegroundProperty);
            ttTravelClaims.Content = "אין ללקוח תביעות חו''ל";
            ttTravelClaims.ClearValue(ForegroundProperty);
            ttClaims.Content = "אין ללקוח תביעות";
            ttClaims.ClearValue(ForegroundProperty);
            ttForeingWorkersClaims.Content = "אין ללקוח תביעות עובדים זרים";
            ttForeingWorkersClaims.ClearValue(ForegroundProperty);
            ttOffers.Content = "אין ללקוח הצעות פתוחות";
            ttOffers.Foreground = Brushes.Red;
            ttElementaryOffers.Content = "אין ללקוח הצעות אלמנטרי";
            ttElementaryOffers.Foreground = Brushes.Red;
            ttLifeOffers.Content = "אין ללקוח הצעות חיים";
            ttLifeOffers.Foreground = Brushes.Red;
            ttHealthOffers.Content = "אין ללקוח הצעות בריאות";
            ttHealthOffers.Foreground = Brushes.Red;
            ttFinanceOffers.Content = "אין ללקוח הצעות פיננסים";
            ttFinanceOffers.Foreground = Brushes.Red;
            ttTravelOffers.Content = "אין ללקוח הצעות חו''ל";
            ttTravelOffers.Foreground = Brushes.Red;
            ttForeingWorkersOffers.Content = "אין ללקוח הצעות עובדים זרים";
            ttForeingWorkersOffers.Foreground = Brushes.Red;
            brCar.Background = Brushes.White;
            brApartment.Background = Brushes.White;
            brBusiness.Background = Brushes.White;
            brHealth.Background = Brushes.White;
            brInvesmentPortfolio.Background = Brushes.White;
            brKerenHishtalmut.Background = Brushes.White;
            brKupatGuemel.Background = Brushes.White;
            brManagers.Background = Brushes.White;
            brMortgage.Background = Brushes.White;
            brPension.Background = Brushes.White;
            brPersonalAccidents.Background = Brushes.White;
            brPrivate.Background = Brushes.White;
            brRisks.Background = Brushes.White;
            brTravel.Background = Brushes.White;
            brElementaryClaims.Background = Brushes.White;
            brElementaryOffers.Background = Brushes.White;
            brLifeClaims.Background = Brushes.White;
            brLifeOffers.Background = Brushes.White;
            brHealthClaims.Background = Brushes.White;
            brHealthOffers.Background = Brushes.White;
            brFinanceOffers.Background = Brushes.White;
            brTravelOffers.Background = Brushes.White;
            lblCar.Foreground = darkBlue;
            lblApartment.Foreground = darkBlue;
            lblBusiness.Foreground = darkBlue;
            lblHealth.Foreground = darkBlue;
            lblInvesmentFolder.Foreground = darkBlue;
            lblKerenHishtalmut.Foreground = darkBlue;
            lblKupatGuemel.Foreground = darkBlue;
            lblManagers.Foreground = darkBlue;
            lblMortgage.Foreground = darkBlue;
            lblPension.Foreground = darkBlue;
            lblPersonalAccidents.Foreground = darkBlue;
            lblPrivate.Foreground = darkBlue;
            lblRisk.Foreground = darkBlue;
            lblForeingWorkers.Foreground = darkBlue;
            lblTravel.Foreground = darkBlue;
        }

        private void DisplayProfitabilityData()
        {
            if (this.IsLoaded)
            {
                ClearClientChart();
                if (tbItemLastYear.IsSelected)
                {
                    txtElementaryPremium.Text = elementaryLastYearPremium.ToString("0.##");
                    txtElementaryClaims.Text = elementaryLastYearClaims.ToString("0.##");
                    txtLifePremium.Text = lifeLastYearPremium.ToString("0.##");
                    txtLifeClaims.Text = lifeLastYearClaims.ToString("0.##");
                    txtHealthPremium.Text = healthLastYearPremium.ToString("0.##");
                    txtHealthClaims.Text = healthLastYearClaims.ToString("0.##");
                    txtPersonalAccidentsPremium.Text = personalAccidentsLastYearPremium.ToString("0.##");
                    txtPersonalAccidentsClaims.Text = personalAccidentsLastYearClaims.ToString("0.##");
                    txtFinancePremium.Text = financeLastYearPremium.ToString("0.##");
                    txtTravelPremium.Text = travelLastYearPremium.ToString("0.##");
                    txtTravelClaims.Text = travelLastYearClaims.ToString("0.##");
                    txtForeingWorkersPremium.Text = foreingWorkersLastYearPremium.ToString("0.##");
                    txtForeingWorkersClaims.Text = foreingWorkersLastYearClaims.ToString("0.##");

                    if (elementaryLastYearPremium > 0)
                    {
                        int elementaryLastYearCount = jobaLastYearCount + carLastYearCount + jabilaLastYearCount + aptLastYearCount + businessLastYearCount;
                        ttElementary.Content = string.Format("סה''כ פוליסות: {0}\nסה''כ פרמיה: {1}", elementaryLastYearCount.ToString(), elementaryLastYearPremium.ToString("0.##"));
                        ttElementary.ClearValue(ForegroundProperty);
                        if (jobaLastYearCount > 0 || carLastYearCount > 0 || jabilaLastYearCount > 0)
                        {
                            int totalCarLastYearCount = jobaLastYearCount + carLastYearCount + jabilaLastYearCount;
                            decimal totalCarLastYearPremium = jobaLastYearPremium + carLastYearPremium + jabilaLastYearPremium;
                            ttCar.Content = string.Format("סה''כ פוליסות: {0}\nסה''כ פרמיה: {1}", totalCarLastYearCount.ToString(), totalCarLastYearPremium.ToString("0.##"));
                            ttCar.ClearValue(ForegroundProperty);
                            brCar.Background = darkBlue;
                            lblCar.Foreground = Brushes.White;
                            
                        }
                        if (aptLastYearCount > 0)
                        {
                            ttApartment.Content = string.Format("סה''כ פוליסות: {0}\nסה''כ פרמיה: {1}", aptLastYearCount.ToString(), aptLastYearPremium.ToString("0.##"));
                            ttApartment.ClearValue(ForegroundProperty);
                            brApartment.Background = darkBlue;
                            lblApartment.Foreground = Brushes.White;
                        }
                        if (businessLastYearCount > 0)
                        {
                            ttBusiness.Content = string.Format("סה''כ פוליסות: {0}\nסה''כ פרמיה: {1}", businessLastYearCount.ToString(), businessLastYearPremium.ToString("0.##"));
                            ttBusiness.ClearValue(ForegroundProperty);
                            brBusiness.Background = darkBlue;
                            lblBusiness.Foreground = Brushes.White;
                        }
                    }
                    if (lifeLastYearPremium > 0)
                    {
                        int lifeLastYearCount = managersLastYearCount + riskLastYearCount + mortgageLastYearCount + privateLastYearCount + pensionLastYearCount;
                        ttLife.Content = string.Format("סה''כ פוליסות: {0}\nסה''כ פרמיה: {1}", lifeLastYearCount.ToString(), lifeLastYearPremium.ToString("0.##"));
                        ttLife.ClearValue(ForegroundProperty);
                        if (managersLastYearCount > 0)
                        {
                            ttManagers.Content = string.Format("סה''כ פוליסות: {0}\nסה''כ פרמיה: {1}", managersLastYearCount.ToString(), managersLastYearPremium.ToString("0.##"));
                            ttManagers.ClearValue(ForegroundProperty);
                            brManagers.Background = darkBlue;
                            lblManagers.Foreground = Brushes.White;
                        }
                        if (riskLastYearCount > 0)
                        {
                            ttRisks.Content = string.Format("סה''כ פוליסות: {0}\nסה''כ פרמיה: {1}", riskLastYearCount.ToString(), riskLastYearPremium.ToString("0.##"));
                            ttRisks.ClearValue(ForegroundProperty);
                            brRisks.Background = darkBlue;
                            lblRisk.Foreground = Brushes.White;
                        }
                        if (mortgageLastYearCount > 0)
                        {
                            ttMortgage.Content = string.Format("סה''כ פוליסות: {0}\nסה''כ פרמיה: {1}", mortgageLastYearCount.ToString(), mortgageLastYearPremium.ToString("0.##"));
                            ttMortgage.ClearValue(ForegroundProperty);
                            brMortgage.Background = darkBlue;
                            lblMortgage.Foreground = Brushes.White;
                        }
                        if (privateLastYearCount > 0)
                        {
                            ttPrivate.Content = string.Format("סה''כ פוליסות: {0}\nסה''כ פרמיה: {1}", privateLastYearCount.ToString(), privateLastYearPremium.ToString("0.##"));
                            ttPrivate.ClearValue(ForegroundProperty);
                            brPrivate.Background = darkBlue;
                            lblPrivate.Foreground = Brushes.White;
                        }
                        if (pensionLastYearCount > 0)
                        {
                            ttPension.Content = string.Format("סה''כ פוליסות: {0}\nסה''כ פרמיה: {1}", pensionLastYearCount.ToString(), pensionLastYearPremium.ToString("0.##"));
                            ttPension.ClearValue(ForegroundProperty);
                            brPension.Background = darkBlue;
                            lblPension.Foreground = Brushes.White;
                        }
                    }
                    if (healthLastYearPremium > 0)
                    {
                        ttHealth.Content = string.Format("סה''כ פוליסות: {0}\nסה''כ פרמיה: {1}", healthLastYearCount.ToString(), healthLastYearPremium.ToString("0.##"));
                        ttHealth.ClearValue(ForegroundProperty);
                        brHealth.Background = darkBlue;
                        lblHealth.Foreground = Brushes.White;
                    }
                    if (personalAccidentsLastYearPremium > 0)
                    {
                        ttPersonalAccidents.Content = string.Format("סה''כ פוליסות: {0}\nסה''כ פרמיה: {1}", personalAccidentsLastYearCount.ToString(), personalAccidentsLastYearPremium.ToString("0.##"));
                        ttPersonalAccidents.ClearValue(ForegroundProperty);
                        brPersonalAccidents.Background = darkBlue;
                        lblPersonalAccidents.Foreground = Brushes.White;
                    }
                    if (financeLastYearPremium > 0)
                    {
                        int financeLastYearCount = kupatGuemelLastYearCount + kerenHishtalmutLastYearCount + invesmentPortfolioLastYearCount;
                        ttFinance.Content = string.Format("סה''כ פוליסות: {0}\nסה''כ פרמיה: {1}", financeLastYearCount.ToString(), financeLastYearPremium.ToString("0.##"));
                        ttFinance.ClearValue(ForegroundProperty);
                        if (kupatGuemelLastYearCount > 0)
                        {
                            ttKupatGuemel.Content = string.Format("סה''כ פוליסות: {0}\nסה''כ פרמיה: {1}", kupatGuemelLastYearCount.ToString(), kupatGuemelLastYearPremium.ToString("0.##"));
                            ttKupatGuemel.ClearValue(ForegroundProperty);
                            brKupatGuemel.Background = darkBlue;
                            lblKupatGuemel.Foreground = Brushes.White;
                        }
                        if (kerenHishtalmutLastYearCount > 0)
                        {
                            ttKerenHishtalmut.Content = string.Format("סה''כ פוליסות: {0}\nסה''כ פרמיה: {1}", kerenHishtalmutLastYearCount.ToString(), kerenHishtalmutLastYearPremium.ToString("0.##"));
                            ttKerenHishtalmut.ClearValue(ForegroundProperty);
                            brKerenHishtalmut.Background = darkBlue;
                            lblKerenHishtalmut.Foreground = Brushes.White;
                        }
                        if (invesmentPortfolioLastYearCount > 0)
                        {
                            ttInvesmentPortfolio.Content = string.Format("סה''כ פוליסות: {0}\nסה''כ פרמיה: {1}", invesmentPortfolioLastYearCount.ToString(), invesmentPortfolioLastYearPremium.ToString("0.##"));
                            ttInvesmentPortfolio.ClearValue(ForegroundProperty);
                            brInvesmentPortfolio.Background = darkBlue;
                            lblInvesmentFolder.Foreground = Brushes.White;
                        }
                    }
                    if (foreingWorkersLastYearPremium > 0)
                    {
                        int foreingWorkersLastYearCount = fWorkersLastYearCount + touristsLastYearCount;
                        ttForeingWorkers.Content = string.Format("סה''כ פוליסות: {0}\nסה''כ פרמיה: {1}", foreingWorkersLastYearCount.ToString(), foreingWorkersLastYearPremium.ToString("0.##"));
                        ttForeingWorkers.ClearValue(ForegroundProperty);
                        brForeingWorkers.Background = darkBlue;
                        lblForeingWorkers.Foreground = Brushes.White;
                    }
                    if (travelLastYearPremium > 0)
                    {
                        ttTravel.Content = string.Format("סה''כ פוליסות: {0}\nסה''כ פרמיה: {1}", travelLastYearCount.ToString(), travelLastYearPremium.ToString("0.##"));
                        ttTravel.ClearValue(ForegroundProperty);
                        brTravel.Background = darkBlue;
                        lblTravel.Foreground = Brushes.White;
                    }

                    if (elementaryLastYearClaimsCount > 0 || lifeLastYearClaimsCount > 0 || healthLastYearClaimsCount > 0 || personalAccidentsLastYearClaimsCount > 0 || travelLastYearClaimsCount > 0 || foreingWorkersLastYearClaimsCount > 0)
                    {
                        int claimsLastYearCount = elementaryLastYearClaimsCount + lifeLastYearClaimsCount + healthLastYearClaimsCount + personalAccidentsLastYearClaimsCount + travelLastYearClaimsCount + foreingWorkersLastYearClaimsCount;
                        decimal claimsLastYearAmount = elementaryLastYearClaims + lifeLastYearClaims + healthLastYearClaims + personalAccidentsLastYearClaims + travelLastYearClaims + foreingWorkersLastYearClaims;
                        ttClaims.Content = string.Format("סה''כ תביעות: {0}\nסה''כ נתבע: {1}", claimsLastYearCount.ToString(), claimsLastYearAmount.ToString("0.##"));
                        ttClaims.Foreground = Brushes.Red;
                        if (elementaryLastYearClaimsCount > 0)
                        {
                            ttElementaryClaims.Content = string.Format("סה''כ תביעות: {0}\nסה''כ נתבע: {1}", elementaryLastYearClaimsCount.ToString(), elementaryLastYearClaims.ToString("0.##"));
                            ttElementaryClaims.Foreground = Brushes.Red;
                            brElementaryClaims.Background = turkiz;
                        }
                        if (lifeLastYearClaimsCount > 0)
                        {
                            ttLifeClaims.Content = string.Format("סה''כ תביעות: {0}\nסה''כ נתבע: {1}", lifeLastYearClaimsCount.ToString(), lifeLastYearClaims.ToString("0.##"));
                            ttLifeClaims.Foreground = Brushes.Red;
                            brLifeClaims.Background = turkiz;
                        }
                        if (healthLastYearClaimsCount > 0)
                        {
                            ttHealthClaims.Content = string.Format("סה''כ תביעות: {0}\nסה''כ נתבע: {1}", healthLastYearClaimsCount.ToString(), healthLastYearClaims.ToString("0.##"));
                            ttHealthClaims.Foreground = Brushes.Red;
                            brHealthClaims.Background = turkiz;
                        }
                        if (personalAccidentsLastYearClaimsCount > 0)
                        {
                            ttPersonalAccidentsClaims.Content = string.Format("סה''כ תביעות: {0}\nסה''כ נתבע: {1}", personalAccidentsLastYearClaimsCount.ToString(), personalAccidentsLastYearClaims.ToString("0.##"));
                            ttPersonalAccidentsClaims.Foreground = Brushes.Red;
                            brPersonalAccidentsClaims.Background = turkiz;
                        }
                        if (travelLastYearClaimsCount > 0)
                        {
                            ttTravelClaims.Content = string.Format("סה''כ תביעות: {0}\nסה''כ נתבע: {1}", travelLastYearClaimsCount.ToString(), travelLastYearClaims.ToString("0.##"));
                            ttTravelClaims.Foreground = Brushes.Red;
                            brTravelClaims.Background = turkiz;
                        }
                        if (foreingWorkersLastYearClaimsCount > 0)
                        {
                            ttForeingWorkersClaims.Content = string.Format("סה''כ תביעות: {0}\nסה''כ נתבע: {1}", foreingWorkersLastYearClaimsCount.ToString(), foreingWorkersLastYearClaims.ToString("0.##"));
                            ttForeingWorkersClaims.Foreground = Brushes.Red;
                            brForeingWorkersClaims.Background = turkiz;
                        }
                    }
                    if (elementaryOffersLastYear>0||personalAccidentsOffersLastYear>0||travelOffersLastYear>0||lifeOffersLastYear>0||healthOffersLastYear>0||financeOffersLastYear>0||foreingWorkersOffersLastYear>0)
                    {
                        int offersCount = elementaryOffersLastYear + personalAccidentsOffersLastYear + travelOffersLastYear + lifeOffersLastYear + healthOffersLastYear + financeOffersLastYear + foreingWorkersOffersLastYear;
                        txtOffers.Text = offersCount.ToString("D");
                        ttOffers.Content = string.Format("סה''כ הצעות פתוחות: {0}", offersCount.ToString("D"));
                        ttOffers.ClearValue(ForegroundProperty);

                        if (elementaryOffersLastYear>0)
                        {
                            ttElementaryOffers.Content= string.Format("סה''כ הצעות אלמנטרי: {0}", elementaryOffersLastYear.ToString("D"));
                            ttElementaryOffers.ClearValue(ForegroundProperty);
                            brElementaryOffers.Background = lightGreen;
                        }
                        if (personalAccidentsOffersLastYear > 0)
                        {
                            ttPersonalAccidentsOffers.Content = string.Format("סה''כ הצעות תאונות אישיות: {0}", personalAccidentsOffersLastYear.ToString("D"));
                            ttPersonalAccidentsOffers.ClearValue(ForegroundProperty);
                            brPersonalAccidentsOffers.Background = lightGreen;
                        }
                        if (travelOffersLastYear > 0)
                        {
                            ttTravelOffers.Content = string.Format("סה''כ הצעות חו''ל: {0}", travelOffersLastYear.ToString("D"));
                            ttTravelOffers.ClearValue(ForegroundProperty);
                            brTravelOffers.Background = lightGreen;
                        }
                        if (lifeOffersLastYear > 0)
                        {
                            ttLifeOffers.Content = string.Format("סה''כ הצעות חיים: {0}", lifeOffersLastYear.ToString("D"));
                            ttLifeOffers.ClearValue(ForegroundProperty);
                            brLifeOffers.Background = lightGreen;
                        }
                        if (healthOffersLastYear > 0)
                        {
                            ttHealthOffers.Content = string.Format("סה''כ הצעות בריאות: {0}", healthOffersLastYear.ToString("D"));
                            ttHealthOffers.ClearValue(ForegroundProperty);
                            brHealthOffers.Background = lightGreen;
                        }
                        if (financeOffersLastYear > 0)
                        {
                            ttFinanceOffers.Content = string.Format("סה''כ הצעות פיננסים: {0}", financeOffersLastYear.ToString("D"));
                            ttFinanceOffers.ClearValue(ForegroundProperty);
                            brFinanceOffers.Background = lightGreen;
                        }
                        if (foreingWorkersOffersLastYear > 0)
                        {
                            ttForeingWorkersOffers.Content = string.Format("סה''כ הצעות עובדים זרים: {0}", foreingWorkersOffersLastYear.ToString("D"));
                            ttForeingWorkersOffers.ClearValue(ForegroundProperty);
                            brForeingWorkersOffers.Background = lightGreen;
                        }
                    }

                }
                else if (tbItem3Years.IsSelected)
                {
                    txtElementaryPremium.Text = elementary3YearPremium.ToString("0.##");
                    txtElementaryClaims.Text = elementary3YearsClaims.ToString("0.##");
                    txtLifePremium.Text = life3YearPremium.ToString("0.##");
                    txtLifeClaims.Text = life3YearsClaims.ToString("0.##");
                    txtHealthPremium.Text = health3YearPremium.ToString("0.##");
                    txtHealthClaims.Text = health3YearsClaims.ToString("0.##");
                    txtPersonalAccidentsPremium.Text = personalAccidents3YearPremium.ToString("0.##");
                    txtPersonalAccidentsClaims.Text = personalAccidents3YearsClaims.ToString("0.##");
                    txtFinancePremium.Text = finance3YearPremium.ToString("0.##");
                    txtTravelPremium.Text = travel3YearPremium.ToString("0.##");
                    txtTravelClaims.Text = travel3YearsClaims.ToString("0.##");
                    txtForeingWorkersPremium.Text = foreingWorkers3YearPremium.ToString("0.##");
                    txtForeingWorkersClaims.Text = foreingWorkers3YearsClaims.ToString("0.##");

                    if (elementary3YearPremium > 0)
                    {
                        int elementary3YearCount = joba3YearsCount + car3YearsCount + jabila3YearsCount + apt3YearsCount + business3YearsCount;
                        ttElementary.Content = string.Format("סה''כ פוליסות: {0}\nסה''כ פרמיה: {1}", elementary3YearCount.ToString(), elementary3YearPremium.ToString("0.##"));
                        ttElementary.ClearValue(ForegroundProperty);
                        if (joba3YearsCount > 0 || car3YearsCount > 0 || jabila3YearsCount > 0)
                        {
                            int totalCar3YearsCount = joba3YearsCount + car3YearsCount + jabila3YearsCount;
                            decimal totalCar3YearsPremium = joba3YearsPremium + car3YearsPremium + jabila3YearsPremium;
                            ttCar.Content = string.Format("סה''כ פוליסות: {0}\nסה''כ פרמיה: {1}", totalCar3YearsCount.ToString(), totalCar3YearsPremium.ToString("0.##"));
                            ttCar.ClearValue(ForegroundProperty);
                            brCar.Background = darkBlue;
                            lblCar.Foreground = Brushes.White;
                        }
                        if (apt3YearsCount > 0)
                        {
                            ttApartment.Content = string.Format("סה''כ פוליסות: {0}\nסה''כ פרמיה: {1}", apt3YearsCount.ToString(), apt3YearsPremium.ToString("0.##"));
                            ttApartment.ClearValue(ForegroundProperty);
                            brApartment.Background = darkBlue;
                            lblApartment.Foreground = Brushes.White;
                        }
                        if (business3YearsCount > 0)
                        {
                            ttBusiness.Content = string.Format("סה''כ פוליסות: {0}\nסה''כ פרמיה: {1}", business3YearsCount.ToString(), business3YearsPremium.ToString("0.##"));
                            ttBusiness.ClearValue(ForegroundProperty);
                            brBusiness.Background = darkBlue;
                            lblBusiness.Foreground = Brushes.White;
                        }
                    }
                    if (life3YearPremium > 0)
                    {
                        int life3YearsCount = managers3YearsCount + risk3YearsCount + mortgage3YearsCount + private3YearsCount + pension3YearsCount;
                        ttLife.Content = string.Format("סה''כ פוליסות: {0}\nסה''כ פרמיה: {1}", life3YearsCount.ToString(), life3YearPremium.ToString("0.##"));
                        ttLife.ClearValue(ForegroundProperty);
                        if (managers3YearsCount > 0)
                        {
                            ttManagers.Content = string.Format("סה''כ פוליסות: {0}\nסה''כ פרמיה: {1}", managers3YearsCount.ToString(), managers3YearsPremium.ToString("0.##"));
                            ttManagers.ClearValue(ForegroundProperty);
                            brManagers.Background = darkBlue;
                            lblManagers.Foreground = Brushes.White;
                        }
                        if (risk3YearsCount > 0)
                        {
                            ttRisks.Content = string.Format("סה''כ פוליסות: {0}\nסה''כ פרמיה: {1}", risk3YearsCount.ToString(), risk3YearsPremium.ToString("0.##"));
                            ttRisks.ClearValue(ForegroundProperty);
                            brRisks.Background = darkBlue;
                            lblRisk.Foreground = Brushes.White;
                        }
                        if (mortgage3YearsCount > 0)
                        {
                            ttMortgage.Content = string.Format("סה''כ פוליסות: {0}\nסה''כ פרמיה: {1}", mortgage3YearsCount.ToString(), mortgage3YearsPremium.ToString("0.##"));
                            ttMortgage.ClearValue(ForegroundProperty);
                            brMortgage.Background = darkBlue;
                            lblMortgage.Foreground = Brushes.White;
                        }
                        if (private3YearsCount > 0)
                        {
                            ttPrivate.Content = string.Format("סה''כ פוליסות: {0}\nסה''כ פרמיה: {1}", private3YearsCount.ToString(), private3YearsPremium.ToString("0.##"));
                            ttPrivate.ClearValue(ForegroundProperty);
                            brPrivate.Background = darkBlue;
                            lblPrivate.Foreground = Brushes.White;
                        }
                        if (pension3YearsCount > 0)
                        {
                            ttPension.Content = string.Format("סה''כ פוליסות: {0}\nסה''כ פרמיה: {1}", pension3YearsCount.ToString(), pension3YearsPremium.ToString("0.##"));
                            ttPension.ClearValue(ForegroundProperty);
                            brPension.Background = darkBlue;
                            lblPension.Foreground = Brushes.White;
                        }
                    }
                    if (health3YearPremium > 0)
                    {
                        ttHealth.Content = string.Format("סה''כ פוליסות: {0}\nסה''כ פרמיה: {1}", health3YearsCount.ToString(), health3YearPremium.ToString("0.##"));
                        ttHealth.ClearValue(ForegroundProperty);
                        brHealth.Background = darkBlue;
                        lblHealth.Foreground = Brushes.White;
                    }
                    if (personalAccidents3YearPremium > 0)
                    {
                        ttPersonalAccidents.Content = string.Format("סה''כ פוליסות: {0}\nסה''כ פרמיה: {1}", personalAccidents3YearsCount.ToString(), personalAccidents3YearPremium.ToString("0.##"));
                        ttPersonalAccidents.ClearValue(ForegroundProperty);
                        brPersonalAccidents.Background = darkBlue;
                        lblPersonalAccidents.Foreground = Brushes.White;
                    }
                    if (finance3YearPremium > 0)
                    {
                        int finance3YearsCount = kupatGuemel3YearsCount + kerenHishtalmut3YearsCount + invesmentPortfolio3YearsCount;
                        ttFinance.Content = string.Format("סה''כ פוליסות: {0}\nסה''כ פרמיה: {1}", finance3YearsCount.ToString(), finance3YearPremium.ToString("0.##"));
                        ttFinance.ClearValue(ForegroundProperty);
                        if (kupatGuemel3YearsCount > 0)
                        {
                            ttKupatGuemel.Content = string.Format("סה''כ פוליסות: {0}\nסה''כ פרמיה: {1}", kupatGuemel3YearsCount.ToString(), kupatGuemel3YearsPremium.ToString("0.##"));
                            ttKupatGuemel.ClearValue(ForegroundProperty);
                            brKupatGuemel.Background = darkBlue;
                            lblKupatGuemel.Foreground = Brushes.White;
                        }
                        if (kerenHishtalmut3YearsCount > 0)
                        {
                            ttKerenHishtalmut.Content = string.Format("סה''כ פוליסות: {0}\nסה''כ פרמיה: {1}", kerenHishtalmut3YearsCount.ToString(), kerenHishtalmut3YearsPremium.ToString("0.##"));
                            ttKerenHishtalmut.ClearValue(ForegroundProperty);
                            brKerenHishtalmut.Background = darkBlue;
                            lblKerenHishtalmut.Foreground = Brushes.White;
                        }
                        if (invesmentPortfolio3YearsCount > 0)
                        {
                            ttInvesmentPortfolio.Content = string.Format("סה''כ פוליסות: {0}\nסה''כ פרמיה: {1}", invesmentPortfolio3YearsCount.ToString(), invesmentPortfolio3YearsPremium.ToString("0.##"));
                            ttInvesmentPortfolio.ClearValue(ForegroundProperty);
                            brInvesmentPortfolio.Background = darkBlue;
                            lblInvesmentFolder.Foreground = Brushes.White;
                        }
                    }
                    if (foreingWorkers3YearPremium > 0)
                    {
                        int foreingWorkers3YearsCount = fWorkers3YearsCount + tourists3YearsCount;
                        ttForeingWorkers.Content = string.Format("סה''כ פוליסות: {0}\nסה''כ פרמיה: {1}", foreingWorkers3YearsCount.ToString(), foreingWorkers3YearPremium.ToString("0.##"));
                        ttForeingWorkers.ClearValue(ForegroundProperty);
                        brForeingWorkers.Background = darkBlue;
                        lblForeingWorkers.Foreground = Brushes.White;
                    }
                    if (travel3YearPremium > 0)
                    {
                        ttTravel.Content = string.Format("סה''כ פוליסות: {0}\nסה''כ פרמיה: {1}", travel3YearsCount.ToString(), travel3YearPremium.ToString("0.##"));
                        ttTravel.ClearValue(ForegroundProperty);
                        brTravel.Background = darkBlue;
                        lblTravel.Foreground = Brushes.White;
                    }

                    if (elementary3YearsClaimsCount > 0 || life3YearsClaimsCount > 0 || health3YearsClaimsCount > 0 || personalAccidents3YearsClaimsCount > 0 || travel3YearsClaimsCount > 0 || foreingWorkers3YearsClaimsCount > 0)
                    {
                        int claims3YearsCount = elementary3YearsClaimsCount + life3YearsClaimsCount + health3YearsClaimsCount + personalAccidents3YearsClaimsCount + travel3YearsClaimsCount + foreingWorkers3YearsClaimsCount;
                        decimal claims3YearsAmount = elementary3YearsClaims + life3YearsClaims + health3YearsClaims + personalAccidents3YearsClaims + travel3YearsClaims + foreingWorkers3YearsClaims;
                        ttClaims.Content = string.Format("סה''כ תביעות: {0}\nסה''כ נתבע: {1}", claims3YearsCount.ToString(), claims3YearsAmount.ToString("0.##"));
                        ttClaims.Foreground = Brushes.Red;
                        if (elementary3YearsClaimsCount > 0)
                        {
                            ttElementaryClaims.Content = string.Format("סה''כ תביעות: {0}\nסה''כ נתבע: {1}", elementary3YearsClaimsCount.ToString(), elementary3YearsClaims.ToString("0.##"));
                            ttElementaryClaims.Foreground = Brushes.Red;
                            brElementaryClaims.Background = turkiz;                            
                        }
                        if (life3YearsClaimsCount > 0)
                        {
                            ttLifeClaims.Content = string.Format("סה''כ תביעות: {0}\nסה''כ נתבע: {1}", life3YearsClaimsCount.ToString(), life3YearsClaims.ToString("0.##"));
                            ttLifeClaims.Foreground = Brushes.Red;
                            brLifeClaims.Background = turkiz;
                        }
                        if (health3YearsClaimsCount > 0)
                        {
                            ttHealthClaims.Content = string.Format("סה''כ תביעות: {0}\nסה''כ נתבע: {1}", health3YearsClaimsCount.ToString(), health3YearsClaims.ToString("0.##"));
                            ttHealthClaims.Foreground = Brushes.Red;
                            brHealthClaims.Background = turkiz;
                        }
                        if (personalAccidents3YearsClaimsCount > 0)
                        {
                            ttPersonalAccidentsClaims.Content = string.Format("סה''כ תביעות: {0}\nסה''כ נתבע: {1}", personalAccidents3YearsClaimsCount.ToString(), personalAccidents3YearsClaims.ToString("0.##"));
                            ttPersonalAccidentsClaims.Foreground = Brushes.Red;
                            brPersonalAccidentsClaims.Background = turkiz;
                        }
                        if (travel3YearsClaimsCount > 0)
                        {
                            ttTravelClaims.Content = string.Format("סה''כ תביעות: {0}\nסה''כ נתבע: {1}", travel3YearsClaimsCount.ToString(), travel3YearsClaims.ToString("0.##"));
                            ttTravelClaims.Foreground = Brushes.Red;
                            brTravelClaims.Background = turkiz;
                        }
                        if (foreingWorkers3YearsClaimsCount > 0)
                        {
                            ttForeingWorkersClaims.Content = string.Format("סה''כ תביעות: {0}\nסה''כ נתבע: {1}", foreingWorkers3YearsClaimsCount.ToString(), foreingWorkers3YearsClaims.ToString("0.##"));
                            ttForeingWorkersClaims.Foreground = Brushes.Red;
                            brForeingWorkersClaims.Background = turkiz;
                        }
                    }

                    if (elementaryOffers3Years > 0 || personalAccidentsOffers3Years > 0 || travelOffers3Years > 0 || lifeOffers3Years > 0 || healthOffers3Years > 0 || financeOffers3Years > 0 || foreingWorkersOffers3Years > 0)
                    {
                        int offersCount = elementaryOffers3Years + personalAccidentsOffers3Years + travelOffers3Years + lifeOffers3Years + healthOffers3Years + financeOffers3Years + foreingWorkersOffers3Years;
                        txtOffers.Text = offersCount.ToString("D");
                        ttOffers.Content = string.Format("סה''כ הצעות פתוחות: {0}", offersCount.ToString("D"));
                        ttOffers.ClearValue(ForegroundProperty);

                        if (elementaryOffers3Years > 0)
                        {
                            ttElementaryOffers.Content = string.Format("סה''כ הצעות אלמנטרי: {0}", elementaryOffers3Years.ToString("D"));
                            ttElementaryOffers.ClearValue(ForegroundProperty);
                            brElementaryOffers.Background = lightGreen;
                        }
                        if (personalAccidentsOffers3Years > 0)
                        {
                            ttPersonalAccidentsOffers.Content = string.Format("סה''כ הצעות תאונות אישיות: {0}", personalAccidentsOffers3Years.ToString("D"));
                            ttPersonalAccidentsOffers.ClearValue(ForegroundProperty);
                            brPersonalAccidentsOffers.Background = lightGreen;
                        }
                        if (travelOffers3Years > 0)
                        {
                            ttTravelOffers.Content = string.Format("סה''כ הצעות חו''ל: {0}", travelOffers3Years.ToString("D"));
                            ttTravelOffers.ClearValue(ForegroundProperty);
                            brTravelOffers.Background = lightGreen;
                        }
                        if (lifeOffers3Years > 0)
                        {
                            ttLifeOffers.Content = string.Format("סה''כ הצעות חיים: {0}", lifeOffers3Years.ToString("D"));
                            ttLifeOffers.ClearValue(ForegroundProperty);
                            brLifeOffers.Background = lightGreen;
                        }
                        if (healthOffers3Years > 0)
                        {
                            ttHealthOffers.Content = string.Format("סה''כ הצעות בריאות: {0}", healthOffers3Years.ToString("D"));
                            ttHealthOffers.ClearValue(ForegroundProperty);
                            brHealthOffers.Background = lightGreen;
                        }
                        if (financeOffers3Years > 0)
                        {
                            ttFinanceOffers.Content = string.Format("סה''כ הצעות פיננסים: {0}", financeOffers3Years.ToString("D"));
                            ttFinanceOffers.ClearValue(ForegroundProperty);
                            brFinanceOffers.Background = lightGreen;
                        }
                        if (foreingWorkersOffers3Years > 0)
                        {
                            ttForeingWorkersOffers.Content = string.Format("סה''כ הצעות עובדים זרים: {0}", foreingWorkersOffers3Years.ToString("D"));
                            ttForeingWorkersOffers.ClearValue(ForegroundProperty);
                            brForeingWorkersOffers.Background = lightGreen;
                        }
                    }
                }
                else if (tbItem5Years.IsSelected)
                {
                    txtElementaryPremium.Text = elementary5YearPremium.ToString("0.##");
                    txtElementaryClaims.Text = elementary5YearsClaims.ToString("0.##");
                    txtLifePremium.Text = life5YearPremium.ToString("0.##");
                    txtLifeClaims.Text = life5YearsClaims.ToString("0.##");
                    txtHealthPremium.Text = health5YearPremium.ToString("0.##");
                    txtHealthClaims.Text = health5YearsClaims.ToString("0.##");
                    txtPersonalAccidentsPremium.Text = personalAccidents5YearPremium.ToString("0.##");
                    txtPersonalAccidentsClaims.Text = personalAccidents5YearsClaims.ToString("0.##");
                    txtFinancePremium.Text = finance5YearPremium.ToString("0.##");
                    txtTravelPremium.Text = travel5YearPremium.ToString("0.##");
                    txtTravelClaims.Text = travel5YearsClaims.ToString("0.##");
                    txtForeingWorkersPremium.Text = foreingWorkers5YearPremium.ToString("0.##");
                    txtForeingWorkersClaims.Text = foreingWorkers5YearsClaims.ToString("0.##");

                    if (elementary5YearPremium > 0)
                    {
                        int elementary5YearCount = joba5YearsCount + car5YearsCount + jabila5YearsCount + apt5YearsCount + business5YearsCount;
                        ttElementary.Content = string.Format("סה''כ פוליסות: {0}\nסה''כ פרמיה: {1}", elementary5YearCount.ToString(), elementary5YearPremium.ToString("0.##"));
                        ttElementary.ClearValue(ForegroundProperty);
                        if (joba5YearsCount > 0 || car5YearsCount > 0 || jabila5YearsCount > 0)
                        {
                            int totalCar5YearsCount = joba5YearsCount + car5YearsCount + jabila5YearsCount;
                            decimal totalCar5YearsPremium = joba5YearsPremium + car5YearsPremium + jabila5YearsPremium;
                            ttCar.Content = string.Format("סה''כ פוליסות: {0}\nסה''כ פרמיה: {1}", totalCar5YearsCount.ToString(), totalCar5YearsPremium.ToString("0.##"));
                            ttCar.ClearValue(ForegroundProperty);
                            brCar.Background = darkBlue;
                            lblCar.Foreground = Brushes.White;
                        }
                        if (apt5YearsCount > 0)
                        {
                            ttApartment.Content = string.Format("סה''כ פוליסות: {0}\nסה''כ פרמיה: {1}", apt5YearsCount.ToString(), apt5YearsPremium.ToString("0.##"));
                            ttApartment.ClearValue(ForegroundProperty);
                            brApartment.Background = darkBlue;
                            lblApartment.Foreground = Brushes.White;
                        }
                        if (business5YearsCount > 0)
                        {
                            ttBusiness.Content = string.Format("סה''כ פוליסות: {0}\nסה''כ פרמיה: {1}", business5YearsCount.ToString(), business5YearsPremium.ToString("0.##"));
                            ttBusiness.ClearValue(ForegroundProperty);
                            brBusiness.Background = darkBlue;
                            lblBusiness.Foreground = Brushes.White;
                        }
                    }
                    if (life5YearPremium > 0)
                    {
                        int life5YearsCount = managers5YearsCount + risk5YearsCount + mortgage5YearsCount + private5YearsCount + pension5YearsCount;
                        ttLife.Content = string.Format("סה''כ פוליסות: {0}\nסה''כ פרמיה: {1}", life5YearsCount.ToString(), life5YearPremium.ToString("0.##"));
                        ttLife.ClearValue(ForegroundProperty);
                        if (managers5YearsCount > 0)
                        {
                            ttManagers.Content = string.Format("סה''כ פוליסות: {0}\nסה''כ פרמיה: {1}", managers5YearsCount.ToString(), managers5YearsPremium.ToString("0.##"));
                            ttManagers.ClearValue(ForegroundProperty);
                            brManagers.Background = darkBlue;
                            lblManagers.Foreground = Brushes.White;
                        }
                        if (risk5YearsCount > 0)
                        {
                            ttRisks.Content = string.Format("סה''כ פוליסות: {0}\nסה''כ פרמיה: {1}", risk5YearsCount.ToString(), risk5YearsPremium.ToString("0.##"));
                            ttRisks.ClearValue(ForegroundProperty);
                            brRisks.Background = darkBlue;
                            lblRisk.Foreground = Brushes.White;
                        }
                        if (mortgage5YearsCount > 0)
                        {
                            ttMortgage.Content = string.Format("סה''כ פוליסות: {0}\nסה''כ פרמיה: {1}", mortgage5YearsCount.ToString(), mortgage5YearsPremium.ToString("0.##"));
                            ttMortgage.ClearValue(ForegroundProperty);
                            brMortgage.Background = darkBlue;
                            lblMortgage.Foreground = Brushes.White;
                        }
                        if (private5YearsCount > 0)
                        {
                            ttPrivate.Content = string.Format("סה''כ פוליסות: {0}\nסה''כ פרמיה: {1}", private5YearsCount.ToString(), private5YearsPremium.ToString("0.##"));
                            ttPrivate.ClearValue(ForegroundProperty);
                            brPrivate.Background = darkBlue;
                            lblPrivate.Foreground = Brushes.White;
                        }
                        if (pension5YearsCount > 0)
                        {
                            ttPension.Content = string.Format("סה''כ פוליסות: {0}\nסה''כ פרמיה: {1}", pension5YearsCount.ToString(), pension5YearsPremium.ToString("0.##"));
                            ttPension.ClearValue(ForegroundProperty);
                            brPension.Background = darkBlue;
                            lblPension.Foreground = Brushes.White;
                        }
                    }
                    if (health5YearPremium > 0)
                    {
                        ttHealth.Content = string.Format("סה''כ פוליסות: {0}\nסה''כ פרמיה: {1}", health5YearsCount.ToString(), health5YearPremium.ToString("0.##"));
                        ttHealth.ClearValue(ForegroundProperty);
                        brHealth.Background = darkBlue;
                        lblHealth.Foreground = Brushes.White;
                    }
                    if (personalAccidents5YearPremium > 0)
                    {
                        ttPersonalAccidents.Content = string.Format("סה''כ פוליסות: {0}\nסה''כ פרמיה: {1}", personalAccidents5YearsCount.ToString(), personalAccidents5YearPremium.ToString("0.##"));
                        ttPersonalAccidents.ClearValue(ForegroundProperty);
                        brPersonalAccidents.Background = darkBlue;
                        lblPersonalAccidents.Foreground = Brushes.White;
                    }
                    if (finance5YearPremium > 0)
                    {
                        int finance5YearsCount = kupatGuemel5YearsCount + kerenHishtalmut5YearsCount + invesmentPortfolio5YearsCount;
                        ttFinance.Content = string.Format("סה''כ פוליסות: {0}\nסה''כ פרמיה: {1}", finance5YearsCount.ToString(), finance5YearPremium.ToString("0.##"));
                        ttFinance.ClearValue(ForegroundProperty);
                        if (kupatGuemel5YearsCount > 0)
                        {
                            ttKupatGuemel.Content = string.Format("סה''כ פוליסות: {0}\nסה''כ פרמיה: {1}", kupatGuemel5YearsCount.ToString(), kupatGuemel5YearsPremium.ToString("0.##"));
                            ttKupatGuemel.ClearValue(ForegroundProperty);
                            brKupatGuemel.Background = darkBlue;
                            lblKupatGuemel.Foreground = Brushes.White;
                        }
                        if (kerenHishtalmut5YearsCount > 0)
                        {
                            ttKerenHishtalmut.Content = string.Format("סה''כ פוליסות: {0}\nסה''כ פרמיה: {1}", kerenHishtalmut5YearsCount.ToString(), kerenHishtalmut5YearsPremium.ToString("0.##"));
                            ttKerenHishtalmut.ClearValue(ForegroundProperty);
                            brKerenHishtalmut.Background = darkBlue;
                            lblKerenHishtalmut.Foreground = Brushes.White;
                        }
                        if (invesmentPortfolio5YearsCount > 0)
                        {
                            ttInvesmentPortfolio.Content = string.Format("סה''כ פוליסות: {0}\nסה''כ פרמיה: {1}", invesmentPortfolio5YearsCount.ToString(), invesmentPortfolio5YearsPremium.ToString("0.##"));
                            ttInvesmentPortfolio.ClearValue(ForegroundProperty);
                            brInvesmentPortfolio.Background = darkBlue;
                            lblInvesmentFolder.Foreground = Brushes.White;
                        }
                    }
                    if (foreingWorkers5YearPremium > 0)
                    {
                        int foreingWorkers5YearsCount = fWorkers5YearsCount + tourists5YearsCount;
                        ttForeingWorkers.Content = string.Format("סה''כ פוליסות: {0}\nסה''כ פרמיה: {1}", foreingWorkers5YearsCount.ToString(), foreingWorkers5YearPremium.ToString("0.##"));
                        ttForeingWorkers.ClearValue(ForegroundProperty);
                        lblForeingWorkers.Foreground = Brushes.White;
                    }
                    if (travel5YearPremium > 0)
                    {
                        ttTravel.Content = string.Format("סה''כ פוליסות: {0}\nסה''כ פרמיה: {1}", travel5YearsCount.ToString(), travel5YearPremium.ToString("0.##"));
                        ttTravel.ClearValue(ForegroundProperty);
                        brTravel.Background = darkBlue;
                        lblTravel.Foreground = Brushes.White;
                    }

                    if (elementary5YearsClaimsCount > 0 || life5YearsClaimsCount > 0 || health5YearsClaimsCount > 0 || personalAccidents5YearsClaimsCount > 0 || travel5YearsClaimsCount > 0 || foreingWorkers5YearsClaimsCount > 0)
                    {
                        int claims5YearsCount = elementary5YearsClaimsCount + life5YearsClaimsCount + health5YearsClaimsCount + personalAccidents5YearsClaimsCount + travel5YearsClaimsCount + foreingWorkers5YearsClaimsCount;
                        decimal claims5YearsAmount = elementary5YearsClaims + life5YearsClaims + health5YearsClaims + personalAccidents5YearsClaims + travel5YearsClaims + foreingWorkers5YearsClaims;
                        ttClaims.Content = string.Format("סה''כ תביעות: {0}\nסה''כ נתבע: {1}", claims5YearsCount.ToString(), claims5YearsAmount.ToString("0.##"));
                        ttClaims.Foreground = Brushes.Red;
                        if (elementary5YearsClaimsCount > 0)
                        {
                            ttElementaryClaims.Content = string.Format("סה''כ תביעות: {0}\nסה''כ נתבע: {1}", elementary5YearsClaimsCount.ToString(), elementary5YearsClaims.ToString("0.##"));
                            ttElementaryClaims.Foreground = Brushes.Red;
                            brElementaryClaims.Background = turkiz;
                        }
                        if (life5YearsClaimsCount > 0)
                        {
                            ttLifeClaims.Content = string.Format("סה''כ תביעות: {0}\nסה''כ נתבע: {1}", life5YearsClaimsCount.ToString(), life5YearsClaims.ToString("0.##"));
                            ttLifeClaims.Foreground = Brushes.Red;
                            brLifeClaims.Background = turkiz;
                        }
                        if (health5YearsClaimsCount > 0)
                        {
                            ttHealthClaims.Content = string.Format("סה''כ תביעות: {0}\nסה''כ נתבע: {1}", health5YearsClaimsCount.ToString(), health5YearsClaims.ToString("0.##"));
                            ttHealthClaims.Foreground = Brushes.Red;
                            brHealthClaims.Background = turkiz;
                        }
                        if (personalAccidents5YearsClaimsCount > 0)
                        {
                            ttPersonalAccidentsClaims.Content = string.Format("סה''כ תביעות: {0}\nסה''כ נתבע: {1}", personalAccidents5YearsClaimsCount.ToString(), personalAccidents5YearsClaims.ToString("0.##"));
                            ttPersonalAccidentsClaims.Foreground = Brushes.Red;
                            brPersonalAccidentsClaims.Background = turkiz;
                        }
                        if (travel5YearsClaimsCount > 0)
                        {
                            ttTravelClaims.Content = string.Format("סה''כ תביעות: {0}\nסה''כ נתבע: {1}", travel5YearsClaimsCount.ToString(), travel5YearsClaims.ToString("0.##"));
                            ttTravelClaims.Foreground = Brushes.Red;
                            brTravelClaims.Background = turkiz;
                        }
                        if (foreingWorkers5YearsClaimsCount > 0)
                        {
                            ttForeingWorkersClaims.Content = string.Format("סה''כ תביעות: {0}\nסה''כ נתבע: {1}", foreingWorkers5YearsClaimsCount.ToString(), foreingWorkers5YearsClaims.ToString("0.##"));
                            ttForeingWorkersClaims.Foreground = Brushes.Red;
                            brForeingWorkersClaims.Background = turkiz;
                        }
                    }
                    if (elementaryOffers5Years > 0 || personalAccidentsOffers5Years > 0 || travelOffers5Years > 0 || lifeOffers5Years > 0 || healthOffers5Years > 0 || financeOffers5Years > 0 || foreingWorkersOffers5Years > 0)
                    {
                        int offersCount = elementaryOffers5Years + personalAccidentsOffers5Years + travelOffers5Years + lifeOffers5Years + healthOffers5Years + financeOffers5Years + foreingWorkersOffers5Years;
                        txtOffers.Text = offersCount.ToString("D");
                        ttOffers.Content = string.Format("סה''כ הצעות פתוחות: {0}", offersCount.ToString("D"));
                        ttOffers.ClearValue(ForegroundProperty);

                        if (elementaryOffers5Years > 0)
                        {
                            ttElementaryOffers.Content = string.Format("סה''כ הצעות אלמנטרי: {0}", elementaryOffers5Years.ToString("D"));
                            ttElementaryOffers.ClearValue(ForegroundProperty);
                            brElementaryOffers.Background = lightGreen;
                        }
                        if (personalAccidentsOffers5Years > 0)
                        {
                            ttPersonalAccidentsOffers.Content = string.Format("סה''כ הצעות תאונות אישיות: {0}", personalAccidentsOffers5Years.ToString("D"));
                            ttPersonalAccidentsOffers.ClearValue(ForegroundProperty);
                            brPersonalAccidentsOffers.Background = lightGreen;
                        }
                        if (travelOffers5Years > 0)
                        {
                            ttTravelOffers.Content = string.Format("סה''כ הצעות חו''ל: {0}", travelOffers5Years.ToString("D"));
                            ttTravelOffers.ClearValue(ForegroundProperty);
                            brTravelOffers.Background = lightGreen;
                        }
                        if (lifeOffers5Years > 0)
                        {
                            ttLifeOffers.Content = string.Format("סה''כ הצעות חיים: {0}", lifeOffers5Years.ToString("D"));
                            ttLifeOffers.ClearValue(ForegroundProperty);
                            brLifeOffers.Background = lightGreen;
                        }
                        if (healthOffers5Years > 0)
                        {
                            ttHealthOffers.Content = string.Format("סה''כ הצעות בריאות: {0}", healthOffers5Years.ToString("D"));
                            ttHealthOffers.ClearValue(ForegroundProperty);
                            brHealthOffers.Background = lightGreen;
                        }
                        if (financeOffers5Years > 0)
                        {
                            ttFinanceOffers.Content = string.Format("סה''כ הצעות פיננסים: {0}", financeOffers5Years.ToString("D"));
                            ttFinanceOffers.ClearValue(ForegroundProperty);
                            brFinanceOffers.Background = lightGreen;
                        }
                        if (foreingWorkersOffers5Years > 0)
                        {
                            ttForeingWorkersOffers.Content = string.Format("סה''כ הצעות עובדים זרים: {0}", foreingWorkersOffers5Years.ToString("D"));
                            ttForeingWorkersOffers.ClearValue(ForegroundProperty);
                            brForeingWorkersOffers.Background = lightGreen;
                        }
                    }
                }
                else
                {
                    txtElementaryPremium.Text = elementaryTotalPremium.ToString("0.##");
                    txtElementaryClaims.Text = elementaryTotalClaims.ToString("0.##");
                    txtLifePremium.Text = lifeTotalPremium.ToString("0.##");
                    txtLifeClaims.Text = lifeTotalClaims.ToString("0.##");
                    txtHealthPremium.Text = healthTotalPremium.ToString("0.##");
                    txtHealthClaims.Text = healthTotalClaims.ToString("0.##");
                    txtPersonalAccidentsPremium.Text = personalAccidentsTotalPremium.ToString("0.##");
                    txtPersonalAccidentsClaims.Text = personalAccidentsTotalClaims.ToString("0.##");
                    txtFinancePremium.Text = financeTotalPremium.ToString("0.##");
                    txtTravelPremium.Text = travelTotalPremium.ToString("0.##");
                    txtTravelClaims.Text = travelTotalClaims.ToString("0.##");
                    txtForeingWorkersPremium.Text = foreingWorkersTotalPremium.ToString("0.##");
                    txtForeingWorkersClaims.Text = foreingWorkersTotalClaims.ToString("0.##");

                    if (elementaryTotalPremium > 0)
                    {
                        int elementaryCount = jobaCount + carCount + jabilaCount + aptCount + businessCount;
                        ttElementary.Content = string.Format("סה''כ פוליסות: {0}\nסה''כ פרמיה: {1}", elementaryCount.ToString(), elementaryTotalPremium.ToString("0.##"));
                        ttElementary.ClearValue(ForegroundProperty);
                        if (jobaCount > 0 || carCount > 0 || jabilaCount > 0)
                        {
                            int totalCarCount = jobaCount + carCount + jabilaCount;
                            decimal totalCarPremium = jobaPremium + carPremium + jabilaPremium;
                            ttCar.Content = string.Format("סה''כ פוליסות: {0}\nסה''כ פרמיה: {1}", totalCarCount.ToString(), totalCarPremium.ToString("0.##"));
                            ttCar.ClearValue(ForegroundProperty);
                            brCar.Background = darkBlue;
                            lblCar.Foreground = Brushes.White;
                        }
                        if (aptCount > 0)
                        {
                            ttApartment.Content = string.Format("סה''כ פוליסות: {0}\nסה''כ פרמיה: {1}", aptCount.ToString(), aptPremium.ToString("0.##"));
                            ttApartment.ClearValue(ForegroundProperty);
                            brApartment.Background = darkBlue;
                            lblApartment.Foreground = Brushes.White;
                        }
                        if (businessCount > 0)
                        {
                            ttBusiness.Content = string.Format("סה''כ פוליסות: {0}\nסה''כ פרמיה: {1}", businessCount.ToString(), businessPremium.ToString("0.##"));
                            ttBusiness.ClearValue(ForegroundProperty);
                            brBusiness.Background = darkBlue;
                            lblBusiness.Foreground = Brushes.White;
                        }
                    }
                    if (lifeTotalPremium > 0)
                    {
                        int lifeCount = managersCount + riskCount + mortgageCount + privateCount + pensionCount;
                        ttLife.Content = string.Format("סה''כ פוליסות: {0}\nסה''כ פרמיה: {1}", lifeCount.ToString(), lifeTotalPremium.ToString("0.##"));
                        ttLife.ClearValue(ForegroundProperty);
                        if (managersCount > 0)
                        {
                            ttManagers.Content = string.Format("סה''כ פוליסות: {0}\nסה''כ פרמיה: {1}", managersCount.ToString(), managersPremium.ToString("0.##"));
                            ttManagers.ClearValue(ForegroundProperty);
                            brManagers.Background = darkBlue;
                            lblManagers.Foreground = Brushes.White;
                        }
                        if (riskCount > 0)
                        {
                            ttRisks.Content = string.Format("סה''כ פוליסות: {0}\nסה''כ פרמיה: {1}", riskCount.ToString(), riskPremium.ToString("0.##"));
                            ttRisks.ClearValue(ForegroundProperty);
                            brRisks.Background = darkBlue;
                            lblRisk.Foreground = Brushes.White;
                        }
                        if (mortgageCount > 0)
                        {
                            ttMortgage.Content = string.Format("סה''כ פוליסות: {0}\nסה''כ פרמיה: {1}", mortgageCount.ToString(), mortgagePremium.ToString("0.##"));
                            ttMortgage.ClearValue(ForegroundProperty);
                            brMortgage.Background = darkBlue;
                            lblMortgage.Foreground = Brushes.White;
                        }
                        if (privateCount > 0)
                        {
                            ttPrivate.Content = string.Format("סה''כ פוליסות: {0}\nסה''כ פרמיה: {1}", privateCount.ToString(), privatePremium.ToString("0.##"));
                            ttPrivate.ClearValue(ForegroundProperty);
                            brPrivate.Background = darkBlue;
                            lblPrivate.Foreground = Brushes.White;
                        }
                        if (pensionCount > 0)
                        {
                            ttPension.Content = string.Format("סה''כ פוליסות: {0}\nסה''כ פרמיה: {1}", pensionCount.ToString(), pensionPremium.ToString("0.##"));
                            ttPension.ClearValue(ForegroundProperty);
                            brPension.Background = darkBlue;
                            lblPension.Foreground = Brushes.White;
                        }
                    }
                    if (healthTotalPremium > 0)
                    {
                        ttHealth.Content = string.Format("סה''כ פוליסות: {0}\nסה''כ פרמיה: {1}", healthCount.ToString(), healthTotalPremium.ToString("0.##"));
                        ttHealth.ClearValue(ForegroundProperty);
                        brHealth.Background = darkBlue;
                        lblHealth.Foreground = Brushes.White;
                    }
                    if (personalAccidentsTotalPremium > 0)
                    {
                        ttPersonalAccidents.Content = string.Format("סה''כ פוליסות: {0}\nסה''כ פרמיה: {1}", personalAccidentsCount.ToString(), personalAccidentsTotalPremium.ToString("0.##"));
                        ttPersonalAccidents.ClearValue(ForegroundProperty);
                        brPersonalAccidents.Background = darkBlue;
                        lblPersonalAccidents.Foreground = Brushes.White;
                    }
                    if (financeTotalPremium > 0)
                    {
                        int financeCount = kupatGuemelCount + kerenHishtalmutCount + invesmentPortfolioCount;
                        ttFinance.Content = string.Format("סה''כ פוליסות: {0}\nסה''כ פרמיה: {1}", financeCount.ToString(), financeTotalPremium.ToString("0.##"));
                        ttFinance.ClearValue(ForegroundProperty);
                        if (kupatGuemelCount > 0)
                        {
                            ttKupatGuemel.Content = string.Format("סה''כ פוליסות: {0}\nסה''כ פרמיה: {1}", kupatGuemelCount.ToString(), kupatGuemelPremium.ToString("0.##"));
                            ttKupatGuemel.ClearValue(ForegroundProperty);
                            brKupatGuemel.Background = darkBlue;
                            lblKupatGuemel.Foreground = Brushes.White;
                        }
                        if (kerenHishtalmutCount > 0)
                        {
                            ttKerenHishtalmut.Content = string.Format("סה''כ פוליסות: {0}\nסה''כ פרמיה: {1}", kerenHishtalmutCount.ToString(), kerenHishtalmutPremium.ToString("0.##"));
                            ttKerenHishtalmut.ClearValue(ForegroundProperty);
                            brKerenHishtalmut.Background = darkBlue;
                            lblKerenHishtalmut.Foreground = Brushes.White;
                        }
                        if (invesmentPortfolioCount > 0)
                        {
                            ttInvesmentPortfolio.Content = string.Format("סה''כ פוליסות: {0}\nסה''כ פרמיה: {1}", invesmentPortfolioCount.ToString(), invesmentPortfolioPremium.ToString("0.##"));
                            ttInvesmentPortfolio.ClearValue(ForegroundProperty);
                            brInvesmentPortfolio.Background = darkBlue;
                            lblInvesmentFolder.Foreground = Brushes.White;
                        }
                    }
                    if (foreingWorkersTotalPremium > 0)
                    {
                        int foreingWorkersCount = fWorkersCount + touristsCount;
                        ttForeingWorkers.Content = string.Format("סה''כ פוליסות: {0}\nסה''כ פרמיה: {1}", foreingWorkersCount.ToString(), foreingWorkersTotalPremium.ToString("0.##"));
                        ttForeingWorkers.ClearValue(ForegroundProperty);
                        brForeingWorkers.Background = darkBlue;
                        lblForeingWorkers.Foreground = Brushes.White;
                    }
                    if (travelTotalPremium > 0)
                    {
                        ttTravel.Content = string.Format("סה''כ פוליסות: {0}\nסה''כ פרמיה: {1}", travelCount.ToString(), travelTotalPremium.ToString("0.##"));
                        ttTravel.ClearValue(ForegroundProperty);
                        brTravel.Background = darkBlue;
                        lblTravel.Foreground = Brushes.White;
                    }

                    if (elementaryClaimCount > 0 || lifeClaimsCount > 0 || healthClaimsCount > 0 || personalAccidentsClaimsCount > 0 || travelClaimsCount > 0||foreingWorkersClaimCount>0)
                    {
                        int claimsCount = elementaryClaimCount + lifeClaimsCount + healthClaimsCount + personalAccidentsClaimsCount + travelClaimsCount+ foreingWorkersClaimCount;
                        decimal claimsAmount = elementaryTotalClaims + lifeTotalClaims + healthTotalClaims + personalAccidentsTotalClaims + travelTotalClaims +foreingWorkersTotalClaims;
                        ttClaims.Content = string.Format("סה''כ תביעות: {0}\nסה''כ נתבע: {1}", claimsCount.ToString(), claimsAmount.ToString("0.##"));
                        ttClaims.Foreground = Brushes.Red;
                        if (elementaryClaimCount > 0)
                        {
                            ttElementaryClaims.Content = string.Format("סה''כ תביעות: {0}\nסה''כ נתבע: {1}", elementaryClaimCount.ToString(), elementaryTotalClaims.ToString("0.##"));
                            ttElementaryClaims.Foreground = Brushes.Red;
                            brElementaryClaims.Background = turkiz;
                        }
                        if (lifeClaimsCount > 0)
                        {
                            ttLifeClaims.Content = string.Format("סה''כ תביעות: {0}\nסה''כ נתבע: {1}", lifeClaimsCount.ToString(), lifeTotalClaims.ToString("0.##"));
                            ttLifeClaims.Foreground = Brushes.Red;
                            brLifeClaims.Background = turkiz;
                        }
                        if (healthClaimsCount > 0)
                        {
                            ttHealthClaims.Content = string.Format("סה''כ תביעות: {0}\nסה''כ נתבע: {1}", healthClaimsCount.ToString(), healthTotalClaims.ToString("0.##"));
                            ttHealthClaims.Foreground = Brushes.Red;
                            brHealthClaims.Background = turkiz;
                        }
                        if (personalAccidentsClaimsCount > 0)
                        {
                            ttPersonalAccidentsClaims.Content = string.Format("סה''כ תביעות: {0}\nסה''כ נתבע: {1}", personalAccidentsClaimsCount.ToString(), personalAccidentsTotalClaims.ToString("0.##"));
                            ttPersonalAccidentsClaims.Foreground = Brushes.Red;
                            brPersonalAccidentsClaims.Background = turkiz;
                        }
                        if (travelClaimsCount > 0)
                        {
                            ttTravelClaims.Content = string.Format("סה''כ תביעות: {0}\nסה''כ נתבע: {1}", travelClaimsCount.ToString(), travelTotalClaims.ToString("0.##"));
                            ttTravelClaims.Foreground = Brushes.Red;
                            brTravelClaims.Background = turkiz;
                        }
                        if (foreingWorkersClaimCount > 0)
                        {
                            ttForeingWorkersClaims.Content = string.Format("סה''כ תביעות: {0}\nסה''כ נתבע: {1}", foreingWorkersClaimCount.ToString(), foreingWorkersTotalClaims.ToString("0.##"));
                            ttForeingWorkersClaims.Foreground = Brushes.Red;
                            brForeingWorkersClaims.Background = turkiz;
                        }
                    }
                    if (elementaryOffersAll > 0 || personalAccidentsOffersAll > 0 || travelOffersAll > 0 || lifeOffersAll > 0 || healthOffersAll > 0 || financeOffersAll > 0 || foreingWorkersOffersAll > 0)
                    {
                        int offersCount = elementaryOffersAll + personalAccidentsOffersAll + travelOffersAll + lifeOffersAll + healthOffersAll + financeOffersAll + foreingWorkersOffersAll;
                        txtOffers.Text = offersCount.ToString("D");
                        ttOffers.Content = string.Format("סה''כ הצעות פתוחות: {0}", offersCount.ToString("D"));
                        ttOffers.ClearValue(ForegroundProperty);

                        if (elementaryOffersAll > 0)
                        {
                            ttElementaryOffers.Content = string.Format("סה''כ הצעות אלמנטרי: {0}", elementaryOffersAll.ToString("D"));
                            ttElementaryOffers.ClearValue(ForegroundProperty);
                            brElementaryOffers.Background = lightGreen;
                        }
                        if (personalAccidentsOffersAll > 0)
                        {
                            ttPersonalAccidentsOffers.Content = string.Format("סה''כ הצעות תאונות אישיות: {0}", personalAccidentsOffersAll.ToString("D"));
                            ttPersonalAccidentsOffers.ClearValue(ForegroundProperty);
                            brPersonalAccidentsOffers.Background = lightGreen;
                        }
                        if (travelOffersAll > 0)
                        {
                            ttTravelOffers.Content = string.Format("סה''כ הצעות חו''ל: {0}", travelOffersAll.ToString("D"));
                            ttTravelOffers.ClearValue(ForegroundProperty);
                            brTravelOffers.Background = lightGreen;
                        }
                        if (lifeOffersAll > 0)
                        {
                            ttLifeOffers.Content = string.Format("סה''כ הצעות חיים: {0}", lifeOffersAll.ToString("D"));
                            ttLifeOffers.ClearValue(ForegroundProperty);
                            brLifeOffers.Background = lightGreen;
                        }
                        if (healthOffersAll > 0)
                        {
                            ttHealthOffers.Content = string.Format("סה''כ הצעות בריאות: {0}", healthOffersAll.ToString("D"));
                            ttHealthOffers.ClearValue(ForegroundProperty);
                            brHealthOffers.Background = lightGreen;
                        }
                        if (financeOffersAll > 0)
                        {
                            ttFinanceOffers.Content = string.Format("סה''כ הצעות פיננסים: {0}", financeOffersAll.ToString("D"));
                            ttFinanceOffers.ClearValue(ForegroundProperty);
                            brFinanceOffers.Background = lightGreen;
                        }
                        if (foreingWorkersOffersAll > 0)
                        {
                            ttForeingWorkersOffers.Content = string.Format("סה''כ הצעות עובדים זרים: {0}", foreingWorkersOffersAll.ToString("D"));
                            ttForeingWorkersOffers.ClearValue(ForegroundProperty);
                            brForeingWorkersOffers.Background = lightGreen;
                        }
                    }
                }
            }
        }

        private void rctPicture_Drop(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                // Note that you can have more than one file.
                string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);

                // Assuming you have one file that you care about, pass it off to whatever
                // handling code you have defined.
                HandleFileOpen(files[0]);
            }
        }

        private void HandleFileOpen(string file)
        {
            string picturePath = "";
            if (File.Exists(file))
            {
                if (Directory.Exists(clientGeneralPath))
                {
                    string pictureName = System.IO.Path.GetFileName(file);
                    picturePath = clientGeneralPath + @"\" + pictureName;
                    if (!File.Exists(picturePath))
                    {
                        File.Copy(file, picturePath);
                    }
                    txtPictureText.Text = "";
                }

                PictureBinding(picturePath);
                clientLogic.InsertPicture(client.ClientID, picturePath);
            }
        }

        private void PictureBinding(string filePath)
        {
            if (filePath != "" && filePath != null)
            {
                if (File.Exists(filePath))
                {
                    Uri imageUri = new Uri(filePath, UriKind.Relative);
                    BitmapImage imageBitmap = new BitmapImage(imageUri);
                    imgClientPicture.ImageSource = imageBitmap;
                }
                else
                {
                    RemovePicture();
                    MessageBox.Show("לא נמצא נתיב התמונה");
                }
            }
        }

        private void mItemDeletePicture_Click(object sender, RoutedEventArgs e)
        {
            RemovePicture();
        }

        private void RemovePicture()
        {
            txtPictureText.Text = "גרור תמונה כאן";
            imgClientPicture.ImageSource = null;
            clientLogic.InsertPicture(client.ClientID, "");
        }

        private void hypReferedByClientCount_MouseEnter(object sender, MouseEventArgs e)
        {
            Point mousePoint = this.PointToScreen(Mouse.GetPosition(this));
            win = new frmReferedByClientList(referedByClient);
            win.Left = mousePoint.X;
            win.Top = mousePoint.Y;
            win.Show();
        }

        private void hypReferedByClientCount_MouseLeave(object sender, MouseEventArgs e)
        {
            win.Close();
        }

        private void hypReferedByClientCount_Click_1(object sender, RoutedEventArgs e)
        {
            Point mousePoint = this.PointToScreen(Mouse.GetPosition(this));
            win = new frmReferedByClientList(referedByClient);
            win.Left = mousePoint.X;
            win.Top = mousePoint.Y;
            win.ShowDialog();
        }

        private void tbPolicies_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            DisplayProfitabilityData();
        }

        private void btnClientDetails_Click(object sender, RoutedEventArgs e)
        {
            frmAddClient clientDetailsWindow = new frmAddClient(client,user);
            clientDetailsWindow.ShowDialog();
            if (clientDetailsWindow.clientUpdated != null)
            {
                client = clientDetailsWindow.clientUpdated;
                RefreshClientDetails();
            }
        }

        private void rctPicture_MouseEnter(object sender, MouseEventArgs e)
        {
            if (imgClientPicture.ImageSource != null)
            {
                Point mousePoint = this.PointToScreen(Mouse.GetPosition(this));
                imageWindow = new frmClientImage(imgClientPicture.ImageSource);
                imageWindow.Left = mousePoint.X;
                imageWindow.Top = mousePoint.Y;                
                imageWindow.Show();
            }
        }

        private void rctPicture_MouseLeave(object sender, MouseEventArgs e)
        {
            if (imageWindow!=null&&imageWindow.IsActive)
            {
                imageWindow.Close();
            }            //if (imgClientPicture.ImageSource != null)
            //{
            //    rctPicture.Height = 148;
            //    rctPicture.Width = 100;
            //}
        }

        private void rctPicture_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (imageWindow != null)
            {
                imageWindow.Close();
            }
        }
    }
}
