﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using GdPicture10;

namespace CoralUI
{
    /// <summary>
    /// Interaction logic for frmScan.xaml
    /// </summary>
    public partial class frmScan : Window
    {
        public frmScan()
        {
            InitializeComponent();
            this.SizeToContent = SizeToContent.Manual;
            //Creating a License Manager object
            LicenseManager oLicenseManager = new LicenseManager();
            oLicenseManager.RegisterKEY("132999999928386951626123255863780");
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            //GdViewer displayFile = new GdViewer();            
            //dpScan.Children.Add(displayFile);
            //DockPanel.SetDock(displayFile, Dock.Left);
            //myDockPanel.Children.Add(myControl);
        }
    }
}
