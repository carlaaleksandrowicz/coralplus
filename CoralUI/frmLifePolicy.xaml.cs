﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using CoralBusinessLogics;
using System.Xml;
using System.Windows.Markup;
using System.IO;

namespace CoralUI
{
    /// <summary>
    /// Interaction logic for frmLifePolicy.xaml
    /// </summary>
    public partial class frmLifePolicy : Window
    {
        Client client = null;
        User handlerUser = null;
        InsurancesLogic insuranceLogic = new InsurancesLogic();
        LifePoliciesLogic lifePolicyLogic = new LifePoliciesLogic();
        LifeIndustryLogic lifeIndustryLogic = new LifeIndustryLogic();
        AgentsLogic agentLogic = new AgentsLogic();
        PolicyHoldersLogic policyHoldersLogic = new PolicyHoldersLogic();
        InvestmentPlansLogic investmentPlansLogics = new InvestmentPlansLogic();
        FundsLogic fundsLogics = new FundsLogic();
        InsuranceWaiversLogic insuranceWaiverLogics = new InsuranceWaiversLogic();
        RetirementPlansLogic retirementPlansLogic = new RetirementPlansLogic();
        //List<TextBox> numbersErrorList = null;
        List<object> nullErrorList = null;
        InputsValidations validations = new InputsValidations();
        List<ManagerPolicyChanx> salaryChanges = new List<ManagerPolicyChanx>();
        decimal salary = 0;
        decimal employerCompensation = 0;
        decimal employerBenefits = 0;
        decimal employerOthers = 0;
        decimal employerDesability = 0;
        decimal compensationPremium = 0;
        decimal benefitPremium = 0;
        decimal disabilityPremium = 0;
        decimal otherEmployerPremium = 0;
        decimal workerBenefits45 = 0;
        decimal workerBenefits47 = 0;
        decimal workerOthers = 0;
        decimal benefits45Premium = 0;
        decimal benefits47Premium = 0;
        decimal otherWorkerPremium = 0;
        ManagerPolicyChanx salarySelected = null;
        List<LifePolicyTransfer> transfers = new List<LifePolicyTransfer>();
        decimal transferingAmount = 0;
        LifePolicyTransfer transferSelected = null;
        List<Risk> risks = new List<Risk>();
        List<LifeAdditionalInsured> additionalsInsureds = new List<LifeAdditionalInsured>();
        List<Mortgage> mortgages = new List<Mortgage>();
        Mortgage mortgageSelected = null;
        LifePolicy lifePolicy = null;
        int policyId;
        List<LifeCoverage> coverages = new List<LifeCoverage>();
        CoveragesLogic coveragesLogic = new CoveragesLogic();
        List<LifeTracking> trackings = new List<LifeTracking>();
        TrackingsLogics trackingLogics = new TrackingsLogics();
        bool isAddition = false;
        bool isMortgageAddition;
        ListViewSettings lv = new ListViewSettings();
        string path = null;
        string newPath = null;
        string clientDirPath = null;
        string lifePath = null;
        bool isOffer;
        private bool isChanges = false;
        bool confirmBeforeClosing = true;
        bool cancelClose = false;
        StandardMoneyCollection standardMoneyCollection;
        StandardMoneyCollectionLogic moneyCollectionLogic = new StandardMoneyCollectionLogic();
        private List<StandardMoneyCollectionTracking> standardMoneyCollectionTrackings;


        public frmLifePolicy(Client clientToAddPolicy, User user, bool isOffer)
        {
            InitializeComponent();
            this.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            client = clientToAddPolicy;
            handlerUser = user;
            this.isOffer = isOffer;
        }

        public frmLifePolicy(Client clientToAddPolicy, User user, bool isAddition, LifePolicy lifePolicyToUpdate)
        {
            InitializeComponent();
            client = clientToAddPolicy;
            handlerUser = user;
            lifePolicy = lifePolicyToUpdate;
            this.isAddition = isAddition;
        }

        public frmLifePolicy(bool isMortgageAddition, Client clientToAddPolicy, User user, LifePolicy lifePolicyToUpdate)
        {
            InitializeComponent();
            client = clientToAddPolicy;
            handlerUser = user;
            lifePolicy = lifePolicyToUpdate;
            this.isMortgageAddition = isMortgageAddition;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            string strAppDir = System.AppDomain.CurrentDomain.BaseDirectory;
            string path = strAppDir + @"Coral Files\ClientsPath.txt";

            if (File.Exists(path))
            {
                clientDirPath = File.ReadAllText(path);
            }
            else
            {
                MessageBox.Show("לא נמצא תיקיית הלקוחות. נא בדוק בהגדרות ראשוניות את נתיב התיקייה");
                Close();
                return;
            }
            lifePath = clientDirPath + @"/" + client.ClientID;
            cbLifeIndustryBinding();
            cbCompanyBinding();
            cbPolicyHolderBinding();
            if (client != null)
            {
                string clientDetails;
                if (client.CellPhone != null && client.CellPhone != "")
                {
                    clientDetails = string.Format("{0} {1}  ת.ז: {2} נייד: {3}", client.FirstName, client.LastName, client.IdNumber, client.CellPhone);
                }
                else
                {
                    clientDetails = string.Format("{0} {1}  ת.ז: {2}", client.FirstName, client.LastName, client.IdNumber);
                }
                lblClientNameAndId.Content = clientDetails;
            }
            tbItemAdm.Visibility = Visibility.Collapsed;
            tbItemRisk.Visibility = Visibility.Collapsed;
            tbItemMortgage.Visibility = Visibility.Collapsed;
            tbItemPrivate.Visibility = Visibility.Collapsed;
            tbItemPension.Visibility = Visibility.Collapsed;
            tbItemCoverages.Visibility = Visibility.Collapsed;
            if (lifePolicy == null)
            {
                dpOpenDate.SelectedDate = DateTime.Now;
                dpStartDate.SelectedDate = DateTime.Now;
                txtAddition.Text = "0";
            }
            else
            {
                dpOpenDate.SelectedDate = lifePolicy.OpenDate;
                txtPolicyNumber.Text = lifePolicy.PolicyNumber;
                txtAddition.Text = lifePolicy.Addition.ToString();
                dpStartDate.SelectedDate = lifePolicy.StartDate;
                dpEndDate.SelectedDate = lifePolicy.EndDate;
                isOffer = lifePolicy.IsOffer;
                txtInsurancedName.Text = lifePolicy.InsuredName;
                txtInsurancedNumber.Text = lifePolicy.InsuredIdNumber;
                txtPrimaryIndex.Text = lifePolicy.PrimaryIndex;
                if (lifePolicy.SubAnnual != null)
                {
                    txtSubAnnual.Text = ((decimal)lifePolicy.SubAnnual).ToString("0.##");
                }
                if (lifePolicy.PolicyFactor != null)
                {
                    txtPolicyFactor.Text = ((decimal)lifePolicy.PolicyFactor).ToString("0.##");
                }
                if (lifePolicy.TotalMonthlyPayment != null)
                {
                    txtMonthlyDeposit.Text = ((decimal)lifePolicy.TotalMonthlyPayment).ToString("0.##");
                }
                txtBankName.Text = lifePolicy.BankPayment;
                txtBankBranch.Text = lifePolicy.BankBranchPayment;
                txtBankAccount.Text = lifePolicy.AccountNumberPayment;
                chbIsMortgaged.IsChecked = lifePolicy.IsMortgaged;
                txtMortgagedBank.Text = lifePolicy.BankName;
                txtMortgagedBranch.Text = lifePolicy.BankBranchNumber;
                txtMortgagedAddress.Text = lifePolicy.BankAddress;
                txtMortgagedBankPhone.Text = lifePolicy.BankPhone1;
                txtMortgagedBankFax.Text = lifePolicy.BankFax;
                txtMortgagedBankContactName.Text = lifePolicy.BankContactName;
                txtMortgagedBankEmail.Text = lifePolicy.BankEmail;
                txtCommentsGeneral.Text = lifePolicy.Comments;
                var principalAgentNumbers = cbPrincipalAgentNumber.Items;
                foreach (var item in principalAgentNumbers)
                {
                    AgentNumber principalAgentNumber = (AgentNumber)item;
                    if (principalAgentNumber.AgentNumberID == lifePolicy.PrincipalAgentNumberID)
                    {
                        cbPrincipalAgentNumber.SelectedItem = item;
                        break;
                    }
                }
                var secundaryAgentNumbers = cbSecundaryAgentNumber.Items;
                foreach (var item in secundaryAgentNumbers)
                {
                    AgentNumber secundaryAgentNumber = (AgentNumber)item;
                    if (secundaryAgentNumber.AgentNumberID == lifePolicy.SubAgentNumberID)
                    {
                        cbSecundaryAgentNumber.SelectedItem = item;
                        break;
                    }
                }
                var paymentMethods = cbPaymentMode.Items;
                foreach (var item in paymentMethods)
                {
                    if (lifePolicy.PaymentMethod == ((ComboBoxItem)item).Content.ToString())
                    {
                        cbPaymentMode.SelectedItem = item;
                        break;
                    }
                }
                var industries = cbLifeIndustry.Items;
                foreach (var item in industries)
                {
                    LifeIndustry industry = (LifeIndustry)item;
                    if (industry.LifeIndustryID == lifePolicy.LifeIndustryID)
                    {
                        cbLifeIndustry.SelectedItem = item;
                        if (industry.LifeIndustryName != "שגויים")
                        {
                            cbLifeIndustry.IsEnabled = false;
                        }
                        break;
                    }
                }

                if (lifePolicy.CompanyID != null)
                {
                    SelectItemInCb((int)lifePolicy.CompanyID, cbCompany);
                }
                var policyHolders = cbPolicyHolder.Items;
                foreach (var item in policyHolders)
                {
                    PolicyOwner holder = (PolicyOwner)item;
                    if (holder.PolicyOwnerID == lifePolicy.PolicyOwnerID)
                    {
                        cbPolicyHolder.SelectedItem = item;
                        break;
                    }
                }
                if (lifePolicy.IsIndexation == true)
                {
                    cbIndexation.SelectedIndex = 0;
                }
                else if (lifePolicy.IsIndexation == false)
                {
                    cbIndexation.SelectedIndex = 1;
                }
                coverages = coveragesLogic.GetAllLifeCoveragesByPolicy(lifePolicy.LifePolicyID);
                dgCoveragesBinding();
                trackings = trackingLogics.GetAllLifeTrackingsByPolicy(lifePolicy.LifePolicyID);
                dgTrackingsBinding();
               
                if (lifePolicy.Addition != 0)
                {
                    txtPolicyNumber.IsEnabled = false;
                }
                if (int.Parse(lifePolicyLogic.GetAdditionNumber(lifePolicy.PolicyNumber, lifePolicy.LifeIndustry)) - 1 > lifePolicy.Addition)
                {
                    dpGeneral.IsEnabled = false;
                    dpManager.IsEnabled = false;
                    dpRisk.IsEnabled = false;
                    dpMortgage.IsEnabled = false;
                    dpPrivate.IsEnabled = false;
                    dpPension.IsEnabled = false;
                    dpCoverage.IsEnabled = false;
                    dpTracking.IsEnabled = false;
                }
                if (isAddition)
                {
                    txtPolicyNumber.IsEnabled = false;
                    txtAddition.Text = lifePolicyLogic.GetAdditionNumber(lifePolicy.PolicyNumber, lifePolicy.LifeIndustry);
                    DateTime now = DateTime.Now;
                    dpOpenDate.SelectedDate = now;
                    dpStartDate.SelectedDate = now;
                    if (cbCompany.SelectedItem != null)
                    {
                        cbCompany.IsEnabled = false;
                    }

                }
                else
                {
                    standardMoneyCollection = moneyCollectionLogic.GetStandardMoneyColletionByPolicy(lifePolicy);
                    if (standardMoneyCollection != null)
                    {
                        chbStandardMoneyCollection.IsChecked = true;
                        chbStandardMoneyCollection.IsEnabled = false;
                        standardMoneyCollectionTrackings = moneyCollectionLogic.GetTrakcingsByStsndardMoneyCollection(standardMoneyCollection);
                    }
                }
                if (isMortgageAddition)
                {
                    txtPolicyNumber.Clear();
                    mortgages.Clear();
                    dgMortgagesBinding();
                    coverages.Clear();
                    dgCoveragesBinding();
                    trackings.Clear();
                    dgTrackingsBinding();
                }
            }
        }
        private void chbIsMortgaged_Checked(object sender, RoutedEventArgs e)
        {
            spMortgagedBankDetails.IsEnabled = true;
        }

        private void chbIsMortgaged_Unchecked(object sender, RoutedEventArgs e)
        {
            spMortgagedBankDetails.IsEnabled = false;
            txtMortgagedBank.Clear();
            txtMortgagedBranch.Clear();
            txtMortgagedAddress.Clear();
            txtMortgagedBankPhone.Clear();
            txtMortgagedBankFax.Clear();
            txtMortgagedBankContactName.Clear();
            txtMortgagedBankEmail.Clear();
        }



        private void cbPolicyHolderBinding()
        {
            cbPolicyHolder.ItemsSource = policyHoldersLogic.GetAllPolicyHolders();
            cbPolicyHolder.DisplayMemberPath = "PolicyOwnerName";
        }

        private void cbLifeIndustryBinding()
        {
            cbLifeIndustry.ItemsSource = lifeIndustryLogic.GetAllLifeIndustries();
            cbLifeIndustry.DisplayMemberPath = "LifeIndustryName";
        }

        private void cbCompanyBinding()
        {
            cbCompany.ItemsSource = insuranceLogic.GetActiveCompaniesByInsurance("חיים");
            cbCompany.DisplayMemberPath = "Company.CompanyName";
        }

        private void cbInsuranceTypeBinding()
        {
            cbInsuranceType.ItemsSource = fundsLogics.GetActiveFundTypesByInsuranceAndCompany(insuranceLogic.GetInsuranceID("חיים"), ((LifeIndustry)cbLifeIndustry.SelectedItem).LifeIndustryID, ((InsuranceCompany)cbCompany.SelectedItem).CompanyID);
            cbInsuranceType.DisplayMemberPath = "FundTypeName";
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void btnUpdateCompanyTable_Click(object sender, RoutedEventArgs e)
        {
            int? selectedItemId = null;
            if (cbCompany.SelectedItem != null)
            {
                selectedItemId = ((InsuranceCompany)cbCompany.SelectedItem).CompanyID;
            }
            int insuranceID = insuranceLogic.GetInsuranceID("חיים");
            InsuranceCompany company = new InsuranceCompany() { InsuranceID = insuranceID };
            frmUpdateTable updateCompaniesTable = new frmUpdateTable(company);
            updateCompaniesTable.ShowDialog();
            cbCompanyBinding();
            if (updateCompaniesTable.ItemAdded != null)
            {
                SelectItemInCb(((InsuranceCompany)updateCompaniesTable.ItemAdded).CompanyID, cbCompany);
            }
            else if (selectedItemId != null)
            {
                SelectItemInCb((int)selectedItemId, cbCompany);
            }
        }

        private void SelectItemInCb(int id, ComboBox cb)
        {
            var items = cb.Items;
            foreach (var item in items)
            {
                InsuranceCompany parsedItem = (InsuranceCompany)item;
                if (parsedItem.CompanyID == id)
                {
                    cb.SelectedItem = item;
                    break;
                }
            }
        }
        private void cbLifeIndustry_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            LifeIndustry industrySelected = (LifeIndustry)cbLifeIndustry.SelectedItem;
            if (industrySelected == null)
            {
                return;
            }
            if (cbCompany.SelectedItem != null)
            {
                cbInsuranceType.IsEnabled = true;
                btnUpdateInsuranceTypeTable.IsEnabled = true;
                cbInsuranceTypeBinding();
                if (lifePolicy != null)
                {
                    //var insuranceTypes = cbInsuranceType.Items;
                    //foreach (var item in insuranceTypes)
                    //{
                    //    FundType insuranceType = (FundType)item;
                    //    if (insuranceType.FundTypeID == lifePolicy.FundTypeID)
                    //    {
                    //        cbInsuranceType.SelectedItem = item;
                    //        break;
                    //    }
                    //}
                    SelectFundItemInCb(lifePolicy.FundTypeID, cbInsuranceType);
                }
            }
            if (industrySelected.LifeIndustryName == "מנהלים")
            {
                txtPolicyNumber.IsEnabled = true;
                tbItemAdm.Visibility = Visibility.Visible;
                tbItemRisk.Visibility = Visibility.Collapsed;
                tbItemMortgage.Visibility = Visibility.Collapsed;
                tbItemPrivate.Visibility = Visibility.Collapsed;
                tbItemPension.Visibility = Visibility.Collapsed;
                FundsdAdmBinding();
                if (lifePolicy != null && lifePolicy.ManagerPolicy != null && lifePolicy.LifeIndustry.LifeIndustryName != "שגויים")
                {
                    //chbIsFundNotPaysCompensationAdm.IsChecked = lifePolicy.ManagerPolicy.IsFundNotPaysForCompensation;
                    chbIsCompulsoryPensionAdm.IsChecked = lifePolicy.ManagerPolicy.IsCompulsoryPension;
                    chbIsShareholderAdm.IsChecked = lifePolicy.ManagerPolicy.IsShareholder;
                    txtCommentsAdm.Text = lifePolicy.ManagerPolicy.Comments;
                    salaryChanges = lifePolicyLogic.GetSalaryChangesByPolicy(lifePolicy.LifePolicyID);
                    dgAdmPolicySalaryChangesBinding();
                    transfers = lifePolicyLogic.GetTransfersByPolicy(lifePolicy.LifePolicyID);
                    dgTransferencesAdmBinding();
                    risks = lifePolicyLogic.GetRisksByPolicy(lifePolicy.LifePolicyID);
                    if (risks.Count > 0)
                    {
                        chbIsRisksAdm.IsChecked = true;
                        rbDeath1Adm.IsChecked = risks[0].IsDeathType1;
                        rbDeath5Adm.IsChecked = risks[0].IsDeathType5;
                        rbDeathFixedAdm.IsChecked = risks[0].IsDeathTypeFixed;
                        if (risks[0].IsProfessionalWorkImpossibility == false)
                        {
                            rbNormalWorkImposibilitationAdm.IsChecked = true;
                        }
                        txtOther1Adm.Text = risks[0].Other1Name;
                        txtOther2Adm.Text = risks[0].Other2Name;
                        txtOther3Adm.Text = risks[0].Other3Name;
                        txtOther4Adm.Text = risks[0].Other4Name;
                        txtOther5Adm.Text = risks[0].Other5Name;
                        if (risks[0].DeathAmount != null)
                        {
                            txtDeathAmountAdm.Text = ((decimal)risks[0].DeathAmount).ToString("0.##");
                        }
                        if (risks[0].WorkImpossibilityAmount != null)
                        {
                            txtWorkImpossibilityAmountAdm.Text = ((decimal)risks[0].WorkImpossibilityAmount).ToString("0.##");
                        }
                        if (risks[0].SecureIncomeAmount != null)
                        {
                            txtIncomeSecureAmountAdm.Text = ((decimal)risks[0].SecureIncomeAmount).ToString("0.##");
                        }
                        if (risks[0].DisabilityByAccidentAmount != null)
                        {
                            txtDesabilityByAccidentAmountAdm.Text = ((decimal)risks[0].DisabilityByAccidentAmount).ToString("0.##");
                        }
                        txtDeathByAccidentAmountAdm.Text = validations.ConvertDecimalToString(risks[0].DeathByAccidentAmount);
                        txtCriticalIllnesesAmountAdm.Text = validations.ConvertDecimalToString(risks[0].CriticalIllnessAmount);
                        txtOther1AmountAdm.Text = validations.ConvertDecimalToString(risks[0].Other1Amount);
                        txtOther2AmountAdm.Text = validations.ConvertDecimalToString(risks[0].Other2Amount);
                        txtOther3AmountAdm.Text = validations.ConvertDecimalToString(risks[0].Other3Amount);
                        txtOther4AmountAdm.Text = validations.ConvertDecimalToString(risks[0].Other4Amount);
                        txtOther5AmountAdm.Text = validations.ConvertDecimalToString(risks[0].Other5Amount);
                        txtDeathPeriodAdm.Text = risks[0].DeathPeriod;
                        txtWorkImpossibilityPeriodAdm.Text = risks[0].WorkImpossibilityPeriod;
                        txtIncomeSecurePeriodAdm.Text = risks[0].SecureIncomePeriod;
                        txtDesabilityByAccidentPeriodAdm.Text = risks[0].DisabilityByAccidentPeriod;
                        txtDeathByAccidentPeriodAdm.Text = risks[0].DeathByAccidentPeriod;
                        txtCriticalIllnesesPeriodAdm.Text = risks[0].CriticalIllnessPeriod;
                        txtOther1PeriodAdm.Text = risks[0].Other1Period;
                        txtOther2PeriodAdm.Text = risks[0].Other2Period;
                        txtOther3PeriodAdm.Text = risks[0].Other3Period;
                        txtOther4PeriodAdm.Text = risks[0].Other4Period;
                        txtOther5PeriodAdm.Text = risks[0].Other5Period;
                        txtDeathCoverageTypeAdm.Text = risks[0].DeathCoverageType;
                        txtWorkImpossibilityCoverageTypeAdm.Text = risks[0].WorkImpossibilityCoverageType;
                        txtIncomeSecureCoverageTypeAdm.Text = risks[0].SecureIncomeCoverageType;
                        txtDesabilityByAccidentCoverageTypeAdm.Text = risks[0].DisabilityByAccidentCoverageType;
                        txtDeathByAccidentCoverageTypeAdm.Text = risks[0].DeathByAccidentCoverageType;
                        txtCriticalIllnesesCoverageTypeAdm.Text = risks[0].CriticalIllnessCoverageType;
                        txtOther1CoverageTypeAdm.Text = risks[0].Other1CoverageType;
                        txtOther2CoverageTypeAdm.Text = risks[0].Other2CoverageType;
                        txtOther3CoverageTypeAdm.Text = risks[0].Other3CoverageType;
                        txtOther4CoverageTypeAdm.Text = risks[0].Other4CoverageType;
                        txtOther5CoverageTypeAdm.Text = risks[0].Other5CoverageType;
                        txtDeathPremiumAdm.Text = validations.ConvertDecimalToString(risks[0].DeathPremium);
                        txtWorkImpossibilityPremiumAdm.Text = validations.ConvertDecimalToString(risks[0].WorkImpossibilityPremium);
                        txtIncomeSecurePremiumAdm.Text = validations.ConvertDecimalToString(risks[0].SecureIncomePremium);
                        txtDesabilityByAccidentPremiumAdm.Text = validations.ConvertDecimalToString(risks[0].DisabilityByAccidentPremium);
                        txtDeathByAccidentPremiumAdm.Text = validations.ConvertDecimalToString(risks[0].DeathByAccidentPremium);
                        txtCriticalIllnesesPremiumAdm.Text = validations.ConvertDecimalToString(risks[0].CriticalIllnessPremium);
                        txtOther1PremiumAdm.Text = validations.ConvertDecimalToString(risks[0].Other1Premium);
                        txtOther2PremiumAdm.Text = validations.ConvertDecimalToString(risks[0].Other2Premium);
                        txtOther3PremiumAdm.Text = validations.ConvertDecimalToString(risks[0].Other3Premium);
                        txtOther4PremiumAdm.Text = validations.ConvertDecimalToString(risks[0].Other4Premium);
                        txtOther5PremiumAdm.Text = validations.ConvertDecimalToString(risks[0].Other5Premium);
                        txtDeathCommentsAdm.Text = risks[0].DeathComments;
                        txtWorkImpossibilityCommentsAdm.Text = risks[0].WorkImpossibilityComments;
                        txtIncomeSecureCommentsAdm.Text = risks[0].SecureIncomeComments;
                        txtDesabilityByAccidentCommentsAdm.Text = risks[0].DisabilityByAccidentComments;
                        txtDeathByAccidentCommentsAdm.Text = risks[0].DeathByAccidentComments;
                        txtCriticalIllnesesCommentsAdm.Text = risks[0].CriticalIllnessComments;
                        txtOther1CommentsAdm.Text = risks[0].Other1Comments;
                        txtOther2CommentsAdm.Text = risks[0].Other2Comments;
                        txtOther3CommentsAdm.Text = risks[0].Other3Comments;
                        txtOther4CommentsAdm.Text = risks[0].Other4Comments;
                        txtOther5CommentsAdm.Text = risks[0].Other5Comments;
                    }
                }
            }
            else if (industrySelected.LifeIndustryName == "ריסק")
            {
                txtPolicyNumber.IsEnabled = true;
                tbItemAdm.Visibility = Visibility.Collapsed;
                tbItemRisk.Visibility = Visibility.Visible;
                tbItemMortgage.Visibility = Visibility.Collapsed;
                tbItemPrivate.Visibility = Visibility.Collapsed;
                tbItemPension.Visibility = Visibility.Collapsed;
                if (lifePolicy != null && lifePolicy.LifeIndustry.LifeIndustryName != "שגויים")
                {
                    risks = lifePolicyLogic.GetRisksByPolicy(lifePolicy.LifePolicyID);
                    if (risks.Count > 0)
                    {
                        chbIsRisksAdm.IsChecked = true;
                        rbDeath1Risk.IsChecked = risks[0].IsDeathType1;
                        rbDeath5Risk.IsChecked = risks[0].IsDeathType5;
                        rbDeathFixedRisk.IsChecked = risks[0].IsDeathTypeFixed;
                        if (risks[0].IsProfessionalWorkImpossibility == false)
                        {
                            rbNormalWorkImposibilitationRisk.IsChecked = true;
                        }
                        txtOther1.Text = risks[0].Other1Name;
                        txtOther2.Text = risks[0].Other2Name;
                        txtOther3.Text = risks[0].Other3Name;
                        txtOther4.Text = risks[0].Other4Name;
                        txtOther5.Text = risks[0].Other5Name;
                        txtDeathAmountRisk.Text = validations.ConvertDecimalToString(risks[0].DeathAmount);
                        txtWorkImpossibilityAmountRisk.Text = validations.ConvertDecimalToString(risks[0].WorkImpossibilityAmount);
                        txtIncomeSecureAmountRisk.Text = validations.ConvertDecimalToString(risks[0].SecureIncomeAmount);
                        txtDesabilityByAccidentAmountRisk.Text = validations.ConvertDecimalToString(risks[0].DisabilityByAccidentAmount);
                        txtDeathByAccidentAmountRisk.Text = validations.ConvertDecimalToString(risks[0].DeathByAccidentAmount);
                        txtCriticalIllnesesAmountRisk.Text = validations.ConvertDecimalToString(risks[0].CriticalIllnessAmount);
                        txtOther1AmountRisk.Text = validations.ConvertDecimalToString(risks[0].Other1Amount);
                        txtOther2AmountRisk.Text = validations.ConvertDecimalToString(risks[0].Other2Amount);
                        txtOther3AmountRisk.Text = validations.ConvertDecimalToString(risks[0].Other3Amount);
                        txtOther4AmountRisk.Text = validations.ConvertDecimalToString(risks[0].Other4Amount);
                        txtOther5AmountRisk.Text = validations.ConvertDecimalToString(risks[0].Other5Amount);
                        txtDeathPeriodRisk.Text = risks[0].DeathPeriod;
                        txtWorkImpossibilityPeriodRisk.Text = risks[0].WorkImpossibilityPeriod;
                        txtIncomeSecurePeriodRisk.Text = risks[0].SecureIncomePeriod;
                        txtDesabilityByAccidentPeriodRisk.Text = risks[0].DisabilityByAccidentPeriod;
                        txtDeathByAccidentPeriodRisk.Text = risks[0].DeathByAccidentPeriod;
                        txtCriticalIllnesesPeriodRisk.Text = risks[0].CriticalIllnessPeriod;
                        txtOther1PeriodRisk.Text = risks[0].Other1Period;
                        txtOther2PeriodRisk.Text = risks[0].Other2Period;
                        txtOther3PeriodRisk.Text = risks[0].Other3Period;
                        txtOther4PeriodRisk.Text = risks[0].Other4Period;
                        txtOther5PeriodRisk.Text = risks[0].Other5Period;
                        txtDeathCoverageTypeRisk.Text = risks[0].DeathCoverageType;
                        txtWorkImpossibilityCoverageTypeRisk.Text = risks[0].WorkImpossibilityCoverageType;
                        txtIncomeSecureCoverageTypeRisk.Text = risks[0].SecureIncomeCoverageType;
                        txtDesabilityByAccidentCoverageTypeRisk.Text = risks[0].DisabilityByAccidentCoverageType;
                        txtDeathByAccidentCoverageTypeRisk.Text = risks[0].DeathByAccidentCoverageType;
                        txtCriticalIllnesesCoverageTypeRisk.Text = risks[0].CriticalIllnessCoverageType;
                        txtOther1CoverageTypeRisk.Text = risks[0].Other1CoverageType;
                        txtOther2CoverageTypeRisk.Text = risks[0].Other2CoverageType;
                        txtOther3CoverageTypeRisk.Text = risks[0].Other3CoverageType;
                        txtOther4CoverageTypeRisk.Text = risks[0].Other4CoverageType;
                        txtOther5CoverageTypeRisk.Text = risks[0].Other5CoverageType;
                        txtDeathPremiumRisk.Text = validations.ConvertDecimalToString(risks[0].DeathPremium);
                        txtWorkImpossibilityPremiumRisk.Text = validations.ConvertDecimalToString(risks[0].WorkImpossibilityPremium);
                        txtIncomeSecurePremiumRisk.Text = validations.ConvertDecimalToString(risks[0].SecureIncomePremium);
                        txtDesabilityByAccidentPremiumRisk.Text = validations.ConvertDecimalToString(risks[0].DisabilityByAccidentPremium);
                        txtDeathByAccidentPremiumRisk.Text = validations.ConvertDecimalToString(risks[0].DeathByAccidentPremium);
                        txtCriticalIllnesesPremiumRisk.Text = validations.ConvertDecimalToString(risks[0].CriticalIllnessPremium);
                        txtOther1PremiumRisk.Text = validations.ConvertDecimalToString(risks[0].Other1Premium);
                        txtOther2PremiumRisk.Text = validations.ConvertDecimalToString(risks[0].Other2Premium);
                        txtOther3PremiumRisk.Text = validations.ConvertDecimalToString(risks[0].Other3Premium);
                        txtOther4PremiumRisk.Text = validations.ConvertDecimalToString(risks[0].Other4Premium);
                        txtOther5PremiumRisk.Text = validations.ConvertDecimalToString(risks[0].Other5Premium);
                        txtDeathCommentsRisk.Text = risks[0].DeathComments;
                        txtWorkImpossibilityCommentsRisk.Text = risks[0].WorkImpossibilityComments;
                        txtIncomeSecureCommentsRisk.Text = risks[0].SecureIncomeComments;
                        txtDesabilityByAccidentCommentsRisk.Text = risks[0].DisabilityByAccidentComments;
                        txtDeathByAccidentCommentsRisk.Text = risks[0].DeathByAccidentComments;
                        txtCriticalIllnesesCommentsRisk.Text = risks[0].CriticalIllnessComments;
                        txtOther1CommentsRisk.Text = risks[0].Other1Comments;
                        txtOther2CommentsRisk.Text = risks[0].Other2Comments;
                        txtOther3CommentsRisk.Text = risks[0].Other3Comments;
                        txtOther4CommentsRisk.Text = risks[0].Other4Comments;
                        txtOther5CommentsRisk.Text = risks[0].Other5Comments;
                        additionalsInsureds = lifePolicyLogic.GetAdditionalInsuredsByPolicy(lifePolicy.LifePolicyID);
                        if (additionalsInsureds.Count > 0)
                        {
                            tbItemInsurencedRisk2.Visibility = Visibility.Visible;
                            tbItemInsurencedRisk2.Header = additionalsInsureds[0].FirstName + " " + additionalsInsureds[0].LastName;

                            if (risks.Count > 1)
                            {
                                rbDeath1Risk2.IsChecked = risks[1].IsDeathType1;
                                rbDeath5Risk2.IsChecked = risks[1].IsDeathType5;
                                rbDeathFixedRisk2.IsChecked = risks[1].IsDeathTypeFixed;
                                if (risks[1].IsProfessionalWorkImpossibility == false)
                                {
                                    rbNormalWorkImposibilitationRisk2.IsChecked = true;
                                }
                                txtOther1_2.Text = risks[1].Other1Name;
                                txtOther2_2.Text = risks[1].Other2Name;
                                txtOther3_2.Text = risks[1].Other3Name;
                                txtOther4_2.Text = risks[1].Other4Name;
                                txtOther5_2.Text = risks[1].Other5Name;
                                txtDeathAmountRisk2.Text = validations.ConvertDecimalToString(risks[1].DeathAmount);
                                txtWorkImpossibilityAmountRisk2.Text = validations.ConvertDecimalToString(risks[1].WorkImpossibilityAmount);
                                txtIncomeSecureAmountRisk2.Text = validations.ConvertDecimalToString(risks[1].SecureIncomeAmount);
                                txtDesabilityByAccidentAmountRisk2.Text = validations.ConvertDecimalToString(risks[1].DisabilityByAccidentAmount);
                                txtDeathByAccidentAmountRisk2.Text = validations.ConvertDecimalToString(risks[1].DeathByAccidentAmount);
                                txtCriticalIllnesesAmountRisk2.Text = validations.ConvertDecimalToString(risks[1].CriticalIllnessAmount);
                                txtOther1AmountRisk2.Text = validations.ConvertDecimalToString(risks[1].Other1Amount);
                                txtOther2AmountRisk2.Text = validations.ConvertDecimalToString(risks[1].Other2Amount);
                                txtOther3AmountRisk2.Text = validations.ConvertDecimalToString(risks[1].Other3Amount);
                                txtOther4AmountRisk2.Text = validations.ConvertDecimalToString(risks[1].Other4Amount);
                                txtOther5AmountRisk2.Text = validations.ConvertDecimalToString(risks[1].Other5Amount);
                                txtDeathPeriodRisk2.Text = risks[1].DeathPeriod;
                                txtWorkImpossibilityPeriodRisk2.Text = risks[1].WorkImpossibilityPeriod;
                                txtIncomeSecurePeriodRisk2.Text = risks[1].SecureIncomePeriod;
                                txtDesabilityByAccidentPeriodRisk2.Text = risks[1].DisabilityByAccidentPeriod;
                                txtDeathByAccidentPeriodRisk2.Text = risks[1].DeathByAccidentPeriod;
                                txtCriticalIllnesesPeriodRisk2.Text = risks[1].CriticalIllnessPeriod;
                                txtOther1PeriodRisk2.Text = risks[1].Other1Period;
                                txtOther2PeriodRisk2.Text = risks[1].Other2Period;
                                txtOther3PeriodRisk2.Text = risks[1].Other3Period;
                                txtOther4PeriodRisk2.Text = risks[1].Other4Period;
                                txtOther5PeriodRisk2.Text = risks[1].Other5Period;
                                txtDeathCoverageTypeRisk2.Text = risks[1].DeathCoverageType;
                                txtWorkImpossibilityCoverageTypeRisk2.Text = risks[1].WorkImpossibilityCoverageType;
                                txtIncomeSecureCoverageTypeRisk2.Text = risks[1].SecureIncomeCoverageType;
                                txtDesabilityByAccidentCoverageTypeRisk2.Text = risks[1].DisabilityByAccidentCoverageType;
                                txtDeathByAccidentCoverageTypeRisk2.Text = risks[1].DeathByAccidentCoverageType;
                                txtCriticalIllnesesCoverageTypeRisk2.Text = risks[1].CriticalIllnessCoverageType;
                                txtOther1CoverageTypeRisk2.Text = risks[1].Other1CoverageType;
                                txtOther2CoverageTypeRisk2.Text = risks[1].Other2CoverageType;
                                txtOther3CoverageTypeRisk2.Text = risks[1].Other3CoverageType;
                                txtOther4CoverageTypeRisk2.Text = risks[1].Other4CoverageType;
                                txtOther5CoverageTypeRisk2.Text = risks[1].Other5CoverageType;
                                txtDeathPremiumRisk2.Text = validations.ConvertDecimalToString(risks[1].DeathPremium);
                                txtWorkImpossibilityPremiumRisk2.Text = validations.ConvertDecimalToString(risks[1].WorkImpossibilityPremium);
                                txtIncomeSecurePremiumRisk2.Text = validations.ConvertDecimalToString(risks[1].SecureIncomePremium);
                                txtDesabilityByAccidentPremiumRisk2.Text = validations.ConvertDecimalToString(risks[1].DisabilityByAccidentPremium);
                                txtDeathByAccidentPremiumRisk2.Text = validations.ConvertDecimalToString(risks[1].DeathByAccidentPremium);
                                txtCriticalIllnesesPremiumRisk2.Text = validations.ConvertDecimalToString(risks[1].CriticalIllnessPremium);
                                txtOther1PremiumRisk2.Text = validations.ConvertDecimalToString(risks[1].Other1Premium);
                                txtOther2PremiumRisk2.Text = validations.ConvertDecimalToString(risks[1].Other2Premium);
                                txtOther3PremiumRisk2.Text = validations.ConvertDecimalToString(risks[1].Other3Premium);
                                txtOther4PremiumRisk2.Text = validations.ConvertDecimalToString(risks[1].Other4Premium);
                                txtOther5PremiumRisk2.Text = validations.ConvertDecimalToString(risks[1].Other5Premium);
                                txtDeathCommentsRisk2.Text = risks[1].DeathComments;
                                txtWorkImpossibilityCommentsRisk2.Text = risks[1].WorkImpossibilityComments;
                                txtIncomeSecureCommentsRisk2.Text = risks[1].SecureIncomeComments;
                                txtDesabilityByAccidentCommentsRisk2.Text = risks[1].DisabilityByAccidentComments;
                                txtDeathByAccidentCommentsRisk2.Text = risks[1].DeathByAccidentComments;
                                txtCriticalIllnesesCommentsRisk2.Text = risks[1].CriticalIllnessComments;
                                txtOther1CommentsRisk2.Text = risks[1].Other1Comments;
                                txtOther2CommentsRisk2.Text = risks[1].Other2Comments;
                                txtOther3CommentsRisk2.Text = risks[1].Other3Comments;
                                txtOther4CommentsRisk2.Text = risks[1].Other4Comments;
                                txtOther5CommentsRisk2.Text = risks[1].Other5Comments;

                                if (additionalsInsureds.Count > 1)
                                {
                                    tbItemInsurencedRisk3.Visibility = Visibility.Visible;
                                    tbItemInsurencedRisk3.Header = additionalsInsureds[1].FirstName + " " + additionalsInsureds[1].LastName;
                                    if (risks.Count > 2)
                                    {
                                        rbDeath1Risk3.IsChecked = risks[2].IsDeathType1;
                                        rbDeath5Risk3.IsChecked = risks[2].IsDeathType5;
                                        rbDeathFixedRisk3.IsChecked = risks[2].IsDeathTypeFixed;
                                        if (risks[2].IsProfessionalWorkImpossibility == false)
                                        {
                                            rbNormalWorkImposibilitationRisk3.IsChecked = true;
                                        }
                                        txtOther1_3.Text = risks[2].Other1Name;
                                        txtOther2_3.Text = risks[2].Other2Name;
                                        txtOther3_3.Text = risks[2].Other3Name;
                                        txtOther4_3.Text = risks[2].Other4Name;
                                        txtOther5_3.Text = risks[2].Other5Name;
                                        txtDeathAmountRisk3.Text = validations.ConvertDecimalToString(risks[2].DeathAmount);
                                        txtWorkImpossibilityAmountRisk3.Text = validations.ConvertDecimalToString(risks[2].WorkImpossibilityAmount);
                                        txtIncomeSecureAmountRisk3.Text = validations.ConvertDecimalToString(risks[2].SecureIncomeAmount);
                                        txtDesabilityByAccidentAmountRisk3.Text = validations.ConvertDecimalToString(risks[2].DisabilityByAccidentAmount);
                                        txtDeathByAccidentAmountRisk3.Text = validations.ConvertDecimalToString(risks[2].DeathByAccidentAmount);
                                        txtCriticalIllnesesAmountRisk3.Text = validations.ConvertDecimalToString(risks[2].CriticalIllnessAmount);
                                        txtOther1AmountRisk3.Text = validations.ConvertDecimalToString(risks[2].Other1Amount);
                                        txtOther2AmountRisk3.Text = validations.ConvertDecimalToString(risks[2].Other2Amount);
                                        txtOther3AmountRisk3.Text = validations.ConvertDecimalToString(risks[2].Other3Amount);
                                        txtOther4AmountRisk3.Text = validations.ConvertDecimalToString(risks[2].Other4Amount);
                                        txtOther5AmountRisk3.Text = validations.ConvertDecimalToString(risks[2].Other5Amount);
                                        txtDeathPeriodRisk3.Text = risks[2].DeathPeriod;
                                        txtWorkImpossibilityPeriodRisk3.Text = risks[2].WorkImpossibilityPeriod;
                                        txtIncomeSecurePeriodRisk3.Text = risks[2].SecureIncomePeriod;
                                        txtDesabilityByAccidentPeriodRisk3.Text = risks[2].DisabilityByAccidentPeriod;
                                        txtDeathByAccidentPeriodRisk3.Text = risks[2].DeathByAccidentPeriod;
                                        txtCriticalIllnesesPeriodRisk3.Text = risks[2].CriticalIllnessPeriod;
                                        txtOther1PeriodRisk3.Text = risks[2].Other1Period;
                                        txtOther2PeriodRisk3.Text = risks[2].Other2Period;
                                        txtOther3PeriodRisk3.Text = risks[2].Other3Period;
                                        txtOther4PeriodRisk3.Text = risks[2].Other4Period;
                                        txtOther5PeriodRisk3.Text = risks[2].Other5Period;
                                        txtDeathCoverageTypeRisk3.Text = risks[2].DeathCoverageType;
                                        txtWorkImpossibilityCoverageTypeRisk3.Text = risks[2].WorkImpossibilityCoverageType;
                                        txtIncomeSecureCoverageTypeRisk3.Text = risks[2].SecureIncomeCoverageType;
                                        txtDesabilityByAccidentCoverageTypeRisk3.Text = risks[2].DisabilityByAccidentCoverageType;
                                        txtDeathByAccidentCoverageTypeRisk3.Text = risks[2].DeathByAccidentCoverageType;
                                        txtCriticalIllnesesCoverageTypeRisk3.Text = risks[2].CriticalIllnessCoverageType;
                                        txtOther1CoverageTypeRisk3.Text = risks[2].Other1CoverageType;
                                        txtOther2CoverageTypeRisk3.Text = risks[2].Other2CoverageType;
                                        txtOther3CoverageTypeRisk3.Text = risks[2].Other3CoverageType;
                                        txtOther4CoverageTypeRisk3.Text = risks[2].Other4CoverageType;
                                        txtOther5CoverageTypeRisk3.Text = risks[2].Other5CoverageType;
                                        txtDeathPremiumRisk3.Text = validations.ConvertDecimalToString(risks[2].DeathPremium);
                                        txtWorkImpossibilityPremiumRisk3.Text = validations.ConvertDecimalToString(risks[2].WorkImpossibilityPremium);
                                        txtIncomeSecurePremiumRisk3.Text = validations.ConvertDecimalToString(risks[2].SecureIncomePremium);
                                        txtDesabilityByAccidentPremiumRisk3.Text = validations.ConvertDecimalToString(risks[2].DisabilityByAccidentPremium);
                                        txtDeathByAccidentPremiumRisk3.Text = validations.ConvertDecimalToString(risks[2].DeathByAccidentPremium);
                                        txtCriticalIllnesesPremiumRisk3.Text = validations.ConvertDecimalToString(risks[2].CriticalIllnessPremium);
                                        txtOther1PremiumRisk3.Text = validations.ConvertDecimalToString(risks[2].Other1Premium);
                                        txtOther2PremiumRisk3.Text = validations.ConvertDecimalToString(risks[2].Other2Premium);
                                        txtOther3PremiumRisk3.Text = validations.ConvertDecimalToString(risks[2].Other3Premium);
                                        txtOther4PremiumRisk3.Text = validations.ConvertDecimalToString(risks[2].Other4Premium);
                                        txtOther5PremiumRisk3.Text = validations.ConvertDecimalToString(risks[2].Other5Premium);
                                        txtDeathCommentsRisk3.Text = risks[2].DeathComments;
                                        txtWorkImpossibilityCommentsRisk3.Text = risks[2].WorkImpossibilityComments;
                                        txtIncomeSecureCommentsRisk3.Text = risks[2].SecureIncomeComments;
                                        txtDesabilityByAccidentCommentsRisk3.Text = risks[2].DisabilityByAccidentComments;
                                        txtDeathByAccidentCommentsRisk3.Text = risks[2].DeathByAccidentComments;
                                        txtCriticalIllnesesCommentsRisk3.Text = risks[2].CriticalIllnessComments;
                                        txtOther1CommentsRisk3.Text = risks[2].Other1Comments;
                                        txtOther2CommentsRisk3.Text = risks[2].Other2Comments;
                                        txtOther3CommentsRisk3.Text = risks[2].Other3Comments;
                                        txtOther4CommentsRisk3.Text = risks[2].Other4Comments;
                                        txtOther5CommentsRisk3.Text = risks[2].Other5Comments;
                                    }

                                    if (additionalsInsureds.Count > 2)
                                    {
                                        tbItemInsurencedRisk4.Visibility = Visibility.Visible;
                                        tbItemInsurencedRisk4.Header = additionalsInsureds[2].FirstName + " " + additionalsInsureds[2].LastName;
                                        if (risks.Count > 3)
                                        {
                                            rbDeath1Risk4.IsChecked = risks[3].IsDeathType1;
                                            rbDeath5Risk4.IsChecked = risks[3].IsDeathType5;
                                            rbDeathFixedRisk4.IsChecked = risks[3].IsDeathTypeFixed;
                                            if (risks[3].IsProfessionalWorkImpossibility == false)
                                            {
                                                rbNormalWorkImposibilitationRisk4.IsChecked = true;
                                            }
                                            txtOther1_4.Text = risks[3].Other1Name;
                                            txtOther2_4.Text = risks[3].Other2Name;
                                            txtOther3_4.Text = risks[3].Other3Name;
                                            txtOther4_4.Text = risks[3].Other4Name;
                                            txtOther5_4.Text = risks[3].Other5Name;
                                            txtDeathAmountRisk4.Text = validations.ConvertDecimalToString(risks[3].DeathAmount);
                                            txtWorkImpossibilityAmountRisk4.Text = validations.ConvertDecimalToString(risks[3].WorkImpossibilityAmount);
                                            txtIncomeSecureAmountRisk4.Text = validations.ConvertDecimalToString(risks[3].SecureIncomeAmount);
                                            txtDesabilityByAccidentAmountRisk4.Text = validations.ConvertDecimalToString(risks[3].DisabilityByAccidentAmount);
                                            txtDeathByAccidentAmountRisk4.Text = validations.ConvertDecimalToString(risks[3].DeathByAccidentAmount);
                                            txtCriticalIllnesesAmountRisk4.Text = validations.ConvertDecimalToString(risks[3].CriticalIllnessAmount);
                                            txtOther1AmountRisk4.Text = validations.ConvertDecimalToString(risks[3].Other1Amount);
                                            txtOther2AmountRisk4.Text = validations.ConvertDecimalToString(risks[3].Other2Amount);
                                            txtOther3AmountRisk4.Text = validations.ConvertDecimalToString(risks[3].Other3Amount);
                                            txtOther4AmountRisk4.Text = validations.ConvertDecimalToString(risks[3].Other4Amount);
                                            txtOther5AmountRisk4.Text = validations.ConvertDecimalToString(risks[3].Other5Amount);
                                            txtDeathPeriodRisk4.Text = risks[3].DeathPeriod;
                                            txtWorkImpossibilityPeriodRisk4.Text = risks[3].WorkImpossibilityPeriod;
                                            txtIncomeSecurePeriodRisk4.Text = risks[3].SecureIncomePeriod;
                                            txtDesabilityByAccidentPeriodRisk4.Text = risks[3].DisabilityByAccidentPeriod;
                                            txtDeathByAccidentPeriodRisk4.Text = risks[3].DeathByAccidentPeriod;
                                            txtCriticalIllnesesPeriodRisk4.Text = risks[3].CriticalIllnessPeriod;
                                            txtOther1PeriodRisk4.Text = risks[3].Other1Period;
                                            txtOther2PeriodRisk4.Text = risks[3].Other2Period;
                                            txtOther3PeriodRisk4.Text = risks[3].Other3Period;
                                            txtOther4PeriodRisk4.Text = risks[3].Other4Period;
                                            txtOther5PeriodRisk4.Text = risks[3].Other5Period;
                                            txtDeathCoverageTypeRisk4.Text = risks[3].DeathCoverageType;
                                            txtWorkImpossibilityCoverageTypeRisk4.Text = risks[3].WorkImpossibilityCoverageType;
                                            txtIncomeSecureCoverageTypeRisk4.Text = risks[3].SecureIncomeCoverageType;
                                            txtDesabilityByAccidentCoverageTypeRisk4.Text = risks[3].DisabilityByAccidentCoverageType;
                                            txtDeathByAccidentCoverageTypeRisk4.Text = risks[3].DeathByAccidentCoverageType;
                                            txtCriticalIllnesesCoverageTypeRisk4.Text = risks[3].CriticalIllnessCoverageType;
                                            txtOther1CoverageTypeRisk4.Text = risks[3].Other1CoverageType;
                                            txtOther2CoverageTypeRisk4.Text = risks[3].Other2CoverageType;
                                            txtOther3CoverageTypeRisk4.Text = risks[3].Other3CoverageType;
                                            txtOther4CoverageTypeRisk4.Text = risks[3].Other4CoverageType;
                                            txtOther5CoverageTypeRisk4.Text = risks[3].Other5CoverageType;
                                            txtDeathPremiumRisk4.Text = validations.ConvertDecimalToString(risks[3].DeathPremium);
                                            txtWorkImpossibilityPremiumRisk4.Text = validations.ConvertDecimalToString(risks[3].WorkImpossibilityPremium);
                                            txtIncomeSecurePremiumRisk4.Text = validations.ConvertDecimalToString(risks[3].SecureIncomePremium);
                                            txtDesabilityByAccidentPremiumRisk4.Text = validations.ConvertDecimalToString(risks[3].DisabilityByAccidentPremium);
                                            txtDeathByAccidentPremiumRisk4.Text = validations.ConvertDecimalToString(risks[3].DeathByAccidentPremium);
                                            txtCriticalIllnesesPremiumRisk4.Text = validations.ConvertDecimalToString(risks[3].CriticalIllnessPremium);
                                            txtOther1PremiumRisk4.Text = validations.ConvertDecimalToString(risks[3].Other1Premium);
                                            txtOther2PremiumRisk4.Text = validations.ConvertDecimalToString(risks[3].Other2Premium);
                                            txtOther3PremiumRisk4.Text = validations.ConvertDecimalToString(risks[3].Other3Premium);
                                            txtOther4PremiumRisk4.Text = validations.ConvertDecimalToString(risks[3].Other4Premium);
                                            txtOther5PremiumRisk4.Text = validations.ConvertDecimalToString(risks[3].Other5Premium);
                                            txtDeathCommentsRisk4.Text = risks[3].DeathComments;
                                            txtWorkImpossibilityCommentsRisk4.Text = risks[3].WorkImpossibilityComments;
                                            txtIncomeSecureCommentsRisk4.Text = risks[3].SecureIncomeComments;
                                            txtDesabilityByAccidentCommentsRisk4.Text = risks[3].DisabilityByAccidentComments;
                                            txtDeathByAccidentCommentsRisk4.Text = risks[3].DeathByAccidentComments;
                                            txtCriticalIllnesesCommentsRisk4.Text = risks[3].CriticalIllnessComments;
                                            txtOther1CommentsRisk4.Text = risks[3].Other1Comments;
                                            txtOther2CommentsRisk4.Text = risks[3].Other2Comments;
                                            txtOther3CommentsRisk4.Text = risks[3].Other3Comments;
                                            txtOther4CommentsRisk4.Text = risks[3].Other4Comments;
                                            txtOther5CommentsRisk4.Text = risks[3].Other5Comments;
                                        }
                                        if (additionalsInsureds.Count > 3)
                                        {
                                            tbItemInsurencedRisk5.Visibility = Visibility.Visible;
                                            tbItemInsurencedRisk5.Header = additionalsInsureds[3].FirstName + " " + additionalsInsureds[3].LastName;
                                            if (risks.Count > 4)
                                            {
                                                rbDeath1Risk5.IsChecked = risks[4].IsDeathType1;
                                                rbDeath5Risk5.IsChecked = risks[4].IsDeathType5;
                                                rbDeathFixedRisk5.IsChecked = risks[4].IsDeathTypeFixed;
                                                if (risks[4].IsProfessionalWorkImpossibility == false)
                                                {
                                                    rbNormalWorkImposibilitationRisk5.IsChecked = true;
                                                }
                                                txtOther1_5.Text = risks[4].Other1Name;
                                                txtOther2_5.Text = risks[4].Other2Name;
                                                txtOther3_5.Text = risks[4].Other3Name;
                                                txtOther4_5.Text = risks[4].Other4Name;
                                                txtOther5_5.Text = risks[4].Other5Name;
                                                txtDeathAmountRisk5.Text = validations.ConvertDecimalToString(risks[4].DeathAmount);
                                                txtWorkImpossibilityAmountRisk5.Text = validations.ConvertDecimalToString(risks[4].WorkImpossibilityAmount);
                                                txtIncomeSecureAmountRisk5.Text = validations.ConvertDecimalToString(risks[4].SecureIncomeAmount);
                                                txtDesabilityByAccidentAmountRisk5.Text = validations.ConvertDecimalToString(risks[4].DisabilityByAccidentAmount);
                                                txtDeathByAccidentAmountRisk5.Text = validations.ConvertDecimalToString(risks[4].DeathByAccidentAmount);
                                                txtCriticalIllnesesAmountRisk5.Text = validations.ConvertDecimalToString(risks[4].CriticalIllnessAmount);
                                                txtOther1AmountRisk5.Text = validations.ConvertDecimalToString(risks[4].Other1Amount);
                                                txtOther2AmountRisk5.Text = validations.ConvertDecimalToString(risks[4].Other2Amount);
                                                txtOther3AmountRisk5.Text = validations.ConvertDecimalToString(risks[4].Other3Amount);
                                                txtOther4AmountRisk5.Text = validations.ConvertDecimalToString(risks[4].Other4Amount);
                                                txtOther5AmountRisk5.Text = validations.ConvertDecimalToString(risks[4].Other5Amount);
                                                txtDeathPeriodRisk5.Text = risks[4].DeathPeriod;
                                                txtWorkImpossibilityPeriodRisk5.Text = risks[4].WorkImpossibilityPeriod;
                                                txtIncomeSecurePeriodRisk5.Text = risks[4].SecureIncomePeriod;
                                                txtDesabilityByAccidentPeriodRisk5.Text = risks[4].DisabilityByAccidentPeriod;
                                                txtDeathByAccidentPeriodRisk5.Text = risks[4].DeathByAccidentPeriod;
                                                txtCriticalIllnesesPeriodRisk5.Text = risks[4].CriticalIllnessPeriod;
                                                txtOther1PeriodRisk5.Text = risks[4].Other1Period;
                                                txtOther2PeriodRisk5.Text = risks[4].Other2Period;
                                                txtOther3PeriodRisk5.Text = risks[4].Other3Period;
                                                txtOther4PeriodRisk5.Text = risks[4].Other4Period;
                                                txtOther5PeriodRisk5.Text = risks[4].Other5Period;
                                                txtDeathCoverageTypeRisk5.Text = risks[4].DeathCoverageType;
                                                txtWorkImpossibilityCoverageTypeRisk5.Text = risks[4].WorkImpossibilityCoverageType;
                                                txtIncomeSecureCoverageTypeRisk5.Text = risks[4].SecureIncomeCoverageType;
                                                txtDesabilityByAccidentCoverageTypeRisk5.Text = risks[4].DisabilityByAccidentCoverageType;
                                                txtDeathByAccidentCoverageTypeRisk5.Text = risks[4].DeathByAccidentCoverageType;
                                                txtCriticalIllnesesCoverageTypeRisk5.Text = risks[4].CriticalIllnessCoverageType;
                                                txtOther1CoverageTypeRisk5.Text = risks[4].Other1CoverageType;
                                                txtOther2CoverageTypeRisk5.Text = risks[4].Other2CoverageType;
                                                txtOther3CoverageTypeRisk5.Text = risks[4].Other3CoverageType;
                                                txtOther4CoverageTypeRisk5.Text = risks[4].Other4CoverageType;
                                                txtOther5CoverageTypeRisk5.Text = risks[4].Other5CoverageType;
                                                txtDeathPremiumRisk5.Text = validations.ConvertDecimalToString(risks[4].DeathPremium);
                                                txtWorkImpossibilityPremiumRisk5.Text = validations.ConvertDecimalToString(risks[4].WorkImpossibilityPremium);
                                                txtIncomeSecurePremiumRisk5.Text = validations.ConvertDecimalToString(risks[4].SecureIncomePremium);
                                                txtDesabilityByAccidentPremiumRisk5.Text = validations.ConvertDecimalToString(risks[4].DisabilityByAccidentPremium);
                                                txtDeathByAccidentPremiumRisk5.Text = validations.ConvertDecimalToString(risks[4].DeathByAccidentPremium);
                                                txtCriticalIllnesesPremiumRisk5.Text = validations.ConvertDecimalToString(risks[4].CriticalIllnessPremium);
                                                txtOther1PremiumRisk5.Text = validations.ConvertDecimalToString(risks[4].Other1Premium);
                                                txtOther2PremiumRisk5.Text = validations.ConvertDecimalToString(risks[4].Other2Premium);
                                                txtOther3PremiumRisk5.Text = validations.ConvertDecimalToString(risks[4].Other3Premium);
                                                txtOther4PremiumRisk5.Text = validations.ConvertDecimalToString(risks[4].Other4Premium);
                                                txtOther5PremiumRisk5.Text = validations.ConvertDecimalToString(risks[4].Other5Premium);
                                                txtDeathCommentsRisk5.Text = risks[4].DeathComments;
                                                txtWorkImpossibilityCommentsRisk5.Text = risks[4].WorkImpossibilityComments;
                                                txtIncomeSecureCommentsRisk5.Text = risks[4].SecureIncomeComments;
                                                txtDesabilityByAccidentCommentsRisk5.Text = risks[4].DisabilityByAccidentComments;
                                                txtDeathByAccidentCommentsRisk5.Text = risks[4].DeathByAccidentComments;
                                                txtCriticalIllnesesCommentsRisk5.Text = risks[4].CriticalIllnessComments;
                                                txtOther1CommentsRisk5.Text = risks[4].Other1Comments;
                                                txtOther2CommentsRisk5.Text = risks[4].Other2Comments;
                                                txtOther3CommentsRisk5.Text = risks[4].Other3Comments;
                                                txtOther4CommentsRisk5.Text = risks[4].Other4Comments;
                                                txtOther5CommentsRisk5.Text = risks[4].Other5Comments;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            else if (industrySelected.LifeIndustryName == "משכנתא")
            {
                txtPolicyNumber.IsEnabled = true;
                tbItemAdm.Visibility = Visibility.Collapsed;
                tbItemRisk.Visibility = Visibility.Collapsed;
                tbItemMortgage.Visibility = Visibility.Visible;
                tbItemPrivate.Visibility = Visibility.Collapsed;
                tbItemPension.Visibility = Visibility.Collapsed;
                if (lifePolicy != null && lifePolicy.MortgageLifeIndustry != null && lifePolicy.LifeIndustry.LifeIndustryName != "שגויים")
                {
                    txtBeneficiaryNameMortgage.Text = lifePolicy.MortgageLifeIndustry.IrrevocableBeneficiary;
                    txtBeneficiaryContactName.Text = lifePolicy.MortgageLifeIndustry.BankContactName;
                    txtBankNameMortgage.Text = lifePolicy.MortgageLifeIndustry.BankNumber;
                    txtBeneficiaryPhone.Text = lifePolicy.MortgageLifeIndustry.BankPhone;
                    txtBranchMortgage.Text = lifePolicy.MortgageLifeIndustry.BankBranch;
                    txtBeneficiaryFax.Text = lifePolicy.MortgageLifeIndustry.BankFax;
                    txtBankAddressMortgage.Text = lifePolicy.MortgageLifeIndustry.BankAddress;
                    txtBeneficiaryEmail.Text = lifePolicy.MortgageLifeIndustry.BankEmail;
                    chbIsReleaseFromPayingPremiums.IsChecked = lifePolicy.MortgageLifeIndustry.IsReleaseFromPayingPremiums;
                    txtPremiumReleaseAmount.Text = validations.ConvertDecimalToString(lifePolicy.MortgageLifeIndustry.Premium);
                    txtCommentsMortgage.Text = lifePolicy.MortgageLifeIndustry.Comments;
                    additionalsInsureds = lifePolicyLogic.GetAdditionalInsuredsByPolicy(lifePolicy.LifePolicyID);
                    if (additionalsInsureds.Count > 0)
                    {
                        chbIsAdditionalInsuredMortgage.IsChecked = true;
                        txtAdditionalInsuredFirstNameMortgage.Text = additionalsInsureds[0].FirstName;
                        txtAdditionalInsuredLastNameMortgage.Text = additionalsInsureds[0].LastName;
                        txtAdditionalInsuredIdMortgage.Text = additionalsInsureds[0].IdNumber;
                        dpAdditionalInsuredBirthDateMortgage.SelectedDate = additionalsInsureds[0].BirthDate;
                        var relationships = cbRelationshipMortgage.Items;
                        foreach (var item in relationships)
                        {
                            if (additionalsInsureds[0].RelationshipWithPrincipalInsured == ((ComboBoxItem)item).Content.ToString())
                            {
                                cbRelationshipMortgage.SelectedItem = item;
                                break;
                            }
                        }
                    }
                    mortgages = lifePolicyLogic.GetMortgagesByPolicy(lifePolicy.LifePolicyID);
                    dgMortgagesBinding();

                }
            }
            else if (industrySelected.LifeIndustryName == "פרט")
            {
                txtPolicyNumber.IsEnabled = true;
                tbItemAdm.Visibility = Visibility.Collapsed;
                tbItemRisk.Visibility = Visibility.Collapsed;
                tbItemMortgage.Visibility = Visibility.Collapsed;
                tbItemPrivate.Visibility = Visibility.Visible;
                tbItemPension.Visibility = Visibility.Collapsed;
                cbInvestmentPrivateBinding();
                FundsPrivateBinding();
                if (lifePolicy != null && lifePolicy.PrivateLifeIndustry != null && lifePolicy.LifeIndustry.LifeIndustryName != "שגויים")
                {
                    if (lifePolicy.PrivateLifeIndustry.IsRewards == true)
                    {
                        rbRewards.IsChecked = true;

                    }
                    txtAgePrivate.Text = lifePolicy.PrivateLifeIndustry.Age.ToString();
                    txtBudgetPrivate.Text = validations.ConvertDecimalToString(lifePolicy.PrivateLifeIndustry.Budget);
                    chbIsFundNotPaysCompensationPrivate.IsChecked = lifePolicy.PrivateLifeIndustry.IsFundNotPaysForCompensation;
                    txtStartBudgetPrivate.Text = validations.ConvertDecimalToString(lifePolicy.PrivateLifeIndustry.StartBudget);
                    txtOneTimeDepositPrivate.Text = validations.ConvertDecimalToString(lifePolicy.PrivateLifeIndustry.OneTimeDeposit);
                    txtPremiumFeesPrivate.Text = validations.ConvertDecimalToString(lifePolicy.PrivateLifeIndustry.PremiumFees);
                    txtAggregateFeesPrivate.Text = validations.ConvertDecimalToString(lifePolicy.PrivateLifeIndustry.AggregateFees);
                    txtCommentsPrivate.Text = lifePolicy.PrivateLifeIndustry.Comments;
                    //var investmentPlans = cbInvestmentPrivate.Items;
                    //foreach (var item in investmentPlans)
                    //{
                    //    InvesmentPlan plan = (InvesmentPlan)item;
                    //    if (plan.InvesmentPlanID == lifePolicy.PrivateLifeIndustry.InvestmentPlanID)
                    //    {
                    //        cbInvestmentPrivate.SelectedItem = item;
                    //        break;
                    //    }
                    //}
                    if (lifePolicy.PrivateLifeIndustry.InvestmentPlanID != null)
                    {
                        SelectInvesmentPlanItemInCb((int)lifePolicy.PrivateLifeIndustry.InvestmentPlanID, cbInvestmentPrivate);
                    }
                    transfers = lifePolicyLogic.GetTransfersByPolicy(lifePolicy.LifePolicyID);
                    dgTransferencesPrivateBinding();
                    additionalsInsureds = lifePolicyLogic.GetAdditionalInsuredsByPolicy(lifePolicy.LifePolicyID);
                    if (additionalsInsureds.Count > 0)
                    {
                        chbIsAdditionalInsuredPrivate.IsChecked = true;
                        txtAdditionalInsuredFirstNamePrivate.Text = additionalsInsureds[0].FirstName;
                        txtAdditionalInsuredLastNamePrivate.Text = additionalsInsureds[0].LastName;
                        txtAdditionalInsuredIdPrivate.Text = additionalsInsureds[0].IdNumber;
                        dpAdditionalInsuredBirthDatePrivate.SelectedDate = additionalsInsureds[0].BirthDate;
                        var relationships = txtRelationshipPrivate.Items;
                        foreach (var item in relationships)
                        {
                            if (additionalsInsureds[0].RelationshipWithPrincipalInsured == ((ComboBoxItem)item).Content.ToString())
                            {
                                txtRelationshipPrivate.SelectedItem = item;
                                break;
                            }
                        }
                    }
                    risks = lifePolicyLogic.GetRisksByPolicy(lifePolicy.LifePolicyID);
                    if (risks.Count > 0)
                    {
                        chbIsRisksPrivate.IsChecked = true;
                        rbDeath1Private.IsChecked = risks[0].IsDeathType1;
                        rbDeath5Private.IsChecked = risks[0].IsDeathType5;
                        rbDeathFixedPrivate.IsChecked = risks[0].IsDeathTypeFixed;
                        if (risks[0].IsProfessionalWorkImpossibility == false)
                        {
                            rbNormalWorkImposibilitationPrivate.IsChecked = true;
                        }
                        txtOther1Private.Text = risks[0].Other1Name;
                        txtOther2Private.Text = risks[0].Other2Name;
                        txtOther3Private.Text = risks[0].Other3Name;
                        txtOther4Private.Text = risks[0].Other4Name;
                        txtOther5Private.Text = risks[0].Other5Name;
                        txtDeathAmountPrivate.Text = validations.ConvertDecimalToString(risks[0].DeathAmount);
                        txtWorkImpossibilityAmountPrivate.Text = validations.ConvertDecimalToString(risks[0].WorkImpossibilityAmount);
                        txtIncomeSecureAmountPrivate.Text = validations.ConvertDecimalToString(risks[0].SecureIncomeAmount);
                        txtDesabilityByAccidentAmountPrivate.Text = validations.ConvertDecimalToString(risks[0].DisabilityByAccidentAmount);
                        txtDeathByAccidentAmountPrivate.Text = validations.ConvertDecimalToString(risks[0].DeathByAccidentAmount);
                        txtCriticalIllnesesAmountPrivate.Text = validations.ConvertDecimalToString(risks[0].CriticalIllnessAmount);
                        txtOther1AmountPrivate.Text = validations.ConvertDecimalToString(risks[0].Other1Amount);
                        txtOther2AmountPrivate.Text = validations.ConvertDecimalToString(risks[0].Other2Amount);
                        txtOther3AmountPrivate.Text = validations.ConvertDecimalToString(risks[0].Other3Amount);
                        txtOther4AmountPrivate.Text = validations.ConvertDecimalToString(risks[0].Other4Amount);
                        txtOther5AmountPrivate.Text = validations.ConvertDecimalToString(risks[0].Other5Amount);
                        txtDeathPeriodPrivate.Text = risks[0].DeathPeriod;
                        txtWorkImpossibilityPeriodPrivate.Text = risks[0].WorkImpossibilityPeriod;
                        txtIncomeSecurePeriodPrivate.Text = risks[0].SecureIncomePeriod;
                        txtDesabilityByAccidentPeriodPrivate.Text = risks[0].DisabilityByAccidentPeriod;
                        txtDeathByAccidentPeriodPrivate.Text = risks[0].DeathByAccidentPeriod;
                        txtCriticalIllnesesPeriodPrivate.Text = risks[0].CriticalIllnessPeriod;
                        txtOther1PeriodPrivate.Text = risks[0].Other1Period;
                        txtOther2PeriodPrivate.Text = risks[0].Other2Period;
                        txtOther3PeriodPrivate.Text = risks[0].Other3Period;
                        txtOther4PeriodPrivate.Text = risks[0].Other4Period;
                        txtOther5PeriodPrivate.Text = risks[0].Other5Period;
                        txtDeathCoverageTypePrivate.Text = risks[0].DeathCoverageType;
                        txtWorkImpossibilityCoverageTypePrivate.Text = risks[0].WorkImpossibilityCoverageType;
                        txtIncomeSecureCoverageTypePrivate.Text = risks[0].SecureIncomeCoverageType;
                        txtDesabilityByAccidentCoverageTypePrivate.Text = risks[0].DisabilityByAccidentCoverageType;
                        txtDeathByAccidentCoverageTypePrivate.Text = risks[0].DeathByAccidentCoverageType;
                        txtCriticalIllnesesCoverageTypePrivate.Text = risks[0].CriticalIllnessCoverageType;
                        txtOther1CoverageTypePrivate.Text = risks[0].Other1CoverageType;
                        txtOther2CoverageTypePrivate.Text = risks[0].Other2CoverageType;
                        txtOther3CoverageTypePrivate.Text = risks[0].Other3CoverageType;
                        txtOther4CoverageTypePrivate.Text = risks[0].Other4CoverageType;
                        txtOther5CoverageTypePrivate.Text = risks[0].Other5CoverageType;
                        txtDeathPremiumPrivate.Text = validations.ConvertDecimalToString(risks[0].DeathPremium);
                        txtWorkImpossibilityPremiumPrivate.Text = validations.ConvertDecimalToString(risks[0].WorkImpossibilityPremium);
                        txtIncomeSecurePremiumPrivate.Text = validations.ConvertDecimalToString(risks[0].SecureIncomePremium);
                        txtDesabilityByAccidentPremiumPrivate.Text = validations.ConvertDecimalToString(risks[0].DisabilityByAccidentPremium);
                        txtDeathByAccidentPremiumPrivate.Text = validations.ConvertDecimalToString(risks[0].DeathByAccidentPremium);
                        txtCriticalIllnesesPremiumPrivate.Text = validations.ConvertDecimalToString(risks[0].CriticalIllnessPremium);
                        txtOther1PremiumPrivate.Text = validations.ConvertDecimalToString(risks[0].Other1Premium);
                        txtOther2PremiumPrivate.Text = validations.ConvertDecimalToString(risks[0].Other2Premium);
                        txtOther3PremiumPrivate.Text = validations.ConvertDecimalToString(risks[0].Other3Premium);
                        txtOther4PremiumPrivate.Text = validations.ConvertDecimalToString(risks[0].Other4Premium);
                        txtOther5PremiumPrivate.Text = validations.ConvertDecimalToString(risks[0].Other5Premium);
                        txtDeathCommentsPrivate.Text = risks[0].DeathComments;
                        txtWorkImpossibilityCommentsPrivate.Text = risks[0].WorkImpossibilityComments;
                        txtIncomeSecureCommentsPrivate.Text = risks[0].SecureIncomeComments;
                        txtDesabilityByAccidentCommentsPrivate.Text = risks[0].DisabilityByAccidentComments;
                        txtDeathByAccidentCommentsPrivate.Text = risks[0].DeathByAccidentComments;
                        txtCriticalIllnesesCommentsPrivate.Text = risks[0].CriticalIllnessComments;
                        txtOther1CommentsPrivate.Text = risks[0].Other1Comments;
                        txtOther2CommentsPrivate.Text = risks[0].Other2Comments;
                        txtOther3CommentsPrivate.Text = risks[0].Other3Comments;
                        txtOther4CommentsPrivate.Text = risks[0].Other4Comments;
                        txtOther5CommentsPrivate.Text = risks[0].Other5Comments;
                        additionalsInsureds = lifePolicyLogic.GetAdditionalInsuredsByPolicy(lifePolicy.LifePolicyID);
                        if (additionalsInsureds.Count > 0)
                        {
                            tbItemInsurenced2Private.Visibility = Visibility.Visible;
                            tbItemInsurenced2Private.Header = additionalsInsureds[0].FirstName + " " + additionalsInsureds[0].LastName;
                            rbDeath1Private2.IsChecked = risks[1].IsDeathType1;
                            rbDeath5Private2.IsChecked = risks[1].IsDeathType5;
                            rbDeathFixedPrivate2.IsChecked = risks[1].IsDeathTypeFixed;
                            if (risks[1].IsProfessionalWorkImpossibility == false)
                            {
                                rbNormalWorkImposibilitationPrivate2.IsChecked = true;
                            }
                            txtOther1Private2.Text = risks[1].Other1Name;
                            txtOther2Private2.Text = risks[1].Other2Name;
                            txtOther3Private2.Text = risks[1].Other3Name;
                            txtOther4Private2.Text = risks[1].Other4Name;
                            txtOther5Private2.Text = risks[1].Other5Name;
                            txtDeathAmountPrivate2.Text = validations.ConvertDecimalToString(risks[1].DeathAmount);
                            txtWorkImpossibilityAmountPrivate2.Text = validations.ConvertDecimalToString(risks[1].WorkImpossibilityAmount);
                            txtIncomeSecureAmountPrivate2.Text = validations.ConvertDecimalToString(risks[1].SecureIncomeAmount);
                            txtDesabilityByAccidentAmountPrivate2.Text = validations.ConvertDecimalToString(risks[1].DisabilityByAccidentAmount);
                            txtDeathByAccidentAmountPrivate2.Text = validations.ConvertDecimalToString(risks[1].DeathByAccidentAmount);
                            txtCriticalIllnesesAmountPrivate2.Text = validations.ConvertDecimalToString(risks[1].CriticalIllnessAmount);
                            txtOther1AmountPrivate2.Text = validations.ConvertDecimalToString(risks[1].Other1Amount);
                            txtOther2AmountPrivate2.Text = validations.ConvertDecimalToString(risks[1].Other2Amount);
                            txtOther3AmountPrivate2.Text = validations.ConvertDecimalToString(risks[1].Other3Amount);
                            txtOther4AmountPrivate2.Text = validations.ConvertDecimalToString(risks[1].Other4Amount);
                            txtOther5AmountPrivate2.Text = validations.ConvertDecimalToString(risks[1].Other5Amount);
                            txtDeathPeriodPrivate2.Text = risks[1].DeathPeriod;
                            txtWorkImpossibilityPeriodPrivate2.Text = risks[1].WorkImpossibilityPeriod;
                            txtIncomeSecurePeriodPrivate2.Text = risks[1].SecureIncomePeriod;
                            txtDesabilityByAccidentPeriodPrivate2.Text = risks[1].DisabilityByAccidentPeriod;
                            txtDeathByAccidentPeriodPrivate2.Text = risks[1].DeathByAccidentPeriod;
                            txtCriticalIllnesesPeriodPrivate2.Text = risks[1].CriticalIllnessPeriod;
                            txtOther1PeriodPrivate2.Text = risks[1].Other1Period;
                            txtOther2PeriodPrivate2.Text = risks[1].Other2Period;
                            txtOther3PeriodPrivate2.Text = risks[1].Other3Period;
                            txtOther4PeriodPrivate2.Text = risks[1].Other4Period;
                            txtOther5PeriodPrivate2.Text = risks[1].Other5Period;
                            txtDeathCoverageTypePrivate2.Text = risks[1].DeathCoverageType;
                            txtWorkImpossibilityCoverageTypePrivate2.Text = risks[1].WorkImpossibilityCoverageType;
                            txtIncomeSecureCoverageTypePrivate2.Text = risks[1].SecureIncomeCoverageType;
                            txtDesabilityByAccidentCoverageTypePrivate2.Text = risks[1].DisabilityByAccidentCoverageType;
                            txtDeathByAccidentCoverageTypePrivate2.Text = risks[1].DeathByAccidentCoverageType;
                            txtCriticalIllnesesCoverageTypePrivate2.Text = risks[1].CriticalIllnessCoverageType;
                            txtOther1CoverageTypePrivate2.Text = risks[1].Other1CoverageType;
                            txtOther2CoverageTypePrivate2.Text = risks[1].Other2CoverageType;
                            txtOther3CoverageTypePrivate2.Text = risks[1].Other3CoverageType;
                            txtOther4CoverageTypePrivate2.Text = risks[1].Other4CoverageType;
                            txtOther5CoverageTypePrivate2.Text = risks[1].Other5CoverageType;
                            txtDeathPremiumPrivate2.Text = validations.ConvertDecimalToString(risks[1].DeathPremium);
                            txtWorkImpossibilityPremiumPrivate2.Text = validations.ConvertDecimalToString(risks[1].WorkImpossibilityPremium);
                            txtIncomeSecurePremiumPrivate2.Text = validations.ConvertDecimalToString(risks[1].SecureIncomePremium);
                            txtDesabilityByAccidentPremiumPrivate2.Text = validations.ConvertDecimalToString(risks[1].DisabilityByAccidentPremium);
                            txtDeathByAccidentPremiumPrivate2.Text = validations.ConvertDecimalToString(risks[1].DeathByAccidentPremium);
                            txtCriticalIllnesesPremiumPrivate2.Text = validations.ConvertDecimalToString(risks[1].CriticalIllnessPremium);
                            txtOther1PremiumPrivate2.Text = validations.ConvertDecimalToString(risks[1].Other1Premium);
                            txtOther2PremiumPrivate2.Text = validations.ConvertDecimalToString(risks[1].Other2Premium);
                            txtOther3PremiumPrivate2.Text = validations.ConvertDecimalToString(risks[1].Other3Premium);
                            txtOther4PremiumPrivate2.Text = validations.ConvertDecimalToString(risks[1].Other4Premium);
                            txtOther5PremiumPrivate2.Text = validations.ConvertDecimalToString(risks[1].Other5Premium);
                            txtDeathCommentsPrivate2.Text = risks[1].DeathComments;
                            txtWorkImpossibilityCommentsPrivate2.Text = risks[1].WorkImpossibilityComments;
                            txtIncomeSecureCommentsPrivate2.Text = risks[1].SecureIncomeComments;
                            txtDesabilityByAccidentCommentsPrivate2.Text = risks[1].DisabilityByAccidentComments;
                            txtDeathByAccidentCommentsPrivate2.Text = risks[1].DeathByAccidentComments;
                            txtCriticalIllnesesCommentsPrivate2.Text = risks[1].CriticalIllnessComments;
                            txtOther1CommentsPrivate2.Text = risks[1].Other1Comments;
                            txtOther2CommentsPrivate2.Text = risks[1].Other2Comments;
                            txtOther3CommentsPrivate2.Text = risks[1].Other3Comments;
                            txtOther4CommentsPrivate2.Text = risks[1].Other4Comments;
                            txtOther5CommentsPrivate2.Text = risks[1].Other5Comments;
                        }
                    }

                }
            }
            else if (industrySelected.LifeIndustryName == "פנסיה")
            {
                txtPolicyNumber.IsEnabled = true;
                tbItemAdm.Visibility = Visibility.Collapsed;
                tbItemRisk.Visibility = Visibility.Collapsed;
                tbItemMortgage.Visibility = Visibility.Collapsed;
                tbItemPrivate.Visibility = Visibility.Collapsed;
                tbItemPension.Visibility = Visibility.Visible;
                cbInvestmentPlanPensionBinding();
                cbFundJoiningPlanBinding();
                cbInsurencesWaiversBinding();
                cbRetirementPlanBinding();
                cbPensionPlanType.SelectedIndex = 0;
                FundsPensionBinding();
                if (lifePolicy != null && lifePolicy.PensionLifeIndustry != null && lifePolicy.LifeIndustry.LifeIndustryName != "שגויים")
                {
                    if (lifePolicy.PensionLifeIndustry.IsEmployee == false)
                    {
                        rbIndependentPension.IsChecked = true;
                    }
                    //chbIsFundNotPaysCompensationPension.IsChecked = lifePolicy.PensionLifeIndustry.IsFundNotPaysForCompensation;
                    chbIsCompulsoryPensionPension.IsChecked = lifePolicy.PensionLifeIndustry.IsCompulsoryPension;
                    chbIsShareholderPension.IsChecked = lifePolicy.PensionLifeIndustry.IsShareholder;
                    txtAgePension.Text = lifePolicy.PensionLifeIndustry.Age.ToString();
                    txtBudgetPension.Text = validations.ConvertDecimalToString(lifePolicy.PensionLifeIndustry.Budget);
                    txtEmployeeSalaryPension.Text = validations.ConvertDecimalToString(lifePolicy.PensionLifeIndustry.EmployeeSalary);

                    if (lifePolicy.PensionLifeIndustry.IsPersonalPlanType == false)
                    {
                        cbPensionPlanType.SelectedIndex = 1;
                    }

                    txtStartSalaryPension.Text = validations.ConvertDecimalToString(lifePolicy.PensionLifeIndustry.StartSalary);
                    txtParogramCompensationsPension.Text = validations.ConvertDecimalToString(lifePolicy.PensionLifeIndustry.CompensationToProgram);
                    txtEmployerRewardPension.Text = validations.ConvertDecimalToString(lifePolicy.PensionLifeIndustry.EmployerRewards);
                    txtEmployeeRewardPension.Text = validations.ConvertDecimalToString(lifePolicy.PensionLifeIndustry.EmployeeRewards);
                    txtOtherRewardsPension.Text = validations.ConvertDecimalToString(lifePolicy.PensionLifeIndustry.OtherRewards);
                    txtPremiumFeesPension.Text = validations.ConvertDecimalToString(lifePolicy.PensionLifeIndustry.PremiumFees);
                    txtAggregateFeesPension.Text = validations.ConvertDecimalToString(lifePolicy.PensionLifeIndustry.AggregateFees);
                    //var investmentPlans = cbInvestmentPlanPension.Items;
                    //foreach (var item in investmentPlans)
                    //{
                    //    InvesmentPlan plan = (InvesmentPlan)item;
                    //    if (plan.InvesmentPlanID == lifePolicy.PensionLifeIndustry.InvestmentPlanID)
                    //    {
                    //        cbInvestmentPlanPension.SelectedItem = item;
                    //        break;
                    //    }
                    //}
                    if (lifePolicy.PensionLifeIndustry.InvestmentPlanID != null)
                    {
                        SelectInvesmentPlanItemInCb((int)lifePolicy.PensionLifeIndustry.InvestmentPlanID, cbInvestmentPlanPension);
                    }
                    //var fundJoiningPlans = cbFundJoiningPlan.Items;
                    //foreach (var item in fundJoiningPlans)
                    //{
                    //    FundJoiningPlan plan = (FundJoiningPlan)item;
                    //    if (plan.FundJoiningPlanID == lifePolicy.PensionLifeIndustry.FundJoiningPlanID)
                    //    {
                    //        cbFundJoiningPlan.SelectedItem = item;
                    //        break;
                    //    }
                    //}
                    if (lifePolicy.PensionLifeIndustry.FundJoiningPlanID != null)
                    {
                        SelectJoiningPlanItemInCb((int)lifePolicy.PensionLifeIndustry.FundJoiningPlanID);
                    }
                    //var waivers = cbInsurencesWaivers.Items;
                    //foreach (var item in waivers)
                    //{
                    //    InsurancesWaiver waiver = (InsurancesWaiver)item;

                    //    if (waiver.InsurancesWaiverID == lifePolicy.PensionLifeIndustry.InsurancesWaiverID)
                    //    {
                    //        cbInsurencesWaivers.SelectedItem = item;
                    //        break;
                    //    }
                    //}
                    if (lifePolicy.PensionLifeIndustry.InsurancesWaiverID != null)
                    {
                        SelectWaiverItemInCb((int)lifePolicy.PensionLifeIndustry.InsurancesWaiverID);
                    }
                    //var retirementPlans = cbRetirementPlan.Items;
                    //foreach (var item in retirementPlans)
                    //{
                    //    RetirementPlan plan = (RetirementPlan)item;
                    //    if (plan.RetirementPlanID == lifePolicy.PensionLifeIndustry.RetirementPlanID)
                    //    {
                    //        cbRetirementPlan.SelectedItem = item;
                    //        break;
                    //    }
                    //}
                    if (lifePolicy.PensionLifeIndustry.RetirementPlanID != null)
                    {
                        SelectRetirementPlanItemInCb((int)lifePolicy.PensionLifeIndustry.RetirementPlanID);
                    }
                    txtPensionReceptionAge.Text = lifePolicy.PensionLifeIndustry.PensionReceptionAge.ToString();
                    txtRetirementAgeToPlan.Text = lifePolicy.PensionLifeIndustry.RetirementAgeToPlan.ToString();
                    txtEndContributionsPaymentAge.Text = lifePolicy.PensionLifeIndustry.EndContributionsPaymentAge.ToString();
                    chbIsDesabilityExtension.IsChecked = lifePolicy.PensionLifeIndustry.IsDesabilityExtension;
                    chbIsIndividualCoverage.IsChecked = lifePolicy.PensionLifeIndustry.IsIndividualCoverage;
                    chbIsWithChildren.IsChecked = lifePolicy.PensionLifeIndustry.IsWithChildren;
                    txtOldAgePension.Text = lifePolicy.PensionLifeIndustry.OldAgePension.ToString();
                    txtPensionerRest.Text = validations.ConvertDecimalToString(lifePolicy.PensionLifeIndustry.PensionerRest);
                    txtDisabilityPension.Text = validations.ConvertDecimalToString(lifePolicy.PensionLifeIndustry.DisabilityPension);
                    txtWidowRests.Text = validations.ConvertDecimalToString(lifePolicy.PensionLifeIndustry.WidowRests);
                    txtOrphanRestsUntil21.Text = validations.ConvertDecimalToString(lifePolicy.PensionLifeIndustry.OrphanRestsUntil21);
                    txtMaximumRests.Text = validations.ConvertDecimalToString(lifePolicy.PensionLifeIndustry.MaximumRests);
                    txtRevenueValue.Text = validations.ConvertDecimalToString(lifePolicy.PensionLifeIndustry.RevenueValue);
                    txtDisabilityPercentage.Text = validations.ConvertDecimalToString(lifePolicy.PensionLifeIndustry.DisabilityPercentage);
                    txtWidowPercentage.Text = validations.ConvertDecimalToString(lifePolicy.PensionLifeIndustry.WidowPercentage);
                    txtOrphanPercentage.Text = validations.ConvertDecimalToString(lifePolicy.PensionLifeIndustry.OrphanPercentage);
                    txtParentSupportedPercentage.Text = validations.ConvertDecimalToString(lifePolicy.PensionLifeIndustry.ParentSupportedPercentage);
                    txtCommentsPension.Text = lifePolicy.PensionLifeIndustry.Comments;
                    transfers = lifePolicyLogic.GetTransfersByPolicy(lifePolicy.LifePolicyID);
                    if (transfers.Count > 0)
                    {
                        dgTransferencesPensionBinding();
                    }
                }
            }
            else
            {
                tbItemAdm.Visibility = Visibility.Collapsed;
                tbItemRisk.Visibility = Visibility.Collapsed;
                tbItemMortgage.Visibility = Visibility.Collapsed;
                tbItemPrivate.Visibility = Visibility.Collapsed;
                tbItemPension.Visibility = Visibility.Collapsed;
            }
        }

        private void cbRetirementPlanBinding()
        {
            cbRetirementPlan.ItemsSource = retirementPlansLogic.GetActiveRetirementPlans();
            cbRetirementPlan.DisplayMemberPath = "RetirementPlanName";
        }

        private void cbInsurencesWaiversBinding()
        {
            cbInsurencesWaivers.ItemsSource = insuranceWaiverLogics.GetActiveInsuranceWaivers();
            cbInsurencesWaivers.DisplayMemberPath = "InsurancesWaiverName";
        }

        private void cbFundJoiningPlanBinding()
        {
            cbFundJoiningPlan.ItemsSource = fundsLogics.GetActiveJoiningPlans();
            cbFundJoiningPlan.DisplayMemberPath = "FundJoiningPlanName";
        }

        private void FundsPrivateBinding()
        {
            int? transferringId = null;
            if (cbTransferringFundPrivate.SelectedItem != null)
            {
                transferringId = ((InsuranceCompany)cbTransferringFundPrivate.SelectedItem).CompanyID;
            }
            int? transferedId = null;
            if (cbTransferedToPrivate.SelectedItem != null)
            {
                transferedId = ((InsuranceCompany)cbTransferedToPrivate.SelectedItem).CompanyID;
            }
            var funds = insuranceLogic.GetActiveCompaniesByInsurance("חיים");
            cbTransferringFundPrivate.ItemsSource = funds;
            cbTransferringFundPrivate.DisplayMemberPath = "Company.CompanyName";
            cbTransferedToPrivate.ItemsSource = funds;
            cbTransferedToPrivate.DisplayMemberPath = "Company.CompanyName";
            if (transferringId != null)
            {
                SelectItemInCb((int)transferringId, cbTransferringFundPrivate);
            }
            if (transferedId != null)
            {
                SelectItemInCb((int)transferedId, cbTransferedToPrivate);
            }
        }

        private void cbInvestmentPrivateBinding()
        {
            //int? selectedItemId = null;
            //if (cbInvestmentPrivate.SelectedItem != null)
            //{
            //    selectedItemId = ((InvesmentPlan)cbInvestmentPrivate.SelectedItem).InvesmentPlanID;
            //}
            cbInvestmentPrivate.ItemsSource = investmentPlansLogics.GetActiveInvestmentPlans();
            cbInvestmentPrivate.DisplayMemberPath = "InvesmentPlanName";
            //if (selectedItemId!=null)
            //{
            //    SelectInvesmentPlanItemInCb((int)((InvesmentPlan)cbInvestmentPrivate.SelectedItem).InvesmentPlanID, cbInvestmentPrivate);
            //}
        }


        private void FundsdAdmBinding()
        {
            int? transferringId = null;
            if (cbTransferringFundAdm.SelectedItem != null)
            {
                transferringId = ((InsuranceCompany)cbTransferringFundAdm.SelectedItem).CompanyID;
            }
            int? transferedId = null;
            if (cbTransferedToAdm.SelectedItem != null)
            {
                transferedId = ((InsuranceCompany)cbTransferedToAdm.SelectedItem).CompanyID;
            }
            var funds = insuranceLogic.GetActiveCompaniesByInsurance("חיים");
            cbTransferringFundAdm.ItemsSource = funds;
            cbTransferringFundAdm.DisplayMemberPath = "Company.CompanyName";
            cbTransferedToAdm.ItemsSource = funds;
            cbTransferedToAdm.DisplayMemberPath = "Company.CompanyName";
            if (transferringId != null)
            {
                SelectItemInCb((int)transferringId, cbTransferringFundAdm);
            }
            if (transferedId != null)
            {
                SelectItemInCb((int)transferedId, cbTransferedToAdm);
            }
        }

        private void cbCompany_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cbCompany.SelectedItem == null)
            {
                cbPrincipalAgentNumber.SelectedItem = null;
                cbSecundaryAgentNumber.SelectedItem = null;
                cbPrincipalAgentNumber.IsEnabled = false;
                cbSecundaryAgentNumber.IsEnabled = false;
                return;
            }
            if (cbLifeIndustry.SelectedItem != null)
            {
                cbInsuranceType.IsEnabled = true;
                btnUpdateInsuranceTypeTable.IsEnabled = true;
                cbInsuranceTypeBinding();
                if (lifePolicy != null)
                {
                    //var insuranceTypes = cbInsuranceType.Items;
                    //foreach (var item in insuranceTypes)
                    //{
                    //    FundType insuranceType = (FundType)item;
                    //    if (insuranceType.FundTypeID == lifePolicy.FundTypeID)
                    //    {
                    //        cbInsuranceType.SelectedItem = item;
                    //        break;
                    //    }
                    //}
                    SelectFundItemInCb(lifePolicy.FundTypeID, cbInsuranceType);
                }
            }
            int insuranceID = insuranceLogic.GetInsuranceID("חיים");
            if (client != null)
            {
                cbPrincipalAgentNumber.ItemsSource = agentLogic.GetAgentNumbersByCompanyAndInsurance(client.PrincipalAgentID, (InsuranceCompany)cbCompany.SelectedItem, insuranceID);
                cbPrincipalAgentNumber.DisplayMemberPath = "Number";
                cbPrincipalAgentNumber.IsEnabled = true;
                cbPrincipalAgentNumber.SelectedIndex = 0;
                cbSecundaryAgentNumber.ItemsSource = agentLogic.GetAgentNumbersByCompanyAndInsurance(client.SecundaryAgentID, (InsuranceCompany)cbCompany.SelectedItem, insuranceID);
                cbSecundaryAgentNumber.DisplayMemberPath = "Number";
                cbSecundaryAgentNumber.IsEnabled = true;
                cbSecundaryAgentNumber.SelectedIndex = 0;
            }
            if (cbCompany.SelectedItem != null && cbPolicyHolder.SelectedItem != null)
            {
                cbPolicyHolderNumber.IsEnabled = true;
                cbPolicyHolderNumber.ItemsSource = policyHoldersLogic.GetNumbersByPolicyHolderAndCompany(((PolicyOwner)cbPolicyHolder.SelectedItem).PolicyOwnerID, ((InsuranceCompany)cbCompany.SelectedItem).CompanyID, insuranceID);
                cbPolicyHolderNumber.DisplayMemberPath = "Number";
                cbPolicyHolderNumber.SelectedIndex = 0;
            }
            else
            {
                cbPolicyHolderNumber.SelectedItem = null;
                cbPolicyHolderNumber.IsEnabled = false;
            }
            //else
            //{
            //    cbPrincipalAgentNumber.ItemsSource = agentLogic.GetAgentNumbersByCompanyAndInsurance(policyClient.PrincipalAgentID, (InsuranceCompany)cbCompany.SelectedItem, insuranceID);
            //}

            //if (client != null)
            //{

            //}
            //else
            //{
            //    cbSecundaryAgentNumber.ItemsSource = agentLogic.GetAgentNumbersByCompanyAndInsurance(policyClient.SecundaryAgentID, (InsuranceCompany)cbCompany.SelectedItem, insuranceID);
            //}

        }

        //private void btnUpdateInsuranceTypeTable_Click(object sender, RoutedEventArgs e)
        //{
        //    LifeIndustry industry = (LifeIndustry)cbLifeIndustry.SelectedItem;
        //    LifeInsuranceType newType = new LifeInsuranceType() { LifeIndustryID = industry.LifeIndustryID };
        //    frmUpdateTable updateLifeInsuranceTypesTable = new frmUpdateTable(newType);
        //    updateLifeInsuranceTypesTable.ShowDialog();
        //    cbInsuranceTypeBinding();
        //}

        private void btnUpdatePolicyHolderTable_Click(object sender, RoutedEventArgs e)
        {
            frmAddPolicyOwner policyOwnerWindow = new frmAddPolicyOwner();
            policyOwnerWindow.ShowDialog();
            cbPolicyHolderBinding();
        }

        private void cbPolicyHolder_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            int insuranceId = insuranceLogic.GetInsuranceID("חיים");
            if (cbCompany.SelectedItem != null && cbPolicyHolder.SelectedItem != null)
            {
                cbPolicyHolderNumber.IsEnabled = true;
                cbPolicyHolderNumber.ItemsSource = policyHoldersLogic.GetNumbersByPolicyHolderAndCompany(((PolicyOwner)cbPolicyHolder.SelectedItem).PolicyOwnerID, ((InsuranceCompany)cbCompany.SelectedItem).CompanyID, insuranceId);
                cbPolicyHolderNumber.DisplayMemberPath = "Number";
                cbPolicyHolderNumber.SelectedIndex = 0;
            }
            else
            {
                cbPolicyHolderNumber.SelectedItem = null;
                cbPolicyHolderNumber.IsEnabled = false;
            }
        }

        private void chbIsReleaseFromPayingPremiums_Checked(object sender, RoutedEventArgs e)
        {
            txtPremiumReleaseAmount.IsEnabled = true;
        }

        private void chbIsReleaseFromPayingPremiums_Unchecked(object sender, RoutedEventArgs e)
        {
            txtPremiumReleaseAmount.Clear();
            txtPremiumReleaseAmount.IsEnabled = false;
        }

        private void btnUpdateInvestmentsTablePrivate_Click(object sender, RoutedEventArgs e)
        {
            string objName = ((Button)sender).Name;
            int? selectedItemId = null;
            ComboBox cb = null;
            if (objName == "btnUpdateInvestmentsTablePrivate")
            {
                if (cbInvestmentPrivate.SelectedItem != null)
                {
                    selectedItemId = ((InvesmentPlan)cbInvestmentPrivate.SelectedItem).InvesmentPlanID;
                }
                cb = cbInvestmentPrivate;
            }
            else if (objName == "btnUpdateInvestmentPlansTablePension")
            {
                if (cbInvestmentPlanPension.SelectedItem != null)
                {
                    selectedItemId = ((InvesmentPlan)cbInvestmentPlanPension.SelectedItem).InvesmentPlanID;
                }
                cb = cbInvestmentPlanPension;
            }
            frmUpdateTable updateInvestmentPlanTable = new frmUpdateTable(new InvesmentPlan());
            updateInvestmentPlanTable.ShowDialog();
            cbInvestmentPrivateBinding();
            cbInvestmentPlanPensionBinding();
            if (updateInvestmentPlanTable.ItemAdded != null)
            {
                SelectInvesmentPlanItemInCb(((InvesmentPlan)updateInvestmentPlanTable.ItemAdded).InvesmentPlanID, cb);
            }
            else if (selectedItemId != null)
            {
                SelectInvesmentPlanItemInCb((int)selectedItemId, cb);
            }
        }

        private void SelectInvesmentPlanItemInCb(int id, ComboBox cb)
        {
            var items = cb.Items;
            foreach (var item in items)
            {
                InvesmentPlan parsedItem = (InvesmentPlan)item;
                if (parsedItem.InvesmentPlanID == id)
                {
                    cb.SelectedItem = item;
                    break;
                }
            }
        }

        private void cbInvestmentPlanPensionBinding()
        {
            //int? selectedItemId = null;
            //if (cbInvestmentPlanPension.SelectedItem != null)
            //{
            //    selectedItemId = ((InvesmentPlan)cbInvestmentPlanPension.SelectedItem).InvesmentPlanID;
            //}
            cbInvestmentPlanPension.ItemsSource = investmentPlansLogics.GetActiveInvestmentPlans();
            cbInvestmentPlanPension.DisplayMemberPath = "InvesmentPlanName";
            //if (selectedItemId != null)
            //{
            //    SelectInvesmentPlanItemInCb((int)((InvesmentPlan)cbInvestmentPlanPension.SelectedItem).InvesmentPlanID, cbInvestmentPlanPension);
            //}           
        }

        private void btnUpdteTransferringFundTableAdm_Click(object sender, RoutedEventArgs e)
        {
            string objName = ((Button)sender).Name;
            int? selectedItemId = null;
            ComboBox cb = null;
            if (objName == "btnUpdteTransferringFundTableAdm")
            {
                if (cbTransferringFundAdm.SelectedItem != null)
                {
                    selectedItemId = ((InsuranceCompany)cbTransferringFundAdm.SelectedItem).CompanyID;
                }
                cb = cbTransferringFundAdm;
            }
            else if (objName == "btnUpdteTransferedToFundTableAdm")
            {
                if (cbTransferedToAdm.SelectedItem != null)
                {
                    selectedItemId = ((InsuranceCompany)cbTransferedToAdm.SelectedItem).CompanyID;
                }
                cb = cbTransferedToAdm;
            }
            else if (objName == "btnUpdteTransferringFundTablePrivate")
            {
                if (cbTransferringFundPrivate.SelectedItem != null)
                {
                    selectedItemId = ((InsuranceCompany)cbTransferringFundPrivate.SelectedItem).CompanyID;
                }
                cb = cbTransferringFundPrivate;
            }
            else if (objName == "btnUpdteTransferedToFundTablePrivate")
            {
                if (cbTransferedToPrivate.SelectedItem != null)
                {
                    selectedItemId = ((InsuranceCompany)cbTransferedToPrivate.SelectedItem).CompanyID;
                }
                cb = cbTransferedToPrivate;
            }
            else if (objName == "btnUpdteTransferringFundTablePension")
            {
                if (cbTransferringFundPension.SelectedItem != null)
                {
                    selectedItemId = ((InsuranceCompany)cbTransferringFundPension.SelectedItem).CompanyID;
                }
                cb = cbTransferringFundPension;
            }
            else if (objName == "btnUpdteTransferedToFundTablePension")
            {
                if (cbTransferedToPension.SelectedItem != null)
                {
                    selectedItemId = ((InsuranceCompany)cbTransferedToPension.SelectedItem).CompanyID;
                }
                cb = cbTransferedToPension;
            }
            int insuranceID = insuranceLogic.GetInsuranceID("חיים");
            InsuranceCompany company = new InsuranceCompany() { InsuranceID = insuranceID };
            frmUpdateTable updateCompaniesTable = new frmUpdateTable(company);
            updateCompaniesTable.ShowDialog();
            FundsdAdmBinding();
            FundsPrivateBinding();
            FundsPensionBinding();
            if (updateCompaniesTable.ItemAdded != null)
            {
                SelectItemInCb(((InsuranceCompany)updateCompaniesTable.ItemAdded).CompanyID, cb);
            }
            else if (selectedItemId != null)
            {
                SelectItemInCb((int)selectedItemId, cb);
            }
        }

        private void FundsPensionBinding()
        {
            int? transferringId = null;
            if (cbTransferringFundPension.SelectedItem != null)
            {
                transferringId = ((InsuranceCompany)cbTransferringFundPension.SelectedItem).CompanyID;
            }
            int? transferedId = null;
            if (cbTransferedToPension.SelectedItem != null)
            {
                transferedId = ((InsuranceCompany)cbTransferedToPension.SelectedItem).CompanyID;
            }
            var funds = insuranceLogic.GetActiveCompaniesByInsurance("חיים");
            cbTransferringFundPension.ItemsSource = funds;
            cbTransferringFundPension.DisplayMemberPath = "Company.CompanyName";
            cbTransferedToPension.ItemsSource = funds;
            cbTransferedToPension.DisplayMemberPath = "Company.CompanyName";
            if (transferringId != null)
            {
                SelectItemInCb((int)transferringId, cbTransferringFundPension);
            }
            if (transferedId != null)
            {
                SelectItemInCb((int)transferedId, cbTransferedToPension);
            }
        }

        private void btnUpdateFundJoiningPlansTable_Click(object sender, RoutedEventArgs e)
        {
            int? selectedItemId = null;
            if (cbFundJoiningPlan.SelectedItem != null)
            {
                selectedItemId = ((FundJoiningPlan)cbFundJoiningPlan.SelectedItem).FundJoiningPlanID;
            }
            frmUpdateTable updateJoiningPlansTable = new frmUpdateTable(new FundJoiningPlan());
            updateJoiningPlansTable.ShowDialog();
            cbFundJoiningPlanBinding();
            if (updateJoiningPlansTable.ItemAdded != null)
            {
                SelectJoiningPlanItemInCb(((FundJoiningPlan)updateJoiningPlansTable.ItemAdded).FundJoiningPlanID);
            }
            else if (selectedItemId != null)
            {
                SelectJoiningPlanItemInCb((int)selectedItemId);
            }
        }

        private void SelectJoiningPlanItemInCb(int id)
        {
            var items = cbFundJoiningPlan.Items;
            foreach (var item in items)
            {
                FundJoiningPlan parsedItem = (FundJoiningPlan)item;
                if (parsedItem.FundJoiningPlanID == id)
                {
                    cbFundJoiningPlan.SelectedItem = item;
                    break;
                }
            }
        }

        private void btnUpdateInsurancesWaiversTable_Click(object sender, RoutedEventArgs e)
        {
            int? selectedItemId = null;
            if (cbInsurencesWaivers.SelectedItem != null)
            {
                selectedItemId = ((InsurancesWaiver)cbInsurencesWaivers.SelectedItem).InsurancesWaiverID;
            }
            frmUpdateTable updateInsuranceWaiversTable = new frmUpdateTable(new InsurancesWaiver());
            updateInsuranceWaiversTable.ShowDialog();
            cbInsurencesWaiversBinding();
            if (updateInsuranceWaiversTable.ItemAdded != null)
            {
                SelectWaiverItemInCb(((InsurancesWaiver)updateInsuranceWaiversTable.ItemAdded).InsurancesWaiverID);
            }
            else if (selectedItemId != null)
            {
                SelectWaiverItemInCb((int)selectedItemId);
            }
        }

        private void SelectWaiverItemInCb(int id)
        {
            var items = cbInsurencesWaivers.Items;
            foreach (var item in items)
            {
                InsurancesWaiver parsedItem = (InsurancesWaiver)item;
                if (parsedItem.InsurancesWaiverID == id)
                {
                    cbInsurencesWaivers.SelectedItem = item;
                    break;
                }
            }
        }

        private void btnUpdateRetirementPlansTable_Click(object sender, RoutedEventArgs e)
        {
            int? selectedItemId = null;
            if (cbRetirementPlan.SelectedItem != null)
            {
                selectedItemId = ((RetirementPlan)cbRetirementPlan.SelectedItem).RetirementPlanID;
            }
            frmUpdateTable updateRetirementPlanTable = new frmUpdateTable(new RetirementPlan());
            updateRetirementPlanTable.ShowDialog();
            cbRetirementPlanBinding();
            if (updateRetirementPlanTable.ItemAdded != null)
            {
                SelectRetirementPlanItemInCb(((RetirementPlan)updateRetirementPlanTable.ItemAdded).RetirementPlanID);
            }
            else if (selectedItemId != null)
            {
                SelectRetirementPlanItemInCb((int)selectedItemId);
            }
        }

        private void SelectRetirementPlanItemInCb(int id)
        {
            var items = cbRetirementPlan.Items;
            foreach (var item in items)
            {
                RetirementPlan parsedItem = (RetirementPlan)item;
                if (parsedItem.RetirementPlanID == id)
                {
                    cbRetirementPlan.SelectedItem = item;
                    break;
                }
            }
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            if (nullErrorList != null && nullErrorList.Count > 0)
            {
                foreach (var item in nullErrorList)
                {
                    if (item is TextBox)
                    {
                        ((TextBox)item).ClearValue(BorderBrushProperty);
                    }
                    else if (item is ComboBox)
                    {
                        ((ComboBox)item).ClearValue(BorderBrushProperty);
                    }
                    else if (item is DatePicker)
                    {
                        ((DatePicker)item).ClearValue(BorderBrushProperty);
                    }
                }
            }
            if (!validations.IsDigitsOnly(txtPolicyNumber.Text))
            {
                cancelClose = true;
                MessageBox.Show("מס' פוליסה יכול להכיל מספרים בלבד", "", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            //בדיקת שדות חובה
            nullErrorList = validations.InputNullValidation(new object[] { txtPolicyNumber, txtAddition, dpStartDate, dpEndDate, cbLifeIndustry, cbCompany, cbInsuranceType, txtMonthlyDeposit });
            if (nullErrorList.Count > 0)
            {
                tbItemGeneral.Focus();
                cancelClose = true;
                MessageBox.Show("נא למלאות שדות חובה", "", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (dpEndDate.SelectedDate <= dpStartDate.SelectedDate)
            {
                tbItemGeneral.Focus();
                cancelClose = true;
                MessageBox.Show("תאריך סיום חייב להיות גדול מתאריך תחילה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            //ולידציה של שדות מספריים
            //numbersErrorList = validations.OnlyNumbersValidation(new TextBox[] { txtAddition, txtSubAnnual, txtPolicyFactor, txtMonthlyDeposit });
            //if (numbersErrorList.Count > 0)
            //{
            //    tbItemGeneral.Focus();
            //    MessageBox.Show("נא להזין מספרים תקינים", "", MessageBoxButton.OK, MessageBoxImage.Error);
            //    return;
            //}
            int addition = Convert.ToInt32(txtAddition.Text);
            bool? isIndexation = null;
            if (cbIndexation.SelectedIndex == 0)
            {
                isIndexation = true;
            }
            if (cbIndexation.SelectedIndex == 1)
            {
                isIndexation = false;
            }
            decimal? subAnnual = null;
            decimal subAnnualResult;
            if (Decimal.TryParse(txtSubAnnual.Text, out subAnnualResult))
            {
                subAnnual = subAnnualResult;
            }
            decimal? policyFactor = null;
            decimal policyFactorResult;
            if (Decimal.TryParse(txtPolicyFactor.Text, out policyFactorResult))
            {
                policyFactor = policyFactorResult;
            }
            decimal? totalMonthlyPayment = null;
            decimal totalMonthlyPaymentResult;

            if (Decimal.TryParse(txtMonthlyDeposit.Text, out totalMonthlyPaymentResult))
            {
                totalMonthlyPayment = totalMonthlyPaymentResult;
            }
            int? principalAgentNumberID = null;
            if ((AgentNumber)cbPrincipalAgentNumber.SelectedItem != null)
            {
                principalAgentNumberID = ((AgentNumber)cbPrincipalAgentNumber.SelectedItem).AgentNumberID;
            }
            int? subAgentNumberID = null;
            if ((AgentNumber)cbSecundaryAgentNumber.SelectedItem != null)
            {
                subAgentNumberID = ((AgentNumber)cbSecundaryAgentNumber.SelectedItem).AgentNumberID;
            }
            int? policyOwnerID = null;
            if ((PolicyOwner)cbPolicyHolder.SelectedItem != null)
            {
                policyOwnerID = ((PolicyOwner)cbPolicyHolder.SelectedItem).PolicyOwnerID;
            }
            int? factoryNumberID = null;
            if ((FactoryNumber)cbPolicyHolderNumber.SelectedItem != null)
            {
                factoryNumberID = ((FactoryNumber)cbPolicyHolderNumber.SelectedItem).FactoryNumberID;
            }
            string paymentMode = null;
            if (cbPaymentMode.SelectedItem != null)
            {
                paymentMode = ((ComboBoxItem)cbPaymentMode.SelectedItem).Content.ToString();
            }
            try
            {
                if (lifePolicy == null || isAddition == true || isMortgageAddition)
                {
                    policyId = lifePolicyLogic.InsertLifePolicy(client.ClientID, isOffer, ((LifeIndustry)cbLifeIndustry.SelectedItem).LifeIndustryID, txtPolicyNumber.Text, addition, (DateTime)dpOpenDate.SelectedDate, ((InsuranceCompany)cbCompany.SelectedItem).CompanyID, principalAgentNumberID, subAgentNumberID, ((FundType)cbInsuranceType.SelectedItem).FundTypeID, dpStartDate.SelectedDate, dpEndDate.SelectedDate, txtPrimaryIndex.Text, txtInsurancedName.Text, txtInsurancedNumber.Text, policyOwnerID, factoryNumberID, paymentMode, isIndexation, subAnnual, policyFactor, totalMonthlyPayment, txtBankName.Text, txtBankBranch.Text, txtBankAccount.Text, chbIsMortgaged.IsChecked, txtMortgagedBank.Text, txtMortgagedBranch.Text, txtMortgagedAddress.Text, txtMortgagedBankPhone.Text, txtMortgagedBankFax.Text, txtMortgagedBankContactName.Text, txtMortgagedBankEmail.Text, txtCommentsGeneral.Text);
                    if (!isOffer)
                    {
                        path = lifePath + @"\חיים\" + txtPolicyNumber.Text + " " + ((LifeIndustry)cbLifeIndustry.SelectedItem).LifeIndustryName;
                    }
                    else
                    {
                        path = lifePath + @"\הצעות\חיים\" + txtPolicyNumber.Text + " " + ((LifeIndustry)cbLifeIndustry.SelectedItem).LifeIndustryName;
                    }
                }
                else
                {
                    lifePolicyLogic.UpdateLifePolicy(lifePolicy.LifePolicyID, txtPolicyNumber.Text, ((LifeIndustry)cbLifeIndustry.SelectedItem).LifeIndustryID, ((InsuranceCompany)cbCompany.SelectedItem).CompanyID, principalAgentNumberID, subAgentNumberID, ((FundType)cbInsuranceType.SelectedItem).FundTypeID, dpStartDate.SelectedDate, dpEndDate.SelectedDate, txtPrimaryIndex.Text, txtInsurancedName.Text, txtInsurancedNumber.Text, policyOwnerID, factoryNumberID, paymentMode, isIndexation, subAnnual, policyFactor, totalMonthlyPayment, txtBankName.Text, txtBankBranch.Text, txtBankAccount.Text, chbIsMortgaged.IsChecked, txtMortgagedBank.Text, txtMortgagedBranch.Text, txtMortgagedAddress.Text, txtMortgagedBankPhone.Text, txtMortgagedBankFax.Text, txtMortgagedBankContactName.Text, txtMortgagedBankEmail.Text, txtCommentsGeneral.Text);
                    if (!isOffer)
                    {
                        string[] dirs = Directory.GetDirectories(lifePath + @"\חיים", "*" + lifePolicy.PolicyNumber + " " + lifePolicy.LifeIndustry.LifeIndustryName + "*");
                        if (dirs.Count() > 0)
                        {
                            path = dirs[0];
                        }
                        else
                        {
                            dirs = Directory.GetDirectories(lifePath + @"\חיים", "*" + lifePolicy.PolicyNumber + "*");
                            if (dirs.Count() > 0)
                            {
                                path = dirs[0];
                            }
                        }
                        newPath = lifePath + @"\חיים\" + txtPolicyNumber.Text + " " + ((LifeIndustry)cbLifeIndustry.SelectedItem).LifeIndustryName;
                    }
                    else
                    {
                        string[] dirs = Directory.GetDirectories(lifePath + @"\הצעות\חיים", "*" + lifePolicy.PolicyNumber + " " + lifePolicy.LifeIndustry.LifeIndustryName + "*");
                        if (dirs.Count() > 0)
                        {
                            path = dirs[0];
                        }
                        else
                        {
                            dirs = Directory.GetDirectories(lifePath + @"\הצעות\חיים", "*" + lifePolicy.PolicyNumber + "*");
                            if (dirs.Count() > 0)
                            {
                                path = dirs[0];
                            }
                        }
                        newPath = lifePath + @"\הצעות\חיים\" + txtPolicyNumber.Text + " " + ((LifeIndustry)cbLifeIndustry.SelectedItem).LifeIndustryName;
                    }
                }
                if (tbItemAdm.Visibility == Visibility.Visible)
                {
                    if (lifePolicy == null || isAddition == true)
                    {
                        lifePolicyLogic.InsertAdmPolicy(policyId, chbIsCompulsoryPensionAdm.IsChecked, chbIsShareholderAdm.IsChecked, txtCommentsAdm.Text);
                        SaveUnsavedSalaryChange();
                        if (salaryChanges.Count > 0)
                        {
                            lifePolicyLogic.InsertSalaryChangesToPolicy(policyId, salaryChanges, isAddition);
                        }
                        SaveUnsavedAdmTransfer();
                        if (transfers.Count > 0)
                        {
                            lifePolicyLogic.InsertPolicyTransfer(policyId, transfers, isAddition);
                        }
                        if (chbIsRisksAdm.IsChecked == true)
                        {
                            FillEmptyTextBoxes(new TextBox[] { txtDeathAmountAdm, txtWorkImpossibilityAmountAdm, txtIncomeSecureAmountAdm, txtDesabilityByAccidentAmountAdm, txtDeathByAccidentAmountAdm, txtCriticalIllnesesAmountAdm, txtOther1AmountAdm, txtOther2AmountAdm, txtOther3AmountAdm, txtOther4AmountAdm, txtOther5AmountAdm, txtDeathPremiumAdm, txtWorkImpossibilityPremiumAdm, txtIncomeSecurePremiumAdm, txtDesabilityByAccidentPremiumAdm, txtDeathByAccidentPremiumAdm, txtCriticalIllnesesPremiumAdm, txtOther1PremiumAdm, txtOther2PremiumAdm, txtOther3PremiumAdm, txtOther4PremiumAdm, txtOther5PremiumAdm });
                            Risk newRisk = new Risk()
                            {
                                IsDeathType1 = rbDeath1Adm.IsChecked,
                                IsDeathType5 = rbDeath5Adm.IsChecked,
                                IsDeathTypeFixed = rbDeathFixedAdm.IsChecked,
                                DeathAmount = Decimal.Parse(txtDeathAmountAdm.Text),
                                DeathPeriod = txtDeathPeriodAdm.Text,
                                DeathCoverageType = txtDeathCoverageTypeAdm.Text,
                                DeathPremium = Decimal.Parse(txtDeathPremiumAdm.Text),
                                DeathComments = txtDeathCommentsAdm.Text,
                                IsProfessionalWorkImpossibility = rbProfessionalWorkImposibilitationAdm.IsChecked,
                                WorkImpossibilityAmount = Decimal.Parse(txtWorkImpossibilityAmountAdm.Text),
                                WorkImpossibilityPeriod = txtWorkImpossibilityPeriodAdm.Text,
                                WorkImpossibilityCoverageType = txtWorkImpossibilityCoverageTypeAdm.Text,
                                WorkImpossibilityPremium = Decimal.Parse(txtWorkImpossibilityPremiumAdm.Text),
                                WorkImpossibilityComments = txtWorkImpossibilityCommentsAdm.Text,
                                SecureIncomeAmount = Decimal.Parse(txtIncomeSecureAmountAdm.Text),
                                SecureIncomePeriod = txtIncomeSecurePeriodAdm.Text,
                                SecureIncomeCoverageType = txtIncomeSecureCoverageTypeAdm.Text,
                                SecureIncomePremium = Decimal.Parse(txtIncomeSecurePremiumAdm.Text),
                                SecureIncomeComments = txtIncomeSecureCommentsAdm.Text,
                                DisabilityByAccidentAmount = Decimal.Parse(txtDesabilityByAccidentAmountAdm.Text),
                                DisabilityByAccidentPeriod = txtDesabilityByAccidentPeriodAdm.Text,
                                DisabilityByAccidentCoverageType = txtDesabilityByAccidentCoverageTypeAdm.Text,
                                DisabilityByAccidentPremium = Decimal.Parse(txtDesabilityByAccidentPremiumAdm.Text),
                                DisabilityByAccidentComments = txtDesabilityByAccidentCommentsAdm.Text,
                                DeathByAccidentAmount = Decimal.Parse(txtDeathByAccidentAmountAdm.Text),
                                DeathByAccidentPeriod = txtDeathByAccidentPeriodAdm.Text,
                                DeathByAccidentCoverageType = txtDeathByAccidentCoverageTypeAdm.Text,
                                DeathByAccidentPremium = Decimal.Parse(txtDeathByAccidentPremiumAdm.Text),
                                DeathByAccidentComments = txtDeathByAccidentCommentsAdm.Text,
                                CriticalIllnessAmount = Decimal.Parse(txtCriticalIllnesesAmountAdm.Text),
                                CriticalIllnessPeriod = txtCriticalIllnesesPeriodAdm.Text,
                                CriticalIllnessCoverageType = txtCriticalIllnesesCoverageTypeAdm.Text,
                                CriticalIllnessPremium = Decimal.Parse(txtCriticalIllnesesPremiumAdm.Text),
                                CriticalIllnessComments = txtCriticalIllnesesCommentsAdm.Text,
                                Other1Name = txtOther1Adm.Text,
                                Other1Amount = Decimal.Parse(txtOther1AmountAdm.Text),
                                Other1Period = txtOther1PeriodAdm.Text,
                                Other1CoverageType = txtOther1CoverageTypeAdm.Text,
                                Other1Premium = Decimal.Parse(txtOther1PremiumAdm.Text),
                                Other1Comments = txtOther1CommentsAdm.Text,
                                Other2Name = txtOther2Adm.Text,
                                Other2Amount = Decimal.Parse(txtOther2AmountAdm.Text),
                                Other2Period = txtOther2PeriodAdm.Text,
                                Other2CoverageType = txtOther2CoverageTypeAdm.Text,
                                Other2Premium = Decimal.Parse(txtOther2PremiumAdm.Text),
                                Other2Comments = txtOther2CommentsAdm.Text,
                                Other3Name = txtOther3Adm.Text,
                                Other3Amount = Decimal.Parse(txtOther3AmountAdm.Text),
                                Other3Period = txtOther3PeriodAdm.Text,
                                Other3CoverageType = txtOther3CoverageTypeAdm.Text,
                                Other3Premium = Decimal.Parse(txtOther3PremiumAdm.Text),
                                Other3Comments = txtOther3CommentsAdm.Text,
                                Other4Name = txtOther4Adm.Text,
                                Other4Amount = Decimal.Parse(txtOther4AmountAdm.Text),
                                Other4Period = txtOther4PeriodAdm.Text,
                                Other4CoverageType = txtOther4CoverageTypeAdm.Text,
                                Other4Premium = Decimal.Parse(txtOther4PremiumAdm.Text),
                                Other4Comments = txtOther4CommentsAdm.Text,
                                Other5Name = txtOther5Adm.Text,
                                Other5Amount = Decimal.Parse(txtOther5AmountAdm.Text),
                                Other5Period = txtOther5PeriodAdm.Text,
                                Other5CoverageType = txtOther5CoverageTypeAdm.Text,
                                Other5Premium = Decimal.Parse(txtOther5PremiumAdm.Text),
                                Other5Comments = txtOther5CommentsAdm.Text
                            };
                            risks.Add(newRisk);
                            lifePolicyLogic.InsertRisks(policyId, risks);
                        }
                    }
                    else
                    {
                        lifePolicyLogic.UpdateAdmPolicy(lifePolicy.LifePolicyID, chbIsCompulsoryPensionAdm.IsChecked, chbIsShareholderAdm.IsChecked, txtCommentsAdm.Text);
                        SaveUnsavedSalaryChange();
                        lifePolicyLogic.InsertSalaryChangesToPolicy(lifePolicy.LifePolicyID, salaryChanges, isAddition);
                        SaveUnsavedAdmTransfer();
                        lifePolicyLogic.InsertPolicyTransfer(lifePolicy.LifePolicyID, transfers, isAddition);
                        if (chbIsRisksAdm.IsChecked == false)
                        {
                            risks.Clear();
                        }
                        //update risk
                        else
                        {
                            FillEmptyTextBoxes(new TextBox[] { txtDeathAmountAdm, txtWorkImpossibilityAmountAdm, txtIncomeSecureAmountAdm, txtDesabilityByAccidentAmountAdm, txtDeathByAccidentAmountAdm, txtCriticalIllnesesAmountAdm, txtOther1AmountAdm, txtOther2AmountAdm, txtOther3AmountAdm, txtOther4AmountAdm, txtOther5AmountAdm, txtDeathPremiumAdm, txtWorkImpossibilityPremiumAdm, txtIncomeSecurePremiumAdm, txtDesabilityByAccidentPremiumAdm, txtDeathByAccidentPremiumAdm, txtCriticalIllnesesPremiumAdm, txtOther1PremiumAdm, txtOther2PremiumAdm, txtOther3PremiumAdm, txtOther4PremiumAdm, txtOther5PremiumAdm });

                            risks[0].IsDeathType1 = rbDeath1Adm.IsChecked;
                            risks[0].IsDeathType5 = rbDeath5Adm.IsChecked;
                            risks[0].IsDeathTypeFixed = rbDeathFixedAdm.IsChecked;
                            risks[0].DeathAmount = Decimal.Parse(txtDeathAmountAdm.Text);
                            risks[0].DeathPeriod = txtDeathPeriodAdm.Text;
                            risks[0].DeathCoverageType = txtDeathCoverageTypeAdm.Text;
                            risks[0].DeathPremium = Decimal.Parse(txtDeathPremiumAdm.Text);
                            risks[0].DeathComments = txtDeathCommentsAdm.Text;
                            risks[0].IsProfessionalWorkImpossibility = rbProfessionalWorkImposibilitationAdm.IsChecked;
                            risks[0].WorkImpossibilityAmount = Decimal.Parse(txtWorkImpossibilityAmountAdm.Text);
                            risks[0].WorkImpossibilityPeriod = txtWorkImpossibilityPeriodAdm.Text;
                            risks[0].WorkImpossibilityCoverageType = txtWorkImpossibilityCoverageTypeAdm.Text;
                            risks[0].WorkImpossibilityPremium = Decimal.Parse(txtWorkImpossibilityPremiumAdm.Text);
                            risks[0].WorkImpossibilityComments = txtWorkImpossibilityCommentsAdm.Text;
                            risks[0].SecureIncomeAmount = Decimal.Parse(txtIncomeSecureAmountAdm.Text);
                            risks[0].SecureIncomePeriod = txtIncomeSecurePeriodAdm.Text;
                            risks[0].SecureIncomeCoverageType = txtIncomeSecureCoverageTypeAdm.Text;
                            risks[0].SecureIncomePremium = Decimal.Parse(txtIncomeSecurePremiumAdm.Text);
                            risks[0].SecureIncomeComments = txtIncomeSecureCommentsAdm.Text;
                            risks[0].DisabilityByAccidentAmount = Decimal.Parse(txtDesabilityByAccidentAmountAdm.Text);
                            risks[0].DisabilityByAccidentPeriod = txtDesabilityByAccidentPeriodAdm.Text;
                            risks[0].DisabilityByAccidentCoverageType = txtDesabilityByAccidentCoverageTypeAdm.Text;
                            risks[0].DisabilityByAccidentPremium = Decimal.Parse(txtDesabilityByAccidentPremiumAdm.Text);
                            risks[0].DisabilityByAccidentComments = txtDesabilityByAccidentCommentsAdm.Text;
                            risks[0].DeathByAccidentAmount = Decimal.Parse(txtDeathByAccidentAmountAdm.Text);
                            risks[0].DeathByAccidentPeriod = txtDeathByAccidentPeriodAdm.Text;
                            risks[0].DeathByAccidentCoverageType = txtDeathByAccidentCoverageTypeAdm.Text;
                            risks[0].DeathByAccidentPremium = Decimal.Parse(txtDeathByAccidentPremiumAdm.Text);
                            risks[0].DeathByAccidentComments = txtDeathByAccidentCommentsAdm.Text;
                            risks[0].CriticalIllnessAmount = Decimal.Parse(txtCriticalIllnesesAmountAdm.Text);
                            risks[0].CriticalIllnessPeriod = txtCriticalIllnesesPeriodAdm.Text;
                            risks[0].CriticalIllnessCoverageType = txtCriticalIllnesesCoverageTypeAdm.Text;
                            risks[0].CriticalIllnessPremium = Decimal.Parse(txtCriticalIllnesesPremiumAdm.Text);
                            risks[0].CriticalIllnessComments = txtCriticalIllnesesCommentsAdm.Text;
                            risks[0].Other1Name = txtOther1Adm.Text;
                            risks[0].Other1Amount = Decimal.Parse(txtOther1AmountAdm.Text);
                            risks[0].Other1Period = txtOther1PeriodAdm.Text;
                            risks[0].Other1CoverageType = txtOther1CoverageTypeAdm.Text;
                            risks[0].Other1Premium = Decimal.Parse(txtOther1PremiumAdm.Text);
                            risks[0].Other1Comments = txtOther1CommentsAdm.Text;
                            risks[0].Other2Name = txtOther2Adm.Text;
                            risks[0].Other2Amount = Decimal.Parse(txtOther2AmountAdm.Text);
                            risks[0].Other2Period = txtOther2PeriodAdm.Text;
                            risks[0].Other2CoverageType = txtOther2CoverageTypeAdm.Text;
                            risks[0].Other2Premium = Decimal.Parse(txtOther2PremiumAdm.Text);
                            risks[0].Other2Comments = txtOther2CommentsAdm.Text;
                            risks[0].Other3Name = txtOther3Adm.Text;
                            risks[0].Other3Amount = Decimal.Parse(txtOther3AmountAdm.Text);
                            risks[0].Other3Period = txtOther3PeriodAdm.Text;
                            risks[0].Other3CoverageType = txtOther3CoverageTypeAdm.Text;
                            risks[0].Other3Premium = Decimal.Parse(txtOther3PremiumAdm.Text);
                            risks[0].Other3Comments = txtOther3CommentsAdm.Text;
                            risks[0].Other4Name = txtOther4Adm.Text;
                            risks[0].Other4Amount = Decimal.Parse(txtOther4AmountAdm.Text);
                            risks[0].Other4Period = txtOther4PeriodAdm.Text;
                            risks[0].Other4CoverageType = txtOther4CoverageTypeAdm.Text;
                            risks[0].Other4Premium = Decimal.Parse(txtOther4PremiumAdm.Text);
                            risks[0].Other4Comments = txtOther4CommentsAdm.Text;
                            risks[0].Other5Name = txtOther5Adm.Text;
                            risks[0].Other5Amount = Decimal.Parse(txtOther5AmountAdm.Text);
                            risks[0].Other5Period = txtOther5PeriodAdm.Text;
                            risks[0].Other5CoverageType = txtOther5CoverageTypeAdm.Text;
                            risks[0].Other5Premium = Decimal.Parse(txtOther5PremiumAdm.Text);
                            risks[0].Other5Comments = txtOther5CommentsAdm.Text;
                        }
                        lifePolicyLogic.InsertRisks(lifePolicy.LifePolicyID, risks);
                    }

                }
                else if (tbItemRisk.Visibility == Visibility.Visible)
                {
                    FillEmptyTextBoxes(new TextBox[] { txtDeathAmountRisk, txtWorkImpossibilityAmountRisk, txtIncomeSecureAmountRisk, txtDesabilityByAccidentAmountRisk, txtDeathByAccidentAmountRisk, txtCriticalIllnesesAmountRisk, txtOther1AmountRisk, txtOther2AmountRisk, txtOther3AmountRisk, txtOther4AmountRisk, txtOther5AmountRisk, txtDeathPremiumRisk, txtWorkImpossibilityPremiumRisk, txtIncomeSecurePremiumRisk, txtDesabilityByAccidentPremiumRisk, txtDeathByAccidentPremiumRisk, txtCriticalIllnesesPremiumRisk, txtOther1PremiumRisk, txtOther2PremiumRisk, txtOther3PremiumRisk, txtOther4PremiumRisk, txtOther5PremiumRisk });
                    if (lifePolicy == null || isAddition == true)
                    {
                        SaveUnsavedAdditionalInsured();
                        List<int> additionalIds = lifePolicyLogic.InsertAdditionalInsureds(policyId, additionalsInsureds, isAddition);
                        Risk newRisk = new Risk()
                        {
                            LifeAdditionalInsuredID = null,
                            IsDeathType1 = rbDeath1Risk.IsChecked,
                            IsDeathType5 = rbDeath5Risk.IsChecked,
                            IsDeathTypeFixed = rbDeathFixedRisk.IsChecked,
                            DeathAmount = Decimal.Parse(txtDeathAmountRisk.Text),
                            DeathPeriod = txtDeathPeriodRisk.Text,
                            DeathCoverageType = txtDeathCoverageTypeRisk.Text,
                            DeathPremium = Decimal.Parse(txtDeathPremiumRisk.Text),
                            DeathComments = txtDeathCommentsRisk.Text,
                            IsProfessionalWorkImpossibility = rbProfessionalWorkImposibilitationRisk.IsChecked,
                            WorkImpossibilityAmount = Decimal.Parse(txtWorkImpossibilityAmountRisk.Text),
                            WorkImpossibilityPeriod = txtWorkImpossibilityPeriodRisk.Text,
                            WorkImpossibilityCoverageType = txtWorkImpossibilityCoverageTypeRisk.Text,
                            WorkImpossibilityPremium = Decimal.Parse(txtWorkImpossibilityPremiumRisk.Text),
                            WorkImpossibilityComments = txtWorkImpossibilityCommentsRisk.Text,
                            SecureIncomeAmount = Decimal.Parse(txtIncomeSecureAmountRisk.Text),
                            SecureIncomePeriod = txtIncomeSecurePeriodRisk.Text,
                            SecureIncomeCoverageType = txtIncomeSecureCoverageTypeRisk.Text,
                            SecureIncomePremium = Decimal.Parse(txtIncomeSecurePremiumRisk.Text),
                            SecureIncomeComments = txtIncomeSecureCommentsRisk.Text,
                            DisabilityByAccidentAmount = Decimal.Parse(txtDesabilityByAccidentAmountRisk.Text),
                            DisabilityByAccidentPeriod = txtDesabilityByAccidentPeriodRisk.Text,
                            DisabilityByAccidentCoverageType = txtDesabilityByAccidentCoverageTypeRisk.Text,
                            DisabilityByAccidentPremium = Decimal.Parse(txtDesabilityByAccidentPremiumRisk.Text),
                            DisabilityByAccidentComments = txtDesabilityByAccidentCommentsRisk.Text,
                            DeathByAccidentAmount = Decimal.Parse(txtDeathByAccidentAmountRisk.Text),
                            DeathByAccidentPeriod = txtDeathByAccidentPeriodRisk.Text,
                            DeathByAccidentCoverageType = txtDeathByAccidentCoverageTypeRisk.Text,
                            DeathByAccidentPremium = Decimal.Parse(txtDeathByAccidentPremiumRisk.Text),
                            DeathByAccidentComments = txtDeathByAccidentCommentsRisk.Text,
                            CriticalIllnessAmount = Decimal.Parse(txtCriticalIllnesesAmountRisk.Text),
                            CriticalIllnessPeriod = txtCriticalIllnesesPeriodRisk.Text,
                            CriticalIllnessCoverageType = txtCriticalIllnesesCoverageTypeRisk.Text,
                            CriticalIllnessPremium = Decimal.Parse(txtCriticalIllnesesPremiumRisk.Text),
                            CriticalIllnessComments = txtCriticalIllnesesCommentsRisk.Text,
                            Other1Name = txtOther1.Text,
                            Other1Amount = Decimal.Parse(txtOther1AmountRisk.Text),
                            Other1Period = txtOther1PeriodRisk.Text,
                            Other1CoverageType = txtOther1CoverageTypeRisk.Text,
                            Other1Premium = Decimal.Parse(txtOther1PremiumRisk.Text),
                            Other1Comments = txtOther1CommentsRisk.Text,
                            Other2Name = txtOther2.Text,
                            Other2Amount = Decimal.Parse(txtOther2AmountRisk.Text),
                            Other2Period = txtOther2PeriodRisk.Text,
                            Other2CoverageType = txtOther2CoverageTypeRisk.Text,
                            Other2Premium = Decimal.Parse(txtOther2PremiumRisk.Text),
                            Other2Comments = txtOther2CommentsRisk.Text,
                            Other3Name = txtOther3.Text,
                            Other3Amount = Decimal.Parse(txtOther3AmountRisk.Text),
                            Other3Period = txtOther3PeriodRisk.Text,
                            Other3CoverageType = txtOther3CoverageTypeRisk.Text,
                            Other3Premium = Decimal.Parse(txtOther3PremiumRisk.Text),
                            Other3Comments = txtOther3CommentsRisk.Text,
                            Other4Name = txtOther4.Text,
                            Other4Amount = Decimal.Parse(txtOther4AmountRisk.Text),
                            Other4Period = txtOther4PeriodRisk.Text,
                            Other4CoverageType = txtOther4CoverageTypeRisk.Text,
                            Other4Premium = Decimal.Parse(txtOther4PremiumRisk.Text),
                            Other4Comments = txtOther4CommentsRisk.Text,
                            Other5Name = txtOther5.Text,
                            Other5Amount = Decimal.Parse(txtOther5AmountRisk.Text),
                            Other5Period = txtOther5PeriodRisk.Text,
                            Other5CoverageType = txtOther5CoverageTypeRisk.Text,
                            Other5Premium = Decimal.Parse(txtOther5PremiumRisk.Text),
                            Other5Comments = txtOther5CommentsRisk.Text
                        };
                        risks.Add(newRisk);
                        //if (tbItemInsurencedRisk2.IsVisible)
                        if (additionalIds.Count > 0)
                        {
                            FillEmptyTextBoxes(new TextBox[] { txtDeathAmountRisk2, txtWorkImpossibilityAmountRisk2, txtIncomeSecureAmountRisk2, txtDesabilityByAccidentAmountRisk2, txtDeathByAccidentAmountRisk2, txtCriticalIllnesesAmountRisk2, txtOther1AmountRisk2, txtOther2AmountRisk2, txtOther3AmountRisk2, txtOther4AmountRisk2, txtOther5AmountRisk2, txtDeathPremiumRisk2, txtWorkImpossibilityPremiumRisk2, txtIncomeSecurePremiumRisk2, txtDesabilityByAccidentPremiumRisk2, txtDeathByAccidentPremiumRisk2, txtCriticalIllnesesPremiumRisk2, txtOther1PremiumRisk2, txtOther2PremiumRisk2, txtOther3PremiumRisk2, txtOther4PremiumRisk2, txtOther5PremiumRisk2 });
                            Risk newAdditionalRisk = new Risk()
                            {
                                LifeAdditionalInsuredID = additionalIds[0],
                                IsDeathType1 = rbDeath1Risk2.IsChecked,
                                IsDeathType5 = rbDeath5Risk2.IsChecked,
                                IsDeathTypeFixed = rbDeathFixedRisk2.IsChecked,
                                DeathAmount = Decimal.Parse(txtDeathAmountRisk2.Text),
                                DeathPeriod = txtDeathPeriodRisk2.Text,
                                DeathCoverageType = txtDeathCoverageTypeRisk2.Text,
                                DeathPremium = Decimal.Parse(txtDeathPremiumRisk2.Text),
                                DeathComments = txtDeathCommentsRisk2.Text,
                                IsProfessionalWorkImpossibility = rbProfessionalWorkImposibilitationRisk2.IsChecked,
                                WorkImpossibilityAmount = Decimal.Parse(txtWorkImpossibilityAmountRisk2.Text),
                                WorkImpossibilityPeriod = txtWorkImpossibilityPeriodRisk2.Text,
                                WorkImpossibilityCoverageType = txtWorkImpossibilityCoverageTypeRisk2.Text,
                                WorkImpossibilityPremium = Decimal.Parse(txtWorkImpossibilityPremiumRisk2.Text),
                                WorkImpossibilityComments = txtWorkImpossibilityCommentsRisk2.Text,
                                SecureIncomeAmount = Decimal.Parse(txtIncomeSecureAmountRisk2.Text),
                                SecureIncomePeriod = txtIncomeSecurePeriodRisk2.Text,
                                SecureIncomeCoverageType = txtIncomeSecureCoverageTypeRisk2.Text,
                                SecureIncomePremium = Decimal.Parse(txtIncomeSecurePremiumRisk2.Text),
                                SecureIncomeComments = txtIncomeSecureCommentsRisk2.Text,
                                DisabilityByAccidentAmount = Decimal.Parse(txtDesabilityByAccidentAmountRisk2.Text),
                                DisabilityByAccidentPeriod = txtDesabilityByAccidentPeriodRisk2.Text,
                                DisabilityByAccidentCoverageType = txtDesabilityByAccidentCoverageTypeRisk2.Text,
                                DisabilityByAccidentPremium = Decimal.Parse(txtDesabilityByAccidentPremiumRisk2.Text),
                                DisabilityByAccidentComments = txtDesabilityByAccidentCommentsRisk2.Text,
                                DeathByAccidentAmount = Decimal.Parse(txtDeathByAccidentAmountRisk2.Text),
                                DeathByAccidentPeriod = txtDeathByAccidentPeriodRisk2.Text,
                                DeathByAccidentCoverageType = txtDeathByAccidentCoverageTypeRisk2.Text,
                                DeathByAccidentPremium = Decimal.Parse(txtDeathByAccidentPremiumRisk2.Text),
                                DeathByAccidentComments = txtDeathByAccidentCommentsRisk2.Text,
                                CriticalIllnessAmount = Decimal.Parse(txtCriticalIllnesesAmountRisk2.Text),
                                CriticalIllnessPeriod = txtCriticalIllnesesPeriodRisk2.Text,
                                CriticalIllnessCoverageType = txtCriticalIllnesesCoverageTypeRisk2.Text,
                                CriticalIllnessPremium = Decimal.Parse(txtCriticalIllnesesPremiumRisk2.Text),
                                CriticalIllnessComments = txtCriticalIllnesesCommentsRisk2.Text,
                                Other1Name = txtOther1_2.Text,
                                Other1Amount = Decimal.Parse(txtOther1AmountRisk2.Text),
                                Other1Period = txtOther1PeriodRisk2.Text,
                                Other1CoverageType = txtOther1CoverageTypeRisk2.Text,
                                Other1Premium = Decimal.Parse(txtOther1PremiumRisk2.Text),
                                Other1Comments = txtOther1CommentsRisk2.Text,
                                Other2Name = txtOther2_2.Text,
                                Other2Amount = Decimal.Parse(txtOther2AmountRisk2.Text),
                                Other2Period = txtOther2PeriodRisk2.Text,
                                Other2CoverageType = txtOther2CoverageTypeRisk2.Text,
                                Other2Premium = Decimal.Parse(txtOther2PremiumRisk2.Text),
                                Other2Comments = txtOther2CommentsRisk2.Text,
                                Other3Name = txtOther3_2.Text,
                                Other3Amount = Decimal.Parse(txtOther3AmountRisk2.Text),
                                Other3Period = txtOther3PeriodRisk2.Text,
                                Other3CoverageType = txtOther3CoverageTypeRisk2.Text,
                                Other3Premium = Decimal.Parse(txtOther3PremiumRisk2.Text),
                                Other3Comments = txtOther3CommentsRisk2.Text,
                                Other4Name = txtOther4_2.Text,
                                Other4Amount = Decimal.Parse(txtOther4AmountRisk2.Text),
                                Other4Period = txtOther4PeriodRisk2.Text,
                                Other4CoverageType = txtOther4CoverageTypeRisk2.Text,
                                Other4Premium = Decimal.Parse(txtOther4PremiumRisk2.Text),
                                Other4Comments = txtOther4CommentsRisk2.Text,
                                Other5Name = txtOther5_2.Text,
                                Other5Amount = Decimal.Parse(txtOther5AmountRisk2.Text),
                                Other5Period = txtOther5PeriodRisk2.Text,
                                Other5CoverageType = txtOther5CoverageTypeRisk2.Text,
                                Other5Premium = Decimal.Parse(txtOther5PremiumRisk2.Text),
                                Other5Comments = txtOther5CommentsRisk2.Text
                            };
                            risks.Add(newAdditionalRisk);
                            //if (tbItemInsurencedRisk3.IsVisible)
                            if (additionalIds.Count > 1)
                            {
                                FillEmptyTextBoxes(new TextBox[] { txtDeathAmountRisk3, txtWorkImpossibilityAmountRisk3, txtIncomeSecureAmountRisk3, txtDesabilityByAccidentAmountRisk3, txtDeathByAccidentAmountRisk3, txtCriticalIllnesesAmountRisk3, txtOther1AmountRisk3, txtOther2AmountRisk3, txtOther3AmountRisk3, txtOther4AmountRisk3, txtOther5AmountRisk3, txtDeathPremiumRisk3, txtWorkImpossibilityPremiumRisk3, txtIncomeSecurePremiumRisk3, txtDesabilityByAccidentPremiumRisk3, txtDeathByAccidentPremiumRisk3, txtCriticalIllnesesPremiumRisk3, txtOther1PremiumRisk3, txtOther2PremiumRisk3, txtOther3PremiumRisk3, txtOther4PremiumRisk3, txtOther5PremiumRisk3 });
                                Risk newAdditionalRisk2 = new Risk()
                                {
                                    LifeAdditionalInsuredID = additionalIds[1],
                                    IsDeathType1 = rbDeath1Risk3.IsChecked,
                                    IsDeathType5 = rbDeath5Risk3.IsChecked,
                                    IsDeathTypeFixed = rbDeathFixedRisk3.IsChecked,
                                    DeathAmount = Decimal.Parse(txtDeathAmountRisk3.Text),
                                    DeathPeriod = txtDeathPeriodRisk3.Text,
                                    DeathCoverageType = txtDeathCoverageTypeRisk3.Text,
                                    DeathPremium = Decimal.Parse(txtDeathPremiumRisk3.Text),
                                    DeathComments = txtDeathCommentsRisk3.Text,
                                    IsProfessionalWorkImpossibility = rbProfessionalWorkImposibilitationRisk3.IsChecked,
                                    WorkImpossibilityAmount = Decimal.Parse(txtWorkImpossibilityAmountRisk3.Text),
                                    WorkImpossibilityPeriod = txtWorkImpossibilityPeriodRisk3.Text,
                                    WorkImpossibilityCoverageType = txtWorkImpossibilityCoverageTypeRisk3.Text,
                                    WorkImpossibilityPremium = Decimal.Parse(txtWorkImpossibilityPremiumRisk3.Text),
                                    WorkImpossibilityComments = txtWorkImpossibilityCommentsRisk3.Text,
                                    SecureIncomeAmount = Decimal.Parse(txtIncomeSecureAmountRisk3.Text),
                                    SecureIncomePeriod = txtIncomeSecurePeriodRisk3.Text,
                                    SecureIncomeCoverageType = txtIncomeSecureCoverageTypeRisk3.Text,
                                    SecureIncomePremium = Decimal.Parse(txtIncomeSecurePremiumRisk3.Text),
                                    SecureIncomeComments = txtIncomeSecureCommentsRisk3.Text,
                                    DisabilityByAccidentAmount = Decimal.Parse(txtDesabilityByAccidentAmountRisk3.Text),
                                    DisabilityByAccidentPeriod = txtDesabilityByAccidentPeriodRisk3.Text,
                                    DisabilityByAccidentCoverageType = txtDesabilityByAccidentCoverageTypeRisk3.Text,
                                    DisabilityByAccidentPremium = Decimal.Parse(txtDesabilityByAccidentPremiumRisk3.Text),
                                    DisabilityByAccidentComments = txtDesabilityByAccidentCommentsRisk3.Text,
                                    DeathByAccidentAmount = Decimal.Parse(txtDeathByAccidentAmountRisk3.Text),
                                    DeathByAccidentPeriod = txtDeathByAccidentPeriodRisk3.Text,
                                    DeathByAccidentCoverageType = txtDeathByAccidentCoverageTypeRisk3.Text,
                                    DeathByAccidentPremium = Decimal.Parse(txtDeathByAccidentPremiumRisk3.Text),
                                    DeathByAccidentComments = txtDeathByAccidentCommentsRisk3.Text,
                                    CriticalIllnessAmount = Decimal.Parse(txtCriticalIllnesesAmountRisk3.Text),
                                    CriticalIllnessPeriod = txtCriticalIllnesesPeriodRisk3.Text,
                                    CriticalIllnessCoverageType = txtCriticalIllnesesCoverageTypeRisk3.Text,
                                    CriticalIllnessPremium = Decimal.Parse(txtCriticalIllnesesPremiumRisk3.Text),
                                    CriticalIllnessComments = txtCriticalIllnesesCommentsRisk3.Text,
                                    Other1Name = txtOther1_3.Text,
                                    Other1Amount = Decimal.Parse(txtOther1AmountRisk3.Text),
                                    Other1Period = txtOther1PeriodRisk3.Text,
                                    Other1CoverageType = txtOther1CoverageTypeRisk3.Text,
                                    Other1Premium = Decimal.Parse(txtOther1PremiumRisk3.Text),
                                    Other1Comments = txtOther1CommentsRisk3.Text,
                                    Other2Name = txtOther2_3.Text,
                                    Other2Amount = Decimal.Parse(txtOther2AmountRisk3.Text),
                                    Other2Period = txtOther2PeriodRisk3.Text,
                                    Other2CoverageType = txtOther2CoverageTypeRisk3.Text,
                                    Other2Premium = Decimal.Parse(txtOther2PremiumRisk3.Text),
                                    Other2Comments = txtOther2CommentsRisk3.Text,
                                    Other3Name = txtOther3_3.Text,
                                    Other3Amount = Decimal.Parse(txtOther3AmountRisk3.Text),
                                    Other3Period = txtOther3PeriodRisk3.Text,
                                    Other3CoverageType = txtOther3CoverageTypeRisk3.Text,
                                    Other3Premium = Decimal.Parse(txtOther3PremiumRisk3.Text),
                                    Other3Comments = txtOther3CommentsRisk3.Text,
                                    Other4Name = txtOther4_3.Text,
                                    Other4Amount = Decimal.Parse(txtOther4AmountRisk3.Text),
                                    Other4Period = txtOther4PeriodRisk3.Text,
                                    Other4CoverageType = txtOther4CoverageTypeRisk3.Text,
                                    Other4Premium = Decimal.Parse(txtOther4PremiumRisk3.Text),
                                    Other4Comments = txtOther4CommentsRisk3.Text,
                                    Other5Name = txtOther5_3.Text,
                                    Other5Amount = Decimal.Parse(txtOther5AmountRisk3.Text),
                                    Other5Period = txtOther5PeriodRisk3.Text,
                                    Other5CoverageType = txtOther5CoverageTypeRisk3.Text,
                                    Other5Premium = Decimal.Parse(txtOther5PremiumRisk3.Text),
                                    Other5Comments = txtOther5CommentsRisk3.Text
                                };
                                risks.Add(newAdditionalRisk2);
                                //if (tbItemInsurencedRisk4.IsVisible)
                                if (additionalIds.Count > 2)
                                {
                                    FillEmptyTextBoxes(new TextBox[] { txtDeathAmountRisk4, txtWorkImpossibilityAmountRisk4, txtIncomeSecureAmountRisk4, txtDesabilityByAccidentAmountRisk4, txtDeathByAccidentAmountRisk4, txtCriticalIllnesesAmountRisk4, txtOther1AmountRisk4, txtOther2AmountRisk4, txtOther3AmountRisk4, txtOther4AmountRisk4, txtOther5AmountRisk4, txtDeathPremiumRisk4, txtWorkImpossibilityPremiumRisk4, txtIncomeSecurePremiumRisk4, txtDesabilityByAccidentPremiumRisk4, txtDeathByAccidentPremiumRisk4, txtCriticalIllnesesPremiumRisk4, txtOther1PremiumRisk4, txtOther2PremiumRisk4, txtOther3PremiumRisk4, txtOther4PremiumRisk4, txtOther5PremiumRisk4 });
                                    Risk newAdditionalRisk3 = new Risk()
                                    {
                                        LifeAdditionalInsuredID = additionalIds[2],
                                        IsDeathType1 = rbDeath1Risk4.IsChecked,
                                        IsDeathType5 = rbDeath5Risk4.IsChecked,
                                        IsDeathTypeFixed = rbDeathFixedRisk4.IsChecked,
                                        DeathAmount = Decimal.Parse(txtDeathAmountRisk4.Text),
                                        DeathPeriod = txtDeathPeriodRisk4.Text,
                                        DeathCoverageType = txtDeathCoverageTypeRisk4.Text,
                                        DeathPremium = Decimal.Parse(txtDeathPremiumRisk4.Text),
                                        DeathComments = txtDeathCommentsRisk4.Text,
                                        IsProfessionalWorkImpossibility = rbProfessionalWorkImposibilitationRisk4.IsChecked,
                                        WorkImpossibilityAmount = Decimal.Parse(txtWorkImpossibilityAmountRisk4.Text),
                                        WorkImpossibilityPeriod = txtWorkImpossibilityPeriodRisk4.Text,
                                        WorkImpossibilityCoverageType = txtWorkImpossibilityCoverageTypeRisk4.Text,
                                        WorkImpossibilityPremium = Decimal.Parse(txtWorkImpossibilityPremiumRisk4.Text),
                                        WorkImpossibilityComments = txtWorkImpossibilityCommentsRisk4.Text,
                                        SecureIncomeAmount = Decimal.Parse(txtIncomeSecureAmountRisk4.Text),
                                        SecureIncomePeriod = txtIncomeSecurePeriodRisk4.Text,
                                        SecureIncomeCoverageType = txtIncomeSecureCoverageTypeRisk4.Text,
                                        SecureIncomePremium = Decimal.Parse(txtIncomeSecurePremiumRisk4.Text),
                                        SecureIncomeComments = txtIncomeSecureCommentsRisk4.Text,
                                        DisabilityByAccidentAmount = Decimal.Parse(txtDesabilityByAccidentAmountRisk4.Text),
                                        DisabilityByAccidentPeriod = txtDesabilityByAccidentPeriodRisk4.Text,
                                        DisabilityByAccidentCoverageType = txtDesabilityByAccidentCoverageTypeRisk4.Text,
                                        DisabilityByAccidentPremium = Decimal.Parse(txtDesabilityByAccidentPremiumRisk4.Text),
                                        DisabilityByAccidentComments = txtDesabilityByAccidentCommentsRisk4.Text,
                                        DeathByAccidentAmount = Decimal.Parse(txtDeathByAccidentAmountRisk4.Text),
                                        DeathByAccidentPeriod = txtDeathByAccidentPeriodRisk4.Text,
                                        DeathByAccidentCoverageType = txtDeathByAccidentCoverageTypeRisk4.Text,
                                        DeathByAccidentPremium = Decimal.Parse(txtDeathByAccidentPremiumRisk4.Text),
                                        DeathByAccidentComments = txtDeathByAccidentCommentsRisk4.Text,
                                        CriticalIllnessAmount = Decimal.Parse(txtCriticalIllnesesAmountRisk4.Text),
                                        CriticalIllnessPeriod = txtCriticalIllnesesPeriodRisk4.Text,
                                        CriticalIllnessCoverageType = txtCriticalIllnesesCoverageTypeRisk4.Text,
                                        CriticalIllnessPremium = Decimal.Parse(txtCriticalIllnesesPremiumRisk4.Text),
                                        CriticalIllnessComments = txtCriticalIllnesesCommentsRisk4.Text,
                                        Other1Name = txtOther1_4.Text,
                                        Other1Amount = Decimal.Parse(txtOther1AmountRisk4.Text),
                                        Other1Period = txtOther1PeriodRisk4.Text,
                                        Other1CoverageType = txtOther1CoverageTypeRisk4.Text,
                                        Other1Premium = Decimal.Parse(txtOther1PremiumRisk4.Text),
                                        Other1Comments = txtOther1CommentsRisk4.Text,
                                        Other2Name = txtOther2_4.Text,
                                        Other2Amount = Decimal.Parse(txtOther2AmountRisk4.Text),
                                        Other2Period = txtOther2PeriodRisk4.Text,
                                        Other2CoverageType = txtOther2CoverageTypeRisk4.Text,
                                        Other2Premium = Decimal.Parse(txtOther2PremiumRisk4.Text),
                                        Other2Comments = txtOther2CommentsRisk4.Text,
                                        Other3Name = txtOther3_4.Text,
                                        Other3Amount = Decimal.Parse(txtOther3AmountRisk4.Text),
                                        Other3Period = txtOther3PeriodRisk4.Text,
                                        Other3CoverageType = txtOther3CoverageTypeRisk4.Text,
                                        Other3Premium = Decimal.Parse(txtOther3PremiumRisk4.Text),
                                        Other3Comments = txtOther3CommentsRisk4.Text,
                                        Other4Name = txtOther4_4.Text,
                                        Other4Amount = Decimal.Parse(txtOther4AmountRisk4.Text),
                                        Other4Period = txtOther4PeriodRisk4.Text,
                                        Other4CoverageType = txtOther4CoverageTypeRisk4.Text,
                                        Other4Premium = Decimal.Parse(txtOther4PremiumRisk4.Text),
                                        Other4Comments = txtOther4CommentsRisk4.Text,
                                        Other5Name = txtOther5_4.Text,
                                        Other5Amount = Decimal.Parse(txtOther5AmountRisk4.Text),
                                        Other5Period = txtOther5PeriodRisk4.Text,
                                        Other5CoverageType = txtOther5CoverageTypeRisk4.Text,
                                        Other5Premium = Decimal.Parse(txtOther5PremiumRisk4.Text),
                                        Other5Comments = txtOther5CommentsRisk4.Text
                                    };
                                    risks.Add(newAdditionalRisk3);
                                    //if (tbItemInsurencedRisk5.IsVisible)
                                    if (additionalIds.Count > 3)
                                    {
                                        FillEmptyTextBoxes(new TextBox[] { txtDeathAmountRisk5, txtWorkImpossibilityAmountRisk5, txtIncomeSecureAmountRisk5, txtDesabilityByAccidentAmountRisk5, txtDeathByAccidentAmountRisk5, txtCriticalIllnesesAmountRisk5, txtOther1AmountRisk5, txtOther2AmountRisk5, txtOther3AmountRisk5, txtOther4AmountRisk5, txtOther5AmountRisk5, txtDeathPremiumRisk5, txtWorkImpossibilityPremiumRisk5, txtIncomeSecurePremiumRisk5, txtDesabilityByAccidentPremiumRisk5, txtDeathByAccidentPremiumRisk5, txtCriticalIllnesesPremiumRisk5, txtOther1PremiumRisk5, txtOther2PremiumRisk5, txtOther3PremiumRisk5, txtOther4PremiumRisk5, txtOther5PremiumRisk5 });
                                        Risk newAdditionalRisk4 = new Risk()
                                        {
                                            LifeAdditionalInsuredID = additionalIds[3],
                                            IsDeathType1 = rbDeath1Risk5.IsChecked,
                                            IsDeathType5 = rbDeath5Risk5.IsChecked,
                                            IsDeathTypeFixed = rbDeathFixedRisk5.IsChecked,
                                            DeathAmount = Decimal.Parse(txtDeathAmountRisk5.Text),
                                            DeathPeriod = txtDeathPeriodRisk5.Text,
                                            DeathCoverageType = txtDeathCoverageTypeRisk5.Text,
                                            DeathPremium = Decimal.Parse(txtDeathPremiumRisk5.Text),
                                            DeathComments = txtDeathCommentsRisk5.Text,
                                            IsProfessionalWorkImpossibility = rbProfessionalWorkImposibilitationRisk5.IsChecked,
                                            WorkImpossibilityAmount = Decimal.Parse(txtWorkImpossibilityAmountRisk5.Text),
                                            WorkImpossibilityPeriod = txtWorkImpossibilityPeriodRisk5.Text,
                                            WorkImpossibilityCoverageType = txtWorkImpossibilityCoverageTypeRisk5.Text,
                                            WorkImpossibilityPremium = Decimal.Parse(txtWorkImpossibilityPremiumRisk5.Text),
                                            WorkImpossibilityComments = txtWorkImpossibilityCommentsRisk5.Text,
                                            SecureIncomeAmount = Decimal.Parse(txtIncomeSecureAmountRisk5.Text),
                                            SecureIncomePeriod = txtIncomeSecurePeriodRisk5.Text,
                                            SecureIncomeCoverageType = txtIncomeSecureCoverageTypeRisk5.Text,
                                            SecureIncomePremium = Decimal.Parse(txtIncomeSecurePremiumRisk5.Text),
                                            SecureIncomeComments = txtIncomeSecureCommentsRisk5.Text,
                                            DisabilityByAccidentAmount = Decimal.Parse(txtDesabilityByAccidentAmountRisk5.Text),
                                            DisabilityByAccidentPeriod = txtDesabilityByAccidentPeriodRisk5.Text,
                                            DisabilityByAccidentCoverageType = txtDesabilityByAccidentCoverageTypeRisk5.Text,
                                            DisabilityByAccidentPremium = Decimal.Parse(txtDesabilityByAccidentPremiumRisk5.Text),
                                            DisabilityByAccidentComments = txtDesabilityByAccidentCommentsRisk5.Text,
                                            DeathByAccidentAmount = Decimal.Parse(txtDeathByAccidentAmountRisk5.Text),
                                            DeathByAccidentPeriod = txtDeathByAccidentPeriodRisk5.Text,
                                            DeathByAccidentCoverageType = txtDeathByAccidentCoverageTypeRisk5.Text,
                                            DeathByAccidentPremium = Decimal.Parse(txtDeathByAccidentPremiumRisk5.Text),
                                            DeathByAccidentComments = txtDeathByAccidentCommentsRisk5.Text,
                                            CriticalIllnessAmount = Decimal.Parse(txtCriticalIllnesesAmountRisk5.Text),
                                            CriticalIllnessPeriod = txtCriticalIllnesesPeriodRisk5.Text,
                                            CriticalIllnessCoverageType = txtCriticalIllnesesCoverageTypeRisk5.Text,
                                            CriticalIllnessPremium = Decimal.Parse(txtCriticalIllnesesPremiumRisk5.Text),
                                            CriticalIllnessComments = txtCriticalIllnesesCommentsRisk5.Text,
                                            Other1Name = txtOther1_5.Text,
                                            Other1Amount = Decimal.Parse(txtOther1AmountRisk5.Text),
                                            Other1Period = txtOther1PeriodRisk5.Text,
                                            Other1CoverageType = txtOther1CoverageTypeRisk5.Text,
                                            Other1Premium = Decimal.Parse(txtOther1PremiumRisk5.Text),
                                            Other1Comments = txtOther1CommentsRisk5.Text,
                                            Other2Name = txtOther2_5.Text,
                                            Other2Amount = Decimal.Parse(txtOther2AmountRisk5.Text),
                                            Other2Period = txtOther2PeriodRisk5.Text,
                                            Other2CoverageType = txtOther2CoverageTypeRisk5.Text,
                                            Other2Premium = Decimal.Parse(txtOther2PremiumRisk5.Text),
                                            Other2Comments = txtOther2CommentsRisk5.Text,
                                            Other3Name = txtOther3_5.Text,
                                            Other3Amount = Decimal.Parse(txtOther3AmountRisk5.Text),
                                            Other3Period = txtOther3PeriodRisk5.Text,
                                            Other3CoverageType = txtOther3CoverageTypeRisk5.Text,
                                            Other3Premium = Decimal.Parse(txtOther3PremiumRisk5.Text),
                                            Other3Comments = txtOther3CommentsRisk5.Text,
                                            Other4Name = txtOther4_5.Text,
                                            Other4Amount = Decimal.Parse(txtOther4AmountRisk5.Text),
                                            Other4Period = txtOther4PeriodRisk5.Text,
                                            Other4CoverageType = txtOther4CoverageTypeRisk5.Text,
                                            Other4Premium = Decimal.Parse(txtOther4PremiumRisk5.Text),
                                            Other4Comments = txtOther4CommentsRisk5.Text,
                                            Other5Name = txtOther5_5.Text,
                                            Other5Amount = Decimal.Parse(txtOther5AmountRisk5.Text),
                                            Other5Period = txtOther5PeriodRisk5.Text,
                                            Other5CoverageType = txtOther5CoverageTypeRisk5.Text,
                                            Other5Premium = Decimal.Parse(txtOther5PremiumRisk5.Text),
                                            Other5Comments = txtOther5CommentsRisk5.Text
                                        };
                                        risks.Add(newAdditionalRisk4);
                                    }
                                }
                            }
                        }
                        lifePolicyLogic.InsertRisks(policyId, risks);
                    }
                    else
                    {

                        risks[0].IsDeathType1 = rbDeath1Risk.IsChecked;
                        risks[0].IsDeathType5 = rbDeath5Risk.IsChecked;
                        risks[0].IsDeathTypeFixed = rbDeathFixedRisk.IsChecked;
                        risks[0].DeathAmount = Decimal.Parse(txtDeathAmountRisk.Text);
                        risks[0].DeathPeriod = txtDeathPeriodRisk.Text;
                        risks[0].DeathCoverageType = txtDeathCoverageTypeRisk.Text;
                        risks[0].DeathPremium = Decimal.Parse(txtDeathPremiumRisk.Text);
                        risks[0].DeathComments = txtDeathCommentsRisk.Text;
                        risks[0].IsProfessionalWorkImpossibility = rbProfessionalWorkImposibilitationRisk.IsChecked;
                        risks[0].WorkImpossibilityAmount = Decimal.Parse(txtWorkImpossibilityAmountRisk.Text);
                        risks[0].WorkImpossibilityPeriod = txtWorkImpossibilityPeriodRisk.Text;
                        risks[0].WorkImpossibilityCoverageType = txtWorkImpossibilityCoverageTypeRisk.Text;
                        risks[0].WorkImpossibilityPremium = Decimal.Parse(txtWorkImpossibilityPremiumRisk.Text);
                        risks[0].WorkImpossibilityComments = txtWorkImpossibilityCommentsRisk.Text;
                        risks[0].SecureIncomeAmount = Decimal.Parse(txtIncomeSecureAmountRisk.Text);
                        risks[0].SecureIncomePeriod = txtIncomeSecurePeriodRisk.Text;
                        risks[0].SecureIncomeCoverageType = txtIncomeSecureCoverageTypeRisk.Text;
                        risks[0].SecureIncomePremium = Decimal.Parse(txtIncomeSecurePremiumRisk.Text);
                        risks[0].SecureIncomeComments = txtIncomeSecureCommentsRisk.Text;
                        risks[0].DisabilityByAccidentAmount = Decimal.Parse(txtDesabilityByAccidentAmountRisk.Text);
                        risks[0].DisabilityByAccidentPeriod = txtDesabilityByAccidentPeriodRisk.Text;
                        risks[0].DisabilityByAccidentCoverageType = txtDesabilityByAccidentCoverageTypeRisk.Text;
                        risks[0].DisabilityByAccidentPremium = Decimal.Parse(txtDesabilityByAccidentPremiumRisk.Text);
                        risks[0].DisabilityByAccidentComments = txtDesabilityByAccidentCommentsRisk.Text;
                        risks[0].DeathByAccidentAmount = Decimal.Parse(txtDeathByAccidentAmountRisk.Text);
                        risks[0].DeathByAccidentPeriod = txtDeathByAccidentPeriodRisk.Text;
                        risks[0].DeathByAccidentCoverageType = txtDeathByAccidentCoverageTypeRisk.Text;
                        risks[0].DeathByAccidentPremium = Decimal.Parse(txtDeathByAccidentPremiumRisk.Text);
                        risks[0].DeathByAccidentComments = txtDeathByAccidentCommentsRisk.Text;
                        risks[0].CriticalIllnessAmount = Decimal.Parse(txtCriticalIllnesesAmountRisk.Text);
                        risks[0].CriticalIllnessPeriod = txtCriticalIllnesesPeriodRisk.Text;
                        risks[0].CriticalIllnessCoverageType = txtCriticalIllnesesCoverageTypeRisk.Text;
                        risks[0].CriticalIllnessPremium = Decimal.Parse(txtCriticalIllnesesPremiumRisk.Text);
                        risks[0].CriticalIllnessComments = txtCriticalIllnesesCommentsRisk.Text;
                        risks[0].Other1Name = txtOther1.Text;
                        risks[0].Other1Amount = Decimal.Parse(txtOther1AmountRisk.Text);
                        risks[0].Other1Period = txtOther1PeriodRisk.Text;
                        risks[0].Other1CoverageType = txtOther1CoverageTypeRisk.Text;
                        risks[0].Other1Premium = Decimal.Parse(txtOther1PremiumRisk.Text);
                        risks[0].Other1Comments = txtOther1CommentsRisk.Text;
                        risks[0].Other2Name = txtOther2.Text;
                        risks[0].Other2Amount = Decimal.Parse(txtOther2AmountRisk.Text);
                        risks[0].Other2Period = txtOther2PeriodRisk.Text;
                        risks[0].Other2CoverageType = txtOther2CoverageTypeRisk.Text;
                        risks[0].Other2Premium = Decimal.Parse(txtOther2PremiumRisk.Text);
                        risks[0].Other2Comments = txtOther2CommentsRisk.Text;
                        risks[0].Other3Name = txtOther3.Text;
                        risks[0].Other3Amount = Decimal.Parse(txtOther3AmountRisk.Text);
                        risks[0].Other3Period = txtOther3PeriodRisk.Text;
                        risks[0].Other3CoverageType = txtOther3CoverageTypeRisk.Text;
                        risks[0].Other3Premium = Decimal.Parse(txtOther3PremiumRisk.Text);
                        risks[0].Other3Comments = txtOther3CommentsRisk.Text;
                        risks[0].Other4Name = txtOther4.Text;
                        risks[0].Other4Amount = Decimal.Parse(txtOther4AmountRisk.Text);
                        risks[0].Other4Period = txtOther4PeriodRisk.Text;
                        risks[0].Other4CoverageType = txtOther4CoverageTypeRisk.Text;
                        risks[0].Other4Premium = Decimal.Parse(txtOther4PremiumRisk.Text);
                        risks[0].Other4Comments = txtOther4CommentsRisk.Text;
                        risks[0].Other5Name = txtOther5.Text;
                        risks[0].Other5Amount = Decimal.Parse(txtOther5AmountRisk.Text);
                        risks[0].Other5Period = txtOther5PeriodRisk.Text;
                        risks[0].Other5CoverageType = txtOther5CoverageTypeRisk.Text;
                        risks[0].Other5Premium = Decimal.Parse(txtOther5PremiumRisk.Text);
                        risks[0].Other5Comments = txtOther5CommentsRisk.Text;


                        //if (tbItemInsurencedRisk2.IsVisible)
                        SaveUnsavedAdditionalInsured();

                        if (additionalsInsureds.Count > 0)
                        {
                            lifePolicyLogic.InsertAdditionalInsureds(lifePolicy.LifePolicyID, additionalsInsureds, isAddition);
                            FillEmptyTextBoxes(new TextBox[] { txtDeathAmountRisk2, txtWorkImpossibilityAmountRisk2, txtIncomeSecureAmountRisk2, txtDesabilityByAccidentAmountRisk2, txtDeathByAccidentAmountRisk2, txtCriticalIllnesesAmountRisk2, txtOther1AmountRisk2, txtOther2AmountRisk2, txtOther3AmountRisk2, txtOther4AmountRisk2, txtOther5AmountRisk2, txtDeathPremiumRisk2, txtWorkImpossibilityPremiumRisk2, txtIncomeSecurePremiumRisk2, txtDesabilityByAccidentPremiumRisk2, txtDeathByAccidentPremiumRisk2, txtCriticalIllnesesPremiumRisk2, txtOther1PremiumRisk2, txtOther2PremiumRisk2, txtOther3PremiumRisk2, txtOther4PremiumRisk2, txtOther5PremiumRisk2 });

                            risks[1].IsDeathType1 = rbDeath1Risk2.IsChecked;
                            risks[1].IsDeathType5 = rbDeath5Risk2.IsChecked;
                            risks[1].IsDeathTypeFixed = rbDeathFixedRisk2.IsChecked;
                            risks[1].DeathAmount = Decimal.Parse(txtDeathAmountRisk2.Text);
                            risks[1].DeathPeriod = txtDeathPeriodRisk2.Text;
                            risks[1].DeathCoverageType = txtDeathCoverageTypeRisk2.Text;
                            risks[1].DeathPremium = Decimal.Parse(txtDeathPremiumRisk2.Text);
                            risks[1].DeathComments = txtDeathCommentsRisk2.Text;
                            risks[1].IsProfessionalWorkImpossibility = rbProfessionalWorkImposibilitationRisk2.IsChecked;
                            risks[1].WorkImpossibilityAmount = Decimal.Parse(txtWorkImpossibilityAmountRisk2.Text);
                            risks[1].WorkImpossibilityPeriod = txtWorkImpossibilityPeriodRisk2.Text;
                            risks[1].WorkImpossibilityCoverageType = txtWorkImpossibilityCoverageTypeRisk2.Text;
                            risks[1].WorkImpossibilityPremium = Decimal.Parse(txtWorkImpossibilityPremiumRisk2.Text);
                            risks[1].WorkImpossibilityComments = txtWorkImpossibilityCommentsRisk2.Text;
                            risks[1].SecureIncomeAmount = Decimal.Parse(txtIncomeSecureAmountRisk2.Text);
                            risks[1].SecureIncomePeriod = txtIncomeSecurePeriodRisk2.Text;
                            risks[1].SecureIncomeCoverageType = txtIncomeSecureCoverageTypeRisk2.Text;
                            risks[1].SecureIncomePremium = Decimal.Parse(txtIncomeSecurePremiumRisk2.Text);
                            risks[1].SecureIncomeComments = txtIncomeSecureCommentsRisk2.Text;
                            risks[1].DisabilityByAccidentAmount = Decimal.Parse(txtDesabilityByAccidentAmountRisk2.Text);
                            risks[1].DisabilityByAccidentPeriod = txtDesabilityByAccidentPeriodRisk2.Text;
                            risks[1].DisabilityByAccidentCoverageType = txtDesabilityByAccidentCoverageTypeRisk2.Text;
                            risks[1].DisabilityByAccidentPremium = Decimal.Parse(txtDesabilityByAccidentPremiumRisk2.Text);
                            risks[1].DisabilityByAccidentComments = txtDesabilityByAccidentCommentsRisk2.Text;
                            risks[1].DeathByAccidentAmount = Decimal.Parse(txtDeathByAccidentAmountRisk2.Text);
                            risks[1].DeathByAccidentPeriod = txtDeathByAccidentPeriodRisk2.Text;
                            risks[1].DeathByAccidentCoverageType = txtDeathByAccidentCoverageTypeRisk2.Text;
                            risks[1].DeathByAccidentPremium = Decimal.Parse(txtDeathByAccidentPremiumRisk2.Text);
                            risks[1].DeathByAccidentComments = txtDeathByAccidentCommentsRisk2.Text;
                            risks[1].CriticalIllnessAmount = Decimal.Parse(txtCriticalIllnesesAmountRisk2.Text);
                            risks[1].CriticalIllnessPeriod = txtCriticalIllnesesPeriodRisk2.Text;
                            risks[1].CriticalIllnessCoverageType = txtCriticalIllnesesCoverageTypeRisk2.Text;
                            risks[1].CriticalIllnessPremium = Decimal.Parse(txtCriticalIllnesesPremiumRisk2.Text);
                            risks[1].CriticalIllnessComments = txtCriticalIllnesesCommentsRisk2.Text;
                            risks[1].Other1Name = txtOther1_2.Text;
                            risks[1].Other1Amount = Decimal.Parse(txtOther1AmountRisk2.Text);
                            risks[1].Other1Period = txtOther1PeriodRisk2.Text;
                            risks[1].Other1CoverageType = txtOther1CoverageTypeRisk2.Text;
                            risks[1].Other1Premium = Decimal.Parse(txtOther1PremiumRisk2.Text);
                            risks[1].Other1Comments = txtOther1CommentsRisk2.Text;
                            risks[1].Other2Name = txtOther2_2.Text;
                            risks[1].Other2Amount = Decimal.Parse(txtOther2AmountRisk2.Text);
                            risks[1].Other2Period = txtOther2PeriodRisk2.Text;
                            risks[1].Other2CoverageType = txtOther2CoverageTypeRisk2.Text;
                            risks[1].Other2Premium = Decimal.Parse(txtOther2PremiumRisk2.Text);
                            risks[1].Other2Comments = txtOther2CommentsRisk2.Text;
                            risks[1].Other3Name = txtOther3_2.Text;
                            risks[1].Other3Amount = Decimal.Parse(txtOther3AmountRisk2.Text);
                            risks[1].Other3Period = txtOther3PeriodRisk2.Text;
                            risks[1].Other3CoverageType = txtOther3CoverageTypeRisk2.Text;
                            risks[1].Other3Premium = Decimal.Parse(txtOther3PremiumRisk2.Text);
                            risks[1].Other3Comments = txtOther3CommentsRisk2.Text;
                            risks[1].Other4Name = txtOther4_2.Text;
                            risks[1].Other4Amount = Decimal.Parse(txtOther4AmountRisk2.Text);
                            risks[1].Other4Period = txtOther4PeriodRisk2.Text;
                            risks[1].Other4CoverageType = txtOther4CoverageTypeRisk2.Text;
                            risks[1].Other4Premium = Decimal.Parse(txtOther4PremiumRisk2.Text);
                            risks[1].Other4Comments = txtOther4CommentsRisk2.Text;
                            risks[1].Other5Name = txtOther5_2.Text;
                            risks[1].Other5Amount = Decimal.Parse(txtOther5AmountRisk2.Text);
                            risks[1].Other5Period = txtOther5PeriodRisk2.Text;
                            risks[1].Other5CoverageType = txtOther5CoverageTypeRisk2.Text;
                            risks[1].Other5Premium = Decimal.Parse(txtOther5PremiumRisk2.Text);
                            risks[1].Other5Comments = txtOther5CommentsRisk2.Text;
                            //if (tbItemInsurencedRisk3.IsVisible)
                            if (additionalsInsureds.Count > 1)
                            {
                                FillEmptyTextBoxes(new TextBox[] { txtDeathAmountRisk3, txtWorkImpossibilityAmountRisk3, txtIncomeSecureAmountRisk3, txtDesabilityByAccidentAmountRisk3, txtDeathByAccidentAmountRisk3, txtCriticalIllnesesAmountRisk3, txtOther1AmountRisk3, txtOther2AmountRisk3, txtOther3AmountRisk3, txtOther4AmountRisk3, txtOther5AmountRisk3, txtDeathPremiumRisk3, txtWorkImpossibilityPremiumRisk3, txtIncomeSecurePremiumRisk3, txtDesabilityByAccidentPremiumRisk3, txtDeathByAccidentPremiumRisk3, txtCriticalIllnesesPremiumRisk3, txtOther1PremiumRisk3, txtOther2PremiumRisk3, txtOther3PremiumRisk3, txtOther4PremiumRisk3, txtOther5PremiumRisk3 });

                                risks[2].IsDeathType1 = rbDeath1Risk3.IsChecked;
                                risks[2].IsDeathType5 = rbDeath5Risk3.IsChecked;
                                risks[2].IsDeathTypeFixed = rbDeathFixedRisk3.IsChecked;
                                risks[2].DeathAmount = Decimal.Parse(txtDeathAmountRisk3.Text);
                                risks[2].DeathPeriod = txtDeathPeriodRisk3.Text;
                                risks[2].DeathCoverageType = txtDeathCoverageTypeRisk3.Text;
                                risks[2].DeathPremium = Decimal.Parse(txtDeathPremiumRisk3.Text);
                                risks[2].DeathComments = txtDeathCommentsRisk3.Text;
                                risks[2].IsProfessionalWorkImpossibility = rbProfessionalWorkImposibilitationRisk3.IsChecked;
                                risks[2].WorkImpossibilityAmount = Decimal.Parse(txtWorkImpossibilityAmountRisk3.Text);
                                risks[2].WorkImpossibilityPeriod = txtWorkImpossibilityPeriodRisk3.Text;
                                risks[2].WorkImpossibilityCoverageType = txtWorkImpossibilityCoverageTypeRisk3.Text;
                                risks[2].WorkImpossibilityPremium = Decimal.Parse(txtWorkImpossibilityPremiumRisk3.Text);
                                risks[2].WorkImpossibilityComments = txtWorkImpossibilityCommentsRisk3.Text;
                                risks[2].SecureIncomeAmount = Decimal.Parse(txtIncomeSecureAmountRisk3.Text);
                                risks[2].SecureIncomePeriod = txtIncomeSecurePeriodRisk3.Text;
                                risks[2].SecureIncomeCoverageType = txtIncomeSecureCoverageTypeRisk3.Text;
                                risks[2].SecureIncomePremium = Decimal.Parse(txtIncomeSecurePremiumRisk3.Text);
                                risks[2].SecureIncomeComments = txtIncomeSecureCommentsRisk3.Text;
                                risks[2].DisabilityByAccidentAmount = Decimal.Parse(txtDesabilityByAccidentAmountRisk3.Text);
                                risks[2].DisabilityByAccidentPeriod = txtDesabilityByAccidentPeriodRisk3.Text;
                                risks[2].DisabilityByAccidentCoverageType = txtDesabilityByAccidentCoverageTypeRisk3.Text;
                                risks[2].DisabilityByAccidentPremium = Decimal.Parse(txtDesabilityByAccidentPremiumRisk3.Text);
                                risks[2].DisabilityByAccidentComments = txtDesabilityByAccidentCommentsRisk3.Text;
                                risks[2].DeathByAccidentAmount = Decimal.Parse(txtDeathByAccidentAmountRisk3.Text);
                                risks[2].DeathByAccidentPeriod = txtDeathByAccidentPeriodRisk3.Text;
                                risks[2].DeathByAccidentCoverageType = txtDeathByAccidentCoverageTypeRisk3.Text;
                                risks[2].DeathByAccidentPremium = Decimal.Parse(txtDeathByAccidentPremiumRisk3.Text);
                                risks[2].DeathByAccidentComments = txtDeathByAccidentCommentsRisk3.Text;
                                risks[2].CriticalIllnessAmount = Decimal.Parse(txtCriticalIllnesesAmountRisk3.Text);
                                risks[2].CriticalIllnessPeriod = txtCriticalIllnesesPeriodRisk3.Text;
                                risks[2].CriticalIllnessCoverageType = txtCriticalIllnesesCoverageTypeRisk3.Text;
                                risks[2].CriticalIllnessPremium = Decimal.Parse(txtCriticalIllnesesPremiumRisk3.Text);
                                risks[2].CriticalIllnessComments = txtCriticalIllnesesCommentsRisk3.Text;
                                risks[2].Other1Name = txtOther1_3.Text;
                                risks[2].Other1Amount = Decimal.Parse(txtOther1AmountRisk3.Text);
                                risks[2].Other1Period = txtOther1PeriodRisk3.Text;
                                risks[2].Other1CoverageType = txtOther1CoverageTypeRisk3.Text;
                                risks[2].Other1Premium = Decimal.Parse(txtOther1PremiumRisk3.Text);
                                risks[2].Other1Comments = txtOther1CommentsRisk3.Text;
                                risks[2].Other2Name = txtOther2_3.Text;
                                risks[2].Other2Amount = Decimal.Parse(txtOther2AmountRisk3.Text);
                                risks[2].Other2Period = txtOther2PeriodRisk3.Text;
                                risks[2].Other2CoverageType = txtOther2CoverageTypeRisk3.Text;
                                risks[2].Other2Premium = Decimal.Parse(txtOther2PremiumRisk3.Text);
                                risks[2].Other2Comments = txtOther2CommentsRisk3.Text;
                                risks[2].Other3Name = txtOther3_3.Text;
                                risks[2].Other3Amount = Decimal.Parse(txtOther3AmountRisk3.Text);
                                risks[2].Other3Period = txtOther3PeriodRisk3.Text;
                                risks[2].Other3CoverageType = txtOther3CoverageTypeRisk3.Text;
                                risks[2].Other3Premium = Decimal.Parse(txtOther3PremiumRisk3.Text);
                                risks[2].Other3Comments = txtOther3CommentsRisk3.Text;
                                risks[2].Other4Name = txtOther4_3.Text;
                                risks[2].Other4Amount = Decimal.Parse(txtOther4AmountRisk3.Text);
                                risks[2].Other4Period = txtOther4PeriodRisk3.Text;
                                risks[2].Other4CoverageType = txtOther4CoverageTypeRisk3.Text;
                                risks[2].Other4Premium = Decimal.Parse(txtOther4PremiumRisk3.Text);
                                risks[2].Other4Comments = txtOther4CommentsRisk3.Text;
                                risks[2].Other5Name = txtOther5_3.Text;
                                risks[2].Other5Amount = Decimal.Parse(txtOther5AmountRisk3.Text);
                                risks[2].Other5Period = txtOther5PeriodRisk3.Text;
                                risks[2].Other5CoverageType = txtOther5CoverageTypeRisk3.Text;
                                risks[2].Other5Premium = Decimal.Parse(txtOther5PremiumRisk3.Text);
                                risks[2].Other5Comments = txtOther5CommentsRisk3.Text;
                                //if (tbItemInsurencedRisk4.IsVisible)
                                if (additionalsInsureds.Count > 2)
                                {
                                    FillEmptyTextBoxes(new TextBox[] { txtDeathAmountRisk4, txtWorkImpossibilityAmountRisk4, txtIncomeSecureAmountRisk4, txtDesabilityByAccidentAmountRisk4, txtDeathByAccidentAmountRisk4, txtCriticalIllnesesAmountRisk4, txtOther1AmountRisk4, txtOther2AmountRisk4, txtOther3AmountRisk4, txtOther4AmountRisk4, txtOther5AmountRisk4, txtDeathPremiumRisk4, txtWorkImpossibilityPremiumRisk4, txtIncomeSecurePremiumRisk4, txtDesabilityByAccidentPremiumRisk4, txtDeathByAccidentPremiumRisk4, txtCriticalIllnesesPremiumRisk4, txtOther1PremiumRisk4, txtOther2PremiumRisk4, txtOther3PremiumRisk4, txtOther4PremiumRisk4, txtOther5PremiumRisk4 });

                                    risks[3].IsDeathType1 = rbDeath1Risk4.IsChecked;
                                    risks[3].IsDeathType5 = rbDeath5Risk4.IsChecked;
                                    risks[3].IsDeathTypeFixed = rbDeathFixedRisk4.IsChecked;
                                    risks[3].DeathAmount = Decimal.Parse(txtDeathAmountRisk4.Text);
                                    risks[3].DeathPeriod = txtDeathPeriodRisk4.Text;
                                    risks[3].DeathCoverageType = txtDeathCoverageTypeRisk4.Text;
                                    risks[3].DeathPremium = Decimal.Parse(txtDeathPremiumRisk4.Text);
                                    risks[3].DeathComments = txtDeathCommentsRisk4.Text;
                                    risks[3].IsProfessionalWorkImpossibility = rbProfessionalWorkImposibilitationRisk4.IsChecked;
                                    risks[3].WorkImpossibilityAmount = Decimal.Parse(txtWorkImpossibilityAmountRisk4.Text);
                                    risks[3].WorkImpossibilityPeriod = txtWorkImpossibilityPeriodRisk4.Text;
                                    risks[3].WorkImpossibilityCoverageType = txtWorkImpossibilityCoverageTypeRisk4.Text;
                                    risks[3].WorkImpossibilityPremium = Decimal.Parse(txtWorkImpossibilityPremiumRisk4.Text);
                                    risks[3].WorkImpossibilityComments = txtWorkImpossibilityCommentsRisk4.Text;
                                    risks[3].SecureIncomeAmount = Decimal.Parse(txtIncomeSecureAmountRisk4.Text);
                                    risks[3].SecureIncomePeriod = txtIncomeSecurePeriodRisk4.Text;
                                    risks[3].SecureIncomeCoverageType = txtIncomeSecureCoverageTypeRisk4.Text;
                                    risks[3].SecureIncomePremium = Decimal.Parse(txtIncomeSecurePremiumRisk4.Text);
                                    risks[3].SecureIncomeComments = txtIncomeSecureCommentsRisk4.Text;
                                    risks[3].DisabilityByAccidentAmount = Decimal.Parse(txtDesabilityByAccidentAmountRisk4.Text);
                                    risks[3].DisabilityByAccidentPeriod = txtDesabilityByAccidentPeriodRisk4.Text;
                                    risks[3].DisabilityByAccidentCoverageType = txtDesabilityByAccidentCoverageTypeRisk4.Text;
                                    risks[3].DisabilityByAccidentPremium = Decimal.Parse(txtDesabilityByAccidentPremiumRisk4.Text);
                                    risks[3].DisabilityByAccidentComments = txtDesabilityByAccidentCommentsRisk4.Text;
                                    risks[3].DeathByAccidentAmount = Decimal.Parse(txtDeathByAccidentAmountRisk4.Text);
                                    risks[3].DeathByAccidentPeriod = txtDeathByAccidentPeriodRisk4.Text;
                                    risks[3].DeathByAccidentCoverageType = txtDeathByAccidentCoverageTypeRisk4.Text;
                                    risks[3].DeathByAccidentPremium = Decimal.Parse(txtDeathByAccidentPremiumRisk4.Text);
                                    risks[3].DeathByAccidentComments = txtDeathByAccidentCommentsRisk4.Text;
                                    risks[3].CriticalIllnessAmount = Decimal.Parse(txtCriticalIllnesesAmountRisk4.Text);
                                    risks[3].CriticalIllnessPeriod = txtCriticalIllnesesPeriodRisk4.Text;
                                    risks[3].CriticalIllnessCoverageType = txtCriticalIllnesesCoverageTypeRisk4.Text;
                                    risks[3].CriticalIllnessPremium = Decimal.Parse(txtCriticalIllnesesPremiumRisk4.Text);
                                    risks[3].CriticalIllnessComments = txtCriticalIllnesesCommentsRisk4.Text;
                                    risks[3].Other1Name = txtOther1_4.Text;
                                    risks[3].Other1Amount = Decimal.Parse(txtOther1AmountRisk4.Text);
                                    risks[3].Other1Period = txtOther1PeriodRisk4.Text;
                                    risks[3].Other1CoverageType = txtOther1CoverageTypeRisk4.Text;
                                    risks[3].Other1Premium = Decimal.Parse(txtOther1PremiumRisk4.Text);
                                    risks[3].Other1Comments = txtOther1CommentsRisk4.Text;
                                    risks[3].Other2Name = txtOther2_4.Text;
                                    risks[3].Other2Amount = Decimal.Parse(txtOther2AmountRisk4.Text);
                                    risks[3].Other2Period = txtOther2PeriodRisk4.Text;
                                    risks[3].Other2CoverageType = txtOther2CoverageTypeRisk4.Text;
                                    risks[3].Other2Premium = Decimal.Parse(txtOther2PremiumRisk4.Text);
                                    risks[3].Other2Comments = txtOther2CommentsRisk4.Text;
                                    risks[3].Other3Name = txtOther3_4.Text;
                                    risks[3].Other3Amount = Decimal.Parse(txtOther3AmountRisk4.Text);
                                    risks[3].Other3Period = txtOther3PeriodRisk4.Text;
                                    risks[3].Other3CoverageType = txtOther3CoverageTypeRisk4.Text;
                                    risks[3].Other3Premium = Decimal.Parse(txtOther3PremiumRisk4.Text);
                                    risks[3].Other3Comments = txtOther3CommentsRisk4.Text;
                                    risks[3].Other4Name = txtOther4_4.Text;
                                    risks[3].Other4Amount = Decimal.Parse(txtOther4AmountRisk4.Text);
                                    risks[3].Other4Period = txtOther4PeriodRisk4.Text;
                                    risks[3].Other4CoverageType = txtOther4CoverageTypeRisk4.Text;
                                    risks[3].Other4Premium = Decimal.Parse(txtOther4PremiumRisk4.Text);
                                    risks[3].Other4Comments = txtOther4CommentsRisk4.Text;
                                    risks[3].Other5Name = txtOther5_4.Text;
                                    risks[3].Other5Amount = Decimal.Parse(txtOther5AmountRisk4.Text);
                                    risks[3].Other5Period = txtOther5PeriodRisk4.Text;
                                    risks[3].Other5CoverageType = txtOther5CoverageTypeRisk4.Text;
                                    risks[3].Other5Premium = Decimal.Parse(txtOther5PremiumRisk4.Text);
                                    risks[3].Other5Comments = txtOther5CommentsRisk4.Text;
                                    //if (tbItemInsurencedRisk5.IsVisible)
                                    if (additionalsInsureds.Count > 3)
                                    {
                                        FillEmptyTextBoxes(new TextBox[] { txtDeathAmountRisk5, txtWorkImpossibilityAmountRisk5, txtIncomeSecureAmountRisk5, txtDesabilityByAccidentAmountRisk5, txtDeathByAccidentAmountRisk5, txtCriticalIllnesesAmountRisk5, txtOther1AmountRisk5, txtOther2AmountRisk5, txtOther3AmountRisk5, txtOther4AmountRisk5, txtOther5AmountRisk5, txtDeathPremiumRisk5, txtWorkImpossibilityPremiumRisk5, txtIncomeSecurePremiumRisk5, txtDesabilityByAccidentPremiumRisk5, txtDeathByAccidentPremiumRisk5, txtCriticalIllnesesPremiumRisk5, txtOther1PremiumRisk5, txtOther2PremiumRisk5, txtOther3PremiumRisk5, txtOther4PremiumRisk5, txtOther5PremiumRisk5 });

                                        risks[4].IsDeathType1 = rbDeath1Risk5.IsChecked;
                                        risks[4].IsDeathType5 = rbDeath5Risk5.IsChecked;
                                        risks[4].IsDeathTypeFixed = rbDeathFixedRisk5.IsChecked;
                                        risks[4].DeathAmount = Decimal.Parse(txtDeathAmountRisk5.Text);
                                        risks[4].DeathPeriod = txtDeathPeriodRisk5.Text;
                                        risks[4].DeathCoverageType = txtDeathCoverageTypeRisk5.Text;
                                        risks[4].DeathPremium = Decimal.Parse(txtDeathPremiumRisk5.Text);
                                        risks[4].DeathComments = txtDeathCommentsRisk5.Text;
                                        risks[4].IsProfessionalWorkImpossibility = rbProfessionalWorkImposibilitationRisk5.IsChecked;
                                        risks[4].WorkImpossibilityAmount = Decimal.Parse(txtWorkImpossibilityAmountRisk5.Text);
                                        risks[4].WorkImpossibilityPeriod = txtWorkImpossibilityPeriodRisk5.Text;
                                        risks[4].WorkImpossibilityCoverageType = txtWorkImpossibilityCoverageTypeRisk5.Text;
                                        risks[4].WorkImpossibilityPremium = Decimal.Parse(txtWorkImpossibilityPremiumRisk5.Text);
                                        risks[4].WorkImpossibilityComments = txtWorkImpossibilityCommentsRisk5.Text;
                                        risks[4].SecureIncomeAmount = Decimal.Parse(txtIncomeSecureAmountRisk5.Text);
                                        risks[4].SecureIncomePeriod = txtIncomeSecurePeriodRisk5.Text;
                                        risks[4].SecureIncomeCoverageType = txtIncomeSecureCoverageTypeRisk5.Text;
                                        risks[4].SecureIncomePremium = Decimal.Parse(txtIncomeSecurePremiumRisk5.Text);
                                        risks[4].SecureIncomeComments = txtIncomeSecureCommentsRisk5.Text;
                                        risks[4].DisabilityByAccidentAmount = Decimal.Parse(txtDesabilityByAccidentAmountRisk5.Text);
                                        risks[4].DisabilityByAccidentPeriod = txtDesabilityByAccidentPeriodRisk5.Text;
                                        risks[4].DisabilityByAccidentCoverageType = txtDesabilityByAccidentCoverageTypeRisk5.Text;
                                        risks[4].DisabilityByAccidentPremium = Decimal.Parse(txtDesabilityByAccidentPremiumRisk5.Text);
                                        risks[4].DisabilityByAccidentComments = txtDesabilityByAccidentCommentsRisk5.Text;
                                        risks[4].DeathByAccidentAmount = Decimal.Parse(txtDeathByAccidentAmountRisk5.Text);
                                        risks[4].DeathByAccidentPeriod = txtDeathByAccidentPeriodRisk5.Text;
                                        risks[4].DeathByAccidentCoverageType = txtDeathByAccidentCoverageTypeRisk5.Text;
                                        risks[4].DeathByAccidentPremium = Decimal.Parse(txtDeathByAccidentPremiumRisk5.Text);
                                        risks[4].DeathByAccidentComments = txtDeathByAccidentCommentsRisk5.Text;
                                        risks[4].CriticalIllnessAmount = Decimal.Parse(txtCriticalIllnesesAmountRisk5.Text);
                                        risks[4].CriticalIllnessPeriod = txtCriticalIllnesesPeriodRisk5.Text;
                                        risks[4].CriticalIllnessCoverageType = txtCriticalIllnesesCoverageTypeRisk5.Text;
                                        risks[4].CriticalIllnessPremium = Decimal.Parse(txtCriticalIllnesesPremiumRisk5.Text);
                                        risks[4].CriticalIllnessComments = txtCriticalIllnesesCommentsRisk5.Text;
                                        risks[4].Other1Name = txtOther1_5.Text;
                                        risks[4].Other1Amount = Decimal.Parse(txtOther1AmountRisk5.Text);
                                        risks[4].Other1Period = txtOther1PeriodRisk5.Text;
                                        risks[4].Other1CoverageType = txtOther1CoverageTypeRisk5.Text;
                                        risks[4].Other1Premium = Decimal.Parse(txtOther1PremiumRisk5.Text);
                                        risks[4].Other1Comments = txtOther1CommentsRisk5.Text;
                                        risks[4].Other2Name = txtOther2_5.Text;
                                        risks[4].Other2Amount = Decimal.Parse(txtOther2AmountRisk5.Text);
                                        risks[4].Other2Period = txtOther2PeriodRisk5.Text;
                                        risks[4].Other2CoverageType = txtOther2CoverageTypeRisk5.Text;
                                        risks[4].Other2Premium = Decimal.Parse(txtOther2PremiumRisk5.Text);
                                        risks[4].Other2Comments = txtOther2CommentsRisk5.Text;
                                        risks[4].Other3Name = txtOther3_5.Text;
                                        risks[4].Other3Amount = Decimal.Parse(txtOther3AmountRisk5.Text);
                                        risks[4].Other3Period = txtOther3PeriodRisk5.Text;
                                        risks[4].Other3CoverageType = txtOther3CoverageTypeRisk5.Text;
                                        risks[4].Other3Premium = Decimal.Parse(txtOther3PremiumRisk5.Text);
                                        risks[4].Other3Comments = txtOther3CommentsRisk5.Text;
                                        risks[4].Other4Name = txtOther4_5.Text;
                                        risks[4].Other4Amount = Decimal.Parse(txtOther4AmountRisk5.Text);
                                        risks[4].Other4Period = txtOther4PeriodRisk5.Text;
                                        risks[4].Other4CoverageType = txtOther4CoverageTypeRisk5.Text;
                                        risks[4].Other4Premium = Decimal.Parse(txtOther4PremiumRisk5.Text);
                                        risks[4].Other4Comments = txtOther4CommentsRisk5.Text;
                                        risks[4].Other5Name = txtOther5_5.Text;
                                        risks[4].Other5Amount = Decimal.Parse(txtOther5AmountRisk5.Text);
                                        risks[4].Other5Period = txtOther5PeriodRisk5.Text;
                                        risks[4].Other5CoverageType = txtOther5CoverageTypeRisk5.Text;
                                        risks[4].Other5Premium = Decimal.Parse(txtOther5PremiumRisk5.Text);
                                        risks[4].Other5Comments = txtOther5CommentsRisk5.Text;
                                    }
                                }
                            }
                        }
                        lifePolicyLogic.InsertRisks(lifePolicy.LifePolicyID, risks);
                    }
                }
                else if (tbItemMortgage.Visibility == Visibility.Visible)
                {
                    decimal? premiumRelease = null;
                    if (txtPremiumReleaseAmount.Text != "")
                    {
                        premiumRelease = Decimal.Parse(txtPremiumReleaseAmount.Text);
                    }
                    string relationship = null;
                    if (cbRelationshipMortgage.SelectedItem != null)
                    {
                        relationship = ((ComboBoxItem)cbRelationshipMortgage.SelectedItem).Content.ToString();
                    }
                    if (lifePolicy == null || isAddition == true || isMortgageAddition)
                    {
                        lifePolicyLogic.InsertMortgagePolicy(policyId, txtBeneficiaryNameMortgage.Text, txtBankNameMortgage.Text, txtBranchMortgage.Text, txtBankAddressMortgage.Text, txtBeneficiaryContactName.Text, txtBeneficiaryPhone.Text, txtBeneficiaryFax.Text, txtBeneficiaryEmail.Text, chbIsReleaseFromPayingPremiums.IsChecked, premiumRelease, txtCommentsMortgage.Text);
                        if (chbIsAdditionalInsuredMortgage.IsChecked == true)
                        {
                            lifePolicyLogic.InsertAdditionalInsured(policyId, txtAdditionalInsuredFirstNameMortgage.Text, txtAdditionalInsuredLastNameMortgage.Text, txtAdditionalInsuredIdMortgage.Text, dpAdditionalInsuredBirthDateMortgage.SelectedDate, relationship);
                        }
                        SaveUnsavedMortgage();
                        if (mortgages.Count > 0)
                        {
                            lifePolicyLogic.InsertMortgages(policyId, mortgages, isAddition);
                        }
                    }
                    else
                    {
                        lifePolicyLogic.UpdateMortgagePolicy(lifePolicy.LifePolicyID, txtBeneficiaryNameMortgage.Text, txtBankNameMortgage.Text, txtBranchMortgage.Text, txtBankAddressMortgage.Text, txtBeneficiaryContactName.Text, txtBeneficiaryPhone.Text, txtBeneficiaryFax.Text, txtBeneficiaryEmail.Text, chbIsReleaseFromPayingPremiums.IsChecked, premiumRelease, txtCommentsMortgage.Text);
                        if (chbIsAdditionalInsuredMortgage.IsChecked == true)
                        {
                            lifePolicyLogic.UpdateAdditionalInsured(lifePolicy.LifePolicyID, txtAdditionalInsuredFirstNameMortgage.Text, txtAdditionalInsuredLastNameMortgage.Text, txtAdditionalInsuredIdMortgage.Text, dpAdditionalInsuredBirthDateMortgage.SelectedDate, relationship);
                        }
                        else
                        {
                            lifePolicyLogic.DeleteAdditionalInsured(lifePolicy.LifePolicyID);
                        }
                        SaveUnsavedMortgage();
                        lifePolicyLogic.InsertMortgages(lifePolicy.LifePolicyID, mortgages, isAddition);
                    }
                }
                else if (tbItemPrivate.Visibility == Visibility.Visible)
                {
                    FillEmptyTextBoxes(new TextBox[] { txtAgePrivate, txtBudgetPrivate, txtStartBudgetPrivate, txtOneTimeDepositPrivate, txtPremiumFeesPrivate, txtAggregateFeesPrivate });

                    int? investmentPlanId = null;
                    if (cbInvestmentPrivate.SelectedItem != null)
                    {
                        investmentPlanId = ((InvesmentPlan)cbInvestmentPrivate.SelectedItem).InvesmentPlanID;
                    }
                    string relationship = null;
                    if (txtRelationshipPrivate.SelectedItem != null)
                    {
                        relationship = ((ComboBoxItem)txtRelationshipPrivate.SelectedItem).Content.ToString();
                    }
                    if (lifePolicy == null || isAddition == true)
                    {
                        lifePolicyLogic.InsertPrivatePolicy(policyId, rbRewards.IsChecked, int.Parse(txtAgePrivate.Text), decimal.Parse(txtBudgetPrivate.Text), chbIsFundNotPaysCompensationPrivate.IsChecked, decimal.Parse(txtStartBudgetPrivate.Text), decimal.Parse(txtOneTimeDepositPrivate.Text), decimal.Parse(txtPremiumFeesPrivate.Text), decimal.Parse(txtAggregateFeesPrivate.Text), investmentPlanId, txtCommentsPrivate.Text);
                        SaveUnsavedPrivateTransfer();
                        if (transfers.Count > 0)
                        {
                            lifePolicyLogic.InsertPolicyTransfer(policyId, transfers, isAddition);
                        }
                        if (chbIsAdditionalInsuredMortgage.IsChecked == true)
                        {
                            lifePolicyLogic.InsertAdditionalInsured(policyId, txtAdditionalInsuredFirstNamePrivate.Text, txtAdditionalInsuredLastNamePrivate.Text, txtAdditionalInsuredIdPrivate.Text, dpAdditionalInsuredBirthDatePrivate.SelectedDate, relationship);
                        }
                        //List<int> additionalIds = lifePolicyLogic.InsertAdditionalInsureds(policyId, additionalsInsureds);
                        if (chbIsRisksPrivate.IsChecked == true)
                        {
                            FillEmptyTextBoxes(new TextBox[] { txtDeathAmountPrivate, txtWorkImpossibilityAmountPrivate, txtIncomeSecureAmountPrivate, txtDesabilityByAccidentAmountPrivate, txtDeathByAccidentAmountPrivate, txtCriticalIllnesesAmountPrivate, txtOther1AmountPrivate, txtOther2AmountPrivate, txtOther3AmountPrivate, txtOther4AmountPrivate, txtOther5AmountPrivate, txtDeathPremiumPrivate, txtWorkImpossibilityPremiumPrivate, txtIncomeSecurePremiumPrivate, txtDesabilityByAccidentPremiumPrivate, txtDeathByAccidentPremiumPrivate, txtCriticalIllnesesPremiumPrivate, txtOther1PremiumPrivate, txtOther2PremiumPrivate, txtOther3PremiumPrivate, txtOther4PremiumPrivate, txtOther5PremiumPrivate });
                            Risk newRisk = new Risk()
                            {
                                LifeAdditionalInsuredID = null,
                                IsDeathType1 = rbDeath1Private.IsChecked,
                                IsDeathType5 = rbDeath5Private.IsChecked,
                                IsDeathTypeFixed = rbDeathFixedPrivate.IsChecked,
                                DeathAmount = Decimal.Parse(txtDeathAmountPrivate.Text),
                                DeathPeriod = txtDeathPeriodPrivate.Text,
                                DeathCoverageType = txtDeathCoverageTypePrivate.Text,
                                DeathPremium = Decimal.Parse(txtDeathPremiumPrivate.Text),
                                DeathComments = txtDeathCommentsPrivate.Text,
                                IsProfessionalWorkImpossibility = rbProfessionalWorkImposibilitationPrivate.IsChecked,
                                WorkImpossibilityAmount = Decimal.Parse(txtWorkImpossibilityAmountPrivate.Text),
                                WorkImpossibilityPeriod = txtWorkImpossibilityPeriodPrivate.Text,
                                WorkImpossibilityCoverageType = txtWorkImpossibilityCoverageTypePrivate.Text,
                                WorkImpossibilityPremium = Decimal.Parse(txtWorkImpossibilityPremiumPrivate.Text),
                                WorkImpossibilityComments = txtWorkImpossibilityCommentsPrivate.Text,
                                SecureIncomeAmount = Decimal.Parse(txtIncomeSecureAmountPrivate.Text),
                                SecureIncomePeriod = txtIncomeSecurePeriodPrivate.Text,
                                SecureIncomeCoverageType = txtIncomeSecureCoverageTypePrivate.Text,
                                SecureIncomePremium = Decimal.Parse(txtIncomeSecurePremiumPrivate.Text),
                                SecureIncomeComments = txtIncomeSecureCommentsPrivate.Text,
                                DisabilityByAccidentAmount = Decimal.Parse(txtDesabilityByAccidentAmountPrivate.Text),
                                DisabilityByAccidentPeriod = txtDesabilityByAccidentPeriodPrivate.Text,
                                DisabilityByAccidentCoverageType = txtDesabilityByAccidentCoverageTypePrivate.Text,
                                DisabilityByAccidentPremium = Decimal.Parse(txtDesabilityByAccidentPremiumPrivate.Text),
                                DisabilityByAccidentComments = txtDesabilityByAccidentCommentsPrivate.Text,
                                DeathByAccidentAmount = Decimal.Parse(txtDeathByAccidentAmountPrivate.Text),
                                DeathByAccidentPeriod = txtDeathByAccidentPeriodPrivate.Text,
                                DeathByAccidentCoverageType = txtDeathByAccidentCoverageTypePrivate.Text,
                                DeathByAccidentPremium = Decimal.Parse(txtDeathByAccidentPremiumPrivate.Text),
                                DeathByAccidentComments = txtDeathByAccidentCommentsPrivate.Text,
                                CriticalIllnessAmount = Decimal.Parse(txtCriticalIllnesesAmountPrivate.Text),
                                CriticalIllnessPeriod = txtCriticalIllnesesPeriodPrivate.Text,
                                CriticalIllnessCoverageType = txtCriticalIllnesesCoverageTypePrivate.Text,
                                CriticalIllnessPremium = Decimal.Parse(txtCriticalIllnesesPremiumPrivate.Text),
                                CriticalIllnessComments = txtCriticalIllnesesCommentsPrivate.Text,
                                Other1Name = txtOther1Private.Text,
                                Other1Amount = Decimal.Parse(txtOther1AmountPrivate.Text),
                                Other1Period = txtOther1PeriodPrivate.Text,
                                Other1CoverageType = txtCriticalIllnesesCoverageTypePrivate.Text,
                                Other1Premium = Decimal.Parse(txtOther1PremiumPrivate.Text),
                                Other1Comments = txtCriticalIllnesesCommentsPrivate.Text,
                                Other2Name = txtOther2Private.Text,
                                Other2Amount = Decimal.Parse(txtOther2AmountPrivate.Text),
                                Other2Period = txtOther2PeriodPrivate.Text,
                                Other2CoverageType = txtOther2CoverageTypePrivate.Text,
                                Other2Premium = Decimal.Parse(txtOther2PremiumPrivate.Text),
                                Other2Comments = txtOther2CommentsPrivate.Text,
                                Other3Name = txtOther3Private.Text,
                                Other3Amount = Decimal.Parse(txtOther3AmountPrivate.Text),
                                Other3Period = txtOther3PeriodPrivate.Text,
                                Other3CoverageType = txtOther3CoverageTypePrivate.Text,
                                Other3Premium = Decimal.Parse(txtOther3PremiumPrivate.Text),
                                Other3Comments = txtOther3CommentsPrivate.Text,
                                Other4Name = txtOther4Private.Text,
                                Other4Amount = Decimal.Parse(txtOther4AmountPrivate.Text),
                                Other4Period = txtOther4PeriodPrivate.Text,
                                Other4CoverageType = txtOther4CoverageTypePrivate.Text,
                                Other4Premium = Decimal.Parse(txtOther4PremiumPrivate.Text),
                                Other4Comments = txtOther4CommentsPrivate.Text,
                                Other5Name = txtOther5Private.Text,
                                Other5Amount = Decimal.Parse(txtOther5AmountPrivate.Text),
                                Other5Period = txtOther5PeriodPrivate.Text,
                                Other5CoverageType = txtOther5CoverageTypePrivate.Text,
                                Other5Premium = Decimal.Parse(txtOther5PremiumPrivate.Text),
                                Other5Comments = txtOther5CommentsPrivate.Text
                            };
                            risks.Add(newRisk);
                            if (tbItemInsurenced2Private.IsVisible)
                            {
                                FillEmptyTextBoxes(new TextBox[] { txtDeathAmountPrivate2, txtWorkImpossibilityAmountPrivate2, txtIncomeSecureAmountPrivate2, txtDesabilityByAccidentAmountPrivate2, txtDeathByAccidentAmountPrivate2, txtCriticalIllnesesAmountPrivate2, txtOther1AmountPrivate2, txtOther2AmountPrivate2, txtOther3AmountPrivate2, txtOther4AmountPrivate2, txtOther5AmountPrivate2, txtDeathPremiumPrivate2, txtWorkImpossibilityPremiumPrivate2, txtIncomeSecurePremiumPrivate2, txtDesabilityByAccidentPremiumPrivate2, txtDeathByAccidentPremiumPrivate2, txtCriticalIllnesesPremiumPrivate2, txtOther1PremiumPrivate2, txtOther2PremiumPrivate2, txtOther3PremiumPrivate2, txtOther4PremiumPrivate2, txtOther5PremiumPrivate2 });
                                Risk newAdditionalRisk = new Risk()
                                {
                                    LifeAdditionalInsuredID = additionalsInsureds[0].LifeAdditionalInsuredID,
                                    IsDeathType1 = rbDeath1Private2.IsChecked,
                                    IsDeathType5 = rbDeath5Private2.IsChecked,
                                    IsDeathTypeFixed = rbDeathFixedPrivate2.IsChecked,
                                    DeathAmount = Decimal.Parse(txtDeathAmountPrivate2.Text),
                                    DeathPeriod = txtDeathPeriodPrivate2.Text,
                                    DeathCoverageType = txtDeathCoverageTypePrivate2.Text,
                                    DeathPremium = Decimal.Parse(txtDeathPremiumPrivate2.Text),
                                    DeathComments = txtDeathCommentsPrivate2.Text,
                                    IsProfessionalWorkImpossibility = rbProfessionalWorkImposibilitationPrivate2.IsChecked,
                                    WorkImpossibilityAmount = Decimal.Parse(txtWorkImpossibilityAmountPrivate2.Text),
                                    WorkImpossibilityPeriod = txtWorkImpossibilityPeriodPrivate2.Text,
                                    WorkImpossibilityCoverageType = txtWorkImpossibilityCoverageTypePrivate2.Text,
                                    WorkImpossibilityPremium = Decimal.Parse(txtWorkImpossibilityPremiumPrivate2.Text),
                                    WorkImpossibilityComments = txtWorkImpossibilityCommentsPrivate2.Text,
                                    SecureIncomeAmount = Decimal.Parse(txtIncomeSecureAmountPrivate2.Text),
                                    SecureIncomePeriod = txtIncomeSecurePeriodPrivate2.Text,
                                    SecureIncomeCoverageType = txtIncomeSecureCoverageTypePrivate2.Text,
                                    SecureIncomePremium = Decimal.Parse(txtIncomeSecurePremiumPrivate2.Text),
                                    SecureIncomeComments = txtIncomeSecureCommentsPrivate2.Text,
                                    DisabilityByAccidentAmount = Decimal.Parse(txtDesabilityByAccidentAmountPrivate2.Text),
                                    DisabilityByAccidentPeriod = txtDesabilityByAccidentPeriodPrivate2.Text,
                                    DisabilityByAccidentCoverageType = txtDesabilityByAccidentCoverageTypePrivate2.Text,
                                    DisabilityByAccidentPremium = Decimal.Parse(txtDesabilityByAccidentPremiumPrivate2.Text),
                                    DisabilityByAccidentComments = txtDesabilityByAccidentCommentsPrivate2.Text,
                                    DeathByAccidentAmount = Decimal.Parse(txtDeathByAccidentAmountPrivate2.Text),
                                    DeathByAccidentPeriod = txtDeathByAccidentPeriodPrivate2.Text,
                                    DeathByAccidentCoverageType = txtDeathByAccidentCoverageTypePrivate2.Text,
                                    DeathByAccidentPremium = Decimal.Parse(txtDeathByAccidentPremiumPrivate2.Text),
                                    DeathByAccidentComments = txtDeathByAccidentCommentsPrivate2.Text,
                                    CriticalIllnessAmount = Decimal.Parse(txtCriticalIllnesesAmountPrivate2.Text),
                                    CriticalIllnessPeriod = txtCriticalIllnesesPeriodPrivate2.Text,
                                    CriticalIllnessCoverageType = txtCriticalIllnesesCoverageTypePrivate2.Text,
                                    CriticalIllnessPremium = Decimal.Parse(txtCriticalIllnesesPremiumPrivate2.Text),
                                    CriticalIllnessComments = txtCriticalIllnesesCommentsPrivate2.Text,
                                    Other1Name = txtOther1Private2.Text,
                                    Other1Amount = Decimal.Parse(txtOther1AmountPrivate2.Text),
                                    Other1Period = txtOther1PeriodPrivate2.Text,
                                    Other1CoverageType = txtCriticalIllnesesCoverageTypePrivate2.Text,
                                    Other1Premium = Decimal.Parse(txtOther1PremiumPrivate2.Text),
                                    Other1Comments = txtCriticalIllnesesCommentsPrivate2.Text,
                                    Other2Name = txtOther2Private2.Text,
                                    Other2Amount = Decimal.Parse(txtOther2AmountPrivate2.Text),
                                    Other2Period = txtOther2PeriodPrivate2.Text,
                                    Other2CoverageType = txtOther2CoverageTypePrivate2.Text,
                                    Other2Premium = Decimal.Parse(txtOther2PremiumPrivate2.Text),
                                    Other2Comments = txtOther2CommentsPrivate2.Text,
                                    Other3Name = txtOther3Private2.Text,
                                    Other3Amount = Decimal.Parse(txtOther3AmountPrivate2.Text),
                                    Other3Period = txtOther3PeriodPrivate2.Text,
                                    Other3CoverageType = txtOther3CoverageTypePrivate2.Text,
                                    Other3Premium = Decimal.Parse(txtOther3PremiumPrivate2.Text),
                                    Other3Comments = txtOther3CommentsPrivate2.Text,
                                    Other4Name = txtOther4Private2.Text,
                                    Other4Amount = Decimal.Parse(txtOther4AmountPrivate2.Text),
                                    Other4Period = txtOther4PeriodPrivate2.Text,
                                    Other4CoverageType = txtOther4CoverageTypePrivate2.Text,
                                    Other4Premium = Decimal.Parse(txtOther4PremiumPrivate2.Text),
                                    Other4Comments = txtOther4CommentsPrivate2.Text,
                                    Other5Name = txtOther5Private2.Text,
                                    Other5Amount = Decimal.Parse(txtOther5AmountPrivate2.Text),
                                    Other5Period = txtOther5PeriodPrivate2.Text,
                                    Other5CoverageType = txtOther5CoverageTypePrivate2.Text,
                                    Other5Premium = Decimal.Parse(txtOther5PremiumPrivate2.Text),
                                    Other5Comments = txtOther5CommentsPrivate2.Text
                                };
                                risks.Add(newAdditionalRisk);
                            }
                            lifePolicyLogic.InsertRisks(policyId, risks);

                        }
                    }
                    else
                    {
                        lifePolicyLogic.UpdatePrivatePolicy(lifePolicy.LifePolicyID, rbRewards.IsChecked, int.Parse(txtAgePrivate.Text), decimal.Parse(txtBudgetPrivate.Text), chbIsFundNotPaysCompensationPrivate.IsChecked, decimal.Parse(txtStartBudgetPrivate.Text), decimal.Parse(txtOneTimeDepositPrivate.Text), decimal.Parse(txtPremiumFeesPrivate.Text), decimal.Parse(txtAggregateFeesPrivate.Text), investmentPlanId, txtCommentsPrivate.Text);
                        SaveUnsavedPrivateTransfer();
                        lifePolicyLogic.InsertPolicyTransfer(lifePolicy.LifePolicyID, transfers, isAddition);
                        if (chbIsAdditionalInsuredPrivate.IsChecked == true)
                        {
                            lifePolicyLogic.UpdateAdditionalInsured(lifePolicy.LifePolicyID, txtAdditionalInsuredFirstNamePrivate.Text, txtAdditionalInsuredLastNamePrivate.Text, txtAdditionalInsuredIdPrivate.Text, dpAdditionalInsuredBirthDatePrivate.SelectedDate, relationship);
                        }
                        //else
                        //{
                        //    //lifePolicyLogic.DeleteAdditionalInsured(lifePolicy.LifePolicyID);
                        //}
                    }
                }
                else if (tbItemPension.Visibility == Visibility.Visible)
                {
                    bool isPersonalplan = true;
                    if (cbPensionPlanType.SelectedIndex == 1)
                    {
                        isPersonalplan = false;
                    }
                    int? investmentPalanId = null;
                    if (cbInvestmentPlanPension.SelectedItem != null)
                    {
                        investmentPalanId = ((InvesmentPlan)cbInvestmentPlanPension.SelectedItem).InvesmentPlanID;
                    }
                    int? fundJoiningPlanId = null;
                    if (cbFundJoiningPlan.SelectedItem != null)
                    {
                        fundJoiningPlanId = ((FundJoiningPlan)cbFundJoiningPlan.SelectedItem).FundJoiningPlanID;
                    }
                    int? insurancesWaiverId = null;
                    if (cbInsurencesWaivers.SelectedItem != null)
                    {
                        insurancesWaiverId = ((InsurancesWaiver)cbInsurencesWaivers.SelectedItem).InsurancesWaiverID;
                    }
                    int? retirementPlanId = null;
                    if (cbRetirementPlan.SelectedItem != null)
                    {
                        retirementPlanId = ((RetirementPlan)cbRetirementPlan.SelectedItem).RetirementPlanID;
                    }
                    FillEmptyTextBoxes(new TextBox[] { txtAgePension, txtBudgetPension, txtEmployeeSalaryPension, txtStartSalaryPension, txtParogramCompensationsPension, txtEmployerRewardPension, txtEmployeeRewardPension, txtOtherRewardsPension, txtPremiumFeesPension, txtAggregateFeesPension, txtPensionReceptionAge, txtRetirementAgeToPlan, txtEndContributionsPaymentAge, txtOldAgePension, txtDisabilityPension, txtWidowRests, txtOrphanRestsUntil21, txtMaximumRests, txtRevenueValue, txtDisabilityPercentage, txtWidowPercentage, txtOrphanPercentage, txtParentSupportedPercentage, txtPensionerRest });
                    if (lifePolicy == null || isAddition == true)
                    {
                        lifePolicyLogic.InsertPensionPolicy(policyId, rbEmployeePension.IsChecked, chbIsCompulsoryPensionPension.IsChecked, chbIsShareholderPension.IsChecked, int.Parse(txtAgePension.Text), decimal.Parse(txtBudgetPension.Text), decimal.Parse(txtEmployeeSalaryPension.Text), isPersonalplan, decimal.Parse(txtStartSalaryPension.Text), decimal.Parse(txtParogramCompensationsPension.Text), decimal.Parse(txtEmployerRewardPension.Text), decimal.Parse(txtEmployeeRewardPension.Text), decimal.Parse(txtOtherRewardsPension.Text), decimal.Parse(txtPremiumFeesPension.Text), decimal.Parse(txtAggregateFeesPension.Text), investmentPalanId, fundJoiningPlanId, insurancesWaiverId, retirementPlanId, int.Parse(txtPensionReceptionAge.Text), int.Parse(txtRetirementAgeToPlan.Text), int.Parse(txtEndContributionsPaymentAge.Text), chbIsDesabilityExtension.IsChecked, chbIsIndividualCoverage.IsChecked, chbIsWithChildren.IsChecked, decimal.Parse(txtOldAgePension.Text), decimal.Parse(txtPensionerRest.Text), decimal.Parse(txtDisabilityPension.Text), decimal.Parse(txtDisabilityPercentage.Text), decimal.Parse(txtWidowRests.Text), decimal.Parse(txtWidowPercentage.Text), decimal.Parse(txtOrphanRestsUntil21.Text), decimal.Parse(txtOrphanPercentage.Text), decimal.Parse(txtMaximumRests.Text), decimal.Parse(txtParentSupportedPercentage.Text), decimal.Parse(txtRevenueValue.Text), txtCommentsPension.Text);
                        SaveUnsavedPensionTransfer();
                        if (transfers.Count > 0)
                        {
                            lifePolicyLogic.InsertPolicyTransfer(policyId, transfers, isAddition);
                        }
                    }
                    else
                    {
                        lifePolicyLogic.UpdatePensionPolicy(lifePolicy.LifePolicyID, rbEmployeePension.IsChecked, chbIsCompulsoryPensionPension.IsChecked, chbIsShareholderPension.IsChecked, int.Parse(txtAgePension.Text), decimal.Parse(txtBudgetPension.Text), decimal.Parse(txtEmployeeSalaryPension.Text), isPersonalplan, decimal.Parse(txtStartSalaryPension.Text), decimal.Parse(txtParogramCompensationsPension.Text), decimal.Parse(txtEmployerRewardPension.Text), decimal.Parse(txtEmployeeRewardPension.Text), decimal.Parse(txtOtherRewardsPension.Text), decimal.Parse(txtPremiumFeesPension.Text), decimal.Parse(txtAggregateFeesPension.Text), investmentPalanId, fundJoiningPlanId, insurancesWaiverId, retirementPlanId, int.Parse(txtPensionReceptionAge.Text), int.Parse(txtRetirementAgeToPlan.Text), int.Parse(txtEndContributionsPaymentAge.Text), chbIsDesabilityExtension.IsChecked, chbIsIndividualCoverage.IsChecked, chbIsWithChildren.IsChecked, decimal.Parse(txtOldAgePension.Text), decimal.Parse(txtPensionerRest.Text), decimal.Parse(txtDisabilityPension.Text), decimal.Parse(txtDisabilityPercentage.Text), decimal.Parse(txtWidowRests.Text), decimal.Parse(txtWidowPercentage.Text), decimal.Parse(txtOrphanRestsUntil21.Text), decimal.Parse(txtOrphanPercentage.Text), decimal.Parse(txtMaximumRests.Text), decimal.Parse(txtParentSupportedPercentage.Text), decimal.Parse(txtRevenueValue.Text), txtCommentsPension.Text);
                        SaveUnsavedPensionTransfer();
                        lifePolicyLogic.InsertPolicyTransfer(lifePolicy.LifePolicyID, transfers, isAddition);
                    }
                }

                if (lifePolicy == null || isAddition == true)
                {
                    if (coverages.Count > 0)
                    {
                        coveragesLogic.InsertCoveragesToLifePolicy(policyId, coverages, isAddition);
                    }
                    if (trackings.Count > 0)
                    {
                        trackingLogics.InsertLifeTrackings(policyId, trackings, isAddition);
                    }
                    if (chbStandardMoneyCollection.IsChecked == true)
                    {
                        int standardMoneyCollectionId = moneyCollectionLogic.InsertStandardMoneyCollection(standardMoneyCollection, new Insurance() { InsuranceName = "חיים" }, policyId);
                        if (standardMoneyCollectionTrackings != null)
                        {
                            moneyCollectionLogic.InsertTrackingsToStandardMoneyCollection(standardMoneyCollectionId, standardMoneyCollectionTrackings);
                        }
                    }
                }
                else
                {
                    coveragesLogic.InsertCoveragesToLifePolicy(lifePolicy.LifePolicyID, coverages, isAddition);
                    trackingLogics.InsertLifeTrackings(lifePolicy.LifePolicyID, trackings, isAddition);
                    if (chbStandardMoneyCollection.IsChecked == true)
                    {
                        int standardMoneyCollectionId = moneyCollectionLogic.InsertStandardMoneyCollection(standardMoneyCollection, new Insurance() { InsuranceName = "חיים" }, lifePolicy.LifePolicyID);
                        moneyCollectionLogic.InsertTrackingsToStandardMoneyCollection(standardMoneyCollectionId, standardMoneyCollectionTrackings);
                    }
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }

            if (isAddition == false)
            {
                if (lifePolicy == null)
                {
                    if (!Directory.Exists(@path))
                    {
                        Directory.CreateDirectory(@path);
                    }
                }
                else
                {
                    if (path == null && newPath != null)
                    {
                        Directory.CreateDirectory(newPath);
                    }
                    else if (path != newPath)
                    {
                        try
                        {
                            Directory.Move(path, newPath);
                        }
                        catch (Exception)
                        {
                            System.Windows.Forms.MessageBox.Show("לא ניתן למצוא את נתיב התיקייה");
                        }
                    }
                }
            }
            if (sender is Button)
            {
                Button btn = (Button)sender;
                if (btn.Name == "btnSave")
                {
                    confirmBeforeClosing = false;
                    Close();
                }
            }
        }

        private void SaveUnsavedPensionTransfer()
        {
            if (cbTransferringFundPension.SelectedItem != null || dpTransferringDatePension.SelectedDate != null || cbTransferedToPension.SelectedItem != null || txtTransferringAmountPension.Text != "")
            {
                btnAddTransferencePension_Click(this, new RoutedEventArgs());
            }
        }

        private void SaveUnsavedMortgage()
        {
            if (txtMortgageNumber.Text != "" || txtMortgageAmount.Text != "" || txtAnualInterest.Text != "" || cbIndexationTypeMortgage.SelectedItem != null || dpMorgageStartDate.SelectedDate != null || dpMorgageEndDate.SelectedDate != null || txtMortgagePremium.Text != "" || txtMortgagePeriod.Text != "" || txtPremiumPeriod.Text != "")
            {
                btnAddMortgage_Click(this, new RoutedEventArgs());
            }
        }

        private void SaveUnsavedAdditionalInsured()
        {
            if (txtAdditionalInsurencedFirstNameRisk.Text != "" || txtAdditionalInsurencedLastNameRisk.Text != "" || txtAdditionalInsurencedIdRisk.Text != "" || dpAdditionalInsurencedBirthDateRisk.SelectedDate != null || cbRelationshipRisk.SelectedItem != null)
            {
                if (tbControlRisks.SelectedItem != null && tbControlRisks.SelectedIndex != 0)
                {
                    btnUpdateInsurencedRisk_Click(this, new RoutedEventArgs());
                }
                else
                {
                    btnAddInsurencedRisk_Click(this, new RoutedEventArgs());
                }
            }
        }

        private void SaveUnsavedAdmTransfer()
        {
            if (cbTransferringFundAdm.SelectedItem != null || dpTransferringDateAdm.SelectedDate != null || cbTransferedToAdm.SelectedItem != null || txtTransferringAmountAdm.Text != "")
            {
                btnAddTransferenceAdm_Click(this, new RoutedEventArgs());
            }
        }

        private void SaveUnsavedPrivateTransfer()
        {
            if (cbTransferringFundPrivate.SelectedItem != null || dpTransferringDatePrivate.SelectedDate != null || cbTransferedToPrivate.SelectedItem != null || txtTransferringAmountPrivate.Text != "")
            {
                btnAddTransferencePrivate_Click(this, new RoutedEventArgs());
            }
        }

        private void SaveUnsavedSalaryChange()
        {
            if (txtInsuredSalaryAdm.Text != "" || txtMonthlyBudgetAdm.Text != "" || dpSalaryFromDateAdm.SelectedDate != null || dpEndOfPeriodAdm.SelectedDate != null)
            {
                btnAddSalaryChangeAdm_Click(this, new RoutedEventArgs());
            }
        }

        private void btnAddSalaryChangeAdm_Click(object sender, RoutedEventArgs e)
        {
            if (txtBtnAddSalaryChange.Text.Contains("שינוי"))
            {
                if (txtInsuredSalaryAdm.Text == "" || txtInsuredSalaryAdm.Text == "0")
                {
                    if (sender is Window)
                    {
                        MessageBox.Show("שים לב, פרטי שינוי השכר לא עודכנו במערכת", "מידע", MessageBoxButton.OK, MessageBoxImage.Information);
                        return;
                    }
                    System.Windows.Forms.MessageBox.Show("שכר מבוטח הינו שדה חובה");

                    return;
                }
                if (dpEndOfPeriodAdm.SelectedDate <= dpSalaryFromDateAdm.SelectedDate)
                {
                    if (sender is Window)
                    {
                        MessageBox.Show("שים לב, פרטי שינוי השכר לא עודכנו במערכת", "מידע", MessageBoxButton.OK, MessageBoxImage.Information);
                        return;
                    }
                    MessageBox.Show("תאריך סיום חייב להיות גדול מתאריך תחילה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);

                    return;
                }
                FillEmptyTextBoxes(new TextBox[] { txtCompensationEmoloyerProvisionsAdm, txtBenefitsEmoloyerProvisionAdm, txtDisabilityEmoloyerProvisionAdm, txtOtherEmoloyerProvisionAdm, txtTotalEmoloyerProvisionAdm, txtCompensationEmoloyerPremiumAdm, txtBenefitsEmoloyerPremiumAdm, txtDisabilityEmoloyerPremiumAdm, txtOtherEmployerPremiumAdm, txtTotalEmoloyerEmoloyerPremiumAdm, txtBenefitsEmployee45ProvisionAdm, txtBenefitsEmployee47ProvisionAdm, txtOtherEmployeeProvisionAdm, txtTotalEmoloyeeProvisionAdm, txtBenefitsEmployee45PremiumAdm, txtBenefitsEmployee47PremiumAdm, txtOtherEmployeePremiumAdm, txtTotalEmoloyeePremiumAdm, txtInsuredSalaryAdm, txtMonthlyBudgetAdm });
                ManagerPolicyChanx newSalary = new ManagerPolicyChanx()
                {
                    EmployerCompensationProvisions = Decimal.Parse(txtCompensationEmoloyerProvisionsAdm.Text),
                    EmployerCompensationRevalued = Decimal.Parse(txtCompensationEmoloyerPremiumAdm.Text),
                    EmployerBenefitsProvisions = Decimal.Parse(txtBenefitsEmoloyerProvisionAdm.Text),
                    EmployerBenefitsRevalued = Decimal.Parse(txtBenefitsEmoloyerPremiumAdm.Text),
                    EmployerDisabilityProvisions = Decimal.Parse(txtDisabilityEmoloyerProvisionAdm.Text),
                    EmployerDisabilityRevalued = Decimal.Parse(txtDisabilityEmoloyerPremiumAdm.Text),
                    EmployerOthersProvisions = Decimal.Parse(txtOtherEmoloyerProvisionAdm.Text),
                    EmployerOthersRevalued = Decimal.Parse(txtOtherEmployerPremiumAdm.Text),
                    EmployerTotalProvisions = Decimal.Parse(txtTotalEmoloyerProvisionAdm.Text),
                    EmployerTotalRevalued = Decimal.Parse(txtTotalEmoloyerEmoloyerPremiumAdm.Text),
                    WorkerBenefits45Provisions = Decimal.Parse(txtBenefitsEmployee45ProvisionAdm.Text),
                    WorkerBenefits45Revalued = Decimal.Parse(txtBenefitsEmployee45PremiumAdm.Text),
                    WorkerBenefits47Provisions = Decimal.Parse(txtBenefitsEmployee47ProvisionAdm.Text),
                    WorkerBenefits47Revalued = Decimal.Parse(txtBenefitsEmployee47PremiumAdm.Text),
                    WorkerOthersProvisions1 = Decimal.Parse(txtOtherEmployeeProvisionAdm.Text),
                    WorkerOthersRevalued1 = Decimal.Parse(txtOtherEmployeePremiumAdm.Text),
                    WorkerTotalProvisions1 = Decimal.Parse(txtTotalEmoloyeeProvisionAdm.Text),
                    WorkerTotalRevalued1 = Decimal.Parse(txtTotalEmoloyeePremiumAdm.Text),
                    TotalSalary = Decimal.Parse(txtInsuredSalaryAdm.Text),
                    MonthlyBudget = Decimal.Parse(txtMonthlyBudgetAdm.Text),
                    FromDate = dpSalaryFromDateAdm.SelectedDate,
                    EndPeriod = dpEndOfPeriodAdm.SelectedDate,
                    TotalPremium = Decimal.Parse(txtTotalEmoloyerEmoloyerPremiumAdm.Text) + Decimal.Parse(txtTotalEmoloyeePremiumAdm.Text)
                };
                salaryChanges.Add(newSalary);
            }
            else
            {
                if (salarySelected != null)
                {
                    salarySelected.EmployerCompensationProvisions = Decimal.Parse(txtCompensationEmoloyerProvisionsAdm.Text);
                    salarySelected.EmployerCompensationRevalued = Decimal.Parse(txtCompensationEmoloyerPremiumAdm.Text);
                    salarySelected.EmployerBenefitsProvisions = Decimal.Parse(txtBenefitsEmoloyerProvisionAdm.Text);
                    salarySelected.EmployerBenefitsRevalued = Decimal.Parse(txtBenefitsEmoloyerPremiumAdm.Text);
                    salarySelected.EmployerDisabilityProvisions = Decimal.Parse(txtDisabilityEmoloyerProvisionAdm.Text);
                    salarySelected.EmployerDisabilityRevalued = Decimal.Parse(txtDisabilityEmoloyerPremiumAdm.Text);
                    salarySelected.EmployerOthersProvisions = Decimal.Parse(txtOtherEmoloyerProvisionAdm.Text);
                    salarySelected.EmployerOthersRevalued = Decimal.Parse(txtOtherEmployerPremiumAdm.Text);
                    salarySelected.EmployerTotalProvisions = Decimal.Parse(txtTotalEmoloyerProvisionAdm.Text);
                    salarySelected.EmployerTotalRevalued = Decimal.Parse(txtTotalEmoloyerEmoloyerPremiumAdm.Text);
                    salarySelected.WorkerBenefits45Provisions = Decimal.Parse(txtBenefitsEmployee45ProvisionAdm.Text);
                    salarySelected.WorkerBenefits45Revalued = Decimal.Parse(txtBenefitsEmployee45PremiumAdm.Text);
                    salarySelected.WorkerBenefits47Provisions = Decimal.Parse(txtBenefitsEmployee47ProvisionAdm.Text);
                    salarySelected.WorkerBenefits47Revalued = Decimal.Parse(txtBenefitsEmployee47PremiumAdm.Text);
                    salarySelected.WorkerOthersProvisions1 = Decimal.Parse(txtOtherEmployeeProvisionAdm.Text);
                    salarySelected.WorkerOthersRevalued1 = Decimal.Parse(txtOtherEmployeePremiumAdm.Text);
                    salarySelected.WorkerTotalProvisions1 = Decimal.Parse(txtTotalEmoloyeeProvisionAdm.Text);
                    salarySelected.WorkerTotalRevalued1 = Decimal.Parse(txtTotalEmoloyeePremiumAdm.Text);
                    salarySelected.TotalSalary = Decimal.Parse(txtInsuredSalaryAdm.Text);
                    salarySelected.MonthlyBudget = Decimal.Parse(txtMonthlyBudgetAdm.Text);
                    salarySelected.FromDate = dpSalaryFromDateAdm.SelectedDate;
                    salarySelected.EndPeriod = dpEndOfPeriodAdm.SelectedDate;
                    salarySelected.TotalPremium = Decimal.Parse(txtTotalEmoloyerEmoloyerPremiumAdm.Text) + Decimal.Parse(txtTotalEmoloyeePremiumAdm.Text);
                }
            }
            ClearInputs(new object[] { dpSalaryFromDateAdm, dpEndOfPeriodAdm, txtCompensationEmoloyerProvisionsAdm, txtBenefitsEmoloyerProvisionAdm, txtDisabilityEmoloyerProvisionAdm, txtOtherEmoloyerProvisionAdm, txtTotalEmoloyerProvisionAdm, txtCompensationEmoloyerPremiumAdm, txtBenefitsEmoloyerPremiumAdm, txtDisabilityEmoloyerPremiumAdm, txtOtherEmployerPremiumAdm, txtTotalEmoloyerEmoloyerPremiumAdm, txtBenefitsEmployee45ProvisionAdm, txtBenefitsEmployee47ProvisionAdm, txtOtherEmployeeProvisionAdm, txtTotalEmoloyeeProvisionAdm, txtBenefitsEmployee45PremiumAdm, txtBenefitsEmployee47PremiumAdm, txtOtherEmployeePremiumAdm, txtTotalEmoloyeePremiumAdm, txtInsuredSalaryAdm, txtMonthlyBudgetAdm });
            txtBtnAddSalaryChange.Text = "שינוי משכורת";
            dgAdmPolicySalaryChanges.UnselectAll();
            ResetData();
            dgAdmPolicySalaryChangesBinding();
        }

        private void ResetData()
        {
            salary = 0;
            employerCompensation = 0;
            employerBenefits = 0;
            employerDesability = 0;
            employerOthers = 0;
            compensationPremium = 0;
            benefitPremium = 0;
            disabilityPremium = 0;
            otherEmployerPremium = 0;
            workerBenefits45 = 0;
            workerBenefits47 = 0;
            workerOthers = 0;
            benefits45Premium = 0;
            benefits47Premium = 0;
            otherWorkerPremium = 0;
        }

        public void FillEmptyTextBoxes(TextBox[] inputs)
        {
            foreach (var item in inputs)
            {
                if (item.Text == "")
                {
                    item.Text = "0";
                }
            }
        }

        private void ClearInputs(object[] inputsToClear)
        {

            foreach (var item in inputsToClear)
            {
                if (item is TextBox)
                {
                    ((TextBox)item).Clear();
                }
                else if (item is ComboBox)
                {
                    ((ComboBox)item).SelectedIndex = -1;
                    ((ComboBox)item).Text = "";
                }
                else if (item is DatePicker)
                {
                    ((DatePicker)item).SelectedDate = null;
                }
            }
        }

        private void dgAdmPolicySalaryChangesBinding()
        {
            dgAdmPolicySalaryChanges.ItemsSource = salaryChanges.OrderByDescending(s => s.FromDate).ToList();
            lv.AutoSizeColumns(dgAdmPolicySalaryChanges.View);
        }

        private void dgAdmPolicySalaryChanges_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (dgAdmPolicySalaryChanges.SelectedItem != null)
            {
                salarySelected = (ManagerPolicyChanx)dgAdmPolicySalaryChanges.SelectedItem;
                txtInsuredSalaryAdm.Text = validations.ConvertDecimalToString(salarySelected.TotalSalary);
                txtMonthlyBudgetAdm.Text = validations.ConvertDecimalToString(salarySelected.MonthlyBudget);
                dpSalaryFromDateAdm.SelectedDate = salarySelected.FromDate;
                dpEndOfPeriodAdm.SelectedDate = salarySelected.EndPeriod;
                txtCompensationEmoloyerProvisionsAdm.Text = validations.ConvertDecimalToString(salarySelected.EmployerCompensationProvisions);
                txtCompensationEmoloyerPremiumAdm.Text = validations.ConvertDecimalToString(salarySelected.EmployerCompensationRevalued);
                txtBenefitsEmoloyerProvisionAdm.Text = validations.ConvertDecimalToString(salarySelected.EmployerBenefitsProvisions);
                txtBenefitsEmoloyerPremiumAdm.Text = validations.ConvertDecimalToString(salarySelected.EmployerBenefitsRevalued);
                txtDisabilityEmoloyerProvisionAdm.Text = validations.ConvertDecimalToString(salarySelected.EmployerDisabilityProvisions);
                txtDisabilityEmoloyerPremiumAdm.Text = validations.ConvertDecimalToString(salarySelected.EmployerDisabilityRevalued);
                txtOtherEmoloyerProvisionAdm.Text = validations.ConvertDecimalToString(salarySelected.EmployerOthersProvisions);
                txtOtherEmployerPremiumAdm.Text = validations.ConvertDecimalToString(salarySelected.EmployerOthersRevalued);
                txtTotalEmoloyerProvisionAdm.Text = validations.ConvertDecimalToString(salarySelected.EmployerTotalProvisions);
                txtTotalEmoloyerEmoloyerPremiumAdm.Text = validations.ConvertDecimalToString(salarySelected.EmployerTotalRevalued);
                txtBenefitsEmployee45ProvisionAdm.Text = validations.ConvertDecimalToString(salarySelected.WorkerBenefits45Provisions);
                txtBenefitsEmployee45PremiumAdm.Text = validations.ConvertDecimalToString(salarySelected.WorkerBenefits45Revalued);
                txtBenefitsEmployee47ProvisionAdm.Text = validations.ConvertDecimalToString(salarySelected.WorkerBenefits47Provisions);
                txtBenefitsEmployee47PremiumAdm.Text = validations.ConvertDecimalToString(salarySelected.WorkerBenefits47Revalued);
                txtOtherEmployeeProvisionAdm.Text = validations.ConvertDecimalToString(salarySelected.WorkerOthersProvisions1);
                txtOtherEmployeePremiumAdm.Text = validations.ConvertDecimalToString(salarySelected.WorkerOthersRevalued1);
                txtTotalEmoloyeeProvisionAdm.Text = validations.ConvertDecimalToString(salarySelected.WorkerTotalProvisions1);
                txtTotalEmoloyeePremiumAdm.Text = validations.ConvertDecimalToString(salarySelected.WorkerTotalRevalued1);
                txtBtnAddSalaryChange.Text = "עדכן משכורת";
            }
        }

        private void txtCompensationEmoloyerProvisionsAdm_TextChanged(object sender, TextChangedEventArgs e)
        {
            txtCompensationEmoloyerProvisionsAdm.ClearValue(BorderBrushProperty);
            if (txtCompensationEmoloyerProvisionsAdm.Text != "")
            {
                try
                {
                    employerCompensation = validations.ConvertStringToDecimal(txtCompensationEmoloyerProvisionsAdm.Text);
                }
                catch (Exception ex)
                {
                    txtCompensationEmoloyerProvisionsAdm.BorderBrush = System.Windows.Media.Brushes.Red;
                    System.Windows.Forms.MessageBox.Show(ex.Message);
                    txtCompensationEmoloyerProvisionsAdm.Text = "";
                }

                EmployerDataCalculator();
            }
        }


        public void EmployerDataCalculator()
        {
            compensationPremium = salary * employerCompensation / 100;
            txtCompensationEmoloyerPremiumAdm.Text = compensationPremium.ToString("0.##");
            benefitPremium = salary * employerBenefits / 100;
            txtBenefitsEmoloyerPremiumAdm.Text = benefitPremium.ToString("0.##");
            otherEmployerPremium = salary * employerOthers / 100;
            txtOtherEmployerPremiumAdm.Text = otherEmployerPremium.ToString("0.##");
            disabilityPremium = salary * employerDesability / 100;
            txtDisabilityEmoloyerPremiumAdm.Text = disabilityPremium.ToString("0.##");
            txtTotalEmoloyerProvisionAdm.Text = (employerCompensation + employerBenefits + employerDesability + employerOthers).ToString("0.##");
            txtTotalEmoloyerEmoloyerPremiumAdm.Text = (compensationPremium + benefitPremium + disabilityPremium + otherEmployerPremium).ToString("0.##");
        }

        public void WorkerDataCalculator()
        {
            benefits45Premium = salary * workerBenefits45 / 100;
            txtBenefitsEmployee45PremiumAdm.Text = benefits45Premium.ToString("0.##");
            benefits47Premium = salary * workerBenefits47 / 100;
            txtBenefitsEmployee47PremiumAdm.Text = benefits47Premium.ToString("0.##");
            otherWorkerPremium = salary * workerOthers / 100;
            txtOtherEmployeePremiumAdm.Text = otherWorkerPremium.ToString("0.##");
            txtTotalEmoloyeeProvisionAdm.Text = (workerBenefits45 + workerBenefits47 + workerOthers).ToString("0.##");
            txtTotalEmoloyeePremiumAdm.Text = (benefits45Premium + benefits47Premium + otherWorkerPremium).ToString("0.##");
        }


        private void txtInsuredSalaryAdm_TextChanged(object sender, TextChangedEventArgs e)
        {
            txtInsuredSalaryAdm.ClearValue(BorderBrushProperty);
            if (txtInsuredSalaryAdm.Text != "")
            {
                try
                {
                    salary = validations.ConvertStringToDecimal(txtInsuredSalaryAdm.Text);
                }
                catch (Exception ex)
                {
                    txtInsuredSalaryAdm.BorderBrush = System.Windows.Media.Brushes.Red;
                    System.Windows.Forms.MessageBox.Show(ex.Message);
                    txtInsuredSalaryAdm.Text = "";
                }
                spEmployerProvisions.IsEnabled = true;
                spWorkerProvisions.IsEnabled = true;
                EmployerDataCalculator();
                WorkerDataCalculator();
            }
            else
            {
                spEmployerProvisions.IsEnabled = false;
                spWorkerProvisions.IsEnabled = false;
            }

        }

        private void txtBenefitsEmoloyerProvisionAdm_TextChanged(object sender, TextChangedEventArgs e)
        {
            txtBenefitsEmoloyerProvisionAdm.ClearValue(BorderBrushProperty);
            if (txtBenefitsEmoloyerProvisionAdm.Text != "")
            {
                try
                {
                    employerBenefits = validations.ConvertStringToDecimal(txtBenefitsEmoloyerProvisionAdm.Text);
                }
                catch (Exception ex)
                {
                    txtBenefitsEmoloyerProvisionAdm.BorderBrush = System.Windows.Media.Brushes.Red;
                    System.Windows.Forms.MessageBox.Show(ex.Message);
                    txtBenefitsEmoloyerProvisionAdm.Text = "";
                }
                //benefitPremium = salary * employerBenefits;
                //txtBenefitsEmoloyerPremiumAdm.Text = benefitPremium.ToString();
                EmployerDataCalculator();
            }
        }

        private void txtDisabilityEmoloyerProvisionAdm_TextChanged(object sender, TextChangedEventArgs e)
        {
            txtDisabilityEmoloyerProvisionAdm.ClearValue(BorderBrushProperty);
            if (txtDisabilityEmoloyerProvisionAdm.Text != "")
            {
                try
                {
                    employerDesability = validations.ConvertStringToDecimal(txtDisabilityEmoloyerProvisionAdm.Text);
                }
                catch (Exception ex)
                {
                    txtDisabilityEmoloyerProvisionAdm.BorderBrush = System.Windows.Media.Brushes.Red;
                    System.Windows.Forms.MessageBox.Show(ex.Message);
                    txtDisabilityEmoloyerProvisionAdm.Text = "";
                }
                //disabilityPremium = salary * employerDesability;
                //txtDisabilityEmoloyerPremiumAdm.Text = disabilityPremium.ToString();
                EmployerDataCalculator();
            }
        }

        private void txtOtherEmoloyerProvisionAdm_TextChanged(object sender, TextChangedEventArgs e)
        {
            txtOtherEmoloyerProvisionAdm.ClearValue(BorderBrushProperty);
            if (txtOtherEmoloyerProvisionAdm.Text != "")
            {
                try
                {
                    employerOthers = validations.ConvertStringToDecimal(txtOtherEmoloyerProvisionAdm.Text);
                }
                catch (Exception ex)
                {
                    txtOtherEmoloyerProvisionAdm.BorderBrush = System.Windows.Media.Brushes.Red;
                    System.Windows.Forms.MessageBox.Show(ex.Message);
                    txtOtherEmoloyerProvisionAdm.Text = "";
                }
                EmployerDataCalculator();
            }
        }

        private void txtBenefitsEmployee45ProvisionAdm_TextChanged(object sender, TextChangedEventArgs e)
        {
            txtBenefitsEmployee45ProvisionAdm.ClearValue(BorderBrushProperty);
            if (txtBenefitsEmployee45ProvisionAdm.Text != "")
            {
                try
                {
                    workerBenefits45 = validations.ConvertStringToDecimal(txtBenefitsEmployee45ProvisionAdm.Text);
                }
                catch (Exception ex)
                {
                    txtBenefitsEmployee45ProvisionAdm.BorderBrush = System.Windows.Media.Brushes.Red;
                    System.Windows.Forms.MessageBox.Show(ex.Message);
                    txtBenefitsEmployee45ProvisionAdm.Text = "";
                }
                WorkerDataCalculator();
            }
        }

        private void txtBenefitsEmployee47ProvisionAdm_TextChanged(object sender, TextChangedEventArgs e)
        {
            txtBenefitsEmployee47ProvisionAdm.ClearValue(BorderBrushProperty);
            if (txtBenefitsEmployee47ProvisionAdm.Text != "")
            {
                try
                {
                    workerBenefits47 = validations.ConvertStringToDecimal(txtBenefitsEmployee47ProvisionAdm.Text);
                }
                catch (Exception ex)
                {
                    txtBenefitsEmployee47ProvisionAdm.BorderBrush = System.Windows.Media.Brushes.Red;
                    System.Windows.Forms.MessageBox.Show(ex.Message);
                    txtBenefitsEmployee47ProvisionAdm.Text = "";
                }
                WorkerDataCalculator();
            }
        }

        private void txtOtherEmployeeProvisionAdm_TextChanged(object sender, TextChangedEventArgs e)
        {
            txtOtherEmployeeProvisionAdm.ClearValue(BorderBrushProperty);
            if (txtOtherEmployeeProvisionAdm.Text != "")
            {
                try
                {
                    workerOthers = validations.ConvertStringToDecimal(txtOtherEmployeeProvisionAdm.Text);
                }
                catch (Exception ex)
                {
                    txtOtherEmployeeProvisionAdm.BorderBrush = System.Windows.Media.Brushes.Red;
                    System.Windows.Forms.MessageBox.Show(ex.Message);
                    txtOtherEmployeeProvisionAdm.Text = "";
                }
                WorkerDataCalculator();
            }
        }

        private void dgAdmPolicySalaryChanges_MouseDown(object sender, MouseButtonEventArgs e)
        {
            salarySelected = null;
            dgAdmPolicySalaryChanges.UnselectAll();
            ClearInputs(new object[] { dpSalaryFromDateAdm, dpEndOfPeriodAdm, txtCompensationEmoloyerProvisionsAdm, txtBenefitsEmoloyerProvisionAdm, txtDisabilityEmoloyerProvisionAdm, txtOtherEmoloyerProvisionAdm, txtTotalEmoloyerProvisionAdm, txtCompensationEmoloyerPremiumAdm, txtBenefitsEmoloyerPremiumAdm, txtDisabilityEmoloyerPremiumAdm, txtOtherEmployerPremiumAdm, txtTotalEmoloyerEmoloyerPremiumAdm, txtBenefitsEmployee45ProvisionAdm, txtBenefitsEmployee47ProvisionAdm, txtOtherEmployeeProvisionAdm, txtTotalEmoloyeeProvisionAdm, txtBenefitsEmployee45PremiumAdm, txtBenefitsEmployee47PremiumAdm, txtOtherEmployeePremiumAdm, txtTotalEmoloyeePremiumAdm, txtInsuredSalaryAdm, txtMonthlyBudgetAdm });
            txtBtnAddSalaryChange.Text = "שינוי משכורת";
        }

        private void btnDeleteSalaryChangeAdm_Click(object sender, RoutedEventArgs e)
        {
            if (dgAdmPolicySalaryChanges.SelectedItem != null)
            {
                if (MessageBox.Show("למחוק משכורת", "אישור", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    salaryChanges.Remove((ManagerPolicyChanx)dgAdmPolicySalaryChanges.SelectedItem);
                    dgAdmPolicySalaryChangesBinding();
                    ClearInputs(new object[] { dpSalaryFromDateAdm, dpEndOfPeriodAdm, txtCompensationEmoloyerProvisionsAdm, txtBenefitsEmoloyerProvisionAdm, txtDisabilityEmoloyerProvisionAdm, txtOtherEmoloyerProvisionAdm, txtTotalEmoloyerProvisionAdm, txtCompensationEmoloyerPremiumAdm, txtBenefitsEmoloyerPremiumAdm, txtDisabilityEmoloyerPremiumAdm, txtOtherEmployerPremiumAdm, txtTotalEmoloyerEmoloyerPremiumAdm, txtBenefitsEmployee45ProvisionAdm, txtBenefitsEmployee47ProvisionAdm, txtOtherEmployeeProvisionAdm, txtTotalEmoloyeeProvisionAdm, txtBenefitsEmployee45PremiumAdm, txtBenefitsEmployee47PremiumAdm, txtOtherEmployeePremiumAdm, txtTotalEmoloyeePremiumAdm, txtInsuredSalaryAdm, txtMonthlyBudgetAdm });
                    txtBtnAddSalaryChange.Text = "שינוי משכורת";
                }
            }
            else
            {
                System.Windows.Forms.MessageBox.Show("נא סמן את המשכורת שברצונך לבטל");
            }
        }

        private void cbTransferringFundAdm_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cbTransferringFundAdm.SelectedItem != null)
            {
                cbFundTypeAdm.IsEnabled = true;
                btnUpdateFundTypeTableAdm.IsEnabled = true;
                TransferringFundsTypeAdmBinding();
            }
            else
            {
                cbFundTypeAdm.IsEnabled = false;
                btnUpdateFundTypeTableAdm.IsEnabled = false;
            }
        }

        private void TransferringFundsTypeAdmBinding()
        {
            if (cbTransferringFundAdm.SelectedItem != null)
            {
                int? selectedItemId = null;
                if (cbFundTypeAdm.SelectedItem != null)
                {
                    selectedItemId = ((FundType)cbFundTypeAdm.SelectedItem).FundTypeID;
                }
                cbFundTypeAdm.ItemsSource = fundsLogics.GetActiveFundTypesByInsuranceAndCompany(insuranceLogic.GetInsuranceID("חיים"), ((LifeIndustry)cbLifeIndustry.SelectedItem).LifeIndustryID, ((InsuranceCompany)cbTransferringFundAdm.SelectedItem).CompanyID);
                cbFundTypeAdm.DisplayMemberPath = "FundTypeName";
                if (selectedItemId != null)
                {
                    SelectFundItemInCb((int)selectedItemId, cbFundTypeAdm);
                }
            }
        }
        private void TransferedFundsTypeAdmBinding()
        {
            if (cbTransferedToAdm.SelectedItem != null)
            {
                int? selectedItemId = null;
                if (cbTransferedToFundType.SelectedItem != null)
                {
                    selectedItemId = ((FundType)cbTransferedToFundType.SelectedItem).FundTypeID;
                }
                cbTransferedToFundType.ItemsSource = fundsLogics.GetActiveFundTypesByInsuranceAndCompany(insuranceLogic.GetInsuranceID("חיים"), ((LifeIndustry)cbLifeIndustry.SelectedItem).LifeIndustryID, ((InsuranceCompany)cbTransferedToAdm.SelectedItem).CompanyID);
                cbTransferedToFundType.DisplayMemberPath = "FundTypeName";
                if (selectedItemId != null)
                {
                    SelectFundItemInCb((int)selectedItemId, cbTransferedToFundType);
                }
            }
        }

        private void cbTransferedToAdm_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cbTransferedToAdm.SelectedItem != null)
            {
                cbTransferedToFundType.IsEnabled = true;
                btnUpdateTransferedToFundTypeTableAdm.IsEnabled = true;
                TransferedFundsTypeAdmBinding();
            }
            else
            {
                cbTransferedToFundType.IsEnabled = false;
                btnUpdateTransferedToFundTypeTableAdm.IsEnabled = false;
            }
        }

        private void btnUpdateFundTypeTableAdm_Click(object sender, RoutedEventArgs e)
        {
            int? selectedItemId = null;
            if (cbFundTypeAdm.SelectedItem != null)
            {
                selectedItemId = ((FundType)cbFundTypeAdm.SelectedItem).FundTypeID;
            }
            int insuranceID = insuranceLogic.GetInsuranceID("חיים");
            FundType fundType = new FundType() { InsuranceID = insuranceID, LifeIndustryID = ((LifeIndustry)cbLifeIndustry.SelectedItem).LifeIndustryID, CompanyID = ((InsuranceCompany)cbTransferringFundAdm.SelectedItem).CompanyID };
            frmUpdateTable updateFundTypesTable = new frmUpdateTable(fundType);
            updateFundTypesTable.ShowDialog();
            TransferringFundsTypeAdmBinding();
            TransferedFundsTypeAdmBinding();
            if (updateFundTypesTable.ItemAdded != null)
            {
                SelectFundItemInCb(((FundType)updateFundTypesTable.ItemAdded).FundTypeID, cbFundTypeAdm);
            }
            else if (selectedItemId != null)
            {
                SelectFundItemInCb((int)selectedItemId, cbFundTypeAdm);
            }
        }

        private void btnAddTransferenceAdm_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                int? transferringCompanyId = null;
                Company transferringCompany = null;
                if (cbTransferringFundAdm.SelectedItem != null)
                {
                    transferringCompanyId = ((InsuranceCompany)cbTransferringFundAdm.SelectedItem).CompanyID;
                    transferringCompany = ((InsuranceCompany)cbTransferringFundAdm.SelectedItem).Company;
                }
                int? transferringFundTypeId = null;
                FundType transferrinfFundType = null;
                if (cbFundTypeAdm.SelectedItem != null)
                {
                    transferringFundTypeId = ((FundType)cbFundTypeAdm.SelectedItem).FundTypeID;
                    transferrinfFundType = (FundType)cbFundTypeAdm.SelectedItem;
                }
                int? transferedToCompanyId = null;
                Company transferedToCompany = null;
                if (cbTransferedToAdm.SelectedItem != null)
                {
                    transferedToCompanyId = ((InsuranceCompany)cbTransferedToAdm.SelectedItem).CompanyID;
                    transferedToCompany = ((InsuranceCompany)cbTransferedToAdm.SelectedItem).Company;
                }
                int? transferedToFundTypeId = null;
                FundType transferedToFundType = null;
                if (cbTransferedToFundType.SelectedItem != null)
                {
                    transferedToFundTypeId = ((FundType)cbTransferedToFundType.SelectedItem).FundTypeID;
                    transferedToFundType = (FundType)cbTransferedToFundType.SelectedItem;
                }
                if (txtBtnAddTransferAdm.Text.Contains("הוסף"))
                {
                    transferSelected = null;
                    LifePolicyTransfer newTransfer = new LifePolicyTransfer() { TransferringCompanyID = transferringCompanyId, FundTypeID = transferringFundTypeId, TransferDate = dpTransferringDateAdm.SelectedDate, TransferAmount = transferingAmount, TransferedToCompanyID = transferedToCompanyId, TransferedToFundTypeID = transferedToFundTypeId, Company = transferringCompany, FundType = transferrinfFundType, Company1 = transferedToCompany, FundType1 = transferedToFundType };
                    transfers.Add(newTransfer);
                }
                else
                {
                    if (transferSelected != null)
                    {
                        transferSelected.TransferringCompanyID = transferringCompanyId;
                        transferSelected.FundTypeID = transferringFundTypeId;
                        transferSelected.TransferDate = dpTransferringDateAdm.SelectedDate;
                        transferSelected.TransferAmount = transferingAmount;
                        transferSelected.TransferedToCompanyID = transferedToCompanyId;
                        transferSelected.TransferedToFundTypeID = transferedToFundTypeId;
                        transferSelected.Company = transferringCompany;
                        transferSelected.FundType = transferrinfFundType;
                        transferSelected.Company1 = transferedToCompany;
                        transferSelected.FundType1 = transferedToFundType;
                    }
                }
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
            }
            dgTransferencesAdmBinding();
            txtBtnAddTransferAdm.Text = "הוסף העברה";
            ClearInputs(new object[] { cbTransferringFundAdm, cbFundTypeAdm, dpTransferringDateAdm, txtTransferringAmountAdm, cbTransferedToAdm, cbTransferedToFundType });
            transferingAmount = 0;
        }

        private void dgTransferencesPrivateBinding()
        {
            dgTransferencesPrivate.ItemsSource = transfers.OrderByDescending(t => t.TransferDate).ToList();
            lv.AutoSizeColumns(dgTransferencesPrivate.View);
        }

        private void dgTransferencesAdmBinding()
        {
            dgTransferencesAdm.ItemsSource = transfers.OrderByDescending(t => t.TransferDate).ToList();
            lv.AutoSizeColumns(dgTransferencesAdm.View);
        }

        private void txtTransferringAmountAdm_TextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox txtBox = (TextBox)sender;
            txtBox.ClearValue(BorderBrushProperty);
            if (txtBox.Text != "")
            {
                try
                {
                    transferingAmount = validations.ConvertStringToDecimal(txtBox.Text);
                }
                catch (Exception ex)
                {
                    txtBox.BorderBrush = System.Windows.Media.Brushes.Red;
                    System.Windows.Forms.MessageBox.Show(ex.Message);
                    txtBox.Text = "";
                }
            }
            //txtTransferringAmountAdm.ClearValue(BorderBrushProperty);
            //if (txtTransferringAmountAdm.Text != "")
            //{
            //    try
            //    {
            //        transferingAmount = validations.ConvertStringToDecimal(txtTransferringAmountAdm.Text);
            //    }
            //    catch (Exception ex)
            //    {
            //        txtTransferringAmountAdm.BorderBrush = System.Windows.Media.Brushes.Red;
            //        System.Windows.Forms.MessageBox.Show(ex.Message);
            //        txtTransferringAmountAdm.Text = "0";
            //    }
            //}
        }

        private void dgTransferencesAdm_MouseDown(object sender, MouseButtonEventArgs e)
        {
            transferSelected = null;
            dgTransferencesAdm.UnselectAll();
            ClearInputs(new object[] { cbTransferringFundAdm, cbFundTypeAdm, dpTransferringDateAdm, txtTransferringAmountAdm, cbTransferedToAdm, cbTransferedToFundType });
            txtBtnAddTransferAdm.Text = "הוסף העברה";
        }

        private void dgTransferencesAdm_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (dgTransferencesAdm.SelectedItem != null)
            {
                txtBtnAddTransferAdm.Text = "עדכן העברה";
                transferSelected = (LifePolicyTransfer)dgTransferencesAdm.SelectedItem;
                var funds = cbTransferringFundAdm.Items;
                foreach (var item in funds)
                {
                    InsuranceCompany fund = (InsuranceCompany)item;
                    if (fund.CompanyID == transferSelected.TransferringCompanyID)
                    {
                        cbTransferringFundAdm.SelectedItem = item;
                    }
                    if (fund.CompanyID == transferSelected.TransferedToCompanyID)
                    {
                        cbTransferedToAdm.SelectedItem = item;
                    }
                }
                var fundTypes = cbFundTypeAdm.Items;
                foreach (var item in fundTypes)
                {
                    FundType type = (FundType)item;
                    if (type.FundTypeID == transferSelected.FundTypeID)
                    {
                        cbFundTypeAdm.SelectedItem = item;
                        break;
                    }
                }
                var transferedToFundTypes = cbTransferedToFundType.Items;
                foreach (var item in transferedToFundTypes)
                {
                    FundType type = (FundType)item;
                    if (type.FundTypeID == transferSelected.TransferedToFundTypeID)
                    {
                        cbTransferedToFundType.SelectedItem = item;
                        break;
                    }
                }
                dpTransferringDateAdm.SelectedDate = transferSelected.TransferDate;
                txtTransferringAmountAdm.Text = validations.ConvertDecimalToString(transferSelected.TransferAmount);
            }
        }

        private void btnDeleteTransferenceAdm_Click(object sender, RoutedEventArgs e)
        {
            if (dgTransferencesAdm.SelectedItem != null)
            {
                if (MessageBox.Show("למחוק העברה", "אישור", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    //salaryChanges.Remove((ManagerPolicyChanx)dgAdmPolicySalaryChanges.SelectedItem);
                    transfers.Remove((LifePolicyTransfer)dgTransferencesAdm.SelectedItem);
                    dgTransferencesAdmBinding();
                    //dgAdmPolicySalaryChangesBinding();
                    ClearInputs(new object[] { cbTransferringFundAdm, cbFundTypeAdm, dpTransferringDateAdm, txtTransferringAmountAdm, cbTransferedToAdm, cbTransferedToFundType });
                    //txtBtnAddSalaryChange.Text = "הוסף שינוי משכורת";
                    txtBtnAddTransferAdm.Text = "הוסף העברה";
                }
            }
            else
            {
                System.Windows.Forms.MessageBox.Show("נא סמן את ההעברה שברצונך לבטל");
            }
        }

        private void chbIsRisksAdm_Checked(object sender, RoutedEventArgs e)
        {
            spRisksAdm.IsEnabled = true;
            //spRisksAdm.Visibility = Visibility.Visible;
        }

        private void chbIsRisksAdm_Unchecked(object sender, RoutedEventArgs e)
        {
            spRisksAdm.IsEnabled = false;
            //spRisksAdm.Visibility = Visibility.Collapsed;
        }

        private void txtDeathAmountAdm_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (sender is TextBox)
            {
                TextBox txtBox = (TextBox)sender;
                txtBox.ClearValue(BorderBrushProperty);
                if (txtBox.Text != "")
                {
                    try
                    {
                        decimal reusult = validations.ConvertStringToDecimal(txtBox.Text);
                    }
                    catch (Exception ex)
                    {
                        txtBox.BorderBrush = System.Windows.Media.Brushes.Red;
                        System.Windows.Forms.MessageBox.Show(ex.Message);
                        txtBox.Text = "";
                    }
                }
            }
        }

        private void btnAddInsurencedRisk_Click(object sender, RoutedEventArgs e)
        {
            if (additionalsInsureds.Count < 4)
            {
                if (txtAdditionalInsurencedIdRisk.Text == "" || txtAdditionalInsurencedFirstNameRisk.Text == "" || txtAdditionalInsurencedLastNameRisk.Text == "")
                {
                    if (sender is Window)
                    {
                        MessageBox.Show("שים לב, פרטי מבוטח נוסף לא עודכנו במערכת", "מידע", MessageBoxButton.OK, MessageBoxImage.Information);
                        return;
                    }
                    System.Windows.Forms.MessageBox.Show("שם פרטי, משפחה ות.ז. הינן שדות חובה");

                    return;
                }
                string relationship = null;
                if (cbRelationshipRisk.SelectedItem != null)
                {
                    relationship = ((ComboBoxItem)cbRelationshipRisk.SelectedItem).Content.ToString();
                }
                LifeAdditionalInsured newAdditional = new LifeAdditionalInsured() { FirstName = txtAdditionalInsurencedFirstNameRisk.Text, LastName = txtAdditionalInsurencedLastNameRisk.Text, IdNumber = txtAdditionalInsurencedIdRisk.Text, BirthDate = dpAdditionalInsurencedBirthDateRisk.SelectedDate, RelationshipWithPrincipalInsured = relationship };
                additionalsInsureds.Add(newAdditional);
                //additionalsCount++;
                ((TabItem)tbControlRisks.Items[additionalsInsureds.Count]).Visibility = Visibility.Visible;
                ((TabItem)tbControlRisks.Items[additionalsInsureds.Count]).Header = string.Format("{0} {1}", newAdditional.FirstName, newAdditional.LastName);
                //AddTabItem();
                ClearInputs(new object[] { txtAdditionalInsurencedFirstNameRisk, txtAdditionalInsurencedLastNameRisk, txtAdditionalInsurencedIdRisk, dpAdditionalInsurencedBirthDateRisk, cbRelationshipRisk });
            }
            else
            {
                System.Windows.Forms.MessageBox.Show("לא ניתן להוסיף עוד מבוטחים");
            }
        }

        //private TabItem AddTabItem(string name)
        //{
        //    //int count = tbControlRisks.Items.Count;
        //    //TabItem tab2 = TrycloneElement(tbItemInsurencedRisk1);
        //    //if (tab2 != null)
        //    //{
        //    //    tbControlRisks.Items.Add(tab2);
        //    //}
        //    //// create new tab item
        //    ////TabItem tab = new TabItem();
        //    //if (name == "")
        //    //{
        //    //    tab2.Header = string.Format("מבוטח {0}", count + 1);
        //    //}
        //    //else
        //    //{
        //    //    tab2.Header = name;
        //    //}
        //    //tab2.Name = string.Format("tbItemInsurencedRisk{0}", count + 1);


        //    string xml = "<StackPanel Orientation='Vertical'>" +
        //                            "<StackPanel Orientation='Horizontal'>" +
        //                                "<Label Foreground='White' FontWeight='Normal' Width='212' Margin='0,0,20,0'></Label>" +
        //                                "<Label x:Name='lblAmount' Foreground='White' FontWeight='Bold' Width='100' HorizontalContentAlignment='Center' Margin='0,0,20,0'>סכום (ש'ח)</Label>";



        //                        //        <Label x:Name="lblPeriod" FontWeight="Bold" Foreground="White" Margin="0,0,20,0" Width="100" HorizontalContentAlignment="Center">תקופה</Label>
        //                        //        <Label x:Name="lblCoverageType" Foreground="White" FontWeight="Bold" Margin="0,0,20,0" HorizontalContentAlignment="Center" Width="100">סוג כיסוי</Label>
        //                        //        <Label x:Name="lblPremium" Foreground="White" FontWeight="Bold" Margin="0,0,20,0" HorizontalContentAlignment="Center" Width="100">פרמיה (ש"ח)</Label>
        //                        //        <Label x:Name="lblComments" Foreground="White" FontWeight="Bold" HorizontalContentAlignment="Center" Width="520">הערות</Label>
        //                        //    </StackPanel>
        //                        //    <StackPanel Orientation="Horizontal">
        //                        //        <StackPanel Orientation="Vertical" Margin="0,0,20,0">

        //                        //            <StackPanel Orientation="Horizontal" Margin="0,0,0,20">
        //                        //                <Label x:Name="lblDeath" Foreground="White" FontWeight="Normal" Height="26" Margin="0,0,5,0">מקרה מוות</Label>
        //                        //                <RadioButton x:Name="rbDeath1Risk" Foreground="White" FontWeight="Normal" Margin="0,6,5,0">1</RadioButton>
        //                        //                <RadioButton x:Name="rbDeath5Risk" Foreground="White" FontWeight="Normal" Margin="0,6,5,0">5</RadioButton>
        //                        //                <RadioButton x:Name="rbDeathFixedRisk" Foreground="White" FontWeight="Normal" Margin="0,6,0,0">קבוע</RadioButton>
        //                        //            </StackPanel>
        //                        //            <StackPanel Orientation="Horizontal" Margin="0,0,0,20">
        //                        //                <Label x:Name="lblWorkImpossibility" FontWeight="Normal" Foreground="White" Height="26">אובדן כושר עבודה</Label>
        //                        //                <RadioButton x:Name="rbProfessionalWorkImposibilitationRisk" Foreground="White" FontWeight="Normal" Margin="0,6,5,0">מקצועי</RadioButton>
        //                        //                <RadioButton x:Name="rbNormalWorkImposibilitationRisk" Foreground="White" FontWeight="Normal" Margin="0,6,5,0">רגיל</RadioButton>
        //                        //            </StackPanel>
        //                        //            <Label x:Name="lblSecureIncome" Foreground="White" FontWeight="Normal" Height="26" Margin="0,0,0,19">הכנסה בטוחה למשפחה (חדשי)</Label>
        //                        //            <Label x:Name="lblDisabilityByAccident" Foreground="White" FontWeight="Normal" Height="26" Margin="0,0,0,19">נכות מתאונה</Label>
        //                        //            <Label x:Name="lblDeathByAccident" Foreground="White" FontWeight="Normal" Height="26" Margin="0,0,0,19">מוות מתאונה</Label>
        //                        //            <Label x:Name="lblCriticalIllness" FontWeight="Normal" Foreground="White" Height="26" Margin="0,0,0,17">מחלות קשות</Label>
        //                        //            <StackPanel Orientation="Horizontal" Margin="0,0,0,15">
        //                        //                <Label x:Name="lblOther1" Foreground="White" FontWeight="Normal">אחר</Label>
        //                        //                <TextBox x:Name="txtOther1" FontWeight="Normal" Height="30" Width="175"></TextBox>
        //                        //            </StackPanel>
        //                        //            <StackPanel Orientation="Horizontal" Margin="0,0,0,15">
        //                        //                <Label x:Name="lblOther2" Foreground="White" FontWeight="Normal">אחר</Label>
        //                        //                <TextBox x:Name="txtOther2" FontWeight="Normal" Height="30" Width="175"></TextBox>
        //                        //            </StackPanel>
        //                        //            <StackPanel Orientation="Horizontal" Margin="0,0,0,15">
        //                        //                <Label x:Name="lblOther3" Foreground="White" FontWeight="Normal">אחר</Label>
        //                        //                <TextBox x:Name="txtOther3" FontWeight="Normal" Height="30" Width="175"></TextBox>
        //                        //            </StackPanel>
        //                        //            <StackPanel Orientation="Horizontal" Margin="0,0,0,15">
        //                        //                <Label x:Name="lblOther4" Foreground="White" FontWeight="Normal">אחר</Label>
        //                        //                <TextBox x:Name="txtOther4" FontWeight="Normal" Height="30" Width="175"></TextBox>
        //                        //            </StackPanel>
        //                        //            <StackPanel Orientation="Horizontal">
        //                        //                <Label x:Name="lblOther5" Foreground="White" FontWeight="Normal">אחר</Label>
        //                        //                <TextBox x:Name="txtOther5" FontWeight="Normal" Height="30" Width="175"></TextBox>
        //                        //            </StackPanel>
        //                        //        </StackPanel>
        //                        //        <StackPanel Orientation="Vertical" Margin="0,0,10,0">
        //                        //            <TextBox x:Name="txtDeathAmountRisk" FontWeight="Normal" Height="30" Width="100" Margin="0,0,10,15"></TextBox>
        //                        //            <TextBox x:Name="txtWorkImpossibilityAmountRisk" FontWeight="Normal" Height="30" Width="100" Margin="0,0,10,15"></TextBox>
        //                        //            <TextBox x:Name="txtIncomeSecureAmountRisk" FontWeight="Normal" Height="30" Width="100" Margin="0,0,10,15"></TextBox>
        //                        //            <TextBox x:Name="txtDesabilityByAccidentAmountRisk" FontWeight="Normal" Height="30" Width="100" Margin="0,0,10,15"></TextBox>
        //                        //            <TextBox x:Name="txtDeathByAccidentAmountRisk" FontWeight="Normal" Height="30" Width="100" Margin="0,0,10,15"></TextBox>
        //                        //            <TextBox x:Name="txtCriticalIllnesesAmountRisk" FontWeight="Normal" Height="30" Width="100" Margin="0,0,10,15"></TextBox>
        //                        //            <TextBox x:Name="txtOther1AmountRisk" FontWeight="Normal" Height="30" Width="100" Margin="0,0,10,15"></TextBox>
        //                        //            <TextBox x:Name="txtOther2AmountRisk" FontWeight="Normal" Height="30" Width="100" Margin="0,0,10,15"></TextBox>
        //                        //            <TextBox x:Name="txtOther3AmountRisk" FontWeight="Normal" Height="30" Width="100" Margin="0,0,10,15"></TextBox>
        //                        //            <TextBox x:Name="txtOther4AmountRisk" FontWeight="Normal" Height="30" Width="100" Margin="0,0,10,15"></TextBox>
        //                        //            <TextBox x:Name="txtOther5AmountRisk" FontWeight="Normal" Height="30" Width="100" Margin="0,0,10,15"></TextBox>
        //                        //        </StackPanel>
        //                        //        <StackPanel Orientation="Vertical" Margin="0,0,10,0">
        //                        //            <TextBox x:Name="txtDeathPeriodRisk" FontWeight="Normal" Height="30" Width="100" Margin="0,0,10,15"></TextBox>
        //                        //            <TextBox x:Name="txtWorkImpossibilityPeriodRisk" FontWeight="Normal" Height="30" Width="100" Margin="0,0,10,15"></TextBox>
        //                        //            <TextBox x:Name="txtIncomeSecurePeriodRisk" FontWeight="Normal" Height="30" Width="100" Margin="0,0,10,15"></TextBox>
        //                        //            <TextBox x:Name="txtDesabilityByAccidentPeriodRisk" FontWeight="Normal" Height="30" Width="100" Margin="0,0,10,15"></TextBox>
        //                        //            <TextBox x:Name="txtDeathByAccidentPeriodRisk" FontWeight="Normal" Height="30" Width="100" Margin="0,0,10,15"></TextBox>
        //                        //            <TextBox x:Name="txtCriticalIllnesesPeriodRisk" FontWeight="Normal" Height="30" Width="100" Margin="0,0,10,15"></TextBox>
        //                        //            <TextBox x:Name="txtOther1PeriodRisk" FontWeight="Normal" Height="30" Width="100" Margin="0,0,10,15"></TextBox>
        //                        //            <TextBox x:Name="txtOther2PeriodRisk" FontWeight="Normal" Height="30" Width="100" Margin="0,0,10,15"></TextBox>
        //                        //            <TextBox x:Name="txtOther3PeriodRisk" FontWeight="Normal" Height="30" Width="100" Margin="0,0,10,15"></TextBox>
        //                        //            <TextBox x:Name="txtOther4PeriodRisk" FontWeight="Normal" Height="30" Width="100" Margin="0,0,10,15"></TextBox>
        //                        //            <TextBox x:Name="txtOther5PeriodRisk" FontWeight="Normal" Height="30" Width="100" Margin="0,0,10,15"></TextBox>
        //                        //        </StackPanel>
        //                        //        <StackPanel Orientation="Vertical" Margin="0,0,10,0">
        //                        //            <TextBox x:Name="txtDeathCoverageTypeRisk" FontWeight="Normal" Height="30" Width="100" Margin="0,0,10,15"></TextBox>
        //                        //            <TextBox x:Name="txtWorkImpossibilityCoverageTypeRisk" FontWeight="Normal" Height="30" Width="100" Margin="0,0,10,15"></TextBox>
        //                        //            <TextBox x:Name="txtIncomeSecureCoverageTypeRisk" FontWeight="Normal" Height="30" Width="100" Margin="0,0,10,15"></TextBox>
        //                        //            <TextBox x:Name="txtDesabilityByAccidentCoverageTypeRisk" FontWeight="Normal" Height="30" Width="100" Margin="0,0,10,15"></TextBox>
        //                        //            <TextBox x:Name="txtDeathByAccidentCoverageTypeRisk" FontWeight="Normal" Height="30" Width="100" Margin="0,0,10,15"></TextBox>
        //                        //            <TextBox x:Name="txtCriticalIllnesesCoverageTypeRisk" FontWeight="Normal" Height="30" Width="100" Margin="0,0,10,15"></TextBox>
        //                        //            <TextBox x:Name="txtOther1CoverageTypeRisk" FontWeight="Normal" Height="30" Width="100" Margin="0,0,10,15"></TextBox>
        //                        //            <TextBox x:Name="txtOther2CoverageTypeRisk" FontWeight="Normal" Height="30" Width="100" Margin="0,0,10,15"></TextBox>
        //                        //            <TextBox x:Name="txtOther3CoverageTypeRisk" FontWeight="Normal" Height="30" Width="100" Margin="0,0,10,15"></TextBox>
        //                        //            <TextBox x:Name="txtOther4CoverageTypeRisk" FontWeight="Normal" Height="30" Width="100" Margin="0,0,10,15"></TextBox>
        //                        //            <TextBox x:Name="txtOther5CoverageTypeRisk" FontWeight="Normal" Height="30" Width="100" Margin="0,0,10,15"></TextBox>
        //                        //        </StackPanel>
        //                        //        <StackPanel Orientation="Vertical" Margin="0,0,10,0">
        //                        //            <TextBox x:Name="txtDeathPremiumRisk" FontWeight="Normal" Height="30" Width="100" Margin="0,0,10,15"></TextBox>
        //                        //            <TextBox x:Name="txtWorkImpossibilityPremiumRisk" FontWeight="Normal" Height="30" Width="100" Margin="0,0,10,15"></TextBox>
        //                        //            <TextBox x:Name="txtIncomeSecurePremiumRisk" FontWeight="Normal" Height="30" Width="100" Margin="0,0,10,15"></TextBox>
        //                        //            <TextBox x:Name="txtDesabilityByAccidentPremiumRisk" FontWeight="Normal" Height="30" Width="100" Margin="0,0,10,15"></TextBox>
        //                        //            <TextBox x:Name="txtDeathByAccidentPremiumRisk" FontWeight="Normal" Height="30" Width="100" Margin="0,0,10,15"></TextBox>
        //                        //            <TextBox x:Name="txtCriticalIllnesesPremiumRisk" FontWeight="Normal" Height="30" Width="100" Margin="0,0,10,15"></TextBox>
        //                        //            <TextBox x:Name="txtOther1PremiumRisk" FontWeight="Normal" Height="30" Width="100" Margin="0,0,10,15"></TextBox>
        //                        //            <TextBox x:Name="txtOther2PremiumRisk" FontWeight="Normal" Height="30" Width="100" Margin="0,0,10,15"></TextBox>
        //                        //            <TextBox x:Name="txtOther3PremiumRisk" FontWeight="Normal" Height="30" Width="100" Margin="0,0,10,15"></TextBox>
        //                        //            <TextBox x:Name="txtOther4PremiumRisk" FontWeight="Normal" Height="30" Width="100" Margin="0,0,10,15"></TextBox>
        //                        //            <TextBox x:Name="txtOther5PremiumRisk" FontWeight="Normal" Height="30" Width="100" Margin="0,0,10,15"></TextBox>
        //                        //        </StackPanel>
        //                        //        <StackPanel Orientation="Vertical">
        //                        //            <TextBox x:Name="txtDeathCommentsRisk" FontWeight="Normal" Height="30" Width="520" Margin="0,0,10,15"></TextBox>
        //                        //            <TextBox x:Name="txtWorkImpossibilityCommentsRisk" FontWeight="Normal" Height="30" Width="520" Margin="0,0,10,15"></TextBox>
        //                        //            <TextBox x:Name="txtIncomeSecureCommentsRisk" FontWeight="Normal" Height="30" Width="520" Margin="0,0,10,15"></TextBox>
        //                        //            <TextBox x:Name="txtDesabilityByAccidentCommentsRisk" FontWeight="Normal" Height="30" Width="520" Margin="0,0,10,15"></TextBox>
        //                        //            <TextBox x:Name="txtDeathByAccidentCommentsRisk" FontWeight="Normal" Height="30" Width="520" Margin="0,0,10,15"></TextBox>
        //                        //            <TextBox x:Name="txtCriticalIllnesesCommentsRisk" FontWeight="Normal" Height="30" Width="520" Margin="0,0,10,15"></TextBox>
        //                        //            <TextBox x:Name="txtOther1CommentsRisk" FontWeight="Normal" Height="30" Width="520" Margin="0,0,10,15"></TextBox>
        //                        //            <TextBox x:Name="txtOther2CommentsRisk" FontWeight="Normal" Height="30" Width="520" Margin="0,0,10,15"></TextBox>
        //                        //            <TextBox x:Name="txtOther3CommentsRisk" FontWeight="Normal" Height="30" Width="520" Margin="0,0,10,15"></TextBox>
        //                        //            <TextBox x:Name="txtOther4CommentsRisk" FontWeight="Normal" Height="30" Width="520" Margin="0,0,10,15"></TextBox>
        //                        //            <TextBox x:Name="txtOther5CommentsRisk" FontWeight="Normal" Height="30" Width="520" Margin="0,0,10,15"></TextBox>
        //                        //        </StackPanel>
        //                        //    </StackPanel>
        //                        //</StackPanel>




        //    //tab.HeaderTemplate = tabDynamic.FindResource("TabHeader") as DataTemplate;

        //    // add controls to tab item, this case I added just a textbox
        //    //TextBox txt = new TextBox();
        //    //txt.Name = "txt";
        //    //tab.Content = txt;

        //    //_tabItems.Insert(count - 1, tab);
        //    //return tab2;
        //}

        //public static T TrycloneElement<T>(T orig)
        //{
        //    try
        //    {
        //        string s = XamlWriter.Save(orig);

        //        StringReader stringReader = new StringReader(s);

        //        XmlReader xmlReader = XmlTextReader.Create(stringReader, new XmlReaderSettings());
        //        XmlReaderSettings sx = new XmlReaderSettings();

        //        object x = XamlReader.Load(xmlReader);
        //        return (T)x;
        //    }
        //    catch
        //    {
        //        return (T)((object)null);
        //    }

        //}

        private void txtInsurancedName_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (txtInsurancedName.Text != "")
            {
                tbItemInsurencedRisk1.Header = txtInsurancedName.Text;
                tbItemInsurenced1Private.Header = txtInsurancedName.Text;
            }
            else
            {
                tbItemInsurencedRisk1.Header = "מבוטח ראשי";
                tbItemInsurenced1Private.Header = "מבוטח ראשי";
            }
        }

        private void chbIsAdditionalInsuredMortgage_Checked(object sender, RoutedEventArgs e)
        {
            spAdditionalInsuredMortgage.IsEnabled = true;
            rbIsSecundaryInsuredPolicy.IsEnabled = true;
            btnCopyMortgage.IsEnabled = true;
        }

        private void chbIsAdditionalInsuredMortgage_Unchecked(object sender, RoutedEventArgs e)
        {
            spAdditionalInsuredMortgage.IsEnabled = false;
            rbIsSecundaryInsuredPolicy.IsEnabled = false;
            btnCopyMortgage.IsEnabled = false;

        }

        private void btnAddMortgage_Click(object sender, RoutedEventArgs e)
        {
            if (dpMorgageStartDate.SelectedDate > dpMorgageEndDate.SelectedDate)
            {
                if (sender is Window)
                {
                    MessageBox.Show("שים לב, פרטי ההלוואה לא עודכנו במערכת", "מידע", MessageBoxButton.OK, MessageBoxImage.Information);
                    return;
                }
                MessageBox.Show("תאריך סיום חייב להיות גדול מתאריך תחילה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);

                return;
            }
            decimal? amount = null;
            if (txtMortgageAmount.Text != "")
            {
                amount = Decimal.Parse(txtMortgageAmount.Text);
            }
            decimal? interest = null;
            if (txtAnualInterest.Text != "")
            {
                interest = Decimal.Parse(txtAnualInterest.Text);
            }
            decimal? premium = null;
            if (txtMortgagePremium.Text != "")
            {
                premium = Decimal.Parse(txtMortgagePremium.Text);
            }
            int? mortgagePeriod = null;
            if (txtMortgagePeriod.Text != "")
            {
                mortgagePeriod = int.Parse(txtMortgagePeriod.Text);
            }
            int? premiumPeriod = null;
            if (txtPremiumPeriod.Text != "")
            {
                premiumPeriod = int.Parse(txtPremiumPeriod.Text);
            }
            if (txtBtnAddMortgage.Text.Contains("הוסף"))
            {

                Mortgage newMortgage = new Mortgage()
                {
                    MortgageNumber = txtMortgageNumber.Text,
                    MortgageAmount = amount,
                    AnualInterest = interest,
                    HatzmadaType = cbIndexationTypeMortgage.Text,
                    StartDate = dpMorgageStartDate.SelectedDate,
                    EndDate = dpMorgageEndDate.SelectedDate,
                    Premium = premium,
                    MortgagePeriod = mortgagePeriod,
                    PremiumPeriod = premiumPeriod,
                    IsPrincipalInsuredPolicy = rbIsPrincipalInsuredPolicy.IsChecked
                };
                mortgages.Add(newMortgage);
            }
            else
            {
                if (mortgageSelected != null)
                {

                    mortgageSelected.MortgageNumber = txtMortgageNumber.Text;
                    mortgageSelected.MortgageAmount = amount;
                    mortgageSelected.AnualInterest = interest;
                    mortgageSelected.HatzmadaType = cbIndexationTypeMortgage.Text;
                    mortgageSelected.StartDate = dpMorgageStartDate.SelectedDate;
                    mortgageSelected.EndDate = dpMorgageEndDate.SelectedDate;
                    mortgageSelected.Premium = premium;
                    mortgageSelected.MortgagePeriod = mortgagePeriod;
                    mortgageSelected.PremiumPeriod = premiumPeriod;
                    mortgageSelected.IsPrincipalInsuredPolicy = rbIsPrincipalInsuredPolicy.IsChecked;
                }
                txtBtnAddMortgage.Text = "הוסף הלוואה";
            }
            dgMortgagesBinding();
            ClearInputs(new object[] { txtMortgageNumber, txtMortgageAmount, txtAnualInterest, cbIndexationTypeMortgage, dpMorgageStartDate, dpMorgageEndDate, txtMortgagePremium, txtMortgagePeriod, txtPremiumPeriod });
            rbIsPrincipalInsuredPolicy.IsChecked = true;
            dgMortgages.UnselectAll();
        }

        private void dgMortgagesBinding()
        {
            dgMortgages.ItemsSource = mortgages.ToList();
            lv.AutoSizeColumns(dgMortgages.View);
        }

        private void txtMortgagePeriod_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (sender is TextBox)
            {
                TextBox txtBox = (TextBox)sender;
                txtBox.ClearValue(BorderBrushProperty);
                if (txtBox.Text != "")
                {
                    try
                    {
                        int reusult = validations.ConvertStringToInt(txtBox.Text);
                    }
                    catch (Exception ex)
                    {
                        txtBox.BorderBrush = System.Windows.Media.Brushes.Red;
                        System.Windows.Forms.MessageBox.Show(ex.Message);
                        txtBox.Text = "";
                    }
                }
            }
        }

        private void dgMortgages_MouseDown(object sender, MouseButtonEventArgs e)
        {
            mortgageSelected = null;
            dgMortgages.UnselectAll();
            ClearInputs(new object[] { txtMortgageNumber, txtMortgageAmount, txtAnualInterest, cbIndexationTypeMortgage, dpMorgageStartDate, dpMorgageEndDate, txtMortgagePremium, txtMortgagePeriod, txtPremiumPeriod });
            rbIsPrincipalInsuredPolicy.IsChecked = true;
            txtBtnAddMortgage.Text = "הוסף הלוואה";
        }

        private void dgMortgages_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (dgMortgages.SelectedItem != null)
            {
                mortgageSelected = (Mortgage)dgMortgages.SelectedItem;
                txtMortgageNumber.Text = mortgageSelected.MortgageNumber;
                txtMortgageAmount.Text = validations.ConvertDecimalToString(mortgageSelected.MortgageAmount);
                txtAnualInterest.Text = validations.ConvertDecimalToString(mortgageSelected.AnualInterest);
                cbIndexationTypeMortgage.Text = mortgageSelected.HatzmadaType;
                dpMorgageStartDate.SelectedDate = mortgageSelected.StartDate;
                dpMorgageEndDate.SelectedDate = mortgageSelected.EndDate;
                txtMortgagePremium.Text = validations.ConvertDecimalToString(mortgageSelected.Premium);
                txtMortgagePeriod.Text = mortgageSelected.MortgagePeriod.ToString();
                txtPremiumPeriod.Text = mortgageSelected.PremiumPeriod.ToString();
                if (mortgageSelected.IsPrincipalInsuredPolicy == true)
                {
                    rbIsPrincipalInsuredPolicy.IsChecked = true;
                }
                else
                {
                    rbIsSecundaryInsuredPolicy.IsChecked = true;
                }
                txtBtnAddMortgage.Text = "עדכן הלוואה";
            }
        }

        private void btnDeleteMortgage_Click(object sender, RoutedEventArgs e)
        {
            if (dgMortgages.SelectedItem != null)
            {
                if (MessageBox.Show("למחוק הלוואה", "אישור", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    mortgages.Remove((Mortgage)dgMortgages.SelectedItem);
                    dgMortgagesBinding();
                    ClearInputs(new object[] { txtMortgageNumber, txtMortgageAmount, txtAnualInterest, cbIndexationTypeMortgage, dpMorgageStartDate, dpMorgageEndDate, txtMortgagePremium, txtMortgagePeriod, txtPremiumPeriod });
                    rbIsPrincipalInsuredPolicy.IsChecked = true;
                    txtBtnAddMortgage.Text = "הוסף הלוואה";
                }
            }
            else
            {
                System.Windows.Forms.MessageBox.Show("נא סמן את ההלוואה שברצונך לבטל");
            }
        }

        private void btnCopyMortgage_Click(object sender, RoutedEventArgs e)
        {
            if (dgMortgages.SelectedItem != null)
            {
                Mortgage selectedMortgage = (Mortgage)dgMortgages.SelectedItem;
                decimal? amount = null;
                if (selectedMortgage.MortgageAmount != null)
                {
                    amount = selectedMortgage.MortgageAmount;
                }
                decimal? interest = null;
                if (selectedMortgage.AnualInterest != null)
                {
                    interest = selectedMortgage.AnualInterest;
                }
                decimal? premium = null;
                if (selectedMortgage.Premium != null)
                {
                    premium = selectedMortgage.Premium;
                }
                int? mortgagePeriod = null;
                if (selectedMortgage.MortgagePeriod != null)
                {
                    mortgagePeriod = selectedMortgage.MortgagePeriod;
                }
                int? premiumPeriod = null;
                if (selectedMortgage.PremiumPeriod != null)
                {
                    premiumPeriod = selectedMortgage.PremiumPeriod;
                }

                Mortgage mortgageCopy = new Mortgage()
                {
                    MortgageNumber = txtMortgageNumber.Text,
                    MortgageAmount = amount,
                    AnualInterest = interest,
                    HatzmadaType = cbIndexationTypeMortgage.Text,
                    StartDate = dpMorgageStartDate.SelectedDate,
                    EndDate = dpMorgageEndDate.SelectedDate,
                    Premium = premium,
                    MortgagePeriod = mortgagePeriod,
                    PremiumPeriod = premiumPeriod,
                    IsPrincipalInsuredPolicy = !rbIsPrincipalInsuredPolicy.IsChecked
                };

                mortgages.Add(mortgageCopy);
                dgMortgagesBinding();
                ClearInputs(new object[] { txtMortgageNumber, txtMortgageAmount, txtAnualInterest, cbIndexationTypeMortgage, dpMorgageStartDate, dpMorgageEndDate, txtMortgagePremium, txtMortgagePeriod, txtPremiumPeriod });
                rbIsPrincipalInsuredPolicy.IsChecked = true;
                txtBtnAddMortgage.Text = "הוסף הלוואה";
            }
        }

        private void btnUpdateInsuranceTypeTable_Click(object sender, RoutedEventArgs e)
        {
            int? selectedItemId = null;
            if (cbInsuranceType.SelectedItem != null)
            {
                selectedItemId = ((FundType)cbInsuranceType.SelectedItem).FundTypeID;
            }
            int insuranceID = insuranceLogic.GetInsuranceID("חיים");
            FundType fundType = new FundType() { InsuranceID = insuranceID, LifeIndustryID = ((LifeIndustry)cbLifeIndustry.SelectedItem).LifeIndustryID, CompanyID = ((InsuranceCompany)cbCompany.SelectedItem).CompanyID };
            frmUpdateTable updateFundTypesTable = new frmUpdateTable(fundType);
            updateFundTypesTable.ShowDialog();
            cbInsuranceTypeBinding();
            TransferringFundsTypeAdmBinding();
            TransferedFundsTypeAdmBinding();
            if (updateFundTypesTable.ItemAdded != null)
            {
                SelectFundItemInCb(((FundType)updateFundTypesTable.ItemAdded).FundTypeID, cbInsuranceType);
            }
            else if (selectedItemId != null)
            {
                SelectFundItemInCb((int)selectedItemId, cbInsuranceType);
            }
            //SelectCbItem(cbInsuranceCompany,);
        }

        private void SelectFundItemInCb(int id, ComboBox cb)
        {
            var items = cb.Items;
            foreach (var item in items)
            {
                FundType parsedItem = (FundType)item;
                if (parsedItem.FundTypeID == id)
                {
                    cb.SelectedItem = item;
                    break;
                }
            }
        }

        private void btnUpdateTransferedToFundTypeTableAdm_Click(object sender, RoutedEventArgs e)
        {
            int? selectedItemId = null;
            if (cbTransferedToFundType.SelectedItem != null)
            {
                selectedItemId = ((FundType)cbTransferedToFundType.SelectedItem).FundTypeID;
            }
            int insuranceID = insuranceLogic.GetInsuranceID("חיים");
            FundType fundType = new FundType() { InsuranceID = insuranceID, LifeIndustryID = ((LifeIndustry)cbLifeIndustry.SelectedItem).LifeIndustryID, CompanyID = ((InsuranceCompany)cbTransferedToAdm.SelectedItem).CompanyID };
            frmUpdateTable updateFundTypesTable = new frmUpdateTable(fundType);
            updateFundTypesTable.ShowDialog();
            TransferringFundsTypeAdmBinding();
            TransferedFundsTypeAdmBinding();
            if (updateFundTypesTable.ItemAdded != null)
            {
                SelectFundItemInCb(((FundType)updateFundTypesTable.ItemAdded).FundTypeID, cbTransferedToFundType);
            }
            else if (selectedItemId != null)
            {
                SelectFundItemInCb((int)selectedItemId, cbTransferedToFundType);
            }
        }

        private void btnAddTransferencePrivate_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                int? transferringCompanyId = null;
                Company transferringCompany = null;
                if (cbTransferringFundPrivate.SelectedItem != null)
                {
                    transferringCompanyId = ((InsuranceCompany)cbTransferringFundPrivate.SelectedItem).CompanyID;
                    transferringCompany = ((InsuranceCompany)cbTransferringFundPrivate.SelectedItem).Company;
                }
                int? transferringFundTypeId = null;
                FundType transferrinfFundType = null;
                if (cbTransferringFundTypePrivate.SelectedItem != null)
                {
                    transferringFundTypeId = ((FundType)cbTransferringFundTypePrivate.SelectedItem).FundTypeID;
                    transferrinfFundType = (FundType)cbTransferringFundTypePrivate.SelectedItem;
                }
                int? transferedToCompanyId = null;
                Company transferedToCompany = null;
                if (cbTransferedToPrivate.SelectedItem != null)
                {
                    transferedToCompanyId = ((InsuranceCompany)cbTransferedToPrivate.SelectedItem).CompanyID;
                    transferedToCompany = ((InsuranceCompany)cbTransferedToPrivate.SelectedItem).Company;
                }
                int? transferedToFundTypeId = null;
                FundType transferedToFundType = null;
                if (cbTransferedToFundTypePrivate.SelectedItem != null)
                {
                    transferedToFundTypeId = ((FundType)cbTransferedToFundTypePrivate.SelectedItem).FundTypeID;
                    transferedToFundType = (FundType)cbTransferedToFundTypePrivate.SelectedItem;
                }
                if (txtBtnAddTransferPrivate.Text.Contains("הוסף"))
                {
                    transferSelected = null;
                    LifePolicyTransfer newTransfer = new LifePolicyTransfer() { TransferringCompanyID = transferringCompanyId, FundTypeID = transferringFundTypeId, TransferDate = dpTransferringDatePrivate.SelectedDate, TransferAmount = transferingAmount, TransferedToCompanyID = transferedToCompanyId, TransferedToFundTypeID = transferedToFundTypeId, Company = transferringCompany, FundType = transferrinfFundType, Company1 = transferedToCompany, FundType1 = transferedToFundType };
                    transfers.Add(newTransfer);
                }
                else
                {
                    if (transferSelected != null)
                    {
                        transferSelected.TransferringCompanyID = transferringCompanyId;
                        transferSelected.FundTypeID = transferringFundTypeId;
                        transferSelected.TransferDate = dpTransferringDatePrivate.SelectedDate;
                        transferSelected.TransferAmount = transferingAmount;
                        transferSelected.TransferedToCompanyID = transferedToCompanyId;
                        transferSelected.TransferedToFundTypeID = transferedToFundTypeId;
                        transferSelected.Company = transferringCompany;
                        transferSelected.FundType = transferrinfFundType;
                        transferSelected.Company1 = transferedToCompany;
                        transferSelected.FundType1 = transferedToFundType;
                    }
                }
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
            }
            dgTransferencesPrivateBinding();
            ClearInputs(new object[] { cbTransferringFundPrivate, cbTransferringFundTypePrivate, dpTransferringDatePrivate, txtTransferringAmountPrivate, cbTransferedToPrivate, cbTransferedToFundTypePrivate });
            txtBtnAddTransferPrivate.Text = "הוסף העברה";
            transferingAmount = 0;
        }

        private void dgTransferencesPrivate_MouseDown(object sender, MouseButtonEventArgs e)
        {
            transferSelected = null;
            dgTransferencesPrivate.UnselectAll();
            ClearInputs(new object[] { cbTransferringFundPrivate, cbTransferringFundTypePrivate, dpTransferringDatePrivate, txtTransferringAmountPrivate, cbTransferedToPrivate, cbTransferedToFundTypePrivate });
            txtBtnAddTransferPrivate.Text = "הוסף העברה";
        }

        private void dgTransferencesPrivate_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (dgTransferencesPrivate.SelectedItem != null)
            {
                txtBtnAddTransferPrivate.Text = "עדכן העברה";
                transferSelected = (LifePolicyTransfer)dgTransferencesPrivate.SelectedItem;
                var funds = cbTransferringFundPrivate.Items;
                foreach (var item in funds)
                {
                    InsuranceCompany fund = (InsuranceCompany)item;
                    if (fund.CompanyID == transferSelected.TransferringCompanyID)
                    {
                        cbTransferringFundPrivate.SelectedItem = item;
                    }
                    if (fund.CompanyID == transferSelected.TransferedToCompanyID)
                    {
                        cbTransferedToPrivate.SelectedItem = item;
                    }
                }
                var fundTypes = cbTransferringFundTypePrivate.Items;
                foreach (var item in fundTypes)
                {
                    FundType type = (FundType)item;
                    if (type.FundTypeID == transferSelected.FundTypeID)
                    {
                        cbTransferringFundTypePrivate.SelectedItem = item;
                        break;
                    }
                }
                var transferedToFundTypes = cbTransferedToFundTypePrivate.Items;
                foreach (var item in transferedToFundTypes)
                {
                    FundType type = (FundType)item;
                    if (type.FundTypeID == transferSelected.TransferedToFundTypeID)
                    {
                        cbTransferedToFundTypePrivate.SelectedItem = item;
                        break;
                    }
                }
                dpTransferringDatePrivate.SelectedDate = transferSelected.TransferDate;
                txtTransferringAmountPrivate.Text = validations.ConvertDecimalToString(transferSelected.TransferAmount);
            }
        }

        private void cbTransferringFundPrivate_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cbTransferringFundPrivate.SelectedItem != null)
            {
                cbTransferringFundTypePrivate.IsEnabled = true;
                btnUpdateTransferringFundTypeTablePrivate.IsEnabled = true;
                TransferringFundsTypePrivateBinding();
            }
            else
            {
                cbTransferringFundTypePrivate.IsEnabled = false;
                btnUpdateTransferringFundTypeTablePrivate.IsEnabled = false;
            }
        }

        private void TransferringFundsTypePrivateBinding()
        {
            if (cbTransferringFundPrivate.SelectedItem != null)
            {
                int? selectedItemId = null;
                if (cbTransferringFundTypePrivate.SelectedItem != null)
                {
                    selectedItemId = ((FundType)cbTransferringFundTypePrivate.SelectedItem).FundTypeID;
                }
                cbTransferringFundTypePrivate.ItemsSource = fundsLogics.GetActiveFundTypesByInsuranceAndCompany(insuranceLogic.GetInsuranceID("חיים"), ((LifeIndustry)cbLifeIndustry.SelectedItem).LifeIndustryID, ((InsuranceCompany)cbTransferringFundPrivate.SelectedItem).CompanyID);
                cbTransferringFundTypePrivate.DisplayMemberPath = "FundTypeName";
                if (selectedItemId != null)
                {
                    SelectFundItemInCb((int)selectedItemId, cbTransferringFundTypePrivate);
                }
            }
        }

        private void cbTransferedToPrivate_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cbTransferedToPrivate.SelectedItem != null)
            {
                cbTransferedToFundTypePrivate.IsEnabled = true;
                btnUpdateTransferedToFundTypeTablePrivate.IsEnabled = true;
                TransferedFundsTypePrivateBinding();
            }
            else
            {
                cbTransferedToFundTypePrivate.IsEnabled = false;
                btnUpdateTransferedToFundTypeTablePrivate.IsEnabled = false;
            }
        }

        private void TransferedFundsTypePrivateBinding()
        {
            if (cbTransferedToPrivate.SelectedItem != null)
            {
                int? selectedItemId = null;
                if (cbTransferedToFundTypePrivate.SelectedItem != null)
                {
                    selectedItemId = ((FundType)cbTransferedToFundTypePrivate.SelectedItem).FundTypeID;
                }
                cbTransferedToFundTypePrivate.ItemsSource = fundsLogics.GetActiveFundTypesByInsuranceAndCompany(insuranceLogic.GetInsuranceID("חיים"), ((LifeIndustry)cbLifeIndustry.SelectedItem).LifeIndustryID, ((InsuranceCompany)cbTransferedToPrivate.SelectedItem).CompanyID);
                cbTransferedToFundTypePrivate.DisplayMemberPath = "FundTypeName";
                if (selectedItemId != null)
                {
                    SelectFundItemInCb((int)selectedItemId, cbTransferedToFundTypePrivate);
                }
            }
        }

        private void btnUpdateTransferringFundTypeTablePrivate_Click(object sender, RoutedEventArgs e)
        {
            int? selectedItemId = null;
            if (cbTransferringFundTypePrivate.SelectedItem != null)
            {
                selectedItemId = ((FundType)cbTransferringFundTypePrivate.SelectedItem).FundTypeID;
            }
            int insuranceID = insuranceLogic.GetInsuranceID("חיים");
            FundType fundType = new FundType() { InsuranceID = insuranceID, LifeIndustryID = ((LifeIndustry)cbLifeIndustry.SelectedItem).LifeIndustryID, CompanyID = ((InsuranceCompany)cbTransferringFundPrivate.SelectedItem).CompanyID };
            frmUpdateTable updateFundTypesTable = new frmUpdateTable(fundType);
            updateFundTypesTable.ShowDialog();
            TransferringFundsTypePrivateBinding();
            TransferedFundsTypePrivateBinding();
            if (updateFundTypesTable.ItemAdded != null)
            {
                SelectFundItemInCb(((FundType)updateFundTypesTable.ItemAdded).FundTypeID, cbTransferringFundTypePrivate);
            }
            else if (selectedItemId != null)
            {
                SelectFundItemInCb((int)selectedItemId, cbTransferringFundTypePrivate);
            }
        }

        private void btnUpdateTransferedToFundTypeTablePrivate_Click(object sender, RoutedEventArgs e)
        {
            int? selectedItemId = null;
            if (cbTransferedToFundTypePrivate.SelectedItem != null)
            {
                selectedItemId = ((FundType)cbTransferedToFundTypePrivate.SelectedItem).FundTypeID;
            }
            int insuranceID = insuranceLogic.GetInsuranceID("חיים");
            FundType fundType = new FundType() { InsuranceID = insuranceID, LifeIndustryID = ((LifeIndustry)cbLifeIndustry.SelectedItem).LifeIndustryID, CompanyID = ((InsuranceCompany)cbTransferedToPrivate.SelectedItem).CompanyID };
            frmUpdateTable updateFundTypesTable = new frmUpdateTable(fundType);
            updateFundTypesTable.ShowDialog();
            TransferringFundsTypePrivateBinding();
            TransferedFundsTypePrivateBinding();
            if (updateFundTypesTable.ItemAdded != null)
            {
                SelectFundItemInCb(((FundType)updateFundTypesTable.ItemAdded).FundTypeID, cbTransferedToFundTypePrivate);
            }
            else if (selectedItemId != null)
            {
                SelectFundItemInCb((int)selectedItemId, cbTransferedToFundTypePrivate);
            }
        }

        private void btnDeleteTransferencePrivate_Click(object sender, RoutedEventArgs e)
        {
            if (dgTransferencesPrivate.SelectedItem != null)
            {
                if (MessageBox.Show("למחוק העברה", "אישור", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    transfers.Remove((LifePolicyTransfer)dgTransferencesPrivate.SelectedItem);
                    dgTransferencesPrivateBinding();
                    ClearInputs(new object[] { cbTransferringFundPrivate, cbTransferringFundTypePrivate, dpTransferringDatePrivate, txtTransferringAmountPrivate, cbTransferedToPrivate, cbTransferedToFundTypePrivate });
                    txtBtnAddTransferPrivate.Text = "הוסף העברה";
                }
            }
            else
            {
                System.Windows.Forms.MessageBox.Show("נא סמן את ההעברה שברצונך לבטל");
            }
        }

        private void chbIsRisksPrivate_Checked(object sender, RoutedEventArgs e)
        {
            spRisksPrivate.IsEnabled = true;
            spRisksPrivate2.IsEnabled = true;
        }

        private void chbIsRisksPrivate_Unchecked(object sender, RoutedEventArgs e)
        {
            spRisksPrivate.IsEnabled = false;
            spRisksPrivate2.IsEnabled = false;
        }

        //private void btnAddInsuredPrivate_Click(object sender, RoutedEventArgs e)
        //{
        //    if (additionalsInsureds.Count < 1)
        //    {
        //        if (txtAdditionalInsuredIdPrivate.Text == "" || txtAdditionalInsuredFirstNamePrivate.Text == "" || txtAdditionalInsuredLastNamePrivate.Text == "")
        //        {
        //            System.Windows.Forms.MessageBox.Show("שם פרטי, משפחה ות.ז. הינן שדות חובה");
        //            return;
        //        }
        //        string relationship = null;
        //        if (txtRelationshipPrivate.SelectedItem != null)
        //        {
        //            relationship = ((ComboBoxItem)txtRelationshipPrivate.SelectedItem).Content.ToString();
        //        }
        //        LifeAdditionalInsured newAdditional = new LifeAdditionalInsured() { FirstName = txtAdditionalInsuredFirstNamePrivate.Text, LastName = txtAdditionalInsuredLastNamePrivate.Text, IdNumber = txtAdditionalInsuredIdPrivate.Text, BirthDate = dpAdditionalInsuredBirthDatePrivate.SelectedDate, RelationshipWithPrincipalInsured = relationship };
        //        additionalsInsureds.Add(newAdditional);
        //        //additionalsCount++;
        //        ((TabItem)tbControlPrivate.Items[additionalsInsureds.Count]).Visibility = Visibility.Visible;
        //        ((TabItem)tbControlPrivate.Items[additionalsInsureds.Count]).Header = string.Format("{0} {1}", newAdditional.FirstName, newAdditional.LastName);
        //        ClearInputs(new object[] { txtAdditionalInsuredFirstNamePrivate, txtAdditionalInsuredLastNamePrivate, txtAdditionalInsuredIdPrivate, dpAdditionalInsuredBirthDatePrivate, txtRelationshipPrivate });
        //    }
        //    else
        //    {
        //        System.Windows.Forms.MessageBox.Show("לא ניתן להוסיף עוד מבוטחים");
        //        ClearInputs(new object[] { txtAdditionalInsuredFirstNamePrivate, txtAdditionalInsuredLastNamePrivate, txtAdditionalInsuredIdPrivate, dpAdditionalInsuredBirthDatePrivate, txtRelationshipPrivate });

        //    }
        //}

        private void cbPensionPlanType_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cbPensionPlanType.SelectedIndex == 1)
            {
                txtDisabilityPension.IsEnabled = false;
                txtDisabilityPension.Clear();
                txtWidowRests.IsEnabled = false;
                txtWidowRests.Clear();
                txtOrphanRestsUntil21.IsEnabled = false;
                txtOrphanRestsUntil21.Clear();
                txtMaximumRests.IsEnabled = false;
                txtMaximumRests.Clear();
                txtRevenueValue.IsEnabled = false;
                txtRevenueValue.Clear();
                txtDisabilityPercentage.IsEnabled = false;
                txtDisabilityPercentage.Clear();
                txtWidowPercentage.IsEnabled = false;
                txtWidowPercentage.Clear();
                txtOrphanPercentage.IsEnabled = false;
                txtOrphanPercentage.Clear();
                txtParentSupportedPercentage.IsEnabled = false;
                txtParentSupportedPercentage.Clear();
            }
            else
            {
                txtDisabilityPension.IsEnabled = true;
                txtWidowRests.IsEnabled = true;
                txtOrphanRestsUntil21.IsEnabled = true;
                txtMaximumRests.IsEnabled = true;
                txtRevenueValue.IsEnabled = true;
                txtDisabilityPercentage.IsEnabled = true;
                txtWidowPercentage.IsEnabled = true;
                txtOrphanPercentage.IsEnabled = true;
                txtParentSupportedPercentage.IsEnabled = true;
            }
        }

        private void cbTransferringFundPension_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cbTransferringFundPension.SelectedItem != null)
            {
                cbTrasferringFundTypePension.IsEnabled = true;
                btnUpdateTransferringFundTypeTablePension.IsEnabled = true;
                TransferringFundsTypePensionBinding();
            }
            else
            {
                cbTrasferringFundTypePension.IsEnabled = false;
                btnUpdateTransferringFundTypeTablePension.IsEnabled = false;
            }
        }

        private void TransferringFundsTypePensionBinding()
        {
            if (cbTransferringFundPension.SelectedItem != null)
            {
                int? selectedItemId = null;
                if (cbTrasferringFundTypePension.SelectedItem != null)
                {
                    selectedItemId = ((FundType)cbTrasferringFundTypePension.SelectedItem).FundTypeID;
                }
                cbTrasferringFundTypePension.ItemsSource = fundsLogics.GetActiveFundTypesByInsuranceAndCompany(insuranceLogic.GetInsuranceID("חיים"), ((LifeIndustry)cbLifeIndustry.SelectedItem).LifeIndustryID, ((InsuranceCompany)cbTransferringFundPension.SelectedItem).CompanyID);
                cbTrasferringFundTypePension.DisplayMemberPath = "FundTypeName";
                if (selectedItemId != null)
                {
                    SelectFundItemInCb((int)selectedItemId, cbTrasferringFundTypePension);
                }
            }
        }

        private void cbTransferedToPension_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cbTransferedToPension.SelectedItem != null)
            {
                cbTransferedToFundTypePension.IsEnabled = true;
                btnUpdateTransferedToFundTypeTablePension.IsEnabled = true;
                TransferedFundsTypePensionBinding();
            }
            else
            {
                cbTransferedToFundTypePension.IsEnabled = false;
                btnUpdateTransferedToFundTypeTablePension.IsEnabled = false;
            }
        }

        private void TransferedFundsTypePensionBinding()
        {
            if (cbTransferedToPension.SelectedItem != null)
            {
                int? selectedItemId = null;
                if (cbTransferedToFundTypePension.SelectedItem != null)
                {
                    selectedItemId = ((FundType)cbTransferedToFundTypePension.SelectedItem).FundTypeID;
                }
                cbTransferedToFundTypePension.ItemsSource = fundsLogics.GetActiveFundTypesByInsuranceAndCompany(insuranceLogic.GetInsuranceID("חיים"), ((LifeIndustry)cbLifeIndustry.SelectedItem).LifeIndustryID, ((InsuranceCompany)cbTransferedToPension.SelectedItem).CompanyID);
                cbTransferedToFundTypePension.DisplayMemberPath = "FundTypeName"; if (selectedItemId != null)
                {
                    SelectFundItemInCb((int)selectedItemId, cbTransferedToFundTypePension);
                }
            }
        }

        private void btnAddTransferencePension_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                int? transferringCompanyId = null;
                Company transferringCompany = null;
                if (cbTransferringFundPension.SelectedItem != null)
                {
                    transferringCompanyId = ((InsuranceCompany)cbTransferringFundPension.SelectedItem).CompanyID;
                    transferringCompany = ((InsuranceCompany)cbTransferringFundPension.SelectedItem).Company;
                }
                int? transferringFundTypeId = null;
                FundType transferrinfFundType = null;
                if (cbTrasferringFundTypePension.SelectedItem != null)
                {
                    transferringFundTypeId = ((FundType)cbTrasferringFundTypePension.SelectedItem).FundTypeID;
                    transferrinfFundType = (FundType)cbTrasferringFundTypePension.SelectedItem;
                }
                int? transferedToCompanyId = null;
                Company transferedToCompany = null;
                if (cbTransferedToPension.SelectedItem != null)
                {
                    transferedToCompanyId = ((InsuranceCompany)cbTransferedToPension.SelectedItem).CompanyID;
                    transferedToCompany = ((InsuranceCompany)cbTransferedToPension.SelectedItem).Company;
                }
                int? transferedToFundTypeId = null;
                FundType transferedToFundType = null;
                if (cbTransferedToFundTypePension.SelectedItem != null)
                {
                    transferedToFundTypeId = ((FundType)cbTransferedToFundTypePension.SelectedItem).FundTypeID;
                    transferedToFundType = (FundType)cbTransferedToFundTypePension.SelectedItem;
                }
                if (txtBtnAddTransferPension.Text.Contains("הוסף"))
                {
                    transferSelected = null;
                    LifePolicyTransfer newTransfer = new LifePolicyTransfer() { TransferringCompanyID = transferringCompanyId, FundTypeID = transferringFundTypeId, TransferDate = dpTransferringDatePension.SelectedDate, TransferAmount = transferingAmount, TransferedToCompanyID = transferedToCompanyId, TransferedToFundTypeID = transferedToFundTypeId, Company = transferringCompany, FundType = transferrinfFundType, Company1 = transferedToCompany, FundType1 = transferedToFundType };
                    transfers.Add(newTransfer);
                }
                else
                {
                    if (transferSelected != null)
                    {
                        transferSelected.TransferringCompanyID = transferringCompanyId;
                        transferSelected.FundTypeID = transferringFundTypeId;
                        transferSelected.TransferDate = dpTransferringDatePension.SelectedDate;
                        transferSelected.TransferAmount = transferingAmount;
                        transferSelected.TransferedToCompanyID = transferedToCompanyId;
                        transferSelected.TransferedToFundTypeID = transferedToFundTypeId;
                        transferSelected.Company = transferringCompany;
                        transferSelected.FundType = transferrinfFundType;
                        transferSelected.Company1 = transferedToCompany;
                        transferSelected.FundType1 = transferedToFundType;
                    }
                }
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
            }
            dgTransferencesPensionBinding();
            ClearInputs(new object[] { cbTransferringFundPension, cbTrasferringFundTypePension, dpTransferringDatePension, txtTransferringAmountPension, cbTransferedToPension, cbTransferedToFundTypePension });
            txtBtnAddTransferPension.Text = "הוסף העברה";
            transferingAmount = 0;
        }

        private void dgTransferencesPensionBinding()
        {
            dgTransferencesPension.ItemsSource = transfers.ToList();
            lv.AutoSizeColumns(dgTransferencesPension.View);
        }

        private void dgTransferencesPension_MouseDown(object sender, MouseButtonEventArgs e)
        {
            transferSelected = null;
            dgTransferencesPension.UnselectAll();
            ClearInputs(new object[] { cbTransferringFundPension, cbTrasferringFundTypePension, dpTransferringDatePension, txtTransferringAmountPension, cbTransferedToPension, cbTransferedToFundTypePension });
            txtBtnAddTransferPension.Text = "הוסף העברה";
        }

        private void btnUpdateTransferringFundTypeTablePension_Click(object sender, RoutedEventArgs e)
        {
            int? selectedItemId = null;
            if (cbTrasferringFundTypePension.SelectedItem != null)
            {
                selectedItemId = ((FundType)cbTrasferringFundTypePension.SelectedItem).FundTypeID;
            }
            int insuranceID = insuranceLogic.GetInsuranceID("חיים");
            FundType fundType = new FundType() { InsuranceID = insuranceID, LifeIndustryID = ((LifeIndustry)cbLifeIndustry.SelectedItem).LifeIndustryID, CompanyID = ((InsuranceCompany)cbTransferringFundPension.SelectedItem).CompanyID };
            frmUpdateTable updateFundTypesTable = new frmUpdateTable(fundType);
            updateFundTypesTable.ShowDialog();
            TransferringFundsTypePensionBinding();
            TransferedFundsTypePensionBinding();
            if (updateFundTypesTable.ItemAdded != null)
            {
                SelectFundItemInCb(((FundType)updateFundTypesTable.ItemAdded).FundTypeID, cbTrasferringFundTypePension);
            }
            else if (selectedItemId != null)
            {
                SelectFundItemInCb((int)selectedItemId, cbTrasferringFundTypePension);
            }
        }

        private void btnUpdateTransferedToFundTypeTablePension_Click(object sender, RoutedEventArgs e)
        {
            int? selectedItemId = null;
            if (cbTransferedToFundTypePension.SelectedItem != null)
            {
                selectedItemId = ((FundType)cbTransferedToFundTypePension.SelectedItem).FundTypeID;
            }
            int insuranceID = insuranceLogic.GetInsuranceID("חיים");
            FundType fundType = new FundType() { InsuranceID = insuranceID, LifeIndustryID = ((LifeIndustry)cbLifeIndustry.SelectedItem).LifeIndustryID, CompanyID = ((InsuranceCompany)cbTransferedToPension.SelectedItem).CompanyID };
            frmUpdateTable updateFundTypesTable = new frmUpdateTable(fundType);
            updateFundTypesTable.ShowDialog();
            TransferringFundsTypePensionBinding();
            TransferedFundsTypePensionBinding();
            if (updateFundTypesTable.ItemAdded != null)
            {
                SelectFundItemInCb(((FundType)updateFundTypesTable.ItemAdded).FundTypeID, cbTransferedToFundTypePension);
            }
            else if (selectedItemId != null)
            {
                SelectFundItemInCb((int)selectedItemId, cbTransferedToFundTypePension);
            }
        }

        private void dgTransferencesPension_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (dgTransferencesPension.SelectedItem != null)
            {
                txtBtnAddTransferPension.Text = "עדכן העברה";
                transferSelected = (LifePolicyTransfer)dgTransferencesPension.SelectedItem;
                var funds = cbTransferringFundPension.Items;
                foreach (var item in funds)
                {
                    InsuranceCompany fund = (InsuranceCompany)item;
                    if (fund.CompanyID == transferSelected.TransferringCompanyID)
                    {
                        cbTransferringFundPension.SelectedItem = item;
                    }
                    if (fund.CompanyID == transferSelected.TransferedToCompanyID)
                    {
                        cbTransferedToPension.SelectedItem = item;
                    }
                }
                var fundTypes = cbTrasferringFundTypePension.Items;
                foreach (var item in fundTypes)
                {
                    FundType type = (FundType)item;
                    if (type.FundTypeID == transferSelected.FundTypeID)
                    {
                        cbTrasferringFundTypePension.SelectedItem = item;
                        break;
                    }
                }
                var transferedToFundTypes = cbTransferedToFundTypePension.Items;
                foreach (var item in transferedToFundTypes)
                {
                    FundType type = (FundType)item;
                    if (type.FundTypeID == transferSelected.TransferedToFundTypeID)
                    {
                        cbTransferedToFundTypePension.SelectedItem = item;
                        break;
                    }
                }
                dpTransferringDatePension.SelectedDate = transferSelected.TransferDate;
                txtTransferringAmountPension.Text = validations.ConvertDecimalToString(transferSelected.TransferAmount);
            }
        }

        private void btnDeleteTransferencePension_Click(object sender, RoutedEventArgs e)
        {
            if (dgTransferencesPension.SelectedItem != null)
            {
                if (MessageBox.Show("למחוק העברה", "אישור", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    transfers.Remove((LifePolicyTransfer)dgTransferencesPension.SelectedItem);
                    dgTransferencesPensionBinding();
                    ClearInputs(new object[] { cbTransferringFundPension, cbTrasferringFundTypePension, dpTransferringDatePension, txtTransferringAmountPension, cbTransferedToPension, cbTransferedToFundTypePension });
                    txtBtnAddTransferPension.Text = "הוסף העברה";
                }
            }
            else
            {
                System.Windows.Forms.MessageBox.Show("נא סמן את ההעברה שברצונך לבטל");
            }
        }

        private void chbIsAdditionalInsuredPrivate_Checked(object sender, RoutedEventArgs e)
        {
            //if (additionalsInsureds.Count < 1)
            //{
            //if (txtAdditionalInsuredIdPrivate.Text == "" || txtAdditionalInsuredFirstNamePrivate.Text == "" || txtAdditionalInsuredLastNamePrivate.Text == "")
            //{
            //    System.Windows.Forms.MessageBox.Show("שם פרטי, משפחה ות.ז. הינן שדות חובה");
            //    return;
            //}
            //string relationship = null;
            //if (txtRelationshipPrivate.SelectedItem != null)
            //{
            //    relationship = ((ComboBoxItem)txtRelationshipPrivate.SelectedItem).Content.ToString();
            //}
            //LifeAdditionalInsured newAdditional = new LifeAdditionalInsured() { FirstName = txtAdditionalInsuredFirstNamePrivate.Text, LastName = txtAdditionalInsuredLastNamePrivate.Text, IdNumber = txtAdditionalInsuredIdPrivate.Text, BirthDate = dpAdditionalInsuredBirthDatePrivate.SelectedDate, RelationshipWithPrincipalInsured = relationship };
            //additionalsInsureds.Add(newAdditional);
            //additionalsCount++;
            ((TabItem)tbControlPrivate.Items[additionalsInsureds.Count]).Visibility = Visibility.Visible;
            chbIsAdditionalInsuredPrivate.IsEnabled = true;
            txtAdditionalInsuredLastNamePrivate.IsEnabled = true;
            txtAdditionalInsuredIdPrivate.IsEnabled = true;
            dpAdditionalInsuredBirthDatePrivate.IsEnabled = true;
            txtRelationshipPrivate.IsEnabled = true;
            //((TabItem)tbControlPrivate.Items[additionalsInsureds.Count]).Header = string.Format("{0} {1}", newAdditional.FirstName, newAdditional.LastName);
            //ClearInputs(new object[] { txtAdditionalInsuredFirstNamePrivate, txtAdditionalInsuredLastNamePrivate, txtAdditionalInsuredIdPrivate, dpAdditionalInsuredBirthDatePrivate, txtRelationshipPrivate });
        }

        private void chbIsAdditionalInsuredPrivate_Unchecked(object sender, RoutedEventArgs e)
        {
            ((TabItem)tbControlPrivate.Items[additionalsInsureds.Count]).Visibility = Visibility.Collapsed;
            chbIsAdditionalInsuredPrivate.IsEnabled = false;
            txtAdditionalInsuredLastNamePrivate.IsEnabled = false;
            txtAdditionalInsuredIdPrivate.IsEnabled = false;
            dpAdditionalInsuredBirthDatePrivate.IsEnabled = false;
            txtRelationshipPrivate.IsEnabled = false;
        }

        private void txtAdditionalInsuredFirstNamePrivate_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (txtAdditionalInsuredFirstNamePrivate.Text != "")
            {
                tbItemInsurenced2Private.Header = string.Format("{0} {1}", txtAdditionalInsuredFirstNamePrivate.Text, txtAdditionalInsuredLastNamePrivate.Text);
            }
        }

        private void txtAdditionalInsuredLastNamePrivate_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (txtAdditionalInsuredLastNamePrivate.Text != "")
            {
                tbItemInsurenced2Private.Header = string.Format("{0} {1}", txtAdditionalInsuredFirstNamePrivate.Text, txtAdditionalInsuredLastNamePrivate.Text);
            }
        }

        private void btnNewCoverage_Click(object sender, RoutedEventArgs e)
        {
            frmLifeCoverage newCoverageWindow = new frmLifeCoverage((FundType)cbInsuranceType.SelectedItem);
            newCoverageWindow.ShowDialog();
            if (newCoverageWindow.NewCoverage != null)
            {
                coverages.Add(newCoverageWindow.NewCoverage);
            }
            dgCoveragesBinding();
        }

        private void dgCoveragesBinding()
        {
            dgLifeCoverages.ItemsSource = coverages.ToList();
            lv.AutoSizeColumns(dgLifeCoverages.View);
        }

        private void cbInsuranceType_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cbInsuranceType.SelectedItem != null)
            {
                tbItemCoverages.Visibility = Visibility.Visible;
            }
            else
            {
                tbItemCoverages.Visibility = Visibility.Collapsed;
            }
        }

        private void dgLifeCoverages_MouseDown(object sender, MouseButtonEventArgs e)
        {
            dgLifeCoverages.UnselectAll();
        }

        private void btnDeleteCoverage_Click(object sender, RoutedEventArgs e)
        {
            if (dgLifeCoverages.SelectedItem == null)
            {
                MessageBox.Show("נא לסמן כיסוי בטבלה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            if (MessageBox.Show("למחוק כיסוי", "אישור", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                coverages.Remove((LifeCoverage)dgLifeCoverages.SelectedItem);
                dgCoveragesBinding();
            }
            else
            {
                dgLifeCoverages.UnselectAll();
            }
        }

        private void btnUpdateCoverage_Click(object sender, RoutedEventArgs e)
        {
            if (dgLifeCoverages.SelectedItem == null)
            {
                MessageBox.Show("נא לסמן כיסוי בטבלה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            LifeCoverage coverageToUpdate = (LifeCoverage)dgLifeCoverages.SelectedItem;
            frmLifeCoverage updateCoverageWindow = new frmLifeCoverage(coverageToUpdate, (FundType)cbInsuranceType.SelectedItem);
            updateCoverageWindow.ShowDialog();
            coverageToUpdate = updateCoverageWindow.NewCoverage;
            dgCoveragesBinding();
            dgLifeCoverages.UnselectAll();
        }

        private void btnNewTracking_Click(object sender, RoutedEventArgs e)
        {
            frmTracking newTrackingWindow = new frmTracking(client, lifePolicy, handlerUser);
            newTrackingWindow.ShowDialog();
            if (newTrackingWindow.trackingContent != null)
            {
                trackings.Add(new LifeTracking() { TrackingContent = newTrackingWindow.trackingContent, Date = DateTime.Now, UserID = handlerUser.UserID, User = handlerUser });
                dgTrackingsBinding();
            }
        }

        private void dgTrackingsBinding()
        {
            dgLifeTrackings.ItemsSource = trackings.OrderByDescending(t => t.Date);
            lv.AutoSizeColumns(dgLifeTrackings.View);
        }

        private void dgLifeTrackings_MouseDown(object sender, MouseButtonEventArgs e)
        {
            dgLifeTrackings.UnselectAll();
        }

        private void btnDeleteTracking_Click(object sender, RoutedEventArgs e)
        {
            if (dgLifeTrackings.SelectedItem == null)
            {
                MessageBox.Show("נא לסמן מעקב בטבלה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            if (MessageBox.Show("למחוק מעקב", "אישור", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                trackings.Remove((LifeTracking)dgLifeTrackings.SelectedItem);
                dgTrackingsBinding();
            }
            else
            {
                dgLifeTrackings.UnselectAll();
            }
        }

        private void btnUpdateTracking_Click(object sender, RoutedEventArgs e)
        {
            if (dgLifeTrackings.SelectedItem == null)
            {
                MessageBox.Show("נא לסמן מעקב בטבלה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            LifeTracking trackingToUpdate = (LifeTracking)dgLifeTrackings.SelectedItem;
            frmTracking updateTrackingWindow = new frmTracking(trackingToUpdate, client, lifePolicy, handlerUser);
            updateTrackingWindow.ShowDialog();
            trackingToUpdate.TrackingContent = updateTrackingWindow.trackingContent;
            dgTrackingsBinding();
            dgLifeTrackings.UnselectAll();
        }

        private void dgLifeTrackings_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            DependencyObject src = (DependencyObject)(e.OriginalSource);
            while (!(src is Control))
            {
                if (!(src is Visual))
                {
                    break;
                }
                src = VisualTreeHelper.GetParent(src);
            }

            if ((src is Control && src.GetType() == typeof(ListViewItem)) || src.GetType() == typeof(Run))
            {
                if (dgLifeTrackings.SelectedItem != null)
                {
                    LifeTracking trackingToUpdate = (LifeTracking)dgLifeTrackings.SelectedItem;
                    frmTracking updateTrackingWindow = new frmTracking(trackingToUpdate, client, lifePolicy, handlerUser);
                    updateTrackingWindow.ShowDialog();
                    trackingToUpdate.TrackingContent = updateTrackingWindow.trackingContent;
                    dgTrackingsBinding();
                    dgLifeTrackings.UnselectAll();
                }
            }
        }

        private void dgLifeCoverages_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            DependencyObject src = (DependencyObject)(e.OriginalSource);
            while (!(src is Control))
            {
                if (!(src is Visual))
                {
                    break;
                }
                src = VisualTreeHelper.GetParent(src);
            }

            if ((src is Control && src.GetType() == typeof(ListViewItem)) || src.GetType() == typeof(Run))
            {
                if (dgLifeCoverages.SelectedItem == null)
                {
                    MessageBox.Show("נא לסמן כיסוי בטבלה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    return;
                }
                LifeCoverage coverageToUpdate = (LifeCoverage)dgLifeCoverages.SelectedItem;
                frmLifeCoverage updateCoverageWindow = new frmLifeCoverage(coverageToUpdate, (FundType)cbInsuranceType.SelectedItem);
                updateCoverageWindow.ShowDialog();
                coverageToUpdate = updateCoverageWindow.NewCoverage;
                dgCoveragesBinding();
                dgLifeCoverages.UnselectAll();
            }
        }

        private void tbControlRisks_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ClearInputs(new object[] { txtAdditionalInsurencedFirstNameRisk, txtAdditionalInsurencedLastNameRisk, txtAdditionalInsurencedIdRisk, dpAdditionalInsurencedBirthDateRisk, cbRelationshipRisk });

            if (tbControlRisks.SelectedItem != null && tbControlRisks.SelectedIndex != 0)
            {
                PrintAdditionalDetails(tbControlRisks.SelectedIndex - 1);
            }
        }

        public void PrintAdditionalDetails(int index)
        {
            txtAdditionalInsurencedFirstNameRisk.Text = additionalsInsureds[index].FirstName;
            txtAdditionalInsurencedLastNameRisk.Text = additionalsInsureds[index].LastName;
            txtAdditionalInsurencedIdRisk.Text = additionalsInsureds[index].IdNumber;
            dpAdditionalInsurencedBirthDateRisk.SelectedDate = additionalsInsureds[index].BirthDate;
            var relationships = cbRelationshipRisk.Items;
            foreach (var item in relationships)
            {
                if (additionalsInsureds[index].RelationshipWithPrincipalInsured == ((ComboBoxItem)item).Content.ToString())
                {
                    cbRelationshipMortgage.SelectedItem = item;
                    break;
                }
            }
        }

        private void btnUpdateInsurencedRisk_Click(object sender, RoutedEventArgs e)
        {
            if (tbControlRisks.SelectedItem != null && tbControlRisks.SelectedIndex != 0)
            {
                additionalsInsureds[tbControlRisks.SelectedIndex - 1].FirstName = txtAdditionalInsurencedFirstNameRisk.Text;
                additionalsInsureds[tbControlRisks.SelectedIndex - 1].LastName = txtAdditionalInsurencedLastNameRisk.Text;
                additionalsInsureds[tbControlRisks.SelectedIndex - 1].IdNumber = txtAdditionalInsurencedIdRisk.Text;
                additionalsInsureds[tbControlRisks.SelectedIndex - 1].BirthDate = dpAdditionalInsurencedBirthDateRisk.SelectedDate;
                string relationship = null;

                if (cbRelationshipRisk.SelectedItem != null)
                {
                    relationship = ((ComboBoxItem)cbRelationshipRisk.SelectedItem).Content.ToString();
                }
                additionalsInsureds[tbControlRisks.SelectedIndex - 1].RelationshipWithPrincipalInsured = relationship;
                ((TabItem)tbControlRisks.Items[tbControlRisks.SelectedIndex]).Header = string.Format("{0} {1}", additionalsInsureds[tbControlRisks.SelectedIndex - 1].FirstName, additionalsInsureds[tbControlRisks.SelectedIndex - 1].LastName);
                ClearInputs(new object[] { txtAdditionalInsurencedFirstNameRisk, txtAdditionalInsurencedLastNameRisk, txtAdditionalInsurencedIdRisk, dpAdditionalInsurencedBirthDateRisk, cbRelationshipRisk });

            }
        }

        private void txtPolicyNumber_GotFocus(object sender, RoutedEventArgs e)
        {
            isChanges = true;
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (confirmBeforeClosing && isChanges)
            {
                if (MessageBox.Show("האם ברצונך לשמור שינוים שבצעת", "אישור", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    btnSave_Click(sender, new RoutedEventArgs());
                    if (cancelClose)
                    {
                        e.Cancel = true;
                        cancelClose = false;
                    }
                }
            }
        }

        private void chbStandardMoneyCollection_Checked(object sender, RoutedEventArgs e)
        {
            btnStandardMoneyCollectionDetails.IsEnabled = true;
        }
        private void chbStandardMoneyCollection_Unchecked(object sender, RoutedEventArgs e)
        {
            btnStandardMoneyCollectionDetails.IsEnabled = false;
        }
        private void btnStandardMoneyCollectionDetails_Click(object sender, RoutedEventArgs e)
        {
            frmStandardMoneyCollectionDetails window = null;
            if (standardMoneyCollection == null)
            {
                window = new frmStandardMoneyCollectionDetails(false, handlerUser);
            }
            else
            {
                window = new frmStandardMoneyCollectionDetails(standardMoneyCollection, standardMoneyCollectionTrackings, false, handlerUser);
            }
            window.ShowDialog();
            if (window.StandardMoneyCollection != null)
            {
                standardMoneyCollection = window.StandardMoneyCollection;
            }
            if (window.Trackings != null)
            {
                standardMoneyCollectionTrackings = window.Trackings;
            }
        }
    }
}
