﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using CoralBusinessLogics;
using Outlook = Microsoft.Office.Interop.Outlook;

namespace CoralUI
{
    /// <summary>
    /// Interaction logic for frmContacts.xaml
    /// </summary>
    public partial class frmContacts : Window
    {
        ContactsLogic logic = new ContactsLogic();
        ContactCategoriesLogic categoryLogic = new ContactCategoriesLogic();
        List<Contact> searchResults = null;
        Email email = new Email();
        ExcelLogic excelLogic = new ExcelLogic();
        ListViewSettings lv = new ListViewSettings();
        public frmContacts()
        {
            InitializeComponent();
            this.SizeToContent = SizeToContent.Manual;
        }
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            DataGridBinding(false);
            ComboBoxBinding();
        }

        //מקשר את הקומבו בוקס עם טבלת הקטגוריות של אנשי קשר
        private void ComboBoxBinding()
        {
            cbCategories.ItemsSource = categoryLogic.GetAllContactCategories();
            cbCategories.DisplayMemberPath = "ContactCategoryName";
        }

        //מקשר את טבלת אנשי הקשר עם בסיס הנתונים
        private void DataGridBinding(bool isSearch)
        {
            dgContacts.UnselectAll();

            if (!isSearch)
            //כאשר לא מדובר על חיפוש- מייצג רשימת כל אנשי הקשר
            {
                cbCategories.SelectedItem = null;
                //cbCategories.Text = "הכל";
                dgContacts.ItemsSource = logic.GetAllContacts();
            }
            else
            {
                //אם זה חיפוש- מייצג את תוצאות החיפוש
                if (searchResults == null)
                {
                    dgContacts.ItemsSource = logic.FindContactByString(txtSearchContacts.Text, (ContactCategory)cbCategories.SelectedItem);
                    isSearch = false;
                }
                else
                {
                    dgContacts.ItemsSource = searchResults;
                    searchResults = null;
                }
            }
        }

        //פותח חלון הוספת איש קשר חדש 
        private void btnAddContact_Click(object sender, RoutedEventArgs e)
        {
            frmAddContact addContactWindow = new frmAddContact();
            addContactWindow.ShowDialog();
            DataGridBinding(false);
            ComboBoxBinding();
        }

        //סוגר את חלון האלפון
        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        //מסנן את אנשי הקשר לפי הקטגוריה המסומנת  
        private void cbCategories_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            DataGridBinding(true);
        }

        //מסנן את אנשי הקשר תוך כדי הקלדה לפי מחרוזת
        private void txtSearchContacts_TextChanged(object sender, TextChangedEventArgs e)
        {
            DataGridBinding(true);
        }

        //מוחק איש קשר
        private void btnDeleteContact_Click(object sender, RoutedEventArgs e)
        {
            if (dgContacts.SelectedItem != null)
            {
                if (MessageBox.Show("למחוק איש קשר", "אישור", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    logic.DeleteContact((Contact)dgContacts.SelectedItem);
                    DataGridBinding(false);
                }
                else
                {
                    dgContacts.UnselectAll();
                }
            }
            else
            {
                MessageBox.Show("נא לסמן את איש הקשר שברצונך למחוק", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
        }

        //מעדכן איש קשר קיים
        private void btnUpdateContact_Click(object sender, RoutedEventArgs e)
        {
            if (dgContacts.SelectedItem != null)
            {
                frmAddContact updateContactWindow = new frmAddContact((Contact)dgContacts.SelectedItem);
                Contact tempContact = (Contact)dgContacts.SelectedItem;
                updateContactWindow.ShowDialog();
                txtSearchContacts.Clear();
                cbCategories.SelectedItem = null;
                cbCategories.Text = "הכל";
                DataGridBinding(false);
                SelectContactInDg(tempContact, dgContacts);
            }
            else
            {
                MessageBox.Show("נא לסמן את איש הקשר שברצונך לעדכן", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
        }

        private void SelectContactInDg(Contact contactToSelect, ListView lv)
        {
            var dgItems = lv.Items;
            foreach (var item in dgItems)
            {
                Contact contact = (Contact)item;
                if (contact.ContactID == contactToSelect.ContactID)
                {
                    lv.SelectedItem = item;
                    lv.ScrollIntoView(item);
                    ListViewItem listViewItem = lv.ItemContainerGenerator.ContainerFromItem(lv.SelectedItem) as ListViewItem;
                    if (listViewItem != null)
                    {
                        listViewItem.Focus();
                    }
                    break;
                }
            }
        }

        //private void btnAdvanceContactsSearch_Click(object sender, RoutedEventArgs e)
        //{
        //    frmAddContact advanceSearch = new frmAddContact(true);
        //    advanceSearch.ShowDialog();
        //    searchResults = advanceSearch.searchResult;
        //    //cbCategories.SelectedItem = null;
        //    cbCategories.Text = "הכל";
        //    txtSearchContacts.Clear();
        //    DataGridBinding(true);
        //}

        private void dgContacts_MouseDown(object sender, MouseButtonEventArgs e)
        {
            dgContacts.UnselectAll();
        }

        //מחזיר את רשימת אנשי הקשר למצב ברירת מחדל- הצגה של כל אנשי הקשר
        private void btnDisplayAll_Click(object sender, RoutedEventArgs e)
        {
            txtSearchContacts.Clear();
            cbCategories.SelectedItem = null;
            cbCategories.Text = "הכל";
            DataGridBinding(false);
        }

        //שליחת מייל לאיש קשר
        private void btnSendEmail_Click(object sender, RoutedEventArgs e)
        {
            if (dgContacts.SelectedItem == null)
            {
                MessageBox.Show("נא לסמן איש קשר", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            Contact contact = (Contact)dgContacts.SelectedItem;
            if (contact.Email == "")
            {
                MessageBox.Show("אין לאיש הקשר כתובת דוא''ל במערכת", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            string[] address = new string[] { contact.Email };
            try
            {
                email.SendEmailFromOutlook("", "", null, address);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void dgContacts_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            DependencyObject src = (DependencyObject)(e.OriginalSource);
            while (!(src is Control))
            {
                if (!(src is Visual))
                {
                    break;
                }
                src = VisualTreeHelper.GetParent(src);
            }

            if ((src is Control && src.GetType() == typeof(ListViewItem)) || src.GetType() == typeof(Run))
            {
                if (dgContacts.SelectedItem != null)
                {
                    frmAddContact updateContactWindow = new frmAddContact((Contact)dgContacts.SelectedItem);
                    updateContactWindow.ShowDialog();
                    DataGridBinding(false);
                }
                else
                {
                    MessageBox.Show("נא לסמן את איש הקשר שברצונך לעדכן", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                }
            }
        }

        private void cbCategories_LostFocus(object sender, RoutedEventArgs e)
        {
            if (cbCategories.Text == "")
            {
                cbCategories.Text = "הכל";
            }
        }

        private void mItemExcel_Click(object sender, RoutedEventArgs e)
        {
            excelLogic.ContactsToExcel((List<Contact>)dgContacts.ItemsSource);
        }

        private void dgContacts_MouseDown_1(object sender, MouseButtonEventArgs e)
        {
            dgContacts.UnselectAll();
        }

        private void dgContacts_MouseDoubleClick_1(object sender, MouseButtonEventArgs e)
        {
            DependencyObject src = (DependencyObject)(e.OriginalSource);
            while (!(src is Control))
            {
                if (!(src is Visual))
                {
                    break;
                }
                src = VisualTreeHelper.GetParent(src);
            }

            if ((src is Control && src.GetType() == typeof(ListViewItem)) || src.GetType() == typeof(Run))
            {
                if (dgContacts.SelectedItem != null)
                {
                    frmAddContact updateContactWindow = new frmAddContact((Contact)dgContacts.SelectedItem);
                    Contact tempContact = (Contact)dgContacts.SelectedItem;
                    updateContactWindow.ShowDialog();
                    txtSearchContacts.Clear();
                    cbCategories.SelectedItem = null;
                    cbCategories.Text = "הכל";
                    DataGridBinding(false);
                    SelectContactInDg(tempContact, dgContacts);
                }
                else
                {
                    MessageBox.Show("נא לסמן את איש הקשר שברצונך לעדכן", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                }
            }
        }

        private void mItemPrint_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                frmPrintPreview preview = new frmPrintPreview("ContactsReport", dgContacts.ItemsSource);
                preview.ShowDialog();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
