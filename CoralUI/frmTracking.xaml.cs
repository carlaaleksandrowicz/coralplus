﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using CoralBusinessLogics;

namespace CoralUI
{
    /// <summary>
    /// Interaction logic for frmTracking.xaml
    /// </summary>
    public partial class frmTracking : Window
    {
        UserLogic userLogic = new UserLogic();
        public string trackingContent { get; set; }
        TrackingsViewModel trackingToUpdate = null;
        RenewalTracking renewalTrackingToUpdate = null;
        ForeingWorkersRenewalTracking foreingWorkersRenewalTrackingToUpdate = null;
        ElementaryClaimTracking claimTrackingToUpdate = null;
        LifeClaimTracking lifeClaimTrackingToUpdate = null;
        PersonalAccidentsClaimTracking personalAccidentsClaimTrackingToUpdate = null;
        HealthClaimTracking healthClaimTrackingToUpdate = null;
        RequestTracking requestTrackingToUpdate = null;
        StandardMoneyCollectionTracking standardMoneyCollectionTrackingToUpdate;
        TravelClaimTracking travelClaimTrackingToUpdate = null;
        Client client = null;
        object policy = null;
        User user = null;
        private LifeTracking lifeTrackingToUpdate;
        private LifePolicy lifePolicy;
        private HealthTracking healthTrackingToUpdate;
        private HealthPolicy healthPolicy;
        private User handlerUser;
        private FinanceTracking financeTrackingToUpdate;
        private FinancePolicy financePolicy;
        private PersonalAccidentsTracking personalAccidentsTrackingToUpdate;
        private PersonalAccidentsPolicy personalAccidentsPolicy;
        private TravelTracking travelTrackingToUpdate;
        private TravelPolicy travelPolicy;
        private ForeingWorkersTracking foreingWorkersTrackingToUpdate;
        private ForeingWorkersClaimTracking foreingWorkersClaimTrackingToUpdate;
        private List<StandardMoneyCollectionTracking> trackings;

        public frmTracking(bool isRequest)
        {
            InitializeComponent();
            this.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            DateTime dt = DateTime.Now;
            //string dtFormat = String.Format("{0:F}", dt);
            //lblDateTimeNow.Content = dtFormat;
            lblDateTimeNow.Content = dt.ToString("dd/MM/yyyy hh:mm");
            txtTracking.Focus();
            if (isRequest)
            {
                btnWorkQueque.Visibility = Visibility.Hidden;
            }
        }

        public frmTracking(Client clientAccount, object trackingPolicy, User userAccount)
        {
            InitializeComponent();
            this.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            DateTime dt = DateTime.Now;
            //string dtFormat = String.Format("{0:F}", dt);
            //lblDateTimeNow.Content = dtFormat;
            lblDateTimeNow.Content = dt.ToString("dd/MM/yyyy hh:mm");
            txtTracking.Focus();
            client = clientAccount;
            policy = trackingPolicy;
            user = userAccount;
        }
        public frmTracking(TrackingsViewModel tracking, Client clientAccount, object trackingPolicy, User userAccount)
        {
            InitializeComponent();
            this.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            trackingToUpdate = tracking;
            DateTime dt = (DateTime)trackingToUpdate.TrackingDate;
            //string dtFormat = String.Format("{0:F}", dt);
            //lblDateTimeNow.Content = dtFormat;
            lblDateTimeNow.Content = dt.ToString("dd/MM/yyyy hh:mm");
            txtTracking.Text = trackingToUpdate.TrackingContent;
            txtTracking.Focus();
            trackingContent = tracking.TrackingContent;
            client = clientAccount;
            policy = trackingPolicy;
            user = userAccount;
        }

        public frmTracking(ElementaryClaimTracking tracking, Client clientAccount, object trackingPolicy, User userAccount)
        {
            InitializeComponent();
            this.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            claimTrackingToUpdate = tracking;
            DateTime dt = (DateTime)claimTrackingToUpdate.Date;
            //string dtFormat = String.Format("{0:F}", dt);
            //lblDateTimeNow.Content = dtFormat;
            lblDateTimeNow.Content = dt.ToString("dd/MM/yyyy hh:mm");
            txtTracking.Text = claimTrackingToUpdate.TrackingContent;
            txtTracking.Focus();
            trackingContent = tracking.TrackingContent;
            client = clientAccount;
            policy = trackingPolicy;
            user = userAccount;
        }

        public frmTracking(LifeClaimTracking tracking, Client clientAccount, object trackingPolicy, User userAccount)
        {
            InitializeComponent();
            this.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            lifeClaimTrackingToUpdate = tracking;
            DateTime dt = (DateTime)lifeClaimTrackingToUpdate.Date;
            //string dtFormat = String.Format("{0:F}", dt);
            //lblDateTimeNow.Content = dtFormat;
            lblDateTimeNow.Content = dt.ToString("dd/MM/yyyy hh:mm");
            txtTracking.Text = lifeClaimTrackingToUpdate.TrackingContent;
            txtTracking.Focus();
            trackingContent = tracking.TrackingContent;
            client = clientAccount;
            policy = trackingPolicy;
            user = userAccount;
        }

        public frmTracking(PersonalAccidentsClaimTracking tracking, Client clientAccount, object trackingPolicy, User userAccount)
        {
            InitializeComponent();
            this.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            personalAccidentsClaimTrackingToUpdate = tracking;
            DateTime dt = (DateTime)personalAccidentsClaimTrackingToUpdate.Date;
            lblDateTimeNow.Content = dt.ToString("dd/MM/yyyy hh:mm");
            txtTracking.Text = personalAccidentsClaimTrackingToUpdate.TrackingContent;
            txtTracking.Focus();
            trackingContent = tracking.TrackingContent;
            client = clientAccount;
            policy = trackingPolicy;
            user = userAccount;
        }

        public frmTracking(ForeingWorkersClaimTracking tracking, Client clientAccount, object trackingPolicy, User userAccount)
        {
            InitializeComponent();
            this.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            foreingWorkersClaimTrackingToUpdate = tracking;
            DateTime dt = (DateTime)foreingWorkersClaimTrackingToUpdate.Date;
            lblDateTimeNow.Content = dt.ToString("dd/MM/yyyy hh:mm");
            txtTracking.Text = foreingWorkersClaimTrackingToUpdate.TrackingContent;
            txtTracking.Focus();
            trackingContent = tracking.TrackingContent;
            client = clientAccount;
            policy = trackingPolicy;
            user = userAccount;
        }

        public frmTracking(HealthClaimTracking tracking, Client clientAccount, object trackingPolicy, User userAccount)
        {
            InitializeComponent();
            this.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            healthClaimTrackingToUpdate = tracking;
            DateTime dt = (DateTime)healthClaimTrackingToUpdate.Date;
            //string dtFormat = String.Format("{0:F}", dt);
            //lblDateTimeNow.Content = dtFormat;
            lblDateTimeNow.Content = dt.ToString("dd/MM/yyyy hh:mm");
            txtTracking.Text = healthClaimTrackingToUpdate.TrackingContent;
            txtTracking.Focus();
            trackingContent = tracking.TrackingContent;
            client = clientAccount;
            policy = trackingPolicy;
            user = userAccount;
        }

        public frmTracking(TravelClaimTracking tracking, Client clientAccount, object trackingPolicy, User userAccount)
        {
            InitializeComponent();
            this.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            travelClaimTrackingToUpdate = tracking;
            DateTime dt = (DateTime)travelClaimTrackingToUpdate.Date;
            //string dtFormat = String.Format("{0:F}", dt);
            //lblDateTimeNow.Content = dtFormat;
            lblDateTimeNow.Content = dt.ToString("dd/MM/yyyy hh:mm");
            txtTracking.Text = travelClaimTrackingToUpdate.TrackingContent;
            txtTracking.Focus();
            trackingContent = tracking.TrackingContent;
            client = clientAccount;
            policy = trackingPolicy;
            user = userAccount;
        }

        public frmTracking(RenewalTracking tracking, Client clientAccount, object trackingPolicy, User userAccount)
        {
            InitializeComponent();
            this.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            renewalTrackingToUpdate = tracking;
            DateTime dt = (DateTime)renewalTrackingToUpdate.Date;
            //string dtFormat = String.Format("{0:F}", dt);
            //lblDateTimeNow.Content = dtFormat;
            lblDateTimeNow.Content = dt.ToString("dd/MM/yyyy hh:mm");
            txtTracking.Text = renewalTrackingToUpdate.TrackingContent;
            txtTracking.Focus();
            trackingContent = tracking.TrackingContent;
            client = clientAccount;
            policy = trackingPolicy;
            user = userAccount;
        }

        public frmTracking(ForeingWorkersRenewalTracking tracking, Client clientAccount, object trackingPolicy, User userAccount)
        {
            InitializeComponent();
            this.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            foreingWorkersRenewalTrackingToUpdate = tracking;
            DateTime dt = (DateTime)foreingWorkersRenewalTrackingToUpdate.Date;
            //string dtFormat = String.Format("{0:F}", dt);
            //lblDateTimeNow.Content = dtFormat;
            lblDateTimeNow.Content = dt.ToString("dd/MM/yyyy hh:mm");
            txtTracking.Text = foreingWorkersRenewalTrackingToUpdate.TrackingContent;
            txtTracking.Focus();
            trackingContent = tracking.TrackingContent;
            client = clientAccount;
            policy = trackingPolicy;
            user = userAccount;
        }

        public frmTracking(RequestTracking tracking)
        {
            InitializeComponent();
            this.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            requestTrackingToUpdate = tracking;
            DateTime dt = (DateTime)requestTrackingToUpdate.Date;
            //string dtFormat = String.Format("{0:F}", dt);
            //lblDateTimeNow.Content = dtFormat;
            lblDateTimeNow.Content = dt.ToString("dd/MM/yyyy hh:mm");
            txtTracking.Text = requestTrackingToUpdate.TrackingContent;
            txtTracking.Focus();
            trackingContent = tracking.TrackingContent;
            btnWorkQueque.Visibility = Visibility.Hidden;
        }

        public frmTracking(LifeTracking trackingToUpdate, Client client, object trackingPolicy, User handlerUser)
        {
            InitializeComponent();
            this.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            DateTime dt = (DateTime)trackingToUpdate.Date;
            lblDateTimeNow.Content = dt.ToString("dd/MM/yyyy hh:mm");
            lifeTrackingToUpdate = trackingToUpdate;
            txtTracking.Text = lifeTrackingToUpdate.TrackingContent;
            txtTracking.Focus();
            trackingContent = lifeTrackingToUpdate.TrackingContent;
            this.client = client;
            policy = trackingPolicy;
            user = handlerUser;
        }

        public frmTracking(HealthTracking trackingToUpdate, Client client, object trackingPolicy, User handlerUser)
        {
            InitializeComponent();
            this.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            DateTime dt = (DateTime)trackingToUpdate.Date;
            lblDateTimeNow.Content = dt.ToString("dd/MM/yyyy hh:mm");
            healthTrackingToUpdate = trackingToUpdate;
            txtTracking.Text = healthTrackingToUpdate.TrackingContent;
            txtTracking.Focus();
            trackingContent = healthTrackingToUpdate.TrackingContent;
            this.client = client;
            policy = trackingPolicy;
            user = handlerUser;
        }

        public frmTracking(FinanceTracking trackingToUpdate, Client client, object trackingPolicy, User user)
        {
            InitializeComponent();
            this.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            DateTime dt = (DateTime)trackingToUpdate.Date;
            lblDateTimeNow.Content = dt.ToString("dd/MM/yyyy hh:mm");
            financeTrackingToUpdate = trackingToUpdate;
            txtTracking.Text = financeTrackingToUpdate.TrackingContent;
            txtTracking.Focus();
            trackingContent = financeTrackingToUpdate.TrackingContent;
            this.client = client;
            policy = trackingPolicy;
            this.user = user;
        }

        public frmTracking(PersonalAccidentsTracking trackingToUpdate, Client client, object trackingPolicy, User user)
        {
            InitializeComponent();
            this.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            DateTime dt = (DateTime)trackingToUpdate.Date;
            lblDateTimeNow.Content = dt.ToString("dd/MM/yyyy hh:mm");
            this.personalAccidentsTrackingToUpdate = trackingToUpdate;
            txtTracking.Text = personalAccidentsTrackingToUpdate.TrackingContent;
            txtTracking.Focus();
            trackingContent = personalAccidentsTrackingToUpdate.TrackingContent;
            this.client = client;
            policy = trackingPolicy;
            this.user = user;
        }

        public frmTracking(TravelTracking trackingToUpdate, Client client, object trackingPolicy, User user)
        {
            InitializeComponent();
            this.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            DateTime dt = (DateTime)trackingToUpdate.Date;
            lblDateTimeNow.Content = dt.ToString("dd/MM/yyyy hh:mm");
            this.travelTrackingToUpdate = trackingToUpdate;
            txtTracking.Text = travelTrackingToUpdate.TrackingContent;
            txtTracking.Focus();
            trackingContent = travelTrackingToUpdate.TrackingContent;
            this.client = client;
            policy = trackingPolicy;
            this.user = user;
        }

        public frmTracking(ForeingWorkersTracking trackingToUpdate, Client client, object trackingPolicy, User user)
        {
            InitializeComponent();
            this.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            DateTime dt = (DateTime)trackingToUpdate.Date;
            lblDateTimeNow.Content = dt.ToString("dd/MM/yyyy hh:mm");
            this.foreingWorkersTrackingToUpdate = trackingToUpdate;
            txtTracking.Text = foreingWorkersTrackingToUpdate.TrackingContent;
            txtTracking.Focus();
            trackingContent = foreingWorkersTrackingToUpdate.TrackingContent;
            this.client = client;
            policy = trackingPolicy;
            this.user = user;
        }

        public frmTracking(StandardMoneyCollectionTracking tracking)
        {
            InitializeComponent();
            this.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            standardMoneyCollectionTrackingToUpdate = tracking;
            DateTime dt = (DateTime)standardMoneyCollectionTrackingToUpdate.Date;
            lblDateTimeNow.Content = dt.ToString("dd/MM/yyyy hh:mm");
            txtTracking.Text = standardMoneyCollectionTrackingToUpdate.TrackingContent;
            txtTracking.Focus();
            trackingContent = standardMoneyCollectionTrackingToUpdate.TrackingContent;
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            if (txtTracking.Text == "")
            {
                MessageBox.Show("נא למלאות את תוכן המעקב", "שגיאה", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            trackingContent = txtTracking.Text;
            Close();
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void btnWorkQueque_Click(object sender, RoutedEventArgs e)
        {
            frmWorkQueue queueWindow = new frmWorkQueue(client, user, policy, txtTracking.Text);
            queueWindow.ShowDialog();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (user != null && user.IsTasksPermission == false)
            {
                btnWorkQueque.IsEnabled = false;
            }
        }
    }
}
