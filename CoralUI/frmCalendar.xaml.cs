﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using CoralBusinessLogics;

namespace CoralUI
{
    /// <summary>
    /// Interaction logic for frmCalendar.xaml
    /// </summary>
    public partial class frmCalendar : Window
    {
        User user;
        public frmCalendar(User user)
        {
            InitializeComponent();
            this.user = user;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            // Create the interop host control.
            System.Windows.Forms.Integration.WindowsFormsHost host =
                new System.Windows.Forms.Integration.WindowsFormsHost();

            // Create the ActiveX control.
            //AxXtremeCalendarControl.AxCalendarControl axWmp = new AxXtremeCalendarControl.AxCalendarControl();
            //CalendarAxLib.CalendarAxControl axCalendar = new CalendarAxLib.CalendarAxControl(user);
            
            //WmpAxLib.WmpAxControl axWmp = new WmpAxLib.WmpAxControl();
            //AxWMPLib.AxWindowsMediaPlayer axWmp = new AxWMPLib.AxWindowsMediaPlayer();

            // Assign the ActiveX control as the host control's child.
            //host.Child = axCalendar;

            // Add the interop host control to the Grid 
            // control's collection of child controls. 
            this.grid1.Children.Add(host);

            // Play a .wav file with the ActiveX control.
            //axWmp.URL = @"C:\Windows\Media\tada.wav";

            
            
        }
    }
}
