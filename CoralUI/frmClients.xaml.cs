﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using CoralBusinessLogics;
using System.IO;
using System.Runtime.InteropServices;
using System.Windows.Xps.Packaging;
using System.Data;
using System.ComponentModel;
using System.Reflection;
using System.Windows.Controls.Primitives;
using System.Diagnostics;
using System.Threading;
using System.Windows.Media.Animation;
using Outlook = Microsoft.Office.Interop.Outlook;
using System.Web;
using ProdoctionImport.UI;

namespace CoralUI
{
    /// <summary>
    /// Interaction logic for frmClients.xaml
    /// </summary>
    public partial class frmClients : Window
    {
        UserLogic logic = new UserLogic();
        ClientsLogic clientsLogic = new ClientsLogic();
        PoliciesLogic policiesLogic = new PoliciesLogic();
        ClaimsLogic claimsLogic = new ClaimsLogic();
        LifeClaimsLogic lifeClaimsLogic = new LifeClaimsLogic();
        HealthClaimsLogic healthClaimsLogic = new HealthClaimsLogic();
        RenewalsLogic renewalLogic = new RenewalsLogic();
        WorkTasksLogic requestLogics = new WorkTasksLogic();
        LifePoliciesLogic lifePolicyLogic = new LifePoliciesLogic();
        HealthPoliciesLogic healthPolicyLogic = new HealthPoliciesLogic();
        FinancePoliciesLogic financePolicyLogic = new FinancePoliciesLogic();
        PersonalAccidentsLogic personalAccidentsLogic = new PersonalAccidentsLogic();
        PersonalAccidentsClaimsLogic personalAccidentsClaimLogic = new PersonalAccidentsClaimsLogic();
        TravelLogic travelLogic = new TravelLogic();
        TravelClaimsLogic travelClaimLogic = new TravelClaimsLogic();
        RolesLogic rolesLogic = new RolesLogic();
        User user = null;
        string path = null;
        Client client = null;
        bool isOffer;
        Client clientSelected = null;
        Client[] searchResults = null;
        Brush darkBlue;
        //Brush darkBlue;
        Brush green;
        frmClientProfile clientProfileWindow;
        ForeingWorkersLogic foreingWorkersLogic = new ForeingWorkersLogic();
        ForeingWorkersClaimsLogic foreingWorkersClaimLogic = new ForeingWorkersClaimsLogic();
        Email email = new Email();
        ExcelLogic excelLogic = new ExcelLogic();
        private Client clientTemp;

        //frmWorkQueue requestWindow = new frmWorkQueue();
        public frmClients(string username)
        {
            InitializeComponent();
            user = logic.FindUserByUsername(username);
            this.SizeToContent = SizeToContent.Manual;
            RequestBaloonBinding();
        }

        void requestWindow_RequestCountChange(object sender, EventArgs e)
        {
            RequestBaloonBinding();
            //lblBaloon.Content = requestLogics.GetActiveRequestsCount(user).ToString();
        }

        public void RequestBaloonBinding()
        {
            lblBaloon.Content = requestLogics.GetActiveRequestsCount(user).ToString();
        }

        //טעינת החלון
        private void Clients_Loaded(object sender, RoutedEventArgs e)
        {
            BrushConverter bc = new BrushConverter();
            //darkBlue = (Brush)bc.ConvertFrom("#FF68EA6E");
            darkBlue = (Brush)bc.ConvertFrom("#FF030F40");
            green = (Brush)bc.ConvertFrom("#66A503");
            tbItemExistingClients.IsSelected = true;
            cbCategoriesBinding();
            DataGridClientsBinding(false);
            if (dgExistingClients.ItemsSource != null)
            {
                dgExistingClients.SelectedIndex = 0;
            }
            lblHelloUsername.Content = user.Usermame + ", שלום";
            frmWorkQueue.RequestCountChange += new EventHandler(requestWindow_RequestCountChange);
            //txtScrollingBinding(null);
            SetPermissions();
        }

        private void SetPermissions()
        {
            if (!rolesLogic.IsUserInRole(user.Usermame, "מנהל ראשי") && rolesLogic.IsUserInRole(user.Usermame, "מנהל"))
            {
                miUsersManagement.IsEnabled = false;
            }
            else if (!rolesLogic.IsUserInRole(user.Usermame, "מנהל ראשי") && rolesLogic.IsUserInRole(user.Usermame, "עובד"))
            {
                miUsersManagement.IsEnabled = false;
            }
            if (user.IsCalendarPermission == false)
            {
                btnCalendar.IsEnabled = false;
            }
            if (user.IsClientProfilePermission == false)
            {
                btnClientProfil.IsEnabled = false;
                mItemClientProfile.IsEnabled = false;
            }
            if (user.IsCreateClientPermission == false)
            {
                btnAddClient.IsEnabled = false;
                mItemAddClient.IsEnabled = false;
                mItemAddNonActiveClient.IsEnabled = false;
                mItemAddPotentialClient.IsEnabled = false;
            }
            if (user.IsElementaryPermission == false)
            {
                tbItemElementary.IsEnabled = false;
                dgElementaryPolicies.ItemsSource = null;
                dgElementaryPolicies.IsEnabled = false;
            }
            if (user.IsFinancePermission == false)
            {
                tbItemFinance.IsEnabled = false;
            }
            if (user.IsForeingWorkersPermission == false)
            {
                tbItemForeingWorkers.IsEnabled = false;
            }
            if (user.IsHealthPermission == false)
            {
                tbItemHealth.IsEnabled = false;
            }
            if (user.IsLifePermission == false)
            {
                tbItemLife.IsEnabled = false;
            }
            if (user.IsOffersPermission == false)
            {
                tbItemOffers.IsEnabled = false;
                btnOffers.IsEnabled = false;
                btnClientOffers.IsEnabled = false;
                btnNewOffer.IsEnabled = false;
            }
            if (user.IsPersonalAccidentsPermission == false)
            {
                tbItemPersonalAccidents.IsEnabled = false;
            }
            if (user.IsRenewalsPermission == false)
            {
                btnRenewals.IsEnabled = false;
            }
            if (user.IsReportsPermission == false)
            {
                btnReports.IsEnabled = false;
            }
            if (user.IsScanPermission == false)
            {
                btnOpticArchive.IsEnabled = false;
                btnScan.IsEnabled = false;
            }
            if (user.IsTasksPermission == false)
            {
                btnMessages.IsEnabled = false;
                btnClientMessages.IsEnabled = false;
                mItemAddMessage.IsEnabled = false;
                mItemAddMessageNonActive.IsEnabled = false;
                mItemAddMessagePotential.IsEnabled = false;
                mItemNewTravelPolicyTask.IsEnabled = false;
                mItemNewPolicyTask.IsEnabled = false;
                mItemNewAccidentsPolicyTask.IsEnabled = false;
                mItemNewLifePolicyTask.IsEnabled = false;
                mItemNewHealthPolicyTask.IsEnabled = false;
                mItemNewForeingWorkersPolicyTask.IsEnabled = false;
                mItemNewFinancePolicyTask.IsEnabled = false;
            }
            if (user.IsTravelPermission == false)
            {
                tbItemTravel.IsEnabled = false;
            }
            if (user.IsElementaryPermission == false && user.IsForeingWorkersPermission == false && user.IsHealthPermission == false && user.IsLifePermission == false && user.IsPersonalAccidentsPermission == false && user.IsTravelPermission == false)
            {
                tbItemClaims.IsEnabled = false;
                btnAddClaim.IsEnabled = false;
                btnClaims.IsEnabled = false;
            }
        }

        //מסנכרן את טבלת ההצעות עם בסיס הנתונים
        private void DataGridOffersBinding(Client client)
        {
            List<object> allOffers = new List<object>();
            List<LifePolicy> lifeOffers;
            List<HealthPolicy> healthOffers;
            List<ElementaryPolicy> elementaryOffers;
            List<PersonalAccidentsPolicy> personalAccidentsOffers;
            List<TravelPolicy> travelOffers;
            List<FinancePolicy> financeOffers;
            List<ForeingWorkersPolicy> foreingWorkersOffers;
            DateTime now = DateTime.Today;
            if (client != null)
            {
                elementaryOffers = policiesLogic.GetAllOffersByClient(client);
                lifeOffers = lifePolicyLogic.GetAllLifeOffersByClient(client);
                healthOffers = healthPolicyLogic.GetAllHealthOffersByClient(client);
                personalAccidentsOffers = personalAccidentsLogic.GetAllPersonalAccidentsOffersByClient(client);
                travelOffers = travelLogic.GetAllTravelOffersByClient(client);
                financeOffers = financePolicyLogic.GetAllFinanceOffersByClient(client);
                foreingWorkersOffers = foreingWorkersLogic.GetAllForeingWorkersOffersByClient(client);

                if (cbStatus.SelectedIndex == 0)
                {
                    elementaryOffers = elementaryOffers.Where(c => c.EndDate >= now).ToList();
                    lifeOffers = lifeOffers.Where(c => c.EndDate >= now).ToList();
                    healthOffers = healthOffers.Where(c => c.EndDate >= now).ToList();
                    personalAccidentsOffers = personalAccidentsOffers.Where(c => c.EndDate >= now).ToList();
                    travelOffers = travelOffers.Where(c => c.EndDate >= now).ToList();
                    foreingWorkersOffers = foreingWorkersOffers.Where(c => c.EndDate >= now).ToList();
                }
                else if (cbStatus.SelectedIndex == 1)
                {
                    elementaryOffers = elementaryOffers.Where(c => c.EndDate < now).ToList();
                    lifeOffers = lifeOffers.Where(c => c.EndDate < now).ToList();
                    healthOffers = healthOffers.Where(c => c.EndDate < now).ToList();
                    personalAccidentsOffers = personalAccidentsOffers.Where(c => c.EndDate < now).ToList();
                    travelOffers = travelOffers.Where(c => c.EndDate < now).ToList();
                    foreingWorkersOffers = foreingWorkersOffers.Where(c => c.EndDate < now).ToList();
                }
            }
            else
            {
                elementaryOffers = policiesLogic.GetAllOffersByClient(new Client());
                lifeOffers = lifePolicyLogic.GetAllLifeOffersByClient(new Client());
                healthOffers = healthPolicyLogic.GetAllHealthOffersByClient(new Client());
                personalAccidentsOffers = personalAccidentsLogic.GetAllPersonalAccidentsOffersByClient(new Client());
                travelOffers = travelLogic.GetAllTravelOffersByClient(new Client());
                financeOffers = financePolicyLogic.GetAllFinanceOffersByClient(new Client());
                foreingWorkersOffers = foreingWorkersLogic.GetAllForeingWorkersOffersByClient(new Client());
            }
            if (txtSearchPolicies.Text != "")
            {
                string key = txtSearchPolicies.Text;
                elementaryOffers = elementaryOffers.Where(o => o.PolicyNumber.StartsWith(key)).ToList();
                personalAccidentsOffers = personalAccidentsOffers.Where(o => o.PolicyNumber.StartsWith(key)).ToList();
                travelOffers = travelOffers.Where(o => o.PolicyNumber.StartsWith(key)).ToList();
                lifeOffers = lifeOffers.Where(o => o.PolicyNumber.StartsWith(key)).ToList();
                healthOffers = healthOffers.Where(o => o.PolicyNumber.StartsWith(key)).ToList();
                financeOffers = financeOffers.Where(o => o.AssociateNumber.StartsWith(key)).ToList();
                foreingWorkersOffers = foreingWorkersOffers.Where(o => o.PolicyNumber.StartsWith(key)).ToList();
            }
            if (user.IsElementaryPermission != false)
            {
                foreach (var item in elementaryOffers)
                {
                    allOffers.Add(item);
                }
            }
            if (user.IsLifePermission != false)
            {
                foreach (var item in lifeOffers)
                {
                    allOffers.Add(item);
                }
            }
            if (user.IsHealthPermission != false)
            {
                foreach (var item in healthOffers)
                {
                    allOffers.Add(item);
                }
            }
            if (user.IsPersonalAccidentsPermission != false)
            {
                foreach (var item in personalAccidentsOffers)
                {
                    allOffers.Add(item);
                }
            }
            if (user.IsTravelPermission != false)
            {
                foreach (var item in travelOffers)
                {
                    allOffers.Add(item);
                }
            }
            if (user.IsFinancePermission != null)
            {
                foreach (var item in financeOffers)
                {
                    allOffers.Add(item);
                }
            }
            if (user.IsForeingWorkersPermission != false)
            {
                foreach (var item in foreingWorkersOffers)
                {
                    allOffers.Add(item);
                }
            }

            dgOffers.ItemsSource = allOffers;

        }

        //מסנכרן את טבלת הפוליסות עם בסיס הנתונים
        private void DataGridPoliciesBinding(Client client)
        {
            var elementaryPolicies = policiesLogic.GetAllActiveElementaryPoliciesByClient(client);

            if (elementaryPolicies != null && elementaryPolicies.Count() > 0)
            {
                ttElementary.Content = string.Format("מס' פוליסות אלמנטרי: {0}", elementaryPolicies.Count().ToString());
                ttElementary.ClearValue(ForegroundProperty);
                var carPolicies = elementaryPolicies.Where(p => p.InsuranceIndustry.InsuranceIndustryName.Contains("רכב"));
                if (carPolicies.Count() > 0)
                {
                    brCar.Background = darkBlue;
                    lblCar.Foreground = Brushes.White;
                    ttCar.Content = string.Format("מס' פוליסות רכב: {0}", carPolicies.Count().ToString());
                    ttCar.ClearValue(ForegroundProperty);
                }
                var aptPolicies = elementaryPolicies.Where(p => p.InsuranceIndustry.InsuranceIndustryName == "דירה");
                if (aptPolicies.Count() > 0)
                {
                    brApartment.Background = darkBlue;
                    lblApartment.Foreground = Brushes.White;
                    ttApartment.Content = string.Format("מס' פוליסות דירה: {0}", aptPolicies.Count().ToString());
                    ttApartment.ClearValue(ForegroundProperty);
                }
                var businessPolicies = elementaryPolicies.Where(p => p.InsuranceIndustry.InsuranceIndustryName == "עסק");
                if (businessPolicies.Count() > 0)
                {
                    brBusiness.Background = darkBlue;
                    lblBusiness.Foreground = Brushes.White;
                    ttBusiness.Content = string.Format("מס' פוליסות עסק: {0}", businessPolicies.Count().ToString());
                    ttBusiness.ClearValue(ForegroundProperty);
                }
            }


            if (cbStatus.SelectedIndex == 0)
            {
                dgElementaryPolicies.ItemsSource = elementaryPolicies;

            }
            else if (cbStatus.SelectedIndex == 1)
            {
                dgElementaryPolicies.ItemsSource = policiesLogic.GetAllNonActiveElementaryPoliciesByClient(client);
            }
            else
            {
                dgElementaryPolicies.ItemsSource = policiesLogic.GetAllElementaryPoliciesByClient(client);
            }
            if (txtSearchPolicies.Text != "")
            {
                dgElementaryPolicies.ItemsSource = ((List<ElementaryPolicy>)dgElementaryPolicies.ItemsSource).Where(p => (p.PolicyNumber != null && p.PolicyNumber.StartsWith(txtSearchPolicies.Text)) || (p.CarPolicy != null && p.CarPolicy.RegistrationNumber != null && p.CarPolicy.RegistrationNumber.StartsWith(txtSearchPolicies.Text))).ToList();
            }
        }

        //מסנכרן את קומבוה בוקס הקטגוריות עם בסיס הנתונים
        private void cbCategoriesBinding()
        {
            cbCategories.ItemsSource = clientsLogic.GetAllCategories();
            cbCategories.DisplayMemberPath = "CategoryName";
        }

        //מסנכרן את טבלאות הלקוחות עם בסיס הנתונים
        private void DataGridClientsBinding(bool isSearch)
        {
            int clientsCount = 0;
            //ברירת מחדל-כל הלקוחות לפי סוגים
            if (!isSearch)
            {
                if (tbItemExistingClients.IsSelected)
                {
                    searchResults = clientsLogic.GetAllClientsByType("לקוחות").ToArray();
                    dgExistingClients.ItemsSource = searchResults;
                }
                else if (tbItemNonActiveClients.IsSelected)
                {
                    searchResults = clientsLogic.GetAllClientsByType("לא פעילים").ToArray();
                    dgNonActiveClients.ItemsSource = searchResults;
                }
                else if (tbItemPotentialClients.IsSelected)
                {
                    searchResults = clientsLogic.GetAllClientsByType("פוטנציאליים").ToArray();
                    dgPotentialClients.ItemsSource = searchResults;
                }
                clientsCount = searchResults.Count();
            }
            //תוצאות חיפוש
            else
            {
                Client[] res = null;
                if (cbCategories.SelectedItem == null)
                {
                    res = searchResults.Where(c => c.LastName.Contains(txtSearchClients.Text) || c.FirstName.Contains(txtSearchClients.Text) || (c.LastName + " " + c.FirstName).Contains(txtSearchClients.Text) || (c.FirstName + " " + c.LastName).Contains(txtSearchClients.Text) || c.IdNumber.Contains(txtSearchClients.Text)).ToArray();
                }
                else
                {
                    res = searchResults.Where(c => c.CategoryID == ((Category)cbCategories.SelectedItem).CategoryID && (c.LastName.Contains(txtSearchClients.Text) || c.FirstName.Contains(txtSearchClients.Text) || (c.LastName + " " + c.FirstName).Contains(txtSearchClients.Text) || (c.FirstName + " " + c.LastName).Contains(txtSearchClients.Text) || c.IdNumber.StartsWith(txtSearchClients.Text))).ToArray();
                }
                if (tbItemExistingClients.IsSelected)
                {
                    //dgExistingClients.ItemsSource = clientsLogic.FindClientByString(txtSearchClients.Text, (Category)cbCategories.SelectedItem, "לקוחות");
                    dgExistingClients.UnselectAll();
                    dgExistingClients.ItemsSource = res;
                }
                else if (tbItemNonActiveClients.IsSelected)
                {
                    dgNonActiveClients.UnselectAll();
                    dgNonActiveClients.ItemsSource = res;
                }
                else if (tbItemPotentialClients.IsSelected)
                {
                    dgPotentialClients.UnselectAll();
                    dgPotentialClients.ItemsSource = res;
                }
                clientsCount = res.Count();
            }
            lblClientsCount.Content = string.Format("סה''כ לקוחות: {0}", clientsCount.ToString());
        }

        //פותח את תיקיית "המסמכים שלי" של המחשב
        private void btnDocuments_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string myDocumentsPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
                System.Diagnostics.Process.Start("explorer", myDocumentsPath);
            }
            catch (Exception)
            {
                MessageBox.Show("לא ניתן לפתוח את תיקיית המסמכים");
            }
        }

        //פותח חלון דו שיח לאישור או ביטול של סגירת התוכנה
        private void btnExit_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        //סוגר את כל החלונות הפתוחות
        private void Clients_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            frmCloseProgram closeProgram = new frmCloseProgram();
            closeProgram.ShowDialog();
            if (closeProgram.confirm == false)
            {
                for (int intCounter = App.Current.Windows.Count - 1; intCounter >= 0; intCounter--)
                {
                    if (App.Current.Windows[intCounter] != this)
                    {
                        App.Current.Windows[intCounter].Close();
                    }
                }

                return;
            }
            e.Cancel = true;
        }

        //פותח את חלון האלפון
        private void btnContacts_Click(object sender, RoutedEventArgs e)
        {
            frmContacts contactsWindow = new frmContacts();
            contactsWindow.Show();
        }

        //פותח את החלון להוספת לקוח חדש
        private void btnAddClient_Click(object sender, RoutedEventArgs e)
        {
            clientTemp = client;
            frmAddClient addClientWindow = new frmAddClient((TabItem)clientsTabs.SelectedItem, user);
            addClientWindow.ShowDialog();
            DataGridClientsBinding(false);
            cbCategoriesBinding();
            SetTabToDisplay(addClientWindow);

        }

        //מגדיר את הכרטסת להצגה
        private void SetTabToDisplay(frmAddClient addClientWindow)
        {
            if (addClientWindow.clientType == null)
            {
                if (clientTemp != null)
                {
                    if (clientTemp.ClientType.ClientTypeName == "לקוחות")
                    {
                        clientsTabs.SelectedIndex = 0;
                        SelectClientInDg(clientTemp, dgExistingClients);
                    }
                    else if (clientTemp.ClientType.ClientTypeName == "לא פעילים")
                    {
                        clientsTabs.SelectedIndex = 1;
                        SelectClientInDg(clientTemp, dgNonActiveClients);
                    }
                    else if (clientTemp.ClientType.ClientTypeName == "פוטנציאליים")
                    {
                        clientsTabs.SelectedIndex = 2;
                        SelectClientInDg(clientTemp, dgPotentialClients);
                    }
                }
                return;
            }
            if (addClientWindow.clientType.ClientTypeName == "לקוחות")
            {
                clientsTabs.SelectedIndex = 0;
                if (addClientWindow.clientAdded != null)
                {
                    SelectClientInDg(addClientWindow.clientAdded, dgExistingClients);
                }
                else if (addClientWindow.clientToUpdate != null)
                {
                    SelectClientInDg(addClientWindow.clientToUpdate, dgExistingClients);
                }

            }
            else if (addClientWindow.clientType.ClientTypeName == "לא פעילים")
            {
                clientsTabs.SelectedIndex = 1;
                if (addClientWindow.clientAdded != null)
                {
                    SelectClientInDg(addClientWindow.clientAdded, dgNonActiveClients);
                }
                else if (addClientWindow.clientToUpdate != null)
                {
                    SelectClientInDg(addClientWindow.clientToUpdate, dgNonActiveClients);
                }
                //if (addClientWindow.clientAdded != null)
                //{
                //    var dgItems = dgNonActiveClients.Items;
                //    foreach (var item in dgItems)
                //    {
                //        Client client = (Client)item;
                //        if (client.IdNumber == addClientWindow.clientAdded.IdNumber)
                //        {
                //            dgNonActiveClients.SelectedItem = item;
                //            dgNonActiveClients.ScrollIntoView(item);
                //            break;
                //        }
                //    }

                //}
            }
            else if (addClientWindow.clientType.ClientTypeName == "פוטנציאליים")
            {
                clientsTabs.SelectedIndex = 2;
                if (addClientWindow.clientAdded != null)
                {
                    SelectClientInDg(addClientWindow.clientAdded, dgPotentialClients);
                }
                else if (addClientWindow.clientToUpdate != null)
                {
                    SelectClientInDg(addClientWindow.clientToUpdate, dgPotentialClients);
                }
                //if (addClientWindow.clientAdded != null)
                //{
                //    var dgItems = dgPotentialClients.Items;
                //    foreach (var item in dgItems)
                //    {
                //        Client client = (Client)item;
                //        if (client.IdNumber == addClientWindow.clientAdded.IdNumber)
                //        {
                //            dgPotentialClients.SelectedItem = item;
                //            dgPotentialClients.ScrollIntoView(item);
                //            break;
                //        }
                //    }
                //}
            }

        }

        private void SelectClientInDg(Client clientToSelect, ListView lv)
        {
            var dgItems = lv.Items;
            foreach (var item in dgItems)
            {
                Client client = (Client)item;
                if (client.ClientID == clientToSelect.ClientID)
                {
                    lv.SelectedItem = item;
                    lv.ScrollIntoView(item);
                    ListViewItem listViewItem = lv.ItemContainerGenerator.ContainerFromItem(lv.SelectedItem) as ListViewItem;
                    if (listViewItem != null)
                    {
                        listViewItem.Focus();
                    }
                    break;
                }
            }
        }

        //פותח את הכרטסת של לקוח מסוים ונותן אפשרות לעדכן נתונים
        private void btnUpdateClient_Click(object sender, RoutedEventArgs e)
        {
            //frmAddClient addClientWindow = new frmAddClient((TabItem)clientsTabs.SelectedItem);
            //addClientWindow.ShowDialog();
            //DataGridClientsBinding(false);
            //cbCategoriesBinding();
            //SetTabToDisplay(addClientWindow);

            frmAddClient updateClientWindow = null;
            if (clientsTabs.SelectedIndex == 0)
            {
                if (dgExistingClients.SelectedItem == null)
                {
                    MessageBox.Show("נא לסמן את לקוח שברצונך לעדכן", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                }
                else
                {
                    updateClientWindow = new frmAddClient((Client)dgExistingClients.SelectedItem, user);
                    updateClientWindow.ShowDialog();
                }
            }
            else if (clientsTabs.SelectedIndex == 1)
            {
                if (dgNonActiveClients.SelectedItem == null)
                {
                    MessageBox.Show("נא לסמן את לקוח שברצונך לעדכן", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                }
                else
                {
                    updateClientWindow = new frmAddClient((Client)dgNonActiveClients.SelectedItem, user);
                    updateClientWindow.ShowDialog();
                }
            }
            else if (clientsTabs.SelectedIndex == 2)
            {
                if (dgPotentialClients.SelectedItem == null)
                {
                    MessageBox.Show("נא לסמן את לקוח שברצונך לעדכן", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                }
                else
                {
                    updateClientWindow = new frmAddClient((Client)dgPotentialClients.SelectedItem, user);
                    updateClientWindow.ShowDialog();
                }
            }
            if (updateClientWindow != null)
            {
                DataGridClientsBinding(false);
                if (!string.IsNullOrWhiteSpace(txtSearchClients.Text))
                {
                    DataGridClientsBinding(true);
                }
                cbCategoriesBinding();
                SetTabToDisplay(updateClientWindow);
            }
        }

        private void dgNonActiveClients_MouseDown(object sender, MouseButtonEventArgs e)
        {
            dgNonActiveClients.UnselectAll();
        }

        private void dgPotentialClients_MouseDown(object sender, MouseButtonEventArgs e)
        {
            dgPotentialClients.UnselectAll();
        }

        //פותח את הכרטסת של לקוח קיים מסוים ונותן אפשרות לעדכן נתונים
        private void dgExistingClients_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            DependencyObject src = (DependencyObject)(e.OriginalSource);
            while (!(src is Control))
            {
                if (!(src is Visual))
                {
                    break;
                }
                src = VisualTreeHelper.GetParent(src);
            }

            if ((src is Control && src.GetType() == typeof(ListViewItem)) || src.GetType() == typeof(Run))
            {
                frmAddClient updateClientWindow = null;
                if (dgExistingClients.SelectedItem != null)
                {
                    updateClientWindow = new frmAddClient((Client)dgExistingClients.SelectedItem, user);
                    updateClientWindow.ShowDialog();
                }
                else
                {
                    return;
                }
                if (updateClientWindow != null)
                {
                    DataGridClientsBinding(false);
                    if (!string.IsNullOrWhiteSpace(txtSearchClients.Text))
                    {
                        DataGridClientsBinding(true);
                    }
                    cbCategoriesBinding();
                    SetTabToDisplay(updateClientWindow);
                }
            }
        }

        //פותח את הכרטסת של לקוח לא פעיל מסוים ונותן אפשרות לעדכן נתונים
        private void dgNonActiveClients_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            DependencyObject src = (DependencyObject)(e.OriginalSource);
            while (!(src is Control))
            {
                if (!(src is Visual))
                {
                    break;
                }
                src = VisualTreeHelper.GetParent(src);
            }

            if ((src is Control && src.GetType() == typeof(ListViewItem)) || src.GetType() == typeof(Run))
            {
                frmAddClient updateClientWindow = null;
                if (dgNonActiveClients.SelectedItem == null)
                {
                    MessageBox.Show("נא לסמן את לקוח שברצונך לעדכן", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                }
                else
                {
                    updateClientWindow = new frmAddClient((Client)dgNonActiveClients.SelectedItem, user);
                    updateClientWindow.ShowDialog();
                }
                if (updateClientWindow != null)
                {
                    DataGridClientsBinding(false);
                    if (!string.IsNullOrWhiteSpace(txtSearchClients.Text))
                    {
                        DataGridClientsBinding(true);
                    }
                    cbCategoriesBinding();
                    SetTabToDisplay(updateClientWindow);
                }
            }
        }

        //פותח את הכרטסת של לקוח פוטנציאלי מסוים ונותן אפשרות לעדכן נתונים
        private void dgPotentialClients_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            DependencyObject src = (DependencyObject)(e.OriginalSource);
            while (!(src is Control))
            {
                if (!(src is Visual))
                {
                    break;
                }
                src = VisualTreeHelper.GetParent(src);
            }

            if ((src is Control && src.GetType() == typeof(ListViewItem)) || src.GetType() == typeof(Run))
            {
                frmAddClient updateClientWindow = null;
                if (dgPotentialClients.SelectedItem == null)
                {
                    MessageBox.Show("נא לסמן את לקוח שברצונך לעדכן", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                }
                else
                {
                    updateClientWindow = new frmAddClient((Client)dgPotentialClients.SelectedItem, user);
                    updateClientWindow.ShowDialog();
                }
                if (updateClientWindow != null)
                {
                    DataGridClientsBinding(false);
                    if (!string.IsNullOrWhiteSpace(txtSearchClients.Text))
                    {
                        DataGridClientsBinding(true);
                    }
                    cbCategoriesBinding();
                    SetTabToDisplay(updateClientWindow);
                }
            }
        }

        //מסנן את רשימת הלקוחות לפי הקטגוריה הנבחרת 
        private void cbCategories_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            DataGridClientsBinding(true);
        }

        //מסנן את רשימת הלקוחות לפי מילת חיפוש 
        private void txtSearchClients_TextChanged(object sender, TextChangedEventArgs e)
        {
            DataGridClientsBinding(true);
        }

        //מנקה את החיפוש ומחזיר את הטבלאות לברירת מחדל
        private void btnDisplayAll_Click(object sender, RoutedEventArgs e)
        {
            cbCategories.SelectedItem = null;
            cbCategories.Text = "הכל";
            txtSearchClients.Clear();
            tbItemExistingClients.IsSelected = true;
            DataGridClientsBinding(false);
            txtScrollingBinding(null);
        }

        //פותח את חלון חיפוש המתקדם של לקוחות עפ"י קרטריונים ומחזיר את התוצאות בטבלת הלקוחות
        private void btnAdvanceClientsSearch_Click(object sender, RoutedEventArgs e)
        {
            frmClientsAdvanceSearch searchWindow = new frmClientsAdvanceSearch((TabItem)clientsTabs.SelectedItem);
            searchWindow.ShowDialog();
            if (searchWindow.clientType == null || searchWindow.searchResult.Count == 0)
            {
                clientsTabs.SelectedIndex = 0;
                return;
            }
            if (searchWindow.clientType.ClientTypeName == "לקוחות")
            {
                clientsTabs.SelectedIndex = 0;
                dgExistingClients.ItemsSource = searchWindow.searchResult;
                dgExistingClients.SelectedItem = dgExistingClients.Items[0];
                dgExistingClients.ScrollIntoView(dgExistingClients.Items[0]);
                ListViewItem listViewItem = dgExistingClients.ItemContainerGenerator.ContainerFromIndex(0) as ListViewItem;
                listViewItem.Focus();
                if (searchWindow.PolicyNumber != null)
                {
                    var elementaryPolicies = policiesLogic.GetAllElementaryPoliciesByClient((Client)dgExistingClients.SelectedItem);
                    var ePolicy = elementaryPolicies.FirstOrDefault(p => p.PolicyNumber == searchWindow.PolicyNumber);
                    if (ePolicy != null)
                    {
                        if (ePolicy.EndDate < DateTime.Now)
                        {
                            cbStatus.SelectedIndex = 1;
                        }
                        else
                        {
                            cbStatus.SelectedIndex = 0;
                        }
                        var dgItems = dgElementaryPolicies.Items;
                        foreach (var item in dgItems)
                        {
                            ElementaryPolicy elementaryPolicy = (ElementaryPolicy)item;
                            if (elementaryPolicy.PolicyNumber == searchWindow.PolicyNumber)
                            {
                                dgElementaryPolicies.SelectedItem = item;
                                dgElementaryPolicies.ScrollIntoView(item);
                                break;
                            }
                        }
                    }
                    else
                    {
                        var lifePolicies = lifePolicyLogic.GetAllLifePoliciesByClient((Client)dgExistingClients.SelectedItem);
                        var lPolicy = lifePolicies.FirstOrDefault(p => p.PolicyNumber == searchWindow.PolicyNumber);
                        if (lPolicy != null)
                        {
                            if (lPolicy.EndDate < DateTime.Now)
                            {
                                cbStatus.SelectedIndex = 1;
                            }
                            else
                            {
                                cbStatus.SelectedIndex = 0;
                            }
                            tbItemLife.IsSelected = true;
                            var dgItems = dgLife.Items;
                            foreach (var item in dgItems)
                            {
                                LifePolicy lifePolicy = (LifePolicy)item;
                                if (lifePolicy.PolicyNumber == searchWindow.PolicyNumber)
                                {
                                    dgLife.SelectedItem = item;
                                    dgLife.ScrollIntoView(item);
                                    break;
                                }
                            }
                        }
                        else
                        {
                            var healthPolicies = healthPolicyLogic.GetAllHealthPoliciesByClient((Client)dgExistingClients.SelectedItem);
                            var hPolicy = healthPolicies.FirstOrDefault(p => p.PolicyNumber == searchWindow.PolicyNumber);
                            if (hPolicy != null)
                            {
                                if (hPolicy.EndDate < DateTime.Now)
                                {
                                    cbStatus.SelectedIndex = 1;
                                }
                                else
                                {
                                    cbStatus.SelectedIndex = 0;
                                }
                                tbItemHealth.IsSelected = true;
                                var dgItems = dgHealth.Items;
                                foreach (var item in dgItems)
                                {
                                    HealthPolicy healthPolicy = (HealthPolicy)item;
                                    if (healthPolicy.PolicyNumber == searchWindow.PolicyNumber)
                                    {
                                        dgHealth.SelectedItem = item;
                                        dgHealth.ScrollIntoView(item);
                                        break;
                                    }
                                }
                            }
                            else
                            {
                                var financePolicies = financePolicyLogic.GetAllFinancePoliciesByClient((Client)dgExistingClients.SelectedItem);
                                var fPolicy = financePolicies.FirstOrDefault(p => p.AssociateNumber == searchWindow.PolicyNumber);
                                if (fPolicy != null)
                                {
                                    if (fPolicy.EndDate < DateTime.Now)
                                    {
                                        cbStatus.SelectedIndex = 1;
                                    }
                                    else
                                    {
                                        cbStatus.SelectedIndex = 0;
                                    }
                                    tbItemFinance.IsSelected = true;
                                    var dgItems = dgFinance.Items;
                                    foreach (var item in dgItems)
                                    {
                                        FinancePolicy financePolicy = (FinancePolicy)item;
                                        if (financePolicy.AssociateNumber == searchWindow.PolicyNumber)
                                        {
                                            dgFinance.SelectedItem = item;
                                            dgFinance.ScrollIntoView(item);
                                            break;
                                        }
                                    }
                                }
                                else
                                {
                                    var travelPolicies = travelLogic.GetAllTravelPoliciesByClient((Client)dgExistingClients.SelectedItem);
                                    var tPolicy = travelPolicies.FirstOrDefault(p => p.PolicyNumber == searchWindow.PolicyNumber);
                                    if (tPolicy != null)
                                    {
                                        if (tPolicy.EndDate < DateTime.Now)
                                        {
                                            cbStatus.SelectedIndex = 1;
                                        }
                                        else
                                        {
                                            cbStatus.SelectedIndex = 0;
                                        }
                                        tbItemTravel.IsSelected = true;
                                        var dgItems = dgTravelPolicies.Items;
                                        foreach (var item in dgItems)
                                        {
                                            TravelPolicy travelPolicy = (TravelPolicy)item;
                                            if (travelPolicy.PolicyNumber == searchWindow.PolicyNumber)
                                            {
                                                dgTravelPolicies.SelectedItem = item;
                                                dgTravelPolicies.ScrollIntoView(item);
                                                break;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        var personalAccidentsPolicies = personalAccidentsLogic.GetAllPersonalAccidentsPoliciesByClient((Client)dgExistingClients.SelectedItem);
                                        var pPolicy = personalAccidentsPolicies.FirstOrDefault(p => p.PolicyNumber == searchWindow.PolicyNumber);
                                        if (pPolicy != null)
                                        {
                                            if (pPolicy.EndDate < DateTime.Now)
                                            {
                                                cbStatus.SelectedIndex = 1;
                                            }
                                            else
                                            {
                                                cbStatus.SelectedIndex = 0;
                                            }
                                            tbItemPersonalAccidents.IsSelected = true;
                                            var dgItems = dgPersonalAccidentsPolicies.Items;
                                            foreach (var item in dgItems)
                                            {
                                                PersonalAccidentsPolicy personalAccidentsPolicy = (PersonalAccidentsPolicy)item;
                                                if (personalAccidentsPolicy.PolicyNumber == searchWindow.PolicyNumber)
                                                {
                                                    dgPersonalAccidentsPolicies.SelectedItem = item;
                                                    dgPersonalAccidentsPolicies.ScrollIntoView(item);
                                                    break;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            var foreingWorkersPolicies = foreingWorkersLogic.GetAllForeingWorkersPoliciesByClient((Client)dgExistingClients.SelectedItem);
                                            var fwPolicy = foreingWorkersPolicies.FirstOrDefault(p => p.PolicyNumber == searchWindow.PolicyNumber);
                                            if (fwPolicy != null)
                                            {
                                                if (fwPolicy.EndDate < DateTime.Now)
                                                {
                                                    cbStatus.SelectedIndex = 1;
                                                }
                                                else
                                                {
                                                    cbStatus.SelectedIndex = 0;
                                                }
                                                tbItemForeingWorkers.IsSelected = true;
                                                var dgItems = dgForeingWorkersPolicies.Items;
                                                foreach (var item in dgItems)
                                                {
                                                    ForeingWorkersPolicy foreingWorkersPolicy = (ForeingWorkersPolicy)item;
                                                    if (foreingWorkersPolicy.PolicyNumber == searchWindow.PolicyNumber)
                                                    {
                                                        dgForeingWorkersPolicies.SelectedItem = item;
                                                        dgForeingWorkersPolicies.ScrollIntoView(item);
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                else if (searchWindow.CarNumber != null)
                {
                    var carPolicies = policiesLogic.GetAllCarPoliciesByClient((Client)dgExistingClients.SelectedItem);
                    var cPolicy = carPolicies.FirstOrDefault(p => p.RegistrationNumber == searchWindow.CarNumber && p.ElementaryPolicy.EndDate >= DateTime.Now);
                    if (cPolicy != null)
                    {
                        SelectElementaryPolicyByCarNumber(searchWindow.CarNumber);
                    }
                    else
                    {
                        var cNonActivePolicy = carPolicies.FirstOrDefault(p => p.RegistrationNumber == searchWindow.CarNumber && p.ElementaryPolicy.EndDate < DateTime.Now);
                        if (cNonActivePolicy != null)
                        {
                            cbStatus.SelectedIndex = 1;
                            SelectElementaryPolicyByCarNumber(searchWindow.CarNumber);
                        }

                    }
                }
            }
            else if (searchWindow.clientType.ClientTypeName == "לא פעילים")
            {
                clientsTabs.SelectedIndex = 1;
                dgNonActiveClients.ItemsSource = searchWindow.searchResult;
                dgNonActiveClients.SelectedItem = dgNonActiveClients.Items[0];
                dgNonActiveClients.ScrollIntoView(dgNonActiveClients.Items[0]);
                ListViewItem listViewItem = dgNonActiveClients.ItemContainerGenerator.ContainerFromIndex(0) as ListViewItem;
                listViewItem.Focus();
                if (searchWindow.PolicyNumber != null)
                {
                    var elementaryPolicies = policiesLogic.GetAllElementaryPoliciesByClient((Client)dgNonActiveClients.SelectedItem);
                    var ePolicy = elementaryPolicies.FirstOrDefault(p => p.PolicyNumber == searchWindow.PolicyNumber);
                    if (ePolicy != null)
                    {
                        if (ePolicy.EndDate < DateTime.Now)
                        {
                            cbStatus.SelectedIndex = 1;
                        }
                        else
                        {
                            cbStatus.SelectedIndex = 0;
                        }
                        var dgItems = dgElementaryPolicies.Items;
                        foreach (var item in dgItems)
                        {
                            ElementaryPolicy elementaryPolicy = (ElementaryPolicy)item;
                            if (elementaryPolicy.PolicyNumber == searchWindow.PolicyNumber)
                            {
                                dgElementaryPolicies.SelectedItem = item;
                                dgElementaryPolicies.ScrollIntoView(item);
                                break;
                            }
                        }
                    }
                    else
                    {
                        var lifePolicies = lifePolicyLogic.GetAllLifePoliciesByClient((Client)dgNonActiveClients.SelectedItem);
                        var lPolicy = lifePolicies.FirstOrDefault(p => p.PolicyNumber == searchWindow.PolicyNumber);
                        if (lPolicy != null)
                        {
                            if (lPolicy.EndDate < DateTime.Now)
                            {
                                cbStatus.SelectedIndex = 1;
                            }
                            else
                            {
                                cbStatus.SelectedIndex = 0;
                            }
                            tbItemLife.IsSelected = true;
                            var dgItems = dgLife.Items;
                            foreach (var item in dgItems)
                            {
                                LifePolicy lifePolicy = (LifePolicy)item;
                                if (lifePolicy.PolicyNumber == searchWindow.PolicyNumber)
                                {
                                    dgLife.SelectedItem = item;
                                    dgLife.ScrollIntoView(item);
                                    break;
                                }
                            }
                        }
                        else
                        {
                            var healthPolicies = healthPolicyLogic.GetAllHealthPoliciesByClient((Client)dgNonActiveClients.SelectedItem);
                            var hPolicy = healthPolicies.FirstOrDefault(p => p.PolicyNumber == searchWindow.PolicyNumber);
                            if (hPolicy != null)
                            {
                                if (hPolicy.EndDate < DateTime.Now)
                                {
                                    cbStatus.SelectedIndex = 1;
                                }
                                else
                                {
                                    cbStatus.SelectedIndex = 0;
                                }
                                tbItemHealth.IsSelected = true;
                                var dgItems = dgHealth.Items;
                                foreach (var item in dgItems)
                                {
                                    HealthPolicy healthPolicy = (HealthPolicy)item;
                                    if (healthPolicy.PolicyNumber == searchWindow.PolicyNumber)
                                    {
                                        dgHealth.SelectedItem = item;
                                        dgHealth.ScrollIntoView(item);
                                        break;
                                    }
                                }
                            }
                            else
                            {
                                var financePolicies = financePolicyLogic.GetAllFinancePoliciesByClient((Client)dgNonActiveClients.SelectedItem);
                                var fPolicy = financePolicies.FirstOrDefault(p => p.AssociateNumber == searchWindow.PolicyNumber);
                                if (fPolicy != null)
                                {
                                    if (fPolicy.EndDate < DateTime.Now)
                                    {
                                        cbStatus.SelectedIndex = 1;
                                    }
                                    else
                                    {
                                        cbStatus.SelectedIndex = 0;
                                    }
                                    tbItemFinance.IsSelected = true;
                                    var dgItems = dgFinance.Items;
                                    foreach (var item in dgItems)
                                    {
                                        FinancePolicy financePolicy = (FinancePolicy)item;
                                        if (financePolicy.AssociateNumber == searchWindow.PolicyNumber)
                                        {
                                            dgFinance.SelectedItem = item;
                                            dgFinance.ScrollIntoView(item);
                                            break;
                                        }
                                    }
                                }
                                else
                                {
                                    var travelPolicies = travelLogic.GetAllTravelPoliciesByClient((Client)dgNonActiveClients.SelectedItem);
                                    var tPolicy = travelPolicies.FirstOrDefault(p => p.PolicyNumber == searchWindow.PolicyNumber);
                                    if (tPolicy != null)
                                    {
                                        if (tPolicy.EndDate < DateTime.Now)
                                        {
                                            cbStatus.SelectedIndex = 1;
                                        }
                                        else
                                        {
                                            cbStatus.SelectedIndex = 0;
                                        }
                                        tbItemTravel.IsSelected = true;
                                        var dgItems = dgTravelPolicies.Items;
                                        foreach (var item in dgItems)
                                        {
                                            TravelPolicy travelPolicy = (TravelPolicy)item;
                                            if (travelPolicy.PolicyNumber == searchWindow.PolicyNumber)
                                            {
                                                dgTravelPolicies.SelectedItem = item;
                                                dgTravelPolicies.ScrollIntoView(item);
                                                break;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        var personalAccidentsPolicies = personalAccidentsLogic.GetAllPersonalAccidentsPoliciesByClient((Client)dgNonActiveClients.SelectedItem);
                                        var pPolicy = personalAccidentsPolicies.FirstOrDefault(p => p.PolicyNumber == searchWindow.PolicyNumber);
                                        if (pPolicy != null)
                                        {
                                            if (pPolicy.EndDate < DateTime.Now)
                                            {
                                                cbStatus.SelectedIndex = 1;
                                            }
                                            else
                                            {
                                                cbStatus.SelectedIndex = 0;
                                            }
                                            tbItemPersonalAccidents.IsSelected = true;
                                            var dgItems = dgPersonalAccidentsPolicies.Items;
                                            foreach (var item in dgItems)
                                            {
                                                PersonalAccidentsPolicy personalAccidentsPolicy = (PersonalAccidentsPolicy)item;
                                                if (personalAccidentsPolicy.PolicyNumber == searchWindow.PolicyNumber)
                                                {
                                                    dgPersonalAccidentsPolicies.SelectedItem = item;
                                                    dgPersonalAccidentsPolicies.ScrollIntoView(item);
                                                    break;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            var foreingWorkersPolicies = foreingWorkersLogic.GetAllForeingWorkersPoliciesByClient((Client)dgNonActiveClients.SelectedItem);
                                            var fwPolicy = foreingWorkersPolicies.FirstOrDefault(p => p.PolicyNumber == searchWindow.PolicyNumber);
                                            if (fwPolicy != null)
                                            {
                                                if (fwPolicy.EndDate < DateTime.Now)
                                                {
                                                    cbStatus.SelectedIndex = 1;
                                                }
                                                else
                                                {
                                                    cbStatus.SelectedIndex = 0;
                                                }
                                                tbItemForeingWorkers.IsSelected = true;
                                                var dgItems = dgForeingWorkersPolicies.Items;
                                                foreach (var item in dgItems)
                                                {
                                                    ForeingWorkersPolicy foreingWorkersPolicy = (ForeingWorkersPolicy)item;
                                                    if (foreingWorkersPolicy.PolicyNumber == searchWindow.PolicyNumber)
                                                    {
                                                        dgForeingWorkersPolicies.SelectedItem = item;
                                                        dgForeingWorkersPolicies.ScrollIntoView(item);
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                else if (searchWindow.CarNumber != null)
                {
                    var carPolicies = policiesLogic.GetAllCarPoliciesByClient((Client)dgNonActiveClients.SelectedItem);
                    var cPolicy = carPolicies.FirstOrDefault(p => p.RegistrationNumber == searchWindow.CarNumber);
                    if (cPolicy != null)
                    {
                        var dgItems = dgElementaryPolicies.Items;
                        foreach (var item in dgItems)
                        {
                            ElementaryPolicy elementaryPolicy = (ElementaryPolicy)item;
                            if (elementaryPolicy.CarPolicy != null && elementaryPolicy.CarPolicy.RegistrationNumber == searchWindow.CarNumber)
                            {
                                dgElementaryPolicies.SelectedItem = item;
                                dgElementaryPolicies.ScrollIntoView(item);
                                break;
                            }
                        }
                    }
                }
            }
            else if (searchWindow.clientType.ClientTypeName == "פוטנציאליים")
            {
                clientsTabs.SelectedIndex = 2;
                dgPotentialClients.ItemsSource = searchWindow.searchResult;
                dgPotentialClients.SelectedItem = dgPotentialClients.Items[0];
                dgPotentialClients.ScrollIntoView(dgPotentialClients.Items[0]);
                ListViewItem listViewItem = dgPotentialClients.ItemContainerGenerator.ContainerFromIndex(0) as ListViewItem;
                listViewItem.Focus();
            }
        }

        private void SelectElementaryPolicyByCarNumber(string carNumber)
        {
            var dgItems = dgElementaryPolicies.Items;
            foreach (var item in dgItems)
            {
                ElementaryPolicy elementaryPolicy = (ElementaryPolicy)item;
                if (elementaryPolicy.CarPolicy != null && elementaryPolicy.CarPolicy.RegistrationNumber == carNumber)
                {
                    dgElementaryPolicies.SelectedItem = item;
                    dgElementaryPolicies.ScrollIntoView(item);
                    break;
                }
            }
        }

        //פותח את חלון הוספת פוליסה חדשה ללקוח
        private void btnAddPolicy_Click(object sender, RoutedEventArgs e)
        {
            if (dgExistingClients.SelectedItem != null)
            {
                clientSelected = (Client)dgExistingClients.SelectedItem;
                if (clientSelected.PrincipalAgentID == null)
                {
                    MessageBox.Show("על מנת להמשיך, יש לשייך סוכן ראשי ללקוח ", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    frmAddClient updateClientWindow = new frmAddClient(clientSelected, user);
                    updateClientWindow.ShowDialog();
                    if (updateClientWindow != null)
                    {
                        DataGridClientsBinding(false);
                        cbCategoriesBinding();
                        SetTabToDisplay(updateClientWindow);
                    }
                    return;
                }
            }
            else
            {
                MessageBox.Show("נא לסמן לקוח", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            if (tbItemElementary.IsSelected)
            {
                isOffer = false;
                frmElementary addElementaryWindow = new frmElementary(clientSelected, user, isOffer);
                addElementaryWindow.ShowDialog();
                path = addElementaryWindow.path;
                DataGridPoliciesBinding(clientSelected);
                dgExistingClients.Focus();
            }
            else if (tbItemLife.IsSelected)
            {
                frmLifePolicy addLifePolicyWindow = new frmLifePolicy(clientSelected, user, false);
                addLifePolicyWindow.ShowDialog();
                dgLifeBinding();
            }
            else if (tbItemHealth.IsSelected)
            {
                frmHealthPolicy addHealthPolicyWindow = new frmHealthPolicy(clientSelected, user, false);
                addHealthPolicyWindow.ShowDialog();
                dgHealthBinding();
            }
            else if (tbItemFinance.IsSelected)
            {
                frmFinance addFinancePolicyWindow = new frmFinance(clientSelected, user, false);
                addFinancePolicyWindow.ShowDialog();
                dgFinanceBinding();
            }
            else if (tbItemPersonalAccidents.IsSelected)
            {
                frmPersonalAccidents addPersonalAccidentsWindow = new frmPersonalAccidents(clientSelected, user, false);
                addPersonalAccidentsWindow.ShowDialog();
                dgPersonalAccidentsPoliciesBinding();
            }
            else if (tbItemTravel.IsSelected)
            {
                frmTravel addTravelWindow = new frmTravel(clientSelected, user, false);
                addTravelWindow.ShowDialog();
                dgTravelPoliciesBinding();
            }
            else if (tbItemForeingWorkers.IsSelected)
            {
                frmForeignWorkers addForeingWorkersWindow = new frmForeignWorkers(clientSelected, user, false);
                addForeingWorkersWindow.ShowDialog();
                dgForeingWorkersBinding();
            }
        }

        private void dgForeingWorkersBinding()
        {
            var foreingWorkersPolicies = foreingWorkersLogic.GetAllActiveForeingWorkersPoliciesByClient(client);
            if (foreingWorkersPolicies != null && foreingWorkersPolicies.Count > 0)
            {
                ttForeingWorkers.Content = string.Format("מס' פוליסות עובדים זרים: {0}", foreingWorkersPolicies.Count().ToString());
                ttForeingWorkers.ClearValue(ForegroundProperty);
                brForeingWorkers.Background = darkBlue;
                lblForeingWorkers.Foreground = Brushes.White;
            }

            if (cbStatus.SelectedIndex == 0)
            {
                dgForeingWorkersPolicies.ItemsSource = foreingWorkersPolicies;
            }
            else if (cbStatus.SelectedIndex == 1)
            {
                dgForeingWorkersPolicies.ItemsSource = foreingWorkersLogic.GetNonActiveForeingWorkersPoliciesByClient(client);
            }
            else
            {
                dgForeingWorkersPolicies.ItemsSource = foreingWorkersLogic.GetAllForeingWorkersPoliciesByClient(client);
            }
            if (txtSearchPolicies.Text != "")
            {
                dgForeingWorkersPolicies.ItemsSource = ((List<ForeingWorkersPolicy>)dgForeingWorkersPolicies.ItemsSource).Where(p => p.PolicyNumber.StartsWith(txtSearchPolicies.Text)).ToList();
            }
        }

        private void dgTravelPoliciesBinding()
        {
            var travelPolicies = travelLogic.GetActiveTravelPoliciesByClient(client);
            if (travelPolicies != null && travelPolicies.Count > 0)
            {
                ttTravel.Content = string.Format("מס' פוליסות חו''ל: {0}", travelPolicies.Count().ToString());
                ttTravel.ClearValue(ForegroundProperty);
                brTravel.Background = darkBlue;
                lblTravel.Foreground = Brushes.White;
            }

            if (cbStatus.SelectedIndex == 0)
            {
                dgTravelPolicies.ItemsSource = travelPolicies;
            }
            else if (cbStatus.SelectedIndex == 1)
            {
                dgTravelPolicies.ItemsSource = travelLogic.GetNonActiveTravelPoliciesByClient(client);

            }
            else
            {
                dgTravelPolicies.ItemsSource = travelLogic.GetAllTravelPoliciesByClient(client);
            }
            if (txtSearchPolicies.Text != "")
            {
                dgTravelPolicies.ItemsSource = ((List<TravelPolicy>)dgTravelPolicies.ItemsSource).Where(p => p.PolicyNumber.StartsWith(txtSearchPolicies.Text)).ToList();
            }
        }

        private void dgPersonalAccidentsPoliciesBinding()
        {
            var personalAccidentsPolicies = personalAccidentsLogic.GetActivePersonalAccidentsPoliciesByClient(client);
            if (personalAccidentsPolicies != null && personalAccidentsPolicies.Count > 0)
            {
                ttPersonalAccidents.Content = string.Format("מס' פוליסות תאונות אישיות: {0}", personalAccidentsPolicies.Count().ToString());
                ttPersonalAccidents.ClearValue(ForegroundProperty);
                brPersonalAccidents.Background = darkBlue;
                lblPersonalAccidents.Foreground = Brushes.White;
            }

            if (cbStatus.SelectedIndex == 0)
            {
                dgPersonalAccidentsPolicies.ItemsSource = personalAccidentsPolicies;
            }
            else if (cbStatus.SelectedIndex == 1)
            {
                dgPersonalAccidentsPolicies.ItemsSource = personalAccidentsLogic.GetNonActivePersonalAccidentsPoliciesByClient(client);

            }
            else
            {
                dgPersonalAccidentsPolicies.ItemsSource = personalAccidentsLogic.GetAllPersonalAccidentsPoliciesByClient(client);
            }
            if (txtSearchPolicies.Text != "")
            {
                dgPersonalAccidentsPolicies.ItemsSource = ((List<PersonalAccidentsPolicy>)dgPersonalAccidentsPolicies.ItemsSource).Where(p => p.PolicyNumber.StartsWith(txtSearchPolicies.Text)).ToList();
            }
        }

        private void dgFinanceBinding()
        {
            var financePolicies = financePolicyLogic.GetAllActiveFinancePoliciesByClient(client);
            if (financePolicies != null && financePolicies.Count > 0)
            {
                ttFinance.Content = string.Format("מס' פוליסות פיננסים: {0}", financePolicies.Count().ToString());
                ttFinance.ClearValue(ForegroundProperty);
                var kerenHishtalmut = financePolicies.Where(p => p.FinanceProgramType.ProgramTypeName == "קרן השתלמות");
                if (kerenHishtalmut.Count() > 0)
                {
                    brKerenHishtalmut.Background = darkBlue;
                    ttKerenHishtalmut.Content = string.Format("מס' קרן השתלמות: {0}", kerenHishtalmut.Count().ToString());
                    ttKerenHishtalmut.ClearValue(ForegroundProperty);
                    lblKerenHishtalmut.Foreground = Brushes.White;
                }
                var kupatGuemel = financePolicies.Where(p => p.FinanceProgramType.ProgramTypeName == "קופת גמל");
                if (kupatGuemel.Count() > 0)
                {
                    brKupatGuemel.Background = darkBlue;
                    ttKupatGuemel.Content = string.Format("מס' קופת גמל: {0}", kupatGuemel.Count().ToString());
                    ttKupatGuemel.ClearValue(ForegroundProperty);
                    lblKupatGuemel.Foreground = Brushes.White;
                }
                var invesmentPortfolio = financePolicies.Where(p => p.FinanceProgramType.ProgramTypeName == "תיק השקעות");
                if (invesmentPortfolio.Count() > 0)
                {
                    brInvesmentPortfolio.Background = darkBlue;
                    ttInvesmentPortfolio.Content = string.Format("מס' תיק השקעות: {0}", invesmentPortfolio.Count().ToString());
                    ttInvesmentPortfolio.ClearValue(ForegroundProperty);
                    lblInvesmentFolder.Foreground = Brushes.White;
                }
            }
            //dgFinance.ItemsSource = financePolicies;
            if (cbStatus.SelectedIndex == 0)
            {
                dgFinance.ItemsSource = financePolicies;
            }
            else if (cbStatus.SelectedIndex == 1)
            {
                dgFinance.ItemsSource = financePolicyLogic.GetNonActiveFinancePoliciesByClient(client);

            }
            else
            {
                dgFinance.ItemsSource = financePolicyLogic.GetAllFinancePoliciesByClient(client);
            }
            if (txtSearchPolicies.Text != "")
            {
                dgFinance.ItemsSource = ((List<FinancePolicy>)dgFinance.ItemsSource).Where(p => p.AssociateNumber.StartsWith(txtSearchPolicies.Text)).ToList();
            }
        }

        private void dgHealthBinding()
        {
            var healthPolicies = healthPolicyLogic.GetActiveAllHealthPoliciesByClient(client);
            if (healthPolicies != null && healthPolicies.Count > 0)
            {
                ttHealth.Content = string.Format("מס' פוליסות בריאות: {0}", healthPolicies.Count().ToString());
                ttHealth.ClearValue(ForegroundProperty);
                brHealth.Background = darkBlue;
                lblHealth.Foreground = Brushes.White;
            }
            if (cbStatus.SelectedIndex == 0)
            {
                dgHealth.ItemsSource = healthPolicies;
            }
            else if (cbStatus.SelectedIndex == 1)
            {
                dgHealth.ItemsSource = healthPolicyLogic.GetNonActiveLifePoliciesByClient(client);
            }
            else
            {
                dgHealth.ItemsSource = healthPolicyLogic.GetAllHealthPoliciesByClient(client);
            }
            if (txtSearchPolicies.Text != "")
            {
                dgHealth.ItemsSource = ((List<HealthPolicy>)dgHealth.ItemsSource).Where(p => p.PolicyNumber.StartsWith(txtSearchPolicies.Text)).ToList();
            }
        }

        private void dgLifeBinding()
        {
            var lifePolicies = lifePolicyLogic.GetAllActiveLifePoliciesByClient(client);
            if (lifePolicies != null && lifePolicies.Count > 0)
            {
                ttLife.Content = string.Format("מס' פוליסות חיים: {0}", lifePolicies.Count().ToString());
                ttLife.ClearValue(ForegroundProperty);
                var managerPolicies = lifePolicies.Where(p => p.LifeIndustry.LifeIndustryName == "מנהלים");
                if (managerPolicies.Count() > 0)
                {
                    brManagers.Background = darkBlue;
                    ttManagers.Content = string.Format("מס' פוליסות מנהלים: {0}", managerPolicies.Count().ToString());
                    ttManagers.ClearValue(ForegroundProperty);
                    lblManager.Foreground = Brushes.White;
                }
                var risksPolicies = new List<Risk>();
                foreach (var item in lifePolicies)
                {
                    var risks = lifePolicyLogic.GetRisksByPolicy(item.LifePolicyID);
                    if (risks.Count() > 0)
                    {
                        //foreach (var risk in risks)
                        //{
                        risksPolicies.Add(risks.FirstOrDefault());
                        //}
                    }
                }
                if (risksPolicies.Count() > 0)
                {
                    brRisks.Background = darkBlue;
                    ttRisks.Content = string.Format("מס' פוליסות ריסק: {0}", risksPolicies.Count().ToString());
                    ttRisks.ClearValue(ForegroundProperty);
                    lblRisk.Foreground = Brushes.White;
                }
                var mortgagePolicies = lifePolicies.Where(p => p.LifeIndustry.LifeIndustryName == "משכנתא");
                if (mortgagePolicies.Count() > 0)
                {
                    brMortgage.Background = darkBlue;
                    ttMortgage.Content = string.Format("מס' פוליסות משכנתא: {0}", mortgagePolicies.Count().ToString());
                    ttMortgage.ClearValue(ForegroundProperty);
                    lblMortgage.Foreground = Brushes.White;
                }
                var privatePolicies = lifePolicies.Where(p => p.LifeIndustry.LifeIndustryName == "פרט");
                if (privatePolicies.Count() > 0)
                {
                    brPrivate.Background = darkBlue;
                    ttPrivate.Content = string.Format("מס' פוליסות פרט: {0}", privatePolicies.Count().ToString());
                    ttPrivate.ClearValue(ForegroundProperty);
                    lblPrivate.Foreground = Brushes.White;
                }
                var pensionPolicies = lifePolicies.Where(p => p.LifeIndustry.LifeIndustryName == "פנסיה");
                if (pensionPolicies.Count() > 0)
                {
                    brPension.Background = darkBlue;
                    ttPension.Content = string.Format("מס' פוליסות פנסיה: {0}", pensionPolicies.Count().ToString());
                    ttPension.ClearValue(ForegroundProperty);
                    lblPension.Foreground = Brushes.White;
                }
            }
            //dgLife.ItemsSource = lifePolicies;
            if (cbStatus.SelectedIndex == 0)
            {
                dgLife.ItemsSource = lifePolicies;

            }
            else if (cbStatus.SelectedIndex == 1)
            {
                //dgElementaryPolicies.ItemsSource = policiesLogic.GetAllNonActiveElementaryPoliciesByClient(client);
                dgLife.ItemsSource = lifePolicyLogic.GetNonActiveLifePoliciesByClient(client);
            }
            else
            {
                //dgElementaryPolicies.ItemsSource = policiesLogic.GetAllElementaryPoliciesByClient(client);
                dgLife.ItemsSource = lifePolicyLogic.GetAllLifePoliciesByClient(client);
            }
            if (txtSearchPolicies.Text != "")
            {
                dgLife.ItemsSource = ((List<LifePolicy>)dgLife.ItemsSource).Where(p => p.PolicyNumber.StartsWith(txtSearchPolicies.Text)).ToList();
            }
        }

        //מרשה/אוסר לחיצה על כפתור הוספת פוליסה חדשה לפי סוג הלקוח הנבחר
        private void clientsTabs_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.Source is TabControl)
            {
                if (this.IsLoaded)
                {
                    //e.Handled = true;
                    cbStatus.SelectedIndex = 0;
                    tbItemElementary.Focus();
                    txtSearchClients.Clear();
                    dgExistingClients.UnselectAll();
                    dgPotentialClients.UnselectAll();
                    dgNonActiveClients.UnselectAll();
                    DataGridClientsBinding(false);
                    if (tbItemNonActiveClients.IsSelected)
                    {
                        btnAddPolicy.IsEnabled = false;
                        cbStatus.SelectedIndex = 1;
                    }
                    else if (tbItemPotentialClients.IsSelected)
                    {
                        btnAddPolicy.IsEnabled = false;
                        tbItemOffers.Focus();
                    }
                    else
                    {
                        btnAddPolicy.IsEnabled = true;
                    }
                }
            }
        }

        private void DataGridClaimsBinding(Client client)
        {
            List<object> allClaims = new List<object>();
            List<LifeClaim> lifeClaims;
            List<HealthClaim> healthClaims;
            List<ElementaryClaim> elementaryClaims;
            List<PersonalAccidentsClaim> personalAccidentsClaims;
            List<TravelClaim> travelClaims;
            List<ForeingWorkersClaim> foreingWorkersClaims;

            if (client != null)
            {
                elementaryClaims = claimsLogic.GetAllElementaryClaimsByClient(client.ClientID);
                lifeClaims = lifeClaimsLogic.GetAllLifeClaimsByClient(client.ClientID);
                healthClaims = healthClaimsLogic.GetAllHealthClaimsByClient(client.ClientID);
                personalAccidentsClaims = personalAccidentsClaimLogic.GetAllPersonalAccidentsClaimsByClient(client.ClientID);
                travelClaims = travelClaimLogic.GetAllTravelClaimsByClient(client.ClientID);
                foreingWorkersClaims = foreingWorkersClaimLogic.GetAllForeingWorkersClaimsByClient(client.ClientID);

                if (cbStatus.SelectedIndex == 0)
                {
                    elementaryClaims = elementaryClaims.Where(c => c.ClaimStatus == true).ToList();
                    lifeClaims = lifeClaims.Where(c => c.ClaimStatus == true).ToList();
                    healthClaims = healthClaims.Where(c => c.ClaimStatus == true).ToList();
                    personalAccidentsClaims = personalAccidentsClaims.Where(c => c.ClaimStatus == true).ToList();
                    travelClaims = travelClaims.Where(c => c.ClaimStatus == true).ToList();
                    foreingWorkersClaims = foreingWorkersClaims.Where(c => c.ClaimStatus == true).ToList();
                }
                else if (cbStatus.SelectedIndex == 1)
                {
                    elementaryClaims = elementaryClaims.Where(c => c.ClaimStatus == false).ToList();
                    lifeClaims = lifeClaims.Where(c => c.ClaimStatus == false).ToList();
                    healthClaims = healthClaims.Where(c => c.ClaimStatus == false).ToList();
                    personalAccidentsClaims = personalAccidentsClaims.Where(c => c.ClaimStatus == false).ToList();
                    travelClaims = travelClaims.Where(c => c.ClaimStatus == false).ToList();
                    foreingWorkersClaims = foreingWorkersClaims.Where(c => c.ClaimStatus == false).ToList();
                }
            }
            else
            {
                elementaryClaims = claimsLogic.GetAllElementaryClaimsByClient(0);
                lifeClaims = lifeClaimsLogic.GetAllLifeClaimsByClient(0);
                healthClaims = healthClaimsLogic.GetAllHealthClaimsByClient(0);
                personalAccidentsClaims = personalAccidentsClaimLogic.GetAllPersonalAccidentsClaimsByClient(0);
                travelClaims = travelClaimLogic.GetAllTravelClaimsByClient(0);
                foreingWorkersClaims = foreingWorkersClaimLogic.GetAllForeingWorkersClaimsByClient(0);

            }
            if (user.IsElementaryPermission != false)
            {
                foreach (var item in elementaryClaims)
                {
                    allClaims.Add(item);
                }
            }
            if (user.IsLifePermission != false)
            {
                foreach (var item in lifeClaims)
                {
                    allClaims.Add(item);
                }
            }
            if (user.IsHealthPermission != false)
            {
                foreach (var item in healthClaims)
                {
                    allClaims.Add(item);
                }
            }
            if (user.IsPersonalAccidentsPermission != false)
            {
                foreach (var item in personalAccidentsClaims)
                {
                    allClaims.Add(item);
                }
            }
            if (user.IsForeingWorkersPermission != false)
            {
                foreach (var item in foreingWorkersClaims)
                {
                    allClaims.Add(item);
                }
            }
            if (user.IsTravelPermission != false)
            {
                foreach (var item in travelClaims)
                {
                    allClaims.Add(item);
                }
            }
            dgClientClaims.ItemsSource = allClaims;
        }

        //מייצג את הפוליסות של הלקוח המסומן
        private void dgExistingClients_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ClearClientChart();
            txtSearchPolicies.Clear();
            client = (Client)dgExistingClients.SelectedItem;
            txtScrollingBinding(client);
            DataGridPoliciesBinding(client);
            DataGridOffersBinding(client);
            DataGridClaimsBinding(client);
            dgLifeBinding();
            dgHealthBinding();
            dgFinanceBinding();
            dgPersonalAccidentsPoliciesBinding();
            dgTravelPoliciesBinding();
            dgForeingWorkersBinding();
        }

        private void txtScrollingBinding(Client client)
        {
            Storyboard sb = (Storyboard)(this.FindName("sbScrollingText"));
            if (client != null)
            {
                string birthdayLabel = "";
                System.Text.StringBuilder requestsLabel = new System.Text.StringBuilder();

                //string requestsLabel = "";
                if (client.BirthDate != null && ((DateTime)client.BirthDate).Month == DateTime.Now.Month && ((DateTime)client.BirthDate).Day == DateTime.Now.Day)
                {
                    birthdayLabel = string.Format("היום יום הולדת ל{0} {1}    ", client.FirstName, client.LastName);
                }
                var requests = requestLogics.GetAllRequestsByClient(client.ClientID, user).Where(r => r.RequestStatus == true).Select(r => r.RequestSubject);
                if (requests != null && requests.Count() > 0)
                {
                    requestsLabel.Append("משימות לקוח:   ");
                }
                foreach (var item in requests)
                {
                    requestsLabel.Append(item + "   *   ");
                }
                txtScrolling.Text = string.Format("{0}  {1}", birthdayLabel, requestsLabel);
                sb.Begin();
            }
            else
            {

                System.Text.StringBuilder birthdaysLabel = new System.Text.StringBuilder();
                birthdaysLabel.Append("היום יום הולדת ל:   ");
                var birthdays = clientsLogic.GetAllClientsByType("לקוחות").Where(c => c.BirthDate != null && ((DateTime)c.BirthDate).Day == DateTime.Now.Day && ((DateTime)c.BirthDate).Month == DateTime.Now.Month);
                if (birthdays != null && birthdays.Count() > 0)
                {
                    foreach (var item in birthdays)
                    {
                        birthdaysLabel.Append(item.FirstName + " " + item.LastName + "   *   ");
                    }
                    txtScrolling.Text = birthdaysLabel.ToString();
                    sb.Begin();
                }
                else
                {
                    txtScrolling.Text = "";
                }
            }
        }

        private void ClearClientChart()
        {
            ttElementary.Content = "אין ללקוח פוליסות אלמנטרי";
            ttElementary.Foreground = Brushes.Red;
            ttApartment.Content = "אין ללקוח פוליסות דירה";
            ttApartment.Foreground = Brushes.Red;
            ttBusiness.Content = "אין ללקוח פוליסות עסק";
            ttBusiness.Foreground = Brushes.Red;
            ttCar.Content = "אין ללקוח פוליסות רכב";
            ttCar.Foreground = Brushes.Red;
            ttFinance.Content = "אין ללקוח פוליסות פיננסים";
            ttFinance.Foreground = Brushes.Red;
            ttHealth.Content = "אין ללקוח פוליסות בריאות";
            ttHealth.Foreground = Brushes.Red;
            ttInvesmentPortfolio.Content = "אין ללקוח תיק השקעות";
            ttInvesmentPortfolio.Foreground = Brushes.Red;
            ttKerenHishtalmut.Content = "אין ללקוח קרן השתלמות";
            ttKerenHishtalmut.Foreground = Brushes.Red;
            ttKupatGuemel.Content = "אין ללקוח קופת גמל";
            ttKupatGuemel.Foreground = Brushes.Red;
            ttLife.Content = "אין ללקוח פוליסות חיים";
            ttLife.Foreground = Brushes.Red;
            ttManagers.Content = "אין ללקוח פוליסות מנהלים";
            ttManagers.Foreground = Brushes.Red;
            ttMortgage.Content = "אין ללקוח פוליסות משכנתא";
            ttMortgage.Foreground = Brushes.Red;
            ttPension.Content = "אין ללקוח פוליסות פנסיה";
            ttPension.Foreground = Brushes.Red;
            ttPersonalAccidents.Content = "אין ללקוח פוליסות תאונות אישיות";
            ttPersonalAccidents.Foreground = Brushes.Red;
            ttPrivate.Content = "אין ללקוח פוליסות פרט";
            ttPrivate.Foreground = Brushes.Red;
            ttRisks.Content = "אין ללקוח פוליסות ריסק";
            ttRisks.Foreground = Brushes.Red;
            ttForeingWorkers.Content = "אין ללקוח פוליסות עובדים זרים";
            ttForeingWorkers.Foreground = Brushes.Red;
            ttTravel.Content = "אין ללקוח פוליסות חו''ל";
            ttTravel.Foreground = Brushes.Red;
            brCar.Background = Brushes.White;
            brApartment.Background = Brushes.White;
            brBusiness.Background = Brushes.White;
            brHealth.Background = Brushes.White;
            brInvesmentPortfolio.Background = Brushes.White;
            brKerenHishtalmut.Background = Brushes.White;
            brKupatGuemel.Background = Brushes.White;
            brManagers.Background = Brushes.White;
            brMortgage.Background = Brushes.White;
            brPension.Background = Brushes.White;
            brPersonalAccidents.Background = Brushes.White;
            brPrivate.Background = Brushes.White;
            brRisks.Background = Brushes.White;
            brForeingWorkers.Background = Brushes.White;
            brTravel.Background = Brushes.White;
            lblCar.Foreground = darkBlue;
            lblApartment.Foreground = darkBlue;
            lblBusiness.Foreground = darkBlue;
            lblHealth.Foreground = darkBlue;
            lblInvesmentFolder.Foreground = darkBlue;
            lblKerenHishtalmut.Foreground = darkBlue;
            lblKupatGuemel.Foreground = darkBlue;
            lblManager.Foreground = darkBlue;
            lblMortgage.Foreground = darkBlue;
            lblPension.Foreground = darkBlue;
            lblPersonalAccidents.Foreground = darkBlue;
            lblPrivate.Foreground = darkBlue;
            lblRisk.Foreground = darkBlue;
            lblForeingWorkers.Foreground = darkBlue;
            lblTravel.Foreground = darkBlue;
        }


        //מייצג את הפוליסות של הלקוח המסומן
        private void dgNonActiveClients_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            txtSearchPolicies.Clear();
            client = (Client)dgNonActiveClients.SelectedItem;
            DataGridPoliciesBinding(client);
            DataGridOffersBinding(client);
            DataGridClaimsBinding(client);
            dgLifeBinding();
            dgFinanceBinding();
            dgHealthBinding();
            dgTravelPoliciesBinding();
            dgPersonalAccidentsPoliciesBinding();
            dgForeingWorkersBinding();
        }

        //מייצג את הפוליסות של הלקוח המסומן
        private void dgPotentialClients_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            txtSearchPolicies.Clear();
            client = (Client)dgPotentialClients.SelectedItem;
            DataGridPoliciesBinding(client);
            DataGridOffersBinding(client);
            DataGridClaimsBinding(client);
            dgLifeBinding();
            dgHealthBinding();
            dgFinanceBinding();
            dgTravelPoliciesBinding();
            dgPersonalAccidentsPoliciesBinding();
            dgForeingWorkersBinding();
        }

        private void btnOpticArchive_Click(object sender, RoutedEventArgs e)
        {
            if (dgExistingClients.SelectedItem == null && dgNonActiveClients.SelectedItem == null && dgPotentialClients.SelectedItem == null)
            {
                MessageBox.Show("נא לסמן לקוח", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            frmScan2 archiveWindow = new frmScan2(client, null, null, user);
            archiveWindow.Closed += (object o, EventArgs ea) =>
            {
                OnChildWindowClosed();
            };
            archiveWindow.ShowDialog();
        }

        private void OnChildWindowClosed()
        {
            Client clientSelected=null;
            if (client!=null)
            {
                clientSelected = clientsLogic.GetClientByClientID(client.ClientID);
            }            
            //bool isSearch = false;
            //if (!string.IsNullOrWhiteSpace(txtSearchClients.Text))
            //{
            //    isSearch = true;
            //}
            //txtSearchClients.Clear();
            DataGridClientsBinding(false);
            if (!string.IsNullOrWhiteSpace(txtSearchClients.Text))
            {
                DataGridClientsBinding(true);
            }
            if (clientSelected!=null)
            {
                if (tbItemExistingClients.IsSelected)
                {
                    SelectClientInDg(clientSelected, dgExistingClients);
                }
                else if (tbItemNonActiveClients.IsSelected)
                {
                    SelectClientInDg(clientSelected, dgNonActiveClients);
                }
                else if (tbItemPotentialClients.IsSelected)
                {
                    SelectClientInDg(clientSelected, dgPotentialClients);
                } 
            }
            
        }

        private void btnUpdatePolicy_Click(object sender, RoutedEventArgs e)
        {
            //frmAddClient updateClientWindow = null;

            if (tbItemElementary.IsSelected)
            {
                if (dgElementaryPolicies.SelectedItem == null)
                {
                    MessageBox.Show("נא לסמן את הפוליסה שברצונך לעדכן", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                }
                else
                {
                    frmElementary updateElementaryPolicy = new frmElementary((ElementaryPolicy)dgElementaryPolicies.SelectedItem, user, false, client);
                    updateElementaryPolicy.ShowDialog();
                    path = updateElementaryPolicy.path;
                }
                DataGridPoliciesBinding((Client)dgExistingClients.SelectedItem);
            }
            else if (tbItemLife.IsSelected)
            {
                if (dgLife.SelectedItem == null)
                {
                    MessageBox.Show("נא לסמן את הפוליסה שברצונך לעדכן", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                }
                else
                {
                    frmLifePolicy updateLifePolicy = new frmLifePolicy(client, user, false, (LifePolicy)dgLife.SelectedItem);
                    updateLifePolicy.ShowDialog();
                }
                //DataGridPoliciesBinding((Client)dgExistingClients.SelectedItem);
                dgLifeBinding();
            }
            else if (tbItemOffers.IsSelected)
            {
                //if (dgOffers.SelectedItem == null)
                //{
                //    MessageBox.Show("נא לסמן את ההצעה שברצונך לעדכן", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                //}
                //else
                //{
                //    frmElementary updateElementaryPolicy = new frmElementary((ElementaryPolicy)dgOffers.SelectedItem, user, false, client);
                //    updateElementaryPolicy.ShowDialog();
                //    path = updateElementaryPolicy.path;
                //}
                //DataGridOffersBinding(client);
                UpdateOffer();
            }
            else if (tbItemHealth.IsSelected)
            {
                if (dgHealth.SelectedItem == null)
                {
                    MessageBox.Show("נא לסמן את הפוליסה שברצונך לעדכן", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                }
                else
                {
                    frmHealthPolicy updateHealthPolicy = new frmHealthPolicy(client, user, false, (HealthPolicy)dgHealth.SelectedItem);
                    updateHealthPolicy.ShowDialog();
                }
                //DataGridPoliciesBinding((Client)dgExistingClients.SelectedItem);
                dgHealthBinding();
            }
            else if (tbItemFinance.IsSelected)
            {
                if (dgFinance.SelectedItem == null)
                {
                    MessageBox.Show("נא לסמן את הפוליסה שברצונך לעדכן", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                }
                else
                {
                    frmFinance updateFinancePolicy = new frmFinance(client, user, false, (FinancePolicy)dgFinance.SelectedItem);
                    updateFinancePolicy.ShowDialog();
                }
                //DataGridPoliciesBinding((Client)dgExistingClients.SelectedItem);
                dgFinanceBinding();
            }
            else if (tbItemPersonalAccidents.IsSelected)
            {
                if (dgPersonalAccidentsPolicies.SelectedItem == null)
                {
                    MessageBox.Show("נא לסמן את הפוליסה שברצונך לעדכן", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                }
                else
                {
                    frmPersonalAccidents updatePersonalAccidentsPolicy = new frmPersonalAccidents(client, user, false, (PersonalAccidentsPolicy)dgPersonalAccidentsPolicies.SelectedItem);
                    updatePersonalAccidentsPolicy.ShowDialog();
                }
                //DataGridPoliciesBinding((Client)dgExistingClients.SelectedItem);
                dgPersonalAccidentsPoliciesBinding();
            }
            else if (tbItemTravel.IsSelected)
            {
                if (dgTravelPolicies.SelectedItem == null)
                {
                    MessageBox.Show("נא לסמן את הפוליסה שברצונך לעדכן", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                }
                else
                {
                    frmTravel updateTravelPolicy = new frmTravel(client, user, false, (TravelPolicy)dgTravelPolicies.SelectedItem);
                    updateTravelPolicy.ShowDialog();
                }
                //DataGridPoliciesBinding((Client)dgExistingClients.SelectedItem);
                dgTravelPoliciesBinding();
            }
            else if (tbItemForeingWorkers.IsSelected)
            {
                if (dgForeingWorkersPolicies.SelectedItem == null)
                {
                    MessageBox.Show("נא לסמן את הפוליסה שברצונך לעדכן", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                }
                else
                {
                    frmForeignWorkers updateForeingWorkersPolicy = new frmForeignWorkers(client, user, false, (ForeingWorkersPolicy)dgForeingWorkersPolicies.SelectedItem);
                    updateForeingWorkersPolicy.ShowDialog();
                }
                //DataGridPoliciesBinding((Client)dgExistingClients.SelectedItem);
                dgForeingWorkersBinding();
            }
            //if (clientsTabs.SelectedIndex == 0)
            //{
            //    if (dgExistingClients.SelectedItem == null)
            //    {
            //        MessageBox.Show("נא לסמן את לקוח שברצונך לעדכן", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            //    }
            //    else
            //    {
            //        updateClientWindow = new frmAddClient((Client)dgExistingClients.SelectedItem);
            //        updateClientWindow.ShowDialog();
            //    }
            //}
            //else if (clientsTabs.SelectedIndex == 1)
            //{
            //    if (dgNonActiveClients.SelectedItem == null)
            //    {
            //        MessageBox.Show("נא לסמן את לקוח שברצונך לעדכן", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            //    }
            //    else
            //    {
            //        updateClientWindow = new frmAddClient((Client)dgNonActiveClients.SelectedItem);
            //        updateClientWindow.ShowDialog();
            //    }
            //}
            //else if (clientsTabs.SelectedIndex == 2)
            //{
            //    if (dgPotentialClients.SelectedItem == null)
            //    {
            //        MessageBox.Show("נא לסמן את לקוח שברצונך לעדכן", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            //    }
            //    else
            //    {
            //        updateClientWindow = new frmAddClient((Client)dgPotentialClients.SelectedItem);
            //        updateClientWindow.ShowDialog();
            //    }
            //}
            //if (updateClientWindow != null)
            //{
            //    DataGridClientsBinding(false, null);
            //    cbCategoriesBinding();
            //    SetTabToDisplay(updateClientWindow);
            //}
        }

        private void dgElementaryPolicies_MouseDown(object sender, MouseButtonEventArgs e)
        {
            dgElementaryPolicies.UnselectAll();
        }

        private void dgLife_MouseDown(object sender, MouseButtonEventArgs e)
        {
            dgLife.UnselectAll();
        }

        private void dgHealth_MouseDown(object sender, MouseButtonEventArgs e)
        {
            dgHealth.UnselectAll();
        }

        private void dgFinance_MouseDown(object sender, MouseButtonEventArgs e)
        {
            dgFinance.UnselectAll();
        }

        private void dgClientClaims_MouseDown(object sender, MouseButtonEventArgs e)
        {
            dgClientClaims.UnselectAll();
        }



        private void dgElementaryPolicies_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            DependencyObject src = (DependencyObject)(e.OriginalSource);
            while (!(src is Control))
            {
                if (!(src is Visual))
                {
                    break;
                }
                src = VisualTreeHelper.GetParent(src);
            }

            if ((src is Control && src.GetType() == typeof(ListViewItem)) || src.GetType() == typeof(Run))
            {
                frmElementary updateElementaryPolicy = null;
                if (dgElementaryPolicies.SelectedItem == null)
                {
                    MessageBox.Show("נא לסמן את הפוליסה שברצונך לעדכן", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                }
                else
                {
                    updateElementaryPolicy = new frmElementary((ElementaryPolicy)dgElementaryPolicies.SelectedItem, user, false, client);
                    updateElementaryPolicy.ShowDialog();
                    path = updateElementaryPolicy.path;

                }
                if (updateElementaryPolicy != null)
                {
                    DataGridPoliciesBinding((Client)dgExistingClients.SelectedItem);

                    //DataGridClientsBinding(false, null);
                    //cbCategoriesBinding();
                    //SetTabToDisplay(updateClientWindow);
                }
                //frmAddClient updateClientWindow = null;

                //if (dgExistingClients.SelectedItem == null)
                //{
                //    MessageBox.Show("נא לסמן את לקוח שברצונך לעדכן", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                //}
                //else
                //{
                //    updateClientWindow = new frmAddClient((Client)dgExistingClients.SelectedItem);
                //    updateClientWindow.ShowDialog();
                //}
                //if (updateClientWindow != null)
                //{
                //    DataGridClientsBinding(false, null);
                //    cbCategoriesBinding();
                //    SetTabToDisplay(updateClientWindow);
                //}
            }
        }

        private void btnScan_Click(object sender, RoutedEventArgs e)
        {
            frmScan2 archiveWindow = null;
            if (tbItemElementary.IsSelected)
            {
                if (dgElementaryPolicies.SelectedItem == null)
                {
                    MessageBox.Show("נא לסמן פוליסה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    return;
                }
                archiveWindow = new frmScan2(client, (ElementaryPolicy)dgElementaryPolicies.SelectedItem, path, user);

                //archiveWindow.ShowDialog();
            }
            else if (tbItemLife.IsSelected)
            {
                if (dgLife.SelectedItem == null)
                {
                    MessageBox.Show("נא לסמן פוליסה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    return;
                }
                archiveWindow = new frmScan2(client, (LifePolicy)dgLife.SelectedItem, path, user);
                //archiveWindow.ShowDialog();
            }
            else if (tbItemHealth.IsSelected)
            {
                if (dgHealth.SelectedItem == null)
                {
                    MessageBox.Show("נא לסמן פוליסה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    return;
                }
                archiveWindow = new frmScan2(client, (HealthPolicy)dgHealth.SelectedItem, path, user);
                //archiveWindow.ShowDialog();
            }
            else if (tbItemFinance.IsSelected)
            {
                if (dgFinance.SelectedItem == null)
                {
                    MessageBox.Show("נא לסמן פוליסה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    return;
                }
                archiveWindow = new frmScan2(client, (FinancePolicy)dgFinance.SelectedItem, path, user);
                //archiveWindow.ShowDialog();
            }
            else if (tbItemPersonalAccidents.IsSelected)
            {
                if (dgPersonalAccidentsPolicies.SelectedItem == null)
                {
                    MessageBox.Show("נא לסמן פוליסה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    return;
                }
                archiveWindow = new frmScan2(client, (PersonalAccidentsPolicy)dgPersonalAccidentsPolicies.SelectedItem, path, user);
                //archiveWindow.ShowDialog();
            }
            else if (tbItemTravel.IsSelected)
            {
                if (dgTravelPolicies.SelectedItem == null)
                {
                    MessageBox.Show("נא לסמן פוליסה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    return;
                }
                archiveWindow = new frmScan2(client, (TravelPolicy)dgTravelPolicies.SelectedItem, path, user);
                // archiveWindow.ShowDialog();
            }
            else if (tbItemForeingWorkers.IsSelected)
            {
                if (dgForeingWorkersPolicies.SelectedItem == null)
                {
                    MessageBox.Show("נא לסמן פוליסה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    return;
                }
                archiveWindow = new frmScan2(client, (ForeingWorkersPolicy)dgForeingWorkersPolicies.SelectedItem, path, user);
                //archiveWindow.ShowDialog();
            }

            else if (tbItemOffers.IsSelected)
            {
                if (dgOffers.SelectedItem == null)
                {
                    MessageBox.Show("נא לסמן הצעה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    return;
                }
                archiveWindow = new frmScan2(client, (object)dgOffers.SelectedItem, path, user);
                //archiveWindow.ShowDialog();
            }
            else if (tbItemClaims.IsSelected)
            {
                archiveWindow = null;
                if (dgClientClaims.SelectedItem == null)
                {
                    MessageBox.Show("נא לסמן תביעה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    return;
                }
                if (dgClientClaims.SelectedItem != null)
                {
                    archiveWindow = new frmScan2(client, (object)dgClientClaims.SelectedItem, path, user);
                }
                //else if (dgLifeClientClaims != null)
                //{
                //    archiveWindow = new frmScan2(client, (object)dgLifeClientClaims.SelectedItem, path);
                //}
                //archiveWindow.ShowDialog();
            }
            archiveWindow.Closed += (object o, EventArgs ea) =>
            {
                OnChildWindowClosed();
            };
            archiveWindow.ShowDialog();
        }


        private void miUsersManagement_Click(object sender, RoutedEventArgs e)
        {
            frmUsersManager usersWindow = new frmUsersManager();
            usersWindow.ShowDialog();
        }

        private void miAgentsManagement_Click(object sender, RoutedEventArgs e)
        {
            frmAgents agentsWindow = new frmAgents();
            agentsWindow.ShowDialog();
        }

        private void cbStatus_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (client != null)
            {
                if (tbItemElementary.IsSelected)
                {
                    DataGridPoliciesBinding(client);
                }
                else if (tbItemLife.IsSelected)
                {
                    dgLifeBinding();
                }
                else if (tbItemHealth.IsSelected)
                {
                    dgHealthBinding();
                }
                else if (tbItemFinance.IsSelected)
                {
                    dgFinanceBinding();
                }
                else if (tbItemPersonalAccidents.IsSelected)
                {
                    dgPersonalAccidentsPoliciesBinding();
                }
                else if (tbItemTravel.IsSelected)
                {
                    dgTravelPoliciesBinding();
                }
                else if (tbItemForeingWorkers.IsSelected)
                {
                    dgForeingWorkersBinding();
                }
                else if (tbItemClaims.IsSelected)
                {
                    DataGridClaimsBinding(client);
                }
                else if (tbItemOffers.IsSelected)
                {
                    DataGridOffersBinding(client);
                }
            }
        }

        private void dgExistingClients_GotFocus(object sender, RoutedEventArgs e)
        {
            //dgExistingClients.UnselectAll();
            dgPotentialClients.UnselectAll();
            dgNonActiveClients.UnselectAll();
        }

        private void dgNonActiveClients_GotFocus(object sender, RoutedEventArgs e)
        {
            dgExistingClients.UnselectAll();
            dgPotentialClients.UnselectAll();
            //dgNonActiveClients.UnselectAll();
        }

        private void dgPotentialClients_GotFocus(object sender, RoutedEventArgs e)
        {
            dgExistingClients.UnselectAll();
            //dgPotentialClients.UnselectAll();
            dgNonActiveClients.UnselectAll();
        }

        private void txtSearchPolicies_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (tbItemElementary.IsSelected)
            {
                DataGridPoliciesBinding(client);
            }
            else if (tbItemPersonalAccidents.IsSelected)
            {
                dgPersonalAccidentsPoliciesBinding();
            }
            else if (tbItemTravel.IsSelected)
            {
                dgTravelPoliciesBinding();
            }
            else if (tbItemLife.IsSelected)
            {
                dgLifeBinding();
            }
            else if (tbItemHealth.IsSelected)
            {
                dgHealthBinding();
            }
            else if (tbItemFinance.IsSelected)
            {
                dgFinanceBinding();
            }
            else if (tbItemForeingWorkers.IsSelected)
            {
                dgForeingWorkersBinding();
            }
            else if (tbItemOffers.IsSelected)
            {
                DataGridOffersBinding(client);
            }
        }

        private void btnDisplayAllPolicies_Click(object sender, RoutedEventArgs e)
        {
            cbStatus.SelectedIndex = 0;
            txtSearchPolicies.Clear();
        }

        private void btnPolicyAddition_Click(object sender, RoutedEventArgs e)
        {
            if (tbItemElementary.IsSelected)
            {
                if (dgElementaryPolicies.SelectedItem != null)
                {
                    if (((ElementaryPolicy)dgElementaryPolicies.SelectedItem).Addition != int.Parse(policiesLogic.GetAdditionNumber(((ElementaryPolicy)dgElementaryPolicies.SelectedItem).PolicyNumber, ((ElementaryPolicy)dgElementaryPolicies.SelectedItem).InsuranceIndustry)) - 1)
                    {
                        MessageBox.Show("נא לסמן את התוספת האחרונה של הפוליסה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                        return;
                    }
                    frmElementary elementaryPolicyAddition = null;
                    elementaryPolicyAddition = new frmElementary((ElementaryPolicy)dgElementaryPolicies.SelectedItem, user, true, client);
                    elementaryPolicyAddition.ShowDialog();
                    path = elementaryPolicyAddition.path;
                    if (elementaryPolicyAddition != null)
                    {
                        DataGridPoliciesBinding((Client)dgExistingClients.SelectedItem);

                        //DataGridClientsBinding(false, null);
                        //cbCategoriesBinding();
                        //SetTabToDisplay(updateClientWindow);
                    }
                }
                else
                {
                    MessageBox.Show("נא לסמן פוליסה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                }
            }
            else if (tbItemLife.IsSelected)
            {
                if (dgLife.SelectedItem != null)
                {
                    if (((LifePolicy)dgLife.SelectedItem).Addition != int.Parse(lifePolicyLogic.GetAdditionNumber(((LifePolicy)dgLife.SelectedItem).PolicyNumber, ((LifePolicy)dgLife.SelectedItem).LifeIndustry)) - 1)
                    {
                        MessageBox.Show("נא לסמן את התוספת האחרונה של הפוליסה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                        return;
                    }
                    frmLifePolicy lifePolicyAddition = null;
                    lifePolicyAddition = new frmLifePolicy(client, user, true, (LifePolicy)dgLife.SelectedItem);
                    lifePolicyAddition.ShowDialog();
                    if (lifePolicyAddition != null)
                    {
                        //taGridPoliciesBinding((Client)dgExistingClients.SelectedItem);
                        dgLifeBinding();
                    }
                }
                else
                {
                    MessageBox.Show("נא לסמן פוליסה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                }
            }
            else if (tbItemHealth.IsSelected)
            {
                if (dgHealth.SelectedItem != null)
                {
                    if (((HealthPolicy)dgHealth.SelectedItem).Addition != int.Parse(healthPolicyLogic.GetAdditionNumber(((HealthPolicy)dgHealth.SelectedItem).PolicyNumber)) - 1)
                    {
                        MessageBox.Show("נא לסמן את התוספת האחרונה של הפוליסה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                        return;
                    }
                    frmHealthPolicy healthPolicyAddition = null;
                    healthPolicyAddition = new frmHealthPolicy(client, user, true, (HealthPolicy)dgHealth.SelectedItem);
                    healthPolicyAddition.ShowDialog();
                    if (healthPolicyAddition != null)
                    {
                        //taGridPoliciesBinding((Client)dgExistingClients.SelectedItem);
                        dgHealthBinding();
                    }
                }
                else
                {
                    MessageBox.Show("נא לסמן פוליסה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                }
            }
            else if (tbItemPersonalAccidents.IsSelected)
            {
                if (dgPersonalAccidentsPolicies.SelectedItem != null)
                {
                    if (((PersonalAccidentsPolicy)dgPersonalAccidentsPolicies.SelectedItem).Addition != int.Parse(personalAccidentsLogic.GetAdditionNumber(((PersonalAccidentsPolicy)dgPersonalAccidentsPolicies.SelectedItem).PolicyNumber)) - 1)
                    {
                        MessageBox.Show("נא לסמן את התוספת האחרונה של הפוליסה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                        return;
                    }
                    frmPersonalAccidents personalAccidentsPolicyAddition = null;
                    personalAccidentsPolicyAddition = new frmPersonalAccidents(client, user, true, (PersonalAccidentsPolicy)dgPersonalAccidentsPolicies.SelectedItem);
                    personalAccidentsPolicyAddition.ShowDialog();
                    if (personalAccidentsPolicyAddition != null)
                    {
                        dgPersonalAccidentsPoliciesBinding();
                    }
                }
                else
                {
                    MessageBox.Show("נא לסמן פוליסה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                }
            }
            else if (tbItemTravel.IsSelected)
            {
                if (dgTravelPolicies.SelectedItem != null)
                {
                    if (((TravelPolicy)dgTravelPolicies.SelectedItem).Addition != int.Parse(travelLogic.GetAdditionNumber(((TravelPolicy)dgTravelPolicies.SelectedItem).PolicyNumber)) - 1)
                    {
                        MessageBox.Show("נא לסמן את התוספת האחרונה של הפוליסה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                        return;
                    }
                    frmTravel travelPolicyAddition = null;
                    travelPolicyAddition = new frmTravel(client, user, true, (TravelPolicy)dgTravelPolicies.SelectedItem);
                    travelPolicyAddition.ShowDialog();
                    if (travelPolicyAddition != null)
                    {
                        dgTravelPoliciesBinding();
                    }
                }
                else
                {
                    MessageBox.Show("נא לסמן פוליסה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                }
            }
            else if (tbItemForeingWorkers.IsSelected)
            {
                if (dgForeingWorkersPolicies.SelectedItem != null)
                {
                    if (((ForeingWorkersPolicy)dgForeingWorkersPolicies.SelectedItem).Addition != int.Parse(foreingWorkersLogic.GetAdditionNumber(((ForeingWorkersPolicy)dgForeingWorkersPolicies.SelectedItem).PolicyNumber)) - 1)
                    {
                        MessageBox.Show("נא לסמן את התוספת האחרונה של הפוליסה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                        return;
                    }
                    frmForeignWorkers foreingWorkersPolicyAddition = null;
                    foreingWorkersPolicyAddition = new frmForeignWorkers(client, user, true, (ForeingWorkersPolicy)dgForeingWorkersPolicies.SelectedItem);
                    foreingWorkersPolicyAddition.ShowDialog();
                    if (foreingWorkersPolicyAddition != null)
                    {
                        dgForeingWorkersBinding();
                    }
                }
                else
                {
                    MessageBox.Show("נא לסמן פוליסה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                }
            }
            else
            {
                if (tbItemOffers.IsSelected == true || tbItemClaims.IsSelected == true || tbItemFinance.IsSelected == true)
                {
                    MessageBox.Show("לא ניתן לבצעה פעולה זו", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                }

            }
        }

        private void dgElementaryPolicies_ContextMenuOpening(object sender, ContextMenuEventArgs e)
        {
            if (dgElementaryPolicies.SelectedItem == null)
            {
                //mItemPrintPoliciesList.IsEnabled = false;
                mItemNewAddition.IsEnabled = false;
                mItemNewClaim.IsEnabled = false;
                mItemPolicyRenewal.IsEnabled = false;
                //mItemPrintPolicy.IsEnabled = false;
                mItemUpdatePolicy.IsEnabled = false;
                mItemExcel.IsEnabled = false;
                //mItemSendPoliciesListByEmail.IsEnabled = false;
                mItemAddPolicyToCar.IsEnabled = false;
                mItemNewPolicyTask.IsEnabled = false;
                mItemTransferPolicy.IsEnabled = false;
            }
            else
            {
                // mItemPrintPoliciesList.IsEnabled = false;
                mItemPolicyRenewal.IsEnabled = true;
                //mItemPrintPolicy.IsEnabled = true;
                mItemUpdatePolicy.IsEnabled = true;
                mItemExcel.IsEnabled = true;
                mItemTransferPolicy.IsEnabled = true;
                //mItemSendPoliciesListByEmail.IsEnabled = true;
                if (user.IsTasksPermission != false)
                {
                    mItemNewPolicyTask.IsEnabled = true;
                }
                if (((ElementaryPolicy)dgElementaryPolicies.SelectedItem).InsuranceIndustry.InsuranceIndustryName.Contains("רכב"))
                {
                    mItemAddPolicyToCar.IsEnabled = true;
                }
                else
                {
                    mItemAddPolicyToCar.IsEnabled = false;
                }
            }
            if (dgElementaryPolicies.SelectedItems.Count > 1)
            {
                //mItemPrintPoliciesList.IsEnabled = true;
                mItemNewAddition.IsEnabled = false;
                mItemNewClaim.IsEnabled = false;
                mItemPolicyRenewal.IsEnabled = false;
                //mItemPrintPolicy.IsEnabled = false;
                mItemUpdatePolicy.IsEnabled = false;
                mItemAddPolicyToCar.IsEnabled = false;
                mItemNewPolicyTask.IsEnabled = false;
                mItemTransferPolicy.IsEnabled = false;
            }
        }

        private void dgElementaryPolicies_ContextMenuClosing(object sender, ContextMenuEventArgs e)
        {
            //mItemPrintPoliciesList.IsEnabled = false;
            mItemNewAddition.IsEnabled = true;
            mItemNewClaim.IsEnabled = true;
            mItemPolicyRenewal.IsEnabled = true;
            // mItemPrintPolicy.IsEnabled = true;
            mItemUpdatePolicy.IsEnabled = true;
            mItemExcel.IsEnabled = true;
            //mItemSendPoliciesListByEmail.IsEnabled = true;
            mItemAddPolicyToCar.IsEnabled = true;
            mItemTransferPolicy.IsEnabled = true;
        }



        private void dgElementaryPolicies_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            dgElementaryPolicies.UnselectAll();
        }

        private void dgExistingClients_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            dgExistingClients.UnselectAll();
        }



        private void dgExistingClients_ContextMenuOpening(object sender, ContextMenuEventArgs e)
        {
            if (dgExistingClients.SelectedItem == null)
            {
                //mItemPrintClientAndMessages.IsEnabled = false;
                mItemAddMessage.IsEnabled = false;
                //mItemPrintClientDetails.IsEnabled = false;
                mItemSendEmail.IsEnabled = false;
                mItemUpdateClient.IsEnabled = false;
                mItemClientRenwals.IsEnabled = false;
                mItemClientProfile.IsEnabled = false;
            }
            else
            {
                //mItemPrintClientAndMessages.IsEnabled = true;
                if (user.IsTasksPermission != false)
                {
                    mItemAddMessage.IsEnabled = true;
                }
                //mItemPrintClientDetails.IsEnabled = true;
                mItemSendEmail.IsEnabled = true;
                mItemUpdateClient.IsEnabled = true;
                mItemClientRenwals.IsEnabled = true;
                if (user.IsClientProfilePermission != false)
                {
                    mItemClientProfile.IsEnabled = true;
                }
            }
        }

        private void dgExistingClients_ContextMenuClosing(object sender, ContextMenuEventArgs e)
        {
            //mItemPrintClientAndMessages.IsEnabled = true;
            if (user.IsTasksPermission != false)
            {
                mItemAddMessage.IsEnabled = true;
            }
            //mItemPrintClientDetails.IsEnabled = true;
            mItemSendEmail.IsEnabled = true;
            mItemUpdateClient.IsEnabled = true;
        }


        private void mItemExcel_Click(object sender, RoutedEventArgs e)
        {
            excelLogic.ElementaryPoliciesToExcel((List<ElementaryPolicy>)dgElementaryPolicies.ItemsSource);
        }

        private void mItemPrintClientList_Click(object sender, RoutedEventArgs e)
        {
            ListView lvToPrint = null;
            if (tbItemExistingClients.IsSelected)
            {
                lvToPrint = dgExistingClients;
            }
            else if (tbItemNonActiveClients.IsSelected)
            {
                lvToPrint = dgNonActiveClients;
            }
            else if (tbItemPotentialClients.IsSelected)
            {
                lvToPrint = dgPotentialClients;
            }
            try
            {
                frmPrintPreview preview = new frmPrintPreview("ClientsReport", lvToPrint.ItemsSource);
                preview.ShowDialog();

            }
            catch (Exception ex)
            {
                MessageBox.Show("1:" + ex.Message);
            }
        }


        private void mItemDisplayLastMonthBirthdays_Click(object sender, RoutedEventArgs e)
        {
            DateTime date = DateTime.Now.AddMonths(-1);
            List<Client> clientListToPrint = clientsLogic.GetClientsByBirthMonth(date.Month);
            string monthName = date.ToString("MMMM");
            string title = "ימי הולדת לחודש " + monthName;
            //frmDocPreview previewWindow = new frmDocPreview(clientListToPrint, dgExistingClients.Columns, title);
            //previewWindow.ShowDialog();
        }

        private void mItemDisplayCurrentMonthBirthdays_Click(object sender, RoutedEventArgs e)
        {
            DateTime date = DateTime.Now;
            List<Client> clientListToPrint = clientsLogic.GetClientsByBirthMonth(date.Month);
            string monthName = date.ToString("MMMM");
            string title = "ימי הולדת לחודש " + monthName;
            //frmDocPreview previewWindow = new frmDocPreview(clientListToPrint, dgExistingClients.Columns, title);
            //previewWindow.ShowDialog();
        }

        private void mItemDisplayNextMonthBirthdays_Click(object sender, RoutedEventArgs e)
        {
            DateTime date = DateTime.Now.AddMonths(1);
            List<Client> clientListToPrint = clientsLogic.GetClientsByBirthMonth(date.Month);
            string monthName = date.ToString("MMMM");
            string title = "ימי הולדת לחודש " + monthName;
            //frmDocPreview previewWindow = new frmDocPreview(clientListToPrint, dgExistingClients.Columns, title);
            //previewWindow.ShowDialog();
        }

        private void mItemPrintClientDetails_Click(object sender, RoutedEventArgs e)
        {
            List<Client> clientListToPrint = new List<Client>();
            clientListToPrint.Add((Client)dgExistingClients.SelectedItem);
            //frmDocPreview previewWindow = new frmDocPreview(clientListToPrint, dgExistingClients.Columns, "פרטי לקוח");
            //previewWindow.ShowDialog();
        }

        private void mItemPrintAllClients_Click(object sender, RoutedEventArgs e)
        {
            List<Client> clientListToPrint = clientsLogic.GetAllClients();
            //frmDocPreview previewWindow = new frmDocPreview(clientListToPrint, dgExistingClients.Columns, "רשימת כל הלקוחות");//לבחור עמודות להצגה
            //previewWindow.ShowDialog();
        }

        private void mItemAddPolicyToCar_Click(object sender, RoutedEventArgs e)
        {
            frmElementary updateElementaryPolicy = new frmElementary(client, user, true, (ElementaryPolicy)dgElementaryPolicies.SelectedItem);
            updateElementaryPolicy.ShowDialog();
            DataGridPoliciesBinding((Client)dgExistingClients.SelectedItem);
        }

        private void miCalculator_Click(object sender, RoutedEventArgs e)
        {
            System.Diagnostics.Process.Start("calc");
        }

        private void btnNewOffer_Click(object sender, RoutedEventArgs e)
        {
            Client clientSelected = null;
            if (dgExistingClients.SelectedItem != null)
            {
                clientSelected = (Client)dgExistingClients.SelectedItem;
            }
            else if (dgPotentialClients.SelectedItem != null)
            {
                clientSelected = (Client)dgPotentialClients.SelectedItem;
            }
            else if (dgNonActiveClients.SelectedItem != null)
            {
                clientSelected = (Client)dgNonActiveClients.SelectedItem;
            }
            else
            {
                MessageBox.Show("נא לסמן לקוח", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }

            isOffer = true;
            if (tbItemElementary.IsSelected)
            {
                frmElementary addElementaryWindow = new frmElementary(clientSelected, user, isOffer);
                addElementaryWindow.ShowDialog();
                path = addElementaryWindow.path;
            }
            else if (tbItemPersonalAccidents.IsSelected)
            {
                frmPersonalAccidents window = new frmPersonalAccidents(clientSelected, user, isOffer);
                window.ShowDialog();
            }
            else if (tbItemTravel.IsSelected)
            {
                frmTravel window = new frmTravel(clientSelected, user, isOffer);
                window.ShowDialog();
            }
            else if (tbItemLife.IsSelected)
            {
                frmLifePolicy window = new frmLifePolicy(clientSelected, user, isOffer);
                window.ShowDialog();
            }
            else if (tbItemHealth.IsSelected)
            {
                frmHealthPolicy window = new frmHealthPolicy(clientSelected, user, isOffer);
                window.ShowDialog();
            }
            else if (tbItemFinance.IsSelected)
            {
                frmFinance window = new frmFinance(clientSelected, user, isOffer);
                window.ShowDialog();
            }
            else if (tbItemForeingWorkers.IsSelected)
            {
                frmForeignWorkers window = new frmForeignWorkers(clientSelected, user, isOffer);
                window.ShowDialog();
            }
            else
            {
                MessageBox.Show("אנא סמן לשונית ביטוח לפתיחת הצעה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            DataGridOffersBinding(clientSelected);
            tbItemOffers.Focus();
            //dgExistingClients.Focus();

        }

        private void btnClientOffers_Click(object sender, RoutedEventArgs e)
        {
            if (dgExistingClients.SelectedItem == null && dgNonActiveClients.SelectedItem == null && dgPotentialClients.SelectedItem == null)
            {
                MessageBox.Show("נא לסמן לקוח", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            frmOffers offerWindow = new frmOffers(client, user);
            offerWindow.ShowDialog();
            DataGridOffersBinding(client);
            DataGridPoliciesBinding(client);
        }

        private void dgOffers_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            DependencyObject src = (DependencyObject)(e.OriginalSource);
            while (!(src is Control))
            {
                if (!(src is Visual))
                {
                    break;
                }
                src = VisualTreeHelper.GetParent(src);
            }

            if ((src is Control && src.GetType() == typeof(ListViewItem)) || src.GetType() == typeof(Run))
            {
                UpdateOffer();
            }
        }

        private void UpdateOffer()
        {
            if (dgOffers.SelectedItem == null)
            {
                MessageBox.Show("נא לסמן הצעה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            if (dgOffers.SelectedItem is ElementaryPolicy)
            {
                frmElementary updateElementaryPolicy = new frmElementary((ElementaryPolicy)dgOffers.SelectedItem, user, false, client);
                updateElementaryPolicy.ShowDialog();
            }
            else if (dgOffers.SelectedItem is PersonalAccidentsPolicy)
            {
                frmPersonalAccidents updatePersonalAccidentsPolicy = new frmPersonalAccidents(client, user, false, (PersonalAccidentsPolicy)dgOffers.SelectedItem);
                updatePersonalAccidentsPolicy.ShowDialog();
            }
            else if (dgOffers.SelectedItem is TravelPolicy)
            {
                frmTravel updateTravelPolicy = new frmTravel(client, user, false, (TravelPolicy)dgOffers.SelectedItem);
                updateTravelPolicy.ShowDialog();
            }
            else if (dgOffers.SelectedItem is LifePolicy)
            {
                frmLifePolicy updateLifePolicy = new frmLifePolicy(client, user, false, (LifePolicy)dgOffers.SelectedItem);
                updateLifePolicy.ShowDialog();
            }
            else if (dgOffers.SelectedItem is HealthPolicy)
            {
                frmHealthPolicy updateHealthPolicy = new frmHealthPolicy(client, user, false, (HealthPolicy)dgOffers.SelectedItem);
                updateHealthPolicy.ShowDialog();
            }
            else if (dgOffers.SelectedItem is FinancePolicy)
            {
                frmFinance updateFinancePolicy = new frmFinance(client, user, false, (FinancePolicy)dgOffers.SelectedItem);
                updateFinancePolicy.ShowDialog();
            }
            else if (dgOffers.SelectedItem is ForeingWorkersPolicy)
            {
                frmForeignWorkers updateFinancePolicy = new frmForeignWorkers(client, user, false, (ForeingWorkersPolicy)dgOffers.SelectedItem);
                updateFinancePolicy.ShowDialog();
            }
            DataGridOffersBinding(client);
        }

        private void mItemConvertToPolicy_Click(object sender, RoutedEventArgs e)
        {
            if (dgOffers.SelectedItem == null)
            {
                MessageBox.Show("נא לסמן הצעה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            bool isOffer = false;
            if (client.ClientTypeID != 1)
            {
                clientsLogic.UpdateClientType(client, new ClientType { ClientTypeID = 1 });
                DataGridClientsBinding(false);
                DataGridClientsBinding(false);
            }
            if (dgOffers.SelectedItem is ElementaryPolicy)
            {
                policiesLogic.ChangeElementaryPolicyOfferStatus(((ElementaryPolicy)dgOffers.SelectedItem).ElementaryPolicyID, ((ElementaryPolicy)dgOffers.SelectedItem).PolicyNumber, ((ElementaryPolicy)dgOffers.SelectedItem).InsuranceIndustryID, isOffer);
                DataGridPoliciesBinding(client);
                tbItemElementary.Focus();
            }
            else if (dgOffers.SelectedItem is PersonalAccidentsPolicy)
            {
                personalAccidentsLogic.ChangePersonalAccidentsPolicyOfferStatus(((PersonalAccidentsPolicy)dgOffers.SelectedItem).PersonalAccidentsPolicyID, isOffer);
                dgPersonalAccidentsPoliciesBinding();
                tbItemPersonalAccidents.Focus();
            }
            else if (dgOffers.SelectedItem is TravelPolicy)
            {
                travelLogic.ChangeTravelPolicyOfferStatus(((TravelPolicy)dgOffers.SelectedItem).TravelPolicyID, isOffer);
                dgTravelPoliciesBinding();
                tbItemTravel.Focus();
            }
            else if (dgOffers.SelectedItem is LifePolicy)
            {
                lifePolicyLogic.ChangeLifePolicyOfferStatus(((LifePolicy)dgOffers.SelectedItem).LifePolicyID, isOffer);
                dgLifeBinding();
                tbItemLife.Focus();
            }
            else if (dgOffers.SelectedItem is HealthPolicy)
            {
                healthPolicyLogic.ChangeHealthPolicyOfferStatus(((HealthPolicy)dgOffers.SelectedItem).HealthPolicyID, isOffer);
                dgHealthBinding();
                tbItemHealth.Focus();
            }
            else if (dgOffers.SelectedItem is FinancePolicy)
            {
                financePolicyLogic.ChangeFinancePolicyOfferStatus(((FinancePolicy)dgOffers.SelectedItem).FinancePolicyID, isOffer);
                dgFinanceBinding();
                tbItemFinance.Focus();
            }
            else if (dgOffers.SelectedItem is ForeingWorkersPolicy)
            {
                foreingWorkersLogic.ChangeForeingWorkersPolicyOfferStatus(((ForeingWorkersPolicy)dgOffers.SelectedItem).ForeingWorkersPolicyID, isOffer);
                dgForeingWorkersBinding();
                tbItemForeingWorkers.Focus();
            }
            DataGridOffersBinding(client);
        }

        private void btnRenewals_Click(object sender, RoutedEventArgs e)
        {
            frmRenewals renewalsWindow = new frmRenewals(user, null);
            renewalsWindow.Closed += (object o, EventArgs ea) =>
            {
                OnChildWindowClosed();
            };
            renewalsWindow.Show();
        }

        private void btnAddClaim_Click(object sender, RoutedEventArgs e)
        {
            if (tbItemElementary.IsSelected)
            {
                if (dgElementaryPolicies.SelectedItem == null)
                {
                    MessageBox.Show("נא לסמן את הפוליסה שברצונך לתבוע", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    return;
                }
                frmElementaryClaims claimWindow = new frmElementaryClaims(client, (ElementaryPolicy)dgElementaryPolicies.SelectedItem, user);
                claimWindow.ShowDialog();
                //DataGridClaimsBinding(client);
            }
            else if (tbItemLife.IsSelected)
            {
                if (dgLife.SelectedItem == null)
                {
                    MessageBox.Show("נא לסמן את הפוליסה שברצונך לתבוע", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    return;
                }
                frmLifeClaims claimWindow = new frmLifeClaims(client, (LifePolicy)dgLife.SelectedItem, user);
                claimWindow.ShowDialog();
                //DataGridClaimsBinding(client);
            }
            else if (tbItemPersonalAccidents.IsSelected)
            {
                if (dgPersonalAccidentsPolicies.SelectedItem == null)
                {
                    MessageBox.Show("נא לסמן את הפוליסה שברצונך לתבוע", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    return;
                }
                frmPersonalAccidentsClaims claimWindow = new frmPersonalAccidentsClaims(client, (PersonalAccidentsPolicy)dgPersonalAccidentsPolicies.SelectedItem, user);
                claimWindow.ShowDialog();
            }
            else if (tbItemHealth.IsSelected)
            {
                if (dgHealth.SelectedItem == null)
                {
                    MessageBox.Show("נא לסמן את הפוליסה שברצונך לתבוע", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    return;
                }
                frmHealthClaim claimWindow = new frmHealthClaim(client, (HealthPolicy)dgHealth.SelectedItem, user);
                claimWindow.ShowDialog();

            }
            else if (tbItemTravel.IsSelected)
            {
                if (dgTravelPolicies.SelectedItem == null)
                {
                    MessageBox.Show("נא לסמן את הפוליסה שברצונך לתבוע", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    return;
                }
                frmTravelClaims claimWindow = new frmTravelClaims(client, (TravelPolicy)dgTravelPolicies.SelectedItem, user);
                claimWindow.ShowDialog();

            }
            else if (tbItemForeingWorkers.IsSelected)
            {
                if (dgForeingWorkersPolicies.SelectedItem == null)
                {
                    MessageBox.Show("נא לסמן את הפוליסה שברצונך לתבוע", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    return;
                }
                frmForeingWorkersClaims claimWindow = new frmForeingWorkersClaims(client, (ForeingWorkersPolicy)dgForeingWorkersPolicies.SelectedItem, user);
                claimWindow.ShowDialog();
            }
            tbItemClaims.IsSelected = true;
            DataGridClaimsBinding(client);
        }

        private void btnClaims_Click(object sender, RoutedEventArgs e)
        {
            frmClaims claimsWindow = new frmClaims(user);
            claimsWindow.Closed += (object o, EventArgs ea) =>
            {
                OnChildWindowClosed();
            };
            claimsWindow.Show();
        }

        private void dgClientClaims_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            DependencyObject src = (DependencyObject)(e.OriginalSource);
            while (!(src is Control))
            {
                if (!(src is Visual))
                {
                    break;
                }
                src = VisualTreeHelper.GetParent(src);
            }

            if ((src is Control && src.GetType() == typeof(ListViewItem)) || src.GetType() == typeof(Run))
            {
                if (dgClientClaims.SelectedItem == null)
                {
                    MessageBox.Show("נא לסמן את התביעה שברצונך לעדכן", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                }
                else
                {
                    if (dgClientClaims.SelectedItem is ElementaryClaim)
                    {
                        frmElementaryClaims updateElementaryClaim = new frmElementaryClaims((ElementaryClaim)dgClientClaims.SelectedItem, client, user);
                        updateElementaryClaim.ShowDialog();
                    }
                    else if (dgClientClaims.SelectedItem is LifeClaim)
                    {
                        frmLifeClaims updateClaim = new frmLifeClaims((LifeClaim)dgClientClaims.SelectedItem, client, user);
                        updateClaim.ShowDialog();
                    }
                    else if (dgClientClaims.SelectedItem is HealthClaim)
                    {
                        frmHealthClaim updateClaim = new frmHealthClaim((HealthClaim)dgClientClaims.SelectedItem, client, user);
                        updateClaim.ShowDialog();
                    }
                    else if (dgClientClaims.SelectedItem is PersonalAccidentsClaim)
                    {
                        frmPersonalAccidentsClaims updateClaim = new frmPersonalAccidentsClaims((PersonalAccidentsClaim)dgClientClaims.SelectedItem, client, user);
                        updateClaim.ShowDialog();
                    }
                    else if (dgClientClaims.SelectedItem is TravelClaim)
                    {
                        frmTravelClaims updateClaim = new frmTravelClaims((TravelClaim)dgClientClaims.SelectedItem, client, user);
                        updateClaim.ShowDialog();
                    }
                    else if (dgClientClaims.SelectedItem is ForeingWorkersClaim)
                    {
                        frmForeingWorkersClaims updateClaim = new frmForeingWorkersClaims((ForeingWorkersClaim)dgClientClaims.SelectedItem, client, user);
                        updateClaim.ShowDialog();
                    }
                    DataGridClaimsBinding(client);
                }

            }
        }

        private void btnCalendar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                CalendarControl.frmMain calendarWindow = new CalendarControl.frmMain();
                calendarWindow.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }            
            //frmCalendar calendarWindow = new frmCalendar(user);
            //calendarWindow.Show();
        }

        private void mItemAddMessage_Click(object sender, RoutedEventArgs e)
        {
            frmWorkQueue queueWindow = new frmWorkQueue(client, user, null);
            queueWindow.ShowDialog();
        }

        private void mItemNewPolicyTask_Click(object sender, RoutedEventArgs e)
        {
            object taskPolicy = null;
            if (tbItemElementary.IsSelected)
            {
                if (dgElementaryPolicies.SelectedItem != null)
                {
                    taskPolicy = dgElementaryPolicies.SelectedItem;
                }
            }
            else if (tbItemLife.IsSelected)
            {
                if (dgLife.SelectedItem != null)
                {
                    taskPolicy = dgLife.SelectedItem;
                }
            }
            else if (tbItemHealth.IsSelected)
            {
                if (dgHealth.SelectedItem != null)
                {
                    taskPolicy = dgHealth.SelectedItem;
                }
            }
            else if (tbItemFinance.IsSelected)
            {
                if (dgFinance.SelectedItem != null)
                {
                    taskPolicy = dgFinance.SelectedItem;
                }
            }
            else if (tbItemPersonalAccidents.IsSelected)
            {
                if (dgPersonalAccidentsPolicies.SelectedItem != null)
                {
                    taskPolicy = dgPersonalAccidentsPolicies.SelectedItem;
                }
            }
            else if (tbItemTravel.IsSelected)
            {
                if (dgTravelPolicies.SelectedItem != null)
                {
                    taskPolicy = dgTravelPolicies.SelectedItem;
                }
            }
            else if (tbItemForeingWorkers.IsSelected)
            {
                if (dgForeingWorkersPolicies.SelectedItem != null)
                {
                    taskPolicy = dgForeingWorkersPolicies.SelectedItem;
                }
            }
            frmWorkQueue queueWindow = new frmWorkQueue(client, user, taskPolicy);
            queueWindow.ShowDialog();
        }


        private void btnClientMessages_Click_1(object sender, RoutedEventArgs e)
        {
            if (client == null)
            {
                MessageBox.Show("נא לבחור לקוח", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            frmClientMessages clientTasksWindow = new frmClientMessages(client, user);
            clientTasksWindow.ShowDialog();
        }

        private void btnMessages_Click(object sender, RoutedEventArgs e)
        {
            frmMessages workTasksWindow = new frmMessages(user);
            workTasksWindow.Show();
        }

        private void dgLife_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            DependencyObject src = (DependencyObject)(e.OriginalSource);
            while (!(src is Control))
            {
                if (!(src is Visual))
                {
                    break;
                }
                src = VisualTreeHelper.GetParent(src);
            }

            if ((src is Control && src.GetType() == typeof(ListViewItem)) || src.GetType() == typeof(Run))
            {
                frmLifePolicy updateLifePolicy = null;
                if (dgLife.SelectedItem == null)
                {
                    MessageBox.Show("נא לסמן את הפוליסה שברצונך לעדכן", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                }
                else
                {
                    updateLifePolicy = new frmLifePolicy(client, user, false, (LifePolicy)dgLife.SelectedItem);
                    updateLifePolicy.ShowDialog();
                }
                if (updateLifePolicy != null)
                {
                    dgLifeBinding();
                }
            }
        }

        //private void dgLifeClientClaims_MouseDown(object sender, MouseButtonEventArgs e)
        //{
        //    dgLifeClientClaims.UnselectAll();
        //}

        //private void dgLifeClientClaims_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        //{
        //    frmLifeClaims updateLifeClaim = null;
        //    frmHealthClaim updateHealthClaim = null;
        //    if (dgLifeClientClaims.SelectedItem == null)
        //    {
        //        MessageBox.Show("נא לסמן את התביעה שברצונך לעדכן", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
        //    }
        //    else
        //    {
        //        if (dgLifeClientClaims.SelectedItem is LifeClaim)
        //        {
        //            updateLifeClaim = new frmLifeClaims((LifeClaim)dgLifeClientClaims.SelectedItem, client, user);
        //            updateLifeClaim.ShowDialog();
        //        }
        //        else if (dgLifeClientClaims.SelectedItem is HealthClaim)
        //        {
        //            updateHealthClaim = new frmHealthClaim((HealthClaim)dgLifeClientClaims.SelectedItem, client, user);
        //            updateHealthClaim.ShowDialog();
        //        }
        //        DataGridClaimsBinding(client);
        //    }
        //}

        private void dgLife_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (dgLife.SelectedItem is LifePolicy && ((LifePolicy)dgLife.SelectedItem).LifeIndustry.LifeIndustryName == "משכנתא")
            {
                mItemAddMortgage.IsEnabled = true;
            }
            else
            {
                mItemAddMortgage.IsEnabled = false;
            }
        }

        private void mItemAddMortgage_Click(object sender, RoutedEventArgs e)
        {
            if (dgLife.SelectedItem != null)
            {
                frmLifePolicy mortgagaeCopyPolicy = new frmLifePolicy(true, client, user, (LifePolicy)dgLife.SelectedItem);
                mortgagaeCopyPolicy.ShowDialog();
                dgLifeBinding();
            }
        }

        private void dgLife_ContextMenuOpening(object sender, ContextMenuEventArgs e)
        {
            if (dgLife.SelectedItem == null)
            {
                //mItemPrintLifePoliciesList.IsEnabled = false;
                mItemLifeNewAddition.IsEnabled = false;
                mItemNewLifeClaim.IsEnabled = false;
                //mItemPrintLifePolicy.IsEnabled = false;
                mItemUpdateLifePolicy.IsEnabled = false;
                mItemExcelLife.IsEnabled = false;
                //mItemSendLifePoliciesListByEmail.IsEnabled = false;
                mItemAddMortgage.IsEnabled = false;
                mItemNewLifePolicyTask.IsEnabled = false;
                mItemTransferLifePolicy.IsEnabled = false;
            }
            else
            {
                //mItemPrintLifePoliciesList.IsEnabled = false;
                mItemLifeNewAddition.IsEnabled = true;
                mItemNewLifeClaim.IsEnabled = true;
                //mItemPrintLifePolicy.IsEnabled = true;
                mItemUpdateLifePolicy.IsEnabled = true;
                mItemExcelLife.IsEnabled = true;
                //mItemSendLifePoliciesListByEmail.IsEnabled = true;
                if (user.IsTasksPermission != false)
                {
                    mItemNewLifePolicyTask.IsEnabled = true;
                }
                mItemTransferLifePolicy.IsEnabled = true;

            }
            if (dgLife.SelectedItems.Count > 1)
            {
                //mItemPrintLifePoliciesList.IsEnabled = true;
                mItemLifeNewAddition.IsEnabled = false;
                mItemNewLifeClaim.IsEnabled = false;
                //mItemPrintLifePolicy.IsEnabled = false;
                mItemUpdateLifePolicy.IsEnabled = false;
                mItemAddMortgage.IsEnabled = false;
                mItemNewLifePolicyTask.IsEnabled = false;
                mItemTransferLifePolicy.IsEnabled = false;

            }
        }

        private void dgLife_ContextMenuClosing(object sender, ContextMenuEventArgs e)
        {
            //mItemPrintLifePoliciesList.IsEnabled = false;
            mItemLifeNewAddition.IsEnabled = true;
            mItemNewLifeClaim.IsEnabled = true;
            //mItemPrintLifePolicy.IsEnabled = true;
            mItemUpdateLifePolicy.IsEnabled = true;
            mItemExcelLife.IsEnabled = true;
            //mItemSendLifePoliciesListByEmail.IsEnabled = true;
            mItemTransferLifePolicy.IsEnabled = true;

        }

        private void dgHealth_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            DependencyObject src = (DependencyObject)(e.OriginalSource);
            while (!(src is Control))
            {
                if (!(src is Visual))
                {
                    break;
                }
                src = VisualTreeHelper.GetParent(src);
            }

            if ((src is Control && src.GetType() == typeof(ListViewItem)) || src.GetType() == typeof(Run))
            {
                frmHealthPolicy updateHealthPolicy = null;
                if (dgHealth.SelectedItem == null)
                {
                    MessageBox.Show("נא לסמן את הפוליסה שברצונך לעדכן", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                }
                else
                {
                    updateHealthPolicy = new frmHealthPolicy(client, user, false, (HealthPolicy)dgHealth.SelectedItem);
                    updateHealthPolicy.ShowDialog();
                }
                if (updateHealthPolicy != null)
                {
                    dgHealthBinding();
                }
            }
        }

        private void dgHealth_ContextMenuOpening(object sender, ContextMenuEventArgs e)
        {
            if (dgHealth.SelectedItem == null)
            {
                //mItemPrintHealthPoliciesList.IsEnabled = false;
                mItemHealthNewAddition.IsEnabled = false;
                mItemNewHealthClaim.IsEnabled = false;
                //mItemPrintHealthPolicy.IsEnabled = false;
                mItemUpdateHealthPolicy.IsEnabled = false;
                mItemExcelHealth.IsEnabled = false;
                //mItemSendHealthPoliciesListByEmail.IsEnabled = false;
                mItemNewHealthPolicyTask.IsEnabled = false;
                mItemTransferHealthPolicy.IsEnabled = false;
            }
            else
            {
                //mItemPrintHealthPoliciesList.IsEnabled = false;
                mItemHealthNewAddition.IsEnabled = true;
                mItemNewHealthClaim.IsEnabled = true;
                //mItemPrintHealthPolicy.IsEnabled = true;
                mItemUpdateHealthPolicy.IsEnabled = true;
                mItemExcelHealth.IsEnabled = true;
                //mItemSendHealthPoliciesListByEmail.IsEnabled = true;
                if (user.IsTasksPermission != false)
                {
                    mItemNewHealthPolicyTask.IsEnabled = true;
                }
                mItemTransferHealthPolicy.IsEnabled = true;
            }
            if (dgHealth.SelectedItems.Count > 1)
            {
                //mItemPrintHealthPoliciesList.IsEnabled = true;
                mItemHealthNewAddition.IsEnabled = false;
                mItemNewHealthClaim.IsEnabled = false;
                //mItemPrintHealthPolicy.IsEnabled = false;
                mItemUpdateHealthPolicy.IsEnabled = false;
                mItemNewHealthPolicyTask.IsEnabled = false;
                mItemTransferHealthPolicy.IsEnabled = false;
            }
        }

        private void dgHealth_ContextMenuClosing(object sender, ContextMenuEventArgs e)
        {
            //mItemPrintHealthPoliciesList.IsEnabled = false;
            mItemHealthNewAddition.IsEnabled = true;
            mItemNewHealthClaim.IsEnabled = true;
            //mItemPrintHealthPolicy.IsEnabled = true;
            mItemUpdateHealthPolicy.IsEnabled = true;
            mItemExcelHealth.IsEnabled = true;
            //mItemSendHealthPoliciesListByEmail.IsEnabled = true;
            mItemTransferHealthPolicy.IsEnabled = true;
        }

        private void policiesTabs_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.Source is TabControl)
            {
                txtSearchPolicies.IsEnabled = true;
                btnPolicyAddition.IsEnabled = true;
                btnAddClaim.IsEnabled = true;
                cbStatus.IsEnabled = true;
                if (tbItemElementary.IsSelected)
                {
                    DataGridPoliciesBinding(client);
                }
                else if (tbItemLife.IsSelected)
                {
                    dgLifeBinding();
                }
                else if (tbItemHealth.IsSelected)
                {
                    dgHealthBinding();
                }
                else if (tbItemFinance.IsSelected)
                {
                    btnPolicyAddition.IsEnabled = false;
                    btnAddClaim.IsEnabled = false;
                    dgFinanceBinding();
                }
                else if (tbItemPersonalAccidents.IsSelected)
                {
                    dgPersonalAccidentsPoliciesBinding();
                }
                else if (tbItemTravel.IsSelected)
                {
                    dgTravelPoliciesBinding();
                }
                else if (tbItemForeingWorkers.IsSelected)
                {
                    dgForeingWorkersBinding();
                }
                else if (tbItemClaims.IsSelected)
                {
                    //cbStatus.IsEnabled = false;
                    DataGridClaimsBinding(client);
                    txtSearchPolicies.IsEnabled = false;
                }
            }
        }

        private void cbCategories_LostFocus(object sender, RoutedEventArgs e)
        {
            if (cbCategories.Text == "")
            {
                cbCategories.Text = "הכל";
            }
        }

        private void miStartSettings_Click(object sender, RoutedEventArgs e)
        {
            frmSettings settingsWindow = new frmSettings(user);
            settingsWindow.ShowDialog();
        }

        private void dgFinance_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            DependencyObject src = (DependencyObject)(e.OriginalSource);
            while (!(src is Control))
            {
                if (!(src is Visual))
                {
                    break;
                }
                src = VisualTreeHelper.GetParent(src);
            }

            if ((src is Control && src.GetType() == typeof(ListViewItem)) || src.GetType() == typeof(Run))
            {
                frmFinance updateFinancePolicy = null;
                if (dgFinance.SelectedItem == null)
                {
                    MessageBox.Show("נא לסמן את הפוליסה שברצונך לעדכן", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                }
                else
                {
                    updateFinancePolicy = new frmFinance(client, user, false, (FinancePolicy)dgFinance.SelectedItem);
                    updateFinancePolicy.ShowDialog();
                }
                if (updateFinancePolicy != null)
                {
                    dgFinanceBinding();
                }
            }
        }

        private void dgFinance_ContextMenuClosing(object sender, ContextMenuEventArgs e)
        {
            //mItemPrintFinancePoliciesList.IsEnabled = false;
            //mItemFinanceNewAddition.IsEnabled = true;
            mItemNewFinanceClaim.IsEnabled = true;
            //mItemPrintFinancePolicy.IsEnabled = true;
            mItemUpdateFinancePolicy.IsEnabled = true;
            mItemExcelFinance.IsEnabled = true;
            //mItemSendFinancePoliciesListByEmail.IsEnabled = true;
            mItemTransferFinancePolicy.IsEnabled = true;
        }

        private void dgFinance_ContextMenuOpening(object sender, ContextMenuEventArgs e)
        {
            if (dgFinance.SelectedItem == null)
            {
                //mItemPrintFinancePoliciesList.IsEnabled = false;
                //mItemFinanceNewAddition.IsEnabled = false;
                mItemNewFinanceClaim.IsEnabled = false;
                //mItemPrintFinancePolicy.IsEnabled = false;
                mItemUpdateFinancePolicy.IsEnabled = false;
                mItemExcelFinance.IsEnabled = false;
                // mItemSendFinancePoliciesListByEmail.IsEnabled = false;
                mItemNewFinancePolicyTask.IsEnabled = false;
                mItemTransferFinancePolicy.IsEnabled = false;
            }
            else
            {
                // mItemPrintFinancePoliciesList.IsEnabled = false;
                //mItemFinanceNewAddition.IsEnabled = true;
                mItemNewFinanceClaim.IsEnabled = true;
                //mItemPrintFinancePolicy.IsEnabled = true;
                mItemUpdateFinancePolicy.IsEnabled = true;
                mItemExcelFinance.IsEnabled = true;
                //mItemSendFinancePoliciesListByEmail.IsEnabled = true;
                if (user.IsTasksPermission != false)
                {
                    mItemNewFinancePolicyTask.IsEnabled = true;
                }
                mItemTransferFinancePolicy.IsEnabled = true;

            }
            if (dgFinance.SelectedItems.Count > 1)
            {
                //mItemPrintFinancePoliciesList.IsEnabled = true;
                //mItemFinanceNewAddition.IsEnabled = false;
                mItemNewFinanceClaim.IsEnabled = false;
                //mItemPrintFinancePolicy.IsEnabled = false;
                mItemUpdateFinancePolicy.IsEnabled = false;
                mItemNewFinancePolicyTask.IsEnabled = false;
                mItemTransferFinancePolicy.IsEnabled = false;
            }
        }

        private void Clients_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            double dgsWidth = this.ActualWidth - 120;
            //double dgsWidth = this.ActualWidth *0.89;
            //double dgsHeight = this.ActualHeight * 0.2;
            double tbClientsHeight = clientsTabs.ActualHeight - 30;
            double dgsHeight = policiesTabs.ActualHeight * 0.85;

            txtScrolling.Width = txtScrolling.ActualWidth - 100;

            dgExistingClients.Width = dgsWidth;
            dgPotentialClients.Width = dgsWidth;
            dgNonActiveClients.Width = dgsWidth;
            dgElementaryPolicies.Width = dgsWidth - 256;
            dgLife.Width = dgsWidth - 256;
            dgHealth.Width = dgsWidth - 256;
            dgForeingWorkersPolicies.Width = dgsWidth - 256;
            dgFinance.Width = dgsWidth - 256;
            dgClientClaims.Width = dgsWidth - 256;
            dgOffers.Width = dgsWidth - 256;
            dgPersonalAccidentsPolicies.Width = dgsWidth - 256;
            dgTravelPolicies.Width = dgsWidth - 256;

            dgExistingClients.Height = tbClientsHeight * 0.95;
            dgPotentialClients.Height = tbClientsHeight * 0.95;
            dgNonActiveClients.Height = tbClientsHeight * 0.95;

            dgElementaryPolicies.Height = dgsHeight;
            dgLife.Height = dgsHeight;
            dgHealth.Height = dgsHeight;
            dgForeingWorkersPolicies.Height = dgsHeight;
            dgFinance.Height = dgsHeight;
            dgClientClaims.Height = dgsHeight;
            dgOffers.Height = dgsHeight;
            dgPersonalAccidentsPolicies.Height = dgsHeight;
            dgTravelPolicies.Height = dgsHeight;
            brClientChart.Height = dgsHeight;
        }

        private void mItemClientProfile_Click(object sender, RoutedEventArgs e)
        {
            if (dgExistingClients.SelectedItem != null)
            {
                frmClientProfile profile = new frmClientProfile(client, user);
                Client clientSelected = client;
                profile.ShowDialog();
                DataGridClientsBinding(false);
                SelectClientInDg(clientSelected, dgExistingClients);
            }
        }

        private void btnClientProfil_MouseEnter(object sender, MouseEventArgs e)
        {
            if (dgExistingClients.SelectedItem != null)
            {
                Point mousePoint = this.PointToScreen(Mouse.GetPosition(this));
                clientProfileWindow = new frmClientProfile(client, user);
                clientProfileWindow.Left = mousePoint.X;
                clientProfileWindow.Top = mousePoint.Y;
                clientProfileWindow.Show();
            }

        }

        private void btnClientProfil_MouseLeave_1(object sender, MouseEventArgs e)
        {
            if (dgExistingClients.SelectedItem != null)
            {
                clientProfileWindow.Close();
            }
        }

        private void dgPersonalAccidentsPolicies_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            DependencyObject src = (DependencyObject)(e.OriginalSource);
            while (!(src is Control))
            {
                if (!(src is Visual))
                {
                    break;
                }
                src = VisualTreeHelper.GetParent(src);
            }

            if ((src is Control && src.GetType() == typeof(ListViewItem)) || src.GetType() == typeof(Run))
            {
                if (dgPersonalAccidentsPolicies.SelectedItem == null)
                {
                    MessageBox.Show("נא לסמן את הפוליסה שברצונך לעדכן", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                }
                else
                {
                    frmPersonalAccidents updatePersonalAccidentsPolicy = new frmPersonalAccidents(client, user, false, (PersonalAccidentsPolicy)dgPersonalAccidentsPolicies.SelectedItem);
                    updatePersonalAccidentsPolicy.ShowDialog();
                }
                //DataGridPoliciesBinding((Client)dgExistingClients.SelectedItem);
                dgPersonalAccidentsPoliciesBinding();
            }
        }

        private void dgTravelPolicies_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            DependencyObject src = (DependencyObject)(e.OriginalSource);
            while (!(src is Control))
            {
                if (!(src is Visual))
                {
                    break;
                }
                src = VisualTreeHelper.GetParent(src);
            }

            if ((src is Control && src.GetType() == typeof(ListViewItem)) || src.GetType() == typeof(Run))
            {
                if (dgTravelPolicies.SelectedItem == null)
                {
                    MessageBox.Show("נא לסמן את הפוליסה שברצונך לעדכן", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                }
                else
                {
                    frmTravel updateTravelPolicy = new frmTravel(client, user, false, (TravelPolicy)dgTravelPolicies.SelectedItem);
                    updateTravelPolicy.ShowDialog();
                }
                //DataGridPoliciesBinding((Client)dgExistingClients.SelectedItem);
                dgTravelPoliciesBinding();
            }
        }

        private void btnOffers_Click(object sender, RoutedEventArgs e)
        {
            frmAllOffers offersWindow = new frmAllOffers(user);
            offersWindow.Show();
            DataGridOffersBinding(client);
            DataGridPoliciesBinding(client);

            //cbCategories.SelectedItem = null;
            //cbCategories.Text = "הכל";
            //txtSearchClients.Clear();
            //tbItemExistingClients.IsSelected = true;
            //DataGridClientsBinding(false);
        }

        private void btnReports_Click(object sender, RoutedEventArgs e)
        {
            frmReports reportsWindow = new frmReports(user);
            reportsWindow.Closed += (object o, EventArgs ea) =>
            {
                OnChildWindowClosed();
            };
            reportsWindow.Show();
        }

        private void dgForeingWorkersPolicies_MouseDown(object sender, MouseButtonEventArgs e)
        {
            dgForeingWorkersPolicies.UnselectAll();
        }

        private void dgForeingWorkersPolicies_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            DependencyObject src = (DependencyObject)(e.OriginalSource);
            while (!(src is Control))
            {
                if (!(src is Visual))
                {
                    break;
                }
                src = VisualTreeHelper.GetParent(src);
            }

            if ((src is Control && src.GetType() == typeof(ListViewItem)) || src.GetType() == typeof(Run))
            {
                if (dgForeingWorkersPolicies.SelectedItem == null)
                {
                    MessageBox.Show("נא לסמן את הפוליסה שברצונך לעדכן", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                }
                else
                {
                    frmForeignWorkers updateForeingWorkersPolicy = new frmForeignWorkers(client, user, false, (ForeingWorkersPolicy)dgForeingWorkersPolicies.SelectedItem);
                    updateForeingWorkersPolicy.ShowDialog();
                }
                dgForeingWorkersBinding();
            }
        }

        private void Chart_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            try
            {
                string strAppDir = System.AppDomain.CurrentDomain.BaseDirectory;
                string coralFilePath = strAppDir + @"Coral Files";
                if (!Directory.Exists(coralFilePath))
                {
                    Directory.CreateDirectory(coralFilePath);
                }
                if (!File.Exists(coralFilePath + @"\OffersPath.txt"))
                {
                    MessageBox.Show("תיקיית הצעות לא קיימת. נא הגדר נתיב בהגדרות הראשוניות", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    return;
                }
                string offersPath = File.ReadAllText(coralFilePath + @"\OffersPath.txt");
                if (!Directory.Exists(offersPath))
                {
                    MessageBox.Show("תיקיית הצעות לא קיימת. נא הגדר נתיב קיים בהגדרות הראשוניות", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    return;
                }
                Border br = (Border)sender;
                switch (br.Name)
                {
                    case "brTravel":
                        string travelPath = offersPath + @"\TravelOffers";
                        if (!Directory.Exists(travelPath))
                        {
                            Directory.CreateDirectory(travelPath);
                        }
                        System.Diagnostics.Process.Start("explorer", travelPath);
                        break;
                    case "brPersonalAccidents":
                        string personalAccidentsPath = offersPath + @"\PersonalAccidentsOffers";
                        if (!Directory.Exists(personalAccidentsPath))
                        {
                            Directory.CreateDirectory(personalAccidentsPath);
                        }
                        System.Diagnostics.Process.Start("explorer", personalAccidentsPath);
                        break;
                    case "brBusiness":
                        string businessPath = offersPath + @"\ElementaryOffers\BusinessOffers";
                        if (!Directory.Exists(businessPath))
                        {
                            Directory.CreateDirectory(businessPath);
                        }
                        System.Diagnostics.Process.Start("explorer", businessPath);
                        break;
                    case "brApartment":
                        string apartmentPath = offersPath + @"\ElementaryOffers\ApartmentOffers";
                        if (!Directory.Exists(apartmentPath))
                        {
                            Directory.CreateDirectory(apartmentPath);
                        }
                        System.Diagnostics.Process.Start("explorer", apartmentPath);
                        break;
                    case "brCar":
                        string carPath = offersPath + @"\ElementaryOffers\CarOffers";
                        if (!Directory.Exists(carPath))
                        {
                            Directory.CreateDirectory(carPath);
                        }
                        System.Diagnostics.Process.Start("explorer", carPath);
                        break;
                    case "brPension":
                        string pensionPath = offersPath + @"\LifeOffers\PensionOffers";
                        if (!Directory.Exists(pensionPath))
                        {
                            Directory.CreateDirectory(pensionPath);
                        }
                        System.Diagnostics.Process.Start("explorer", pensionPath);
                        break;
                    case "brPrivate":
                        string privatePath = offersPath + @"\LifeOffers\PrivateOffers";
                        if (!Directory.Exists(privatePath))
                        {
                            Directory.CreateDirectory(privatePath);
                        }
                        System.Diagnostics.Process.Start("explorer", privatePath);
                        break;
                    case "brMortgage":
                        string mortgagePath = offersPath + @"\LifeOffers\MortgageOffers";
                        if (!Directory.Exists(mortgagePath))
                        {
                            Directory.CreateDirectory(mortgagePath);
                        }
                        System.Diagnostics.Process.Start("explorer", mortgagePath);
                        break;
                    case "brRisks":
                        string risksPath = offersPath + @"\LifeOffers\RisksOffers";
                        if (!Directory.Exists(risksPath))
                        {
                            Directory.CreateDirectory(risksPath);
                        }
                        System.Diagnostics.Process.Start("explorer", risksPath);
                        break;
                    case "brManagers":
                        string managersPath = offersPath + @"\LifeOffers\ManagersOffers";
                        if (!Directory.Exists(managersPath))
                        {
                            Directory.CreateDirectory(managersPath);
                        }
                        System.Diagnostics.Process.Start("explorer", managersPath);
                        break;
                    case "brHealth":
                        string healthPath = offersPath + @"\HealthOffers";
                        if (!Directory.Exists(healthPath))
                        {
                            Directory.CreateDirectory(healthPath);
                        }
                        System.Diagnostics.Process.Start("explorer", healthPath);
                        break;
                    case "brForeingWorkers":
                        string foreingWorkersPath = offersPath + @"\ForeingWorkersOffers";
                        if (!Directory.Exists(foreingWorkersPath))
                        {
                            Directory.CreateDirectory(foreingWorkersPath);
                        }
                        System.Diagnostics.Process.Start("explorer", foreingWorkersPath);
                        break;
                    case "brInvesmentPortfolio":
                        string invesmentPortfolioPath = offersPath + @"\FinanceOffers\InvesmentPortfolioOffers";
                        if (!Directory.Exists(invesmentPortfolioPath))
                        {
                            Directory.CreateDirectory(invesmentPortfolioPath);
                        }
                        System.Diagnostics.Process.Start("explorer", invesmentPortfolioPath);
                        break;
                    case "brKerenHishtalmut":
                        string kerenHishtalmutPath = offersPath + @"\FinanceOffers\KerenHishtalmutOffers";
                        if (!Directory.Exists(kerenHishtalmutPath))
                        {
                            Directory.CreateDirectory(kerenHishtalmutPath);
                        }
                        System.Diagnostics.Process.Start("explorer", kerenHishtalmutPath);
                        break;
                    case "brKupatGuemel":
                        string kupatGuemelPath = offersPath + @"\FinanceOffers\KupatGuemelOffers";
                        if (!Directory.Exists(kupatGuemelPath))
                        {
                            Directory.CreateDirectory(kupatGuemelPath);
                        }
                        System.Diagnostics.Process.Start("explorer", kupatGuemelPath);
                        break;
                    default:
                        break;
                }
            }
            catch (Exception)
            {
                MessageBox.Show("לא ניתן לפתוח את התיקייה המבוקשת");
            }
        }

        private void btnSendEmail_Click(object sender, RoutedEventArgs e)
        {
            if (client == null)
            {
                MessageBox.Show("נא לסמן לקוח", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            if (client.Email == "")
            {
                MessageBox.Show("אין ללקוח כתובת דוא''ל במערכת", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            string[] address = new string[] { client.Email };
            try
            {
                email.SendEmailFromOutlook("", "", null, address);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }


        private void dgTravelPolicies_ContextMenuOpening(object sender, ContextMenuEventArgs e)
        {
            if (dgTravelPolicies.SelectedItem == null)
            {
                //mItemPrintTravelPoliciesList.IsEnabled = false;
                mItemNewTravelAddition.IsEnabled = false;
                mItemNewTravelClaim.IsEnabled = false;
                //mItemPrintTravelPolicy.IsEnabled = false;
                mItemUpdateTravelPolicy.IsEnabled = false;
                mItemExcelTravel.IsEnabled = false;
                //mItemSendTravelPoliciesListByEmail.IsEnabled = false;
                mItemAddMortgage.IsEnabled = false;
                mItemNewTravelPolicyTask.IsEnabled = false;
                mItemTransferTravelPolicy.IsEnabled = false;
            }
            else
            {
                //mItemPrintTravelPoliciesList.IsEnabled = false;
                mItemNewTravelAddition.IsEnabled = true;
                mItemNewTravelClaim.IsEnabled = true;
                //mItemPrintTravelPolicy.IsEnabled = true;
                mItemUpdateTravelPolicy.IsEnabled = true;
                mItemExcelTravel.IsEnabled = true;
                //mItemSendTravelPoliciesListByEmail.IsEnabled = true;
                if (user.IsTasksPermission != false)
                {
                    mItemNewTravelPolicyTask.IsEnabled = true;
                }
                mItemTransferTravelPolicy.IsEnabled = true;
            }
            if (dgTravelPolicies.SelectedItems.Count > 1)
            {
                //mItemPrintTravelPoliciesList.IsEnabled = true;
                mItemNewTravelAddition.IsEnabled = false;
                mItemNewTravelClaim.IsEnabled = false;
                //mItemPrintTravelPolicy.IsEnabled = false;
                mItemUpdateTravelPolicy.IsEnabled = false;
                mItemAddMortgage.IsEnabled = false;
                mItemNewTravelPolicyTask.IsEnabled = false;
                mItemTransferTravelPolicy.IsEnabled = false;
            }
        }

        private void dgTravelPolicies_ContextMenuClosing(object sender, ContextMenuEventArgs e)
        {
            //mItemPrintTravelPoliciesList.IsEnabled = false;
            mItemNewTravelAddition.IsEnabled = true;
            mItemNewTravelClaim.IsEnabled = true;
            //mItemPrintTravelPolicy.IsEnabled = true;
            mItemUpdateTravelPolicy.IsEnabled = true;
            mItemExcelTravel.IsEnabled = true;
            //mItemSendTravelPoliciesListByEmail.IsEnabled = true;
            mItemTransferForeingWorkersPolicy.IsEnabled = true;
            mItemTransferTravelPolicy.IsEnabled = true;
        }

        private void dgForeingWorkersPolicies_ContextMenuOpening(object sender, ContextMenuEventArgs e)
        {
            if (dgForeingWorkersPolicies.SelectedItem == null)
            {
                //mItemPrintForeingWorkersPoliciesList.IsEnabled = false;
                mItemNewForeingWorkersAddition.IsEnabled = false;
                mItemNewForeingWorkersClaim.IsEnabled = false;
                //mItemPrintForeingWorkersPolicy.IsEnabled = false;
                mItemUpdateForeingWorkersPolicy.IsEnabled = false;
                mItemExcelForeingWorkers.IsEnabled = false;
                //mItemSendForeingWorkersPoliciesListByEmail.IsEnabled = false;
                mItemAddMortgage.IsEnabled = false;
                mItemNewForeingWorkersPolicyTask.IsEnabled = false;
                mItemTransferForeingWorkersPolicy.IsEnabled = false;
            }
            else
            {
                //mItemPrintForeingWorkersPoliciesList.IsEnabled = false;
                mItemNewForeingWorkersAddition.IsEnabled = true;
                mItemNewForeingWorkersClaim.IsEnabled = true;
                //mItemPrintForeingWorkersPolicy.IsEnabled = true;
                mItemUpdateForeingWorkersPolicy.IsEnabled = true;
                mItemExcelForeingWorkers.IsEnabled = true;
                //mItemSendForeingWorkersPoliciesListByEmail.IsEnabled = true;
                if (user.IsTasksPermission != false)
                {
                    mItemNewForeingWorkersPolicyTask.IsEnabled = true;
                }
                mItemTransferForeingWorkersPolicy.IsEnabled = true;

            }
            if (dgForeingWorkersPolicies.SelectedItems.Count > 1)
            {
                //mItemPrintForeingWorkersPoliciesList.IsEnabled = true;
                mItemNewForeingWorkersAddition.IsEnabled = false;
                mItemNewForeingWorkersClaim.IsEnabled = false;
                // mItemPrintForeingWorkersPolicy.IsEnabled = false;
                mItemUpdateForeingWorkersPolicy.IsEnabled = false;
                mItemAddMortgage.IsEnabled = false;
                mItemNewForeingWorkersPolicyTask.IsEnabled = false;
                mItemTransferForeingWorkersPolicy.IsEnabled = false;
            }
        }

        private void dgForeingWorkersPolicies_ContextMenuClosing(object sender, ContextMenuEventArgs e)
        {
            //mItemPrintForeingWorkersPoliciesList.IsEnabled = false;
            mItemNewForeingWorkersAddition.IsEnabled = true;
            mItemNewForeingWorkersClaim.IsEnabled = true;
            //mItemPrintForeingWorkersPolicy.IsEnabled = true;
            mItemUpdateForeingWorkersPolicy.IsEnabled = true;
            mItemExcelForeingWorkers.IsEnabled = true;
            //mItemSendForeingWorkersPoliciesListByEmail.IsEnabled = true;
        }

        private void dgPersonalAccidentsPolicies_ContextMenuOpening(object sender, ContextMenuEventArgs e)
        {
            if (dgPersonalAccidentsPolicies.SelectedItem == null)
            {
                //mItemPrintAccidentsPoliciesList.IsEnabled = false;
                mItemNewAccidentsAddition.IsEnabled = false;
                mItemNewAccidentsClaim.IsEnabled = false;
                //mItemPrintAccidentsPolicy.IsEnabled = false;
                mItemUpdateAccidentsPolicy.IsEnabled = false;
                mItemExcelAccidents.IsEnabled = false;
                //mItemSendAccidentsPoliciesListByEmail.IsEnabled = false;
                mItemAddMortgage.IsEnabled = false;
                mItemNewAccidentsPolicyTask.IsEnabled = false;
                mItemTransferPersonalAccidentsPolicy.IsEnabled = false;
            }
            else
            {
                //mItemPrintAccidentsPoliciesList.IsEnabled = false;
                mItemNewAccidentsAddition.IsEnabled = true;
                mItemNewAccidentsClaim.IsEnabled = true;
                //mItemPrintAccidentsPolicy.IsEnabled = true;
                mItemUpdateAccidentsPolicy.IsEnabled = true;
                mItemExcelAccidents.IsEnabled = true;
                //mItemSendAccidentsPoliciesListByEmail.IsEnabled = true;
                if (user.IsTasksPermission != false)
                {
                    mItemNewAccidentsPolicyTask.IsEnabled = true;
                }
                mItemTransferPersonalAccidentsPolicy.IsEnabled = true;

            }
            if (dgPersonalAccidentsPolicies.SelectedItems.Count > 1)
            {
                //mItemPrintAccidentsPoliciesList.IsEnabled = true;
                mItemNewAccidentsAddition.IsEnabled = false;
                mItemNewAccidentsClaim.IsEnabled = false;
                //mItemPrintAccidentsPolicy.IsEnabled = false;
                mItemUpdateAccidentsPolicy.IsEnabled = false;
                mItemAddMortgage.IsEnabled = false;
                mItemNewAccidentsPolicyTask.IsEnabled = false;
                mItemTransferPersonalAccidentsPolicy.IsEnabled = false;

            }
        }

        private void dgPersonalAccidentsPolicies_ContextMenuClosing(object sender, ContextMenuEventArgs e)
        {
            //mItemPrintAccidentsPoliciesList.IsEnabled = false;
            mItemNewAccidentsAddition.IsEnabled = true;
            mItemNewAccidentsClaim.IsEnabled = true;
            //mItemPrintAccidentsPolicy.IsEnabled = true;
            mItemUpdateAccidentsPolicy.IsEnabled = true;
            mItemExcelAccidents.IsEnabled = true;
            //mItemSendAccidentsPoliciesListByEmail.IsEnabled = true;
            mItemTransferPersonalAccidentsPolicy.IsEnabled = true;

        }

        private void mItemPolicyRenewal_Click(object sender, RoutedEventArgs e)
        {
            if (dgElementaryPolicies.SelectedItem != null)
            {
                ElementaryPolicy policy = (ElementaryPolicy)dgElementaryPolicies.SelectedItem;
                if (policy.CarPolicy != null)
                {
                    frmNewRenewal carRenewal = new frmNewRenewal(client, policy, user);
                    carRenewal.ShowDialog();
                }
                else if (policy.BusinessPolicy != null || policy.ApartmentPolicy != null)
                {
                    frmApartmentRenewal aptBusinessRenewal = new frmApartmentRenewal(client, policy, user);
                    aptBusinessRenewal.ShowDialog();
                }
                DataGridPoliciesBinding(client);
            }
        }

        private void mItemForeingWorkersPolicyRenewal_Click(object sender, RoutedEventArgs e)
        {
            if (dgForeingWorkersPolicies.SelectedItem != null)
            {
                ForeingWorkersPolicy policy = (ForeingWorkersPolicy)dgForeingWorkersPolicies.SelectedItem;
                frmForeingWorkersRenewal foreingWorkersRenewal = new frmForeingWorkersRenewal(client, policy, user);
                foreingWorkersRenewal.ShowDialog();
                dgForeingWorkersBinding();
            }
        }

        private void mItemExcelAccidents_Click(object sender, RoutedEventArgs e)
        {
            excelLogic.PersonalAccidentsPoliciesToExcel((List<PersonalAccidentsPolicy>)dgPersonalAccidentsPolicies.ItemsSource);
        }

        private void mItemExcelTravel_Click(object sender, RoutedEventArgs e)
        {
            excelLogic.TravelPoliciesToExcel((List<TravelPolicy>)dgTravelPolicies.ItemsSource);
        }

        private void mItemExcelLife_Click(object sender, RoutedEventArgs e)
        {
            excelLogic.LifePoliciesToExcel((List<LifePolicy>)dgLife.ItemsSource);
        }

        private void mItemExcelHealth_Click(object sender, RoutedEventArgs e)
        {
            excelLogic.HealthPoliciesToExcel((List<HealthPolicy>)dgHealth.ItemsSource);
        }

        private void mItemExcelForeingWorkers_Click(object sender, RoutedEventArgs e)
        {
            excelLogic.ForeingWorkersPoliciesToExcel((List<ForeingWorkersPolicy>)dgForeingWorkersPolicies.ItemsSource);
        }

        private void mItemExcelFinance_Click(object sender, RoutedEventArgs e)
        {
            excelLogic.FinancePoliciesToExcel((List<FinancePolicy>)dgFinance.ItemsSource);
        }

        private void mItemExistingClientsExcel_Click(object sender, RoutedEventArgs e)
        {
            excelLogic.ClientsToExcel(((Client[])dgExistingClients.ItemsSource).ToList());
        }

        private void mItemNonActiveClientsExcel_Click(object sender, RoutedEventArgs e)
        {
            excelLogic.ClientsToExcel(((Client[])dgNonActiveClients.ItemsSource).ToList());
        }

        private void mItemPotencialClientsExcel_Click(object sender, RoutedEventArgs e)
        {
            excelLogic.ClientsToExcel(((Client[])dgPotentialClients.ItemsSource).ToList());
        }

        private void mItemExcelOffersExcel_Click(object sender, RoutedEventArgs e)
        {
            excelLogic.AllPoliciesToExcel((List<object>)dgOffers.ItemsSource);
        }

        private void mItemExcelClaimsExcel_Click(object sender, RoutedEventArgs e)
        {
            excelLogic.AllClaimsToExcel((List<object>)dgClientClaims.ItemsSource);
        }

        private void mItemPrintPoliciesList_Click(object sender, RoutedEventArgs e)
        {            
            //frmPrintPreview preview = new frmPrintPreview("",dgElementaryPolicies.ItemsSource);
            //preview.ShowDialog();
        }

        private void dgExistingClients_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (dgExistingClients.SelectedItem == null)
            {
                txtScrollingBinding(null);
            }
        }

        private void miSnippingTool_Click(object sender, RoutedEventArgs e)
        {
            Process snippingToolProcess = new Process();
            snippingToolProcess.EnableRaisingEvents = true;
            if (!Environment.Is64BitProcess)
            {
                snippingToolProcess.StartInfo.FileName = "C:\\Windows\\sysnative\\SnippingTool.exe";
                snippingToolProcess.Start();
            }
            else
            {
                snippingToolProcess.StartInfo.FileName = "C:\\Windows\\system32\\SnippingTool.exe";
                snippingToolProcess.Start();
            }
        }

        private void mItemSendEmail_Click(object sender, RoutedEventArgs e)
        {
            if (client == null)
            {
                MessageBox.Show("נא לסמן לקוח", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            if (client.Email == "")
            {
                MessageBox.Show("אין ללקוח כתובת דוא''ל במערכת", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            string[] address = new string[] { client.Email };
            try
            {
                email.SendEmailFromOutlook("", "", null, address);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void mItemAddOfferToCar_Click(object sender, RoutedEventArgs e)
        {
            frmElementary updateElementaryPolicy = new frmElementary(client, user, true, (ElementaryPolicy)dgOffers.SelectedItem);
            updateElementaryPolicy.ShowDialog();
            DataGridOffersBinding(client);
        }

        private void dgOffers_ContextMenuOpening(object sender, ContextMenuEventArgs e)
        {
            if (dgOffers.SelectedItem != null && dgOffers.SelectedItem is ElementaryPolicy)
            {
                if (((ElementaryPolicy)dgOffers.SelectedItem).CarPolicy != null)
                {
                    mItemAddOfferToCar.IsEnabled = true;
                }
            }
        }

        private void dgOffers_ContextMenuClosing(object sender, ContextMenuEventArgs e)
        {
            mItemAddOfferToCar.IsEnabled = false;
        }

        private void dgOffers_MouseDown(object sender, MouseButtonEventArgs e)
        {
            dgOffers.UnselectAll();
        }

        private void miCoralSoftware_Click(object sender, RoutedEventArgs e)
        {
            frmAboutUs aboutUsWindow = new frmAboutUs();
            aboutUsWindow.ShowDialog();
        }

        private void btnPolicyOwners_Click(object sender, RoutedEventArgs e)
        {
            frmPolicyOwners policyOwnersWindow = new frmPolicyOwners(user);
            policyOwnersWindow.ShowDialog();
        }

        private void miStandardMoneyCollection_Click(object sender, RoutedEventArgs e)
        {
            frmStandardMoneyCollection standardMoneyCollectionWindow = new frmStandardMoneyCollection(user);
            standardMoneyCollectionWindow.Closed += (object o, EventArgs ea) =>
            {
                OnChildWindowClosed();
            };
            standardMoneyCollectionWindow.ShowDialog();
        }

        private void mItemClientRenwals_Click(object sender, RoutedEventArgs e)
        {
            if (dgExistingClients.SelectedItem != null)
            {
                frmRenewals renewalsWindow = new frmRenewals(user, client);
                renewalsWindow.Show();
            }
        }

        private void miProduction_Click(object sender, RoutedEventArgs e)
        {
            ImportProdFrm productionWindow = new ImportProdFrm();
            productionWindow.ShowDialog();
            while (productionWindow.UpdateAgent == true)
            {
                frmAddAgent updateAgent = new frmAddAgent(productionWindow.AgentSelected);
                updateAgent.ShowDialog();
                productionWindow = new ImportProdFrm(productionWindow.AgentSelected, productionWindow.CompanySelected, productionWindow.InsuranceSelected, productionWindow._prodFileName);
                productionWindow.ShowDialog();
            }
            tbItemExistingClients.IsSelected = true;
            DataGridClientsBinding(false);
        }

        private void mItemTransferPolicy_Click(object sender, RoutedEventArgs e)
        {
            if (tbItemElementary.IsSelected && dgElementaryPolicies.SelectedItem != null)
            {
                frmPolicyTransfer policyTransferWindow = new frmPolicyTransfer(dgElementaryPolicies.SelectedItem);
                policyTransferWindow.ShowDialog();
                DataGridPoliciesBinding(client);
            }
            else if (tbItemTravel.IsSelected && dgTravelPolicies.SelectedItem != null)
            {
                frmPolicyTransfer policyTransferWindow = new frmPolicyTransfer(dgTravelPolicies.SelectedItem);
                policyTransferWindow.ShowDialog();
                dgTravelPoliciesBinding();
            }
            else if (tbItemForeingWorkers.IsSelected && dgForeingWorkersPolicies.SelectedItem != null)
            {
                frmPolicyTransfer policyTransferWindow = new frmPolicyTransfer(dgForeingWorkersPolicies.SelectedItem);
                policyTransferWindow.ShowDialog();
                dgForeingWorkersBinding();
            }
            else if (tbItemHealth.IsSelected && dgHealth.SelectedItem != null)
            {
                frmPolicyTransfer policyTransferWindow = new frmPolicyTransfer(dgHealth.SelectedItem);
                policyTransferWindow.ShowDialog();
                dgHealthBinding();
            }
            else if (tbItemLife.IsSelected && dgLife.SelectedItem != null)
            {
                frmPolicyTransfer policyTransferWindow = new frmPolicyTransfer(dgLife.SelectedItem);
                policyTransferWindow.ShowDialog();
                dgLifeBinding();
            }
            else if (tbItemPersonalAccidents.IsSelected && dgPersonalAccidentsPolicies.SelectedItem != null)
            {
                frmPolicyTransfer policyTransferWindow = new frmPolicyTransfer(dgPersonalAccidentsPolicies.SelectedItem);
                policyTransferWindow.ShowDialog();
                dgPersonalAccidentsPoliciesBinding();
            }
            else if (tbItemFinance.IsSelected && dgFinance.SelectedItem != null)
            {
                frmPolicyTransfer policyTransferWindow = new frmPolicyTransfer(dgFinance.SelectedItem);
                policyTransferWindow.ShowDialog();
                dgFinanceBinding();
            }
            else
            {
                MessageBox.Show("נא לסמן את הפוליסה שברצונך להעביר", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }

        }

        private void miBackup_Click(object sender, RoutedEventArgs e)
        {
            frmBackup backupWindows = new frmBackup(user);
            backupWindows.ShowDialog();
        }
    }
}

