﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using CoralBusinessLogics;
using System.IO;

namespace CoralUI
{
    /// <summary>
    /// Interaction logic for frmTravelClaims.xaml
    /// </summary>
    public partial class frmTravelClaims : Window
    {
        Client client = null;
        TravelPolicy policy = null;
        TravelClaimsLogic claimLogic = new TravelClaimsLogic();
        InputsValidations validations = new InputsValidations();
        int claimId;
        List<TravelWitness> witnesses = new List<TravelWitness>();
        decimal? totalAmountClaimed = 0;
        List<TravelClaimTracking> trackings = new List<TravelClaimTracking>();
        TrackingsLogics trackingLogic = new TrackingsLogics();
        User user;
        TravelClaim claim = null;
        ListViewSettings lv = new ListViewSettings();
        string path = "";
        string newPath = "";
        string clientDirPath = "";
        List<object> nullErrorList = null;
        InputsValidations validation = new InputsValidations();
        InsurancesLogic insuranceLogic = new InsurancesLogic();
        string[] claimPaths = null;
        List<TravelDamage> damages = new List<TravelDamage>();
        string[] travelPaths = null;
        private bool isChanges=false;
        bool confirmBeforeClosing = true;
        bool cancelClose = false;
        TravelWitness witnessSelected;

        public frmTravelClaims(Client policyClient, TravelPolicy policyClaim, User userAccount)
        {
            InitializeComponent();
            //this.SizeToContent = SizeToContent.Manual;
            client = policyClient;
            policy = policyClaim;
            user = userAccount;
        }
        public frmTravelClaims(TravelClaim claimToUpdate, Client policyClient, User userAccount)
        {
            InitializeComponent();
            //this.SizeToContent = SizeToContent.Manual;
            client = policyClient;
            claim = claimToUpdate;
            policy = claim.TravelPolicy;
            user = userAccount;
        }
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            string strAppDir = System.AppDomain.CurrentDomain.BaseDirectory;
            string path = strAppDir + @"Coral Files\ClientsPath.txt";

            if (File.Exists(path))
            {
                clientDirPath = File.ReadAllText(path);
            }
            else
            {
                MessageBox.Show("לא נמצא תיקיית הלקוחות. נא בדוק בהגדרות ראשוניות את נתיב התיקייה");
                Close();
                return;
            }
            if (claim != null)
            {
                travelPaths = Directory.GetDirectories(clientDirPath + @"/" + client.ClientID + @"/נסיעות לחו''ל", "*" + claim.TravelPolicy.PolicyNumber + "*");
            }
            else if (policy != null)
            {
                travelPaths = Directory.GetDirectories(clientDirPath + @"/" + client.ClientID + @"/נסיעות לחו''ל", "*" + policy.PolicyNumber + "*");
            }
            //if (travelPaths.Count() > 0)
            //{
            //    claimPaths = Directory.GetDirectories(travelPaths[0], "תביעות*");
            //    if (claimPaths.Count() > 0)
            //    {
            //        path = claimPaths[0];
            //    }
            //}
            lblNameSpace.Content = client.FirstName + " " + client.LastName;
            lblCellPhoneSpace.Content = client.CellPhone;
            lblEmailSpace.Content = client.Email;
            lblEndDateSpace.Content = ((DateTime)policy.EndDate).Date;
            lblIdSpace.Content = client.IdNumber;
            lblPhoneSpace.Content = client.PhoneHome;
            lblStartDateSpace.Content = ((DateTime)policy.StartDate).Date;
            lblCompanySpace.Content = policy.Company.CompanyName;
            dpOpenDate.SelectedDate = DateTime.Now;
            txtPolicyNumber.Text = policy.PolicyNumber;
            cbClaimTypeBinding();
            cbClaimConditionBinding();
            cbCompanyBinding();
            if (claim != null)
            {
                dpOpenDate.SelectedDate = claim.OpenDate;
                txtPolicyNumber.Text = claim.TravelPolicy.PolicyNumber;
                txtClaimNumber.Text = claim.ClaimNumber;
                txtThirdPartyClaimNumber.Text = claim.ThirdPartyClaimNumber;
                dpDeliveryDateToCompany.SelectedDate = claim.DeliveredToCompanyDate;
                dpMoneyReceivedDate.SelectedDate = claim.MoneyReceivedDate;
                txtAmountClaimed.Text = validations.ConvertDecimalToString(claim.ClaimAmount);
                txtAmountReceived.Text = validations.ConvertDecimalToString(claim.AmountReceived);
                dpEventDate.SelectedDate = claim.EventDateAndTime;
                tpEventHour.Value = claim.EventDateAndTime;
                txtEventPlace.Text = claim.EventPlace;
                txtEventDescription.Text = claim.EventDescription;
                txtContactName.Text = claim.ContactName;
                txtContactAddress.Text = claim.ContactAddress;
                txtContactPhone.Text = claim.ContactPhone;
                txtContactCellPhone.Text = claim.ContactCellPhone;
                txtContactFax.Text = claim.ContactFax;
                txtContactEmail.Text = claim.ContactEmail;
                witnesses = claimLogic.GetWitnessesByClaim(claim.TravelClaimID);
                if (claim.TravelClaimTypeID != null)
                {
                    SelectClaimTypeItemInCb((int)claim.TravelClaimTypeID);
                }
                if (claim.ClaimStatus == true)
                {
                    cbStatus.SelectedIndex = 0;
                }
                else
                {
                    cbStatus.SelectedIndex = 1;
                }
                if (claim.TravelClaimConditionID != null)
                {
                    SelectClaimConditionItemInCb((int)claim.TravelClaimConditionID);
                }
                dgWitnessesBinding();
                trackings = trackingLogic.GetAllTravelClaimTrackingsByClaimID(claim.TravelClaimID);
                dgTrackingsBinding();
                damages = claimLogic.GetAllDamagesByClaim(claim.TravelClaimID);
                dgCalimDetailsBinding();
                txtTotalItemsAmount.Text = validations.ConvertDecimalToString(claim.TotalItmesAmount);
                if (claim.TotalItmesAmount != null)
                {
                    totalAmountClaimed = (decimal)claim.TotalItmesAmount;
                }
                if (claim.IsThirdPartyDamages == true)
                {
                    chbThirdPartyDamage.IsChecked = true;
                    txtDamagedName.Text = claim.DamagedName;
                    txtDamagedId.Text = claim.DamagedIdNumber;
                    txtDamagedAddress.Text = claim.DamagedAddress;
                    txtDamagedPhone.Text = claim.DamagedPhone;
                    txtDamagedCellPhone.Text = claim.DamagedCellPhone;
                    txtDamagedEmail.Text = claim.DamagedEmail;
                    txtDamageDescipion.Text = claim.DamageDescription;
                    txtThirdPartyTotalAmountClaimed.Text = validations.ConvertDecimalToString(claim.DamageAmountClaimed);
                    var companies = cbThirdPartyInsuranceCompany.Items;
                    foreach (var item in companies)
                    {
                        InsuranceCompany company = (InsuranceCompany)item;
                        if (company.CompanyID == claim.DamagedCompanyID)
                        {
                            cbThirdPartyInsuranceCompany.SelectedItem = item;
                            break;
                        }
                    }
                }
            }
        }

        private void dgCalimDetailsBinding()
        {
            dgCalimDetails.ItemsSource = damages.ToList();
        }

        private void cbClaimTypeBinding()
        {
            cbClaimType.ItemsSource = claimLogic.GetActiveTravelClaimTypes();
            cbClaimType.DisplayMemberPath = "ClaimTypeName";
        }

        private void cbClaimConditionBinding()
        {
            cbClaimCondition.ItemsSource = claimLogic.GetActiveTravelClaimConditions();
            cbClaimCondition.DisplayMemberPath = "Description";
        }

        private void cbCompanyBinding()
        {
            cbThirdPartyInsuranceCompany.ItemsSource = insuranceLogic.GetActiveCompaniesByInsurance("נסיעות לחו''ל");
            cbThirdPartyInsuranceCompany.DisplayMemberPath = "Company.CompanyName";
        }

        private void dgTrackingsBinding()
        {
            dgTrackings.ItemsSource = trackings.OrderByDescending(t => t.Date);
            lv.AutoSizeColumns(dgTrackings.View);
        }

        private void dgWitnessesBinding()
        {
            dgWitnesses.ItemsSource = witnesses.ToList();
            lv.AutoSizeColumns(dgWitnesses.View);
        }

        private void SelectClaimConditionItemInCb(int id)
        {
            var items = cbClaimCondition.Items;
            foreach (var item in items)
            {
                TravelClaimCondition parsedItem = (TravelClaimCondition)item;
                if (parsedItem.TravelClaimConditionID == id)
                {
                    cbClaimCondition.SelectedItem = item;
                    break;
                }
            }
        }

        private void SelectClaimTypeItemInCb(int id)
        {
            var items = cbClaimType.Items;
            foreach (var item in items)
            {
                TravelClaimType parsedItem = (TravelClaimType)item;
                if (parsedItem.TravelClaimTypeID == id)
                {
                    cbClaimType.SelectedItem = item;
                    break;
                }
            }
        }

        private void chbThirdPartyDamage_Checked(object sender, RoutedEventArgs e)
        {
            gbThirdPartyDetails.IsEnabled = true;
        }

        private void chbThirdPartyDamage_Unchecked(object sender, RoutedEventArgs e)
        {
            gbThirdPartyDetails.IsEnabled = false;
        }

        private void btnUpdateClaimTypeTable_Click(object sender, RoutedEventArgs e)
        {
            int? selectedItemId = null;
            if (cbClaimType.SelectedItem != null)
            {
                selectedItemId = ((TravelClaimType)cbClaimType.SelectedItem).TravelClaimTypeID;
            }
            frmUpdateTable updateClaimTypesTable = new frmUpdateTable(new TravelClaimType());
            updateClaimTypesTable.ShowDialog();
            cbClaimTypeBinding();
            if (updateClaimTypesTable.ItemAdded != null)
            {
                SelectClaimTypeItemInCb(((TravelClaimType)updateClaimTypesTable.ItemAdded).TravelClaimTypeID);
            }
            else if (selectedItemId != null)
            {
                SelectClaimTypeItemInCb((int)selectedItemId);
            }
        }

        private void btnUpdateClaimConditionTable_Click(object sender, RoutedEventArgs e)
        {
            int? selectedItemId = null;
            if (cbClaimCondition.SelectedItem != null)
            {
                selectedItemId = ((TravelClaimCondition)cbClaimCondition.SelectedItem).TravelClaimConditionID;
            }
            frmUpdateTable updateClaimConditionsTable = new frmUpdateTable(new TravelClaimCondition());
            updateClaimConditionsTable.ShowDialog();
            cbClaimConditionBinding();
            if (updateClaimConditionsTable.ItemAdded != null)
            {
                SelectClaimConditionItemInCb(((TravelClaimCondition)updateClaimConditionsTable.ItemAdded).TravelClaimConditionID);
            }
            else if (selectedItemId != null)
            {
                SelectClaimConditionItemInCb((int)selectedItemId);
            }
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            if (nullErrorList != null && nullErrorList.Count > 0)
            {
                foreach (var item in nullErrorList)
                {
                    if (item is TextBox)
                    {
                        ((TextBox)item).ClearValue(BorderBrushProperty);
                    }
                    else if (item is ComboBox)
                    {
                        ((ComboBox)item).ClearValue(BorderBrushProperty);
                    }
                    else if (item is DatePicker)
                    {
                        ((DatePicker)item).ClearValue(BorderBrushProperty);
                    }
                }
            }
            nullErrorList = validation.InputNullValidation(new object[] { dpEventDate });
            if (nullErrorList.Count > 0)
            {
                tbItemGeneral.Focus();
                MessageBox.Show("נא למלאות שדות חובה", "", MessageBoxButton.OK, MessageBoxImage.Error);
                cancelClose = true;
                return;
            }
            bool isClaimOpen = true;
            if (cbStatus.SelectedIndex == 1)
            {
                isClaimOpen = false;
            }
            decimal? amounClaimed = null;
            if (txtAmountClaimed.Text != "")
            {
                amounClaimed = decimal.Parse(txtAmountClaimed.Text);
            }
            decimal? amountRecived = null;
            if (txtAmountReceived.Text != "")
            {
                amountRecived = decimal.Parse(txtAmountReceived.Text);
            }
            DateTime openDate = (DateTime)dpOpenDate.SelectedDate;
            DateTime? eventDateTime = null;
            if (tpEventHour.Value != null)
            {
                DateTime eventTime = Convert.ToDateTime(tpEventHour.Value.ToString());
                eventDateTime = new DateTime(((DateTime)dpEventDate.SelectedDate).Year, ((DateTime)dpEventDate.SelectedDate).Month, ((DateTime)dpEventDate.SelectedDate).Day, eventTime.Hour, eventTime.Minute, eventTime.Second, eventTime.Millisecond);
            }
            else
            {
                if (dpEventDate.SelectedDate != null)
                {
                    eventDateTime = (DateTime)dpEventDate.SelectedDate;
                }
            }
            int? typeID = null;
            if (cbClaimType.SelectedItem != null)
            {
                typeID = ((TravelClaimType)cbClaimType.SelectedItem).TravelClaimTypeID;
            }
            int? conditionID = null;
            if (cbClaimCondition.SelectedItem != null)
            {
                conditionID = ((TravelClaimCondition)cbClaimCondition.SelectedItem).TravelClaimConditionID;
            }
            decimal? damageAmountClaimed = null;
            if (txtThirdPartyTotalAmountClaimed.Text != "")
            {
                damageAmountClaimed = decimal.Parse(txtThirdPartyTotalAmountClaimed.Text);
            }
            int? damagedCompanyId = null;
            if (cbThirdPartyInsuranceCompany.SelectedItem != null)
            {
                damagedCompanyId = ((InsuranceCompany)cbThirdPartyInsuranceCompany.SelectedItem).CompanyID;
            }
            decimal? totalItemsAmount = null;
            if (txtTotalItemsAmount.Text != "")
            {
                totalItemsAmount = decimal.Parse(txtTotalItemsAmount.Text);
            }
            try
            {
                if (claim == null)
                {
                    claimId = claimLogic.InsertTravelClaim(policy.TravelPolicyID, typeID, conditionID, isClaimOpen, txtClaimNumber.Text, openDate, txtThirdPartyClaimNumber.Text, dpDeliveryDateToCompany.SelectedDate, dpMoneyReceivedDate.SelectedDate, amounClaimed, amountRecived, eventDateTime, txtEventPlace.Text, txtEventDescription.Text, txtContactName.Text, txtContactAddress.Text, txtContactPhone.Text, txtContactCellPhone.Text, txtContactFax.Text, txtContactEmail.Text, damageAmountClaimed, txtDamagedAddress.Text, txtDamagedCellPhone.Text, damagedCompanyId, txtDamagedEmail.Text, txtDamageDescipion.Text, txtDamagedId.Text, txtDamagedName.Text, txtDamagedPhone.Text, chbThirdPartyDamage.IsChecked, totalItemsAmount);
                    if (travelPaths.Count() > 0)
                    {
                        path = travelPaths[0] + @"/תביעה " + ((DateTime)dpEventDate.SelectedDate).ToString("dd-MM-yyyy");
                    }
                }
                else
                {
                    claimId = claimLogic.UpdateTravelClaim(claim.TravelClaimID, typeID, conditionID, isClaimOpen, txtClaimNumber.Text, openDate, txtThirdPartyClaimNumber.Text, dpDeliveryDateToCompany.SelectedDate, dpMoneyReceivedDate.SelectedDate, amounClaimed, amountRecived, eventDateTime, txtEventPlace.Text, txtEventDescription.Text, txtContactName.Text, txtContactAddress.Text, txtContactPhone.Text, txtContactCellPhone.Text, txtContactFax.Text, txtContactEmail.Text, damageAmountClaimed, txtDamagedAddress.Text, txtDamagedCellPhone.Text, damagedCompanyId, txtDamagedEmail.Text, txtDamageDescipion.Text, txtDamagedId.Text, txtDamagedName.Text, txtDamagedPhone.Text, chbThirdPartyDamage.IsChecked, totalItemsAmount);
                    if (travelPaths.Count() > 0)
                    {
                        newPath = travelPaths[0] + @"/תביעה " + ((DateTime)dpEventDate.SelectedDate).ToString("dd-MM-yyyy");
                        path = travelPaths[0] + @"/תביעה " + ((DateTime)claim.EventDateAndTime).ToString("dd-MM-yyyy");
                    }
                }
                SaveUnsavedWitness();
                if (witnesses.Count > 0)
                {
                    claimLogic.InsertWitnesses(witnesses, claimId);
                }
                if (damages.Count > 0)
                {
                    claimLogic.InsertDamages(claimId, damages);
                }
                if (trackings.Count > 0)
                {
                    trackingLogic.InsertTravelClaimTrackings(trackings, claimId, user.UserID);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "", MessageBoxButton.OK, MessageBoxImage.Error);
            }

            if (claim == null)
            {
                if (path != null && path != "")
                {
                    if (!Directory.Exists(@path))
                    {
                        Directory.CreateDirectory(@path);
                    }
                }
                else
                {
                    System.Windows.Forms.MessageBox.Show("לא ניתן ליצור את תיקיית התביעה");
                }
            }
            else
            {
                if (path != "")
                {
                    if (Directory.Exists(path) && newPath != "")
                    {
                        if (path != newPath)
                        {
                            try
                            {
                                Directory.Move(path, newPath);
                            }
                            catch (Exception)
                            {
                                System.Windows.Forms.MessageBox.Show("לא ניתן למצוא את נתיב התיקייה");
                            }
                        }
                    }
                }
                else
                {
                    if (newPath != "")
                    {
                        Directory.CreateDirectory(newPath);
                    }
                }
            }

            if (sender is Button)
            {
                Button btn = (Button)sender;
                if (btn.Name == "btnSave")
                {
                    confirmBeforeClosing = false;
                    Close();
                }
            }
        }

        private void txtAmountClaimed_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (sender is TextBox)
            {
                TextBox txtBox = (TextBox)sender;
                txtBox.ClearValue(BorderBrushProperty);
                if (txtBox.Text != "")
                {
                    try
                    {
                        decimal reusult = validations.ConvertStringToDecimal(txtBox.Text);
                    }
                    catch (Exception ex)
                    {
                        txtBox.BorderBrush = System.Windows.Media.Brushes.Red;
                        System.Windows.Forms.MessageBox.Show(ex.Message);
                        txtBox.Text = "";
                    }
                }
            }
        }

        private void btnAddWitness_Click(object sender, RoutedEventArgs e)
        {
            if (witnessSelected == null)
            {
                TravelWitness witness = new TravelWitness() { WitnessName = txtWitnessName.Text, WittnessAddress = txtWitnessAddress.Text, WitnessPhone = txtWitnessPhone.Text, WitnessCellPhone = txtWitnessCellPhone.Text };
                witnesses.Add(witness);
            }
            else
            {
                witnessSelected.WitnessCellPhone = txtWitnessCellPhone.Text;
                witnessSelected.WitnessName = txtWitnessName.Text;
                witnessSelected.WitnessPhone = txtWitnessPhone.Text;
                witnessSelected.WittnessAddress = txtWitnessAddress.Text;
            }
            dgWitnessesBinding();
            dgWitnesses.UnselectAll();
            txtWitnessAddress.Clear();
            txtWitnessCellPhone.Clear();
            txtWitnessName.Clear();
            txtWitnessPhone.Clear();
        }

        private void btnAddItem_Click(object sender, RoutedEventArgs e)
        {
            //decimal? amountClaimed = null;
            //if (txtItemAmountClaimed.Text != "")
            //{
            //    amountClaimed = decimal.Parse(txtItemAmountClaimed.Text);
            //}
            //TravelDamage damage = new TravelDamage() { ItemDescription = txtItemDescription.Text, AmountClaimed = amountClaimed, Comments = txtItemComments.Text };
            frmClaimItem addClaimItem = new frmClaimItem();
            addClaimItem.ShowDialog();
            if (addClaimItem.Damage != null)
            {
                damages.Add(addClaimItem.Damage);
                dgCalimDetailsBinding();
                if (addClaimItem.Damage.AmountClaimed != null)
                {
                    totalAmountClaimed = totalAmountClaimed + (decimal)addClaimItem.Damage.AmountClaimed;
                }
            }

            txtTotalItemsAmount.Text = validations.ConvertDecimalToString(totalAmountClaimed);
            //txtItemDescription.Clear();
            //txtItemAmountClaimed.Clear();
            //txtItemComments.Clear();
        }

        private void btnNewTracking_Click(object sender, RoutedEventArgs e)
        {
            frmTracking newTrackingWindow = new frmTracking(client, policy, user);
            newTrackingWindow.ShowDialog();
            if (newTrackingWindow.trackingContent != null)
            {
                trackings.Add(new TravelClaimTracking { TrackingContent = newTrackingWindow.trackingContent, Date = DateTime.Now, User = user });
                dgTrackingsBinding();
            }
        }

        private void btnDeleteTracking_Click(object sender, RoutedEventArgs e)
        {
            if (dgTrackings.SelectedItem == null)
            {
                MessageBox.Show("נא לסמן מעקב בטבלה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            if (MessageBox.Show("למחוק מעקב", "אישור", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                if (((TravelClaimTracking)dgTrackings.SelectedItem).TravelClaimTrackingID != 0)
                {
                    trackingLogic.DeleteTravelClaimTracking((TravelClaimTracking)dgTrackings.SelectedItem);
                }
                trackings.Remove((TravelClaimTracking)dgTrackings.SelectedItem);
                dgTrackingsBinding();
            }
            else
            {
                dgTrackings.UnselectAll();
            }
        }

        private void dgTrackings_MouseDown(object sender, MouseButtonEventArgs e)
        {
            dgTrackings.UnselectAll();
        }


        private void btnUpdateTracking_Click(object sender, RoutedEventArgs e)
        {
            if (dgTrackings.SelectedItem == null)
            {
                MessageBox.Show("נא לסמן מעקב בטבלה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            TravelClaimTracking trackingToUpdate = (TravelClaimTracking)dgTrackings.SelectedItem;
            frmTracking updateTrackingWindow = new frmTracking(trackingToUpdate, client, policy, user);
            updateTrackingWindow.ShowDialog();
            trackingToUpdate.TrackingContent = updateTrackingWindow.trackingContent;
            dgTrackingsBinding();
            dgTrackings.UnselectAll();
        }


        private void dgWitnesses_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            witnessSelected = (TravelWitness)dgWitnesses.SelectedItem;
            if (witnessSelected != null)
            {
                txtWitnessName.Text = witnessSelected.WitnessName;
                txtWitnessAddress.Text = witnessSelected.WittnessAddress;
                txtWitnessPhone.Text = witnessSelected.WitnessPhone;
                txtWitnessCellPhone.Text = witnessSelected.WitnessCellPhone;
            }
        }

        private void dgWitnesses_MouseDown(object sender, MouseButtonEventArgs e)
        {
            dgWitnesses.UnselectAll();
            txtWitnessName.Clear();
            txtWitnessAddress.Clear();
            txtWitnessPhone.Clear();
            txtWitnessCellPhone.Clear();
        }

        private void dpEventDate_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            if (dpEventDate.SelectedDate != null)
            {
                tpEventHour.IsEnabled = true;
            }
            else
            {
                tpEventHour.IsEnabled = false;
                tpEventHour.Value = null;
            }
        }

        private void dgTrackings_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            DependencyObject src = (DependencyObject)(e.OriginalSource);
            while (!(src is Control))
            {
                if (!(src is Visual))
                {
                    break;
                }
                src = VisualTreeHelper.GetParent(src);
            }

            if ((src is Control && src.GetType() == typeof(ListViewItem)) || src.GetType() == typeof(Run))
            {
                if (dgTrackings.SelectedItem != null)
                {
                    TravelClaimTracking trackingToUpdate = (TravelClaimTracking)dgTrackings.SelectedItem;
                    frmTracking updateTrackingWindow = new frmTracking(trackingToUpdate, client, policy, user);
                    updateTrackingWindow.ShowDialog();
                    trackingToUpdate.TrackingContent = updateTrackingWindow.trackingContent;
                    dgTrackingsBinding();
                    dgTrackings.UnselectAll();
                }
            }
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void dgPolicyOwners_MouseDown(object sender, MouseButtonEventArgs e)
        {
            dgCalimDetails.UnselectAll();
        }

        private void btnUpdateItem_Click(object sender, RoutedEventArgs e)
        {
            if (dgCalimDetails.SelectedItem == null)
            {
                MessageBox.Show("נא סמן את הפריט שברצונך לעדכן", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            TravelDamage item = (TravelDamage)dgCalimDetails.SelectedItem;
            frmClaimItem updateItem = new frmClaimItem(dgCalimDetails.SelectedItem);
            updateItem.ShowDialog();
            if (updateItem.Damage != null)
            {
                item.AmountClaimed = updateItem.Damage.AmountClaimed;
                item.Comments = updateItem.Damage.Comments;
                item.ItemDescription = updateItem.Damage.ItemDescription;
                dgCalimDetailsBinding();
                dgCalimDetails.UnselectAll();
                if (updateItem.Damage.AmountClaimed != null)
                {
                    totalAmountClaimed = damages.Where(d=>d.AmountClaimed!=null).Sum(d => d.AmountClaimed);
                }
            }
            txtTotalItemsAmount.Text = validations.ConvertDecimalToString(totalAmountClaimed);
        }

        private void btnDeleteItem_Click(object sender, RoutedEventArgs e)
        {
            if (dgCalimDetails.SelectedItem == null)
            {
                MessageBox.Show("נא סמן את הפריט שברצונך למחוק", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            TravelDamage item = (TravelDamage)dgCalimDetails.SelectedItem;
            if (MessageBox.Show("למחוק פריט", "אישור", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                if (item.TravelDamageID != 0)
                {
                    claimLogic.DeleteDamage(item);
                }
                damages.Remove(item);
                dgCalimDetailsBinding();
            }
            else
            {
                dgCalimDetails.UnselectAll();
            }
        }

        private void cbClaimType_GotFocus(object sender, RoutedEventArgs e)
        {
            isChanges = true;
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (confirmBeforeClosing && isChanges)
            {
                if (MessageBox.Show("האם ברצונך לשמור שינוים שבצעת", "אישור", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    btnSave_Click(sender, new RoutedEventArgs());
                    if (cancelClose)
                    {
                        e.Cancel = true;
                        cancelClose = false;
                    }
                }
            }
        }
        private void SaveUnsavedWitness()
        {
            if (txtWitnessName.Text != "" || txtWitnessAddress.Text != "" || txtWitnessPhone.Text != "" || txtWitnessCellPhone.Text != "")
            {
                btnAddWitness_Click(this, new RoutedEventArgs());
            }
        }

       
        private void CompanySelection(int companyID)
        {
            var companies = cbThirdPartyInsuranceCompany.Items;
            foreach (var item in companies)
            {
                InsuranceCompany company = (InsuranceCompany)item;
                if (company.CompanyID == companyID)
                {
                    cbThirdPartyInsuranceCompany.SelectedItem = item;
                    break;
                }
            }
        }

        private void btnUpdateThirdPartyInsuranceCompanyTable_Click_1(object sender, RoutedEventArgs e)
        {
            int? selectedItemId = null;
            if (cbThirdPartyInsuranceCompany.SelectedItem != null)
            {
                selectedItemId = ((InsuranceCompany)cbThirdPartyInsuranceCompany.SelectedItem).CompanyID;
            }
            int insuranceID = insuranceLogic.GetInsuranceID("נסיעות לחו''ל");
            InsuranceCompany company = new InsuranceCompany() { InsuranceID = insuranceID };
            frmUpdateTable updateCompaniesTable = new frmUpdateTable(company);
            updateCompaniesTable.ShowDialog();
            cbCompanyBinding();
            if (updateCompaniesTable.ItemAdded != null)
            {
                CompanySelection(((InsuranceCompany)updateCompaniesTable.ItemAdded).CompanyID);
            }
            else if (selectedItemId != null)
            {
                CompanySelection((int)selectedItemId);
            }
        }
    }
}
