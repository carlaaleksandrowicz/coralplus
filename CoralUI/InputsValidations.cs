﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace CoralUI
{
    public class InputsValidations
    {
        public bool IsDigitsOnly(string str)
        {
            foreach (char c in str)
            {
                if (c < '0' || c > '9')
                    return false;
            }

            return true;
        }

        //פונקציה הבודקת שתוכן תיבות הטקסט הינו מיספרי
        public List<TextBox> OnlyNumbersValidation(TextBox[] inputsToValidate)
        {
            List<TextBox> inputsErrorList = new List<TextBox>();
            foreach (TextBox inputToValidate in inputsToValidate)
            {
                string text = inputToValidate.Text;
                int dotCount = 0;
                foreach (char c in text)
                {
                    if (c == '.')
                    {
                        dotCount++;
                    }
                    if (((c < '0' || c > '9') && c != '.' && c != '-') || dotCount > 1)
                    {

                        inputToValidate.BorderBrush = System.Windows.Media.Brushes.Red;
                        inputsErrorList.Add(inputToValidate);
                        break;
                    }
                }
            }
            return inputsErrorList;
        }

        //הופך טקסט למספר אם ניתן
        public decimal ConvertStringToDecimal(string text)
        {
            decimal convertedText = 0;
            decimal result;
            if (Decimal.TryParse(text, out result))
            {
                convertedText = result;
                return convertedText;
            }
            else
            {
                throw new Exception("מס' לא תקין");
            }
        }

        public string ConvertDecimalToString(decimal? number)
        {            
            if (number != null)
            {
                return ((decimal)number).ToString("0.##");
            }
            return "";
        }
        public decimal? ConvertStringToNullableDecimal(string text)
        {
            if (text=="-")
            {
                return null;
            }
            decimal convertedText = 0;
            decimal result;
            if (Decimal.TryParse(text, out result))
            {
                convertedText = result;
                return convertedText;
            }
            else
            {
                throw new Exception("מס' לא תקין");
            }
        }

        public int ConvertStringToInt(string text)
        {
            int convertedText = 0;
            int result;
            if (int.TryParse(text, out result))
            {
                convertedText = result;
                return convertedText;
            }
            else
            {
                throw new Exception("מס' לא תקין");
            }

        }

        //בודק ששדות החובה של פוליסה כללית יהיו ממולים
        public List<object> InputNullValidation(object[] inputsToValidate)
        {
            List<object> inputsErrorList = new List<object>();
            foreach (var inputToValidate in inputsToValidate)
            {
                if (inputToValidate is TextBox)
                {
                    if (((TextBox)inputToValidate).Text == "")
                    {
                        ((TextBox)inputToValidate).BorderBrush = System.Windows.Media.Brushes.Red;
                        inputsErrorList.Add(inputToValidate);
                    }
                }
                else if (inputToValidate is ComboBox)
                {
                    if (((ComboBox)inputToValidate).SelectedItem == null)
                    {
                        ((ComboBox)inputToValidate).BorderBrush = System.Windows.Media.Brushes.Red;
                        inputsErrorList.Add(inputToValidate);
                    }
                }
                else if (inputToValidate is DatePicker)
                {
                    if (((DatePicker)inputToValidate).SelectedDate == null)
                    {
                        ((DatePicker)inputToValidate).BorderBrush = System.Windows.Media.Brushes.Red;
                        inputsErrorList.Add(inputToValidate);
                    }
                }
            }
            return inputsErrorList;
        }
    }
}
