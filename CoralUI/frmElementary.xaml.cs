﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using CoralBusinessLogics;
using System.Drawing;
using System.IO;


namespace CoralUI
{
    /// <summary>
    /// Interaction logic for frmElementary.xaml
    /// </summary>
    public partial class frmElementary : Window
    {
        Client client = null;
        User handlerUser = null;
        InsurancesLogic insuranceLogic = new InsurancesLogic();
        IndustriesLogic industryLogic = new IndustriesLogic();
        AgentsLogic agentLogic = new AgentsLogic();
        PoliciesLogic policyLogic = new PoliciesLogic();
        CompaniesLogic companyLogic = new CompaniesLogic();
        AppraisersLogic appraiserLogic = new AppraisersLogic();
        StructureTypesLogic structureTypesLogic = new StructureTypesLogic();
        VehicleTypesLogic vehicleLogic = new VehicleTypesLogic();
        AllowedToDriveLogic allowedToDriveLogic = new AllowedToDriveLogic();
        DriversByNameLogic driversLogic = new DriversByNameLogic();
        ClientsLogic clientLogic = new ClientsLogic();
        CoveragesLogic coverageLogic = new CoveragesLogic();
        bool isOffer;
        List<TextBox> errorList = null;
        List<DeclaredDriver> declaredDrivers = new List<DeclaredDriver>();
        ElementaryPolicy elementaryPolicyToUpdate = null;
        Client policyClient = null;
        List<TrackingsViewModel> trackings = new List<TrackingsViewModel>();
        TrackingsLogics trackingLogic = new TrackingsLogics();
        InputsValidations validations = new InputsValidations();
        List<ElementaryCoverage> coveragesToAdd = new List<ElementaryCoverage>();
        public string path = null;
        public string newPath = null;
        bool isAdditon;
        DateTime now;
        bool isCarCopy;
        bool isRenewal;
        ApartmentRenewalQuote aptRenewalQuote;
        CarRenewalQuote carRenewalQuote;
        public bool renewalAccepted = false;
        ListViewSettings lv = new ListViewSettings();
        decimal? decimalAmount = 0;
        string clientDirPath = null;
        string elementaryIndustryPath = null;
        private bool isChanges = false;
        bool confirmBeforeClosing = true;
        bool cancelClose = false;
        StandardMoneyCollection standardMoneyCollection;
        StandardMoneyCollectionLogic moneyCollectionLogic = new StandardMoneyCollectionLogic();
        int policyId;
        private List<StandardMoneyCollectionTracking> standardMoneyCollectionTrackings;

        //בנאי להוספת פוליסה חדשה
        public frmElementary(Client clientToAddPolicy, User user, bool offer)
        {
            InitializeComponent();
            this.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            now = DateTime.Now;
            dpOpenDate.SelectedDate = now;
            dpStartDate.SelectedDate = now;
            client = clientToAddPolicy;
            handlerUser = user;
            txtAddition.IsEnabled = false;
            isOffer = offer;
        }

        //בנאי לפתיחה פוליסה על אותו רכב
        public frmElementary(Client clientToAddPolicy, User user, bool isCarPolicyCopy, ElementaryPolicy policy)
        {
            InitializeComponent();
            this.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            now = DateTime.Now;
            dpOpenDate.SelectedDate = now;
            dpStartDate.SelectedDate = now;
            client = clientToAddPolicy;
            handlerUser = user;
            txtAddition.IsEnabled = false;
            isCarCopy = isCarPolicyCopy;
            txtPolicyNumber.Text = policy.PolicyNumber;
            elementaryPolicyToUpdate = policy;
            isOffer = policy.IsOffer;
        }

        //בנאי לחידוש פוליסה 
        public frmElementary(Client clientToAddPolicy, User user, bool isPolicyRenewal, ElementaryPolicy oldPolicy, ApartmentRenewalQuote aptBusinessQuote, CarRenewalQuote carQuote)
        {
            InitializeComponent();
            this.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            now = DateTime.Now;
            dpOpenDate.SelectedDate = now;
            //dpStartDate.SelectedDate = now;
            client = clientToAddPolicy;
            handlerUser = user;
            txtAddition.IsEnabled = false;
            elementaryPolicyToUpdate = oldPolicy;
            isRenewal = isPolicyRenewal;
            if (aptBusinessQuote != null)
            {
                aptRenewalQuote = aptBusinessQuote;
            }
            else if (carQuote != null)
            {
                carRenewalQuote = carQuote;
            }
        }

        //בנאי לעדכון פוליסה קיימת
        public frmElementary(ElementaryPolicy policy, User user, bool additon, Client policyClient)
        {
            InitializeComponent();
            this.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            elementaryPolicyToUpdate = policy;
            handlerUser = user;
            isAdditon = additon;
            client = policyClient;
            txtAddition.IsEnabled = false;
        }

        //טעינת החלון
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            string strAppDir = System.AppDomain.CurrentDomain.BaseDirectory;
            string path = strAppDir + @"Coral Files\ClientsPath.txt";

            if (File.Exists(path))
            {
                clientDirPath = File.ReadAllText(path);
            }
            else
            {
                MessageBox.Show("לא נמצא תיקיית הלקוחות. נא בדוק בהגדרות ראשוניות את נתיב התיקייה");
                Close();
                return;
            }
            tbItemCar.Visibility = Visibility.Collapsed;
            tbItemApartment.Visibility = Visibility.Collapsed;
            tbItemBusiness.Visibility = Visibility.Collapsed;
            tbItemCoverage.Visibility = Visibility.Collapsed;

            cbCompanyBinding();
            cbVehicleTypeBinding();
            cbIndustryBinding();
            cbOldCompaniesBinding();
            cbStructureTypeBinding();
            cbAppraisersBinding();
            cbAllowedToDriveBinding();
            if (client != null)
            {
                string clientDetails;
                if (client.CellPhone != null && client.CellPhone != "")
                {
                    clientDetails = string.Format("{0} {1}  ת.ז: {2} נייד: {3}", client.FirstName, client.LastName, client.IdNumber, client.CellPhone);
                }
                else
                {
                    clientDetails = string.Format("{0} {1}  ת.ז: {2}", client.FirstName, client.LastName, client.IdNumber);
                }
                lblClientNameAndId.Content = clientDetails;
                txtAddition.Text = "0";
            }

            if (isRenewal)
            {
                if (aptRenewalQuote != null)
                {
                    txtPolicyNumber.Text = aptRenewalQuote.PolicyNumber;
                    CompanySelection((int)aptRenewalQuote.CompanyID);
                    if (aptRenewalQuote.Premium != null)
                    {
                        txtTotalPremium.Text = ((decimal)aptRenewalQuote.Premium).ToString("0.##");
                    }
                    InsuranceIndustrySelection(elementaryPolicyToUpdate.InsuranceIndustryID);
                }
                else if (carRenewalQuote != null)
                {
                    txtPolicyNumber.Text = carRenewalQuote.PolicyNumber;
                    CompanySelection((int)carRenewalQuote.CompanyID);
                    if (carRenewalQuote.Premium != null)
                    {
                        txtTotalPremium.Text = ((decimal)carRenewalQuote.Premium).ToString("0.##");
                    }
                    InsuranceIndustrySelection(carRenewalQuote.InsuranceIndustryID);
                }
                dpStartDate.SelectedDate = ((DateTime)elementaryPolicyToUpdate.EndDate).AddDays(1);
                InsuranceTypeSelection(elementaryPolicyToUpdate.ElementaryInsuranceTypeID);
                CurrencySelection();
                PaymentMethodSelection();
                txtPaymentesNumber.Text = elementaryPolicyToUpdate.PaymentsNumber.ToString();
                dpPaymentDate.SelectedDate = elementaryPolicyToUpdate.PaymentDate;
                coveragesToAdd = coverageLogic.GetAllCoveragesByPolicy(elementaryPolicyToUpdate.PolicyNumber, elementaryPolicyToUpdate.Addition);
                dgCoveragesBinding();
                if (elementaryPolicyToUpdate.IsMortgaged == true)
                {
                    MessageBox.Show("פוליסה משועבדת", "", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
                return;
            }
            if (elementaryPolicyToUpdate != null && isCarCopy != true)
            {
                chbisNew.IsChecked = elementaryPolicyToUpdate.isNew;
                policyClient = clientLogic.GetClientByClientID(elementaryPolicyToUpdate.ClientID);
                string clientDetails = string.Format("{0} {1}  ת.ז: {2} נייד: {3}", policyClient.FirstName, policyClient.LastName, policyClient.IdNumber, policyClient.CellPhone);
                lblClientNameAndId.Content = clientDetails;
                dpOpenDate.SelectedDate = elementaryPolicyToUpdate.OpenDate;
                txtPolicyNumber.Text = elementaryPolicyToUpdate.PolicyNumber;
                txtAddition.Text = elementaryPolicyToUpdate.Addition.ToString();
                dpStartDate.SelectedDate = elementaryPolicyToUpdate.StartDate;
                dpEndDate.SelectedDate = elementaryPolicyToUpdate.EndDate;
                isOffer = elementaryPolicyToUpdate.IsOffer;
                InsuranceIndustrySelection(elementaryPolicyToUpdate.InsuranceIndustryID);
                CompanySelection((int)elementaryPolicyToUpdate.CompanyID);
                InsuranceTypeSelection(elementaryPolicyToUpdate.ElementaryInsuranceTypeID);
                var principalAgentNumbers = cbPrincipalAgentNumber.Items;
                foreach (var item in principalAgentNumbers)
                {
                    AgentNumber principalAgentNumber = (AgentNumber)item;
                    if (principalAgentNumber.AgentNumberID == elementaryPolicyToUpdate.PrincipalAgentNumberID)
                    {
                        cbPrincipalAgentNumber.SelectedItem = item;
                        break;
                    }
                }
                var secundaryAgentNumbers = cbSecundaryAgentNumber.Items;
                foreach (var item in secundaryAgentNumbers)
                {
                    AgentNumber secundaryAgentNumber = (AgentNumber)item;
                    if (secundaryAgentNumber.AgentNumberID == elementaryPolicyToUpdate.SubAgentNumberID)
                    {
                        cbSecundaryAgentNumber.SelectedItem = item;
                        break;
                    }
                }
                CurrencySelection();
                PaymentMethodSelection();
                txtPaymentesNumber.Text = elementaryPolicyToUpdate.PaymentsNumber.ToString();
                dpPaymentDate.SelectedDate = elementaryPolicyToUpdate.PaymentDate;
                if (elementaryPolicyToUpdate.PremiumNeto1 != null)
                {
                    txtPremiumNeto1.Text = ConvertDecimalToString(elementaryPolicyToUpdate.PremiumNeto1);
                }
                if (elementaryPolicyToUpdate.PremiumNeto2 != null)
                {
                    txtPremiumNeto2.Text = ConvertDecimalToString(elementaryPolicyToUpdate.PremiumNeto2);
                }
                if (elementaryPolicyToUpdate.PremiumNeto3 != null)
                {
                    txtPremiumNeto3.Text = ConvertDecimalToString(elementaryPolicyToUpdate.PremiumNeto3);
                }
                if (elementaryPolicyToUpdate.PremiumNeto4 != null)
                {
                    txtPremiumNeto4.Text = ConvertDecimalToString(elementaryPolicyToUpdate.PremiumNeto4);
                }
                if (elementaryPolicyToUpdate.InscriptionFees != null)
                {
                    txtInscriptionFees.Text = ConvertDecimalToString(elementaryPolicyToUpdate.InscriptionFees);
                }
                if (elementaryPolicyToUpdate.PolicyFees != null)
                {
                    txtPolicyFees.Text = ConvertDecimalToString(elementaryPolicyToUpdate.PolicyFees);
                }
                if (elementaryPolicyToUpdate.ProjectionFees != null)
                {
                    txtProjectionFees.Text = ConvertDecimalToString(elementaryPolicyToUpdate.ProjectionFees);
                }
                if (elementaryPolicyToUpdate.StampsFees != null)
                {
                    txtStampFees.Text = ConvertDecimalToString(elementaryPolicyToUpdate.StampsFees);
                }
                if (elementaryPolicyToUpdate.HandlingFees != null)
                {
                    txtHandlingFees.Text = ConvertDecimalToString(elementaryPolicyToUpdate.HandlingFees);
                }
                if (elementaryPolicyToUpdate.TotalFees != null)
                {
                    txtTotalFees.Text = ConvertDecimalToString(elementaryPolicyToUpdate.TotalFees);
                }
                if (elementaryPolicyToUpdate.Credit != null)
                {
                    txtTotalCredit.Text = ConvertDecimalToString(elementaryPolicyToUpdate.Credit);
                }
                if (elementaryPolicyToUpdate.TotalPremium != null)
                {
                    txtTotalPremium.Text = ConvertDecimalToString(elementaryPolicyToUpdate.TotalPremium);
                }
                chbIsMortgaged.IsChecked = elementaryPolicyToUpdate.IsMortgaged;
                if (elementaryPolicyToUpdate.IsMortgaged == true)
                {
                    txtBank.Text = elementaryPolicyToUpdate.BankName;
                    txtBranch.Text = elementaryPolicyToUpdate.BankBranchNumber;
                    txtAddress.Text = elementaryPolicyToUpdate.BankAddress;
                    txtPhone.Text = elementaryPolicyToUpdate.BankPhone1;
                    txtFax.Text = elementaryPolicyToUpdate.BankFax;
                    txtContactName.Text = elementaryPolicyToUpdate.BankContactName;
                    txtEmail.Text = elementaryPolicyToUpdate.BankEmail;
                }
                txtComments.Text = elementaryPolicyToUpdate.Comments;
                List<ElementaryTracking> policyTrackings = trackingLogic.GetAllElementaryTrackingsByPolicy(elementaryPolicyToUpdate.PolicyNumber, elementaryPolicyToUpdate.Addition);
                foreach (ElementaryTracking tracking in policyTrackings)
                {
                    TrackingsViewModel trackingToAdd = new TrackingsViewModel() { TrackingContent = tracking.TrackingContent, TrackingDate = tracking.Date, User = tracking.User };
                    trackings.Add(trackingToAdd);
                }
                dgTrackingsBinding();
                coveragesToAdd = coverageLogic.GetAllCoveragesByPolicy(elementaryPolicyToUpdate.PolicyNumber, elementaryPolicyToUpdate.Addition);
                dgCoveragesBinding();

                if (isAdditon)
                {
                    txtPolicyNumber.IsEnabled = false;
                    dpEndDate.IsEnabled = false;
                    //int? addition=elementaryPolicyToUpdate.Addition+1;
                    txtAddition.Text = policyLogic.GetAdditionNumber(elementaryPolicyToUpdate.PolicyNumber, elementaryPolicyToUpdate.InsuranceIndustry);
                    now = DateTime.Now;
                    dpOpenDate.SelectedDate = now;
                    dpStartDate.SelectedDate = now;
                    dpEndDate.SelectedDate = elementaryPolicyToUpdate.EndDate;
                    cbCompany.IsEnabled = false;
                }
                else
                {
                    txtAddition.IsEnabled = false;
                    if (int.Parse(policyLogic.GetAdditionNumber(elementaryPolicyToUpdate.PolicyNumber, elementaryPolicyToUpdate.InsuranceIndustry)) - 1 > elementaryPolicyToUpdate.Addition || elementaryPolicyToUpdate.EndDate < DateTime.Now)
                    {
                        spApt.IsEnabled = false;
                        spBusiness.IsEnabled = false;
                        spCar.IsEnabled = false;
                        spGeneral.IsEnabled = false;
                        dpCoverages.IsEnabled = false;
                        dpTrackings.IsEnabled = false;
                        btnSave.IsEnabled = false;
                    }
                    if (elementaryPolicyToUpdate.Addition != 0)
                    {
                        dpEndDate.IsEnabled = false;
                        txtPolicyNumber.IsEnabled = false;
                    }
                    standardMoneyCollection = moneyCollectionLogic.GetStandardMoneyColletionByPolicy(elementaryPolicyToUpdate);
                    if (standardMoneyCollection != null)
                    {
                        chbStandardMoneyCollection.IsChecked = true;
                        chbStandardMoneyCollection.IsEnabled = false;
                        standardMoneyCollectionTrackings = moneyCollectionLogic.GetTrakcingsByStsndardMoneyCollection(standardMoneyCollection);
                    }
                }
            }
        }

        private void PaymentMethodSelection()
        {
            var paymentMethods = cbPaymentMode.Items;
            foreach (var item in paymentMethods)
            {
                if (elementaryPolicyToUpdate.PaymentMethod == ((ComboBoxItem)item).Content.ToString())
                {
                    cbPaymentMode.SelectedItem = item;
                    break;
                }
            }
        }

        private void CurrencySelection()
        {
            var currencies = cbCurrency.Items;
            foreach (var item in currencies)
            {
                if (elementaryPolicyToUpdate.Currency == ((ComboBoxItem)item).Content.ToString())
                {
                    cbCurrency.SelectedItem = item;
                    break;
                }
            }
        }

        private void InsuranceTypeSelection(int id)
        {
            var insuranceTypes = cbInsuranceType.Items;
            foreach (var item in insuranceTypes)
            {
                ElementaryInsuranceType insuranceType = (ElementaryInsuranceType)item;
                if (insuranceType.ElementaryInsuranceTypeID == id)
                {
                    cbInsuranceType.SelectedItem = item;
                    break;
                }
            }

        }

        private void CompanySelection(int companyID)
        {
            var companies = cbCompany.Items;
            foreach (var item in companies)
            {
                InsuranceCompany company = (InsuranceCompany)item;
                if (company.CompanyID == companyID)
                {
                    cbCompany.SelectedItem = item;
                    break;
                }
            }
        }

        private void InsuranceIndustrySelection(int industryID)
        {
            var industriesItems = cbIndustry.Items;
            foreach (var item in industriesItems)
            {
                InsuranceIndustry industry = (InsuranceIndustry)item;
                if (industry.InsuranceIndustryID == industryID)
                {
                    cbIndustry.SelectedItem = item;
                    if (industry.InsuranceIndustryName != "שגויים")
                    {
                        cbIndustry.IsEnabled = false;
                    }
                    break;
                }
            }
        }

        //פונקציה שממירה מספר עשרוני לטקסט
        private string ConvertDecimalToString(decimal? number)
        {
            if (number == null)
            {
                return null;
            }
            return ((decimal)number).ToString("G29");
        }


        //מסנכרן את הקומבו בוקסים של חברות ביטוח שלעבר של לקוח 
        private void cbOldCompaniesBinding()
        {
            cbAptThisYearCompany.ItemsSource = companyLogic.GetAllCompanies();
            cbAptThisYearCompany.DisplayMemberPath = "CompanyName";
            cbAptLastYearCompany.ItemsSource = companyLogic.GetAllCompanies();
            cbAptLastYearCompany.DisplayMemberPath = "CompanyName";
            cbApt2YearsAgoCompany.ItemsSource = companyLogic.GetAllCompanies();
            cbApt2YearsAgoCompany.DisplayMemberPath = "CompanyName";
            cbApt3YearsAgoCompany.ItemsSource = companyLogic.GetAllCompanies();
            cbApt3YearsAgoCompany.DisplayMemberPath = "CompanyName";
            cbThisYearCompany.ItemsSource = companyLogic.GetAllCompanies();
            cbThisYearCompany.DisplayMemberPath = "CompanyName";
            cbLastYearCompany.ItemsSource = companyLogic.GetAllCompanies();
            cbLastYearCompany.DisplayMemberPath = "CompanyName";
            cb2YearsAgoCompany.ItemsSource = companyLogic.GetAllCompanies();
            cb2YearsAgoCompany.DisplayMemberPath = "CompanyName";
            cb3YearsAgoCompany.ItemsSource = companyLogic.GetAllCompanies();
            cb3YearsAgoCompany.DisplayMemberPath = "CompanyName";
            cbBusinessThisYearCompany.ItemsSource = companyLogic.GetAllCompanies();
            cbBusinessThisYearCompany.DisplayMemberPath = "CompanyName";
            cbBusinessLastYearCompany.ItemsSource = companyLogic.GetAllCompanies();
            cbBusinessLastYearCompany.DisplayMemberPath = "CompanyName";
            cbBusiness2YearsAgoCompany.ItemsSource = companyLogic.GetAllCompanies();
            cbBusiness2YearsAgoCompany.DisplayMemberPath = "CompanyName";
            cbBusiness3YearsAgoCompany.ItemsSource = companyLogic.GetAllCompanies();
            cbBusiness3YearsAgoCompany.DisplayMemberPath = "CompanyName";
        }

        //מסנכרן את קומבו בוקס של ענף עם בסיס הנתונים
        private void cbIndustryBinding()
        {
            var industries = industryLogic.GetAllIndustries();
            if (!isCarCopy)
            {
                cbIndustry.ItemsSource = industries;
                cbIndustry.DisplayMemberPath = "InsuranceIndustryName";

            }
            else
            {
                cbIndustry.ItemsSource = industries.Where(i => i.InsuranceIndustryName.Contains("רכב"));
                cbIndustry.DisplayMemberPath = "InsuranceIndustryName";
                tbItemCar.Visibility = Visibility.Visible;

                txtRegistrationNumber.Text = elementaryPolicyToUpdate.CarPolicy.RegistrationNumber;
                txtModelCode.Text = elementaryPolicyToUpdate.CarPolicy.ModelCode;
                txtYear.Text = elementaryPolicyToUpdate.CarPolicy.Year.ToString();
                txtEngine.Text = elementaryPolicyToUpdate.CarPolicy.Engine;
                txtManufacturer.Text = elementaryPolicyToUpdate.CarPolicy.Manufacturer;
                txtChassis.Text = elementaryPolicyToUpdate.CarPolicy.ChassisNumber;
                txtWeight.Text = ConvertDecimalToString(elementaryPolicyToUpdate.CarPolicy.Weight);
                txtPlacesNumber.Text = elementaryPolicyToUpdate.CarPolicy.PlacesNumber.ToString();
                if (elementaryPolicyToUpdate.CarPolicy.VehicleTypeID != null)
                {
                    SelectItemInCb((int)elementaryPolicyToUpdate.CarPolicy.VehicleTypeID);
                }
                txtHobaCertificateNumber.Text = elementaryPolicyToUpdate.CarPolicy.ChovaCertificateNumber;
                txtProtectionCode.Text = elementaryPolicyToUpdate.CarPolicy.ProtectionCode;
                lblInsuranceThisYear.IsChecked = elementaryPolicyToUpdate.CarPolicy.InsuranceThisYear;
                lblInsuranceLastYear.IsChecked = elementaryPolicyToUpdate.CarPolicy.InsuranceLastYear;
                lblInsurance2YearsAgo.IsChecked = elementaryPolicyToUpdate.CarPolicy.Insurance2YearsAgo;
                lblInsurance3YearsAgo.IsChecked = elementaryPolicyToUpdate.CarPolicy.Insurance3YearsAgo;
                chbDrivesInShabbat.IsChecked = elementaryPolicyToUpdate.CarPolicy.DrivesInShabbat;
                txtClaimsThisYear.Text = elementaryPolicyToUpdate.CarPolicy.ClaimsNumberThisYear.ToString();
                txtClaimsLastYear.Text = elementaryPolicyToUpdate.CarPolicy.ClaimsNumberLastYear.ToString();
                txtClaims2YearsAgo.Text = elementaryPolicyToUpdate.CarPolicy.ClaimsNumber2YearsAgo.ToString();
                txtClaims3YearsAgo.Text = elementaryPolicyToUpdate.CarPolicy.ClaimsNumber3YearsAgo.ToString();
                cbThisYearCompany.SelectedItem = GetSelectedCompany(cbThisYearCompany, elementaryPolicyToUpdate.CarPolicy.InsuranceCompanyThisYear);
                if (cbThisYearCompany.SelectedItem == null)
                {
                    cbThisYearCompany.Text = elementaryPolicyToUpdate.CarPolicy.InsuranceCompanyThisYear;
                }
                cbLastYearCompany.SelectedItem = GetSelectedCompany(cbLastYearCompany, elementaryPolicyToUpdate.CarPolicy.InsuranceCompanyLastYear);
                if (cbLastYearCompany.SelectedItem == null)
                {
                    cbLastYearCompany.Text = elementaryPolicyToUpdate.CarPolicy.InsuranceCompanyLastYear;
                }
                cb2YearsAgoCompany.SelectedItem = GetSelectedCompany(cb2YearsAgoCompany, elementaryPolicyToUpdate.CarPolicy.InsuranceCompany2YearsAgo);
                if (cb2YearsAgoCompany.SelectedItem == null)
                {
                    cb2YearsAgoCompany.Text = elementaryPolicyToUpdate.CarPolicy.InsuranceCompany2YearsAgo;
                }
                cb3YearsAgoCompany.SelectedItem = GetSelectedCompany(cb3YearsAgoCompany, elementaryPolicyToUpdate.CarPolicy.InsuranceCompany3YearsAgo);
                if (cb3YearsAgoCompany.SelectedItem == null)
                {
                    cb3YearsAgoCompany.Text = elementaryPolicyToUpdate.CarPolicy.InsuranceCompany3YearsAgo;
                }
                //var allowedDrivers = cbAllowedToDrive.Items;
                //foreach (var item in allowedDrivers)
                //{
                //    AllowedToDrive driver = (AllowedToDrive)item;
                //    if (driver.AllowedToDriveID == elementaryPolicyToUpdate.CarPolicy.AllowedToDriveID)
                //    {
                //        cbAllowedToDrive.SelectedItem = item;
                //        break;
                //    }
                //}
                if (elementaryPolicyToUpdate.CarPolicy.AllowedToDriveID != null)
                {
                    SelectDriverItemInCb((int)elementaryPolicyToUpdate.CarPolicy.AllowedToDriveID);
                }
                txtYoungestDriverAge.Text = elementaryPolicyToUpdate.CarPolicy.YoungestDriverAge.ToString();
                txtYoungestDriverSeniority.Text = elementaryPolicyToUpdate.CarPolicy.YoungestDriverSeniority.ToString();
                txtOtherDriversAge.Text = elementaryPolicyToUpdate.CarPolicy.OtherDriversAge.ToString();
                txtOtherDriversSeniority.Text = elementaryPolicyToUpdate.CarPolicy.OtherDriversSeniority.ToString();
                declaredDrivers = driversLogic.GetAllDeclareDriversByPolicy(elementaryPolicyToUpdate.PolicyNumber, elementaryPolicyToUpdate.Addition);
                dgDriversByName.ItemsSource = declaredDrivers.OrderBy(d => d.DriverLastName).ThenBy(d => d.DriverFirstName);

            }
            cbIndustry.DisplayMemberPath = "InsuranceIndustryName";
        }

        //מסנכרן את קומבו בוקס של חברת ביטוח עם בסיס הנתונים
        private void cbCompanyBinding()
        {
            cbCompany.ItemsSource = insuranceLogic.GetActiveCompaniesByInsurance("אלמנטרי");
            cbCompany.DisplayMemberPath = "Company.CompanyName";
        }

        //מסנכרן את קומבו בוקס של סוגי רכבים עם בסיס הנתונים
        private void cbVehicleTypeBinding()
        {
            cbCarType.ItemsSource = vehicleLogic.GetAllActiveVehicleTypes();
            cbCarType.DisplayMemberPath = "Description";
        }

        //מסנכרן את קומבו בוקס של רשאים לנהוג עם בסיס הנתונים
        private void cbAllowedToDriveBinding()
        {
            cbAllowedToDrive.ItemsSource = allowedToDriveLogic.GetActiveAllowedToDrive();
            cbAllowedToDrive.DisplayMemberPath = "Description";
        }

        //כשמסומן שהפוליסה משועבדת, פותח לכתיבה את פרטי הבנק
        private void chbIsMortgaged_Checked(object sender, RoutedEventArgs e)
        {
            spBankDetails.IsEnabled = true;
        }

        //כשלא מסומן שהפוליסה משועבדת, סוגר לכתיבה את פרטי הבנק
        private void chbIsMortgaged_Unchecked(object sender, RoutedEventArgs e)
        {
            spBankDetails.IsEnabled = false;
            txtBank.Clear();
            txtBranch.Clear();
            txtAddress.Clear();
            txtPhone.Clear();
            txtFax.Clear();
            txtContactName.Clear();
            txtEmail.Clear();
        }

        //הפונקציות הבאות פותחות/סוגרות לכתיבה את פרטי הביטוחים הקודמים
        private void lblInsuranceThisYear_Checked(object sender, RoutedEventArgs e)
        {
            txtClaimsThisYear.IsEnabled = true;
            cbThisYearCompany.IsEnabled = true;
        }

        private void lblInsuranceThisYear_Unchecked(object sender, RoutedEventArgs e)
        {
            txtClaimsThisYear.IsEnabled = false;
            cbThisYearCompany.IsEnabled = false;
            cbThisYearCompany.SelectedItem = null;
        }

        private void lblInsuranceLastYear_Checked(object sender, RoutedEventArgs e)
        {
            txtClaimsLastYear.IsEnabled = true;
            cbLastYearCompany.IsEnabled = true;
        }

        private void lblInsuranceLastYear_Unchecked(object sender, RoutedEventArgs e)
        {
            txtClaimsLastYear.IsEnabled = false;
            cbLastYearCompany.IsEnabled = false;
            cbLastYearCompany.SelectedItem = null;

        }

        private void lblInsurance2YearsAgo_Checked(object sender, RoutedEventArgs e)
        {
            txtClaims2YearsAgo.IsEnabled = true;
            cb2YearsAgoCompany.IsEnabled = true;
        }

        private void lblInsurance2YearsAgo_Unchecked(object sender, RoutedEventArgs e)
        {
            txtClaims2YearsAgo.IsEnabled = false;
            cb2YearsAgoCompany.IsEnabled = false;
            cb2YearsAgoCompany.SelectedItem = null;

        }

        private void lblInsurance3YearsAgo_Checked(object sender, RoutedEventArgs e)
        {
            txtClaims3YearsAgo.IsEnabled = true;
            cb3YearsAgoCompany.IsEnabled = true;
        }

        private void lblInsurance3YearsAgo_Unchecked(object sender, RoutedEventArgs e)
        {
            txtClaims3YearsAgo.IsEnabled = false;
            cb3YearsAgoCompany.IsEnabled = false;
            cb3YearsAgoCompany.SelectedItem = null;

        }

        private void lblAptInsuranceThisYear_Checked(object sender, RoutedEventArgs e)
        {
            txtAptClaimsThisYear.IsEnabled = true;
            cbAptThisYearCompany.IsEnabled = true;
        }

        private void lblAptInsuranceThisYear_Unchecked(object sender, RoutedEventArgs e)
        {
            txtAptClaimsThisYear.IsEnabled = false;
            cbAptThisYearCompany.IsEnabled = false;
            cbAptThisYearCompany.SelectedItem = null;
        }

        private void lblAptInsuranceLastYear_Checked(object sender, RoutedEventArgs e)
        {
            txtAptClaimsLastYear.IsEnabled = true;
            cbAptLastYearCompany.IsEnabled = true;
        }

        private void lblAptInsuranceLastYear_Unchecked(object sender, RoutedEventArgs e)
        {
            txtAptClaimsLastYear.IsEnabled = false;
            cbAptLastYearCompany.IsEnabled = false;
            cbAptLastYearCompany.SelectedItem = null;

        }

        private void lblAptInsurance2YearsAgo_Checked(object sender, RoutedEventArgs e)
        {
            txtAptClaims2YearsAgo.IsEnabled = true;
            cbApt2YearsAgoCompany.IsEnabled = true;
        }

        private void lblAptInsurance2YearsAgo_Unchecked(object sender, RoutedEventArgs e)
        {
            txtAptClaims2YearsAgo.IsEnabled = false;
            cbApt2YearsAgoCompany.IsEnabled = false;
            cbApt2YearsAgoCompany.SelectedItem = null;

        }

        private void lblAptInsurance3YearsAgo_Checked(object sender, RoutedEventArgs e)
        {
            txtAptClaims3YearsAgo.IsEnabled = true;
            cbApt3YearsAgoCompany.IsEnabled = true;
        }

        private void lblAptInsurance3YearsAgo_Unchecked(object sender, RoutedEventArgs e)
        {
            txtAptClaims3YearsAgo.IsEnabled = false;
            cbApt3YearsAgoCompany.IsEnabled = false;
            cbApt3YearsAgoCompany.SelectedItem = null;

        }

        private void lblBusinessInsuranceThisYear_Checked(object sender, RoutedEventArgs e)
        {
            txtBusinessClaimsThisYear.IsEnabled = true;
            cbBusinessThisYearCompany.IsEnabled = true;
        }

        private void lblBusinessInsuranceThisYear_Unchecked(object sender, RoutedEventArgs e)
        {
            txtBusinessClaimsThisYear.IsEnabled = false;
            cbBusinessThisYearCompany.IsEnabled = false;
            cbBusinessThisYearCompany.SelectedItem = null;

        }

        private void lblBusinessInsuranceLastYear_Checked(object sender, RoutedEventArgs e)
        {
            txtBusinessClaimsLastYear.IsEnabled = true;
            cbBusinessLastYearCompany.IsEnabled = true;
        }

        private void lblBusinessInsuranceLastYear_Unchecked(object sender, RoutedEventArgs e)
        {
            txtBusinessClaimsLastYear.IsEnabled = false;
            cbBusinessLastYearCompany.IsEnabled = false;
            cbBusinessLastYearCompany.SelectedItem = null;

        }

        private void lblBusinessInsurance2YearsAgo_Checked(object sender, RoutedEventArgs e)
        {
            txtBusinessClaims2YearsAgo.IsEnabled = true;
            cbBusiness2YearsAgoCompany.IsEnabled = true;
        }

        private void lblBusinessInsurance2YearsAgo_Unchecked(object sender, RoutedEventArgs e)
        {
            txtBusinessClaims2YearsAgo.IsEnabled = false;
            cbBusiness2YearsAgoCompany.IsEnabled = false;
            cbBusiness2YearsAgoCompany.SelectedItem = null;

        }

        private void lblBusinessInsurance3YearsAgo_Checked(object sender, RoutedEventArgs e)
        {
            txtBusinessClaims3YearsAgo.IsEnabled = true;
            cbBusiness3YearsAgoCompany.IsEnabled = true;
        }

        private void lblBusinessInsurance3YearsAgo_Unchecked(object sender, RoutedEventArgs e)
        {
            txtBusinessClaims3YearsAgo.IsEnabled = false;
            cbBusiness3YearsAgoCompany.IsEnabled = false;
            cbBusiness3YearsAgoCompany.SelectedItem = null;

        }

        //פתיחת חלון עדכון טבלת חברות ביטוח
        private void btnUpdateCompanyTable_Click(object sender, RoutedEventArgs e)
        {
            int? selectedItemId = null;
            if (cbCompany.SelectedItem != null)
            {
                selectedItemId = ((InsuranceCompany)cbCompany.SelectedItem).CompanyID;
            }
            int insuranceID = insuranceLogic.GetInsuranceID("אלמנטרי");
            InsuranceCompany company = new InsuranceCompany() { InsuranceID = insuranceID };
            frmUpdateTable updateCompaniesTable = new frmUpdateTable(company);
            updateCompaniesTable.ShowDialog();
            cbCompanyBinding();
            if (updateCompaniesTable.ItemAdded != null)
            {
                CompanySelection(((InsuranceCompany)updateCompaniesTable.ItemAdded).CompanyID);
            }
            else if (selectedItemId != null)
            {
                CompanySelection((int)selectedItemId);
            }
        }


        //פונקציה המחשבת תאריך סיום ברירת מחדל לשנה אחרי תאריך ההתחלה לפי השינוי בתאריך ההתחלה
        private void dpStartDate_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            if (dpStartDate.SelectedDate != null)
            {
                if (elementaryPolicyToUpdate == null || (elementaryPolicyToUpdate.Addition == 0 && isAdditon == false) || isRenewal)
                {
                    DateTime selectedDate = ((DateTime)dpStartDate.SelectedDate).Date;
                    if (selectedDate.Day <= 15)
                    {
                        dpEndDate.SelectedDate = selectedDate.AddYears(1).AddDays(-selectedDate.Day);
                    }
                    else
                    {
                        //dpEndDate.SelectedDate = selectedDate.AddYears(1).AddMonths(1).AddDays(-selectedDate.Day);
                        dpEndDate.SelectedDate = new DateTime(selectedDate.AddYears(1).Year, selectedDate.Month, DateTime.DaysInMonth(selectedDate.AddYears(1).Year, selectedDate.Month));
                        //DateTime.DaysInMonth(year,month)
                    }
                }
            }
        }

        //פותח/סוגר לשוניות רלוונטיות על פי שינוים בבחירת הענף
        //אם מדובר על עדכון פוליסה- גם ממלא את השדות בלשוניות הרלוונטיות
        private void cbIndustry_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            InsuranceIndustry industrySelected = (InsuranceIndustry)cbIndustry.SelectedItem;
            if (industrySelected == null)
            {
                return;
            }
            cbInsuranceType.IsEnabled = true;
            btnUpdateInsuranceTypeTable.IsEnabled = true;
            cbInsuranceTypeBinding();
            if (industrySelected.InsuranceIndustryName.Contains("רכב"))

            //if (industrySelected.InsuranceIndustryName == "רכב" || industrySelected.InsuranceIndustryName == "רכב חובה")
            {
                tbItemCar.Visibility = Visibility.Visible;
                tbItemApartment.Visibility = Visibility.Collapsed;
                tbItemBusiness.Visibility = Visibility.Collapsed;

                //מילואי שדות פוליסת רכב בעדבון
                if (elementaryPolicyToUpdate != null && !isCarCopy && elementaryPolicyToUpdate.InsuranceIndustry.InsuranceIndustryName != "שגויים")
                {
                    txtRegistrationNumber.Text = elementaryPolicyToUpdate.CarPolicy.RegistrationNumber;
                    txtModelCode.Text = elementaryPolicyToUpdate.CarPolicy.ModelCode;
                    txtYear.Text = elementaryPolicyToUpdate.CarPolicy.Year.ToString();
                    txtEngine.Text = elementaryPolicyToUpdate.CarPolicy.Engine;
                    txtManufacturer.Text = elementaryPolicyToUpdate.CarPolicy.Manufacturer;
                    txtChassis.Text = elementaryPolicyToUpdate.CarPolicy.ChassisNumber;
                    txtWeight.Text = ConvertDecimalToString(elementaryPolicyToUpdate.CarPolicy.Weight);
                    txtPlacesNumber.Text = elementaryPolicyToUpdate.CarPolicy.PlacesNumber.ToString();
                    if (elementaryPolicyToUpdate.CarPolicy.VehicleTypeID != null)
                    {
                        SelectItemInCb((int)elementaryPolicyToUpdate.CarPolicy.VehicleTypeID);
                    }
                    txtHobaCertificateNumber.Text = elementaryPolicyToUpdate.CarPolicy.ChovaCertificateNumber;
                    txtProtectionCode.Text = elementaryPolicyToUpdate.CarPolicy.ProtectionCode;
                    lblInsuranceThisYear.IsChecked = elementaryPolicyToUpdate.CarPolicy.InsuranceThisYear;
                    lblInsuranceLastYear.IsChecked = elementaryPolicyToUpdate.CarPolicy.InsuranceLastYear;
                    lblInsurance2YearsAgo.IsChecked = elementaryPolicyToUpdate.CarPolicy.Insurance2YearsAgo;
                    lblInsurance3YearsAgo.IsChecked = elementaryPolicyToUpdate.CarPolicy.Insurance3YearsAgo;
                    chbDrivesInShabbat.IsChecked = elementaryPolicyToUpdate.CarPolicy.DrivesInShabbat;
                    txtClaimsThisYear.Text = elementaryPolicyToUpdate.CarPolicy.ClaimsNumberThisYear.ToString();
                    txtClaimsLastYear.Text = elementaryPolicyToUpdate.CarPolicy.ClaimsNumberLastYear.ToString();
                    txtClaims2YearsAgo.Text = elementaryPolicyToUpdate.CarPolicy.ClaimsNumber2YearsAgo.ToString();
                    txtClaims3YearsAgo.Text = elementaryPolicyToUpdate.CarPolicy.ClaimsNumber3YearsAgo.ToString();
                    cbThisYearCompany.SelectedItem = GetSelectedCompany(cbThisYearCompany, elementaryPolicyToUpdate.CarPolicy.InsuranceCompanyThisYear);
                    if (cbThisYearCompany.SelectedItem == null)
                    {
                        cbThisYearCompany.Text = elementaryPolicyToUpdate.CarPolicy.InsuranceCompanyThisYear;
                    }
                    cbLastYearCompany.SelectedItem = GetSelectedCompany(cbLastYearCompany, elementaryPolicyToUpdate.CarPolicy.InsuranceCompanyLastYear);
                    if (cbLastYearCompany.SelectedItem == null)
                    {
                        cbLastYearCompany.Text = elementaryPolicyToUpdate.CarPolicy.InsuranceCompanyLastYear;
                    }
                    cb2YearsAgoCompany.SelectedItem = GetSelectedCompany(cb2YearsAgoCompany, elementaryPolicyToUpdate.CarPolicy.InsuranceCompany2YearsAgo);
                    if (cb2YearsAgoCompany.SelectedItem == null)
                    {
                        cb2YearsAgoCompany.Text = elementaryPolicyToUpdate.CarPolicy.InsuranceCompany2YearsAgo;
                    }
                    cb3YearsAgoCompany.SelectedItem = GetSelectedCompany(cb3YearsAgoCompany, elementaryPolicyToUpdate.CarPolicy.InsuranceCompany3YearsAgo);
                    if (cb3YearsAgoCompany.SelectedItem == null)
                    {
                        cb3YearsAgoCompany.Text = elementaryPolicyToUpdate.CarPolicy.InsuranceCompany3YearsAgo;
                    }
                    //var allowedDrivers = cbAllowedToDrive.Items;
                    //foreach (var item in allowedDrivers)
                    //{
                    //    AllowedToDrive driver = (AllowedToDrive)item;
                    //    if (driver.AllowedToDriveID == elementaryPolicyToUpdate.CarPolicy.AllowedToDriveID)
                    //    {
                    //        cbAllowedToDrive.SelectedItem = item;
                    //        break;
                    //    }
                    //}
                    if (elementaryPolicyToUpdate.CarPolicy.AllowedToDriveID != null)
                    {
                        SelectDriverItemInCb((int)elementaryPolicyToUpdate.CarPolicy.AllowedToDriveID);
                    }
                    txtYoungestDriverAge.Text = elementaryPolicyToUpdate.CarPolicy.YoungestDriverAge.ToString();
                    txtYoungestDriverSeniority.Text = elementaryPolicyToUpdate.CarPolicy.YoungestDriverSeniority.ToString();
                    txtOtherDriversAge.Text = elementaryPolicyToUpdate.CarPolicy.OtherDriversAge.ToString();
                    txtOtherDriversSeniority.Text = elementaryPolicyToUpdate.CarPolicy.OtherDriversSeniority.ToString();
                    declaredDrivers = driversLogic.GetAllDeclareDriversByPolicy(elementaryPolicyToUpdate.PolicyNumber, elementaryPolicyToUpdate.Addition);
                    dgDriversByName.ItemsSource = declaredDrivers.OrderBy(d => d.DriverLastName).ThenBy(d => d.DriverFirstName);
                }
            }
            //מילואי שדות פוליסת עסק בעדכון
            else if (industrySelected.InsuranceIndustryName == "עסק")
            {
                tbItemBusiness.Visibility = Visibility.Visible;
                tbItemCar.Visibility = Visibility.Collapsed;
                tbItemApartment.Visibility = Visibility.Collapsed;

                if (elementaryPolicyToUpdate != null && elementaryPolicyToUpdate.InsuranceIndustry.InsuranceIndustryName != "שגויים")
                {
                    txtBusinessCity.Text = elementaryPolicyToUpdate.BusinessPolicy.City;
                    txtBusinessStreet.Text = elementaryPolicyToUpdate.BusinessPolicy.Street;
                    txtBusinessHouseNumber.Text = elementaryPolicyToUpdate.BusinessPolicy.HomeNumber;
                    txtBusinessAptNumber.Text = elementaryPolicyToUpdate.BusinessPolicy.ApartmentNumber;
                    txtBusinessZipCode.Text = elementaryPolicyToUpdate.BusinessPolicy.ZipCode;
                    txtBusinessOwner.Text = elementaryPolicyToUpdate.BusinessPolicy.BusinessOwner;
                    txtBusinessName.Text = elementaryPolicyToUpdate.BusinessPolicy.BusinessName;
                    txtBusinessPhone.Text = elementaryPolicyToUpdate.BusinessPolicy.BusinessPhone;
                    txtBusinessSecondPhone.Text = elementaryPolicyToUpdate.BusinessPolicy.BusinessPhone2;
                    txtBusinessFax.Text = elementaryPolicyToUpdate.BusinessPolicy.BusinessFax;
                    txtBusinessEmail.Text = elementaryPolicyToUpdate.BusinessPolicy.BusinessEmail;
                    txtBusinessDescription.Text = elementaryPolicyToUpdate.BusinessPolicy.BusinessDescription;
                    //var structureTypes = cbBusinessStructureType.Items;
                    //foreach (var item in structureTypes)
                    //{
                    //    StructureType structure = (StructureType)item;
                    //    if (structure.StructureTypeID == elementaryPolicyToUpdate.BusinessPolicy.StructureTypeID)
                    //    {
                    //        cbBusinessStructureType.SelectedItem = item;
                    //        break;
                    //    }
                    //}
                    if (elementaryPolicyToUpdate.BusinessPolicy.StructureTypeID != null)
                    {
                        SelectStructureItemInCb((int)elementaryPolicyToUpdate.BusinessPolicy.StructureTypeID, cbBusinessStructureType);
                    }
                    txtBusinessFloor.Text = elementaryPolicyToUpdate.BusinessPolicy.Floor;
                    txtBusinessTotalFloors.Text = elementaryPolicyToUpdate.BusinessPolicy.TotalBuildingFloorsNumber;
                    txtBusinessSize.Text = ConvertDecimalToString(elementaryPolicyToUpdate.BusinessPolicy.SizeInMeters);
                    txtBusinessRoomsNumber.Text = elementaryPolicyToUpdate.BusinessPolicy.RoomsNumber;
                    txtBusinessContactLastName.Text = elementaryPolicyToUpdate.BusinessPolicy.ContactLastName;
                    txtBusinessContactFirstName.Text = elementaryPolicyToUpdate.BusinessPolicy.ContactName;
                    txtBusinessContactPhone.Text = elementaryPolicyToUpdate.BusinessPolicy.ContactPhone;
                    txtBusinessContactCellPhone.Text = elementaryPolicyToUpdate.BusinessPolicy.ContactCellPhone;
                    txtBusinessContactFax.Text = elementaryPolicyToUpdate.BusinessPolicy.ContactFax;
                    txtBusinessContactEmail.Text = elementaryPolicyToUpdate.BusinessPolicy.ContactEmail;
                    chbIsBusinessSurveyDone.IsChecked = elementaryPolicyToUpdate.BusinessPolicy.IsSurveyDone;
                    dpBusinessSurveyDate.SelectedDate = elementaryPolicyToUpdate.BusinessPolicy.SurveyDate;
                    //var appraisers = cbBusinessSurveyBy.Items;
                    //foreach (var item in appraisers)
                    //{
                    //    Appraiser appraiser = (Appraiser)item;
                    //    if (appraiser.AppraiserID == elementaryPolicyToUpdate.BusinessPolicy.SurveyByAppraiserID)
                    //    {
                    //        cbBusinessSurveyBy.SelectedItem = item;
                    //        break;
                    //    }
                    //}
                    if (elementaryPolicyToUpdate.BusinessPolicy.SurveyByAppraiserID != null)
                    {
                        SelectAppraiserItemInCb((int)elementaryPolicyToUpdate.BusinessPolicy.SurveyByAppraiserID, cbBusinessSurveyBy);
                    }
                    lblBusinessInsuranceThisYear.IsChecked = elementaryPolicyToUpdate.BusinessPolicy.InsuranceThisYear;
                    lblBusinessInsuranceLastYear.IsChecked = elementaryPolicyToUpdate.BusinessPolicy.InsuranceLastYear;
                    lblBusinessInsurance2YearsAgo.IsChecked = elementaryPolicyToUpdate.BusinessPolicy.Insurance2YearsAgo;
                    lblBusinessInsurance3YearsAgo.IsChecked = elementaryPolicyToUpdate.BusinessPolicy.Insurance3YearsAgo;
                    txtBusinessClaimsThisYear.Text = elementaryPolicyToUpdate.BusinessPolicy.ClaimsNumberThisYear.ToString();
                    txtBusinessClaimsLastYear.Text = elementaryPolicyToUpdate.BusinessPolicy.ClaimsNumberLastYear.ToString();
                    txtBusinessClaims2YearsAgo.Text = elementaryPolicyToUpdate.BusinessPolicy.ClaimsNumber2YearsAgo.ToString();
                    txtBusinessClaims3YearsAgo.Text = elementaryPolicyToUpdate.BusinessPolicy.ClaimsNumber3YearsAgo.ToString();
                    cbBusinessThisYearCompany.SelectedItem = GetSelectedCompany(cbBusinessThisYearCompany, elementaryPolicyToUpdate.BusinessPolicy.InsuranceCompanyThisYear);
                    if (cbBusinessThisYearCompany.SelectedItem == null)
                    {
                        cbBusinessThisYearCompany.Text = elementaryPolicyToUpdate.BusinessPolicy.InsuranceCompanyThisYear;
                    }
                    cbBusinessLastYearCompany.SelectedItem = GetSelectedCompany(cbBusinessLastYearCompany, elementaryPolicyToUpdate.BusinessPolicy.InsuranceCompanyLastYear);
                    if (cbBusinessLastYearCompany.SelectedItem == null)
                    {
                        cbBusinessLastYearCompany.Text = elementaryPolicyToUpdate.BusinessPolicy.InsuranceCompanyLastYear;
                    }
                    cbBusiness2YearsAgoCompany.SelectedItem = GetSelectedCompany(cbBusiness2YearsAgoCompany, elementaryPolicyToUpdate.BusinessPolicy.InsuranceCompany2YearsAgo);
                    if (cbBusiness2YearsAgoCompany.SelectedItem == null)
                    {
                        cbBusiness2YearsAgoCompany.Text = elementaryPolicyToUpdate.BusinessPolicy.InsuranceCompany2YearsAgo;
                    }
                    cbBusiness3YearsAgoCompany.SelectedItem = GetSelectedCompany(cbBusiness3YearsAgoCompany, elementaryPolicyToUpdate.BusinessPolicy.InsuranceCompany3YearsAgo);
                    if (cbBusiness3YearsAgoCompany.SelectedItem == null)
                    {
                        cbBusiness3YearsAgoCompany.Text = elementaryPolicyToUpdate.BusinessPolicy.InsuranceCompany3YearsAgo;
                    }
                }
            }
            //מילואי שדות פוליסת דירה בעדבון
            else if (industrySelected.InsuranceIndustryName == "דירה")
            {
                tbItemApartment.Visibility = Visibility.Visible;
                tbItemCar.Visibility = Visibility.Collapsed;
                tbItemBusiness.Visibility = Visibility.Collapsed;


                if (elementaryPolicyToUpdate != null && elementaryPolicyToUpdate.InsuranceIndustry.InsuranceIndustryName != "שגויים")
                {
                    txtCity.Text = elementaryPolicyToUpdate.ApartmentPolicy.City;
                    txtStreet.Text = elementaryPolicyToUpdate.ApartmentPolicy.Street;
                    txtHouseNumber.Text = elementaryPolicyToUpdate.ApartmentPolicy.HomeNumber;
                    txtAptNumber.Text = elementaryPolicyToUpdate.ApartmentPolicy.ApartmentNumber;
                    txtZipCode.Text = elementaryPolicyToUpdate.ApartmentPolicy.ZipCode;
                    //var structureTypes = cbStructureType.Items;
                    //foreach (var item in structureTypes)
                    //{
                    //    StructureType structure = (StructureType)item;
                    //    if (structure.StructureTypeID == elementaryPolicyToUpdate.ApartmentPolicy.StructureTypeID)
                    //    {
                    //        cbStructureType.SelectedItem = item;
                    //        break;
                    //    }
                    //}
                    if (elementaryPolicyToUpdate.ApartmentPolicy.StructureTypeID != null)
                    {
                        SelectStructureItemInCb((int)elementaryPolicyToUpdate.ApartmentPolicy.StructureTypeID, cbStructureType);
                    }
                    txtFloor.Text = elementaryPolicyToUpdate.ApartmentPolicy.Floor;
                    txtTotalFloors.Text = elementaryPolicyToUpdate.ApartmentPolicy.TotalBuildingFloorsNumber;
                    txtSize.Text = ConvertDecimalToString(elementaryPolicyToUpdate.ApartmentPolicy.SizeInMeters);
                    txtRoomsNumber.Text = elementaryPolicyToUpdate.ApartmentPolicy.RoomsNumber;
                    txtProtectionRequired.Text = elementaryPolicyToUpdate.ApartmentPolicy.ProtectionRequired;
                    txtAptContactName.Text = elementaryPolicyToUpdate.ApartmentPolicy.ContactName;
                    txtContactPhone.Text = elementaryPolicyToUpdate.ApartmentPolicy.ContactPhone;
                    txtContactCellPhone.Text = elementaryPolicyToUpdate.ApartmentPolicy.ContactCellPhone;
                    txtContactFax.Text = elementaryPolicyToUpdate.ApartmentPolicy.ContactFax;
                    txtContactEmail.Text = elementaryPolicyToUpdate.ApartmentPolicy.ContactEmail;
                    chbIsSurveyDone.IsChecked = elementaryPolicyToUpdate.ApartmentPolicy.IsSurveyDone;
                    dpSurveyDate.SelectedDate = elementaryPolicyToUpdate.ApartmentPolicy.SurveyDate;
                    //var appraisers = cbSurveyBy.Items;
                    //foreach (var item in appraisers)
                    //{
                    //    Appraiser appraiser = (Appraiser)item;
                    //    if (appraiser.AppraiserID == elementaryPolicyToUpdate.ApartmentPolicy.SurveyByAppraiserID)
                    //    {
                    //        cbSurveyBy.SelectedItem = item;
                    //        break;
                    //    }
                    //}
                    if (elementaryPolicyToUpdate.ApartmentPolicy.SurveyByAppraiserID != null)
                    {
                        SelectAppraiserItemInCb((int)elementaryPolicyToUpdate.ApartmentPolicy.SurveyByAppraiserID, cbSurveyBy);
                    }
                    lblAptInsuranceThisYear.IsChecked = elementaryPolicyToUpdate.ApartmentPolicy.InsuranceThisYear;
                    lblAptInsuranceLastYear.IsChecked = elementaryPolicyToUpdate.ApartmentPolicy.InsuranceLastYear;
                    lblAptInsurance2YearsAgo.IsChecked = elementaryPolicyToUpdate.ApartmentPolicy.Insurance2YearsAgo;
                    lblAptInsurance3YearsAgo.IsChecked = elementaryPolicyToUpdate.ApartmentPolicy.Insurance3YearsAgo;
                    txtAptClaimsThisYear.Text = elementaryPolicyToUpdate.ApartmentPolicy.ClaimsNumberThisYear.ToString();
                    txtAptClaimsLastYear.Text = elementaryPolicyToUpdate.ApartmentPolicy.ClaimsNumberLastYear.ToString();
                    txtAptClaims2YearsAgo.Text = elementaryPolicyToUpdate.ApartmentPolicy.ClaimsNumber2YearsAgo.ToString();
                    txtAptClaims3YearsAgo.Text = elementaryPolicyToUpdate.ApartmentPolicy.ClaimsNumber3YearsAgo.ToString();
                    cbAptThisYearCompany.SelectedItem = GetSelectedCompany(cbAptThisYearCompany, elementaryPolicyToUpdate.ApartmentPolicy.InsuranceCompanyThisYear);
                    if (cbAptThisYearCompany.SelectedItem == null)
                    {
                        cbAptThisYearCompany.Text = elementaryPolicyToUpdate.ApartmentPolicy.InsuranceCompanyThisYear;
                    }
                    cbAptLastYearCompany.SelectedItem = GetSelectedCompany(cbAptLastYearCompany, elementaryPolicyToUpdate.ApartmentPolicy.InsuranceCompanyLastYear);
                    if (cbAptLastYearCompany.SelectedItem == null)
                    {
                        cbAptLastYearCompany.Text = elementaryPolicyToUpdate.ApartmentPolicy.InsuranceCompanyLastYear;
                    }
                    cbApt2YearsAgoCompany.SelectedItem = GetSelectedCompany(cbApt2YearsAgoCompany, elementaryPolicyToUpdate.ApartmentPolicy.InsuranceCompany2YearsAgo);
                    if (cbApt2YearsAgoCompany.SelectedItem == null)
                    {
                        cbApt2YearsAgoCompany.Text = elementaryPolicyToUpdate.ApartmentPolicy.InsuranceCompany2YearsAgo;
                    }
                    cbApt3YearsAgoCompany.SelectedItem = GetSelectedCompany(cbApt3YearsAgoCompany, elementaryPolicyToUpdate.ApartmentPolicy.InsuranceCompany3YearsAgo);
                    if (cbApt3YearsAgoCompany.SelectedItem == null)
                    {
                        cbApt3YearsAgoCompany.Text = elementaryPolicyToUpdate.ApartmentPolicy.InsuranceCompany3YearsAgo;
                    }
                    txtAptComments.Text = elementaryPolicyToUpdate.ApartmentPolicy.Comments;
                }
            }

        }

        //פונקציה המחזירה את  פריט הקומבו בוקס של חברת הביטוח שנבחרה בפוליסה 
        private object GetSelectedCompany(ComboBox comboBoxToGetItems, string companyFromPolicy)
        {
            var companies = comboBoxToGetItems.Items;
            foreach (var item in companies)
            {
                Company company = (Company)item;
                if (companyFromPolicy == company.CompanyName)
                {
                    return item;
                }
            }
            return null;
        }

        //מסנכרן את קומבו בוקס של סוגי ביטוח עם בסיס הנתונים
        private void cbInsuranceTypeBinding()
        {
            InsuranceIndustry industry = (InsuranceIndustry)cbIndustry.SelectedItem;
            cbInsuranceType.ItemsSource = industryLogic.GetActiveInsuranceTypesByIndustry(industry.InsuranceIndustryID);
            cbInsuranceType.DisplayMemberPath = "ElementaryInsuranceTypeName";
        }

        //הגדרה של סוכני ביטוח, ראשי ומישנה, לפי חברת הביטוח שנבחרה
        private void cbCompany_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cbCompany.SelectedItem == null)
            {
                cbPrincipalAgentNumber.SelectedItem = null;
                cbSecundaryAgentNumber.SelectedItem = null;
                cbPrincipalAgentNumber.IsEnabled = false;
                cbSecundaryAgentNumber.IsEnabled = false;
                return;
            }
            int insuranceID = insuranceLogic.GetInsuranceID("אלמנטרי");
            if (client != null)
            {
                cbPrincipalAgentNumber.ItemsSource = agentLogic.GetAgentNumbersByCompanyAndInsurance(client.PrincipalAgentID, (InsuranceCompany)cbCompany.SelectedItem, insuranceID);
            }
            else
            {
                cbPrincipalAgentNumber.ItemsSource = agentLogic.GetAgentNumbersByCompanyAndInsurance(policyClient.PrincipalAgentID, (InsuranceCompany)cbCompany.SelectedItem, insuranceID);
            }
            cbPrincipalAgentNumber.DisplayMemberPath = "Number";
            cbPrincipalAgentNumber.IsEnabled = true;
            cbPrincipalAgentNumber.SelectedIndex = 0;
            if (client != null)
            {
                cbSecundaryAgentNumber.ItemsSource = agentLogic.GetAgentNumbersByCompanyAndInsurance(client.SecundaryAgentID, (InsuranceCompany)cbCompany.SelectedItem, insuranceID);
            }
            else
            {
                cbSecundaryAgentNumber.ItemsSource = agentLogic.GetAgentNumbersByCompanyAndInsurance(policyClient.SecundaryAgentID, (InsuranceCompany)cbCompany.SelectedItem, insuranceID);
            }
            cbSecundaryAgentNumber.DisplayMemberPath = "Number";
            cbSecundaryAgentNumber.IsEnabled = true;
            cbSecundaryAgentNumber.SelectedIndex = 0;
        }

        //פותח את לשונית הכיסוים כאשר נבחר סוג ביטוח וסוגר אותה אם לא נבחר
        private void cbInsuranceType_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cbInsuranceType.SelectedItem != null)
            {
                tbItemCoverage.Visibility = Visibility.Visible;
            }
            else
            {
                tbItemCoverage.Visibility = Visibility.Collapsed;
            }

        }

        //פותח את חלון עדכון טבלת סוגי ביטוח
        private void btnUpdateInsuranceTypeTable_Click(object sender, RoutedEventArgs e)
        {
            int? selectedItemId = null;
            if (cbInsuranceType.SelectedItem != null)
            {
                selectedItemId = ((ElementaryInsuranceType)cbInsuranceType.SelectedItem).ElementaryInsuranceTypeID;
            }
            InsuranceIndustry industry = (InsuranceIndustry)cbIndustry.SelectedItem;
            ElementaryInsuranceType newType = new ElementaryInsuranceType() { InsuranceIndustryID = industry.InsuranceIndustryID };
            frmUpdateTable updateInsuranceTypesTable = new frmUpdateTable(newType);
            updateInsuranceTypesTable.ShowDialog();
            cbInsuranceTypeBinding();
            if (updateInsuranceTypesTable.ItemAdded != null)
            {
                InsuranceTypeSelection(((ElementaryInsuranceType)updateInsuranceTypesTable.ItemAdded).ElementaryInsuranceTypeID);
            }
            else if (selectedItemId != null)
            {
                InsuranceTypeSelection((int)selectedItemId);
            }
        }



        //בודק ששדות החובה של פוליסה כללית יהיו ממולים
        public bool InputNullValidation()
        {
            bool isValid = true;
            if (txtPolicyNumber.Text == "")
            {
                txtPolicyNumber.BorderBrush = System.Windows.Media.Brushes.Red;
                isValid = false;
            }
            if (txtAddition.Text == "")
            {
                txtAddition.BorderBrush = System.Windows.Media.Brushes.Red;
                isValid = false;
            }
            if (dpStartDate.SelectedDate == null)
            {
                dpStartDate.BorderBrush = System.Windows.Media.Brushes.Red;
                isValid = false;
            }
            if (dpEndDate.SelectedDate == null)
            {
                dpEndDate.BorderBrush = System.Windows.Media.Brushes.Red;
                isValid = false;
            }
            if (cbIndustry.SelectedItem == null)
            {
                cbIndustry.BorderBrush = System.Windows.Media.Brushes.Red;
                isValid = false;
            }
            if (cbCompany.SelectedItem == null)
            {
                cbCompany.BorderBrush = System.Windows.Media.Brushes.Red;
                isValid = false;
            }
            if (cbInsuranceType.SelectedItem == null)
            {
                cbInsuranceType.BorderBrush = System.Windows.Media.Brushes.Red;
                isValid = false;
            }
            if (txtTotalPremium.Text == "")
            {
                txtTotalPremium.BorderBrush = System.Windows.Media.Brushes.Red;
                isValid = false;
            }
            return isValid;
        }

        //בודק ששדות החובה של פוליסת דירה יהיו ממולים

        public bool AptInputValidation()
        {
            bool isValid = true;
            if (txtCity.Text == "")
            {
                txtCity.BorderBrush = System.Windows.Media.Brushes.Red;
                isValid = false;
            }
            if (txtStreet.Text == "")
            {
                txtStreet.BorderBrush = System.Windows.Media.Brushes.Red;
                isValid = false;
            }
            if (txtHouseNumber.Text == "")
            {
                txtHouseNumber.BorderBrush = System.Windows.Media.Brushes.Red;
                isValid = false;
            }
            return isValid;
        }

        //בודק ששדות החובה של פוליסת עסק יהיו ממולים
        public bool BusinessInputValidation()
        {
            bool isValid = true;
            if (txtBusinessCity.Text == "")
            {
                txtBusinessCity.BorderBrush = System.Windows.Media.Brushes.Red;
                isValid = false;
            }
            if (txtBusinessStreet.Text == "")
            {
                txtBusinessStreet.BorderBrush = System.Windows.Media.Brushes.Red;
                isValid = false;
            }
            if (txtBusinessHouseNumber.Text == "")
            {
                txtBusinessHouseNumber.BorderBrush = System.Windows.Media.Brushes.Red;
                isValid = false;
            }
            if (txtBusinessOwner.Text == "")
            {
                txtBusinessOwner.BorderBrush = System.Windows.Media.Brushes.Red;
                isValid = false;
            }
            if (txtBusinessName.Text == "")
            {
                txtBusinessName.BorderBrush = System.Windows.Media.Brushes.Red;
                isValid = false;
            }
            return isValid;
        }

        //בודק ששדות החובה של פוליסת רכב יהיו ממולים
        public bool CarInputValidation()
        {
            bool isValid = true;
            if (txtYear.Text == "")
            {
                txtYear.BorderBrush = System.Windows.Media.Brushes.Red;
                isValid = false;
            }
            if (txtRegistrationNumber.Text == "")
            {
                txtRegistrationNumber.BorderBrush = System.Windows.Media.Brushes.Red;
                isValid = false;
            }
            if (txtManufacturer.Text == "")
            {
                txtManufacturer.BorderBrush = System.Windows.Media.Brushes.Red;
                isValid = false;
            }
            //if (cbCarType.SelectedItem == null)
            //{
            //    cbCarType.BorderBrush = System.Windows.Media.Brushes.Red;
            //    isValid = false;
            //}
            return isValid;
        }
        //שמירה/עדכון פוליסה
        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            bool isPolicyAdded = false;
            txtPolicyNumber.ClearValue(BorderBrushProperty);
            txtAddition.ClearValue(BorderBrushProperty);
            dpStartDate.ClearValue(BorderBrushProperty);
            dpEndDate.ClearValue(BorderBrushProperty);
            cbIndustry.ClearValue(BorderBrushProperty);
            cbCompany.ClearValue(BorderBrushProperty);
            cbInsuranceType.ClearValue(BorderBrushProperty);
            txtTotalPremium.ClearValue(BorderBrushProperty);
            if (errorList != null && errorList.Count > 0)
            {
                foreach (TextBox item in errorList)
                {
                    item.ClearValue(BorderBrushProperty);
                }
            }
            if (!validations.IsDigitsOnly(txtPolicyNumber.Text))
            {
                cancelClose = true;
                MessageBox.Show("מס' פוליסה יכול להכיל מספרים בלבד", "", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            //בדיקת שדות חובה
            if (!InputNullValidation())
            {
                tbItemGeneral.Focus();
                MessageBox.Show("נא למלאות שדות חובה", "", MessageBoxButton.OK, MessageBoxImage.Error);
                cancelClose = true;
                return;
            }
            //בדיקה שתאריך סיום גדול מתאריך התחלה
            if (dpEndDate.SelectedDate <= dpStartDate.SelectedDate)
            {
                tbItemGeneral.Focus();
                MessageBox.Show("תאריך סיום חייב להיות גדול מתאריך תחילה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                cancelClose = true;
                return;
            }
            if (isAdditon && dpStartDate.SelectedDate < elementaryPolicyToUpdate.StartDate)
            {
                if (((DateTime)dpStartDate.SelectedDate).Date == ((DateTime)elementaryPolicyToUpdate.StartDate).Date)
                {
                    dpStartDate.SelectedDate = new DateTime(((DateTime)dpStartDate.SelectedDate).Year, ((DateTime)dpStartDate.SelectedDate).Month, ((DateTime)dpStartDate.SelectedDate).Day, 23, 59, 59);
                }
                else
                {
                    tbItemGeneral.Focus();
                    MessageBox.Show("תאריך תחילת התוספת חייב להיות גדול מתאריך תחילת התוספת הקודמת", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    cancelClose = true;
                    return;
                }
            }
            //ולידציה של שדות מספריים
            errorList = validations.OnlyNumbersValidation(new TextBox[] { txtAddition, txtPaymentesNumber, txtPremiumNeto1, txtPremiumNeto2, txtPremiumNeto3, txtPremiumNeto4, txtInscriptionFees, txtPolicyFees, txtProjectionFees, txtStampFees, txtHandlingFees, txtTotalFees, txtTotalCredit, txtTotalPremium });
            if (errorList.Count > 0)
            {
                tbItemGeneral.Focus();
                MessageBox.Show("נא להזין מספרים או נקודה בלבד", "", MessageBoxButton.OK, MessageBoxImage.Error);
                cancelClose = true;
                return;
            }
            try
            {
                //המרת התוכן הטקסטואלי של השדות לסוג הנדרשת בסיס הנתונים 
                int? addition = int.Parse(txtAddition.Text);
                int? paymentNumbers = null;
                if (txtPaymentesNumber.Text != "")
                {
                    paymentNumbers = int.Parse(txtPaymentesNumber.Text);
                }
                decimal? premium1 = null;
                if (txtPremiumNeto1.Text != "")
                {
                    premium1 = decimal.Parse(txtPremiumNeto1.Text);
                }
                decimal? premium2 = null;
                if (txtPremiumNeto2.Text != "")
                {
                    premium2 = decimal.Parse(txtPremiumNeto2.Text);
                }
                decimal? premium3 = null;
                if (txtPremiumNeto3.Text != "")
                {
                    premium3 = decimal.Parse(txtPremiumNeto3.Text);
                }
                decimal? premium4 = null;
                if (txtPremiumNeto4.Text != "")
                {
                    premium4 = decimal.Parse(txtPremiumNeto4.Text);
                }
                decimal? inscriptionFees = null;
                if (txtInscriptionFees.Text != "")
                {
                    inscriptionFees = decimal.Parse(txtInscriptionFees.Text);
                }
                decimal? policyFees = null;
                if (txtPolicyFees.Text != "")
                {
                    policyFees = decimal.Parse(txtPolicyFees.Text);
                }
                decimal? projectionFees = null;
                if (txtProjectionFees.Text != "")
                {
                    projectionFees = decimal.Parse(txtProjectionFees.Text);
                }
                decimal? stampFees = null;
                if (txtStampFees.Text != "")
                {
                    stampFees = decimal.Parse(txtStampFees.Text);
                }
                decimal? handlingFees = null;
                if (txtHandlingFees.Text != "")
                {
                    handlingFees = decimal.Parse(txtHandlingFees.Text);
                }
                decimal? totalPremium = null;
                if (txtTotalPremium.Text != "")
                {
                    totalPremium = decimal.Parse(txtTotalPremium.Text);
                }
                decimal? totalFees = null;
                if (txtTotalFees.Text != "")
                {
                    totalFees = decimal.Parse(txtTotalFees.Text);
                }
                decimal? totalCredit = null;
                if (txtTotalCredit.Text != "")
                {
                    totalCredit = decimal.Parse(txtTotalCredit.Text);
                }
                string paymentMode = "";
                if (cbPaymentMode.SelectedItem != null)
                {
                    paymentMode = ((ComboBoxItem)cbPaymentMode.SelectedItem).Content.ToString();
                }
                DateTime? start = dpStartDate.SelectedDate;
                if (elementaryPolicyToUpdate == null || ((DateTime)dpStartDate.SelectedDate).Date != ((DateTime)elementaryPolicyToUpdate.StartDate).Date)
                {
                    start = start + DateTime.Now.TimeOfDay;
                }
                DateTime end = (DateTime)dpEndDate.SelectedDate;
                TimeSpan endOfDay = new TimeSpan(23, 59, 59);
                end = end.Date + endOfDay;


                //פוליסת דירה
                if (((InsuranceIndustry)cbIndustry.SelectedItem).InsuranceIndustryName == "דירה")
                {
                    txtCity.ClearValue(BorderBrushProperty);
                    txtStreet.ClearValue(BorderBrushProperty);
                    txtHouseNumber.ClearValue(BorderBrushProperty);
                    if (errorList != null && errorList.Count > 0)
                    {
                        foreach (TextBox item in errorList)
                        {
                            item.ClearValue(BorderBrushProperty);
                        }
                    }
                    //בדיקת שדות החובה
                    if (!AptInputValidation())
                    {
                        tbItemApartment.Focus();
                        MessageBox.Show("נא למלאות שדות חובה", "", MessageBoxButton.OK, MessageBoxImage.Error);
                        cancelClose = true;
                        return;
                    }
                    //ולידציה של שדות מספריים
                    errorList = validations.OnlyNumbersValidation(new TextBox[] { txtSize, txtAptClaimsThisYear, txtAptClaimsLastYear, txtAptClaims2YearsAgo, txtAptClaims3YearsAgo });
                    if (errorList.Count > 0)
                    {
                        tbItemApartment.Focus();
                        MessageBox.Show("נא להזין מספרים בלבד", "", MessageBoxButton.OK, MessageBoxImage.Error);
                        cancelClose = true;
                        return;
                    }
                    //המרת השדות מטקסט לסוג הנדרש
                    decimal? size = null;
                    if (txtSize.Text != "")
                    {
                        size = decimal.Parse(txtSize.Text);
                    }
                    bool isNew = false;
                    if (chbisNew.IsChecked == true)
                    {
                        isNew = true;
                    }
                    int? claimsThisYear = null;
                    if (txtAptClaimsThisYear.Text != "")
                    {
                        claimsThisYear = int.Parse(txtAptClaimsThisYear.Text);
                    }
                    int? claimsLastYear = null;
                    if (txtAptClaimsLastYear.Text != "")
                    {
                        claimsLastYear = int.Parse(txtAptClaimsLastYear.Text);
                    }
                    int? claims2YearsAgo = null;
                    if (txtAptClaims2YearsAgo.Text != "")
                    {
                        claims2YearsAgo = int.Parse(txtAptClaims2YearsAgo.Text);
                    }
                    int? claims3YearsAgo = null;
                    if (txtAptClaims3YearsAgo.Text != "")
                    {
                        claims3YearsAgo = int.Parse(txtAptClaims3YearsAgo.Text);
                    }
                    string companyThisYear = "";
                    if (cbAptThisYearCompany.SelectedItem != null)
                    {
                        companyThisYear = ((Company)cbAptThisYearCompany.SelectedItem).CompanyName;
                    }
                    else
                    {
                        companyThisYear = cbAptThisYearCompany.Text;
                    }
                    string companyLastYear = "";
                    if (cbAptLastYearCompany.SelectedItem != null)
                    {
                        companyLastYear = ((Company)cbAptLastYearCompany.SelectedItem).CompanyName;
                    }
                    else
                    {
                        companyLastYear = cbAptLastYearCompany.Text;
                    }
                    string company2YearsAgo = "";
                    if (cbApt2YearsAgoCompany.SelectedItem != null)
                    {
                        company2YearsAgo = ((Company)cbApt2YearsAgoCompany.SelectedItem).CompanyName;
                    }
                    else
                    {
                        company2YearsAgo = cbApt2YearsAgoCompany.Text;
                    }
                    string company3YearsAgo = "";
                    if (cbApt3YearsAgoCompany.SelectedItem != null)
                    {
                        company3YearsAgo = ((Company)cbApt3YearsAgoCompany.SelectedItem).CompanyName;
                    }
                    else
                    {
                        company3YearsAgo = cbApt3YearsAgoCompany.Text;
                    }
                    int? appraiserID = null;
                    if (cbSurveyBy.SelectedItem != null)
                    {
                        appraiserID = ((Appraiser)cbSurveyBy.SelectedItem).AppraiserID;
                    }
                    if (elementaryPolicyToUpdate == null || isAdditon == true || isRenewal == true)
                    {
                        //שליחת הנתונים לשמירה של הפרטים הכלליים של פוליסה חדשה
                        policyId = policyLogic.InsertElementaryPolicy(client, isOffer, now, (InsuranceIndustry)cbIndustry.SelectedItem, txtPolicyNumber.Text, addition, (AgentNumber)cbPrincipalAgentNumber.SelectedItem, (AgentNumber)cbSecundaryAgentNumber.SelectedItem, start, end, (InsuranceCompany)cbCompany.SelectedItem, (ElementaryInsuranceType)cbInsuranceType.SelectedItem, ((ComboBoxItem)cbCurrency.SelectedItem).Content.ToString(), paymentMode, paymentNumbers, dpPaymentDate.SelectedDate, premium1, premium2, premium3, premium4, inscriptionFees, policyFees, projectionFees, stampFees, handlingFees, totalPremium, totalFees, totalCredit, chbIsMortgaged.IsChecked, txtBank.Text, txtBranch.Text, txtAddress.Text, txtPhone.Text, txtFax.Text, txtContactName.Text, txtEmail.Text, txtComments.Text, isNew);
                        isPolicyAdded = true;
                        renewalAccepted = true;
                        if (!isOffer)
                        {
                            path = clientDirPath + @"\" + client.ClientID + @"\אלמנטרי\דירה\" + ((InsuranceIndustry)cbIndustry.SelectedItem).InsuranceIndustryName + " " + txtPolicyNumber.Text;
                        }
                        else
                        {
                            path = clientDirPath + @"\" + client.ClientID + @"\הצעות\אלמנטרי\" + ((InsuranceIndustry)cbIndustry.SelectedItem).InsuranceIndustryName + " " + txtPolicyNumber.Text;
                        }

                        //שליחת הנתונים לשמירה של פוליסת דירה חדשה
                        policyLogic.InsertApartmentPolicy(policyId, txtPolicyNumber.Text, addition, txtCity.Text, txtStreet.Text, txtHouseNumber.Text, txtAptNumber.Text, txtZipCode.Text, txtFloor.Text, txtTotalFloors.Text, size, txtRoomsNumber.Text, (StructureType)cbStructureType.SelectedItem, txtProtectionRequired.Text, txtAptContactName.Text, txtContactPhone.Text, txtContactCellPhone.Text, txtContactFax.Text, txtContactEmail.Text, chbIsSurveyDone.IsChecked, dpSurveyDate.SelectedDate, appraiserID, lblAptInsuranceThisYear.IsChecked, lblAptInsuranceLastYear.IsChecked, lblAptInsurance2YearsAgo.IsChecked, lblAptInsurance3YearsAgo.IsChecked, claimsThisYear, claimsLastYear, claims2YearsAgo, claims3YearsAgo, companyThisYear, companyLastYear, company2YearsAgo, company3YearsAgo, txtAptComments.Text);
                        //"/אלמנטרי/רכב" + policy.PolicyNumber;
                    }
                    else
                    {
                        //שליחת הנתונים לעדכון של הפרטים הכלליים של פוליסה קיימת
                        policyLogic.UpdateElementaryPolicy(elementaryPolicyToUpdate.ElementaryPolicyID, elementaryPolicyToUpdate.PolicyNumber, elementaryPolicyToUpdate.InsuranceIndustryID, isOffer, txtPolicyNumber.Text, ((InsuranceIndustry)cbIndustry.SelectedItem).InsuranceIndustryID, addition, (AgentNumber)cbPrincipalAgentNumber.SelectedItem, (AgentNumber)cbSecundaryAgentNumber.SelectedItem, start, end, (InsuranceCompany)cbCompany.SelectedItem, (ElementaryInsuranceType)cbInsuranceType.SelectedItem, ((ComboBoxItem)cbCurrency.SelectedItem).Content.ToString(), paymentMode, paymentNumbers, dpPaymentDate.SelectedDate, premium1, premium2, premium3, premium4, inscriptionFees, policyFees, projectionFees, stampFees, handlingFees, totalPremium, totalFees, totalCredit, chbIsMortgaged.IsChecked, txtBank.Text, txtBranch.Text, txtAddress.Text, txtPhone.Text, txtFax.Text, txtContactName.Text, txtEmail.Text, txtComments.Text, isNew);
                        isPolicyAdded = true;
                        if (!isOffer)
                        {
                            if (Directory.Exists(clientDirPath + @"\" + elementaryPolicyToUpdate.ClientID + @"\אלמנטרי\דירה"))
                            {
                                string[] dirs = Directory.GetDirectories(clientDirPath + @"\" + elementaryPolicyToUpdate.ClientID + @"\אלמנטרי\דירה", "*" + elementaryPolicyToUpdate.InsuranceIndustry.InsuranceIndustryName + " " + elementaryPolicyToUpdate.PolicyNumber + "*");
                                if (dirs.Count() > 0)
                                {
                                    path = dirs[0];
                                }
                                else
                                {
                                    dirs = Directory.GetDirectories(clientDirPath + @"\" + elementaryPolicyToUpdate.ClientID + @"\אלמנטרי\דירה", "*" + elementaryPolicyToUpdate.PolicyNumber + "*");
                                    if (dirs.Count() > 0)
                                    {
                                        path = dirs[0];
                                    }
                                }
                            }
                            newPath = clientDirPath + @"\" + elementaryPolicyToUpdate.ClientID + @"\אלמנטרי\דירה\" + ((InsuranceIndustry)cbIndustry.SelectedItem).InsuranceIndustryName + " " + txtPolicyNumber.Text;
                        }
                        else
                        {
                            if (Directory.Exists(clientDirPath + @"\" + elementaryPolicyToUpdate.ClientID + @"\הצעות\אלמנטרי"))
                            {
                                string[] dirs = Directory.GetDirectories(clientDirPath + @"\" + elementaryPolicyToUpdate.ClientID + @"\הצעות\אלמנטרי", "*" + elementaryPolicyToUpdate.InsuranceIndustry.InsuranceIndustryName + " " + elementaryPolicyToUpdate.PolicyNumber + "*");
                                if (dirs.Count() > 0)
                                {
                                    path = dirs[0];
                                }
                                else
                                {
                                    dirs = Directory.GetDirectories(clientDirPath + @"\" + elementaryPolicyToUpdate.ClientID + @"\הצעות\אלמנטרי", "*" + elementaryPolicyToUpdate.PolicyNumber + "*");
                                    if (dirs.Count() > 0)
                                    {
                                        path = dirs[0];
                                    }
                                }
                            }
                            newPath = clientDirPath + @"\" + elementaryPolicyToUpdate.ClientID + @"\הצעות\אלמנטרי\" + ((InsuranceIndustry)cbIndustry.SelectedItem).InsuranceIndustryName + " " + txtPolicyNumber.Text;
                        }

                        //שליחת הנתונים לעדכון של פוליסת דירה קיימת
                        policyLogic.UpdateApartmentPolicy(elementaryPolicyToUpdate.ElementaryPolicyID, txtPolicyNumber.Text, addition, txtCity.Text, txtStreet.Text, txtHouseNumber.Text, txtAptNumber.Text, txtZipCode.Text, txtFloor.Text, txtTotalFloors.Text, size, txtRoomsNumber.Text, (StructureType)cbStructureType.SelectedItem, txtProtectionRequired.Text, txtAptContactName.Text, txtContactPhone.Text, txtContactCellPhone.Text, txtContactFax.Text, txtContactEmail.Text, chbIsSurveyDone.IsChecked, dpSurveyDate.SelectedDate, appraiserID, lblAptInsuranceThisYear.IsChecked, lblAptInsuranceLastYear.IsChecked, lblAptInsurance2YearsAgo.IsChecked, lblAptInsurance3YearsAgo.IsChecked, claimsThisYear, claimsLastYear, claims2YearsAgo, claims3YearsAgo, companyThisYear, companyLastYear, company2YearsAgo, company3YearsAgo, txtAptComments.Text);
                    }
                }
                else if (((InsuranceIndustry)cbIndustry.SelectedItem).InsuranceIndustryName == "עסק")
                {
                    txtBusinessCity.ClearValue(BorderBrushProperty);
                    txtBusinessStreet.ClearValue(BorderBrushProperty);
                    txtBusinessHouseNumber.ClearValue(BorderBrushProperty);
                    txtBusinessOwner.ClearValue(BorderBrushProperty);
                    txtBusinessName.ClearValue(BorderBrushProperty);
                    if (errorList != null && errorList.Count > 0)
                    {
                        foreach (TextBox item in errorList)
                        {
                            item.ClearValue(BorderBrushProperty);
                        }
                    }
                    //בדיקת שדות החובה
                    if (!BusinessInputValidation())
                    {
                        tbItemBusiness.Focus();
                        MessageBox.Show("נא למלאות שדות חובה", "", MessageBoxButton.OK, MessageBoxImage.Error);
                        cancelClose = true;
                        return;
                    }
                    //ולידציה של שדות מספריים
                    errorList = validations.OnlyNumbersValidation(new TextBox[] { txtBusinessSize, txtBusinessClaimsThisYear, txtBusinessClaimsLastYear, txtBusinessClaims2YearsAgo, txtBusinessClaims3YearsAgo });
                    if (errorList.Count > 0)
                    {
                        tbItemBusiness.Focus();
                        MessageBox.Show("נא להזין מספרים בלבד", "", MessageBoxButton.OK, MessageBoxImage.Error);
                        cancelClose = true;
                        return;
                    }
                    //המרת שדות טקסטואליות לסוג הנדרש בבסיס הנתונים
                    decimal? businessSize = null;
                    if (txtBusinessSize.Text != "")
                    {
                        businessSize = decimal.Parse(txtBusinessSize.Text);
                    }
                    int? appraiserID = null;
                    if (cbBusinessSurveyBy.SelectedItem != null)
                    {
                        appraiserID = ((Appraiser)cbBusinessSurveyBy.SelectedItem).AppraiserID;
                    }
                    int? claimsThisYear = null;
                    if (txtBusinessClaimsThisYear.Text != "")
                    {
                        claimsThisYear = int.Parse(txtBusinessClaimsThisYear.Text);
                    }
                    int? claimsLastYear = null;
                    if (txtBusinessClaimsLastYear.Text != "")
                    {
                        claimsLastYear = int.Parse(txtBusinessClaimsLastYear.Text);
                    }
                    int? claims2YearsAgo = null;
                    if (txtBusinessClaims2YearsAgo.Text != "")
                    {
                        claims2YearsAgo = int.Parse(txtBusinessClaims2YearsAgo.Text);
                    }
                    int? claims3YearsAgo = null;
                    if (txtBusinessClaims3YearsAgo.Text != "")
                    {
                        claims3YearsAgo = int.Parse(txtBusinessClaims3YearsAgo.Text);
                    }
                    string companyThisYear = "";
                    if (cbBusinessThisYearCompany.SelectedItem != null)
                    {
                        companyThisYear = ((Company)cbBusinessThisYearCompany.SelectedItem).CompanyName;
                    }
                    else
                    {
                        companyThisYear = cbBusinessThisYearCompany.Text;
                    }
                    string companyLastYear = "";
                    if (cbBusinessLastYearCompany.SelectedItem != null)
                    {
                        companyLastYear = ((Company)cbBusinessLastYearCompany.SelectedItem).CompanyName;
                    }
                    bool isNew = false;
                    if (chbisNew.IsChecked == true)
                    {
                        isNew = true;
                    }
                    else
                    {
                        companyLastYear = cbBusinessLastYearCompany.Text;
                    }
                    string company2YearsAgo = "";
                    if (cbBusiness2YearsAgoCompany.SelectedItem != null)
                    {
                        company2YearsAgo = ((Company)cbBusiness2YearsAgoCompany.SelectedItem).CompanyName;
                    }
                    else
                    {
                        company2YearsAgo = cbBusiness2YearsAgoCompany.Text;
                    }
                    string company3YearsAgo = "";
                    if (cbBusiness3YearsAgoCompany.SelectedItem != null)
                    {
                        company3YearsAgo = ((Company)cbBusiness3YearsAgoCompany.SelectedItem).CompanyName;
                    }
                    else
                    {
                        company3YearsAgo = cbBusiness3YearsAgoCompany.Text;
                    }
                    if (elementaryPolicyToUpdate == null || isAdditon == true || isRenewal == true)
                    {
                        //שליחת נתונים כלליים להוספת פוליסה חדשה
                        policyId = policyLogic.InsertElementaryPolicy(client, isOffer, now, (InsuranceIndustry)cbIndustry.SelectedItem, txtPolicyNumber.Text, addition, (AgentNumber)cbPrincipalAgentNumber.SelectedItem, (AgentNumber)cbSecundaryAgentNumber.SelectedItem, start, end, (InsuranceCompany)cbCompany.SelectedItem, (ElementaryInsuranceType)cbInsuranceType.SelectedItem, ((ComboBoxItem)cbCurrency.SelectedItem).Content.ToString(), paymentMode, paymentNumbers, dpPaymentDate.SelectedDate, premium1, premium2, premium3, premium4, inscriptionFees, policyFees, projectionFees, stampFees, handlingFees, totalPremium, totalFees, totalCredit, chbIsMortgaged.IsChecked, txtBank.Text, txtBranch.Text, txtAddress.Text, txtPhone.Text, txtFax.Text, txtContactName.Text, txtEmail.Text, txtComments.Text, isNew);
                        isPolicyAdded = true;
                        renewalAccepted = true;
                        if (!isOffer)
                        {
                            path = clientDirPath + @"\" + client.ClientID + @"\אלמנטרי\עסק\" + ((InsuranceIndustry)cbIndustry.SelectedItem).InsuranceIndustryName + " " + txtPolicyNumber.Text;
                        }
                        else
                        {
                            path = clientDirPath + @"\" + client.ClientID + @"\הצעות\אלמנטרי\" + ((InsuranceIndustry)cbIndustry.SelectedItem).InsuranceIndustryName + " " + txtPolicyNumber.Text;
                        }

                        //שליחת נתונים להוספת פוליסת עסק חדשה
                        policyLogic.InsertBusinessPolicy(policyId, txtPolicyNumber.Text, addition, txtBusinessOwner.Text, txtBusinessDescription.Text, txtBusinessName.Text, txtBusinessCity.Text, txtBusinessStreet.Text, txtBusinessHouseNumber.Text, txtBusinessAptNumber.Text, txtBusinessZipCode.Text, txtBusinessPhone.Text, txtBusinessSecondPhone.Text, txtBusinessFax.Text, txtBusinessEmail.Text, (StructureType)cbBusinessStructureType.SelectedItem, txtBusinessFloor.Text, txtBusinessTotalFloors.Text, businessSize, txtBusinessRoomsNumber.Text, txtBusinessContactFirstName.Text, txtBusinessContactLastName.Text, txtBusinessContactPhone.Text, txtBusinessContactCellPhone.Text, txtBusinessContactFax.Text, txtBusinessEmail.Text, chbIsBusinessSurveyDone.IsChecked, dpBusinessSurveyDate.SelectedDate, appraiserID, lblBusinessInsuranceThisYear.IsChecked, lblBusinessInsuranceLastYear.IsChecked, lblBusinessInsurance2YearsAgo.IsChecked, lblBusinessInsurance3YearsAgo.IsChecked, claimsThisYear, claimsLastYear, claims2YearsAgo, claims3YearsAgo, companyThisYear, companyLastYear, company2YearsAgo, company3YearsAgo);

                    }
                    else
                    {
                        //שליחת נתונים כלליים עדכון פוליסה קימת
                        policyLogic.UpdateElementaryPolicy(elementaryPolicyToUpdate.ElementaryPolicyID, elementaryPolicyToUpdate.PolicyNumber, elementaryPolicyToUpdate.InsuranceIndustryID, isOffer, txtPolicyNumber.Text, ((InsuranceIndustry)cbIndustry.SelectedItem).InsuranceIndustryID, addition, (AgentNumber)cbPrincipalAgentNumber.SelectedItem, (AgentNumber)cbSecundaryAgentNumber.SelectedItem, start, end, (InsuranceCompany)cbCompany.SelectedItem, (ElementaryInsuranceType)cbInsuranceType.SelectedItem, ((ComboBoxItem)cbCurrency.SelectedItem).Content.ToString(), paymentMode, paymentNumbers, dpPaymentDate.SelectedDate, premium1, premium2, premium3, premium4, inscriptionFees, policyFees, projectionFees, stampFees, handlingFees, totalPremium, totalFees, totalCredit, chbIsMortgaged.IsChecked, txtBank.Text, txtBranch.Text, txtAddress.Text, txtPhone.Text, txtFax.Text, txtContactName.Text, txtEmail.Text, txtComments.Text, isNew);
                        isPolicyAdded = true;
                        if (!isOffer)
                        {
                            if (Directory.Exists(clientDirPath + @"\" + elementaryPolicyToUpdate.ClientID + @"\אלמנטרי\עסק"))
                            {
                                string[] dirs = Directory.GetDirectories(clientDirPath + @"\" + elementaryPolicyToUpdate.ClientID + @"\אלמנטרי\עסק", "*" + elementaryPolicyToUpdate.InsuranceIndustry.InsuranceIndustryName + " " + elementaryPolicyToUpdate.PolicyNumber + "*");
                                if (dirs.Count() > 0)
                                {
                                    path = dirs[0];
                                }
                                else
                                {
                                    dirs = Directory.GetDirectories(clientDirPath + @"\" + elementaryPolicyToUpdate.ClientID + @"\אלמנטרי\עסק", "*" + elementaryPolicyToUpdate.PolicyNumber + "*");
                                    if (dirs.Count() > 0)
                                    {
                                        path = dirs[0];
                                    }
                                }
                            }
                            newPath = clientDirPath + @"\" + elementaryPolicyToUpdate.ClientID + @"\אלמנטרי\עסק\" + ((InsuranceIndustry)cbIndustry.SelectedItem).InsuranceIndustryName + " " + txtPolicyNumber.Text;
                        }
                        else
                        {
                            if (Directory.Exists(clientDirPath + @"\" + elementaryPolicyToUpdate.ClientID + @"\הצעות\אלמנטרי"))
                            {
                                string[] dirs = Directory.GetDirectories(clientDirPath + @"\" + elementaryPolicyToUpdate.ClientID + @"\הצעות\אלמנטרי", "*" + elementaryPolicyToUpdate.InsuranceIndustry.InsuranceIndustryName + " " + elementaryPolicyToUpdate.PolicyNumber + "*");
                                if (dirs.Count() > 0)
                                {
                                    path = dirs[0];
                                }
                                else
                                {
                                    dirs = Directory.GetDirectories(clientDirPath + @"\" + elementaryPolicyToUpdate.ClientID + @"\הצעות\אלמנטרי", "*" + elementaryPolicyToUpdate.PolicyNumber + "*");
                                    if (dirs.Count() > 0)
                                    {
                                        path = dirs[0];
                                    }
                                }
                                newPath = clientDirPath + @"\" + elementaryPolicyToUpdate.ClientID + @"\הצעות\אלמנטרי\" + ((InsuranceIndustry)cbIndustry.SelectedItem).InsuranceIndustryName + " " + txtPolicyNumber.Text;
                            }
                        }

                        //שליחת נתונים עדכון פוליסת עסק קיימת
                        policyLogic.UpdateBusinessPolicy(elementaryPolicyToUpdate.ElementaryPolicyID, txtPolicyNumber.Text, addition, txtBusinessOwner.Text, txtBusinessDescription.Text, txtBusinessName.Text, txtBusinessCity.Text, txtBusinessStreet.Text, txtBusinessHouseNumber.Text, txtBusinessAptNumber.Text, txtBusinessZipCode.Text, txtBusinessPhone.Text, txtBusinessSecondPhone.Text, txtBusinessFax.Text, txtBusinessEmail.Text, (StructureType)cbBusinessStructureType.SelectedItem, txtBusinessFloor.Text, txtBusinessTotalFloors.Text, businessSize, txtBusinessRoomsNumber.Text, txtBusinessContactFirstName.Text, txtBusinessContactLastName.Text, txtBusinessContactPhone.Text, txtBusinessContactCellPhone.Text, txtBusinessContactFax.Text, txtBusinessEmail.Text, chbIsBusinessSurveyDone.IsChecked, dpBusinessSurveyDate.SelectedDate, appraiserID, lblBusinessInsuranceThisYear.IsChecked, lblBusinessInsuranceLastYear.IsChecked, lblBusinessInsurance2YearsAgo.IsChecked, lblBusinessInsurance3YearsAgo.IsChecked, claimsThisYear, claimsLastYear, claims2YearsAgo, claims3YearsAgo, companyThisYear, companyLastYear, company2YearsAgo, company3YearsAgo);
                    }
                }
                //אם מדובר על פוליסת רכב (כולל פוליסת חובה
                else if (((InsuranceIndustry)cbIndustry.SelectedItem).InsuranceIndustryName.Contains("רכב"))

                //else if (((InsuranceIndustry)cbIndustry.SelectedItem).InsuranceIndustryName == "רכב" || ((InsuranceIndustry)cbIndustry.SelectedItem).InsuranceIndustryName == "רכב חובה")
                {
                    txtYear.ClearValue(BorderBrushProperty);
                    txtRegistrationNumber.ClearValue(BorderBrushProperty);
                    txtManufacturer.ClearValue(BorderBrushProperty);
                    //cbCarType.ClearValue(BorderBrushProperty);
                    if (errorList != null && errorList.Count > 0)
                    {
                        foreach (TextBox item in errorList)
                        {
                            item.ClearValue(BorderBrushProperty);
                        }
                    }
                    //שולח לבדיקת שדות חובה
                    if (!CarInputValidation())
                    {
                        tbItemCar.Focus();
                        MessageBox.Show("נא למלאות שדות חובה", "", MessageBoxButton.OK, MessageBoxImage.Error);
                        cancelClose = true;
                        return;
                    }
                    //שולח לבדיקת שדות מספריים
                    errorList = validations.OnlyNumbersValidation(new TextBox[] { txtYear, txtPlacesNumber, txtWeight, txtClaimsThisYear, txtClaimsLastYear, txtClaims2YearsAgo, txtClaims3YearsAgo, txtYoungestDriverAge, txtYoungestDriverSeniority, txtOtherDriversAge, txtOtherDriversSeniority });
                    if (errorList.Count > 0)
                    {
                        tbItemCar.Focus();
                        MessageBox.Show("נא להזין מספרים בלבד", "", MessageBoxButton.OK, MessageBoxImage.Error);
                        cancelClose = true;
                        return;
                    }
                    //המרת שדות טקסטואלים לסוג הנדרש בבסיס הנתונים
                    int? year = null;
                    if (txtYear.Text != "")
                    {
                        year = int.Parse(txtYear.Text);
                    }
                    decimal? weight = null;
                    if (txtWeight.Text != "")
                    {
                        weight = decimal.Parse(txtWeight.Text);
                    }
                    int? places = null;
                    if (txtPlacesNumber.Text != "")
                    {
                        places = int.Parse(txtPlacesNumber.Text);
                    }
                    int? claimsThisYear = null;
                    if (txtClaimsThisYear.Text != "")
                    {
                        claimsThisYear = int.Parse(txtClaimsThisYear.Text);
                    }
                    int? claimsLastYear = null;
                    if (txtClaimsLastYear.Text != "")
                    {
                        claimsLastYear = int.Parse(txtClaimsLastYear.Text);
                    }
                    int? claims2YearsAgo = null;
                    if (txtClaims2YearsAgo.Text != "")
                    {
                        claims2YearsAgo = int.Parse(txtClaims2YearsAgo.Text);
                    }
                    int? claims3YearsAgo = null;
                    if (txtClaims3YearsAgo.Text != "")
                    {
                        claims3YearsAgo = int.Parse(txtClaims3YearsAgo.Text);
                    }
                    string companyThisYear = "";
                    if (cbThisYearCompany.SelectedItem != null)
                    {
                        companyThisYear = ((Company)cbThisYearCompany.SelectedItem).CompanyName;
                    }
                    else
                    {
                        companyThisYear = cbThisYearCompany.Text;
                    }
                    string companyLastYear = "";
                    if (cbLastYearCompany.SelectedItem != null)
                    {
                        companyLastYear = ((Company)cbLastYearCompany.SelectedItem).CompanyName;
                    }
                    else
                    {
                        companyLastYear = cbLastYearCompany.Text;
                    }
                    string company2YearsAgo = "";
                    if (cb2YearsAgoCompany.SelectedItem != null)
                    {
                        company2YearsAgo = ((Company)cb2YearsAgoCompany.SelectedItem).CompanyName;
                    }
                    else
                    {
                        company2YearsAgo = cb2YearsAgoCompany.Text;
                    }
                    string company3YearsAgo = "";
                    if (cb3YearsAgoCompany.SelectedItem != null)
                    {
                        company3YearsAgo = ((Company)cb3YearsAgoCompany.SelectedItem).CompanyName;
                    }
                    else
                    {
                        company3YearsAgo = cb3YearsAgoCompany.Text;
                    }
                    int? youngestDriverAge = null;
                    if (txtYoungestDriverAge.Text != "")
                    {
                        youngestDriverAge = int.Parse(txtYoungestDriverAge.Text);
                    }
                    int? youngestDriverSeniority = null;
                    if (txtYoungestDriverSeniority.Text != "")
                    {
                        youngestDriverSeniority = int.Parse(txtYoungestDriverSeniority.Text);
                    }
                    int? otherDriversAge = null;
                    if (txtOtherDriversAge.Text != "")
                    {
                        otherDriversAge = int.Parse(txtOtherDriversAge.Text);
                    }
                    int? otherDriversSeniority = null;
                    if (txtOtherDriversSeniority.Text != "")
                    {
                        otherDriversSeniority = int.Parse(txtOtherDriversSeniority.Text);
                    }
                    //שולח להוספת פוליסה כללית+פוליסת רכב+נהגים נקובים בשם
                    if (elementaryPolicyToUpdate == null || isAdditon == true || isCarCopy == true || isRenewal == true)
                    {
                        bool isNew = false;
                        if (chbisNew.IsChecked == true)
                        {
                            isNew = true;
                        }
                        policyId = policyLogic.InsertElementaryPolicy(client, isOffer, now, (InsuranceIndustry)cbIndustry.SelectedItem, txtPolicyNumber.Text, addition, (AgentNumber)cbPrincipalAgentNumber.SelectedItem, (AgentNumber)cbSecundaryAgentNumber.SelectedItem, start, end, (InsuranceCompany)cbCompany.SelectedItem, (ElementaryInsuranceType)cbInsuranceType.SelectedItem, ((ComboBoxItem)cbCurrency.SelectedItem).Content.ToString(), paymentMode, paymentNumbers, dpPaymentDate.SelectedDate, premium1, premium2, premium3, premium4, inscriptionFees, policyFees, projectionFees, stampFees, handlingFees, totalPremium, totalFees, totalCredit, chbIsMortgaged.IsChecked, txtBank.Text, txtBranch.Text, txtAddress.Text, txtPhone.Text, txtFax.Text, txtContactName.Text, txtEmail.Text, txtComments.Text, isNew);
                        isPolicyAdded = true;
                        renewalAccepted = true;

                        if (!isOffer)
                        {
                            path = clientDirPath + @"\" + client.ClientID + @"\אלמנטרי\רכב\" + ((InsuranceIndustry)cbIndustry.SelectedItem).InsuranceIndustryName + " " + txtPolicyNumber.Text;
                        }
                        else
                        {
                            path = clientDirPath + @"\" + client.ClientID + @"\הצעות\אלמנטרי\" + ((InsuranceIndustry)cbIndustry.SelectedItem).InsuranceIndustryName + " " + txtPolicyNumber.Text;
                        }

                        policyLogic.InsertCarPolicy(policyId, txtPolicyNumber.Text, addition, (InsuranceIndustry)cbIndustry.SelectedItem, txtRegistrationNumber.Text, txtHobaCertificateNumber.Text, (VehicleType)cbCarType.SelectedItem, txtManufacturer.Text, txtModelCode.Text, year, txtEngine.Text, weight, places, txtProtectionCode.Text, txtChassis.Text, lblInsuranceThisYear.IsChecked, lblInsuranceLastYear.IsChecked, lblInsurance2YearsAgo.IsChecked, lblInsurance3YearsAgo.IsChecked, claimsThisYear, claimsLastYear, claims2YearsAgo, claims3YearsAgo, companyThisYear, companyLastYear, company2YearsAgo, company3YearsAgo, youngestDriverAge, youngestDriverSeniority, otherDriversAge, otherDriversSeniority, (AllowedToDrive)cbAllowedToDrive.SelectedItem, chbDrivesInShabbat.IsChecked);
                        if (declaredDrivers.Count > 0)
                        {
                            driversLogic.InsertDeclaredDriversToPolicy(policyId, txtPolicyNumber.Text, addition, declaredDrivers);
                        }
                    }
                    //שולח לעדכון פוליסה כללית+פוליסת רכב+נהגים נקובים בשם
                    else
                    {
                        bool isNew = false;
                        if (chbisNew.IsChecked == true)
                        {
                            isNew = true;
                        }
                        policyLogic.UpdateElementaryPolicy(elementaryPolicyToUpdate.ElementaryPolicyID, elementaryPolicyToUpdate.PolicyNumber, elementaryPolicyToUpdate.InsuranceIndustryID, isOffer, txtPolicyNumber.Text, ((InsuranceIndustry)cbIndustry.SelectedItem).InsuranceIndustryID, addition, (AgentNumber)cbPrincipalAgentNumber.SelectedItem, (AgentNumber)cbSecundaryAgentNumber.SelectedItem, start, end, (InsuranceCompany)cbCompany.SelectedItem, (ElementaryInsuranceType)cbInsuranceType.SelectedItem, ((ComboBoxItem)cbCurrency.SelectedItem).Content.ToString(), paymentMode, paymentNumbers, dpPaymentDate.SelectedDate, premium1, premium2, premium3, premium4, inscriptionFees, policyFees, projectionFees, stampFees, handlingFees, totalPremium, totalFees, totalCredit, chbIsMortgaged.IsChecked, txtBank.Text, txtBranch.Text, txtAddress.Text, txtPhone.Text, txtFax.Text, txtContactName.Text, txtEmail.Text, txtComments.Text, isNew);
                        isPolicyAdded = true;
                        if (!isOffer)
                        {
                            if (Directory.Exists(clientDirPath + @"\" + elementaryPolicyToUpdate.ClientID + @"\אלמנטרי\רכב"))
                            {
                                string[] dirs = Directory.GetDirectories(clientDirPath + @"\" + elementaryPolicyToUpdate.ClientID + @"\אלמנטרי\רכב", "*" + elementaryPolicyToUpdate.InsuranceIndustry.InsuranceIndustryName + " " + elementaryPolicyToUpdate.PolicyNumber + "*");
                                if (dirs.Count() > 0)
                                {
                                    path = dirs[0];
                                }
                                else
                                {
                                    dirs = Directory.GetDirectories(clientDirPath + @"\" + elementaryPolicyToUpdate.ClientID + @"\אלמנטרי\רכב", "*" + elementaryPolicyToUpdate.PolicyNumber + "*");
                                    if (dirs.Count() > 0)
                                    {
                                        path = dirs[0];
                                    }

                                }
                            }
                            newPath = clientDirPath + @"\" + elementaryPolicyToUpdate.ClientID + @"\אלמנטרי\רכב\" + ((InsuranceIndustry)cbIndustry.SelectedItem).InsuranceIndustryName + " " + txtPolicyNumber.Text;
                        }
                        else
                        {
                            if (Directory.Exists(clientDirPath + @"\" + elementaryPolicyToUpdate.ClientID + @"\הצעות\אלמנטרי"))
                            {
                                string[] dirs = Directory.GetDirectories(clientDirPath + @"\" + elementaryPolicyToUpdate.ClientID + @"\הצעות\אלמנטרי", "*" + elementaryPolicyToUpdate.InsuranceIndustry.InsuranceIndustryName + " " + elementaryPolicyToUpdate.PolicyNumber + "*");
                                if (dirs.Count() > 0)
                                {
                                    path = dirs[0];
                                }
                                else
                                {
                                    dirs = Directory.GetDirectories(clientDirPath + @"\" + elementaryPolicyToUpdate.ClientID + @"\הצעות\אלמנטרי", "*" + elementaryPolicyToUpdate.PolicyNumber + "*");
                                    if (dirs.Count() > 0)
                                    {
                                        path = dirs[0];
                                    }
                                }
                            }
                            newPath = clientDirPath + @"\" + elementaryPolicyToUpdate.ClientID + @"\הצעות\אלמנטרי\" + ((InsuranceIndustry)cbIndustry.SelectedItem).InsuranceIndustryName + " " + txtPolicyNumber.Text;
                        }
                        policyLogic.UpdateCarPolicy(elementaryPolicyToUpdate.ElementaryPolicyID, txtPolicyNumber.Text, (InsuranceIndustry)cbIndustry.SelectedItem, addition, txtRegistrationNumber.Text, txtHobaCertificateNumber.Text, (VehicleType)cbCarType.SelectedItem, txtManufacturer.Text, txtModelCode.Text, year, txtEngine.Text, weight, places, txtProtectionCode.Text, txtChassis.Text, lblInsuranceThisYear.IsChecked, lblInsuranceLastYear.IsChecked, lblInsurance2YearsAgo.IsChecked, lblInsurance3YearsAgo.IsChecked, claimsThisYear, claimsLastYear, claims2YearsAgo, claims3YearsAgo, companyThisYear, companyLastYear, company2YearsAgo, company3YearsAgo, youngestDriverAge, youngestDriverSeniority, otherDriversAge, otherDriversSeniority, (AllowedToDrive)cbAllowedToDrive.SelectedItem, chbDrivesInShabbat.IsChecked);
                        driversLogic.UpdateDeclaredDriversToPolicy(elementaryPolicyToUpdate.ElementaryPolicyID, txtPolicyNumber.Text, addition, declaredDrivers, declaredDrivers);
                    }
                }
                //שולח להוספת מעקבים או כיסוים
                if (elementaryPolicyToUpdate == null || isAdditon == true || isCarCopy == true || isRenewal == true)
                {
                    if (trackings.Count > 0)
                    {
                        trackingLogic.InsertElementaryTrackings(policyId, trackings, addition, txtPolicyNumber.Text, handlerUser.UserID);
                    }
                    if (coveragesToAdd.Count > 0)
                    {
                        coverageLogic.InsertCoveragesToElementaryPolicy(policyId, txtPolicyNumber.Text, addition, coveragesToAdd);
                    }
                    if (chbStandardMoneyCollection.IsChecked == true)
                    {
                        int standardMoneyCollectionId = moneyCollectionLogic.InsertStandardMoneyCollection(standardMoneyCollection, new Insurance() { InsuranceName = "אלמנטרי" }, policyId);
                        if (standardMoneyCollectionTrackings != null)
                        {
                            moneyCollectionLogic.InsertTrackingsToStandardMoneyCollection(standardMoneyCollectionId, standardMoneyCollectionTrackings);
                        }
                    }
                }
                //שולח לעדכון מעקבים
                else
                {
                    trackingLogic.UpdateElementaryTrackings(elementaryPolicyToUpdate.ElementaryPolicyID, trackings, addition, trackings, txtPolicyNumber.Text, handlerUser.UserID);
                    coverageLogic.UpdateCoveragesToElementaryPolicy(elementaryPolicyToUpdate.ElementaryPolicyID, txtPolicyNumber.Text, addition, coveragesToAdd);
                    if (chbStandardMoneyCollection.IsChecked == true)
                    {
                        int standardMoneyCollectionId = moneyCollectionLogic.InsertStandardMoneyCollection(standardMoneyCollection, new Insurance() { InsuranceName = "אלמנטרי" }, elementaryPolicyToUpdate.ElementaryPolicyID);
                        moneyCollectionLogic.InsertTrackingsToStandardMoneyCollection(standardMoneyCollectionId, standardMoneyCollectionTrackings);
                    }
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            if (isPolicyAdded && isAdditon == false) /*&& isCarCopy != true)*/
            {
                if (elementaryPolicyToUpdate == null || isRenewal || isCarCopy)
                {
                    if (!Directory.Exists(@path))
                    {
                        Directory.CreateDirectory(@path);
                    }
                }
                else
                {
                    if (path == null && newPath != null)
                    {
                        Directory.CreateDirectory(newPath);
                    }
                    else if (path != newPath)
                    {
                        try
                        {
                            Directory.Move(path, newPath);
                        }
                        catch (Exception)
                        {
                            System.Windows.Forms.MessageBox.Show("לא ניתן למצוא את נתיב התיקייה");
                        }
                    }
                }
                path = newPath;
            }
            if (sender is Button)
            {
                Button btn = (Button)sender;
                if (btn.Name == "btnSave")
                {
                    confirmBeforeClosing = false;
                    Close();
                }
            }
        }

        //פתיחה לכתיבה של נתוני הסקר כאשר מסומן שהסקר בוצע
        private void chbIsSurveyDone_Checked(object sender, RoutedEventArgs e)
        {
            dpSurveyDate.IsEnabled = true;
            cbSurveyBy.IsEnabled = true;
            btnUpdateSurveyByTable.IsEnabled = true;
        }

        //סגירה לכתיבה של נתוני הסקר כאשר מסומן שהסקר בוצע
        private void chbIsSurveyDone_Unchecked(object sender, RoutedEventArgs e)
        {
            dpSurveyDate.SelectedDate = null;
            dpSurveyDate.IsEnabled = false;
            cbSurveyBy.SelectedItem = null;
            cbSurveyBy.IsEnabled = false;
            btnUpdateSurveyByTable.IsEnabled = false;
        }

        //מדביק את הכתובת של כרטסת הלקוח בפוליסה
        private void chbIsSameAddress_Checked(object sender, RoutedEventArgs e)
        {
            txtCity.Text = client.City;
            txtStreet.Text = client.Street;
            txtHouseNumber.Text = client.HomeNumber;
            txtAptNumber.Text = client.ApartmentNumber;
            txtZipCode.Text = client.ZipCode;
        }

        //מוחק את הכתובת של כרטסת הלקוח מהפוליסה
        private void chbIsSameAddress_Unchecked(object sender, RoutedEventArgs e)
        {
            txtCity.Clear();
            txtStreet.Clear();
            txtHouseNumber.Clear();
            txtAptNumber.Clear();
            txtZipCode.Clear();
        }

        //כניסה לחלון של עדכון טבלת סוגי מבנה
        private void btnUpdateStructureTypeTable_Click(object sender, RoutedEventArgs e)
        {
            string objName = ((Button)sender).Name;
            int? selectedItemId = null;
            ComboBox cb = null;
            if (objName == "btnUpdateStructureTypeTable")
            {
                if (cbStructureType.SelectedItem != null)
                {
                    selectedItemId = ((StructureType)cbStructureType.SelectedItem).StructureTypeID;
                }
                cb = cbStructureType;
            }
            else if (objName == "btnUpdateBusinessStructureTypeTable")
            {
                if (cbBusinessStructureType.SelectedItem != null)
                {
                    selectedItemId = ((StructureType)cbBusinessStructureType.SelectedItem).StructureTypeID;
                }
                cb = cbBusinessStructureType;
            }
            frmUpdateTable updateStructureTypesTable = new frmUpdateTable(new StructureType());
            updateStructureTypesTable.ShowDialog();
            cbStructureTypeBinding();
            if (updateStructureTypesTable.ItemAdded != null)
            {
                SelectStructureItemInCb(((StructureType)updateStructureTypesTable.ItemAdded).StructureTypeID, cb);
            }
            else if (selectedItemId != null)
            {
                SelectStructureItemInCb((int)selectedItemId, cb);
            }

        }

        private void SelectStructureItemInCb(int id, ComboBox cb)
        {
            var items = cb.Items;
            foreach (var item in items)
            {
                StructureType parsedItem = (StructureType)item;
                if (parsedItem.StructureTypeID == id)
                {
                    cb.SelectedItem = item;
                    //cbBusinessStructureType.SelectedItem = item;
                    break;
                }
            }
        }

        //מסנכרן את הקומבו בוקס של סוגי מבנה עם בסיס הנתונים
        private void cbStructureTypeBinding()
        {
            cbStructureType.ItemsSource = structureTypesLogic.GetAllActiveStructures();
            cbStructureType.DisplayMemberPath = "StructureTypeName";
            cbBusinessStructureType.ItemsSource = structureTypesLogic.GetAllActiveStructures();
            cbBusinessStructureType.DisplayMemberPath = "StructureTypeName";
        }

        //פותח את החלון לעדכון שמאים 
        private void btnUpdateSurveyByTable_Click(object sender, RoutedEventArgs e)
        {
            string objName = ((Button)sender).Name;
            int? selectedItemId = null;
            ComboBox cb = null;
            if (objName == "btnUpdateSurveyByTable")
            {
                if (cbSurveyBy.SelectedItem != null)
                {
                    selectedItemId = ((Appraiser)cbSurveyBy.SelectedItem).AppraiserID;
                }
                cb = cbSurveyBy;
            }
            else if (objName == "btnUpdateBusinessSurveyByTable")
            {
                if (cbBusinessSurveyBy.SelectedItem != null)
                {
                    selectedItemId = ((Appraiser)cbBusinessSurveyBy.SelectedItem).AppraiserID;
                }
                cb = cbBusinessSurveyBy;
            }
            frmUpdateTable updateSurveyByTable = new frmUpdateTable(new Appraiser());
            updateSurveyByTable.ShowDialog();
            cbAppraisersBinding();
            if (updateSurveyByTable.ItemAdded != null)
            {
                SelectAppraiserItemInCb(((Appraiser)updateSurveyByTable.ItemAdded).AppraiserID, cb);
            }
            else if (selectedItemId != null)
            {
                SelectAppraiserItemInCb((int)selectedItemId, cb);
            }
            //SelectCbItem(cbInsuranceCompany,);
        }

        private void SelectAppraiserItemInCb(int id, ComboBox cb)
        {
            var items = cb.Items;
            foreach (var item in items)
            {
                Appraiser parsedItem = (Appraiser)item;
                if (parsedItem.AppraiserID == id)
                {
                    cb.SelectedItem = item;
                    break;
                }
            }
        }

        //מסנכרן את הקומבו בוקס של שמאים עם בסיס הנתונים
        private void cbAppraisersBinding()
        {
            cbSurveyBy.ItemsSource = appraiserLogic.GetAllActiveAppraisers();
            cbSurveyBy.DisplayMemberPath = "AppraiserName";
            cbBusinessSurveyBy.ItemsSource = appraiserLogic.GetAllActiveAppraisers();
            cbBusinessSurveyBy.DisplayMemberPath = "AppraiserName";
        }

        //פתיחה לכתיבה של נתוני הסקר כאשר מסומן שהסקר בוצע
        private void chbIsBusinessSurveyDone_Checked(object sender, RoutedEventArgs e)
        {
            dpBusinessSurveyDate.IsEnabled = true;
            cbBusinessSurveyBy.IsEnabled = true;
            btnUpdateBusinessSurveyByTable.IsEnabled = true;
        }

        //סגירה לכתיבה של נתוני הסקר כאשר מסומן שהסקר בוצע
        private void chbIsBusinessSurveyDone_Unchecked(object sender, RoutedEventArgs e)
        {
            dpBusinessSurveyDate.SelectedDate = null;
            dpBusinessSurveyDate.IsEnabled = false;
            cbBusinessSurveyBy.SelectedItem = null;
            cbBusinessSurveyBy.IsEnabled = false;
            btnUpdateBusinessSurveyByTable.IsEnabled = false;
        }

        //מדביק בפוליסה את כתובת הלקוח מהכרטסת 
        private void chbBussinesIsSameAddress_Checked(object sender, RoutedEventArgs e)
        {
            txtBusinessCity.Text = client.City;
            txtBusinessStreet.Text = client.Street;
            txtBusinessHouseNumber.Text = client.HomeNumber;
            txtBusinessAptNumber.Text = client.ApartmentNumber;
            txtBusinessZipCode.Text = client.ZipCode;
        }

        //מוחק מהפוליסה את כתובת הלקוח מהכרטסת 
        private void chbBussinesIsSameAddress_Unchecked(object sender, RoutedEventArgs e)
        {
            txtBusinessCity.Clear();
            txtBusinessStreet.Clear();
            txtBusinessHouseNumber.Clear();
            txtBusinessAptNumber.Clear();
            txtBusinessZipCode.Clear();
        }

        //פותח את חלון עדכון טבלת סוגי רכב
        private void btnUpdateCarTypeTable_Click(object sender, RoutedEventArgs e)
        {
            int? selectedItemId = null;
            if (cbCarType.SelectedItem != null)
            {
                selectedItemId = ((VehicleType)cbCarType.SelectedItem).VehicleTypeID;
            }
            frmUpdateTable updateVehicleTypesTable = new frmUpdateTable(new VehicleType());
            updateVehicleTypesTable.ShowDialog();
            cbVehicleTypeBinding();

            if (updateVehicleTypesTable.ItemAdded != null)
            {
                SelectItemInCb(((VehicleType)updateVehicleTypesTable.ItemAdded).VehicleTypeID);
            }
            else if (selectedItemId != null)
            {
                SelectItemInCb((int)selectedItemId);
            }
            //SelectCbItem(cbInsuranceCompany,);
        }

        private void SelectItemInCb(int id)
        {
            var items = cbCarType.Items;
            foreach (var item in items)
            {
                VehicleType parsedItem = (VehicleType)item;
                if (parsedItem.VehicleTypeID == id)
                {
                    cbCarType.SelectedItem = item;
                    break;
                }
            }
        }

        //פותח את חלון עדכון טבלת נהגים רשאים לנהוג
        private void btnUpdateDriversTable_Click(object sender, RoutedEventArgs e)
        {
            int? selectedItemId = null;
            if (cbAllowedToDrive.SelectedItem != null)
            {
                selectedItemId = ((AllowedToDrive)cbAllowedToDrive.SelectedItem).AllowedToDriveID;
            }
            frmUpdateTable updateAllowedToDriveTable = new frmUpdateTable(new AllowedToDrive());
            updateAllowedToDriveTable.ShowDialog();
            cbAllowedToDriveBinding(); if (updateAllowedToDriveTable.ItemAdded != null)
            {
                SelectDriverItemInCb(((AllowedToDrive)updateAllowedToDriveTable.ItemAdded).AllowedToDriveID);
            }
            else if (selectedItemId != null)
            {
                SelectDriverItemInCb((int)selectedItemId);
            }
            //SelectCbItem(cbInsuranceCompany,);
        }

        private void SelectDriverItemInCb(int id)
        {
            var items = cbAllowedToDrive.Items;
            foreach (var item in items)
            {
                AllowedToDrive parsedItem = (AllowedToDrive)item;
                if (parsedItem.AllowedToDriveID == id)
                {
                    cbAllowedToDrive.SelectedItem = item;
                    break;
                }
            }
        }
        //פותח את החלון להוספת נהג נקוב בשם
        private void btnAddDriverByName_Click(object sender, RoutedEventArgs e)
        {
            frmAddDriverByName addDriverWindow = new frmAddDriverByName();
            addDriverWindow.ShowDialog();
            if (addDriverWindow.newDriver != null)
            {
                declaredDrivers.Add(addDriverWindow.newDriver);
                dgDriversByNameBinding();
            }
        }

        //מסנכרן את טבלת הנהגים הנקובים בשם
        private void dgDriversByNameBinding()
        {
            dgDriversByName.ItemsSource = declaredDrivers.OrderBy(d => d.DriverLastName).ThenBy(d => d.DriverFirstName);
            lv.AutoSizeColumns(dgDriversByName.View);
        }

        //פותח את החלון לעדכון נהג נקוב בשם
        private void btnUpdateDriverByName_Click(object sender, RoutedEventArgs e)
        {
            if (dgDriversByName.SelectedItem == null)
            {
                MessageBox.Show("נא לסמן נהג בטבלה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            frmAddDriverByName updateDriverWindow = new frmAddDriverByName((DeclaredDriver)dgDriversByName.SelectedItem);
            updateDriverWindow.ShowDialog();
            for (int i = 0; i < declaredDrivers.Count; i++)
            {
                if (declaredDrivers[i].DriverIdNumber == updateDriverWindow.driverToUpdate.DriverIdNumber)
                {
                    declaredDrivers[i] = updateDriverWindow.driverToUpdate;
                    break;
                }
            }
            dgDriversByNameBinding();
        }

        //מוחק נהג נקוב בשם מהטבלה
        private void btnDeleteDriverByName_Click(object sender, RoutedEventArgs e)
        {
            if (dgDriversByName.SelectedItem == null)
            {
                MessageBox.Show("נא לסמן נהג בטבלה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            if (MessageBox.Show("למחוק נהג נקוב בשם", "אישור", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                declaredDrivers.Remove((DeclaredDriver)dgDriversByName.SelectedItem);
                dgDriversByNameBinding();
            }
            else
            {
                dgDriversByName.UnselectAll();
            }
        }

        private void dgDriversByName_MouseDown(object sender, MouseButtonEventArgs e)
        {
            dgDriversByName.UnselectAll();
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        //פותח את החלון להוספת מעקב
        private void btnNewTracking_Click(object sender, RoutedEventArgs e)
        {
            frmTracking newTrackingWindow = new frmTracking(client, elementaryPolicyToUpdate, handlerUser);
            newTrackingWindow.ShowDialog();
            if (newTrackingWindow.trackingContent != null)
            {
                trackings.Add(new TrackingsViewModel() { TrackingContent = newTrackingWindow.trackingContent, TrackingDate = DateTime.Now, User = handlerUser });
                dgTrackingsBinding();
            }
        }

        //מסנכרן את טבלת המעקבים
        private void dgTrackingsBinding()
        {
            dgTrackings.ItemsSource = trackings.OrderByDescending(t => t.TrackingDate);
            lv.AutoSizeColumns(dgTrackings.View);
        }

        private void dgTrackings_MouseDown(object sender, MouseButtonEventArgs e)
        {
            dgTrackings.UnselectAll();
        }

        //מוחק מעקב מהטבלה
        private void btnDeleteTracking_Click(object sender, RoutedEventArgs e)
        {
            if (dgTrackings.SelectedItem == null)
            {
                MessageBox.Show("נא לסמן מעקב בטבלה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            if (MessageBox.Show("למחוק מעקב", "אישור", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                trackings.Remove((TrackingsViewModel)dgTrackings.SelectedItem);
                dgTrackingsBinding();
            }
            else
            {
                dgTrackings.UnselectAll();
            }
        }

        //פותח את החלון לעדכון מעקב
        private void btnUpdateTracking_Click(object sender, RoutedEventArgs e)
        {
            if (dgTrackings.SelectedItem == null)
            {
                MessageBox.Show("נא לסמן מעקב בטבלה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            TrackingsViewModel trackingToUpdate = (TrackingsViewModel)dgTrackings.SelectedItem;
            frmTracking updateTrackingWindow = new frmTracking(trackingToUpdate, client, elementaryPolicyToUpdate, handlerUser);
            updateTrackingWindow.ShowDialog();
            trackingToUpdate.TrackingContent = updateTrackingWindow.trackingContent;
            dgTrackingsBinding();
            dgTrackings.UnselectAll();
        }

        //פותח את החלון לעדכון מעקב
        private void dgTrackings_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            DependencyObject src = (DependencyObject)(e.OriginalSource);
            while (!(src is Control))
            {
                if (!(src is Visual))
                {
                    break;
                }
                src = VisualTreeHelper.GetParent(src);
            }

            if ((src is Control && src.GetType() == typeof(ListViewItem)) || src.GetType() == typeof(Run))
            {
                if (dgTrackings.SelectedItem != null)
                {
                    TrackingsViewModel trackingToUpdate = (TrackingsViewModel)dgTrackings.SelectedItem;
                    frmTracking updateTrackingWindow = new frmTracking(trackingToUpdate, client, elementaryPolicyToUpdate, handlerUser);
                    updateTrackingWindow.ShowDialog();
                    trackingToUpdate.TrackingContent = updateTrackingWindow.trackingContent;
                    dgTrackingsBinding();
                    dgTrackings.UnselectAll();
                }
            }
        }

        private void btnNewCoverage_Click(object sender, RoutedEventArgs e)
        {
            frmCoverage newCoverageWindow = new frmCoverage((ElementaryInsuranceType)cbInsuranceType.SelectedItem);
            newCoverageWindow.ShowDialog();
            if (newCoverageWindow.NewCoverage != null)
            {
                coveragesToAdd.Add(newCoverageWindow.NewCoverage);
            }
            dgCoveragesBinding();
        }

        private void dgCoveragesBinding()
        {
            if (coveragesToAdd.Count > 0)
            {
                dgCoverages.ItemsSource = coveragesToAdd.ToList();
                lv.AutoSizeColumns(dgCoverages.View);
                //if (elementaryPolicyToUpdate!=null)
                //{
                //    var dgItems = dgCoverages.Items;
                //    foreach (var item in dgItems)
                //    {
                //        ElementaryCoverage coverage = (ElementaryCoverage)item;
                //        if (coverage.Date < dpStartDate.SelectedDate)
                //        {

                //            DataGridRow row = (DataGridRow)item;
                //            row.IsEnabled = false;
                //        }
                //    } 
                //}
            }
        }

        private void dgCoverages_MouseDown(object sender, MouseButtonEventArgs e)
        {
            dgCoverages.UnselectAll();
        }

        private void btnDeleteCoverage_Click(object sender, RoutedEventArgs e)
        {
            if (dgCoverages.SelectedItem == null)
            {
                MessageBox.Show("נא לסמן כיסוי בטבלה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            if (MessageBox.Show("למחוק כיסוי", "אישור", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                coveragesToAdd.Remove((ElementaryCoverage)dgCoverages.SelectedItem);
                dgCoveragesBinding();
            }
            else
            {
                dgCoverages.UnselectAll();
            }
        }

        private void btnUpdateCoverage_Click(object sender, RoutedEventArgs e)
        {
            if (dgCoverages.SelectedItem == null)
            {
                MessageBox.Show("נא לסמן כיסוי בטבלה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            ElementaryCoverage coverageToUpdate = (ElementaryCoverage)dgCoverages.SelectedItem;
            frmCoverage updateCoverageWindow = new frmCoverage((ElementaryInsuranceType)cbInsuranceType.SelectedItem, coverageToUpdate);
            updateCoverageWindow.ShowDialog();
            coverageToUpdate.ElementaryCoverageType = updateCoverageWindow.NewCoverage.ElementaryCoverageType;
            coverageToUpdate.ElementaryCoverageTypeID = updateCoverageWindow.NewCoverage.ElementaryCoverageTypeID;
            coverageToUpdate.CoverageAmount = updateCoverageWindow.NewCoverage.CoverageAmount;
            coverageToUpdate.Porcentage = updateCoverageWindow.NewCoverage.Porcentage;
            coverageToUpdate.premium = updateCoverageWindow.NewCoverage.premium;
            coverageToUpdate.Comments = updateCoverageWindow.NewCoverage.Comments;
            dgCoveragesBinding();
            dgCoverages.UnselectAll();
        }

        private void dgCoverages_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            DependencyObject src = (DependencyObject)(e.OriginalSource);
            while (!(src is Control))
            {
                if (!(src is Visual))
                {
                    break;
                }
                src = VisualTreeHelper.GetParent(src);
            }

            if ((src is Control && src.GetType() == typeof(ListViewItem)) || src.GetType() == typeof(Run))
            {
                if (dgCoverages.SelectedItem != null)
                {
                    ElementaryCoverage coverageToUpdate = (ElementaryCoverage)dgCoverages.SelectedItem;
                    frmCoverage updateCoverageWindow = new frmCoverage((ElementaryInsuranceType)cbInsuranceType.SelectedItem, coverageToUpdate);
                    updateCoverageWindow.ShowDialog();
                    coverageToUpdate.ElementaryCoverageType = updateCoverageWindow.NewCoverage.ElementaryCoverageType;
                    coverageToUpdate.ElementaryCoverageTypeID = updateCoverageWindow.NewCoverage.ElementaryCoverageTypeID;
                    coverageToUpdate.CoverageAmount = updateCoverageWindow.NewCoverage.CoverageAmount;
                    coverageToUpdate.Porcentage = updateCoverageWindow.NewCoverage.Porcentage;
                    coverageToUpdate.premium = updateCoverageWindow.NewCoverage.premium;
                    coverageToUpdate.Comments = updateCoverageWindow.NewCoverage.Comments;
                    dgCoveragesBinding();
                    dgCoverages.UnselectAll();
                }
            }
        }

        private void txtTotalPremium_TextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox txtBox = (TextBox)sender;
            txtBox.ClearValue(BorderBrushProperty);
            if (txtBox.Text != "")
            {
                try
                {
                    decimalAmount = validations.ConvertStringToNullableDecimal(txtBox.Text);
                }
                catch (Exception ex)
                {
                    txtBox.BorderBrush = System.Windows.Media.Brushes.Red;
                    System.Windows.Forms.MessageBox.Show(ex.Message);
                    txtBox.Text = "";
                }
            }
        }

        private void txtPolicyNumber_GotFocus(object sender, RoutedEventArgs e)
        {
            isChanges = true;
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (confirmBeforeClosing && isChanges)
            {
                if (MessageBox.Show("האם ברצונך לשמור שינוים שבצעת", "אישור", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    btnSave_Click(sender, new RoutedEventArgs());
                    if (cancelClose)
                    {
                        e.Cancel = true;
                        cancelClose = false;
                    }
                }
            }
        }

        private void chbStandardMoneyCollection_Checked(object sender, RoutedEventArgs e)
        {
            btnStandardMoneyCollectionDetails.IsEnabled = true;
        }

        private void chbStandardMoneyCollection_Unchecked(object sender, RoutedEventArgs e)
        {
            btnStandardMoneyCollectionDetails.IsEnabled = false;
        }

        private void btnStandardMoneyCollectionDetails_Click(object sender, RoutedEventArgs e)
        {
            frmStandardMoneyCollectionDetails window = null;
            if (standardMoneyCollection == null)
            {
                window = new frmStandardMoneyCollectionDetails(false, handlerUser);
            }
            else
            {
                window = new frmStandardMoneyCollectionDetails(standardMoneyCollection, standardMoneyCollectionTrackings, false, handlerUser);
            }
            window.ShowDialog();
            if (window.StandardMoneyCollection != null)
            {
                standardMoneyCollection = window.StandardMoneyCollection;
            }
            if (window.Trackings != null)
            {
                standardMoneyCollectionTrackings = window.Trackings;
            }
        }
    }
}
