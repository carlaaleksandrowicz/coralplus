﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Outlook = Microsoft.Office.Interop.Outlook;


namespace CoralUI
{
    public class Email
    {
        public void SendEmailFromOutlook(string subject, string body, string[] attachments, string[] recipients)
        {
            try
            {
                Outlook.Application oApp = new Outlook.Application();
                Outlook.MailItem oMsg = (Outlook.MailItem)oApp.CreateItem(Outlook.OlItemType.olMailItem);

                oMsg.Subject = subject;
                oMsg.BodyFormat = Outlook.OlBodyFormat.olFormatHTML;
                //oMsg.HTMLBody = body; //Here comes your body;

                if (attachments != null)
                {
                    foreach (var item in attachments)
                    {
                        if (item != "")
                        {
                            oMsg.Attachments.Add(item, Outlook.OlAttachmentType.olByValue, Type.Missing, Type.Missing);
                        }
                    }
                }
                if (recipients != null)
                {
                    foreach (var item in recipients)
                    {
                        if (item != "")
                        {
                            oMsg.Recipients.Add(item);
                        }
                    }
                }
                oMsg.Display(false); //In order to display it in modal inspector change the argument to true
                if (oMsg != null)
                {

                    oMsg.HTMLBody = body + oMsg.HTMLBody; //Here comes your body;

                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        //public static void GenerateEmail(string emailTo, string ccTo, string subject, string body)
        //{
        //    var objOutlook = new Application();
        //    var mailItem = (MailItem)(objOutlook.CreateItem(OlItemType.olMailItem));
        //    mailItem.To = emailTo;
        //    mailItem.CC = ccTo;
        //    mailItem.Subject = subject;
        //    mailItem.Display(mailItem);
        //    mailItem.HTMLBody = body + mailItem.HTMLBody;
        //}


        private void SendEmailFromCoral()
        {
            Outlook.Application outlookApp = new Outlook.Application();

            Outlook.MailItem mail = outlookApp.CreateItem(Outlook.OlItemType.olMailItem) as Outlook.MailItem;
            mail.Subject = "Test";
            Outlook.AddressEntry currentUser = outlookApp.Session.CurrentUser.AddressEntry;
            if (currentUser.Type == "EX")
            {
                Outlook.ExchangeUser manager = currentUser.GetExchangeUser().GetExchangeUserManager();
                // Add recipient using display name, alias, or smtp address
                mail.Recipients.Add("cdukesb@gmail.com");
                mail.Recipients.ResolveAll();
                mail.Attachments.Add(@"C:\Users\karla.DOMAIN\Documents\Bugs.xlsx", Outlook.OlAttachmentType.olByValue, Type.Missing, Type.Missing);
                mail.Send();
            }
        }
    }
}
