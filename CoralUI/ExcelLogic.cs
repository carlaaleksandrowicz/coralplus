﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Runtime.InteropServices;
using System.Windows;
using CoralBusinessLogics;

namespace CoralUI
{
    class ExcelLogic
    {

        public void AllPoliciesToExcel(List<object> data)
        {
            // Create the `DataTable` structure according to your data source
            DataTable table = new DataTable();
            table.Columns.Add("ביטוח", typeof(string));
            table.Columns.Add("שם משפחה", typeof(string));
            table.Columns.Add("שם פרטי", typeof(string));
            table.Columns.Add("תעודת זהות", typeof(string));
            table.Columns.Add("מס' פוליסה", typeof(string));
            table.Columns.Add("ח' ביטוח", typeof(string));
            table.Columns.Add("ת. תחילה", typeof(DateTime));
            table.Columns.Add("ת. סיום", typeof(DateTime));
            table.Columns.Add("סוג ביטוח", typeof(string));
            table.Columns.Add("ענף ", typeof(string));
            table.Columns.Add("מס' רישוי", typeof(string));
            table.Columns.Add("יצרן", typeof(string));
            table.Columns.Add("כתובת", typeof(string));
            table.Columns.Add("פרמיה", typeof(decimal));
            table.Columns.Add("משועבד ", typeof(bool));

            // Iterate through data source object and fill the table
            foreach (var obj in data)
            {
                string carNumber = "";
                string manufacturer = "";
                string address = "";
                if (obj is ElementaryPolicy)
                {
                    ElementaryPolicy item = (ElementaryPolicy)obj;

                    if (item.CarPolicy != null)
                    {
                        carNumber = item.CarPolicy.RegistrationNumber;
                        manufacturer = item.CarPolicy.Manufacturer;
                    }

                    if (item.ApartmentPolicy != null)
                    {
                        address = string.Format("{0}, {1} {2}", item.ApartmentPolicy.City, item.ApartmentPolicy.Street, item.ApartmentPolicy.HomeNumber);
                    }
                    else if (item.BusinessPolicy != null)
                    {
                        address = string.Format("{0}, {1} {2}", item.BusinessPolicy.City, item.BusinessPolicy.Street, item.BusinessPolicy.HomeNumber);
                    }

                    table.Rows.Add(
                        item.Insurance.InsuranceName,
                        item.Client.FirstName,
                        item.Client.LastName,
                        item.Client.IdNumber,
                        item.PolicyNumber,
                        item.Company.CompanyName,
                        item.StartDate,
                        item.EndDate,
                        item.ElementaryInsuranceType.ElementaryInsuranceTypeName,
                        item.InsuranceIndustry.InsuranceIndustryName,
                        carNumber,
                        manufacturer,
                        address,
                        item.TotalPremium,
                        item.IsMortgaged);
                }
                else if (obj is PersonalAccidentsPolicy)
                {
                    PersonalAccidentsPolicy item = (PersonalAccidentsPolicy)obj;

                    table.Rows.Add(
                        item.Insurance.InsuranceName,
                        item.Client.FirstName,
                        item.Client.LastName,
                        item.Client.IdNumber,
                        item.PolicyNumber,
                        item.Company.CompanyName,
                        item.StartDate,
                        item.EndDate,
                        "",
                        "",
                        carNumber,
                        manufacturer,
                        address,
                        item.TotalPremium,
                        false);
                }
                else if (obj is TravelPolicy)
                {
                    TravelPolicy item = (TravelPolicy)obj;

                    table.Rows.Add(
                        item.Insurance.InsuranceName,
                        item.Client.FirstName,
                        item.Client.LastName,
                        item.Client.IdNumber,
                        item.PolicyNumber,
                        item.Company.CompanyName,
                        item.StartDate,
                        item.EndDate,
                        "",
                        "",
                        carNumber,
                        manufacturer,
                        address,
                        item.Premium,
                        false);
                }
                else if (obj is LifePolicy)
                {
                    LifePolicy item = (LifePolicy)obj;

                    table.Rows.Add(
                        item.Insurance.InsuranceName,
                        item.Client.FirstName,
                        item.Client.LastName,
                        item.Client.IdNumber,
                        item.PolicyNumber,
                        item.Company.CompanyName,
                        item.StartDate,
                        item.EndDate,
                        item.FundType.FundTypeName,
                        item.LifeIndustry.LifeIndustryName,
                        carNumber,
                        manufacturer,
                        address,
                        item.TotalMonthlyPayment,
                        item.IsMortgaged);
                }
                else if (obj is HealthPolicy)
                {
                    HealthPolicy item = (HealthPolicy)obj;

                    table.Rows.Add(
                        item.Insurance.InsuranceName,
                        item.Client.FirstName,
                        item.Client.LastName,
                        item.Client.IdNumber,
                        item.PolicyNumber,
                        item.Company.CompanyName,
                        item.StartDate,
                        item.EndDate,
                        item.HealthInsuranceType.HealthInsuranceTypeName,
                        "",
                        carNumber,
                        manufacturer,
                        address,
                        item.TotalMonthlyPayment,
                        item.IsMortgaged);
                }
                else if (obj is ForeingWorkersPolicy)
                {
                    ForeingWorkersPolicy item = (ForeingWorkersPolicy)obj;

                    table.Rows.Add(
                        item.Insurance.InsuranceName,
                        item.Client.FirstName,
                        item.Client.LastName,
                        item.Client.IdNumber,
                        item.PolicyNumber,
                        item.Company.CompanyName,
                        item.StartDate,
                        item.EndDate,
                        item.ForeingWorkersInsuranceType.ForeingWorkersInsuranceTypeName,
                        item.ForeingWorkersIndustry.ForeingWorkersIndustryName,
                        carNumber,
                        manufacturer,
                        address,
                        item.TotalPremium,
                        false);
                }
                else if (obj is FinancePolicy)
                {
                    FinancePolicy item = (FinancePolicy)obj;

                    table.Rows.Add(
                        item.Insurance.InsuranceName,
                        item.Client.FirstName,
                        item.Client.LastName,
                        item.Client.IdNumber,
                        item.AssociateNumber,
                        item.Company.CompanyName,
                        item.StartDate,
                        item.EndDate,
                        item.FinanceProgram.ProgramName,
                        item.FinanceProgramType.ProgramTypeName,
                        carNumber,
                        manufacturer,
                        address,
                        0,
                        false);
                }
            }

            CreateExcelSheet(table);
        }

        public void AllClaimsToExcel(List<object> data)
        {
            // Create the `DataTable` structure according to your data source
            DataTable table = new DataTable();
            table.Columns.Add("ביטוח", typeof(string));
            table.Columns.Add("שם פרטי", typeof(string));
            table.Columns.Add("שם משפחה", typeof(string));
            table.Columns.Add("תעודת זהות", typeof(string));
            table.Columns.Add("מצב תביעה", typeof(string));
            table.Columns.Add("סוג תביעה", typeof(string));
            table.Columns.Add("ת. מקרה", typeof(DateTime));
            table.Columns.Add("ח' ביטוח", typeof(string));
            table.Columns.Add("ענף", typeof(string));
            table.Columns.Add("מס' פוליסה", typeof(string));
            table.Columns.Add("מס' רישוי", typeof(string));
            table.Columns.Add("מס' תביעה", typeof(string));
            table.Columns.Add("מס' תביעה צד ג", typeof(string));
            table.Columns.Add("סטטוס", typeof(bool));

            // Iterate through data source object and fill the table
            foreach (var obj in data)
            {
                string carNumber = "";
                string claimCondition = "";
                string claimType = "";

                if (obj is ElementaryClaim)
                {
                    ElementaryClaim item = (ElementaryClaim)obj;
                    if (item.ElementaryPolicy.CarPolicy != null)
                    {
                        carNumber = item.ElementaryPolicy.CarPolicy.RegistrationNumber;
                    }
                    if (item.ElementaryClaimCondition != null)
                    {
                        claimCondition = item.ElementaryClaimCondition.Description;
                    }
                    if (item.ElementaryClaimType != null)
                    {
                        claimType = item.ElementaryClaimType.ClaimTypeName;
                    }
                    table.Rows.Add(
                    item.ElementaryPolicy.Insurance.InsuranceName,
                    item.ElementaryPolicy.Client.FirstName,
                    item.ElementaryPolicy.Client.LastName,
                    item.ElementaryPolicy.Client.IdNumber,
                    claimCondition,
                    claimType,
                    item.EventDate,
                    item.ElementaryPolicy.Company.CompanyName,
                    item.ElementaryPolicy.InsuranceIndustry.InsuranceIndustryName,
                    item.ElementaryPolicy.PolicyNumber,
                    carNumber,
                    item.ClaimNumber,
                    item.ThirdPartyClaimNumber,
                    item.ClaimStatus);
                }
                else if (obj is PersonalAccidentsClaim)
                {
                    PersonalAccidentsClaim item = (PersonalAccidentsClaim)obj;

                    if (item.PersonalAccidentsClaimCondition != null)
                    {
                        claimCondition = item.PersonalAccidentsClaimCondition.Description;
                    }
                    if (item.PersonalAccidentsClaimType != null)
                    {
                        claimType = item.PersonalAccidentsClaimType.ClaimTypeName;
                    }
                    table.Rows.Add(
                    item.PersonalAccidentsPolicy.Insurance.InsuranceName,
                    item.PersonalAccidentsPolicy.Client.FirstName,
                    item.PersonalAccidentsPolicy.Client.LastName,
                    item.PersonalAccidentsPolicy.Client.IdNumber,
                    claimCondition,
                    claimType,
                    item.EventDateAndTime,
                    item.PersonalAccidentsPolicy.Company.CompanyName,
                    "",
                    item.PersonalAccidentsPolicy.PolicyNumber,
                    carNumber,
                    item.ClaimNumber,
                    "",
                    item.ClaimStatus);
                }
                else if (obj is TravelClaim)
                {
                    TravelClaim item = (TravelClaim)obj;

                    if (item.TravelClaimCondition != null)
                    {
                        claimCondition = item.TravelClaimCondition.Description;
                    }
                    if (item.TravelClaimType != null)
                    {
                        claimType = item.TravelClaimType.ClaimTypeName;
                    }
                    table.Rows.Add(
                    item.TravelPolicy.Insurance.InsuranceName,
                    item.TravelPolicy.Client.FirstName,
                    item.TravelPolicy.Client.LastName,
                    item.TravelPolicy.Client.IdNumber,
                    claimCondition,
                    claimType,
                    item.EventDateAndTime,
                    item.TravelPolicy.Company.CompanyName,
                    "",
                    item.TravelPolicy.PolicyNumber,
                    carNumber,
                    item.ClaimNumber,
                    "",
                    item.ClaimStatus);
                }
                else if (obj is LifeClaim)
                {
                    LifeClaim item = (LifeClaim)obj;

                    if (item.LifeClaimCondition != null)
                    {
                        claimCondition = item.LifeClaimCondition.Description;
                    }
                    if (item.LifeClaimType != null)
                    {
                        claimType = item.LifeClaimType.ClaimTypeName;
                    }
                    table.Rows.Add(
                    item.LifePolicy.Insurance.InsuranceName,
                    item.LifePolicy.Client.FirstName,
                    item.LifePolicy.Client.LastName,
                    item.LifePolicy.Client.IdNumber,
                    claimCondition,
                    claimType,
                    item.EventDateAndTime,
                    item.LifePolicy.Company.CompanyName,
                    item.LifePolicy.LifeIndustry.LifeIndustryName,
                    item.LifePolicy.PolicyNumber,
                    carNumber,
                    item.ClaimNumber,
                    "",
                    item.ClaimStatus);
                }
                else if (obj is HealthClaim)
                {
                    HealthClaim item = (HealthClaim)obj;

                    if (item.HealthClaimCondition != null)
                    {
                        claimCondition = item.HealthClaimCondition.Description;
                    }
                    if (item.HealthClaimType != null)
                    {
                        claimType = item.HealthClaimType.ClaimTypeName;
                    }
                    table.Rows.Add(
                    item.HealthPolicy.Insurance.InsuranceName,
                    item.HealthPolicy.Client.FirstName,
                    item.HealthPolicy.Client.LastName,
                    item.HealthPolicy.Client.IdNumber,
                    claimCondition,
                    claimType,
                    item.EventDateAndTime,
                    item.HealthPolicy.Company.CompanyName,
                    "",
                    item.HealthPolicy.PolicyNumber,
                    carNumber,
                    item.ClaimNumber,
                    "",
                    item.ClaimStatus);
                }
                else if (obj is ForeingWorkersClaim)
                {
                    ForeingWorkersClaim item = (ForeingWorkersClaim)obj;

                    if (item.ForeingWorkersClaimCondition != null)
                    {
                        claimCondition = item.ForeingWorkersClaimCondition.Description;
                    }
                    if (item.ForeingWorkersClaimType != null)
                    {
                        claimType = item.ForeingWorkersClaimType.ClaimTypeName;
                    }
                    table.Rows.Add(
                    item.ForeingWorkersPolicy.Insurance.InsuranceName,
                    item.ForeingWorkersPolicy.Client.FirstName,
                    item.ForeingWorkersPolicy.Client.LastName,
                    item.ForeingWorkersPolicy.Client.IdNumber,
                    claimCondition,
                    claimType,
                    item.EventDateAndTime,
                    item.ForeingWorkersPolicy.Company.CompanyName,
                    item.ForeingWorkersPolicy.ForeingWorkersIndustry.ForeingWorkersIndustryName,
                    item.ForeingWorkersPolicy.PolicyNumber,
                    carNumber,
                    item.ClaimNumber,
                    "",
                    item.ClaimStatus);
                }
            }

            CreateExcelSheet(table);
        }

        public void ElementaryPoliciesToExcel(List<ElementaryPolicy> data)
        {
            // Create the `DataTable` structure according to your data source
            DataTable table = new DataTable();
            table.Columns.Add("שם פרטי", typeof(string));
            table.Columns.Add("שם משפחה", typeof(string));
            table.Columns.Add("תעודת זהות", typeof(string));
            table.Columns.Add("מס' פוליסה", typeof(string));
            table.Columns.Add("תוספת", typeof(int));
            table.Columns.Add("ח' ביטוח", typeof(string));
            table.Columns.Add("ת. תחילה", typeof(DateTime));
            table.Columns.Add("ת. סיום", typeof(DateTime));
            table.Columns.Add("סוג ביטוח", typeof(string));
            table.Columns.Add("ענף ", typeof(string));
            table.Columns.Add("מס' רישוי", typeof(string));
            table.Columns.Add("יצרן", typeof(string));
            table.Columns.Add("כתובת", typeof(string));
            table.Columns.Add("פרמיה", typeof(decimal));
            table.Columns.Add("משועבד ", typeof(bool));

            // Iterate through data source object and fill the table
            foreach (var item in data)
            {
                string carNumber = "";
                string manufacturer = "";
                if (item.CarPolicy != null)
                {
                    carNumber = item.CarPolicy.RegistrationNumber;
                    manufacturer = item.CarPolicy.Manufacturer;
                }
                string address = "";
                if (item.ApartmentPolicy != null)
                {
                    address = string.Format("{0}, {1} {2}", item.ApartmentPolicy.City, item.ApartmentPolicy.Street, item.ApartmentPolicy.HomeNumber);
                }
                else if (item.BusinessPolicy != null)
                {
                    address = string.Format("{0}, {1} {2}", item.BusinessPolicy.City, item.BusinessPolicy.Street, item.BusinessPolicy.HomeNumber);
                }
                table.Rows.Add(
                    item.Client.FirstName,
                    item.Client.LastName,
                    item.Client.IdNumber,
                    item.PolicyNumber,
                    item.Addition,
                    item.Company.CompanyName,
                    item.StartDate,
                    item.EndDate,
                    item.ElementaryInsuranceType.ElementaryInsuranceTypeName,
                    item.InsuranceIndustry.InsuranceIndustryName,
                    carNumber,
                    manufacturer,
                    address,
                    item.TotalPremium,
                    item.IsMortgaged);
            }

            CreateExcelSheet(table);
        }

        public void ElementaryClaimsToExcel(List<ElementaryClaim> data)
        {
            // Create the `DataTable` structure according to your data source
            DataTable table = new DataTable();
            table.Columns.Add("שם פרטי", typeof(string));
            table.Columns.Add("שם משפחה", typeof(string));
            table.Columns.Add("תעודת זהות", typeof(string));
            table.Columns.Add("מצב תביעה", typeof(string));
            table.Columns.Add("סוג תביעה", typeof(string));
            table.Columns.Add("ת. מקרה", typeof(DateTime));
            table.Columns.Add("ח' ביטוח", typeof(string));
            table.Columns.Add("ענף", typeof(string));
            table.Columns.Add("מס' פוליסה", typeof(string));
            table.Columns.Add("מס' רישוי", typeof(string));
            table.Columns.Add("מס' תביעה", typeof(string));
            table.Columns.Add("מס' תביעה צד ג", typeof(string));
            table.Columns.Add("סטטוס", typeof(bool));


            // Iterate through data source object and fill the table
            foreach (var item in data)
            {
                string carNumber = "";
                if (item.ElementaryPolicy.CarPolicy != null)
                {
                    carNumber = item.ElementaryPolicy.CarPolicy.RegistrationNumber;
                }
                string claimCondition = "";
                if (item.ElementaryClaimCondition != null)
                {
                    claimCondition = item.ElementaryClaimCondition.Description;
                }
                string claimType = "";
                if (item.ElementaryClaimType != null)
                {
                    claimType = item.ElementaryClaimType.ClaimTypeName;
                }
                table.Rows.Add(
                    item.ElementaryPolicy.Client.FirstName,
                    item.ElementaryPolicy.Client.LastName,
                    item.ElementaryPolicy.Client.IdNumber,
                    claimCondition,
                    claimType,
                    item.EventDate,
                    item.ElementaryPolicy.Company.CompanyName,
                    item.ElementaryPolicy.InsuranceIndustry.InsuranceIndustryName,
                    item.ElementaryPolicy.PolicyNumber,
                    carNumber,
                    item.ClaimNumber,
                    item.ThirdPartyClaimNumber,
                    item.ClaimStatus);
            }

            CreateExcelSheet(table);
        }

        public void PersonalAccidentsPoliciesToExcel(List<PersonalAccidentsPolicy> data)
        {
            // Create the `DataTable` structure according to your data source
            DataTable table = new DataTable();
            table.Columns.Add("שם פרטי", typeof(string));
            table.Columns.Add("שם משפחה", typeof(string));
            table.Columns.Add("תעודת זהות", typeof(string));
            table.Columns.Add("מס' פוליסה", typeof(string));
            table.Columns.Add("תוספת", typeof(int));
            table.Columns.Add("ח' ביטוח", typeof(string));
            table.Columns.Add("ת. תחילה", typeof(DateTime));
            table.Columns.Add("ת. סיום", typeof(DateTime));
            table.Columns.Add("פרמיה", typeof(decimal));

            // Iterate through data source object and fill the table
            foreach (var item in data)
            {
                table.Rows.Add(
                    item.Client.FirstName,
                    item.Client.LastName,
                    item.Client.IdNumber,
                    item.PolicyNumber,
                    item.Addition,
                    item.Company.CompanyName,
                    item.StartDate,
                    item.EndDate,
                    item.TotalPremium);
            }

            CreateExcelSheet(table);
        }

        public void PersonalAccidentsClaimsToExcel(List<PersonalAccidentsClaim> data)
        {
            // Create the `DataTable` structure according to your data source
            DataTable table = new DataTable();
            table.Columns.Add("שם פרטי", typeof(string));
            table.Columns.Add("שם משפחה", typeof(string));
            table.Columns.Add("תעודת זהות", typeof(string));
            table.Columns.Add("מצב תביעה", typeof(string));
            table.Columns.Add("סוג תביעה", typeof(string));
            table.Columns.Add("ת. מקרה", typeof(DateTime));
            table.Columns.Add("ח' ביטוח", typeof(string));
            table.Columns.Add("מס' פוליסה", typeof(string));
            table.Columns.Add("מס' תביעה", typeof(string));
            table.Columns.Add("סטטוס", typeof(bool));


            // Iterate through data source object and fill the table
            foreach (var item in data)
            {
                string claimCondition = "";
                if (item.PersonalAccidentsClaimCondition != null)
                {
                    claimCondition = item.PersonalAccidentsClaimCondition.Description;
                }
                string claimType = "";
                if (item.PersonalAccidentsClaimType != null)
                {
                    claimType = item.PersonalAccidentsClaimType.ClaimTypeName;
                }
                table.Rows.Add(
                    item.PersonalAccidentsPolicy.Client.FirstName,
                    item.PersonalAccidentsPolicy.Client.LastName,
                    item.PersonalAccidentsPolicy.Client.IdNumber,
                    claimCondition,
                    claimType,
                    item.EventDateAndTime,
                    item.PersonalAccidentsPolicy.Company.CompanyName,
                    item.PersonalAccidentsPolicy.PolicyNumber,
                    item.ClaimNumber,
                    item.ClaimStatus);
            }
            CreateExcelSheet(table);
        }

        public void TravelClaimsToExcel(List<TravelClaim> data)
        {
            // Create the `DataTable` structure according to your data source
            DataTable table = new DataTable();
            table.Columns.Add("שם פרטי", typeof(string));
            table.Columns.Add("שם משפחה", typeof(string));
            table.Columns.Add("תעודת זהות", typeof(string));
            table.Columns.Add("מצב תביעה", typeof(string));
            table.Columns.Add("סוג תביעה", typeof(string));
            table.Columns.Add("ת. מקרה", typeof(DateTime));
            table.Columns.Add("ח' ביטוח", typeof(string));
            table.Columns.Add("מס' פוליסה", typeof(string));
            table.Columns.Add("מס' תביעה", typeof(string));
            table.Columns.Add("סטטוס", typeof(bool));


            // Iterate through data source object and fill the table
            foreach (var item in data)
            {
                string claimCondition = "";
                if (item.TravelClaimCondition != null)
                {
                    claimCondition = item.TravelClaimCondition.Description;
                }
                string claimType = "";
                if (item.TravelClaimType != null)
                {
                    claimType = item.TravelClaimType.ClaimTypeName;
                }
                table.Rows.Add(
                    item.TravelPolicy.Client.FirstName,
                    item.TravelPolicy.Client.LastName,
                    item.TravelPolicy.Client.IdNumber,
                    claimCondition,
                    claimType,
                    item.EventDateAndTime,
                    item.TravelPolicy.Company.CompanyName,
                    item.TravelPolicy.PolicyNumber,
                    item.ClaimNumber,
                    item.ClaimStatus);
            }
            CreateExcelSheet(table);
        }

        public void LifeClaimsToExcel(List<LifeClaim> data)
        {
            // Create the `DataTable` structure according to your data source
            DataTable table = new DataTable();
            table.Columns.Add("שם פרטי", typeof(string));
            table.Columns.Add("שם משפחה", typeof(string));
            table.Columns.Add("תעודת זהות", typeof(string));
            table.Columns.Add("מצב תביעה", typeof(string));
            table.Columns.Add("סוג תביעה", typeof(string));
            table.Columns.Add("ת. מקרה", typeof(DateTime));
            table.Columns.Add("ח' ביטוח", typeof(string));
            table.Columns.Add("ענף", typeof(string));
            table.Columns.Add("מס' פוליסה", typeof(string));
            table.Columns.Add("מס' תביעה", typeof(string));
            table.Columns.Add("סטטוס", typeof(bool));

            // Iterate through data source object and fill the table
            foreach (var item in data)
            {
                string claimCondition = "";
                if (item.LifeClaimCondition != null)
                {
                    claimCondition = item.LifeClaimCondition.Description;
                }
                string claimType = "";
                if (item.LifeClaimType != null)
                {
                    claimType = item.LifeClaimType.ClaimTypeName;
                }
                table.Rows.Add(
                    item.LifePolicy.Client.FirstName,
                    item.LifePolicy.Client.LastName,
                    item.LifePolicy.Client.IdNumber,
                    claimCondition,
                    claimType,
                    item.EventDateAndTime,
                    item.LifePolicy.Company.CompanyName,
                    item.LifePolicy.LifeIndustry.LifeIndustryName,
                    item.LifePolicy.PolicyNumber,
                    item.ClaimNumber,
                    item.ClaimStatus);
            }
            CreateExcelSheet(table);
        }

        public void HealthClaimsToExcel(List<HealthClaim> data)
        {
            // Create the `DataTable` structure according to your data source
            DataTable table = new DataTable();
            table.Columns.Add("שם פרטי", typeof(string));
            table.Columns.Add("שם משפחה", typeof(string));
            table.Columns.Add("תעודת זהות", typeof(string));
            table.Columns.Add("מצב תביעה", typeof(string));
            table.Columns.Add("סוג תביעה", typeof(string));
            table.Columns.Add("ת. מקרה", typeof(DateTime));
            table.Columns.Add("ח' ביטוח", typeof(string));
            table.Columns.Add("מס' פוליסה", typeof(string));
            table.Columns.Add("מס' תביעה", typeof(string));
            table.Columns.Add("סטטוס", typeof(bool));


            // Iterate through data source object and fill the table
            foreach (var item in data)
            {
                string claimCondition = "";
                if (item.HealthClaimCondition != null)
                {
                    claimCondition = item.HealthClaimCondition.Description;
                }
                string claimType = "";
                if (item.HealthClaimType != null)
                {
                    claimType = item.HealthClaimType.ClaimTypeName;
                }
                table.Rows.Add(
                    item.HealthPolicy.Client.FirstName,
                    item.HealthPolicy.Client.LastName,
                    item.HealthPolicy.Client.IdNumber,
                    claimCondition,
                    claimType,
                    item.EventDateAndTime,
                    item.HealthPolicy.Company.CompanyName,
                    item.HealthPolicy.PolicyNumber,
                    item.ClaimNumber,
                    item.ClaimStatus);
            }
            CreateExcelSheet(table);
        }

        public void ForeingWorkersClaimsToExcel(List<ForeingWorkersClaim> data)
        {
            // Create the `DataTable` structure according to your data source
            DataTable table = new DataTable();
            table.Columns.Add("שם פרטי", typeof(string));
            table.Columns.Add("שם משפחה", typeof(string));
            table.Columns.Add("תעודת זהות", typeof(string));
            table.Columns.Add("מצב תביעה", typeof(string));
            table.Columns.Add("סוג תביעה", typeof(string));
            table.Columns.Add("ת. מקרה", typeof(DateTime));
            table.Columns.Add("ח' ביטוח", typeof(string));
            table.Columns.Add("מס' פוליסה", typeof(string));
            table.Columns.Add("מס' תביעה", typeof(string));
            table.Columns.Add("סטטוס", typeof(bool));


            // Iterate through data source object and fill the table
            foreach (var item in data)
            {
                string claimCondition = "";
                if (item.ForeingWorkersClaimCondition != null)
                {
                    claimCondition = item.ForeingWorkersClaimCondition.Description;
                }
                string claimType = "";
                if (item.ForeingWorkersClaimType != null)
                {
                    claimType = item.ForeingWorkersClaimType.ClaimTypeName;
                }
                table.Rows.Add(
                    item.ForeingWorkersPolicy.Client.FirstName,
                    item.ForeingWorkersPolicy.Client.LastName,
                    item.ForeingWorkersPolicy.Client.IdNumber,
                    claimCondition,
                    claimType,
                    item.EventDateAndTime,
                    item.ForeingWorkersPolicy.Company.CompanyName,
                    item.ForeingWorkersPolicy.PolicyNumber,
                    item.ClaimNumber,
                    item.ClaimStatus);
            }
            CreateExcelSheet(table);
        }

        public void TravelPoliciesToExcel(List<TravelPolicy> data)
        {
            // Create the `DataTable` structure according to your data source
            DataTable table = new DataTable();
            table.Columns.Add("שם פרטי", typeof(string));
            table.Columns.Add("שם משפחה", typeof(string));
            table.Columns.Add("תעודת זהות", typeof(string));
            table.Columns.Add("מס' פוליסה", typeof(string));
            table.Columns.Add("תוספת", typeof(int));
            table.Columns.Add("ח' ביטוח", typeof(string));
            table.Columns.Add("ת. תחילה", typeof(DateTime));
            table.Columns.Add("ת. סיום", typeof(DateTime));
            table.Columns.Add("מדינת יעד", typeof(string));
            table.Columns.Add("פרמיה", typeof(decimal));

            // Iterate through data source object and fill the table
            foreach (var item in data)
            {
                string country = "";
                if (item.Country != null)
                {
                    country = item.Country.CountryName;
                }
                table.Rows.Add(
                    item.Client.FirstName,
                    item.Client.LastName,
                    item.Client.IdNumber,
                    item.PolicyNumber,
                    item.Addition,
                    item.Company.CompanyName,
                    item.StartDate,
                    item.EndDate,
                    country,
                    item.Premium);
            }

            CreateExcelSheet(table);
        }

        public void LifePoliciesToExcel(List<LifePolicy> data)
        {
            // Create the `DataTable` structure according to your data source
            DataTable table = new DataTable();
            table.Columns.Add("שם פרטי", typeof(string));
            table.Columns.Add("שם משפחה", typeof(string));
            table.Columns.Add("תעודת זהות", typeof(string));
            table.Columns.Add("מס' פוליסה", typeof(string));
            table.Columns.Add("תוספת", typeof(int));
            table.Columns.Add("ח' ביטוח", typeof(string));
            table.Columns.Add("ת. תחילה", typeof(DateTime));
            table.Columns.Add("ת. סיום", typeof(DateTime));
            table.Columns.Add("סוג ביטוח", typeof(string));
            table.Columns.Add("ענף ", typeof(string));
            table.Columns.Add("פרמיה", typeof(decimal));
            table.Columns.Add("משועבד ", typeof(bool));


            // Iterate through data source object and fill the table
            foreach (var item in data)
            {
                table.Rows.Add(
                    item.Client.FirstName,
                    item.Client.LastName,
                    item.Client.IdNumber,
                    item.PolicyNumber,
                    item.Addition,
                    item.Company.CompanyName,
                    item.StartDate,
                    item.EndDate,
                    item.FundType.FundTypeName,
                    item.LifeIndustry.LifeIndustryName,
                    item.TotalMonthlyPayment,
                    item.IsMortgaged);
            }

            CreateExcelSheet(table);
        }

        public void HealthPoliciesToExcel(List<HealthPolicy> data)
        {
            // Create the `DataTable` structure according to your data source
            DataTable table = new DataTable();
            table.Columns.Add("שם פרטי", typeof(string));
            table.Columns.Add("שם משפחה", typeof(string));
            table.Columns.Add("תעודת זהות", typeof(string));
            table.Columns.Add("מס' פוליסה", typeof(string));
            table.Columns.Add("תוספת", typeof(int));
            table.Columns.Add("ח' ביטוח", typeof(string));
            table.Columns.Add("ת. תחילה", typeof(DateTime));
            table.Columns.Add("ת. סיום", typeof(DateTime));
            table.Columns.Add("סוג ביטוח", typeof(string));
            table.Columns.Add("פרמיה", typeof(decimal));
            table.Columns.Add("משועבד ", typeof(bool));


            // Iterate through data source object and fill the table
            foreach (var item in data)
            {
                table.Rows.Add(
                    item.Client.FirstName,
                    item.Client.LastName,
                    item.Client.IdNumber,
                    item.PolicyNumber,
                    item.Addition,
                    item.Company.CompanyName,
                    item.StartDate,
                    item.EndDate,
                    item.HealthInsuranceType.HealthInsuranceTypeName,
                    item.TotalMonthlyPayment,
                    item.IsMortgaged);
            }

            CreateExcelSheet(table);
        }

        public void ForeingWorkersPoliciesToExcel(List<ForeingWorkersPolicy> data)
        {
            // Create the `DataTable` structure according to your data source
            DataTable table = new DataTable();
            table.Columns.Add("שם פרטי", typeof(string));
            table.Columns.Add("שם משפחה", typeof(string));
            table.Columns.Add("תעודת זהות", typeof(string));
            table.Columns.Add("מס' פוליסה", typeof(string));
            table.Columns.Add("תוספת", typeof(int));
            table.Columns.Add("ח' ביטוח", typeof(string));
            table.Columns.Add("ת. תחילה", typeof(DateTime));
            table.Columns.Add("ת. סיום", typeof(DateTime));
            table.Columns.Add("סוג ביטוח", typeof(string));
            table.Columns.Add("ענף", typeof(string));
            table.Columns.Add("שם מבוטח", typeof(string));
            table.Columns.Add("פרמיה", typeof(decimal));

            // Iterate through data source object and fill the table
            foreach (var item in data)
            {
                string insuredName = item.InsuredFirstName + " " + item.InsuredLastName;
                table.Rows.Add(
                    item.Client.FirstName,
                    item.Client.LastName,
                    item.Client.IdNumber,
                    item.PolicyNumber,
                    item.Addition,
                    item.Company.CompanyName,
                    item.StartDate,
                    item.EndDate,
                    item.ForeingWorkersInsuranceType.ForeingWorkersInsuranceTypeName,
                    item.ForeingWorkersIndustry.ForeingWorkersIndustryName,
                    insuredName,
                    item.TotalPremium);
            }

            CreateExcelSheet(table);
        }

        public void FinancePoliciesToExcel(List<FinancePolicy> data)
        {
            // Create the `DataTable` structure according to your data source
            DataTable table = new DataTable();
            table.Columns.Add("שם פרטי", typeof(string));
            table.Columns.Add("שם משפחה", typeof(string));
            table.Columns.Add("תעודת זהות", typeof(string));
            table.Columns.Add("מס' עמית", typeof(string));
            table.Columns.Add("גוף מנהל", typeof(string));
            table.Columns.Add("ת. תחילה", typeof(DateTime));
            table.Columns.Add("ת. סיום", typeof(DateTime));
            table.Columns.Add("שם תכנית", typeof(string));
            table.Columns.Add("סוג תכנית", typeof(string));

            // Iterate through data source object and fill the table
            foreach (var item in data)
            {
                table.Rows.Add(
                    item.Client.FirstName,
                    item.Client.LastName,
                    item.Client.IdNumber,
                    item.AssociateNumber,
                    item.Company.CompanyName,
                    item.StartDate,
                    item.EndDate,
                    item.FinanceProgram.ProgramName,
                    item.FinanceProgramType.ProgramTypeName);
            }

            CreateExcelSheet(table);
        }

        public void TasksToExcel(List<Request> data)
        {
            // Create the `DataTable` structure according to your data source
            DataTable table = new DataTable();
            table.Columns.Add("שם לקוח", typeof(string));
            table.Columns.Add("תאריך פנייה", typeof(DateTime));
            table.Columns.Add("תאריך יעד", typeof(DateTime));
            table.Columns.Add("עובד מטפל", typeof(string));
            table.Columns.Add("פתוחה", typeof(bool));
            table.Columns.Add("קטגוריה", typeof(string));
            table.Columns.Add("מצב המשימה", typeof(string));
            table.Columns.Add("שם הפונה", typeof(string));
            table.Columns.Add("נייד הפונה", typeof(string));
            table.Columns.Add("נושא הפנייה", typeof(string));


            // Iterate through data source object and fill the table
            foreach (var item in data)
            {
                string clientName = "";
                if (item.Client != null)
                {
                    clientName = item.Client.FirstName + " " + item.Client.LastName;
                }
                string category = "";
                if (item.RequestCategory != null)
                {
                    category = item.RequestCategory.RequestCategoryName;
                }
                string condition = "";
                if (item.RequestCondition != null)
                {
                    condition = item.RequestCondition.RequestConditionName;
                }
                string requesterName = item.RequesterFirstName + " " + item.RequesterLastName;

                table.Rows.Add(
                    clientName,
                    item.RequesteDate,
                    item.RequestDueDate,
                    item.User1.Usermame,
                    item.RequestStatus,
                    category,
                    condition,
                    requesterName,
                    item.RequesterCellPhone,
                    item.RequestSubject);
            }

            CreateExcelSheet(table);
        }

        public void ClientsToExcel(List<Client> data)
        {
            // Create the `DataTable` structure according to your data source
            DataTable table = new DataTable();
            table.Columns.Add("שם פרטי", typeof(string));
            table.Columns.Add("שם משפחה", typeof(string));
            table.Columns.Add("תעודת זהות", typeof(string));
            table.Columns.Add("תאריך לידה", typeof(DateTime));
            table.Columns.Add("ישוב", typeof(string));
            table.Columns.Add("רחוב", typeof(string));
            table.Columns.Add("מס' בית", typeof(string));
            table.Columns.Add("מס' דירה", typeof(string));
            table.Columns.Add("טלפון בית", typeof(string));
            table.Columns.Add("טלפון נייד", typeof(string));
            table.Columns.Add("טלפון עבודה", typeof(string));
            table.Columns.Add("פקס", typeof(string));
            table.Columns.Add("דוא''ל", typeof(string));
            table.Columns.Add("מס' לקוח", typeof(string));
            table.Columns.Add("סוכן משנה", typeof(string));
            table.Columns.Add("סוכן ראשי", typeof(string));

            // Iterate through data source object and fill the table
            foreach (var item in data)
            {
                string secundaryAgent = "";
                if (item.Agent1 != null)
                {
                    secundaryAgent = item.Agent1.FirstName + " " + item.Agent1.LastName;
                }
                string principalAgent = "";
                if (item.Agent != null)
                {
                    principalAgent = item.Agent.FirstName + " " + item.Agent.LastName;
                }
                table.Rows.Add(
                    item.FirstName,
                    item.LastName,
                    item.IdNumber,
                    item.BirthDate,
                    item.City,
                    item.Street,
                    item.HomeNumber,
                    item.ApartmentNumber,
                    item.PhoneHome,
                    item.CellPhone,
                    item.PhoneWork,
                    item.Fax,
                    item.Email,
                    item.ClientNumber,
                    secundaryAgent,
                    principalAgent
                    );
            }

            CreateExcelSheet(table);
        }
        public void ContactsToExcel(List<Contact> data)
        {
            // Create the `DataTable` structure according to your data source
            DataTable table = new DataTable();
            table.Columns.Add("שם משפחה", typeof(string));
            table.Columns.Add("שם פרטי", typeof(string));
            table.Columns.Add("קטגוריה", typeof(string));
            table.Columns.Add("שם העסק", typeof(string));
            table.Columns.Add("ישוב", typeof(string));
            table.Columns.Add("רחוב", typeof(string));
            table.Columns.Add("מס' בית", typeof(string));
            table.Columns.Add("טלפון ", typeof(string));
            table.Columns.Add("נייד", typeof(string));
            table.Columns.Add("פקס", typeof(string));
            table.Columns.Add("דוא''ל", typeof(string));
            table.Columns.Add("אתר אינטרנט", typeof(string));
            table.Columns.Add("הערות", typeof(string));

            // Iterate through data source object and fill the table
            foreach (var item in data)
            {
                string category = "";
                if (item.ContactCategory != null)
                {
                    category = item.ContactCategory.ContactCategoryName;
                }
                table.Rows.Add(
                    item.LastName,
                    item.FirstName,
                    category,
                    item.CompanyName,
                    item.City,
                    item.Street,
                    item.HomeNumber,
                    item.Phone,
                    item.CellPhone,
                    item.Fax,
                    item.Email,
                    item.Website,
                    item.Remarks
                    );
            }

            CreateExcelSheet(table);
        }

        public void StandardMoneyCollectionToExcel(List<StandardMoneyCollection> data)
        {
            // Create the `DataTable` structure according to your data source
            DataTable table = new DataTable();
            table.Columns.Add("שם פרטי", typeof(string));
            table.Columns.Add("שם משפחה", typeof(string));
            table.Columns.Add("תעודת זהות", typeof(string));
            table.Columns.Add("נייד", typeof(string));
            table.Columns.Add("טלפון", typeof(string));
            table.Columns.Add("דוא''ל", typeof(string));
            table.Columns.Add("מס' פוליסה", typeof(string));
            table.Columns.Add("ח' ביטוח", typeof(string));
            table.Columns.Add("ת. תחילה", typeof(DateTime));
            table.Columns.Add("ת. סיום", typeof(DateTime));
            table.Columns.Add("ענף ", typeof(string));
            table.Columns.Add("פרמיה", typeof(decimal));
            table.Columns.Add("סוג גבייה ", typeof(string));
            table.Columns.Add("סכום גבייה", typeof(decimal));
            table.Columns.Add("פתוח", typeof(bool));
            table.Columns.Add("הערות ", typeof(string));

            // Iterate through data source object and fill the table
            foreach (var item in data)
            {
                string firstName = "";
                string lastName = "";
                string idNumber = "";
                string cellphone = "";
                string phone = "";
                string email = "";
                string policyNumber = "";
                string company = "";
                DateTime? starDate = null;
                DateTime? endDate = null;
                string industry = "";
                decimal? premium = null;

                if (item.ElementaryPolicy != null)
                {
                    firstName = item.ElementaryPolicy.Client.FirstName;
                    lastName = item.ElementaryPolicy.Client.LastName;
                    idNumber = item.ElementaryPolicy.Client.IdNumber;
                    cellphone = item.ElementaryPolicy.Client.CellPhone;
                    phone = item.ElementaryPolicy.Client.PhoneHome;
                    email = item.ElementaryPolicy.Client.Email;
                    policyNumber = item.ElementaryPolicy.PolicyNumber;
                    company = item.ElementaryPolicy.Company.CompanyName;
                    starDate = item.ElementaryPolicy.StartDate;
                    endDate = item.ElementaryPolicy.EndDate;
                    industry = item.ElementaryPolicy.InsuranceIndustry.InsuranceIndustryName;
                    premium = item.ElementaryPolicy.TotalPremium;
                }
                else if (item.PersonalAccidentsPolicy != null)
                {
                    firstName = item.PersonalAccidentsPolicy.Client.FirstName;
                    lastName = item.PersonalAccidentsPolicy.Client.LastName;
                    idNumber = item.PersonalAccidentsPolicy.Client.IdNumber;
                    cellphone = item.PersonalAccidentsPolicy.Client.CellPhone;
                    phone = item.PersonalAccidentsPolicy.Client.PhoneHome;
                    email = item.PersonalAccidentsPolicy.Client.Email;
                    policyNumber = item.PersonalAccidentsPolicy.PolicyNumber;
                    company = item.PersonalAccidentsPolicy.Company.CompanyName;
                    starDate = item.PersonalAccidentsPolicy.StartDate;
                    endDate = item.PersonalAccidentsPolicy.EndDate;
                    premium = item.PersonalAccidentsPolicy.TotalPremium;
                }
                else if (item.TravelPolicy != null)
                {
                    firstName = item.TravelPolicy.Client.FirstName;
                    lastName = item.TravelPolicy.Client.LastName;
                    idNumber = item.TravelPolicy.Client.IdNumber;
                    cellphone = item.TravelPolicy.Client.CellPhone;
                    phone = item.TravelPolicy.Client.PhoneHome;
                    email = item.TravelPolicy.Client.Email;
                    policyNumber = item.TravelPolicy.PolicyNumber;
                    company = item.TravelPolicy.Company.CompanyName;
                    starDate = item.TravelPolicy.StartDate;
                    endDate = item.TravelPolicy.EndDate;
                    premium = item.TravelPolicy.Premium;
                }
                else if (item.LifePolicy != null)
                {
                    firstName = item.LifePolicy.Client.FirstName;
                    lastName = item.LifePolicy.Client.LastName;
                    idNumber = item.LifePolicy.Client.IdNumber;
                    cellphone = item.LifePolicy.Client.CellPhone;
                    phone = item.LifePolicy.Client.PhoneHome;
                    email = item.LifePolicy.Client.Email;
                    policyNumber = item.LifePolicy.PolicyNumber;
                    company = item.LifePolicy.Company.CompanyName;
                    starDate = item.LifePolicy.StartDate;
                    endDate = item.LifePolicy.EndDate;
                    industry = item.LifePolicy.LifeIndustry.LifeIndustryName;
                    premium = item.LifePolicy.TotalMonthlyPayment;
                }
                else if (item.HealthPolicy != null)
                {
                    firstName = item.HealthPolicy.Client.FirstName;
                    lastName = item.HealthPolicy.Client.LastName;
                    idNumber = item.HealthPolicy.Client.IdNumber;
                    cellphone = item.HealthPolicy.Client.CellPhone;
                    phone = item.HealthPolicy.Client.PhoneHome;
                    email = item.HealthPolicy.Client.Email;
                    policyNumber = item.HealthPolicy.PolicyNumber;
                    company = item.HealthPolicy.Company.CompanyName;
                    starDate = item.HealthPolicy.StartDate;
                    endDate = item.HealthPolicy.EndDate;
                    premium = item.HealthPolicy.TotalMonthlyPayment;
                }
                else if (item.FinancePolicy != null)
                {
                    firstName = item.FinancePolicy.Client.FirstName;
                    lastName = item.FinancePolicy.Client.LastName;
                    idNumber = item.FinancePolicy.Client.IdNumber;
                    cellphone = item.FinancePolicy.Client.CellPhone;
                    phone = item.FinancePolicy.Client.PhoneHome;
                    email = item.FinancePolicy.Client.Email;
                    policyNumber = item.FinancePolicy.AssociateNumber;
                    company = item.FinancePolicy.Company.CompanyName;
                    starDate = item.FinancePolicy.StartDate;
                    endDate = item.FinancePolicy.EndDate;
                    industry = item.FinancePolicy.FinanceProgramType.ProgramTypeName;
                }
                else if (item.ForeingWorkersPolicy != null)
                {
                    firstName = item.ForeingWorkersPolicy.Client.FirstName;
                    lastName = item.ForeingWorkersPolicy.Client.LastName;
                    idNumber = item.ForeingWorkersPolicy.Client.IdNumber;
                    cellphone = item.ForeingWorkersPolicy.Client.CellPhone;
                    phone = item.ForeingWorkersPolicy.Client.PhoneHome;
                    email = item.ForeingWorkersPolicy.Client.Email;
                    policyNumber = item.ForeingWorkersPolicy.PolicyNumber;
                    company = item.ForeingWorkersPolicy.Company.CompanyName;
                    starDate = item.ForeingWorkersPolicy.StartDate;
                    endDate = item.ForeingWorkersPolicy.EndDate;
                    industry = item.ForeingWorkersPolicy.ForeingWorkersIndustry.ForeingWorkersIndustryName;
                    premium = item.ForeingWorkersPolicy.TotalPremium;
                }
                decimal? amount = null;
                if (item.MoneyCollectionAmount!=null)
                {
                    amount = item.MoneyCollectionAmount;
                }
                string moneyCollectionTypeName = "";
                if (item.MoneyCollectionType!=null)
                {
                    moneyCollectionTypeName = item.MoneyCollectionType.MoneyCollectionTypeName;
                }
                table.Rows.Add(firstName, lastName, idNumber, cellphone, phone, email, policyNumber, company, starDate, endDate, industry, premium, moneyCollectionTypeName, amount, item.IsStandardMoneyCollectionOpen, item.Comments);
            }
            CreateExcelSheet(table);
        }



        public void CreateExcelSheet(DataTable dt)
        {
            Microsoft.Office.Interop.Excel.Application excel = null;
            Microsoft.Office.Interop.Excel.Workbook wb = null;

            object missing = Type.Missing;
            Microsoft.Office.Interop.Excel.Worksheet ws = null;
            Microsoft.Office.Interop.Excel.Range rng = null;

            try
            {
                excel = new Microsoft.Office.Interop.Excel.Application();
                wb = excel.Workbooks.Add();
                ws = (Microsoft.Office.Interop.Excel.Worksheet)wb.ActiveSheet;
                ws.DisplayRightToLeft = true;

                for (int Idx = 0; Idx < dt.Columns.Count; Idx++)
                {
                    ws.Range["A1"].Offset[0, Idx].Value = dt.Columns[Idx].ColumnName;
                }

                for (int Idx = 0; Idx < dt.Rows.Count; Idx++)
                {
                    ws.Range["A2"].Offset[Idx].Resize[1, dt.Columns.Count].Value =
                    dt.Rows[Idx].ItemArray;
                }

                excel.Visible = true;
                wb.Activate();
            }
            catch (COMException ex)
            {
                MessageBox.Show("Error accessing Excel: " + ex.ToString());
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.ToString());
            }
        }



    }
}
