﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using CoralBusinessLogics;
using System.IO;
using System.Windows.Media;
using System.Windows.Documents;

namespace CoralUI
{
    /// <summary>
    /// Interaction logic for PersonalAccidents.xaml
    /// </summary>
    public partial class frmPersonalAccidents : Window
    {
        bool isMedicalCostsOpen = false;
        bool isOthersOpen = false;
        private Client client;
        private User user;
        string clientDirPath = null;
        InsurancesLogic insuranceLogic = new InsurancesLogic();
        AgentsLogic agentLogic = new AgentsLogic();
        InputsValidations validations = new InputsValidations();
        List<object> nullErrorList = null;
        PersonalAccidentsLogic personalAccidentsLogic = new PersonalAccidentsLogic();
        int policyID;
        string personalAccidentsPath = null;
        private string path;
        private bool isAddition;
        private PersonalAccidentsPolicy personalAccidentsPolicy = null;
        private string newPath;
        List<PersonalAccidentsAdditionalInsured> insureds = new List<PersonalAccidentsAdditionalInsured>();
        ListViewSettings lv = new ListViewSettings();
        private bool isPartnerEnabled;
        private bool isChildrenEnabled;
        PersonalAccidentsInsuranceTypesLogic personalAccidentInsuranceLogic = new PersonalAccidentsInsuranceTypesLogic();
        PersonalAccidentsInsurancesDetail principalInsuranceDetails = null;
        PersonalAccidentsInsurancesDetail partnerInsuranceDetails = null;
        PersonalAccidentsInsurancesDetail childrenInsuranceDetails = null;
        List<PersonalAccidentsCoverage> coverages = new List<PersonalAccidentsCoverage>();
        CoveragesLogic coveragesLogic = new CoveragesLogic();
        List<PersonalAccidentsTracking> trackings = new List<PersonalAccidentsTracking>();
        TrackingsLogics trackingLogics = new TrackingsLogics();
        bool isOffer;
        private bool isChanges = false;
        bool confirmBeforeClosing = true;
        bool cancelClose = false;
        StandardMoneyCollection standardMoneyCollection;
        StandardMoneyCollectionLogic moneyCollectionLogic = new StandardMoneyCollectionLogic();
        private List<StandardMoneyCollectionTracking> standardMoneyCollectionTrackings;


        public frmPersonalAccidents()
        {
            InitializeComponent();
        }

        public frmPersonalAccidents(Client clientSelected, User user, bool isOffer)
        {
            InitializeComponent();
            this.client = clientSelected;
            this.user = user;
            this.isOffer = isOffer;
        }

        public frmPersonalAccidents(Client client, User user, bool isAddition, PersonalAccidentsPolicy personalAccidentsPolicy)
        {
            InitializeComponent();
            this.client = client;
            this.user = user;
            this.isAddition = isAddition;
            this.personalAccidentsPolicy = personalAccidentsPolicy;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            string strAppDir = System.AppDomain.CurrentDomain.BaseDirectory;
            string path = strAppDir + @"Coral Files\ClientsPath.txt";

            if (File.Exists(path))
            {
                clientDirPath = File.ReadAllText(path);
            }
            else
            {
                MessageBox.Show("לא נמצא תיקיית הלקוחות. נא בדוק בהגדרות ראשוניות את נתיב התיקייה");
                Close();
                return;
            }
            personalAccidentsPath = clientDirPath + @"\" + client.ClientID;
            tbItemCoverages.Visibility = Visibility.Collapsed;
            cbCompanyBinding();
            cbInsuredTypeBinding();
            if (client != null)
            {
                string clientDetails;
                if (client.CellPhone != null && client.CellPhone != "")
                {
                    clientDetails = string.Format("{0} {1}  ת.ז: {2} נייד: {3}", client.FirstName, client.LastName, client.IdNumber, client.CellPhone);
                }
                else
                {
                    clientDetails = string.Format("{0} {1}  ת.ז: {2}", client.FirstName, client.LastName, client.IdNumber);
                }
                lblClientNameAndId.Content = clientDetails;
                txtRelativeLastName.Text = client.LastName;
            }
            if (personalAccidentsPolicy == null)
            {
                txtAddition.Text = "0";
                txtAddition.IsEnabled = false;
                DateTime now = DateTime.Now;
                dpOpenDate.SelectedDate = now;
                dpStartDate.SelectedDate = now;
            }
            else
            {
                dpOpenDate.SelectedDate = personalAccidentsPolicy.OpenDate;
                txtPolicyNumber.Text = personalAccidentsPolicy.PolicyNumber;
                dpStartDate.SelectedDate = personalAccidentsPolicy.StartDate;
                dpEndDate.SelectedDate = personalAccidentsPolicy.EndDate;
                txtAddition.Text = personalAccidentsPolicy.Addition.ToString();
                isOffer = personalAccidentsPolicy.IsOffer;
                CompanySelection((int)personalAccidentsPolicy.CompanyID);
                var principalAgentNumbers = cbPrincipalAgentNumber.Items;
                foreach (var item in principalAgentNumbers)
                {
                    AgentNumber principalAgentNumber = (AgentNumber)item;
                    if (principalAgentNumber.AgentNumberID == personalAccidentsPolicy.PrincipalAgentNumberID)
                    {
                        cbPrincipalAgentNumber.SelectedItem = item;
                        break;
                    }
                }
                var secundaryAgentNumbers = cbSecundaryAgentNumber.Items;
                foreach (var item in secundaryAgentNumbers)
                {
                    AgentNumber secundaryAgentNumber = (AgentNumber)item;
                    if (secundaryAgentNumber.AgentNumberID == personalAccidentsPolicy.SubAgentNumberID)
                    {
                        cbSecundaryAgentNumber.SelectedItem = item;
                        break;
                    }
                }
                CurrencySelection();
                PaymentMethodSelection();
                txtPaymentesNumber.Text = personalAccidentsPolicy.PaymentsNumber.ToString();
                dpPaymentDate.SelectedDate = personalAccidentsPolicy.PaymentDate;
                if (personalAccidentsPolicy.PremiumNeto1 != null)
                {
                    txtPremiumNeto1.Text = ConvertDecimalToString(personalAccidentsPolicy.PremiumNeto1);
                }
                if (personalAccidentsPolicy.PremiumNeto2 != null)
                {
                    txtPremiumNeto2.Text = ConvertDecimalToString(personalAccidentsPolicy.PremiumNeto2);
                }
                if (personalAccidentsPolicy.PremiumNeto3 != null)
                {
                    txtPremiumNeto3.Text = ConvertDecimalToString(personalAccidentsPolicy.PremiumNeto3);
                }
                if (personalAccidentsPolicy.PremiumNeto4 != null)
                {
                    txtPremiumNeto4.Text = ConvertDecimalToString(personalAccidentsPolicy.PremiumNeto4);
                }
                if (personalAccidentsPolicy.InscriptionFees != null)
                {
                    txtInscriptionFees.Text = ConvertDecimalToString(personalAccidentsPolicy.InscriptionFees);
                }
                if (personalAccidentsPolicy.PolicyFees != null)
                {
                    txtPolicyFees.Text = ConvertDecimalToString(personalAccidentsPolicy.PolicyFees);
                }
                if (personalAccidentsPolicy.ProjectionFees != null)
                {
                    txtProjectionFees.Text = ConvertDecimalToString(personalAccidentsPolicy.ProjectionFees);
                }
                if (personalAccidentsPolicy.StampsFees != null)
                {
                    txtStampFees.Text = ConvertDecimalToString(personalAccidentsPolicy.StampsFees);
                }
                if (personalAccidentsPolicy.HandlingFees != null)
                {
                    txtHandlingFees.Text = ConvertDecimalToString(personalAccidentsPolicy.HandlingFees);
                }
                if (personalAccidentsPolicy.TotalFees != null)
                {
                    txtTotalFees.Text = ConvertDecimalToString(personalAccidentsPolicy.TotalFees);
                }
                if (personalAccidentsPolicy.Credit != null)
                {
                    txtTotalCredit.Text = ConvertDecimalToString(personalAccidentsPolicy.Credit);
                }
                if (personalAccidentsPolicy.TotalPremium != null)
                {
                    txtTotalPremium.Text = ConvertDecimalToString(personalAccidentsPolicy.TotalPremium);
                }
                txtComments.Text = personalAccidentsPolicy.Comments;
                insureds = personalAccidentsLogic.GetAdditionalInsuredsByPolicyId(personalAccidentsPolicy.PersonalAccidentsPolicyID);
                foreach (var item in insureds)
                {
                    item.InsuredType = personalAccidentsLogic.GetInsuredTypeByTypeId(item.InsuredTypeID);
                    if (item.InsuredType.InsuredTypeName == "בן/בת זוג")
                    {
                        partnerInsuranceDetails = personalAccidentsLogic.GetInsuranceDetails(personalAccidentsPolicy.PersonalAccidentsPolicyID, item.InsuredTypeID);
                    }
                }
                dgInsuredsBinding();
                if (insureds.Count > 0)
                {
                    var insured = insureds.FirstOrDefault(i => i.InsuredType.InsuredTypeName == "ילד/ילדה");
                    if (insured != null)
                    {
                        childrenInsuranceDetails = personalAccidentsLogic.GetInsuranceDetails(personalAccidentsPolicy.PersonalAccidentsPolicyID, insured.InsuredTypeID);
                    }
                }
                DisplayInsuranceDetails();
                coverages = coveragesLogic.GetAllPersonalAccidentsCoveragesByPolicy(personalAccidentsPolicy.PersonalAccidentsPolicyID);
                dgCoveragesBinding();
                trackings = trackingLogics.GetAllPersonalAccidentsTrackingsByPolicy(personalAccidentsPolicy.PersonalAccidentsPolicyID);
                dgTrackingsBinding();
                
                if (personalAccidentsPolicy.Addition != 0)
                {
                    txtPolicyNumber.IsEnabled = false;
                }
                if (int.Parse(personalAccidentsLogic.GetAdditionNumber(personalAccidentsPolicy.PolicyNumber)) - 1 > personalAccidentsPolicy.Addition)
                {
                    spGeneral.IsEnabled = false;
                    dpInsureds.IsEnabled = false;
                    spInsuranceDetails.IsEnabled = false;
                    dpCoverage.IsEnabled = false;
                    dpTracking.IsEnabled = false;
                }
                if (isAddition)
                {
                    txtPolicyNumber.IsEnabled = false;
                    txtAddition.Text = personalAccidentsLogic.GetAdditionNumber(personalAccidentsPolicy.PolicyNumber);
                    DateTime now = DateTime.Now;
                    dpOpenDate.SelectedDate = now;
                    dpStartDate.SelectedDate = now;
                    cbCompany.IsEnabled = false;
                }
                else
                {
                    standardMoneyCollection = moneyCollectionLogic.GetStandardMoneyColletionByPolicy(personalAccidentsPolicy);
                    if (standardMoneyCollection != null)
                    {
                        chbStandardMoneyCollection.IsChecked = true;
                        chbStandardMoneyCollection.IsEnabled = false;
                        standardMoneyCollectionTrackings = moneyCollectionLogic.GetTrakcingsByStsndardMoneyCollection(standardMoneyCollection);
                    }
                }
            }
        }

        private void DisplayInsuranceDetails()
        {

            var insured = personalAccidentsLogic.GetInsuredTypes().FirstOrDefault(i => i.InsuredTypeName == "ראשי");
            principalInsuranceDetails = personalAccidentsLogic.GetInsuranceDetails(personalAccidentsPolicy.PersonalAccidentsPolicyID, insured.InsuredTypeID);
            if (principalInsuranceDetails != null)
            {
                txtIncapacityByAccidentPeriod.Text = principalInsuranceDetails.IncapacityByAccidentPeriod;
                txtIncapacityBySicknessPeriod.Text = principalInsuranceDetails.IncapacityBySicknessPeriod;
                txtOther1.Text = principalInsuranceDetails.Other1Name;
                txtOther2.Text = principalInsuranceDetails.Other2Name;
                txtOther3.Text = principalInsuranceDetails.Other3Name;
                txtOther4.Text = principalInsuranceDetails.Other4Name;
                txtOther5.Text = principalInsuranceDetails.Other5Name;

                if (principalInsuranceDetails.PersonalAccidentsInsurenceTypeID != null)
                {
                    SelectItemInCb((int)principalInsuranceDetails.PersonalAccidentsInsurenceTypeID, cbInsurenceTypePrincipal);
                }
                chbDeathByAccidentPrincipal.IsChecked = principalInsuranceDetails.IsDeathByAccident;
                txtDeathByAccidentPrincipal.Text = ParseToString(principalInsuranceDetails.DeathByAccidentAmount);
                chbDisabilityByAccidentPrincipal.IsChecked = principalInsuranceDetails.IsDisabilityByAccident;
                txtDisabilityByAccidentPrincipal.Text = ParseToString(principalInsuranceDetails.DisabilityByAccidentAmount);
                chbFracturesAndBurnsByAccidentPrincipal.IsChecked = principalInsuranceDetails.IsFracturesAndBurnsByAccident;
                txtFracturesAndBurnsByAccidentPrincipal.Text = ParseToString(principalInsuranceDetails.FracturesAndBurnsByAccidentAmount);
                chbCompensationForHospitalizationPrincipal.IsChecked = principalInsuranceDetails.IsCompensationForHospitalization;
                txtCompensationForHospitalizationPrincipal.Text = ParseToString(principalInsuranceDetails.CompensationForHospitalizationAmount);
                txtHospitalizationDaysOfWaitingPrincipal.Text = principalInsuranceDetails.CompensationForHospitalizationWaitingDays;
                chbSiudiByAccidentPrincipal.IsChecked = principalInsuranceDetails.IsSiudByAccident;
                txtSiudiByAccidentPrincipal.Text = ParseToString(principalInsuranceDetails.SiudByAccidentAmount);
                chbSportsPrincipal.IsChecked = principalInsuranceDetails.IsSports;
                txtSportsPrincipal.Text = ParseToString(principalInsuranceDetails.SportsAmount);
                chbTwoWheeledCancellationPrincipal.IsChecked = principalInsuranceDetails.IsTwoWheeledCancellation;
                txtTwoWheeledCancellationPrincipal.Text = ParseToString(principalInsuranceDetails.TwoWheeledCancellationAmount);
                txtUpToCCPrincipal.Text = principalInsuranceDetails.TwoWheeledCancellationUpToCC;
                chbExtremeSportCancellationPrincipal.IsChecked = principalInsuranceDetails.IsExtremeSportCancellation;
                txtExtremeSportCancellationPrincipal.Text = ParseToString(principalInsuranceDetails.ExtremeSportCancellationAmount);
                chbMedicalChargesPrincipal.IsChecked = principalInsuranceDetails.IsMedicalCharges;
                txtMedicalChargesPrincipal.Text = ParseToString(principalInsuranceDetails.MedicalChargesAmount);
                chbMedicalAndDentalChargesPrincipal.IsChecked = principalInsuranceDetails.IsMedicalAndDentalCharges;
                txtMedicalAndDentalChargesPrincipal.Text = ParseToString(principalInsuranceDetails.MedicalAndDentalChargesAmount);
                chbWarRiskCoveragePrincipal.IsChecked = principalInsuranceDetails.IsWarRiskCoverage;
                txtWarRiskCoveragePrincipal.Text = ParseToString(principalInsuranceDetails.WarRiskCoverageAmount);
                chbIncapacityByAccidentPrincipal.IsChecked = principalInsuranceDetails.IsIncapacityByAccident;
                txtIncapacityByAccidentPrincipal.Text = ParseToString(principalInsuranceDetails.IncapacityByAccidentAmount);
                txtIncapacityByAccidentWaitingDaysPrincipal.Text = principalInsuranceDetails.IncapacityByAccidentWaitingDays;
                chbIncapacityBySicknessPrincipal.IsChecked = principalInsuranceDetails.IsIncapacityBySickness;
                txtIncapacityBySicknessPrincipal.Text = ParseToString(principalInsuranceDetails.IncapacityBySicknessAmount);
                txtIncapacityBySicknessWaitingDaysPrincipal.Text = principalInsuranceDetails.IncapacityBySicknessWaitingDays;
                chbOther1Principal.IsChecked = principalInsuranceDetails.IsOther1;
                chbOther2Principal.IsChecked = principalInsuranceDetails.IsOther2;
                chbOther3Principal.IsChecked = principalInsuranceDetails.IsOther3;
                chbOther4Principal.IsChecked = principalInsuranceDetails.IsOther4;
                chbOther5Principal.IsChecked = principalInsuranceDetails.IsOther5;
                txtOther1Principal.Text = ParseToString(principalInsuranceDetails.Other1Amount);
                txtOther2Principal.Text = ParseToString(principalInsuranceDetails.Other2Amount);
                txtOther3Principal.Text = ParseToString(principalInsuranceDetails.Other3Amount);
                txtOther4Principal.Text = ParseToString(principalInsuranceDetails.Other4Amount);
                txtOther5Principal.Text = ParseToString(principalInsuranceDetails.Other5Amount);
                chbMedicalAdviceAndPeriodiCheckPrincipal.IsChecked = principalInsuranceDetails.IsMedicalAdviceAndPeriodiCheck;
                txtMedicalAdviceAndPeriodiCheckPrincipal.Text = ParseToString(principalInsuranceDetails.MedicalAdviceAndPeriodiCheckAmount);
                chbDiagnosticTestsAndPregnancyAnnexPrincipal.IsChecked = principalInsuranceDetails.IsDiagnosticTestsAndPregnancyAnnex;
                txtDiagnosticTestsAndPregnancyAnnexPrincipal.Text = ParseToString(principalInsuranceDetails.DiagnosticTestsAndPregnancyAnnexAmount);
                txtDiagnosticTestsAndPregnancyAnnexTypePrincipal.Text = principalInsuranceDetails.DiagnosticTestsAndPregnancyAnnexType;
                chbFastDiagnosisAndMedicalGuidancePrincipal.IsChecked = principalInsuranceDetails.IsFastDiagnosisAndMedicalGuidance;
                txtFastDiagnosisAndMedicalGuidancePrincipal.Text = ParseToString(principalInsuranceDetails.FastDiagnosisAndMedicalGuidanceAmount);
                chbPsicologyPrincipal.IsChecked = principalInsuranceDetails.IsPsicology;
                txtPsicologyPrincipal.Text = ParseToString(principalInsuranceDetails.PsicologyAmount);
                chbComplementaryPrincipal.IsChecked = principalInsuranceDetails.IsComplementary;
                txtComplementaryPrincipal.Text = ParseToString(principalInsuranceDetails.ComplementaryAmount);
                chbToTheChildPrincipal.IsChecked = principalInsuranceDetails.IsToTheChild;
                txtToTheChildPrincipal.Text = ParseToString(principalInsuranceDetails.ToTheChildAmount);
            }

            if (partnerInsuranceDetails != null)
            {
                if (partnerInsuranceDetails.PersonalAccidentsInsurenceTypeID != null)
                {
                    SelectItemInCb((int)partnerInsuranceDetails.PersonalAccidentsInsurenceTypeID, cbInsurenceTypePartner);
                }
                chbDeathByAccidentPartner.IsChecked = partnerInsuranceDetails.IsDeathByAccident;
                txtDeathByAccidentPartner.Text = ParseToString(partnerInsuranceDetails.DeathByAccidentAmount);
                chbDisabilityByAccidentPartner.IsChecked = partnerInsuranceDetails.IsDisabilityByAccident;
                txtDisabilityByAccidentPartner.Text = ParseToString(partnerInsuranceDetails.DisabilityByAccidentAmount);
                chbFracturesAndBurnsByAccidentPartner.IsChecked = partnerInsuranceDetails.IsFracturesAndBurnsByAccident;
                txtFracturesAndBurnsByAccidentPartner.Text = ParseToString(partnerInsuranceDetails.FracturesAndBurnsByAccidentAmount);
                chbCompensationForHospitalizationPartner.IsChecked = partnerInsuranceDetails.IsCompensationForHospitalization;
                txtCompensationForHospitalizationPartner.Text = ParseToString(partnerInsuranceDetails.CompensationForHospitalizationAmount);
                txtHospitalizationDaysOfWaitingPartner.Text = partnerInsuranceDetails.CompensationForHospitalizationWaitingDays;
                chbSiudiByAccidentPartner.IsChecked = partnerInsuranceDetails.IsSiudByAccident;
                txtSiudiByAccidentPartner.Text = ParseToString(partnerInsuranceDetails.SiudByAccidentAmount);
                chbSportsPartner.IsChecked = partnerInsuranceDetails.IsSports;
                txtSportsPartner.Text = ParseToString(partnerInsuranceDetails.SportsAmount);
                chbTwoWheeledCancellationPartner.IsChecked = partnerInsuranceDetails.IsTwoWheeledCancellation;
                txtTwoWheeledCancellationPartner.Text = ParseToString(partnerInsuranceDetails.TwoWheeledCancellationAmount);
                txtUpToCCPartner.Text = partnerInsuranceDetails.TwoWheeledCancellationUpToCC;
                chbExtremeSportCancellationPartner.IsChecked = partnerInsuranceDetails.IsExtremeSportCancellation;
                txtExtremeSportCancellationPartner.Text = ParseToString(partnerInsuranceDetails.ExtremeSportCancellationAmount);
                chbMedicalChargesPartner.IsChecked = partnerInsuranceDetails.IsMedicalCharges;
                txtMedicalChargesPartner.Text = ParseToString(partnerInsuranceDetails.MedicalChargesAmount);
                chbMedicalAndDentalChargesPartner.IsChecked = partnerInsuranceDetails.IsMedicalAndDentalCharges;
                txtMedicalAndDentalChargesPartner.Text = ParseToString(partnerInsuranceDetails.MedicalAndDentalChargesAmount);
                chbWarRiskCoveragePartner.IsChecked = partnerInsuranceDetails.IsWarRiskCoverage;
                txtWarRiskCoveragePartner.Text = ParseToString(partnerInsuranceDetails.WarRiskCoverageAmount);
                chbIncapacityByAccidentPartner.IsChecked = partnerInsuranceDetails.IsIncapacityByAccident;
                txtIncapacityByAccidentPartner.Text = ParseToString(partnerInsuranceDetails.IncapacityByAccidentAmount);
                txtIncapacityByAccidentWaitingDaysPartner.Text = partnerInsuranceDetails.IncapacityByAccidentWaitingDays;
                chbIncapacityBySicknessPartner.IsChecked = partnerInsuranceDetails.IsIncapacityBySickness;
                txtIncapacityBySicknessPartner.Text = ParseToString(partnerInsuranceDetails.IncapacityBySicknessAmount);
                txtIncapacityBySicknessWaitingDaysPartner.Text = partnerInsuranceDetails.IncapacityBySicknessWaitingDays;
                chbOther1Partner.IsChecked = partnerInsuranceDetails.IsOther1;
                chbOther2Partner.IsChecked = partnerInsuranceDetails.IsOther2;
                chbOther3Partner.IsChecked = partnerInsuranceDetails.IsOther3;
                chbOther4Partner.IsChecked = partnerInsuranceDetails.IsOther4;
                chbOther5Partner.IsChecked = partnerInsuranceDetails.IsOther5;
                txtOther1Partner.Text = ParseToString(partnerInsuranceDetails.Other1Amount);
                txtOther2Partner.Text = ParseToString(partnerInsuranceDetails.Other2Amount);
                txtOther3Partner.Text = ParseToString(partnerInsuranceDetails.Other3Amount);
                txtOther4Partner.Text = ParseToString(partnerInsuranceDetails.Other4Amount);
                txtOther5Partner.Text = ParseToString(partnerInsuranceDetails.Other5Amount);
                chbMedicalAdviceAndPeriodiCheckPartner.IsChecked = partnerInsuranceDetails.IsMedicalAdviceAndPeriodiCheck;
                txtMedicalAdviceAndPeriodiCheckPartner.Text = ParseToString(partnerInsuranceDetails.MedicalAdviceAndPeriodiCheckAmount);
                chbDiagnosticTestsAndPregnancyAnnexPartner.IsChecked = partnerInsuranceDetails.IsDiagnosticTestsAndPregnancyAnnex;
                txtDiagnosticTestsAndPregnancyAnnexPartner.Text = ParseToString(partnerInsuranceDetails.DiagnosticTestsAndPregnancyAnnexAmount);
                txtDiagnosticTestsAndPregnancyAnnexTypePartner.Text = partnerInsuranceDetails.DiagnosticTestsAndPregnancyAnnexType;
                chbFastDiagnosisAndMedicalGuidancePartner.IsChecked = partnerInsuranceDetails.IsFastDiagnosisAndMedicalGuidance;
                txtFastDiagnosisAndMedicalGuidancePartner.Text = ParseToString(partnerInsuranceDetails.FastDiagnosisAndMedicalGuidanceAmount);
                chbPsicologyPartner.IsChecked = partnerInsuranceDetails.IsPsicology;
                txtPsicologyPartner.Text = ParseToString(partnerInsuranceDetails.PsicologyAmount);
                chbComplementaryPartner.IsChecked = partnerInsuranceDetails.IsComplementary;
                txtComplementaryPartner.Text = ParseToString(partnerInsuranceDetails.ComplementaryAmount);
                chbToTheChildPartner.IsChecked = partnerInsuranceDetails.IsToTheChild;
                txtToTheChildPartner.Text = ParseToString(partnerInsuranceDetails.ToTheChildAmount);
            }

            if (childrenInsuranceDetails != null)
            {
                if (childrenInsuranceDetails.PersonalAccidentsInsurenceTypeID != null)
                {
                    SelectItemInCb((int)childrenInsuranceDetails.PersonalAccidentsInsurenceTypeID, cbInsurenceTypeChildren);
                }
                chbDeathByAccidentChildren.IsChecked = childrenInsuranceDetails.IsDeathByAccident;
                txtDeathByAccidentChildren.Text = ParseToString(childrenInsuranceDetails.DeathByAccidentAmount);
                chbDisabilityByAccidentChildren.IsChecked = childrenInsuranceDetails.IsDisabilityByAccident;
                txtDisabilityByAccidentChildren.Text = ParseToString(childrenInsuranceDetails.DisabilityByAccidentAmount);
                chbFracturesAndBurnsByAccidentChildren.IsChecked = childrenInsuranceDetails.IsFracturesAndBurnsByAccident;
                txtFracturesAndBurnsByAccidentChildren.Text = ParseToString(childrenInsuranceDetails.FracturesAndBurnsByAccidentAmount);
                chbCompensationForHospitalizationChildren.IsChecked = childrenInsuranceDetails.IsCompensationForHospitalization;
                txtCompensationForHospitalizationChildren.Text = ParseToString(childrenInsuranceDetails.CompensationForHospitalizationAmount);
                txtHospitalizationDaysOfWaitingChildren.Text = childrenInsuranceDetails.CompensationForHospitalizationWaitingDays;
                chbSiudiByAccidentChildren.IsChecked = childrenInsuranceDetails.IsSiudByAccident;
                txtSiudiByAccidentChildren.Text = ParseToString(childrenInsuranceDetails.SiudByAccidentAmount);
                chbSportsChildren.IsChecked = childrenInsuranceDetails.IsSports;
                txtSportsChildren.Text = ParseToString(childrenInsuranceDetails.SportsAmount);
                chbTwoWheeledCancellationChildren.IsChecked = childrenInsuranceDetails.IsTwoWheeledCancellation;
                txtTwoWheeledCancellationChildren.Text = ParseToString(childrenInsuranceDetails.TwoWheeledCancellationAmount);
                txtUpToCCChildren.Text = childrenInsuranceDetails.TwoWheeledCancellationUpToCC;
                chbExtremeSportCancellationChildren.IsChecked = childrenInsuranceDetails.IsExtremeSportCancellation;
                txtExtremeSportCancellationChildren.Text = ParseToString(childrenInsuranceDetails.ExtremeSportCancellationAmount);
                chbMedicalChargesChildren.IsChecked = childrenInsuranceDetails.IsMedicalCharges;
                txtMedicalChargesChildren.Text = ParseToString(childrenInsuranceDetails.MedicalChargesAmount);
                chbMedicalAndDentalChargesChildren.IsChecked = childrenInsuranceDetails.IsMedicalAndDentalCharges;
                txtMedicalAndDentalChargesChildren.Text = ParseToString(childrenInsuranceDetails.MedicalAndDentalChargesAmount);
                chbWarRiskCoverageChildren.IsChecked = childrenInsuranceDetails.IsWarRiskCoverage;
                txtWarRiskCoverageChildren.Text = ParseToString(childrenInsuranceDetails.WarRiskCoverageAmount);
                chbIncapacityByAccidentChildren.IsChecked = childrenInsuranceDetails.IsIncapacityByAccident;
                txtIncapacityByAccidentChildren.Text = ParseToString(childrenInsuranceDetails.IncapacityByAccidentAmount);
                txtIncapacityByAccidentWaitingDaysChildren.Text = childrenInsuranceDetails.IncapacityByAccidentWaitingDays;
                chbIncapacityBySicknessChildren.IsChecked = childrenInsuranceDetails.IsIncapacityBySickness;
                txtIncapacityBySicknessChildren.Text = ParseToString(childrenInsuranceDetails.IncapacityBySicknessAmount);
                txtIncapacityBySicknessWaitingDaysChildren.Text = childrenInsuranceDetails.IncapacityBySicknessWaitingDays;
                chbOther1Children.IsChecked = childrenInsuranceDetails.IsOther1;
                chbOther2Children.IsChecked = childrenInsuranceDetails.IsOther2;
                chbOther3Children.IsChecked = childrenInsuranceDetails.IsOther3;
                chbOther4Children.IsChecked = childrenInsuranceDetails.IsOther4;
                chbOther5Children.IsChecked = childrenInsuranceDetails.IsOther5;
                txtOther1Children.Text = ParseToString(childrenInsuranceDetails.Other1Amount);
                txtOther2Children.Text = ParseToString(childrenInsuranceDetails.Other2Amount);
                txtOther3Children.Text = ParseToString(childrenInsuranceDetails.Other3Amount);
                txtOther4Children.Text = ParseToString(childrenInsuranceDetails.Other4Amount);
                txtOther5Children.Text = ParseToString(childrenInsuranceDetails.Other5Amount);
                chbMedicalAdviceAndPeriodiCheckChildren.IsChecked = childrenInsuranceDetails.IsMedicalAdviceAndPeriodiCheck;
                txtMedicalAdviceAndPeriodiCheckChildren.Text = ParseToString(childrenInsuranceDetails.MedicalAdviceAndPeriodiCheckAmount);
                chbDiagnosticTestsAndPregnancyAnnexChildren.IsChecked = childrenInsuranceDetails.IsDiagnosticTestsAndPregnancyAnnex;
                txtDiagnosticTestsAndPregnancyAnnexChildren.Text = ParseToString(childrenInsuranceDetails.DiagnosticTestsAndPregnancyAnnexAmount);
                txtDiagnosticTestsAndPregnancyAnnexTypeChildren.Text = childrenInsuranceDetails.DiagnosticTestsAndPregnancyAnnexType;
                chbFastDiagnosisAndMedicalGuidanceChildren.IsChecked = childrenInsuranceDetails.IsFastDiagnosisAndMedicalGuidance;
                txtFastDiagnosisAndMedicalGuidanceChildren.Text = ParseToString(childrenInsuranceDetails.FastDiagnosisAndMedicalGuidanceAmount);
                chbPsicologyChildren.IsChecked = childrenInsuranceDetails.IsPsicology;
                txtPsicologyChildren.Text = ParseToString(childrenInsuranceDetails.PsicologyAmount);
                chbComplementaryChildren.IsChecked = childrenInsuranceDetails.IsComplementary;
                txtComplementaryChildren.Text = ParseToString(childrenInsuranceDetails.ComplementaryAmount);
                chbToTheChildChildren.IsChecked = childrenInsuranceDetails.IsToTheChild;
                txtToTheChildChildren.Text = ParseToString(childrenInsuranceDetails.ToTheChildAmount);
            }
        }

        private string ParseToString(decimal? amount)
        {
            if (amount != null)
            {
                return ((decimal)amount).ToString("0.##");
            }
            else
            {
                return "";
            }
        }

        private void cbInsuredTypeBinding()
        {
            cbInsuredType.ItemsSource = personalAccidentsLogic.GetInsuredTypes().Where(i => i.InsuredTypeName != "ראשי");
            cbInsuredType.DisplayMemberPath = "InsuredTypeName";
        }

        //פונקציה שממירה מספר עשרוני לטקסט
        private string ConvertDecimalToString(decimal? number)
        {
            if (number == null)
            {
                return null;
            }
            return ((decimal)number).ToString("G29");
        }

        private void CurrencySelection()
        {
            var currencies = cbCurrency.Items;
            foreach (var item in currencies)
            {
                if (personalAccidentsPolicy.Currency == ((ComboBoxItem)item).Content.ToString())
                {
                    cbCurrency.SelectedItem = item;
                    break;
                }
            }
        }

        private void PaymentMethodSelection()
        {
            var paymentMethods = cbPaymentMode.Items;
            foreach (var item in paymentMethods)
            {
                if (personalAccidentsPolicy.PaymentMethod == ((ComboBoxItem)item).Content.ToString())
                {
                    cbPaymentMode.SelectedItem = item;
                    break;
                }
            }
        }

        private void cbCompanyBinding()
        {
            cbCompany.ItemsSource = insuranceLogic.GetActiveCompaniesByInsurance("תאונות אישיות");
            cbCompany.DisplayMemberPath = "Company.CompanyName";
        }



        private void btnShowMedicalCosts_Click(object sender, RoutedEventArgs e)
        {
            if (!isMedicalCostsOpen)
            {
                spMedicalCosts.Visibility = Visibility.Visible;
                var addUriSource = new Uri(@"/IconsMind/Arrow-Up-2_24px.png", UriKind.Relative);
                imgBtnMedicalCosts.Source = new BitmapImage(addUriSource);
                isMedicalCostsOpen = true;
                svInsuranceDetails.PageDown();
            }
            else
            {
                spMedicalCosts.Visibility = Visibility.Collapsed;
                var addUriSource = new Uri(@"/IconsMind/Arrow-Down-2_24px.png", UriKind.Relative);
                imgBtnMedicalCosts.Source = new BitmapImage(addUriSource);
                isMedicalCostsOpen = false;
            }
        }

        private void btnShowOthers_Click(object sender, RoutedEventArgs e)
        {
            if (!isOthersOpen)
            {
                spOthers.Visibility = Visibility.Visible;
                var addUriSource = new Uri(@"/IconsMind/Arrow-Up-2_24px.png", UriKind.Relative);
                imgBtnOthers.Source = new BitmapImage(addUriSource);
                isOthersOpen = true;
                svInsuranceDetails.PageDown();
            }
            else
            {
                spOthers.Visibility = Visibility.Collapsed;
                var addUriSource = new Uri(@"/IconsMind/Arrow-Down-2_24px.png", UriKind.Relative);
                imgBtnOthers.Source = new BitmapImage(addUriSource);
                isOthersOpen = false;
            }
        }

        private void btnUpdateCompanyTable_Click(object sender, RoutedEventArgs e)
        {
            int? selectedItemId = null;
            if (cbCompany.SelectedItem != null)
            {
                selectedItemId = ((InsuranceCompany)cbCompany.SelectedItem).CompanyID;
            }
            int insuranceID = insuranceLogic.GetInsuranceID("תאונות אישיות");
            InsuranceCompany company = new InsuranceCompany() { InsuranceID = insuranceID };
            frmUpdateTable updateCompaniesTable = new frmUpdateTable(company);
            updateCompaniesTable.ShowDialog();
            cbCompanyBinding();
            if (updateCompaniesTable.ItemAdded != null)
            {
                CompanySelection(((InsuranceCompany)updateCompaniesTable.ItemAdded).CompanyID);
            }
            else if (selectedItemId != null)
            {
                CompanySelection((int)selectedItemId);
            }
        }

        private void CompanySelection(int companyID)
        {
            var companies = cbCompany.Items;
            foreach (var item in companies)
            {
                InsuranceCompany company = (InsuranceCompany)item;
                if (company.CompanyID == companyID)
                {
                    cbCompany.SelectedItem = item;
                    break;
                }
            }
        }

        private void cbCompany_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cbCompany.SelectedItem == null)
            {
                cbPrincipalAgentNumber.SelectedItem = null;
                cbSecundaryAgentNumber.SelectedItem = null;
                cbPrincipalAgentNumber.IsEnabled = false;
                cbSecundaryAgentNumber.IsEnabled = false;
                return;
            }
            cbInsurenceTypeBinding();
            int insuranceID = insuranceLogic.GetInsuranceID("תאונות אישיות");
            if (client != null)
            {
                cbPrincipalAgentNumber.ItemsSource = agentLogic.GetAgentNumbersByCompanyAndInsurance(client.PrincipalAgentID, (InsuranceCompany)cbCompany.SelectedItem, insuranceID);
                cbPrincipalAgentNumber.DisplayMemberPath = "Number";
                cbPrincipalAgentNumber.IsEnabled = true;
                cbPrincipalAgentNumber.SelectedIndex = 0;
                cbSecundaryAgentNumber.ItemsSource = agentLogic.GetAgentNumbersByCompanyAndInsurance(client.SecundaryAgentID, (InsuranceCompany)cbCompany.SelectedItem, insuranceID);
                cbSecundaryAgentNumber.DisplayMemberPath = "Number";
                cbSecundaryAgentNumber.IsEnabled = true;
                cbSecundaryAgentNumber.SelectedIndex = 0;
            }
            tbItemCoverages.Visibility = Visibility.Visible;
        }

        private void txtPremiumNeto1_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (sender is TextBox)
            {
                TextBox txtBox = (TextBox)sender;
                txtBox.ClearValue(BorderBrushProperty);
                if (txtBox.Text != "")
                {
                    try
                    {
                        decimal reusult = validations.ConvertStringToDecimal(txtBox.Text);
                    }
                    catch (Exception ex)
                    {
                        txtBox.BorderBrush = System.Windows.Media.Brushes.Red;
                        System.Windows.Forms.MessageBox.Show(ex.Message);
                        txtBox.Text = "";
                    }
                }
            }
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            if (nullErrorList != null && nullErrorList.Count > 0)
            {
                foreach (var item in nullErrorList)
                {
                    if (item is TextBox)
                    {
                        ((TextBox)item).ClearValue(BorderBrushProperty);
                    }
                    else if (item is ComboBox)
                    {
                        ((ComboBox)item).ClearValue(BorderBrushProperty);
                    }
                    else if (item is DatePicker)
                    {
                        ((DatePicker)item).ClearValue(BorderBrushProperty);
                    }
                }
            }
            if (!validations.IsDigitsOnly(txtPolicyNumber.Text))
            {
                cancelClose = true;
                MessageBox.Show("מס' פוליסה יכול להכיל מספרים בלבד", "", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            //בדיקת שדות חובה
            nullErrorList = validations.InputNullValidation(new object[] { txtPolicyNumber, txtAddition, dpStartDate, dpEndDate, cbCompany, txtTotalPremium });
            if (nullErrorList.Count > 0)
            {
                cancelClose = true;
                MessageBox.Show("נא למלאות שדות חובה", "", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (dpEndDate.SelectedDate <= dpStartDate.SelectedDate)
            {
                cancelClose = true;
                tbItemGeneral.Focus();
                MessageBox.Show("תאריך סיום חייב להיות גדול מתאריך תחילה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            int addition = Convert.ToInt32(txtAddition.Text);
            int? paymentNumbers = null;
            if (txtPaymentesNumber.Text != "")
            {
                paymentNumbers = int.Parse(txtPaymentesNumber.Text);
            }
            decimal? premium1 = null;
            if (txtPremiumNeto1.Text != "")
            {
                premium1 = decimal.Parse(txtPremiumNeto1.Text);
            }
            decimal? premium2 = null;
            if (txtPremiumNeto2.Text != "")
            {
                premium2 = decimal.Parse(txtPremiumNeto2.Text);
            }
            decimal? premium3 = null;
            if (txtPremiumNeto3.Text != "")
            {
                premium3 = decimal.Parse(txtPremiumNeto3.Text);
            }
            decimal? premium4 = null;
            if (txtPremiumNeto4.Text != "")
            {
                premium4 = decimal.Parse(txtPremiumNeto4.Text);
            }
            decimal? inscriptionFees = null;
            if (txtInscriptionFees.Text != "")
            {
                inscriptionFees = decimal.Parse(txtInscriptionFees.Text);
            }
            decimal? policyFees = null;
            if (txtPolicyFees.Text != "")
            {
                policyFees = decimal.Parse(txtPolicyFees.Text);
            }
            decimal? projectionFees = null;
            if (txtProjectionFees.Text != "")
            {
                projectionFees = decimal.Parse(txtProjectionFees.Text);
            }
            decimal? stampFees = null;
            if (txtStampFees.Text != "")
            {
                stampFees = decimal.Parse(txtStampFees.Text);
            }
            decimal? handlingFees = null;
            if (txtHandlingFees.Text != "")
            {
                handlingFees = decimal.Parse(txtHandlingFees.Text);
            }
            decimal? totalPremium = null;
            if (txtTotalPremium.Text != "")
            {
                totalPremium = decimal.Parse(txtTotalPremium.Text);
            }
            decimal? totalFees = null;
            if (txtTotalFees.Text != "")
            {
                totalFees = decimal.Parse(txtTotalFees.Text);
            }
            decimal? totalCredit = null;
            if (txtTotalCredit.Text != "")
            {
                totalCredit = decimal.Parse(txtTotalCredit.Text);
            }
            string paymentMode = "";
            if (cbPaymentMode.SelectedItem != null)
            {
                paymentMode = ((ComboBoxItem)cbPaymentMode.SelectedItem).Content.ToString();
            }
            int? principalAgentNumberID = null;
            if ((AgentNumber)cbPrincipalAgentNumber.SelectedItem != null)
            {
                principalAgentNumberID = ((AgentNumber)cbPrincipalAgentNumber.SelectedItem).AgentNumberID;
            }
            int? subAgentNumberID = null;
            if ((AgentNumber)cbSecundaryAgentNumber.SelectedItem != null)
            {
                subAgentNumberID = ((AgentNumber)cbSecundaryAgentNumber.SelectedItem).AgentNumberID;
            }
            decimal? deathByAccidentPrincipal = ParseToDecimal(chbDeathByAccidentPrincipal, txtDeathByAccidentPrincipal.Text);
            decimal? disabilityByAccidentPrincipal = ParseToDecimal(chbDisabilityByAccidentPrincipal, txtDisabilityByAccidentPrincipal.Text);
            decimal? fracturesAndBurnsByAccidentPrincipal = ParseToDecimal(chbFracturesAndBurnsByAccidentPrincipal, txtFracturesAndBurnsByAccidentPrincipal.Text);
            decimal? compensationForHospitalizationPrincipal = ParseToDecimal(chbCompensationForHospitalizationPrincipal, txtCompensationForHospitalizationPrincipal.Text);
            decimal? siudByAccidentPrincipal = ParseToDecimal(chbSiudiByAccidentPrincipal, txtSiudiByAccidentPrincipal.Text);
            decimal? sportsPrincipal = ParseToDecimal(chbSportsPrincipal, txtSportsPrincipal.Text);
            decimal? twoWheeledCancellationPrincipal = ParseToDecimal(chbTwoWheeledCancellationPrincipal, txtTwoWheeledCancellationPrincipal.Text);
            decimal? extremeSportCancellationPrincipal = ParseToDecimal(chbExtremeSportCancellationPrincipal, txtExtremeSportCancellationPrincipal.Text);
            decimal? medicalChargesPrincipal = ParseToDecimal(chbMedicalChargesPrincipal, txtMedicalChargesPrincipal.Text);
            decimal? medicalAndDentalChargesPrincipal = ParseToDecimal(chbMedicalAndDentalChargesPrincipal, txtMedicalAndDentalChargesPrincipal.Text);
            decimal? warRiskCoveragePrincipal = ParseToDecimal(chbWarRiskCoveragePrincipal, txtWarRiskCoveragePrincipal.Text);
            decimal? incapacityByAccidentPrincipal = ParseToDecimal(chbIncapacityByAccidentPrincipal, txtIncapacityByAccidentPrincipal.Text);
            decimal? incapacityBySicknessPrincipal = ParseToDecimal(chbIncapacityBySicknessPrincipal, txtIncapacityBySicknessPrincipal.Text);
            decimal? other1Principal = ParseToDecimal(chbOther1Principal, txtOther1Principal.Text);
            decimal? other2Principal = ParseToDecimal(chbOther2Principal, txtOther2Principal.Text);
            decimal? other3Principal = ParseToDecimal(chbOther3Principal, txtOther3Principal.Text);
            decimal? other4Principal = ParseToDecimal(chbOther4Principal, txtOther4Principal.Text);
            decimal? other5Principal = ParseToDecimal(chbOther5Principal, txtOther5Principal.Text);
            decimal? medicalAdviceAndPeriodiCheckPrincipal = ParseToDecimal(chbMedicalAdviceAndPeriodiCheckPrincipal, txtMedicalAdviceAndPeriodiCheckPrincipal.Text);
            decimal? diagnosticTestsAndPregnancyAnnexPrincipal = ParseToDecimal(chbDiagnosticTestsAndPregnancyAnnexPrincipal, txtDiagnosticTestsAndPregnancyAnnexPrincipal.Text);
            decimal? fastDiagnosisAndMedicalGuidancePrincipal = ParseToDecimal(chbFastDiagnosisAndMedicalGuidancePrincipal, txtFastDiagnosisAndMedicalGuidancePrincipal.Text);
            decimal? psicologyPrincipal = ParseToDecimal(chbPsicologyPrincipal, txtPsicologyPrincipal.Text);
            decimal? complementaryPrincipal = ParseToDecimal(chbComplementaryPrincipal, txtComplementaryPrincipal.Text);
            decimal? toTheChildPrincipal = ParseToDecimal(chbToTheChildPrincipal, txtToTheChildPrincipal.Text);
            int? insuranceTypePrincipal = null;
            if (cbInsurenceTypePrincipal.SelectedItem != null)
            {
                insuranceTypePrincipal = ((PersonalAccidentsInsuranceType)cbInsurenceTypePrincipal.SelectedItem).PersonalAccidentsInsuranceTypeID;
            }
            int insuredTypeID = (personalAccidentsLogic.GetInsuredTypes().FirstOrDefault(i => i.InsuredTypeName == "ראשי")).InsuredTypeID;

            try
            {
                if (personalAccidentsPolicy == null || isAddition == true)
                {
                    policyID = personalAccidentsLogic.InsertPersonalAccidentsPolicy(client, isOffer, (DateTime)dpOpenDate.SelectedDate, txtPolicyNumber.Text, addition, principalAgentNumberID, subAgentNumberID, (DateTime)dpStartDate.SelectedDate, (DateTime)dpEndDate.SelectedDate, ((InsuranceCompany)cbCompany.SelectedItem).CompanyID, ((ComboBoxItem)cbCurrency.SelectedItem).Content.ToString(), paymentMode, paymentNumbers, dpPaymentDate.SelectedDate, premium1, premium2, premium3, premium4, inscriptionFees, policyFees, projectionFees, stampFees, handlingFees, totalPremium, totalFees, totalCredit, txtComments.Text);
                    if (!isOffer)
                    {
                        path = personalAccidentsPath + @"\תאונות אישיות\תאונות אישיות " + txtPolicyNumber.Text;
                    }
                    else
                    {
                        path = personalAccidentsPath + @"\הצעות\תאונות אישיות\" + txtPolicyNumber.Text;

                    }
                    principalInsuranceDetails = new PersonalAccidentsInsurancesDetail()
                    {
                        CompensationForHospitalizationAmount = compensationForHospitalizationPrincipal,
                        CompensationForHospitalizationWaitingDays = txtHospitalizationDaysOfWaitingPrincipal.Text,
                        ComplementaryAmount = complementaryPrincipal,
                        DeathByAccidentAmount = deathByAccidentPrincipal,
                        DiagnosticTestsAndPregnancyAnnexAmount = diagnosticTestsAndPregnancyAnnexPrincipal,
                        DiagnosticTestsAndPregnancyAnnexType = txtDiagnosticTestsAndPregnancyAnnexTypePrincipal.Text,
                        DisabilityByAccidentAmount = disabilityByAccidentPrincipal,
                        ExtremeSportCancellationAmount = extremeSportCancellationPrincipal,
                        FastDiagnosisAndMedicalGuidanceAmount = fastDiagnosisAndMedicalGuidancePrincipal,
                        FracturesAndBurnsByAccidentAmount = fracturesAndBurnsByAccidentPrincipal,
                        IncapacityByAccidentAmount = incapacityByAccidentPrincipal,
                        IncapacityByAccidentPeriod = txtIncapacityByAccidentPeriod.Text,
                        IncapacityByAccidentWaitingDays = txtIncapacityByAccidentWaitingDaysPrincipal.Text,
                        IncapacityBySicknessAmount = incapacityBySicknessPrincipal,
                        IncapacityBySicknessPeriod = txtIncapacityBySicknessPeriod.Text,
                        IncapacityBySicknessWaitingDays = txtIncapacityBySicknessWaitingDaysPrincipal.Text,
                        InsuredTypeID = insuredTypeID,
                        IsCompensationForHospitalization = chbCompensationForHospitalizationPrincipal.IsChecked,
                        IsComplementary = chbComplementaryPrincipal.IsChecked,
                        IsDeathByAccident = chbDeathByAccidentPrincipal.IsChecked,
                        IsDiagnosticTestsAndPregnancyAnnex = chbDiagnosticTestsAndPregnancyAnnexPrincipal.IsChecked,
                        IsDisabilityByAccident = chbDisabilityByAccidentPrincipal.IsChecked,
                        IsExtremeSportCancellation = chbExtremeSportCancellationPrincipal.IsChecked,
                        IsFastDiagnosisAndMedicalGuidance = chbFastDiagnosisAndMedicalGuidancePrincipal.IsChecked,
                        IsFracturesAndBurnsByAccident = chbFracturesAndBurnsByAccidentPrincipal.IsChecked,
                        IsIncapacityByAccident = chbIncapacityByAccidentPrincipal.IsChecked,
                        IsIncapacityBySickness = chbIncapacityBySicknessPrincipal.IsChecked,
                        IsMedicalAdviceAndPeriodiCheck = chbMedicalAdviceAndPeriodiCheckPrincipal.IsChecked,
                        IsMedicalAndDentalCharges = chbMedicalAndDentalChargesPrincipal.IsChecked,
                        IsMedicalCharges = chbMedicalChargesPrincipal.IsChecked,
                        IsOther1 = chbOther1Principal.IsChecked,
                        IsOther2 = chbOther2Principal.IsChecked,
                        IsOther3 = chbOther3Principal.IsChecked,
                        IsOther4 = chbOther4Principal.IsChecked,
                        IsOther5 = chbOther5Principal.IsChecked,
                        IsPsicology = chbPsicologyPrincipal.IsChecked,
                        IsSiudByAccident = chbSiudiByAccidentPrincipal.IsChecked,
                        IsSports = chbSportsPrincipal.IsChecked,
                        IsToTheChild = chbToTheChildPrincipal.IsChecked,
                        IsTwoWheeledCancellation = chbTwoWheeledCancellationPrincipal.IsChecked,
                        IsWarRiskCoverage = chbWarRiskCoveragePrincipal.IsChecked,
                        MedicalAdviceAndPeriodiCheckAmount = medicalAdviceAndPeriodiCheckPrincipal,
                        MedicalAndDentalChargesAmount = medicalAndDentalChargesPrincipal,
                        MedicalChargesAmount = medicalChargesPrincipal,
                        Other1Amount = other1Principal,
                        Other2Amount = other2Principal,
                        Other3Amount = other3Principal,
                        Other4Amount = other4Principal,
                        Other5Amount = other5Principal,
                        Other1Name = txtOther1.Text,
                        Other2Name = txtOther2.Text,
                        Other3Name = txtOther3.Text,
                        Other4Name = txtOther4.Text,
                        Other5Name = txtOther5.Text,
                        PersonalAccidentsInsurenceTypeID = insuranceTypePrincipal,
                        PersonalAccidentsPolicyID = policyID,
                        PsicologyAmount = psicologyPrincipal,
                        SiudByAccidentAmount = siudByAccidentPrincipal,
                        SportsAmount = sportsPrincipal,
                        ToTheChildAmount = toTheChildPrincipal,
                        TwoWheeledCancellationAmount = twoWheeledCancellationPrincipal,
                        TwoWheeledCancellationUpToCC = txtUpToCCPrincipal.Text,
                        WarRiskCoverageAmount = warRiskCoveragePrincipal
                    };
                    SaveUnsavedInsured();
                    if (insureds.Count > 0)
                    {
                        personalAccidentsLogic.InsertAdditionalInsureds(policyID, insureds, isAddition);
                        int partnerTypeId = (personalAccidentsLogic.GetInsuredTypes().FirstOrDefault(i => i.InsuredTypeName == "בן/בת זוג")).InsuredTypeID;
                        int childrenTypeId = (personalAccidentsLogic.GetInsuredTypes().FirstOrDefault(i => i.InsuredTypeName == "ילד/ילדה")).InsuredTypeID;

                        if (insureds.FirstOrDefault(i => i.InsuredTypeID == partnerTypeId) != null)
                        {
                            decimal? deathByAccidentPartner = ParseToDecimal(chbDeathByAccidentPartner, txtDeathByAccidentPartner.Text);
                            decimal? disabilityByAccidentPartner = ParseToDecimal(chbDisabilityByAccidentPartner, txtDisabilityByAccidentPartner.Text);
                            decimal? fracturesAndBurnsByAccidentPartner = ParseToDecimal(chbFracturesAndBurnsByAccidentPartner, txtFracturesAndBurnsByAccidentPartner.Text);
                            decimal? compensationForHospitalizationPartner = ParseToDecimal(chbCompensationForHospitalizationPartner, txtCompensationForHospitalizationPartner.Text);
                            decimal? siudByAccidentPartner = ParseToDecimal(chbSiudiByAccidentPartner, txtSiudiByAccidentPartner.Text);
                            decimal? sportsPartner = ParseToDecimal(chbSportsPartner, txtSportsPartner.Text);
                            decimal? twoWheeledCancellationPartner = ParseToDecimal(chbTwoWheeledCancellationPartner, txtTwoWheeledCancellationPartner.Text);
                            decimal? extremeSportCancellationPartner = ParseToDecimal(chbExtremeSportCancellationPartner, txtExtremeSportCancellationPartner.Text);
                            decimal? medicalChargesPartner = ParseToDecimal(chbMedicalChargesPartner, txtMedicalChargesPartner.Text);
                            decimal? medicalAndDentalChargesPartner = ParseToDecimal(chbMedicalAndDentalChargesPartner, txtMedicalAndDentalChargesPartner.Text);
                            decimal? warRiskCoveragePartner = ParseToDecimal(chbWarRiskCoveragePartner, txtWarRiskCoveragePartner.Text);
                            decimal? incapacityByAccidentPartner = ParseToDecimal(chbIncapacityByAccidentPartner, txtIncapacityByAccidentPartner.Text);
                            decimal? incapacityBySicknessPartner = ParseToDecimal(chbIncapacityBySicknessPartner, txtIncapacityBySicknessPartner.Text);
                            decimal? other1Partner = ParseToDecimal(chbOther1Partner, txtOther1Partner.Text);
                            decimal? other2Partner = ParseToDecimal(chbOther2Partner, txtOther2Partner.Text);
                            decimal? other3Partner = ParseToDecimal(chbOther3Partner, txtOther3Partner.Text);
                            decimal? other4Partner = ParseToDecimal(chbOther4Partner, txtOther4Partner.Text);
                            decimal? other5Partner = ParseToDecimal(chbOther5Partner, txtOther5Partner.Text);
                            decimal? medicalAdviceAndPeriodiCheckPartner = ParseToDecimal(chbMedicalAdviceAndPeriodiCheckPartner, txtMedicalAdviceAndPeriodiCheckPartner.Text);
                            decimal? diagnosticTestsAndPregnancyAnnexPartner = ParseToDecimal(chbDiagnosticTestsAndPregnancyAnnexPartner, txtDiagnosticTestsAndPregnancyAnnexPartner.Text);
                            decimal? fastDiagnosisAndMedicalGuidancePartner = ParseToDecimal(chbFastDiagnosisAndMedicalGuidancePartner, txtFastDiagnosisAndMedicalGuidancePartner.Text);
                            decimal? psicologyPartner = ParseToDecimal(chbPsicologyPartner, txtPsicologyPartner.Text);
                            decimal? complementaryPartner = ParseToDecimal(chbComplementaryPartner, txtComplementaryPartner.Text);
                            decimal? toTheChildPartner = ParseToDecimal(chbToTheChildPartner, txtToTheChildPartner.Text);
                            int? insuranceTypePartner = null;
                            if (cbInsurenceTypePartner.SelectedItem != null)
                            {
                                insuranceTypePartner = ((PersonalAccidentsInsuranceType)cbInsurenceTypePartner.SelectedItem).PersonalAccidentsInsuranceTypeID;
                            }

                            partnerInsuranceDetails = new PersonalAccidentsInsurancesDetail()
                            {
                                CompensationForHospitalizationAmount = compensationForHospitalizationPartner,
                                CompensationForHospitalizationWaitingDays = txtHospitalizationDaysOfWaitingPartner.Text,
                                ComplementaryAmount = complementaryPartner,
                                DeathByAccidentAmount = deathByAccidentPartner,
                                DiagnosticTestsAndPregnancyAnnexAmount = diagnosticTestsAndPregnancyAnnexPartner,
                                DiagnosticTestsAndPregnancyAnnexType = txtDiagnosticTestsAndPregnancyAnnexTypePartner.Text,
                                DisabilityByAccidentAmount = disabilityByAccidentPartner,
                                ExtremeSportCancellationAmount = extremeSportCancellationPartner,
                                FastDiagnosisAndMedicalGuidanceAmount = fastDiagnosisAndMedicalGuidancePartner,
                                FracturesAndBurnsByAccidentAmount = fracturesAndBurnsByAccidentPartner,
                                IncapacityByAccidentAmount = incapacityByAccidentPartner,
                                IncapacityByAccidentPeriod = txtIncapacityByAccidentPeriod.Text,
                                IncapacityByAccidentWaitingDays = txtIncapacityByAccidentWaitingDaysPartner.Text,
                                IncapacityBySicknessAmount = incapacityBySicknessPartner,
                                IncapacityBySicknessPeriod = txtIncapacityBySicknessPeriod.Text,
                                IncapacityBySicknessWaitingDays = txtIncapacityBySicknessWaitingDaysPartner.Text,
                                InsuredTypeID = partnerTypeId,
                                IsCompensationForHospitalization = chbCompensationForHospitalizationPartner.IsChecked,
                                IsComplementary = chbComplementaryPartner.IsChecked,
                                IsDeathByAccident = chbDeathByAccidentPartner.IsChecked,
                                IsDiagnosticTestsAndPregnancyAnnex = chbDiagnosticTestsAndPregnancyAnnexPartner.IsChecked,
                                IsDisabilityByAccident = chbDisabilityByAccidentPartner.IsChecked,
                                IsExtremeSportCancellation = chbExtremeSportCancellationPartner.IsChecked,
                                IsFastDiagnosisAndMedicalGuidance = chbFastDiagnosisAndMedicalGuidancePartner.IsChecked,
                                IsFracturesAndBurnsByAccident = chbFracturesAndBurnsByAccidentPartner.IsChecked,
                                IsIncapacityByAccident = chbIncapacityByAccidentPartner.IsChecked,
                                IsIncapacityBySickness = chbIncapacityBySicknessPartner.IsChecked,
                                IsMedicalAdviceAndPeriodiCheck = chbMedicalAdviceAndPeriodiCheckPartner.IsChecked,
                                IsMedicalAndDentalCharges = chbMedicalAndDentalChargesPartner.IsChecked,
                                IsMedicalCharges = chbMedicalChargesPartner.IsChecked,
                                IsOther1 = chbOther1Partner.IsChecked,
                                IsOther2 = chbOther2Partner.IsChecked,
                                IsOther3 = chbOther3Partner.IsChecked,
                                IsOther4 = chbOther4Partner.IsChecked,
                                IsOther5 = chbOther5Partner.IsChecked,
                                IsPsicology = chbPsicologyPartner.IsChecked,
                                IsSiudByAccident = chbSiudiByAccidentPartner.IsChecked,
                                IsSports = chbSportsPartner.IsChecked,
                                IsToTheChild = chbToTheChildPartner.IsChecked,
                                IsTwoWheeledCancellation = chbTwoWheeledCancellationPartner.IsChecked,
                                IsWarRiskCoverage = chbWarRiskCoveragePartner.IsChecked,
                                MedicalAdviceAndPeriodiCheckAmount = medicalAdviceAndPeriodiCheckPartner,
                                MedicalAndDentalChargesAmount = medicalAndDentalChargesPartner,
                                MedicalChargesAmount = medicalChargesPartner,
                                Other1Amount = other1Partner,
                                Other2Amount = other2Partner,
                                Other3Amount = other3Partner,
                                Other4Amount = other4Partner,
                                Other5Amount = other5Partner,
                                Other1Name = txtOther1.Text,
                                Other2Name = txtOther2.Text,
                                Other3Name = txtOther3.Text,
                                Other4Name = txtOther4.Text,
                                Other5Name = txtOther5.Text,
                                PersonalAccidentsInsurenceTypeID = insuranceTypePartner,
                                PersonalAccidentsPolicyID = policyID,
                                PsicologyAmount = psicologyPartner,
                                SiudByAccidentAmount = siudByAccidentPartner,
                                SportsAmount = sportsPartner,
                                ToTheChildAmount = toTheChildPartner,
                                TwoWheeledCancellationAmount = twoWheeledCancellationPartner,
                                TwoWheeledCancellationUpToCC = txtUpToCCPartner.Text,
                                WarRiskCoverageAmount = warRiskCoveragePartner
                            };
                        }

                        if (insureds.FirstOrDefault(i => i.InsuredTypeID == childrenTypeId) != null)
                        {
                            decimal? deathByAccidentChildren = ParseToDecimal(chbDeathByAccidentChildren, txtDeathByAccidentChildren.Text);
                            decimal? disabilityByAccidentChildren = ParseToDecimal(chbDisabilityByAccidentChildren, txtDisabilityByAccidentChildren.Text);
                            decimal? fracturesAndBurnsByAccidentChildren = ParseToDecimal(chbFracturesAndBurnsByAccidentChildren, txtFracturesAndBurnsByAccidentChildren.Text);
                            decimal? compensationForHospitalizationChildren = ParseToDecimal(chbCompensationForHospitalizationChildren, txtCompensationForHospitalizationChildren.Text);
                            decimal? siudByAccidentChildren = ParseToDecimal(chbSiudiByAccidentChildren, txtSiudiByAccidentChildren.Text);
                            decimal? sportsChildren = ParseToDecimal(chbSportsChildren, txtSportsChildren.Text);
                            decimal? twoWheeledCancellationChildren = ParseToDecimal(chbTwoWheeledCancellationChildren, txtTwoWheeledCancellationChildren.Text);
                            decimal? extremeSportCancellationChildren = ParseToDecimal(chbExtremeSportCancellationChildren, txtExtremeSportCancellationChildren.Text);
                            decimal? medicalChargesChildren = ParseToDecimal(chbMedicalChargesChildren, txtMedicalChargesChildren.Text);
                            decimal? medicalAndDentalChargesChildren = ParseToDecimal(chbMedicalAndDentalChargesChildren, txtMedicalAndDentalChargesChildren.Text);
                            decimal? warRiskCoverageChildren = ParseToDecimal(chbWarRiskCoverageChildren, txtWarRiskCoverageChildren.Text);
                            decimal? incapacityByAccidentChildren = ParseToDecimal(chbIncapacityByAccidentChildren, txtIncapacityByAccidentChildren.Text);
                            decimal? incapacityBySicknessChildren = ParseToDecimal(chbIncapacityBySicknessChildren, txtIncapacityBySicknessChildren.Text);
                            decimal? other1Children = ParseToDecimal(chbOther1Children, txtOther1Children.Text);
                            decimal? other2Children = ParseToDecimal(chbOther2Children, txtOther2Children.Text);
                            decimal? other3Children = ParseToDecimal(chbOther3Children, txtOther3Children.Text);
                            decimal? other4Children = ParseToDecimal(chbOther4Children, txtOther4Children.Text);
                            decimal? other5Children = ParseToDecimal(chbOther5Children, txtOther5Children.Text);
                            decimal? medicalAdviceAndPeriodiCheckChildren = ParseToDecimal(chbMedicalAdviceAndPeriodiCheckChildren, txtMedicalAdviceAndPeriodiCheckChildren.Text);
                            decimal? diagnosticTestsAndPregnancyAnnexChildren = ParseToDecimal(chbDiagnosticTestsAndPregnancyAnnexChildren, txtDiagnosticTestsAndPregnancyAnnexChildren.Text);
                            decimal? fastDiagnosisAndMedicalGuidanceChildren = ParseToDecimal(chbFastDiagnosisAndMedicalGuidanceChildren, txtFastDiagnosisAndMedicalGuidanceChildren.Text);
                            decimal? psicologyChildren = ParseToDecimal(chbPsicologyChildren, txtPsicologyChildren.Text);
                            decimal? complementaryChildren = ParseToDecimal(chbComplementaryChildren, txtComplementaryChildren.Text);
                            decimal? toTheChildChildren = ParseToDecimal(chbToTheChildChildren, txtToTheChildChildren.Text);
                            int? insuranceTypeChildren = null;
                            if (cbInsurenceTypeChildren.SelectedItem != null)
                            {
                                insuranceTypeChildren = ((PersonalAccidentsInsuranceType)cbInsurenceTypeChildren.SelectedItem).PersonalAccidentsInsuranceTypeID;
                            }

                            childrenInsuranceDetails = new PersonalAccidentsInsurancesDetail()
                            {
                                CompensationForHospitalizationAmount = compensationForHospitalizationChildren,
                                CompensationForHospitalizationWaitingDays = txtHospitalizationDaysOfWaitingChildren.Text,
                                ComplementaryAmount = complementaryChildren,
                                DeathByAccidentAmount = deathByAccidentChildren,
                                DiagnosticTestsAndPregnancyAnnexAmount = diagnosticTestsAndPregnancyAnnexChildren,
                                DiagnosticTestsAndPregnancyAnnexType = txtDiagnosticTestsAndPregnancyAnnexTypeChildren.Text,
                                DisabilityByAccidentAmount = disabilityByAccidentChildren,
                                ExtremeSportCancellationAmount = extremeSportCancellationChildren,
                                FastDiagnosisAndMedicalGuidanceAmount = fastDiagnosisAndMedicalGuidanceChildren,
                                FracturesAndBurnsByAccidentAmount = fracturesAndBurnsByAccidentChildren,
                                IncapacityByAccidentAmount = incapacityByAccidentChildren,
                                IncapacityByAccidentPeriod = txtIncapacityByAccidentPeriod.Text,
                                IncapacityByAccidentWaitingDays = txtIncapacityByAccidentWaitingDaysChildren.Text,
                                IncapacityBySicknessAmount = incapacityBySicknessChildren,
                                IncapacityBySicknessPeriod = txtIncapacityBySicknessPeriod.Text,
                                IncapacityBySicknessWaitingDays = txtIncapacityBySicknessWaitingDaysChildren.Text,
                                InsuredTypeID = childrenTypeId,
                                IsCompensationForHospitalization = chbCompensationForHospitalizationChildren.IsChecked,
                                IsComplementary = chbComplementaryChildren.IsChecked,
                                IsDeathByAccident = chbDeathByAccidentChildren.IsChecked,
                                IsDiagnosticTestsAndPregnancyAnnex = chbDiagnosticTestsAndPregnancyAnnexChildren.IsChecked,
                                IsDisabilityByAccident = chbDisabilityByAccidentChildren.IsChecked,
                                IsExtremeSportCancellation = chbExtremeSportCancellationChildren.IsChecked,
                                IsFastDiagnosisAndMedicalGuidance = chbFastDiagnosisAndMedicalGuidanceChildren.IsChecked,
                                IsFracturesAndBurnsByAccident = chbFracturesAndBurnsByAccidentChildren.IsChecked,
                                IsIncapacityByAccident = chbIncapacityByAccidentChildren.IsChecked,
                                IsIncapacityBySickness = chbIncapacityBySicknessChildren.IsChecked,
                                IsMedicalAdviceAndPeriodiCheck = chbMedicalAdviceAndPeriodiCheckChildren.IsChecked,
                                IsMedicalAndDentalCharges = chbMedicalAndDentalChargesChildren.IsChecked,
                                IsMedicalCharges = chbMedicalChargesChildren.IsChecked,
                                IsOther1 = chbOther1Children.IsChecked,
                                IsOther2 = chbOther2Children.IsChecked,
                                IsOther3 = chbOther3Children.IsChecked,
                                IsOther4 = chbOther4Children.IsChecked,
                                IsOther5 = chbOther5Children.IsChecked,
                                IsPsicology = chbPsicologyChildren.IsChecked,
                                IsSiudByAccident = chbSiudiByAccidentChildren.IsChecked,
                                IsSports = chbSportsChildren.IsChecked,
                                IsToTheChild = chbToTheChildChildren.IsChecked,
                                IsTwoWheeledCancellation = chbTwoWheeledCancellationChildren.IsChecked,
                                IsWarRiskCoverage = chbWarRiskCoverageChildren.IsChecked,
                                MedicalAdviceAndPeriodiCheckAmount = medicalAdviceAndPeriodiCheckChildren,
                                MedicalAndDentalChargesAmount = medicalAndDentalChargesChildren,
                                MedicalChargesAmount = medicalChargesChildren,
                                Other1Amount = other1Children,
                                Other2Amount = other2Children,
                                Other3Amount = other3Children,
                                Other4Amount = other4Children,
                                Other5Amount = other5Children,
                                Other1Name = txtOther1.Text,
                                Other2Name = txtOther2.Text,
                                Other3Name = txtOther3.Text,
                                Other4Name = txtOther4.Text,
                                Other5Name = txtOther5.Text,
                                PersonalAccidentsInsurenceTypeID = insuranceTypeChildren,
                                PersonalAccidentsPolicyID = policyID,
                                PsicologyAmount = psicologyChildren,
                                SiudByAccidentAmount = siudByAccidentChildren,
                                SportsAmount = sportsChildren,
                                ToTheChildAmount = toTheChildChildren,
                                TwoWheeledCancellationAmount = twoWheeledCancellationChildren,
                                TwoWheeledCancellationUpToCC = txtUpToCCChildren.Text,
                                WarRiskCoverageAmount = warRiskCoverageChildren
                            };

                        }

                    }
                    personalAccidentsLogic.InsertInsurancesDetails(new PersonalAccidentsInsurancesDetail[] { principalInsuranceDetails, partnerInsuranceDetails, childrenInsuranceDetails });

                    if (coverages.Count > 0)
                    {
                        coveragesLogic.InsertCoveragesToPersonalAccidentsPolicy(policyID, coverages, isAddition);
                    }
                    if (trackings.Count > 0)
                    {
                        trackingLogics.InsertPersonalAccidentsTrackings(policyID, trackings, isAddition);
                    }
                    if (chbStandardMoneyCollection.IsChecked == true)
                    {
                        int standardMoneyCollectionId = moneyCollectionLogic.InsertStandardMoneyCollection(standardMoneyCollection, new Insurance() { InsuranceName = "תאונות אישיות" }, policyID);
                        if (standardMoneyCollectionTrackings != null)
                        {
                            moneyCollectionLogic.InsertTrackingsToStandardMoneyCollection(standardMoneyCollectionId, standardMoneyCollectionTrackings);
                        }
                    }
                }
                else
                {
                    personalAccidentsLogic.UpdatePersonalAccidentsPolicy(personalAccidentsPolicy.PersonalAccidentsPolicyID, txtPolicyNumber.Text, principalAgentNumberID, subAgentNumberID, (DateTime)dpStartDate.SelectedDate, (DateTime)dpEndDate.SelectedDate, ((InsuranceCompany)cbCompany.SelectedItem).CompanyID, ((ComboBoxItem)cbCurrency.SelectedItem).Content.ToString(), paymentMode, paymentNumbers, dpPaymentDate.SelectedDate, premium1, premium2, premium3, premium4, inscriptionFees, policyFees, projectionFees, stampFees, handlingFees, totalPremium, totalFees, totalCredit, txtComments.Text);
                    if (!isOffer)
                    {
                        string[] dirs = Directory.GetDirectories(personalAccidentsPath + @"\תאונות אישיות\", "*" + personalAccidentsPolicy.PolicyNumber + "*");
                        if (dirs.Count() > 0)
                        {
                            path = dirs[0];
                        }
                        newPath = personalAccidentsPath + @"\תאונות אישיות\תאונות אישיות " + txtPolicyNumber.Text;
                    }
                    else
                    {
                        string[] dirs = Directory.GetDirectories(personalAccidentsPath + @"\הצעות\תאונות אישיות", "*" + personalAccidentsPolicy.PolicyNumber + "*");
                        if (dirs.Count() > 0)
                        {
                            path = dirs[0];
                        }
                        newPath = personalAccidentsPath + @"\הצעות\תאונות אישיות\" + txtPolicyNumber.Text;
                    }

                    SaveUnsavedInsured();
                    personalAccidentsLogic.InsertAdditionalInsureds(personalAccidentsPolicy.PersonalAccidentsPolicyID, insureds, isAddition);

                    principalInsuranceDetails.CompensationForHospitalizationAmount = compensationForHospitalizationPrincipal;
                    principalInsuranceDetails.CompensationForHospitalizationWaitingDays = txtHospitalizationDaysOfWaitingPrincipal.Text;
                    principalInsuranceDetails.ComplementaryAmount = complementaryPrincipal;
                    principalInsuranceDetails.DeathByAccidentAmount = deathByAccidentPrincipal;
                    principalInsuranceDetails.DiagnosticTestsAndPregnancyAnnexAmount = diagnosticTestsAndPregnancyAnnexPrincipal;
                    principalInsuranceDetails.DiagnosticTestsAndPregnancyAnnexType = txtDiagnosticTestsAndPregnancyAnnexTypePrincipal.Text;
                    principalInsuranceDetails.DisabilityByAccidentAmount = disabilityByAccidentPrincipal;
                    principalInsuranceDetails.ExtremeSportCancellationAmount = extremeSportCancellationPrincipal;
                    principalInsuranceDetails.FastDiagnosisAndMedicalGuidanceAmount = fastDiagnosisAndMedicalGuidancePrincipal;
                    principalInsuranceDetails.FracturesAndBurnsByAccidentAmount = fracturesAndBurnsByAccidentPrincipal;
                    principalInsuranceDetails.IncapacityByAccidentAmount = incapacityByAccidentPrincipal;
                    principalInsuranceDetails.IncapacityByAccidentPeriod = txtIncapacityByAccidentPeriod.Text;
                    principalInsuranceDetails.IncapacityByAccidentWaitingDays = txtIncapacityByAccidentWaitingDaysPrincipal.Text;
                    principalInsuranceDetails.IncapacityBySicknessAmount = incapacityBySicknessPrincipal;
                    principalInsuranceDetails.IncapacityBySicknessPeriod = txtIncapacityBySicknessPeriod.Text;
                    principalInsuranceDetails.IncapacityBySicknessWaitingDays = txtIncapacityBySicknessWaitingDaysPrincipal.Text;
                    principalInsuranceDetails.InsuredTypeID = insuredTypeID;
                    principalInsuranceDetails.IsCompensationForHospitalization = chbCompensationForHospitalizationPrincipal.IsChecked;
                    principalInsuranceDetails.IsComplementary = chbComplementaryPrincipal.IsChecked;
                    principalInsuranceDetails.IsDeathByAccident = chbDeathByAccidentPrincipal.IsChecked;
                    principalInsuranceDetails.IsDiagnosticTestsAndPregnancyAnnex = chbDiagnosticTestsAndPregnancyAnnexPrincipal.IsChecked;
                    principalInsuranceDetails.IsDisabilityByAccident = chbDisabilityByAccidentPrincipal.IsChecked;
                    principalInsuranceDetails.IsExtremeSportCancellation = chbExtremeSportCancellationPrincipal.IsChecked;
                    principalInsuranceDetails.IsFastDiagnosisAndMedicalGuidance = chbFastDiagnosisAndMedicalGuidancePrincipal.IsChecked;
                    principalInsuranceDetails.IsFracturesAndBurnsByAccident = chbFracturesAndBurnsByAccidentPrincipal.IsChecked;
                    principalInsuranceDetails.IsIncapacityByAccident = chbIncapacityByAccidentPrincipal.IsChecked;
                    principalInsuranceDetails.IsIncapacityBySickness = chbIncapacityBySicknessPrincipal.IsChecked;
                    principalInsuranceDetails.IsMedicalAdviceAndPeriodiCheck = chbMedicalAdviceAndPeriodiCheckPrincipal.IsChecked;
                    principalInsuranceDetails.IsMedicalAndDentalCharges = chbMedicalAndDentalChargesPrincipal.IsChecked;
                    principalInsuranceDetails.IsMedicalCharges = chbMedicalChargesPrincipal.IsChecked;
                    principalInsuranceDetails.IsOther1 = chbOther1Principal.IsChecked;
                    principalInsuranceDetails.IsOther2 = chbOther2Principal.IsChecked;
                    principalInsuranceDetails.IsOther3 = chbOther3Principal.IsChecked;
                    principalInsuranceDetails.IsOther4 = chbOther4Principal.IsChecked;
                    principalInsuranceDetails.IsOther5 = chbOther5Principal.IsChecked;
                    principalInsuranceDetails.IsPsicology = chbPsicologyPrincipal.IsChecked;
                    principalInsuranceDetails.IsSiudByAccident = chbSiudiByAccidentPrincipal.IsChecked;
                    principalInsuranceDetails.IsSports = chbSportsPrincipal.IsChecked;
                    principalInsuranceDetails.IsToTheChild = chbToTheChildPrincipal.IsChecked;
                    principalInsuranceDetails.IsTwoWheeledCancellation = chbTwoWheeledCancellationPrincipal.IsChecked;
                    principalInsuranceDetails.IsWarRiskCoverage = chbWarRiskCoveragePrincipal.IsChecked;
                    principalInsuranceDetails.MedicalAdviceAndPeriodiCheckAmount = medicalAdviceAndPeriodiCheckPrincipal;
                    principalInsuranceDetails.MedicalAndDentalChargesAmount = medicalAndDentalChargesPrincipal;
                    principalInsuranceDetails.MedicalChargesAmount = medicalChargesPrincipal;
                    principalInsuranceDetails.Other1Amount = other1Principal;
                    principalInsuranceDetails.Other2Amount = other2Principal;
                    principalInsuranceDetails.Other3Amount = other3Principal;
                    principalInsuranceDetails.Other4Amount = other4Principal;
                    principalInsuranceDetails.Other5Amount = other5Principal;
                    principalInsuranceDetails.Other1Name = txtOther1.Text;
                    principalInsuranceDetails.Other2Name = txtOther2.Text;
                    principalInsuranceDetails.Other3Name = txtOther3.Text;
                    principalInsuranceDetails.Other4Name = txtOther4.Text;
                    principalInsuranceDetails.Other5Name = txtOther5.Text;
                    principalInsuranceDetails.PersonalAccidentsInsurenceTypeID = insuranceTypePrincipal;
                    principalInsuranceDetails.PsicologyAmount = psicologyPrincipal;
                    principalInsuranceDetails.SiudByAccidentAmount = siudByAccidentPrincipal;
                    principalInsuranceDetails.SportsAmount = sportsPrincipal;
                    principalInsuranceDetails.ToTheChildAmount = toTheChildPrincipal;
                    principalInsuranceDetails.TwoWheeledCancellationAmount = twoWheeledCancellationPrincipal;
                    principalInsuranceDetails.TwoWheeledCancellationUpToCC = txtUpToCCPrincipal.Text;
                    principalInsuranceDetails.WarRiskCoverageAmount = warRiskCoveragePrincipal;


                    if (insureds.Count > 0)
                    {

                        int partnerTypeId = (personalAccidentsLogic.GetInsuredTypes().FirstOrDefault(i => i.InsuredTypeName == "בן/בת זוג")).InsuredTypeID;
                        int childrenTypeId = (personalAccidentsLogic.GetInsuredTypes().FirstOrDefault(i => i.InsuredTypeName == "ילד/ילדה")).InsuredTypeID;

                        if (insureds.FirstOrDefault(i => i.InsuredTypeID == partnerTypeId) != null)
                        {
                            decimal? deathByAccidentPartner = ParseToDecimal(chbDeathByAccidentPartner, txtDeathByAccidentPartner.Text);
                            decimal? disabilityByAccidentPartner = ParseToDecimal(chbDisabilityByAccidentPartner, txtDisabilityByAccidentPartner.Text);
                            decimal? fracturesAndBurnsByAccidentPartner = ParseToDecimal(chbFracturesAndBurnsByAccidentPartner, txtFracturesAndBurnsByAccidentPartner.Text);
                            decimal? compensationForHospitalizationPartner = ParseToDecimal(chbCompensationForHospitalizationPartner, txtCompensationForHospitalizationPartner.Text);
                            decimal? siudByAccidentPartner = ParseToDecimal(chbSiudiByAccidentPartner, txtSiudiByAccidentPartner.Text);
                            decimal? sportsPartner = ParseToDecimal(chbSportsPartner, txtSportsPartner.Text);
                            decimal? twoWheeledCancellationPartner = ParseToDecimal(chbTwoWheeledCancellationPartner, txtTwoWheeledCancellationPartner.Text);
                            decimal? extremeSportCancellationPartner = ParseToDecimal(chbExtremeSportCancellationPartner, txtExtremeSportCancellationPartner.Text);
                            decimal? medicalChargesPartner = ParseToDecimal(chbMedicalChargesPartner, txtMedicalChargesPartner.Text);
                            decimal? medicalAndDentalChargesPartner = ParseToDecimal(chbMedicalAndDentalChargesPartner, txtMedicalAndDentalChargesPartner.Text);
                            decimal? warRiskCoveragePartner = ParseToDecimal(chbWarRiskCoveragePartner, txtWarRiskCoveragePartner.Text);
                            decimal? incapacityByAccidentPartner = ParseToDecimal(chbIncapacityByAccidentPartner, txtIncapacityByAccidentPartner.Text);
                            decimal? incapacityBySicknessPartner = ParseToDecimal(chbIncapacityBySicknessPartner, txtIncapacityBySicknessPartner.Text);
                            decimal? other1Partner = ParseToDecimal(chbOther1Partner, txtOther1Partner.Text);
                            decimal? other2Partner = ParseToDecimal(chbOther2Partner, txtOther2Partner.Text);
                            decimal? other3Partner = ParseToDecimal(chbOther3Partner, txtOther3Partner.Text);
                            decimal? other4Partner = ParseToDecimal(chbOther4Partner, txtOther4Partner.Text);
                            decimal? other5Partner = ParseToDecimal(chbOther5Partner, txtOther5Partner.Text);
                            decimal? medicalAdviceAndPeriodiCheckPartner = ParseToDecimal(chbMedicalAdviceAndPeriodiCheckPartner, txtMedicalAdviceAndPeriodiCheckPartner.Text);
                            decimal? diagnosticTestsAndPregnancyAnnexPartner = ParseToDecimal(chbDiagnosticTestsAndPregnancyAnnexPartner, txtDiagnosticTestsAndPregnancyAnnexPartner.Text);
                            decimal? fastDiagnosisAndMedicalGuidancePartner = ParseToDecimal(chbFastDiagnosisAndMedicalGuidancePartner, txtFastDiagnosisAndMedicalGuidancePartner.Text);
                            decimal? psicologyPartner = ParseToDecimal(chbPsicologyPartner, txtPsicologyPartner.Text);
                            decimal? complementaryPartner = ParseToDecimal(chbComplementaryPartner, txtComplementaryPartner.Text);
                            decimal? toTheChildPartner = ParseToDecimal(chbToTheChildPartner, txtToTheChildPartner.Text);
                            int? insuranceTypePartner = null;
                            if (cbInsurenceTypePartner.SelectedItem != null)
                            {
                                insuranceTypePartner = ((PersonalAccidentsInsuranceType)cbInsurenceTypePartner.SelectedItem).PersonalAccidentsInsuranceTypeID;
                            }

                            if (partnerInsuranceDetails == null)
                            {
                                partnerInsuranceDetails = new PersonalAccidentsInsurancesDetail()
                                {
                                    CompensationForHospitalizationAmount = compensationForHospitalizationPartner,
                                    CompensationForHospitalizationWaitingDays = txtHospitalizationDaysOfWaitingPartner.Text,
                                    ComplementaryAmount = complementaryPartner,
                                    DeathByAccidentAmount = deathByAccidentPartner,
                                    DiagnosticTestsAndPregnancyAnnexAmount = diagnosticTestsAndPregnancyAnnexPartner,
                                    DiagnosticTestsAndPregnancyAnnexType = txtDiagnosticTestsAndPregnancyAnnexTypePartner.Text,
                                    DisabilityByAccidentAmount = disabilityByAccidentPartner,
                                    ExtremeSportCancellationAmount = extremeSportCancellationPartner,
                                    FastDiagnosisAndMedicalGuidanceAmount = fastDiagnosisAndMedicalGuidancePartner,
                                    FracturesAndBurnsByAccidentAmount = fracturesAndBurnsByAccidentPartner,
                                    IncapacityByAccidentAmount = incapacityByAccidentPartner,
                                    IncapacityByAccidentPeriod = txtIncapacityByAccidentPeriod.Text,
                                    IncapacityByAccidentWaitingDays = txtIncapacityByAccidentWaitingDaysPartner.Text,
                                    IncapacityBySicknessAmount = incapacityBySicknessPartner,
                                    IncapacityBySicknessPeriod = txtIncapacityBySicknessPeriod.Text,
                                    IncapacityBySicknessWaitingDays = txtIncapacityBySicknessWaitingDaysPartner.Text,
                                    InsuredTypeID = partnerTypeId,
                                    IsCompensationForHospitalization = chbCompensationForHospitalizationPartner.IsChecked,
                                    IsComplementary = chbComplementaryPartner.IsChecked,
                                    IsDeathByAccident = chbDeathByAccidentPartner.IsChecked,
                                    IsDiagnosticTestsAndPregnancyAnnex = chbDiagnosticTestsAndPregnancyAnnexPartner.IsChecked,
                                    IsDisabilityByAccident = chbDisabilityByAccidentPartner.IsChecked,
                                    IsExtremeSportCancellation = chbExtremeSportCancellationPartner.IsChecked,
                                    IsFastDiagnosisAndMedicalGuidance = chbFastDiagnosisAndMedicalGuidancePartner.IsChecked,
                                    IsFracturesAndBurnsByAccident = chbFracturesAndBurnsByAccidentPartner.IsChecked,
                                    IsIncapacityByAccident = chbIncapacityByAccidentPartner.IsChecked,
                                    IsIncapacityBySickness = chbIncapacityBySicknessPartner.IsChecked,
                                    IsMedicalAdviceAndPeriodiCheck = chbMedicalAdviceAndPeriodiCheckPartner.IsChecked,
                                    IsMedicalAndDentalCharges = chbMedicalAndDentalChargesPartner.IsChecked,
                                    IsMedicalCharges = chbMedicalChargesPartner.IsChecked,
                                    IsOther1 = chbOther1Partner.IsChecked,
                                    IsOther2 = chbOther2Partner.IsChecked,
                                    IsOther3 = chbOther3Partner.IsChecked,
                                    IsOther4 = chbOther4Partner.IsChecked,
                                    IsOther5 = chbOther5Partner.IsChecked,
                                    IsPsicology = chbPsicologyPartner.IsChecked,
                                    IsSiudByAccident = chbSiudiByAccidentPartner.IsChecked,
                                    IsSports = chbSportsPartner.IsChecked,
                                    IsToTheChild = chbToTheChildPartner.IsChecked,
                                    IsTwoWheeledCancellation = chbTwoWheeledCancellationPartner.IsChecked,
                                    IsWarRiskCoverage = chbWarRiskCoveragePartner.IsChecked,
                                    MedicalAdviceAndPeriodiCheckAmount = medicalAdviceAndPeriodiCheckPartner,
                                    MedicalAndDentalChargesAmount = medicalAndDentalChargesPartner,
                                    MedicalChargesAmount = medicalChargesPartner,
                                    Other1Amount = other1Partner,
                                    Other2Amount = other2Partner,
                                    Other3Amount = other3Partner,
                                    Other4Amount = other4Partner,
                                    Other5Amount = other5Partner,
                                    Other1Name = txtOther1.Text,
                                    Other2Name = txtOther2.Text,
                                    Other3Name = txtOther3.Text,
                                    Other4Name = txtOther4.Text,
                                    Other5Name = txtOther5.Text,
                                    PersonalAccidentsInsurenceTypeID = insuranceTypePartner,
                                    PersonalAccidentsPolicyID = personalAccidentsPolicy.PersonalAccidentsPolicyID,
                                    PsicologyAmount = psicologyPartner,
                                    SiudByAccidentAmount = siudByAccidentPartner,
                                    SportsAmount = sportsPartner,
                                    ToTheChildAmount = toTheChildPartner,
                                    TwoWheeledCancellationAmount = twoWheeledCancellationPartner,
                                    TwoWheeledCancellationUpToCC = txtUpToCCPartner.Text,
                                    WarRiskCoverageAmount = warRiskCoveragePartner
                                };
                            }
                            else
                            {
                                partnerInsuranceDetails.CompensationForHospitalizationAmount = compensationForHospitalizationPartner;
                                partnerInsuranceDetails.CompensationForHospitalizationWaitingDays = txtHospitalizationDaysOfWaitingPartner.Text;
                                partnerInsuranceDetails.ComplementaryAmount = complementaryPartner;
                                partnerInsuranceDetails.DeathByAccidentAmount = deathByAccidentPartner;
                                partnerInsuranceDetails.DiagnosticTestsAndPregnancyAnnexAmount = diagnosticTestsAndPregnancyAnnexPartner;
                                partnerInsuranceDetails.DiagnosticTestsAndPregnancyAnnexType = txtDiagnosticTestsAndPregnancyAnnexTypePartner.Text;
                                partnerInsuranceDetails.DisabilityByAccidentAmount = disabilityByAccidentPartner;
                                partnerInsuranceDetails.ExtremeSportCancellationAmount = extremeSportCancellationPartner;
                                partnerInsuranceDetails.FastDiagnosisAndMedicalGuidanceAmount = fastDiagnosisAndMedicalGuidancePartner;
                                partnerInsuranceDetails.FracturesAndBurnsByAccidentAmount = fracturesAndBurnsByAccidentPartner;
                                partnerInsuranceDetails.IncapacityByAccidentAmount = incapacityByAccidentPartner;
                                partnerInsuranceDetails.IncapacityByAccidentPeriod = txtIncapacityByAccidentPeriod.Text;
                                partnerInsuranceDetails.IncapacityByAccidentWaitingDays = txtIncapacityByAccidentWaitingDaysPartner.Text;
                                partnerInsuranceDetails.IncapacityBySicknessAmount = incapacityBySicknessPartner;
                                partnerInsuranceDetails.IncapacityBySicknessPeriod = txtIncapacityBySicknessPeriod.Text;
                                partnerInsuranceDetails.IncapacityBySicknessWaitingDays = txtIncapacityBySicknessWaitingDaysPartner.Text;
                                partnerInsuranceDetails.InsuredTypeID = partnerTypeId;
                                partnerInsuranceDetails.IsCompensationForHospitalization = chbCompensationForHospitalizationPartner.IsChecked;
                                partnerInsuranceDetails.IsComplementary = chbComplementaryPartner.IsChecked;
                                partnerInsuranceDetails.IsDeathByAccident = chbDeathByAccidentPartner.IsChecked;
                                partnerInsuranceDetails.IsDiagnosticTestsAndPregnancyAnnex = chbDiagnosticTestsAndPregnancyAnnexPartner.IsChecked;
                                partnerInsuranceDetails.IsDisabilityByAccident = chbDisabilityByAccidentPartner.IsChecked;
                                partnerInsuranceDetails.IsExtremeSportCancellation = chbExtremeSportCancellationPartner.IsChecked;
                                partnerInsuranceDetails.IsFastDiagnosisAndMedicalGuidance = chbFastDiagnosisAndMedicalGuidancePartner.IsChecked;
                                partnerInsuranceDetails.IsFracturesAndBurnsByAccident = chbFracturesAndBurnsByAccidentPartner.IsChecked;
                                partnerInsuranceDetails.IsIncapacityByAccident = chbIncapacityByAccidentPartner.IsChecked;
                                partnerInsuranceDetails.IsIncapacityBySickness = chbIncapacityBySicknessPartner.IsChecked;
                                partnerInsuranceDetails.IsMedicalAdviceAndPeriodiCheck = chbMedicalAdviceAndPeriodiCheckPartner.IsChecked;
                                partnerInsuranceDetails.IsMedicalAndDentalCharges = chbMedicalAndDentalChargesPartner.IsChecked;
                                partnerInsuranceDetails.IsMedicalCharges = chbMedicalChargesPartner.IsChecked;
                                partnerInsuranceDetails.IsOther1 = chbOther1Partner.IsChecked;
                                partnerInsuranceDetails.IsOther2 = chbOther2Partner.IsChecked;
                                partnerInsuranceDetails.IsOther3 = chbOther3Partner.IsChecked;
                                partnerInsuranceDetails.IsOther4 = chbOther4Partner.IsChecked;
                                partnerInsuranceDetails.IsOther5 = chbOther5Partner.IsChecked;
                                partnerInsuranceDetails.IsPsicology = chbPsicologyPartner.IsChecked;
                                partnerInsuranceDetails.IsSiudByAccident = chbSiudiByAccidentPartner.IsChecked;
                                partnerInsuranceDetails.IsSports = chbSportsPartner.IsChecked;
                                partnerInsuranceDetails.IsToTheChild = chbToTheChildPartner.IsChecked;
                                partnerInsuranceDetails.IsTwoWheeledCancellation = chbTwoWheeledCancellationPartner.IsChecked;
                                partnerInsuranceDetails.IsWarRiskCoverage = chbWarRiskCoveragePartner.IsChecked;
                                partnerInsuranceDetails.MedicalAdviceAndPeriodiCheckAmount = medicalAdviceAndPeriodiCheckPartner;
                                partnerInsuranceDetails.MedicalAndDentalChargesAmount = medicalAndDentalChargesPartner;
                                partnerInsuranceDetails.MedicalChargesAmount = medicalChargesPartner;
                                partnerInsuranceDetails.Other1Amount = other1Partner;
                                partnerInsuranceDetails.Other2Amount = other2Partner;
                                partnerInsuranceDetails.Other3Amount = other3Partner;
                                partnerInsuranceDetails.Other4Amount = other4Partner;
                                partnerInsuranceDetails.Other5Amount = other5Partner;
                                partnerInsuranceDetails.Other1Name = txtOther1.Text;
                                partnerInsuranceDetails.Other2Name = txtOther2.Text;
                                partnerInsuranceDetails.Other3Name = txtOther3.Text;
                                partnerInsuranceDetails.Other4Name = txtOther4.Text;
                                partnerInsuranceDetails.Other5Name = txtOther5.Text;
                                partnerInsuranceDetails.PersonalAccidentsInsurenceTypeID = insuranceTypePartner;
                                partnerInsuranceDetails.PsicologyAmount = psicologyPartner;
                                partnerInsuranceDetails.SiudByAccidentAmount = siudByAccidentPartner;
                                partnerInsuranceDetails.SportsAmount = sportsPartner;
                                partnerInsuranceDetails.ToTheChildAmount = toTheChildPartner;
                                partnerInsuranceDetails.TwoWheeledCancellationAmount = twoWheeledCancellationPartner;
                                partnerInsuranceDetails.TwoWheeledCancellationUpToCC = txtUpToCCPartner.Text;
                                partnerInsuranceDetails.WarRiskCoverageAmount = warRiskCoveragePartner;
                            }
                        }

                        if (insureds.FirstOrDefault(i => i.InsuredTypeID == childrenTypeId) != null)
                        {
                            decimal? deathByAccidentChildren = ParseToDecimal(chbDeathByAccidentChildren, txtDeathByAccidentChildren.Text);
                            decimal? disabilityByAccidentChildren = ParseToDecimal(chbDisabilityByAccidentChildren, txtDisabilityByAccidentChildren.Text);
                            decimal? fracturesAndBurnsByAccidentChildren = ParseToDecimal(chbFracturesAndBurnsByAccidentChildren, txtFracturesAndBurnsByAccidentChildren.Text);
                            decimal? compensationForHospitalizationChildren = ParseToDecimal(chbCompensationForHospitalizationChildren, txtCompensationForHospitalizationChildren.Text);
                            decimal? siudByAccidentChildren = ParseToDecimal(chbSiudiByAccidentChildren, txtSiudiByAccidentChildren.Text);
                            decimal? sportsChildren = ParseToDecimal(chbSportsChildren, txtSportsChildren.Text);
                            decimal? twoWheeledCancellationChildren = ParseToDecimal(chbTwoWheeledCancellationChildren, txtTwoWheeledCancellationChildren.Text);
                            decimal? extremeSportCancellationChildren = ParseToDecimal(chbExtremeSportCancellationChildren, txtExtremeSportCancellationChildren.Text);
                            decimal? medicalChargesChildren = ParseToDecimal(chbMedicalChargesChildren, txtMedicalChargesChildren.Text);
                            decimal? medicalAndDentalChargesChildren = ParseToDecimal(chbMedicalAndDentalChargesChildren, txtMedicalAndDentalChargesChildren.Text);
                            decimal? warRiskCoverageChildren = ParseToDecimal(chbWarRiskCoverageChildren, txtWarRiskCoverageChildren.Text);
                            decimal? incapacityByAccidentChildren = ParseToDecimal(chbIncapacityByAccidentChildren, txtIncapacityByAccidentChildren.Text);
                            decimal? incapacityBySicknessChildren = ParseToDecimal(chbIncapacityBySicknessChildren, txtIncapacityBySicknessChildren.Text);
                            decimal? other1Children = ParseToDecimal(chbOther1Children, txtOther1Children.Text);
                            decimal? other2Children = ParseToDecimal(chbOther2Children, txtOther2Children.Text);
                            decimal? other3Children = ParseToDecimal(chbOther3Children, txtOther3Children.Text);
                            decimal? other4Children = ParseToDecimal(chbOther4Children, txtOther4Children.Text);
                            decimal? other5Children = ParseToDecimal(chbOther5Children, txtOther5Children.Text);
                            decimal? medicalAdviceAndPeriodiCheckChildren = ParseToDecimal(chbMedicalAdviceAndPeriodiCheckChildren, txtMedicalAdviceAndPeriodiCheckChildren.Text);
                            decimal? diagnosticTestsAndPregnancyAnnexChildren = ParseToDecimal(chbDiagnosticTestsAndPregnancyAnnexChildren, txtDiagnosticTestsAndPregnancyAnnexChildren.Text);
                            decimal? fastDiagnosisAndMedicalGuidanceChildren = ParseToDecimal(chbFastDiagnosisAndMedicalGuidanceChildren, txtFastDiagnosisAndMedicalGuidanceChildren.Text);
                            decimal? psicologyChildren = ParseToDecimal(chbPsicologyChildren, txtPsicologyChildren.Text);
                            decimal? complementaryChildren = ParseToDecimal(chbComplementaryChildren, txtComplementaryChildren.Text);
                            decimal? toTheChildChildren = ParseToDecimal(chbToTheChildChildren, txtToTheChildChildren.Text);
                            int? insuranceTypeChildren = null;
                            if (cbInsurenceTypeChildren.SelectedItem != null)
                            {
                                insuranceTypeChildren = ((PersonalAccidentsInsuranceType)cbInsurenceTypeChildren.SelectedItem).PersonalAccidentsInsuranceTypeID;
                            }

                            if (childrenInsuranceDetails == null)
                            {
                                childrenInsuranceDetails = new PersonalAccidentsInsurancesDetail()
                                {
                                    CompensationForHospitalizationAmount = compensationForHospitalizationChildren,
                                    CompensationForHospitalizationWaitingDays = txtHospitalizationDaysOfWaitingChildren.Text,
                                    ComplementaryAmount = complementaryChildren,
                                    DeathByAccidentAmount = deathByAccidentChildren,
                                    DiagnosticTestsAndPregnancyAnnexAmount = diagnosticTestsAndPregnancyAnnexChildren,
                                    DiagnosticTestsAndPregnancyAnnexType = txtDiagnosticTestsAndPregnancyAnnexTypeChildren.Text,
                                    DisabilityByAccidentAmount = disabilityByAccidentChildren,
                                    ExtremeSportCancellationAmount = extremeSportCancellationChildren,
                                    FastDiagnosisAndMedicalGuidanceAmount = fastDiagnosisAndMedicalGuidanceChildren,
                                    FracturesAndBurnsByAccidentAmount = fracturesAndBurnsByAccidentChildren,
                                    IncapacityByAccidentAmount = incapacityByAccidentChildren,
                                    IncapacityByAccidentPeriod = txtIncapacityByAccidentPeriod.Text,
                                    IncapacityByAccidentWaitingDays = txtIncapacityByAccidentWaitingDaysChildren.Text,
                                    IncapacityBySicknessAmount = incapacityBySicknessChildren,
                                    IncapacityBySicknessPeriod = txtIncapacityBySicknessPeriod.Text,
                                    IncapacityBySicknessWaitingDays = txtIncapacityBySicknessWaitingDaysChildren.Text,
                                    InsuredTypeID = childrenTypeId,
                                    IsCompensationForHospitalization = chbCompensationForHospitalizationChildren.IsChecked,
                                    IsComplementary = chbComplementaryChildren.IsChecked,
                                    IsDeathByAccident = chbDeathByAccidentChildren.IsChecked,
                                    IsDiagnosticTestsAndPregnancyAnnex = chbDiagnosticTestsAndPregnancyAnnexChildren.IsChecked,
                                    IsDisabilityByAccident = chbDisabilityByAccidentChildren.IsChecked,
                                    IsExtremeSportCancellation = chbExtremeSportCancellationChildren.IsChecked,
                                    IsFastDiagnosisAndMedicalGuidance = chbFastDiagnosisAndMedicalGuidanceChildren.IsChecked,
                                    IsFracturesAndBurnsByAccident = chbFracturesAndBurnsByAccidentChildren.IsChecked,
                                    IsIncapacityByAccident = chbIncapacityByAccidentChildren.IsChecked,
                                    IsIncapacityBySickness = chbIncapacityBySicknessChildren.IsChecked,
                                    IsMedicalAdviceAndPeriodiCheck = chbMedicalAdviceAndPeriodiCheckChildren.IsChecked,
                                    IsMedicalAndDentalCharges = chbMedicalAndDentalChargesChildren.IsChecked,
                                    IsMedicalCharges = chbMedicalChargesChildren.IsChecked,
                                    IsOther1 = chbOther1Children.IsChecked,
                                    IsOther2 = chbOther2Children.IsChecked,
                                    IsOther3 = chbOther3Children.IsChecked,
                                    IsOther4 = chbOther4Children.IsChecked,
                                    IsOther5 = chbOther5Children.IsChecked,
                                    IsPsicology = chbPsicologyChildren.IsChecked,
                                    IsSiudByAccident = chbSiudiByAccidentChildren.IsChecked,
                                    IsSports = chbSportsChildren.IsChecked,
                                    IsToTheChild = chbToTheChildChildren.IsChecked,
                                    IsTwoWheeledCancellation = chbTwoWheeledCancellationChildren.IsChecked,
                                    IsWarRiskCoverage = chbWarRiskCoverageChildren.IsChecked,
                                    MedicalAdviceAndPeriodiCheckAmount = medicalAdviceAndPeriodiCheckChildren,
                                    MedicalAndDentalChargesAmount = medicalAndDentalChargesChildren,
                                    MedicalChargesAmount = medicalChargesChildren,
                                    Other1Amount = other1Children,
                                    Other2Amount = other2Children,
                                    Other3Amount = other3Children,
                                    Other4Amount = other4Children,
                                    Other5Amount = other5Children,
                                    Other1Name = txtOther1.Text,
                                    Other2Name = txtOther2.Text,
                                    Other3Name = txtOther3.Text,
                                    Other4Name = txtOther4.Text,
                                    Other5Name = txtOther5.Text,
                                    PersonalAccidentsInsurenceTypeID = insuranceTypeChildren,
                                    PersonalAccidentsPolicyID = personalAccidentsPolicy.PersonalAccidentsPolicyID,
                                    PsicologyAmount = psicologyChildren,
                                    SiudByAccidentAmount = siudByAccidentChildren,
                                    SportsAmount = sportsChildren,
                                    ToTheChildAmount = toTheChildChildren,
                                    TwoWheeledCancellationAmount = twoWheeledCancellationChildren,
                                    TwoWheeledCancellationUpToCC = txtUpToCCChildren.Text,
                                    WarRiskCoverageAmount = warRiskCoverageChildren
                                };
                            }
                            else
                            {
                                childrenInsuranceDetails.CompensationForHospitalizationAmount = compensationForHospitalizationChildren;
                                childrenInsuranceDetails.CompensationForHospitalizationWaitingDays = txtHospitalizationDaysOfWaitingChildren.Text;
                                childrenInsuranceDetails.ComplementaryAmount = complementaryChildren;
                                childrenInsuranceDetails.DeathByAccidentAmount = deathByAccidentChildren;
                                childrenInsuranceDetails.DiagnosticTestsAndPregnancyAnnexAmount = diagnosticTestsAndPregnancyAnnexChildren;
                                childrenInsuranceDetails.DiagnosticTestsAndPregnancyAnnexType = txtDiagnosticTestsAndPregnancyAnnexTypeChildren.Text;
                                childrenInsuranceDetails.DisabilityByAccidentAmount = disabilityByAccidentChildren;
                                childrenInsuranceDetails.ExtremeSportCancellationAmount = extremeSportCancellationChildren;
                                childrenInsuranceDetails.FastDiagnosisAndMedicalGuidanceAmount = fastDiagnosisAndMedicalGuidanceChildren;
                                childrenInsuranceDetails.FracturesAndBurnsByAccidentAmount = fracturesAndBurnsByAccidentChildren;
                                childrenInsuranceDetails.IncapacityByAccidentAmount = incapacityByAccidentChildren;
                                childrenInsuranceDetails.IncapacityByAccidentPeriod = txtIncapacityByAccidentPeriod.Text;
                                childrenInsuranceDetails.IncapacityByAccidentWaitingDays = txtIncapacityByAccidentWaitingDaysChildren.Text;
                                childrenInsuranceDetails.IncapacityBySicknessAmount = incapacityBySicknessChildren;
                                childrenInsuranceDetails.IncapacityBySicknessPeriod = txtIncapacityBySicknessPeriod.Text;
                                childrenInsuranceDetails.IncapacityBySicknessWaitingDays = txtIncapacityBySicknessWaitingDaysChildren.Text;
                                childrenInsuranceDetails.InsuredTypeID = childrenTypeId;
                                childrenInsuranceDetails.IsCompensationForHospitalization = chbCompensationForHospitalizationChildren.IsChecked;
                                childrenInsuranceDetails.IsComplementary = chbComplementaryChildren.IsChecked;
                                childrenInsuranceDetails.IsDeathByAccident = chbDeathByAccidentChildren.IsChecked;
                                childrenInsuranceDetails.IsDiagnosticTestsAndPregnancyAnnex = chbDiagnosticTestsAndPregnancyAnnexChildren.IsChecked;
                                childrenInsuranceDetails.IsDisabilityByAccident = chbDisabilityByAccidentChildren.IsChecked;
                                childrenInsuranceDetails.IsExtremeSportCancellation = chbExtremeSportCancellationChildren.IsChecked;
                                childrenInsuranceDetails.IsFastDiagnosisAndMedicalGuidance = chbFastDiagnosisAndMedicalGuidanceChildren.IsChecked;
                                childrenInsuranceDetails.IsFracturesAndBurnsByAccident = chbFracturesAndBurnsByAccidentChildren.IsChecked;
                                childrenInsuranceDetails.IsIncapacityByAccident = chbIncapacityByAccidentChildren.IsChecked;
                                childrenInsuranceDetails.IsIncapacityBySickness = chbIncapacityBySicknessChildren.IsChecked;
                                childrenInsuranceDetails.IsMedicalAdviceAndPeriodiCheck = chbMedicalAdviceAndPeriodiCheckChildren.IsChecked;
                                childrenInsuranceDetails.IsMedicalAndDentalCharges = chbMedicalAndDentalChargesChildren.IsChecked;
                                childrenInsuranceDetails.IsMedicalCharges = chbMedicalChargesChildren.IsChecked;
                                childrenInsuranceDetails.IsOther1 = chbOther1Children.IsChecked;
                                childrenInsuranceDetails.IsOther2 = chbOther2Children.IsChecked;
                                childrenInsuranceDetails.IsOther3 = chbOther3Children.IsChecked;
                                childrenInsuranceDetails.IsOther4 = chbOther4Children.IsChecked;
                                childrenInsuranceDetails.IsOther5 = chbOther5Children.IsChecked;
                                childrenInsuranceDetails.IsPsicology = chbPsicologyChildren.IsChecked;
                                childrenInsuranceDetails.IsSiudByAccident = chbSiudiByAccidentChildren.IsChecked;
                                childrenInsuranceDetails.IsSports = chbSportsChildren.IsChecked;
                                childrenInsuranceDetails.IsToTheChild = chbToTheChildChildren.IsChecked;
                                childrenInsuranceDetails.IsTwoWheeledCancellation = chbTwoWheeledCancellationChildren.IsChecked;
                                childrenInsuranceDetails.IsWarRiskCoverage = chbWarRiskCoverageChildren.IsChecked;
                                childrenInsuranceDetails.MedicalAdviceAndPeriodiCheckAmount = medicalAdviceAndPeriodiCheckChildren;
                                childrenInsuranceDetails.MedicalAndDentalChargesAmount = medicalAndDentalChargesChildren;
                                childrenInsuranceDetails.MedicalChargesAmount = medicalChargesChildren;
                                childrenInsuranceDetails.Other1Amount = other1Children;
                                childrenInsuranceDetails.Other2Amount = other2Children;
                                childrenInsuranceDetails.Other3Amount = other3Children;
                                childrenInsuranceDetails.Other4Amount = other4Children;
                                childrenInsuranceDetails.Other5Amount = other5Children;
                                childrenInsuranceDetails.Other1Name = txtOther1.Text;
                                childrenInsuranceDetails.Other2Name = txtOther2.Text;
                                childrenInsuranceDetails.Other3Name = txtOther3.Text;
                                childrenInsuranceDetails.Other4Name = txtOther4.Text;
                                childrenInsuranceDetails.Other5Name = txtOther5.Text;
                                childrenInsuranceDetails.PersonalAccidentsInsurenceTypeID = insuranceTypeChildren;
                                childrenInsuranceDetails.PsicologyAmount = psicologyChildren;
                                childrenInsuranceDetails.SiudByAccidentAmount = siudByAccidentChildren;
                                childrenInsuranceDetails.SportsAmount = sportsChildren;
                                childrenInsuranceDetails.ToTheChildAmount = toTheChildChildren;
                                childrenInsuranceDetails.TwoWheeledCancellationAmount = twoWheeledCancellationChildren;
                                childrenInsuranceDetails.TwoWheeledCancellationUpToCC = txtUpToCCChildren.Text;
                                childrenInsuranceDetails.WarRiskCoverageAmount = warRiskCoverageChildren;
                            }

                        }

                        personalAccidentsLogic.InsertInsurancesDetails(new PersonalAccidentsInsurancesDetail[] { principalInsuranceDetails, partnerInsuranceDetails, childrenInsuranceDetails });

                    }
                    coveragesLogic.InsertCoveragesToPersonalAccidentsPolicy(personalAccidentsPolicy.PersonalAccidentsPolicyID, coverages, isAddition);
                    trackingLogics.InsertPersonalAccidentsTrackings(personalAccidentsPolicy.PersonalAccidentsPolicyID, trackings, isAddition);
                    if (chbStandardMoneyCollection.IsChecked == true)
                    {
                        int standardMoneyCollectionId = moneyCollectionLogic.InsertStandardMoneyCollection(standardMoneyCollection, new Insurance() { InsuranceName = "תאונות אישיות" }, personalAccidentsPolicy.PersonalAccidentsPolicyID);
                        moneyCollectionLogic.InsertTrackingsToStandardMoneyCollection(standardMoneyCollectionId, standardMoneyCollectionTrackings);
                    }
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }

            if (isAddition == false)
            {
                if (personalAccidentsPolicy == null)
                {
                    if (!Directory.Exists(@path))
                    {
                        Directory.CreateDirectory(@path);
                    }
                }
                else
                {
                    if (path == null && newPath != null)
                    {
                        Directory.CreateDirectory(newPath);
                    }
                    else if (path != newPath)
                    {
                        try
                        {
                            Directory.Move(path, newPath);
                        }
                        catch (Exception)
                        {
                            System.Windows.Forms.MessageBox.Show("לא ניתן למצוא את נתיב התיקייה");
                        }
                    }
                }
            }

            if (sender is Button)
            {
                Button btn = (Button)sender;
                if (btn.Name == "btnSave")
                {
                    confirmBeforeClosing = false;
                    Close();
                }
            }
        }

        private void SaveUnsavedInsured()
        {
            if (cbInsuredType.SelectedItem != null || txtRelativeId.Text != "" || txtRelativeFirstName.Text != "" || dpRelativeBirthDate.SelectedDate != null || cbRelativeMaritalStatus.SelectedItem != null || cbRelativeGender.SelectedItem != null || txtRelativeProfession.Text != "")
            {
                if (dgInsureds.SelectedItem == null)
                {
                    btnAddInsured_Click(this, new RoutedEventArgs());
                }
                else
                {
                    btnUpdateInsured_Click(this, new RoutedEventArgs());
                }
            }
        }

        private decimal? ParseToDecimal(CheckBox chbox, string text)
        {
            if (chbox.IsChecked == true && text != "")
            {
                return decimal.Parse(text);
            }
            else
            {
                return null;
            }
        }

        private void btnAddInsured_Click(object sender, RoutedEventArgs e)
        {
            if (nullErrorList != null && nullErrorList.Count > 0)
            {
                foreach (var item in nullErrorList)
                {
                    if (item is TextBox)
                    {
                        ((TextBox)item).ClearValue(BorderBrushProperty);
                    }
                    else if (item is ComboBox)
                    {
                        ((ComboBox)item).ClearValue(BorderBrushProperty);
                    }
                }
            }

            //בדיקת שדות חובה
            nullErrorList = validations.InputNullValidation(new object[] { cbInsuredType, txtRelativeLastName, txtRelativeFirstName });
            if (nullErrorList.Count > 0)
            {
                if (sender is Window)
                {
                    MessageBox.Show("שים לב, פרטי מבוטח הנוסף לא עודכנו במערכת", "מידע", MessageBoxButton.OK, MessageBoxImage.Information);
                    return;
                }
                MessageBox.Show("נא למלאות שדות חובה", "", MessageBoxButton.OK, MessageBoxImage.Error);

                return;
            }
            bool? isMale = null;
            if (cbRelativeGender.SelectedIndex == 1)
            {
                isMale = false;
            }
            else if (cbRelativeGender.SelectedIndex == 0)
            {
                isMale = true;
            }
            string maritalStatus = "";
            if (cbRelativeMaritalStatus.SelectedItem != null)
            {
                maritalStatus = ((ComboBoxItem)cbRelativeMaritalStatus.SelectedItem).Content.ToString();
            }
            PersonalAccidentsAdditionalInsured insured = new PersonalAccidentsAdditionalInsured() { BirthDate = dpRelativeBirthDate.SelectedDate, FirstName = txtRelativeFirstName.Text, IdNumber = txtRelativeId.Text, InsuredType = (InsuredType)cbInsuredType.SelectedItem, InsuredTypeID = ((InsuredType)cbInsuredType.SelectedItem).InsuredTypeID, IsMale = isMale, LastName = txtRelativeLastName.Text, MaritalStatus = maritalStatus, Profession = txtRelativeProfession.Text };
            insureds.Add(insured);
            dgInsuredsBinding();
            ClearInsuredInputs();
        }

        private void ClearInsuredInputs()
        {
            cbInsuredType.SelectedIndex = -1;
            txtRelativeId.Clear();
            txtRelativeLastName.Text = client.LastName;
            txtRelativeFirstName.Clear();
            dpRelativeBirthDate.SelectedDate = null;
            cbRelativeMaritalStatus.SelectedIndex = -1;
            cbRelativeGender.SelectedIndex = -1;
            txtRelativeProfession.Clear();
        }

        private void dgInsuredsBinding()
        {
            dgInsureds.ItemsSource = insureds.ToList();
            lv.AutoSizeColumns(dgInsureds.View);
            if (insureds.FirstOrDefault(i => i.InsuredType.InsuredTypeName == "בן/בת זוג") != null)
            {
                EnablePartnerInputs();
                isPartnerEnabled = true;
            }
            else
            {
                DisablePartnerInputs();
                isPartnerEnabled = false;

            }
            if (insureds.FirstOrDefault(i => i.InsuredType.InsuredTypeName == "ילד/ילדה") != null)
            {
                EnableChildrenInputs();
                isChildrenEnabled = true;
            }
            else
            {
                DisableChildrenInputs();
                isChildrenEnabled = false;
            }
        }

        private void DisableChildrenInputs()
        {
            spChildrenInsuranceType.IsEnabled = false;
            chbDeathByAccidentChildren.IsEnabled = false;
            chbDisabilityByAccidentChildren.IsEnabled = false;
            chbFracturesAndBurnsByAccidentChildren.IsEnabled = false;
            chbCompensationForHospitalizationChildren.IsEnabled = false;
            chbSiudiByAccidentChildren.IsEnabled = false;
            chbSportsChildren.IsEnabled = false;
            chbMedicalChargesChildren.IsEnabled = false;
            chbMedicalAndDentalChargesChildren.IsEnabled = false;
            chbWarRiskCoverageChildren.IsEnabled = false;
            chbOther1Children.IsEnabled = false;
            chbOther2Children.IsEnabled = false;
            chbOther3Children.IsEnabled = false;
            chbOther4Children.IsEnabled = false;
            chbOther5Children.IsEnabled = false;
            chbMedicalAdviceAndPeriodiCheckChildren.IsEnabled = false;
            chbDiagnosticTestsAndPregnancyAnnexChildren.IsEnabled = false;
            chbFastDiagnosisAndMedicalGuidanceChildren.IsEnabled = false;
            chbPsicologyChildren.IsEnabled = false;
            chbComplementaryChildren.IsEnabled = false;
            chbToTheChildChildren.IsEnabled = false;

            chbDeathByAccidentChildren.IsChecked = false;
            chbDisabilityByAccidentChildren.IsChecked = false;
            chbFracturesAndBurnsByAccidentChildren.IsChecked = false;
            chbCompensationForHospitalizationChildren.IsChecked = false;
            chbSiudiByAccidentChildren.IsChecked = false;
            chbSportsChildren.IsChecked = false;
            chbMedicalChargesChildren.IsChecked = false;
            chbMedicalAndDentalChargesChildren.IsChecked = false;
            chbWarRiskCoverageChildren.IsChecked = false;
            chbOther1Children.IsChecked = false;
            chbOther2Children.IsChecked = false;
            chbOther3Children.IsChecked = false;
            chbOther4Children.IsChecked = false;
            chbOther5Children.IsChecked = false;
            chbMedicalAdviceAndPeriodiCheckChildren.IsChecked = false;
            chbDiagnosticTestsAndPregnancyAnnexChildren.IsChecked = false;
            chbFastDiagnosisAndMedicalGuidanceChildren.IsChecked = false;
            chbPsicologyChildren.IsChecked = false;
            chbComplementaryChildren.IsChecked = false;
            chbToTheChildChildren.IsChecked = false;

        }

        private void DisablePartnerInputs()
        {
            spPartnerInsuranceType.IsEnabled = false;
            chbDeathByAccidentPartner.IsEnabled = false;
            chbDisabilityByAccidentPartner.IsEnabled = false;
            chbFracturesAndBurnsByAccidentPartner.IsEnabled = false;
            chbCompensationForHospitalizationPartner.IsEnabled = false;
            chbSiudiByAccidentPartner.IsEnabled = false;
            chbSportsPartner.IsEnabled = false;
            chbTwoWheeledCancellationPartner.IsEnabled = false;
            chbExtremeSportCancellationPartner.IsEnabled = false;
            chbMedicalChargesPartner.IsEnabled = false;
            chbMedicalAndDentalChargesPartner.IsEnabled = false;
            chbWarRiskCoveragePartner.IsEnabled = false;
            chbIncapacityByAccidentPartner.IsEnabled = false;
            chbIncapacityBySicknessPartner.IsEnabled = false;
            chbOther1Partner.IsEnabled = false;
            chbOther2Partner.IsEnabled = false;
            chbOther3Partner.IsEnabled = false;
            chbOther4Partner.IsEnabled = false;
            chbOther5Partner.IsEnabled = false;
            chbMedicalAdviceAndPeriodiCheckPartner.IsEnabled = false;
            chbDiagnosticTestsAndPregnancyAnnexPartner.IsEnabled = false;
            chbFastDiagnosisAndMedicalGuidancePartner.IsEnabled = false;
            chbPsicologyPartner.IsEnabled = false;
            chbComplementaryPartner.IsEnabled = false;
            chbToTheChildPartner.IsEnabled = false;

            chbDeathByAccidentPartner.IsChecked = false;
            chbDisabilityByAccidentPartner.IsChecked = false;
            chbFracturesAndBurnsByAccidentPartner.IsChecked = false;
            chbCompensationForHospitalizationPartner.IsChecked = false;
            chbSiudiByAccidentPartner.IsChecked = false;
            chbSportsPartner.IsChecked = false;
            chbTwoWheeledCancellationPartner.IsChecked = false;
            chbExtremeSportCancellationPartner.IsChecked = false;
            chbMedicalChargesPartner.IsChecked = false;
            chbMedicalAndDentalChargesPartner.IsChecked = false;
            chbWarRiskCoveragePartner.IsChecked = false;
            chbIncapacityByAccidentPartner.IsChecked = false;
            chbIncapacityBySicknessPartner.IsChecked = false;
            chbOther1Partner.IsChecked = false;
            chbOther2Partner.IsChecked = false;
            chbOther3Partner.IsChecked = false;
            chbOther4Partner.IsChecked = false;
            chbOther5Partner.IsChecked = false;
            chbMedicalAdviceAndPeriodiCheckPartner.IsChecked = false;
            chbDiagnosticTestsAndPregnancyAnnexPartner.IsChecked = false;
            chbFastDiagnosisAndMedicalGuidancePartner.IsChecked = false;
            chbPsicologyPartner.IsChecked = false;
            chbComplementaryPartner.IsChecked = false;
            chbToTheChildPartner.IsChecked = false;
        }

        private void EnablePartnerInputs()
        {
            spPartnerInsuranceType.IsEnabled = true;
            chbDeathByAccidentPartner.IsEnabled = true;
            chbDisabilityByAccidentPartner.IsEnabled = true;
            chbFracturesAndBurnsByAccidentPartner.IsEnabled = true;
            chbCompensationForHospitalizationPartner.IsEnabled = true;
            chbSiudiByAccidentPartner.IsEnabled = true;
            chbSportsPartner.IsEnabled = true;
            chbTwoWheeledCancellationPartner.IsEnabled = true;
            chbExtremeSportCancellationPartner.IsEnabled = true;
            chbMedicalChargesPartner.IsEnabled = true;
            chbMedicalAndDentalChargesPartner.IsEnabled = true;
            chbWarRiskCoveragePartner.IsEnabled = true;
            chbIncapacityByAccidentPartner.IsEnabled = true;
            chbIncapacityBySicknessPartner.IsEnabled = true;
            chbOther1Partner.IsEnabled = true;
            chbOther2Partner.IsEnabled = true;
            chbOther3Partner.IsEnabled = true;
            chbOther4Partner.IsEnabled = true;
            chbOther5Partner.IsEnabled = true;
            chbMedicalAdviceAndPeriodiCheckPartner.IsEnabled = true;
            chbDiagnosticTestsAndPregnancyAnnexPartner.IsEnabled = true;
            chbFastDiagnosisAndMedicalGuidancePartner.IsEnabled = true;
            chbPsicologyPartner.IsEnabled = true;
            chbComplementaryPartner.IsEnabled = true;
            chbToTheChildPartner.IsEnabled = true;
        }

        private void EnableChildrenInputs()
        {
            spChildrenInsuranceType.IsEnabled = true;
            chbDeathByAccidentChildren.IsEnabled = true;
            chbDisabilityByAccidentChildren.IsEnabled = true;
            chbFracturesAndBurnsByAccidentChildren.IsEnabled = true;
            chbCompensationForHospitalizationChildren.IsEnabled = true;
            chbSiudiByAccidentChildren.IsEnabled = true;
            chbSportsChildren.IsEnabled = true;
            chbMedicalChargesChildren.IsEnabled = true;
            chbMedicalAndDentalChargesChildren.IsEnabled = true;
            chbWarRiskCoverageChildren.IsEnabled = true;
            chbOther1Children.IsEnabled = true;
            chbOther2Children.IsEnabled = true;
            chbOther3Children.IsEnabled = true;
            chbOther4Children.IsEnabled = true;
            chbOther5Children.IsEnabled = true;
            chbMedicalAdviceAndPeriodiCheckChildren.IsEnabled = true;
            chbDiagnosticTestsAndPregnancyAnnexChildren.IsEnabled = true;
            chbFastDiagnosisAndMedicalGuidanceChildren.IsEnabled = true;
            chbPsicologyChildren.IsEnabled = true;
            chbComplementaryChildren.IsEnabled = true;
            chbToTheChildChildren.IsEnabled = true;
        }

        private void dgInsureds_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (dgInsureds.SelectedItem != null)
            {
                DisplayInsuredDetails();
            }
            else
            {
                ClearInsuredInputs();
            }
        }

        private void DisplayInsuredDetails()
        {
            PersonalAccidentsAdditionalInsured insured = (PersonalAccidentsAdditionalInsured)dgInsureds.SelectedItem;
            var insuredTypes = cbInsuredType.Items;
            foreach (var item in insuredTypes)
            {
                InsuredType type = (InsuredType)item;
                if (type.InsuredTypeID == insured.InsuredTypeID)
                {
                    cbInsuredType.SelectedItem = item;
                    break;
                }
            }
            txtRelativeId.Text = insured.IdNumber;
            txtRelativeLastName.Text = insured.LastName;
            txtRelativeFirstName.Text = insured.FirstName;
            dpRelativeBirthDate.SelectedDate = insured.BirthDate;
            var maritalStatus = cbRelativeMaritalStatus.Items;
            foreach (var item in maritalStatus)
            {
                ComboBoxItem status = (ComboBoxItem)item;
                if (status.Content.ToString() == insured.MaritalStatus)
                {
                    cbRelativeMaritalStatus.SelectedItem = item;
                    break;
                }
            }
            if (insured.IsMale == true)
            {
                cbRelativeGender.SelectedIndex = 0;
            }
            else if (insured.IsMale == false)
            {
                cbRelativeGender.SelectedIndex = 1;
            }
            txtRelativeProfession.Text = insured.Profession;
        }

        private void dgInsureds_MouseDown(object sender, MouseButtonEventArgs e)
        {
            //ClearInsuredInputs();
            dgInsureds.UnselectAll();
        }

        private void btnDeleteInsured_Click(object sender, RoutedEventArgs e)
        {
            if (dgInsureds.SelectedItem == null)
            {
                MessageBox.Show("נא לסמן מבוטח", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            if (MessageBox.Show("למחוק מבוטח", "אישור", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                insureds.Remove((PersonalAccidentsAdditionalInsured)dgInsureds.SelectedItem);
                dgInsuredsBinding();
            }
            else
            {
                dgInsureds.UnselectAll();
            }
        }

        private void btnUpdateInsured_Click(object sender, RoutedEventArgs e)
        {
            if (dgInsureds.SelectedItem == null)
            {
                MessageBox.Show("נא לסמן מבוטח", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            PersonalAccidentsAdditionalInsured insured = (PersonalAccidentsAdditionalInsured)dgInsureds.SelectedItem;
            insured.BirthDate = dpRelativeBirthDate.SelectedDate;
            insured.FirstName = txtRelativeFirstName.Text;
            insured.IdNumber = txtRelativeId.Text;
            insured.InsuredTypeID = ((InsuredType)cbInsuredType.SelectedItem).InsuredTypeID;
            insured.InsuredType = (InsuredType)cbInsuredType.SelectedItem;
            bool isMale = true;
            if (cbRelativeGender.SelectedIndex == 1)
            {
                isMale = false;
            }
            string maritalStatus = "";
            if (cbRelativeMaritalStatus.SelectedItem != null)
            {
                maritalStatus = ((ComboBoxItem)cbRelativeMaritalStatus.SelectedItem).Content.ToString();
            }
            insured.IsMale = isMale;
            insured.LastName = txtRelativeLastName.Text;
            insured.MaritalStatus = maritalStatus;
            insured.Profession = txtRelativeProfession.Text;
            dgInsuredsBinding();
        }

        private void btnUpdateInsuranceTypePrincipal_Click(object sender, RoutedEventArgs e)
        {
            string objName = ((Button)sender).Name;
            int? selectedItemId = null;
            if (cbCompany.SelectedItem == null)
            {
                MessageBox.Show("נא בחר חברת ביטוח");
                return;
            }
            ComboBox cb = null;
            if (objName == "btnUpdateInsuranceTypePrincipal")
            {
                if (cbInsurenceTypePrincipal.SelectedItem != null)
                {
                    selectedItemId = ((PersonalAccidentsInsuranceType)cbInsurenceTypePrincipal.SelectedItem).PersonalAccidentsInsuranceTypeID;
                }
                cb = cbInsurenceTypePrincipal;
            }
            else if (objName == "btnUpdateInsuranceTypePartner")
            {
                if (cbInsurenceTypePartner.SelectedItem != null)
                {
                    selectedItemId = ((PersonalAccidentsInsuranceType)cbInsurenceTypePartner.SelectedItem).PersonalAccidentsInsuranceTypeID;
                }
                cb = cbInsurenceTypePartner;
            }
            else if (objName == "btnUpdateInsuranceTypeChildren")
            {
                if (cbInsurenceTypeChildren.SelectedItem != null)
                {
                    selectedItemId = ((PersonalAccidentsInsuranceType)cbInsurenceTypeChildren.SelectedItem).PersonalAccidentsInsuranceTypeID;
                }
                cb = cbInsurenceTypeChildren;
            }

            frmPersonalAccidentsInsuranceTypes programs = new frmPersonalAccidentsInsuranceTypes((InsuranceCompany)cbCompany.SelectedItem);
            programs.ShowDialog();
            cbInsurenceTypeBinding();

            if (programs.typeAdded != null)
            {
                SelectItemInCb(((PersonalAccidentsInsuranceType)programs.typeAdded).PersonalAccidentsInsuranceTypeID, cb);
            }
            else if (selectedItemId != null)
            {
                SelectItemInCb((int)selectedItemId, cb);
            }
        }

        private void cbInsurenceTypeBinding()
        {
            int? principalId = null;
            if (cbInsurenceTypePrincipal.SelectedItem != null)
            {
                principalId = ((PersonalAccidentsInsuranceType)cbInsurenceTypePrincipal.SelectedItem).PersonalAccidentsInsuranceTypeID;
            }
            int? partnerId = null;
            if (cbInsurenceTypePartner.SelectedItem != null)
            {
                partnerId = ((PersonalAccidentsInsuranceType)cbInsurenceTypePartner.SelectedItem).PersonalAccidentsInsuranceTypeID;
            }
            int? childrenId = null;
            if (cbInsurenceTypeChildren.SelectedItem != null)
            {
                childrenId = ((PersonalAccidentsInsuranceType)cbInsurenceTypeChildren.SelectedItem).PersonalAccidentsInsuranceTypeID;
            }
            var insurances = personalAccidentInsuranceLogic.GetActivePersonalAccidentsInsuranceTypesByCompany(((InsuranceCompany)cbCompany.SelectedItem).CompanyID);
            cbInsurenceTypePrincipal.ItemsSource = insurances;
            cbInsurenceTypePrincipal.DisplayMemberPath = "PersonalAccidentsInsuranceTypeName";
            cbInsurenceTypePartner.ItemsSource = insurances;
            cbInsurenceTypePartner.DisplayMemberPath = "PersonalAccidentsInsuranceTypeName";
            cbInsurenceTypeChildren.ItemsSource = insurances;
            cbInsurenceTypeChildren.DisplayMemberPath = "PersonalAccidentsInsuranceTypeName";
            if (principalId != null)
            {
                SelectItemInCb((int)principalId, cbInsurenceTypePrincipal);
            }
            if (partnerId != null)
            {
                SelectItemInCb((int)partnerId, cbInsurenceTypePartner);
            }
            if (childrenId != null)
            {
                SelectItemInCb((int)childrenId, cbInsurenceTypeChildren);
            }
        }

        private void SelectItemInCb(int id, ComboBox cb)
        {
            var items = cb.Items;
            foreach (var item in items)
            {
                PersonalAccidentsInsuranceType parsedItem = (PersonalAccidentsInsuranceType)item;
                if (parsedItem.PersonalAccidentsInsuranceTypeID == id)
                {
                    cb.SelectedItem = item;
                    break;
                }
            }
        }

        private void cbInsurenceTypePrincipal_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cbInsurenceTypePrincipal.SelectedItem != null)
            {
                PersonalAccidentsInsuranceType type = (PersonalAccidentsInsuranceType)cbInsurenceTypePrincipal.SelectedItem;
                if (type.DeathByAccidentAmount != null)
                {
                    txtDeathByAccidentPrincipal.Text = validations.ConvertDecimalToString(type.DeathByAccidentAmount);
                    chbDeathByAccidentPrincipal.IsChecked = true;
                }
                else
                {
                    txtDeathByAccidentPrincipal.Clear();
                    chbDeathByAccidentPrincipal.IsChecked = false;
                }
                if (type.IncapacityByAccidentAmount != null)
                {
                    txtDisabilityByAccidentPrincipal.Text = validations.ConvertDecimalToString(type.IncapacityByAccidentAmount);
                    chbDisabilityByAccidentPrincipal.IsChecked = true;
                }
                else
                {
                    txtDisabilityByAccidentPrincipal.Clear();
                    chbDisabilityByAccidentPrincipal.IsChecked = false;
                }
                if (type.FracturesAndBurnsByAccidentAmount != null)
                {
                    txtFracturesAndBurnsByAccidentPrincipal.Text = validations.ConvertDecimalToString(type.FracturesAndBurnsByAccidentAmount);
                    chbFracturesAndBurnsByAccidentPrincipal.IsChecked = true;
                }
                else
                {
                    txtFracturesAndBurnsByAccidentPrincipal.Clear();
                    chbFracturesAndBurnsByAccidentPrincipal.IsChecked = false;
                }
                if (type.CompensationForHospitalizationAmount != null)
                {
                    txtCompensationForHospitalizationPrincipal.Text = validations.ConvertDecimalToString(type.CompensationForHospitalizationAmount);
                    chbCompensationForHospitalizationPrincipal.IsChecked = true;
                }
                else
                {
                    txtCompensationForHospitalizationPrincipal.Clear();
                    chbCompensationForHospitalizationPrincipal.IsChecked = false;
                }
                if (type.SiudByAccidentAmount != null)
                {
                    txtSiudiByAccidentPrincipal.Text = validations.ConvertDecimalToString(type.SiudByAccidentAmount);
                    chbSiudiByAccidentPrincipal.IsChecked = true;
                }
                else
                {
                    txtSiudiByAccidentPrincipal.Clear();
                    chbSiudiByAccidentPrincipal.IsChecked = false;
                }
            }
        }

        private void cbInsurenceTypePartner_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cbInsurenceTypePartner.SelectedItem != null)
            {
                PersonalAccidentsInsuranceType type = (PersonalAccidentsInsuranceType)cbInsurenceTypePartner.SelectedItem;
                if (type.DeathByAccidentAmount != null)
                {
                    txtDeathByAccidentPartner.Text = validations.ConvertDecimalToString(type.DeathByAccidentAmount);
                    chbDeathByAccidentPartner.IsChecked = true;
                }
                else
                {
                    txtDeathByAccidentPartner.Clear();
                    chbDeathByAccidentPartner.IsChecked = false;
                }
                if (type.IncapacityByAccidentAmount != null)
                {
                    txtDisabilityByAccidentPartner.Text = validations.ConvertDecimalToString(type.IncapacityByAccidentAmount);
                    chbDisabilityByAccidentPartner.IsChecked = true;
                }
                else
                {
                    txtDisabilityByAccidentPartner.Clear();
                    chbDisabilityByAccidentPartner.IsChecked = false;
                }
                if (type.FracturesAndBurnsByAccidentAmount != null)
                {
                    txtFracturesAndBurnsByAccidentPartner.Text = validations.ConvertDecimalToString(type.FracturesAndBurnsByAccidentAmount);
                    chbFracturesAndBurnsByAccidentPartner.IsChecked = true;
                }
                else
                {
                    txtFracturesAndBurnsByAccidentPartner.Clear();
                    chbFracturesAndBurnsByAccidentPartner.IsChecked = false;
                }
                if (type.CompensationForHospitalizationAmount != null)
                {
                    txtCompensationForHospitalizationPartner.Text = validations.ConvertDecimalToString(type.CompensationForHospitalizationAmount);
                    chbCompensationForHospitalizationPartner.IsChecked = true;
                }
                else
                {
                    txtCompensationForHospitalizationPartner.Clear();
                    chbCompensationForHospitalizationPartner.IsChecked = false;
                }
                if (type.SiudByAccidentAmount != null)
                {
                    txtSiudiByAccidentPartner.Text = validations.ConvertDecimalToString(type.SiudByAccidentAmount);
                    chbSiudiByAccidentPartner.IsChecked = true;
                }
                else
                {
                    txtSiudiByAccidentPartner.Clear();
                    chbSiudiByAccidentPartner.IsChecked = false;
                }
            }
        }

        private void cbInsurenceTypeChildren_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cbInsurenceTypeChildren.SelectedItem != null)
            {
                PersonalAccidentsInsuranceType type = (PersonalAccidentsInsuranceType)cbInsurenceTypeChildren.SelectedItem;
                if (type.DeathByAccidentAmount != null)
                {
                    txtDeathByAccidentChildren.Text = validations.ConvertDecimalToString(type.DeathByAccidentAmount);
                    chbDeathByAccidentChildren.IsChecked = true;
                }
                else
                {
                    txtDeathByAccidentChildren.Clear();
                    chbDeathByAccidentChildren.IsChecked = false;
                }
                if (type.IncapacityByAccidentAmount != null)
                {
                    txtDisabilityByAccidentChildren.Text = validations.ConvertDecimalToString(type.IncapacityByAccidentAmount);
                    chbDisabilityByAccidentChildren.IsChecked = true;
                }
                else
                {
                    txtDisabilityByAccidentChildren.Clear();
                    chbDisabilityByAccidentChildren.IsChecked = false;
                }
                if (type.FracturesAndBurnsByAccidentAmount != null)
                {
                    txtFracturesAndBurnsByAccidentChildren.Text = validations.ConvertDecimalToString(type.FracturesAndBurnsByAccidentAmount);
                    chbFracturesAndBurnsByAccidentChildren.IsChecked = true;
                }
                else
                {
                    txtFracturesAndBurnsByAccidentChildren.Clear();
                    chbFracturesAndBurnsByAccidentChildren.IsChecked = false;
                }
                if (type.CompensationForHospitalizationAmount != null)
                {
                    txtCompensationForHospitalizationChildren.Text = validations.ConvertDecimalToString(type.CompensationForHospitalizationAmount);
                    chbCompensationForHospitalizationChildren.IsChecked = true;
                }
                else
                {
                    txtCompensationForHospitalizationChildren.Clear();
                    chbCompensationForHospitalizationChildren.IsChecked = false;
                }
                if (type.SiudByAccidentAmount != null)
                {
                    txtSiudiByAccidentChildren.Text = validations.ConvertDecimalToString(type.SiudByAccidentAmount);
                    chbSiudiByAccidentChildren.IsChecked = true;
                }
                else
                {
                    txtSiudiByAccidentChildren.Clear();
                    chbSiudiByAccidentChildren.IsChecked = false;
                }
            }
        }

        private void chbDeathByAccidentPrincipal_Checked(object sender, RoutedEventArgs e)
        {
            txtDeathByAccidentPrincipal.IsEnabled = true;
        }

        private void chbDeathByAccidentPrincipal_Unchecked(object sender, RoutedEventArgs e)
        {
            txtDeathByAccidentPrincipal.IsEnabled = false;
            txtDeathByAccidentPrincipal.Clear();
        }

        private void chbDisabilityByAccidentPrincipal_Checked(object sender, RoutedEventArgs e)
        {
            txtDisabilityByAccidentPrincipal.IsEnabled = true;
        }

        private void chbDisabilityByAccidentPrincipal_Unchecked(object sender, RoutedEventArgs e)
        {
            txtDisabilityByAccidentPrincipal.IsEnabled = false;
            txtDisabilityByAccidentPrincipal.Clear();
        }

        private void chbFracturesAndBurnsByAccidentPrincipal_Checked(object sender, RoutedEventArgs e)
        {
            txtFracturesAndBurnsByAccidentPrincipal.IsEnabled = true;
        }

        private void chbFracturesAndBurnsByAccidentPrincipal_Unchecked(object sender, RoutedEventArgs e)
        {
            txtFracturesAndBurnsByAccidentPrincipal.IsEnabled = false;
            txtFracturesAndBurnsByAccidentPrincipal.Clear();
        }

        private void chbCompensationForHospitalizationPrincipal_Checked(object sender, RoutedEventArgs e)
        {
            txtCompensationForHospitalizationPrincipal.IsEnabled = true;
            txtHospitalizationDaysOfWaitingPrincipal.IsEnabled = true;
        }

        private void chbCompensationForHospitalizationPrincipal_Unchecked(object sender, RoutedEventArgs e)
        {
            txtCompensationForHospitalizationPrincipal.IsEnabled = false;
            txtCompensationForHospitalizationPrincipal.Clear();
            txtHospitalizationDaysOfWaitingPrincipal.IsEnabled = false;
            txtHospitalizationDaysOfWaitingPrincipal.Clear();
        }

        private void chbSiudiByAccidentPrincipal_Checked(object sender, RoutedEventArgs e)
        {
            txtSiudiByAccidentPrincipal.IsEnabled = true;
        }

        private void chbSiudiByAccidentPrincipal_Unchecked(object sender, RoutedEventArgs e)
        {
            txtSiudiByAccidentPrincipal.IsEnabled = false;
            txtSiudiByAccidentPrincipal.Clear();
        }

        private void chbDeathByAccidentPartner_Checked(object sender, RoutedEventArgs e)
        {
            txtDeathByAccidentPartner.IsEnabled = true;
        }

        private void chbDeathByAccidentPartner_Unchecked(object sender, RoutedEventArgs e)
        {
            txtDeathByAccidentPartner.IsEnabled = false;
            txtDeathByAccidentPartner.Clear();
        }

        private void chbDisabilityByAccidentPartner_Checked(object sender, RoutedEventArgs e)
        {
            txtDisabilityByAccidentPartner.IsEnabled = true;
        }

        private void chbDisabilityByAccidentPartner_Unchecked(object sender, RoutedEventArgs e)
        {
            txtDisabilityByAccidentPartner.IsEnabled = false;
            txtDisabilityByAccidentPartner.Clear();
        }

        private void chbFracturesAndBurnsByAccidentPartner_Checked(object sender, RoutedEventArgs e)
        {
            txtFracturesAndBurnsByAccidentPartner.IsEnabled = true;
        }

        private void chbFracturesAndBurnsByAccidentPartner_Unchecked(object sender, RoutedEventArgs e)
        {
            txtFracturesAndBurnsByAccidentPartner.IsEnabled = false;
            txtFracturesAndBurnsByAccidentPartner.Clear();
        }

        private void chbCompensationForHospitalizationPartner_Checked(object sender, RoutedEventArgs e)
        {
            txtCompensationForHospitalizationPartner.IsEnabled = true;
            txtHospitalizationDaysOfWaitingPartner.IsEnabled = true;
        }

        private void chbCompensationForHospitalizationPartner_Unchecked(object sender, RoutedEventArgs e)
        {
            txtCompensationForHospitalizationPartner.IsEnabled = false;
            txtCompensationForHospitalizationPartner.Clear();
            txtHospitalizationDaysOfWaitingPartner.IsEnabled = false;
            txtHospitalizationDaysOfWaitingPartner.Clear();
        }

        private void chbSiudiByAccidentPartner_Checked(object sender, RoutedEventArgs e)
        {
            txtSiudiByAccidentPartner.IsEnabled = true;
        }

        private void chbSiudiByAccidentPartner_Unchecked(object sender, RoutedEventArgs e)
        {
            txtSiudiByAccidentPartner.IsEnabled = false;
            txtSiudiByAccidentPartner.Clear();
        }

        private void chbDeathByAccidentChildren_Checked(object sender, RoutedEventArgs e)
        {
            txtDeathByAccidentChildren.IsEnabled = true;
        }

        private void chbDeathByAccidentChildren_Unchecked(object sender, RoutedEventArgs e)
        {
            txtDeathByAccidentChildren.IsEnabled = false;
            txtDeathByAccidentChildren.Clear();
        }

        private void chbDisabilityByAccidentChildren_Checked(object sender, RoutedEventArgs e)
        {
            txtDisabilityByAccidentChildren.IsEnabled = true;
        }

        private void chbDisabilityByAccidentChildren_Unchecked(object sender, RoutedEventArgs e)
        {
            txtDisabilityByAccidentChildren.IsEnabled = false;
            txtDisabilityByAccidentChildren.Clear();
        }

        private void chbFracturesAndBurnsByAccidentChildren_Checked(object sender, RoutedEventArgs e)
        {
            txtFracturesAndBurnsByAccidentChildren.IsEnabled = true;
        }

        private void chbFracturesAndBurnsByAccidentChildren_Unchecked(object sender, RoutedEventArgs e)
        {
            txtFracturesAndBurnsByAccidentChildren.IsEnabled = false;
            txtFracturesAndBurnsByAccidentChildren.Clear();
        }

        private void chbCompensationForHospitalizationChildren_Checked(object sender, RoutedEventArgs e)
        {
            txtHospitalizationDaysOfWaitingChildren.IsEnabled = true;
            txtCompensationForHospitalizationChildren.IsEnabled = true;
        }

        private void chbCompensationForHospitalizationChildren_Unchecked(object sender, RoutedEventArgs e)
        {
            txtHospitalizationDaysOfWaitingChildren.IsEnabled = false;
            txtHospitalizationDaysOfWaitingChildren.Clear();
            txtCompensationForHospitalizationChildren.IsEnabled = false;
            txtCompensationForHospitalizationChildren.Clear();
        }

        private void chbSiudiByAccidentChildren_Checked(object sender, RoutedEventArgs e)
        {
            txtSiudiByAccidentChildren.IsEnabled = true;
        }

        private void chbSiudiByAccidentChildren_Unchecked(object sender, RoutedEventArgs e)
        {
            txtSiudiByAccidentChildren.IsEnabled = false;
            txtSiudiByAccidentChildren.Clear();
        }

        private void chbSportsPrincipal_Checked(object sender, RoutedEventArgs e)
        {
            txtSportsPrincipal.IsEnabled = true;
        }

        private void chbSportsPrincipal_Unchecked(object sender, RoutedEventArgs e)
        {
            txtSportsPrincipal.IsEnabled = false;
            txtSportsPrincipal.Clear();
        }

        private void chbSportsPartner_Checked(object sender, RoutedEventArgs e)
        {
            txtSportsPartner.IsEnabled = true;
        }

        private void chbSportsPartner_Unchecked(object sender, RoutedEventArgs e)
        {
            txtSportsPartner.IsEnabled = false;
            txtSportsPartner.Clear();
        }

        private void chbSportsChildren_Checked(object sender, RoutedEventArgs e)
        {
            txtSportsChildren.IsEnabled = true;
        }

        private void chbSportsChildren_Unchecked(object sender, RoutedEventArgs e)
        {
            txtSportsChildren.IsEnabled = false;
            txtSportsChildren.Clear();
        }

        private void chbTwoWheeledCancellationPrincipal_Checked(object sender, RoutedEventArgs e)
        {
            txtUpToCCPrincipal.IsEnabled = true;
            txtTwoWheeledCancellationPrincipal.IsEnabled = true;
        }

        private void chbTwoWheeledCancellationPrincipal_Unchecked(object sender, RoutedEventArgs e)
        {
            txtUpToCCPrincipal.IsEnabled = false;
            txtUpToCCPrincipal.Clear();
            txtTwoWheeledCancellationPrincipal.IsEnabled = false;
            txtTwoWheeledCancellationPrincipal.Clear();
        }

        private void chbExtremeSportCancellationPrincipal_Checked(object sender, RoutedEventArgs e)
        {
            txtExtremeSportCancellationPrincipal.IsEnabled = true;
        }

        private void chbExtremeSportCancellationPrincipal_Unchecked(object sender, RoutedEventArgs e)
        {
            txtExtremeSportCancellationPrincipal.IsEnabled = false;
            txtExtremeSportCancellationPrincipal.Clear();
        }

        private void chbMedicalChargesPrincipal_Checked(object sender, RoutedEventArgs e)
        {
            txtMedicalChargesPrincipal.IsEnabled = true;
        }

        private void chbMedicalChargesPrincipal_Unchecked(object sender, RoutedEventArgs e)
        {
            txtMedicalChargesPrincipal.IsEnabled = false;
            txtMedicalChargesPrincipal.Clear();
        }

        private void chbMedicalAndDentalChargesPrincipal_Checked(object sender, RoutedEventArgs e)
        {
            txtMedicalAndDentalChargesPrincipal.IsEnabled = true;
        }

        private void chbMedicalAndDentalChargesPrincipal_Unchecked(object sender, RoutedEventArgs e)
        {
            txtMedicalAndDentalChargesPrincipal.IsEnabled = false;
            txtMedicalAndDentalChargesPrincipal.Clear();
        }

        private void chbWarRiskCoveragePrincipal_Checked(object sender, RoutedEventArgs e)
        {
            txtWarRiskCoveragePrincipal.IsEnabled = true;
        }

        private void chbWarRiskCoveragePrincipal_Unchecked(object sender, RoutedEventArgs e)
        {
            txtWarRiskCoveragePrincipal.IsEnabled = false;
            txtWarRiskCoveragePrincipal.Clear();
        }

        private void chbIncapacityByAccidentPrincipal_Checked(object sender, RoutedEventArgs e)
        {
            txtIncapacityByAccidentWaitingDaysPrincipal.IsEnabled = true;
            txtIncapacityByAccidentPrincipal.IsEnabled = true;
        }

        private void chbIncapacityByAccidentPrincipal_Unchecked(object sender, RoutedEventArgs e)
        {
            txtIncapacityByAccidentWaitingDaysPrincipal.IsEnabled = false;
            txtIncapacityByAccidentWaitingDaysPrincipal.Clear();
            txtIncapacityByAccidentPrincipal.IsEnabled = false;
            txtIncapacityByAccidentPrincipal.Clear();
        }

        private void chbIncapacityBySicknessPrincipal_Checked(object sender, RoutedEventArgs e)
        {
            txtIncapacityBySicknessWaitingDaysPrincipal.IsEnabled = true;
            txtIncapacityBySicknessPrincipal.IsEnabled = true;
        }

        private void chbIncapacityBySicknessPrincipal_Unchecked(object sender, RoutedEventArgs e)
        {
            txtIncapacityBySicknessWaitingDaysPrincipal.IsEnabled = false;
            txtIncapacityBySicknessWaitingDaysPrincipal.Clear();
            txtIncapacityBySicknessPrincipal.IsEnabled = false;
            txtIncapacityBySicknessPrincipal.Clear();

        }

        private void chbTwoWheeledCancellationPartner_Checked(object sender, RoutedEventArgs e)
        {
            txtUpToCCPartner.IsEnabled = true;
            txtTwoWheeledCancellationPartner.IsEnabled = true;
        }

        private void chbTwoWheeledCancellationPartner_Unchecked(object sender, RoutedEventArgs e)
        {
            txtUpToCCPartner.IsEnabled = false;
            txtUpToCCPartner.Clear();
            txtTwoWheeledCancellationPartner.IsEnabled = false;
            txtTwoWheeledCancellationPartner.Clear();
        }

        private void chbExtremeSportCancellationPartner_Checked(object sender, RoutedEventArgs e)
        {
            txtExtremeSportCancellationPartner.IsEnabled = true;
        }

        private void chbExtremeSportCancellationPartner_Unchecked(object sender, RoutedEventArgs e)
        {
            txtExtremeSportCancellationPartner.IsEnabled = false;
            txtExtremeSportCancellationPartner.Clear();
        }

        private void chbMedicalChargesPartner_Checked(object sender, RoutedEventArgs e)
        {
            txtMedicalChargesPartner.IsEnabled = true;
        }

        private void chbMedicalChargesPartner_Unchecked(object sender, RoutedEventArgs e)
        {
            txtMedicalChargesPartner.IsEnabled = false;
            txtMedicalChargesPartner.Clear();
        }

        private void chbMedicalAndDentalChargesPartner_Checked(object sender, RoutedEventArgs e)
        {
            txtMedicalAndDentalChargesPartner.IsEnabled = true;
        }

        private void chbMedicalAndDentalChargesPartner_Unchecked(object sender, RoutedEventArgs e)
        {
            txtMedicalAndDentalChargesPartner.IsEnabled = false;
            txtMedicalAndDentalChargesPartner.Clear();
        }

        private void chbWarRiskCoveragePartner_Checked(object sender, RoutedEventArgs e)
        {
            txtWarRiskCoveragePartner.IsEnabled = true;
        }

        private void chbWarRiskCoveragePartner_Unchecked(object sender, RoutedEventArgs e)
        {
            txtWarRiskCoveragePartner.IsEnabled = false;
            txtWarRiskCoveragePartner.Clear();
        }

        private void chbIncapacityByAccidentPartner_Checked(object sender, RoutedEventArgs e)
        {
            txtIncapacityByAccidentWaitingDaysPartner.IsEnabled = true;
            txtIncapacityByAccidentPartner.IsEnabled = true;
        }

        private void chbIncapacityByAccidentPartner_Unchecked(object sender, RoutedEventArgs e)
        {
            txtIncapacityByAccidentWaitingDaysPartner.IsEnabled = false;
            txtIncapacityByAccidentWaitingDaysPartner.Clear();
            txtIncapacityByAccidentPartner.IsEnabled = false;
            txtIncapacityByAccidentPartner.Clear();
        }

        private void chbIncapacityBySicknessPartner_Checked(object sender, RoutedEventArgs e)
        {
            txtIncapacityBySicknessWaitingDaysPartner.IsEnabled = true;
            txtIncapacityBySicknessPartner.IsEnabled = true;
        }

        private void chbIncapacityBySicknessPartner_Unchecked(object sender, RoutedEventArgs e)
        {
            txtIncapacityBySicknessWaitingDaysPartner.IsEnabled = false;
            txtIncapacityBySicknessWaitingDaysPartner.Clear();
            txtIncapacityBySicknessPartner.IsEnabled = false;
            txtIncapacityBySicknessPartner.Clear();
        }

        private void chbMedicalChargesChildren_Checked(object sender, RoutedEventArgs e)
        {
            txtMedicalChargesChildren.IsEnabled = true;
        }

        private void chbMedicalChargesChildren_Unchecked(object sender, RoutedEventArgs e)
        {
            txtMedicalChargesChildren.IsEnabled = false;
            txtMedicalChargesChildren.Clear();
        }

        private void chbMedicalAndDentalChargesChildren_Checked(object sender, RoutedEventArgs e)
        {
            txtMedicalAndDentalChargesChildren.IsEnabled = true;
        }

        private void chbMedicalAndDentalChargesChildren_Unchecked(object sender, RoutedEventArgs e)
        {
            txtMedicalAndDentalChargesChildren.IsEnabled = false;
            txtMedicalAndDentalChargesChildren.Clear();
        }

        private void chbWarRiskCoverageChildren_Checked(object sender, RoutedEventArgs e)
        {
            txtWarRiskCoverageChildren.IsEnabled = true;
        }

        private void chbWarRiskCoverageChildren_Unchecked(object sender, RoutedEventArgs e)
        {
            txtWarRiskCoverageChildren.IsEnabled = false;
            txtWarRiskCoverageChildren.Clear();
        }

        private void chbOther1Principal_Checked(object sender, RoutedEventArgs e)
        {
            txtOther1Principal.IsEnabled = true;
        }

        private void chbOther1Principal_Unchecked(object sender, RoutedEventArgs e)
        {
            txtOther1Principal.IsEnabled = false;
            txtOther1Principal.Clear();
        }

        private void chbOther2Principal_Checked(object sender, RoutedEventArgs e)
        {
            txtOther2Principal.IsEnabled = true;
        }

        private void chbOther2Principal_Unchecked(object sender, RoutedEventArgs e)
        {
            txtOther2Principal.IsEnabled = false;
            txtOther2Principal.Clear();
        }

        private void chbOther3Principal_Checked(object sender, RoutedEventArgs e)
        {
            txtOther3Principal.IsEnabled = true;
        }

        private void chbOther3Principal_Unchecked(object sender, RoutedEventArgs e)
        {
            txtOther3Principal.IsEnabled = false;
            txtOther3Principal.Clear();
        }

        private void chbOther4Principal_Checked(object sender, RoutedEventArgs e)
        {
            txtOther4Principal.IsEnabled = true;
        }

        private void chbOther4Principal_Unchecked(object sender, RoutedEventArgs e)
        {
            txtOther4Principal.IsEnabled = false;
            txtOther4Principal.Clear();
        }

        private void chbOther5Principal_Checked(object sender, RoutedEventArgs e)
        {
            txtOther5Principal.IsEnabled = true;
        }

        private void chbOther5Principal_Unchecked(object sender, RoutedEventArgs e)
        {
            txtOther5Principal.IsEnabled = false;
            txtOther5Principal.Clear();
        }

        private void chbOther1Partner_Checked(object sender, RoutedEventArgs e)
        {
            txtOther1Partner.IsEnabled = true;
        }

        private void chbOther1Partner_Unchecked(object sender, RoutedEventArgs e)
        {
            txtOther1Partner.IsEnabled = false;
            txtOther1Partner.Clear();
        }

        private void chbOther2Partner_Checked(object sender, RoutedEventArgs e)
        {
            txtOther2Partner.IsEnabled = true;
        }

        private void chbOther2Partner_Unchecked(object sender, RoutedEventArgs e)
        {
            txtOther2Partner.IsEnabled = false;
            txtOther2Partner.Clear();
        }

        private void chbOther3Partner_Checked(object sender, RoutedEventArgs e)
        {
            txtOther3Partner.IsEnabled = true;
        }

        private void chbOther3Partner_Unchecked(object sender, RoutedEventArgs e)
        {
            txtOther3Partner.IsEnabled = false;
            txtOther3Partner.Clear();
        }

        private void chbOther4Partner_Checked(object sender, RoutedEventArgs e)
        {
            txtOther4Partner.IsEnabled = true;
        }

        private void chbOther4Partner_Unchecked(object sender, RoutedEventArgs e)
        {
            txtOther4Partner.IsEnabled = false;
            txtOther4Partner.Clear();
        }

        private void chbOther5Partner_Checked(object sender, RoutedEventArgs e)
        {
            txtOther5Partner.IsEnabled = true;
        }

        private void chbOther5Partner_Unchecked(object sender, RoutedEventArgs e)
        {
            txtOther5Partner.IsEnabled = false;
            txtOther5Partner.Clear();
        }

        private void chbOther1Children_Checked(object sender, RoutedEventArgs e)
        {
            txtOther1Children.IsEnabled = true;
        }

        private void chbOther1Children_Unchecked(object sender, RoutedEventArgs e)
        {
            txtOther1Children.IsEnabled = false;
            txtOther1Children.Clear();
        }

        private void chbOther2Children_Checked(object sender, RoutedEventArgs e)
        {
            txtOther2Children.IsEnabled = true;
        }

        private void chbOther2Children_Unchecked(object sender, RoutedEventArgs e)
        {
            txtOther2Children.IsEnabled = false;
            txtOther2Children.Clear();
        }

        private void chbOther3Children_Checked(object sender, RoutedEventArgs e)
        {
            txtOther3Children.IsEnabled = true;
        }

        private void chbOther3Children_Unchecked(object sender, RoutedEventArgs e)
        {
            txtOther3Children.IsEnabled = false;
            txtOther3Children.Clear();
        }

        private void chbOther4Children_Checked(object sender, RoutedEventArgs e)
        {
            txtOther4Children.IsEnabled = true;
        }

        private void chbOther4Children_Unchecked(object sender, RoutedEventArgs e)
        {
            txtOther4Children.IsEnabled = false;
            txtOther4Children.Clear();
        }

        private void chbOther5Children_Checked(object sender, RoutedEventArgs e)
        {
            txtOther5Children.IsEnabled = true;
        }

        private void chbOther5Children_Unchecked(object sender, RoutedEventArgs e)
        {
            txtOther5Children.IsEnabled = false;
            txtOther5Children.Clear();
        }

        private void chbMedicalAdviceAndPeriodiCheckPrincipal_Checked(object sender, RoutedEventArgs e)
        {
            txtMedicalAdviceAndPeriodiCheckPrincipal.IsEnabled = true;
        }

        private void chbMedicalAdviceAndPeriodiCheckPrincipal_Unchecked(object sender, RoutedEventArgs e)
        {
            txtMedicalAdviceAndPeriodiCheckPrincipal.IsEnabled = false;
            txtMedicalAdviceAndPeriodiCheckPrincipal.Clear();
        }

        private void chbDiagnosticTestsAndPregnancyAnnexPrincipal_Checked(object sender, RoutedEventArgs e)
        {
            txtDiagnosticTestsAndPregnancyAnnexTypePrincipal.IsEnabled = true;
            txtDiagnosticTestsAndPregnancyAnnexPrincipal.IsEnabled = true;
        }

        private void chbDiagnosticTestsAndPregnancyAnnexPrincipal_Unchecked(object sender, RoutedEventArgs e)
        {
            txtDiagnosticTestsAndPregnancyAnnexTypePrincipal.IsEnabled = false;
            txtDiagnosticTestsAndPregnancyAnnexTypePrincipal.Clear();
            txtDiagnosticTestsAndPregnancyAnnexPrincipal.IsEnabled = false;
            txtDiagnosticTestsAndPregnancyAnnexPrincipal.Clear();
        }

        private void chbFastDiagnosisAndMedicalGuidancePrincipal_Checked(object sender, RoutedEventArgs e)
        {
            txtFastDiagnosisAndMedicalGuidancePrincipal.IsEnabled = true;
        }

        private void chbFastDiagnosisAndMedicalGuidancePrincipal_Unchecked(object sender, RoutedEventArgs e)
        {
            txtFastDiagnosisAndMedicalGuidancePrincipal.IsEnabled = false;
            txtFastDiagnosisAndMedicalGuidancePrincipal.Clear();
        }

        private void chbPsicologyPrincipal_Checked(object sender, RoutedEventArgs e)
        {
            txtPsicologyPrincipal.IsEnabled = true;
        }

        private void chbPsicologyPrincipal_Unchecked(object sender, RoutedEventArgs e)
        {
            txtPsicologyPrincipal.IsEnabled = false;
            txtPsicologyPrincipal.Clear();
        }

        private void chbComplementaryPrincipal_Checked(object sender, RoutedEventArgs e)
        {
            txtComplementaryPrincipal.IsEnabled = true;
        }

        private void chbComplementaryPrincipal_Unchecked(object sender, RoutedEventArgs e)
        {
            txtComplementaryPrincipal.IsEnabled = false;
            txtComplementaryPrincipal.Clear();
        }

        private void chbToTheChildPrincipal_Checked(object sender, RoutedEventArgs e)
        {
            txtToTheChildPrincipal.IsEnabled = true;
        }

        private void chbToTheChildPrincipal_Unchecked(object sender, RoutedEventArgs e)
        {
            txtToTheChildPrincipal.IsEnabled = false;
            txtToTheChildPrincipal.Clear();
        }

        private void chbMedicalAdviceAndPeriodiCheckPartner_Checked(object sender, RoutedEventArgs e)
        {
            txtMedicalAdviceAndPeriodiCheckPartner.IsEnabled = true;
        }

        private void chbMedicalAdviceAndPeriodiCheckPartner_Unchecked(object sender, RoutedEventArgs e)
        {
            txtMedicalAdviceAndPeriodiCheckPartner.IsEnabled = false;
            txtMedicalAdviceAndPeriodiCheckPartner.Clear();
        }

        private void chbDiagnosticTestsAndPregnancyAnnexPartner_Checked(object sender, RoutedEventArgs e)
        {
            txtDiagnosticTestsAndPregnancyAnnexTypePartner.IsEnabled = true;
            txtDiagnosticTestsAndPregnancyAnnexPartner.IsEnabled = true;
        }

        private void chbDiagnosticTestsAndPregnancyAnnexPartner_Unchecked(object sender, RoutedEventArgs e)
        {
            txtDiagnosticTestsAndPregnancyAnnexTypePartner.IsEnabled = false;
            txtDiagnosticTestsAndPregnancyAnnexTypePartner.Clear();
            txtDiagnosticTestsAndPregnancyAnnexPartner.IsEnabled = false;
            txtDiagnosticTestsAndPregnancyAnnexPartner.Clear();
        }

        private void chbFastDiagnosisAndMedicalGuidancePartner_Checked(object sender, RoutedEventArgs e)
        {
            txtFastDiagnosisAndMedicalGuidancePartner.IsEnabled = true;
        }

        private void chbFastDiagnosisAndMedicalGuidancePartner_Unchecked(object sender, RoutedEventArgs e)
        {
            txtFastDiagnosisAndMedicalGuidancePartner.IsEnabled = false;
            txtFastDiagnosisAndMedicalGuidancePartner.Clear();
        }

        private void chbPsicologyPartner_Checked(object sender, RoutedEventArgs e)
        {
            txtPsicologyPartner.IsEnabled = true;
        }

        private void chbPsicologyPartner_Unchecked(object sender, RoutedEventArgs e)
        {
            txtPsicologyPartner.IsEnabled = false;
            txtPsicologyPartner.Clear();
        }

        private void chbComplementaryPartner_Checked(object sender, RoutedEventArgs e)
        {
            txtComplementaryPartner.IsEnabled = true;
        }

        private void chbComplementaryPartner_Unchecked(object sender, RoutedEventArgs e)
        {
            txtComplementaryPartner.IsEnabled = false;
            txtComplementaryPartner.Clear();
        }

        private void chbToTheChildPartner_Checked(object sender, RoutedEventArgs e)
        {
            txtToTheChildPartner.IsEnabled = true;
        }

        private void chbToTheChildPartner_Unchecked(object sender, RoutedEventArgs e)
        {
            txtToTheChildPartner.IsEnabled = false;
            txtToTheChildPartner.Clear();
        }

        private void chbMedicalAdviceAndPeriodiCheckChildren_Checked(object sender, RoutedEventArgs e)
        {
            txtMedicalAdviceAndPeriodiCheckChildren.IsEnabled = true;
        }

        private void chbMedicalAdviceAndPeriodiCheckChildren_Unchecked(object sender, RoutedEventArgs e)
        {
            txtMedicalAdviceAndPeriodiCheckChildren.IsEnabled = false;
            txtMedicalAdviceAndPeriodiCheckChildren.Clear();
        }

        private void chbDiagnosticTestsAndPregnancyAnnexChildren_Checked(object sender, RoutedEventArgs e)
        {
            txtDiagnosticTestsAndPregnancyAnnexTypeChildren.IsEnabled = true;
            txtDiagnosticTestsAndPregnancyAnnexChildren.IsEnabled = true;
        }

        private void chbDiagnosticTestsAndPregnancyAnnexChildren_Unchecked(object sender, RoutedEventArgs e)
        {
            txtDiagnosticTestsAndPregnancyAnnexTypeChildren.IsEnabled = false;
            txtDiagnosticTestsAndPregnancyAnnexTypeChildren.Clear();
            txtDiagnosticTestsAndPregnancyAnnexChildren.IsEnabled = false;
            txtDiagnosticTestsAndPregnancyAnnexChildren.Clear();
        }

        private void chbFastDiagnosisAndMedicalGuidanceChildren_Checked(object sender, RoutedEventArgs e)
        {
            txtFastDiagnosisAndMedicalGuidanceChildren.IsEnabled = true;
        }

        private void chbFastDiagnosisAndMedicalGuidanceChildren_Unchecked(object sender, RoutedEventArgs e)
        {
            txtFastDiagnosisAndMedicalGuidanceChildren.IsEnabled = false;
            txtFastDiagnosisAndMedicalGuidanceChildren.Clear();
        }

        private void chbPsicologyChildren_Checked(object sender, RoutedEventArgs e)
        {
            txtPsicologyChildren.IsEnabled = true;
        }

        private void chbPsicologyChildren_Unchecked(object sender, RoutedEventArgs e)
        {
            txtPsicologyChildren.IsEnabled = false;
            txtPsicologyChildren.Clear();
        }

        private void chbComplementaryChildren_Checked(object sender, RoutedEventArgs e)
        {
            txtComplementaryChildren.IsEnabled = true;
        }

        private void chbComplementaryChildren_Unchecked(object sender, RoutedEventArgs e)
        {
            txtComplementaryChildren.IsEnabled = false;
            txtComplementaryChildren.Clear();
        }

        private void chbToTheChildChildren_Checked(object sender, RoutedEventArgs e)
        {
            txtToTheChildChildren.IsEnabled = true;
        }

        private void chbToTheChildChildren_Unchecked(object sender, RoutedEventArgs e)
        {
            txtToTheChildChildren.IsEnabled = false;
            txtToTheChildChildren.Clear();
        }

        private void btnNewCoverage_Click(object sender, RoutedEventArgs e)
        {
            frmLifeCoverage newCoverageWindow = new frmLifeCoverage(new PersonalAccidentsInsuranceType());
            newCoverageWindow.ShowDialog();
            if (newCoverageWindow.PersonalAccidentsCoverage != null)
            {
                coverages.Add(newCoverageWindow.PersonalAccidentsCoverage);
            }
            dgCoveragesBinding();
        }

        private void dgCoveragesBinding()
        {
            dgCoverages.ItemsSource = coverages.ToList();
            lv.AutoSizeColumns(dgCoverages.View);
        }

        private void btnDeleteCoverage_Click(object sender, RoutedEventArgs e)
        {
            if (dgCoverages.SelectedItem == null)
            {
                MessageBox.Show("נא לסמן כיסוי בטבלה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            if (MessageBox.Show("למחוק כיסוי", "אישור", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                coverages.Remove((PersonalAccidentsCoverage)dgCoverages.SelectedItem);
                dgCoveragesBinding();
            }
            else
            {
                dgCoverages.UnselectAll();
            }
        }

        private void btnUpdateCoverage_Click(object sender, RoutedEventArgs e)
        {
            if (dgCoverages.SelectedItem == null)
            {
                MessageBox.Show("נא לסמן כיסוי בטבלה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            PersonalAccidentsCoverage coverageToUpdate = (PersonalAccidentsCoverage)dgCoverages.SelectedItem;
            frmLifeCoverage updateCoverageWindow = new frmLifeCoverage(coverageToUpdate, new PersonalAccidentsInsuranceType());
            updateCoverageWindow.ShowDialog();
            coverageToUpdate = updateCoverageWindow.PersonalAccidentsCoverage;
            dgCoveragesBinding();
            dgCoverages.UnselectAll();
        }

        private void dgCoverages_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            DependencyObject src = (DependencyObject)(e.OriginalSource);
            while (!(src is Control))
            {
                if (!(src is Visual))
                {
                    break;
                }
                src = VisualTreeHelper.GetParent(src);
            }

            if ((src is Control && src.GetType() == typeof(ListViewItem)) || src.GetType() == typeof(Run))
            {
                if (dgCoverages.SelectedItem == null)
                {
                    return;
                }
                PersonalAccidentsCoverage coverageToUpdate = (PersonalAccidentsCoverage)dgCoverages.SelectedItem;
                frmLifeCoverage updateCoverageWindow = new frmLifeCoverage(coverageToUpdate, new PersonalAccidentsInsuranceType());
                updateCoverageWindow.ShowDialog();
                coverageToUpdate = updateCoverageWindow.PersonalAccidentsCoverage;
                dgCoveragesBinding();
                dgCoverages.UnselectAll();
            }
        }

        private void btnNewTracking_Click(object sender, RoutedEventArgs e)
        {
            frmTracking newTrackingWindow = new frmTracking(client, personalAccidentsPolicy, user);
            newTrackingWindow.ShowDialog();
            if (newTrackingWindow.trackingContent != null)
            {
                trackings.Add(new PersonalAccidentsTracking() { TrackingContent = newTrackingWindow.trackingContent, Date = DateTime.Now, UserID = user.UserID, User = user });
                dgTrackingsBinding();
            }
        }

        private void dgTrackingsBinding()
        {
            dgTrackings.ItemsSource = trackings.OrderByDescending(t => t.Date);
            lv.AutoSizeColumns(dgTrackings.View);
        }

        private void btnDeleteTracking_Click(object sender, RoutedEventArgs e)
        {
            if (dgTrackings.SelectedItem == null)
            {
                MessageBox.Show("נא לסמן מעקב בטבלה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            if (MessageBox.Show("למחוק מעקב", "אישור", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                trackings.Remove((PersonalAccidentsTracking)dgTrackings.SelectedItem);
                dgTrackingsBinding();
            }
            else
            {
                dgTrackings.UnselectAll();
            }
        }

        private void btnUpdateTracking_Click(object sender, RoutedEventArgs e)
        {
            if (dgTrackings.SelectedItem == null)
            {
                MessageBox.Show("נא לסמן מעקב בטבלה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            PersonalAccidentsTracking trackingToUpdate = (PersonalAccidentsTracking)dgTrackings.SelectedItem;
            frmTracking updateTrackingWindow = new frmTracking(trackingToUpdate, client, personalAccidentsPolicy, user);
            updateTrackingWindow.ShowDialog();
            trackingToUpdate.TrackingContent = updateTrackingWindow.trackingContent;
            dgTrackingsBinding();
            dgTrackings.UnselectAll();
        }

        private void dgTrackings_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            DependencyObject src = (DependencyObject)(e.OriginalSource);
            while (!(src is Control))
            {
                if (!(src is Visual))
                {
                    break;
                }
                src = VisualTreeHelper.GetParent(src);
            }

            if ((src is Control && src.GetType() == typeof(ListViewItem)) || src.GetType() == typeof(Run))
            {
                if (dgTrackings.SelectedItem == null)
                {
                    return;
                }
                PersonalAccidentsTracking trackingToUpdate = (PersonalAccidentsTracking)dgTrackings.SelectedItem;
                frmTracking updateTrackingWindow = new frmTracking(trackingToUpdate, client, personalAccidentsPolicy, user);
                updateTrackingWindow.ShowDialog();
                trackingToUpdate.TrackingContent = updateTrackingWindow.trackingContent;
                dgTrackingsBinding();
                dgTrackings.UnselectAll();
            }
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void txtPaymentesNumber_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (sender is TextBox)
            {
                TextBox txtBox = (TextBox)sender;
                txtBox.ClearValue(BorderBrushProperty);
                if (txtBox.Text != "")
                {
                    try
                    {
                        decimal reusult = validations.ConvertStringToInt(txtBox.Text);
                    }
                    catch (Exception ex)
                    {
                        txtBox.BorderBrush = System.Windows.Media.Brushes.Red;
                        System.Windows.Forms.MessageBox.Show(ex.Message);
                        txtBox.Text = "";
                    }
                }
            }
        }

        private void dgCoverages_MouseDown(object sender, MouseButtonEventArgs e)
        {
            dgCoverages.UnselectAll();
        }

        private void dgTrackings_MouseDown(object sender, MouseButtonEventArgs e)
        {
            dgTrackings.UnselectAll();
        }

        private void txtPolicyNumber_GotFocus(object sender, RoutedEventArgs e)
        {
            isChanges = true;
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (confirmBeforeClosing && isChanges)
            {
                if (MessageBox.Show("האם ברצונך לשמור שינוים שבצעת", "אישור", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    btnSave_Click(sender, new RoutedEventArgs());
                    if (cancelClose)
                    {
                        e.Cancel = true;
                        cancelClose = false;
                    }
                }
            }
        }

        private void chbStandardMoneyCollection_Checked(object sender, RoutedEventArgs e)
        {
            btnStandardMoneyCollectionDetails.IsEnabled = true;
        }
        private void chbStandardMoneyCollection_Unchecked(object sender, RoutedEventArgs e)
        {
            btnStandardMoneyCollectionDetails.IsEnabled = false;
        }

        private void btnStandardMoneyCollectionDetails_Click(object sender, RoutedEventArgs e)
        {
            frmStandardMoneyCollectionDetails window = null;
            if (standardMoneyCollection == null)
            {
                window = new frmStandardMoneyCollectionDetails(false, user);
            }
            else
            {
                window = new frmStandardMoneyCollectionDetails(standardMoneyCollection, standardMoneyCollectionTrackings, false, user);
            }
            window.ShowDialog();
            if (window.StandardMoneyCollection != null)
            {
                standardMoneyCollection = window.StandardMoneyCollection;
            }
            if (window.Trackings != null)
            {
                standardMoneyCollectionTrackings = window.Trackings;
            }
        }
    }
}