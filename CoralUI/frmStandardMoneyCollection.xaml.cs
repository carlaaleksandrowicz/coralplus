﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using CoralBusinessLogics;

namespace CoralUI
{
    /// <summary>
    /// Interaction logic for frmStandardMoneyCollection.xaml
    /// </summary>
    public partial class frmStandardMoneyCollection : Window
    {
        StandardMoneyCollectionLogic standardMoneyCollectionLogic = new StandardMoneyCollectionLogic();
        List<StandardMoneyCollection> standardMoneyCollections = new List<StandardMoneyCollection>();
        InsurancesLogic insuranceLogic = new InsurancesLogic();
        IndustriesLogic elementaryIndustryLogic = new IndustriesLogic();
        LifeIndustryLogic lifeIndustryLogic = new LifeIndustryLogic();
        FinanceIndustryLogic financeIndustryLogic = new FinanceIndustryLogic();
        ForeingWorkersIndutryLogic foreingWorkersIndustryLogic = new ForeingWorkersIndutryLogic();
        PoliciesLogic elementaryLogic = new PoliciesLogic();
        PersonalAccidentsLogic personalAccidentsLogic = new PersonalAccidentsLogic();
        TravelLogic travelLogic = new TravelLogic();
        LifePoliciesLogic lifeLogic = new LifePoliciesLogic();
        HealthPoliciesLogic healthLogic = new HealthPoliciesLogic();
        FinancePoliciesLogic financeLogic = new FinancePoliciesLogic();
        ForeingWorkersLogic foreingWorkersLogic = new ForeingWorkersLogic();
        User user;
        ClientsLogic clientLogic = new ClientsLogic();
        Email email = new Email();
        ExcelLogic excelLogic = new ExcelLogic();
        public frmStandardMoneyCollection(User user)
        {
            InitializeComponent();
            this.SizeToContent = SizeToContent.Manual;
            this.user = user;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            cbStandardMoneyCollectionTypeBinding();
            GetStandardMoneyCollections();
        }

        private void cbStandardMoneyCollectionTypeBinding()
        {
            cbStandardMoneyCollectionType.ItemsSource = standardMoneyCollectionLogic.GetActiveMoneyCollectionTypes();
            cbStandardMoneyCollectionType.DisplayMemberPath = "MoneyCollectionTypeName";
        }

        private void GetStandardMoneyCollections()
        {
            standardMoneyCollections = standardMoneyCollectionLogic.GetAllStandardMoneyCollections();
            StandardMoneyCollectionsBinding();
        }

        private void policiesTabs_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.Source is TabControl)
            {
                ClearSearchInputs();
                cbIndustryBinding();
                cbCompanyBinding();
                StandardMoneyCollectionsBinding();
            }
        }

        private void cbCompanyBinding()
        {
            if (tbItemElementary.IsSelected)
            {
                cbCompany.ItemsSource = insuranceLogic.GetCompaniesByInsurance(insuranceLogic.GetInsuranceID("אלמנטרי"));
            }
            else if (tbItemPersonalAccidents.IsSelected)
            {
                cbCompany.ItemsSource = insuranceLogic.GetCompaniesByInsurance(insuranceLogic.GetInsuranceID("תאונות אישיות"));
            }
            else if (tbItemTravel.IsSelected)
            {
                cbCompany.ItemsSource = insuranceLogic.GetCompaniesByInsurance(insuranceLogic.GetInsuranceID("נסיעות לחו''ל"));
            }
            else if (tbItemLife.IsSelected)
            {
                cbCompany.ItemsSource = insuranceLogic.GetCompaniesByInsurance(insuranceLogic.GetInsuranceID("חיים"));
            }
            else if (tbItemHealth.IsSelected)
            {
                cbCompany.ItemsSource = insuranceLogic.GetCompaniesByInsurance(insuranceLogic.GetInsuranceID("בריאות"));
            }
            else if (tbItemFinance.IsSelected)
            {
                cbCompany.ItemsSource = insuranceLogic.GetCompaniesByInsurance(insuranceLogic.GetInsuranceID("פיננסים"));
            }
            else if (tbItemForeingWorkers.IsSelected)
            {
                cbCompany.ItemsSource = insuranceLogic.GetCompaniesByInsurance(insuranceLogic.GetInsuranceID("עובדים זרים"));
            }
            cbCompany.DisplayMemberPath = "Company.CompanyName";
        }

        private void cbIndustryBinding()
        {
            if (tbItemElementary.IsSelected)
            {
                cbIndustry.ItemsSource = elementaryIndustryLogic.GetAllIndustries();
                cbIndustry.DisplayMemberPath = "InsuranceIndustryName";
                cbIndustry.IsEnabled = true;
            }
            else if (tbItemLife.IsSelected)
            {
                cbIndustry.ItemsSource = lifeIndustryLogic.GetAllLifeIndustries();
                cbIndustry.DisplayMemberPath = "LifeIndustryName";
                cbIndustry.IsEnabled = true;
            }

            else if (tbItemFinance.IsSelected)
            {
                cbIndustry.ItemsSource = financeIndustryLogic.GetAllFinanceProgramTypes();
                cbIndustry.DisplayMemberPath = "ProgramTypeName";
                cbIndustry.IsEnabled = true;
            }
            else if (tbItemForeingWorkers.IsSelected)
            {
                cbIndustry.ItemsSource = foreingWorkersIndustryLogic.GetAllForeingWorkersIndustries();
                cbIndustry.DisplayMemberPath = "ForeingWorkersIndustryName";
                cbIndustry.IsEnabled = true;
            }
            else
            {
                cbIndustry.IsEnabled = false;
            }
        }

        private void ClearSearchInputs()
        {
            cbCompany.SelectedIndex = -1;
            cbIndustry.SelectedIndex = -1;
            cbStandardMoneyCollectionType.SelectedIndex = -1;
            txtSearch.Clear();
        }
        private void StandardMoneyCollectionsBinding()
        {
            if (tbItemElementary.IsSelected)
            {
                var elementary = standardMoneyCollections.Where(s => s.ElementaryPolicyID != null).OrderByDescending(s => s.ElementaryPolicy.StartDate).ToList();
                if (cbStandardMoneyCollectionStatus.SelectedIndex == 0)
                {
                    elementary = elementary.Where(s => s.IsStandardMoneyCollectionOpen == true).ToList();
                }
                else
                {
                    elementary = elementary.Where(s => s.IsStandardMoneyCollectionOpen != true).ToList();
                }
                if (cbStandardMoneyCollectionType.SelectedItem != null)
                {
                    elementary = elementary.Where(s => s.MoneyCollectionTypeID == ((MoneyCollectionType)cbStandardMoneyCollectionType.SelectedItem).MoneyCollectionTypeID).ToList();
                }
                if (cbPolicyStatus.SelectedIndex == 0)
                {
                    elementary = elementary.Where(s => s.ElementaryPolicy.EndDate >= DateTime.Today).ToList();
                }
                else if (cbPolicyStatus.SelectedIndex == 1)
                {
                    elementary = elementary.Where(s => s.ElementaryPolicy.EndDate < DateTime.Today).ToList();
                }
                if (cbIndustry.SelectedItem != null)
                {
                    elementary = elementary.Where(s => s.ElementaryPolicy.InsuranceIndustryID == ((InsuranceIndustry)cbIndustry.SelectedItem).InsuranceIndustryID).ToList();
                }
                if (cbCompany.SelectedItem != null)
                {
                    elementary = elementary.Where(s => s.ElementaryPolicy.CompanyID == ((InsuranceCompany)cbCompany.SelectedItem).CompanyID).ToList();
                }
                if (txtSearch.Text != "")
                {
                    string key = txtSearch.Text;
                    elementary = elementary.Where(s => s.ElementaryPolicy.PolicyNumber.StartsWith(key) || s.ElementaryPolicy.Client.LastName.StartsWith(key) || s.ElementaryPolicy.Client.FirstName.StartsWith(key) || (s.ElementaryPolicy.Client.LastName + " " + s.ElementaryPolicy.Client.FirstName).StartsWith(key) || (s.ElementaryPolicy.Client.FirstName + " " + s.ElementaryPolicy.Client.LastName).StartsWith(key) || s.ElementaryPolicy.Client.IdNumber.StartsWith(key)).ToList();
                }
                dgElementary.ItemsSource = elementary;
                lblCount.Content = string.Format("סה''כ רשומות: {0}  סה''כ חוב: {1} ש''ח", elementary.Count.ToString(), ((decimal)(elementary.Where(s => s.MoneyCollectionAmount != null).Sum(s => s.MoneyCollectionAmount))).ToString("0.##"));
            }
            else if (tbItemPersonalAccidents.IsSelected)
            {
                var personalAccidents = standardMoneyCollections.Where(s => s.PersonalAccidentsPolicyID != null).OrderByDescending(s => s.PersonalAccidentsPolicy.StartDate).ToList();
                if (cbStandardMoneyCollectionStatus.SelectedIndex == 0)
                {
                    personalAccidents = personalAccidents.Where(s => s.IsStandardMoneyCollectionOpen == true).ToList();
                }
                else
                {
                    personalAccidents = personalAccidents.Where(s => s.IsStandardMoneyCollectionOpen != true).ToList();
                }
                if (cbStandardMoneyCollectionType.SelectedItem != null)
                {
                    personalAccidents = personalAccidents.Where(s => s.MoneyCollectionTypeID == ((MoneyCollectionType)cbStandardMoneyCollectionType.SelectedItem).MoneyCollectionTypeID).ToList();
                }
                if (cbPolicyStatus.SelectedIndex == 0)
                {
                    personalAccidents = personalAccidents.Where(s => s.PersonalAccidentsPolicy.EndDate >= DateTime.Today).ToList();
                }
                else if (cbPolicyStatus.SelectedIndex == 1)
                {
                    personalAccidents = personalAccidents.Where(s => s.PersonalAccidentsPolicy.EndDate < DateTime.Today).ToList();
                }
                if (cbCompany.SelectedItem != null)
                {
                    personalAccidents = personalAccidents.Where(s => s.PersonalAccidentsPolicy.CompanyID == ((InsuranceCompany)cbCompany.SelectedItem).CompanyID).ToList();
                }
                if (txtSearch.Text != "")
                {
                    string key = txtSearch.Text;
                    personalAccidents = personalAccidents.Where(s => s.PersonalAccidentsPolicy.PolicyNumber.StartsWith(key) || s.PersonalAccidentsPolicy.Client.LastName.StartsWith(key) || s.PersonalAccidentsPolicy.Client.FirstName.StartsWith(key) || s.PersonalAccidentsPolicy.Client.IdNumber.StartsWith(key)).ToList();
                }
                dgPersonalAccidentsPolicies.ItemsSource = personalAccidents;
                lblCount.Content = string.Format("סה''כ רשומות: {0}  סה''כ חוב: {1} ש''ח", personalAccidents.Count.ToString(), ((decimal)(personalAccidents.Where(s => s.MoneyCollectionAmount != null).Sum(s => s.MoneyCollectionAmount))).ToString("0.##"));
            }
            else if (tbItemTravel.IsSelected)
            {
                var travel = standardMoneyCollections.Where(s => s.TravelPolicyID != null).OrderByDescending(s => s.TravelPolicy.StartDate).ToList();
                if (cbStandardMoneyCollectionStatus.SelectedIndex == 0)
                {
                    travel = travel.Where(s => s.IsStandardMoneyCollectionOpen == true).ToList();
                }
                else
                {
                    travel = travel.Where(s => s.IsStandardMoneyCollectionOpen != true).ToList();
                }
                if (cbStandardMoneyCollectionType.SelectedItem != null)
                {
                    travel = travel.Where(s => s.MoneyCollectionTypeID == ((MoneyCollectionType)cbStandardMoneyCollectionType.SelectedItem).MoneyCollectionTypeID).ToList();
                }
                if (cbPolicyStatus.SelectedIndex == 0)
                {
                    travel = travel.Where(s => s.TravelPolicy.EndDate >= DateTime.Today).ToList();
                }
                else if (cbPolicyStatus.SelectedIndex == 1)
                {
                    travel = travel.Where(s => s.TravelPolicy.EndDate < DateTime.Today).ToList();
                }
                if (cbCompany.SelectedItem != null)
                {
                    travel = travel.Where(s => s.TravelPolicy.CompanyID == ((InsuranceCompany)cbCompany.SelectedItem).CompanyID).ToList();
                }
                if (txtSearch.Text != "")
                {
                    string key = txtSearch.Text;
                    travel = travel.Where(s => s.TravelPolicy.PolicyNumber.StartsWith(key) || s.TravelPolicy.Client.LastName.StartsWith(key) || s.TravelPolicy.Client.FirstName.StartsWith(key) || s.TravelPolicy.Client.IdNumber.StartsWith(key)).ToList();
                }
                dgTravelPolicies.ItemsSource = travel;
                lblCount.Content = string.Format("סה''כ רשומות: {0}  סה''כ חוב: {1} ש''ח", travel.Count.ToString(), ((decimal)(travel.Where(s => s.MoneyCollectionAmount != null).Sum(s => s.MoneyCollectionAmount))).ToString("0.##"));
            }
            else if (tbItemLife.IsSelected)
            {
                var life = standardMoneyCollections.Where(s => s.LifePolicyID != null).OrderByDescending(s => s.LifePolicy.StartDate).ToList();
                if (cbStandardMoneyCollectionStatus.SelectedIndex == 0)
                {
                    life = life.Where(s => s.IsStandardMoneyCollectionOpen == true).ToList();
                }
                else
                {
                    life = life.Where(s => s.IsStandardMoneyCollectionOpen != true).ToList();
                }
                if (cbStandardMoneyCollectionType.SelectedItem != null)
                {
                    life = life.Where(s => s.MoneyCollectionTypeID == ((MoneyCollectionType)cbStandardMoneyCollectionType.SelectedItem).MoneyCollectionTypeID).ToList();
                }
                if (cbPolicyStatus.SelectedIndex == 0)
                {
                    life = life.Where(s => s.LifePolicy.EndDate >= DateTime.Today).ToList();
                }
                else if (cbPolicyStatus.SelectedIndex == 1)
                {
                    life = life.Where(s => s.LifePolicy.EndDate < DateTime.Today).ToList();
                }
                if (cbIndustry.SelectedItem != null)
                {
                    life = life.Where(s => s.LifePolicy.LifeIndustryID == ((LifeIndustry)cbIndustry.SelectedItem).LifeIndustryID).ToList();
                }
                if (cbCompany.SelectedItem != null)
                {
                    life = life.Where(s => s.LifePolicy.CompanyID == ((InsuranceCompany)cbCompany.SelectedItem).CompanyID).ToList();
                }
                if (txtSearch.Text != "")
                {
                    string key = txtSearch.Text;
                    life = life.Where(s => s.LifePolicy.PolicyNumber.StartsWith(key) || s.LifePolicy.Client.LastName.StartsWith(key) || s.LifePolicy.Client.FirstName.StartsWith(key) || s.LifePolicy.Client.IdNumber.StartsWith(key)).ToList();
                }
                dgLife.ItemsSource = life;
                lblCount.Content = string.Format("סה''כ רשומות: {0}  סה''כ חוב: {1} ש''ח", life.Count.ToString(), ((decimal)(life.Where(s => s.MoneyCollectionAmount != null).Sum(s => s.MoneyCollectionAmount))).ToString("0.##"));
            }
            else if (tbItemHealth.IsSelected)
            {
                var health = standardMoneyCollections.Where(s => s.HealthPolicyID != null).OrderByDescending(s => s.HealthPolicy.StartDate).ToList();
                if (cbStandardMoneyCollectionStatus.SelectedIndex == 0)
                {
                    health = health.Where(s => s.IsStandardMoneyCollectionOpen == true).ToList();
                }
                else
                {
                    health = health.Where(s => s.IsStandardMoneyCollectionOpen != true).ToList();
                }
                if (cbStandardMoneyCollectionType.SelectedItem != null)
                {
                    health = health.Where(s => s.MoneyCollectionTypeID == ((MoneyCollectionType)cbStandardMoneyCollectionType.SelectedItem).MoneyCollectionTypeID).ToList();
                }
                if (cbPolicyStatus.SelectedIndex == 0)
                {
                    health = health.Where(s => s.HealthPolicy.EndDate >= DateTime.Today).ToList();
                }
                else if (cbPolicyStatus.SelectedIndex == 1)
                {
                    health = health.Where(s => s.HealthPolicy.EndDate < DateTime.Today).ToList();
                }
                if (cbCompany.SelectedItem != null)
                {
                    health = health.Where(s => s.HealthPolicy.CompanyID == ((InsuranceCompany)cbCompany.SelectedItem).CompanyID).ToList();
                }
                if (txtSearch.Text != "")
                {
                    string key = txtSearch.Text;
                    health = health.Where(s => s.HealthPolicy.PolicyNumber.StartsWith(key) || s.HealthPolicy.Client.LastName.StartsWith(key) || s.HealthPolicy.Client.FirstName.StartsWith(key) || s.HealthPolicy.Client.IdNumber.StartsWith(key)).ToList();
                }
                dgHealth.ItemsSource = health;
                lblCount.Content = string.Format("סה''כ רשומות: {0}  סה''כ חוב: {1} ש''ח", health.Count.ToString(), ((decimal)(health.Where(s => s.MoneyCollectionAmount != null).Sum(s => s.MoneyCollectionAmount))).ToString("0.##"));
            }
            else if (tbItemFinance.IsSelected)
            {
                var finance = standardMoneyCollections.Where(s => s.FinancePolicyID != null).OrderByDescending(s => s.FinancePolicy.StartDate).ToList();
                if (cbStandardMoneyCollectionStatus.SelectedIndex == 0)
                {
                    finance = finance.Where(s => s.IsStandardMoneyCollectionOpen == true).ToList();
                }
                else
                {
                    finance = finance.Where(s => s.IsStandardMoneyCollectionOpen != true).ToList();
                }
                if (cbStandardMoneyCollectionType.SelectedItem != null)
                {
                    finance = finance.Where(s => s.MoneyCollectionTypeID == ((MoneyCollectionType)cbStandardMoneyCollectionType.SelectedItem).MoneyCollectionTypeID).ToList();
                }
                if (cbPolicyStatus.SelectedIndex == 0)
                {
                    finance = finance.Where(s => s.FinancePolicy.EndDate >= DateTime.Today).ToList();
                }
                else if (cbPolicyStatus.SelectedIndex == 1)
                {
                    finance = finance.Where(s => s.FinancePolicy.EndDate < DateTime.Today).ToList();
                }
                if (cbIndustry.SelectedItem != null)
                {
                    finance = finance.Where(s => s.FinancePolicy.ProgramTypeID == ((FinanceProgramType)cbIndustry.SelectedItem).ProgramTypeID).ToList();
                }
                if (cbCompany.SelectedItem != null)
                {
                    finance = finance.Where(s => s.FinancePolicy.CompanyID == ((InsuranceCompany)cbCompany.SelectedItem).CompanyID).ToList();
                }
                if (txtSearch.Text != "")
                {
                    string key = txtSearch.Text;
                    finance = finance.Where(s => s.FinancePolicy.AssociateNumber.StartsWith(key) || s.FinancePolicy.Client.LastName.StartsWith(key) || s.FinancePolicy.Client.FirstName.StartsWith(key) || s.FinancePolicy.Client.IdNumber.StartsWith(key)).ToList();
                }
                dgFinance.ItemsSource = finance;
                lblCount.Content = string.Format("סה''כ רשומות: {0}  סה''כ חוב: {1} ש''ח", finance.Count.ToString(), ((decimal)(finance.Where(s => s.MoneyCollectionAmount != null).Sum(s => s.MoneyCollectionAmount))).ToString("0.##"));
            }
            else if (tbItemForeingWorkers.IsSelected)
            {
                var foreingWorkers = standardMoneyCollections.Where(s => s.ForeingWorkersPolicyID != null).OrderByDescending(s => s.ForeingWorkersPolicy.StartDate).ToList();
                if (cbStandardMoneyCollectionStatus.SelectedIndex == 0)
                {
                    foreingWorkers = foreingWorkers.Where(s => s.IsStandardMoneyCollectionOpen == true).ToList();
                }
                else
                {
                    foreingWorkers = foreingWorkers.Where(s => s.IsStandardMoneyCollectionOpen != true).ToList();
                }
                if (cbStandardMoneyCollectionType.SelectedItem != null)
                {
                    foreingWorkers = foreingWorkers.Where(s => s.MoneyCollectionTypeID == ((MoneyCollectionType)cbStandardMoneyCollectionType.SelectedItem).MoneyCollectionTypeID).ToList();
                }
                if (cbPolicyStatus.SelectedIndex == 0)
                {
                    foreingWorkers = foreingWorkers.Where(s => s.ForeingWorkersPolicy.EndDate >= DateTime.Today).ToList();
                }
                else if (cbPolicyStatus.SelectedIndex == 1)
                {
                    foreingWorkers = foreingWorkers.Where(s => s.ForeingWorkersPolicy.EndDate < DateTime.Today).ToList();
                }
                if (cbIndustry.SelectedItem != null)
                {
                    foreingWorkers = foreingWorkers.Where(s => s.ForeingWorkersPolicy.ForeingWorkersIndustryID == ((ForeingWorkersIndustry)cbIndustry.SelectedItem).ForeingWorkersIndustryID).ToList();
                }
                if (cbCompany.SelectedItem != null)
                {
                    foreingWorkers = foreingWorkers.Where(s => s.ForeingWorkersPolicy.CompanyID == ((InsuranceCompany)cbCompany.SelectedItem).CompanyID).ToList();
                }
                if (txtSearch.Text != "")
                {
                    string key = txtSearch.Text;
                    foreingWorkers = foreingWorkers.Where(s => s.ForeingWorkersPolicy.PolicyNumber.StartsWith(key) || s.ForeingWorkersPolicy.Client.LastName.StartsWith(key) || s.ForeingWorkersPolicy.Client.FirstName.StartsWith(key) || s.ForeingWorkersPolicy.Client.IdNumber.StartsWith(key)).ToList();
                }
                dgForeingWorkersPolicies.ItemsSource = foreingWorkers;
                lblCount.Content = string.Format("סה''כ רשומות: {0}  סה''כ חוב: {1} ש''ח", foreingWorkers.Count.ToString(), ((decimal)(foreingWorkers.Where(s => s.MoneyCollectionAmount != null).Sum(s => s.MoneyCollectionAmount))).ToString("0.##"));
            }
        }

        private void cbStandardMoneyCollectionStatus_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (standardMoneyCollections.Count > 0)
            {
                StandardMoneyCollectionsBinding();
            }
        }

        private void txtSearch_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (standardMoneyCollections.Count > 0)
            {
                StandardMoneyCollectionsBinding();
            }
        }

        private void btnClear_Click(object sender, RoutedEventArgs e)
        {
            ClearSearchInputs();
        }

        private void btnUpdateStandarMoneyCollection_Click(object sender, RoutedEventArgs e)
        {
            StandardMoneyCollection standardMoneyCollectionToUpdate = null;
            List<StandardMoneyCollectionTracking> trackings = null;
            //object standardMoneyCollectionToSelect = null;
            ListView lv = null;
            if (tbItemElementary.IsSelected)
            {
                if (dgElementary.SelectedItem == null)
                {
                    MessageBox.Show("נא לסמן את הגבייה הרגילה שברצונך לעדכן", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    return;
                }
                lv = dgElementary;
                //standardMoneyCollectionToSelect = dgElementary.SelectedItem;
                standardMoneyCollectionToUpdate = (StandardMoneyCollection)dgElementary.SelectedItem;
                trackings = standardMoneyCollectionLogic.GetTrakcingsByStsndardMoneyCollection(standardMoneyCollectionToUpdate);
            }
            else if (tbItemPersonalAccidents.IsSelected)
            {
                if (dgPersonalAccidentsPolicies.SelectedItem == null)
                {
                    MessageBox.Show("נא לסמן את הגבייה הרגילה שברצונך לעדכן", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    return;
                }
                lv = dgPersonalAccidentsPolicies;
                //standardMoneyCollectionToSelect = dgPersonalAccidentsPolicies.SelectedItem;
                standardMoneyCollectionToUpdate = (StandardMoneyCollection)dgPersonalAccidentsPolicies.SelectedItem;
                trackings = standardMoneyCollectionLogic.GetTrakcingsByStsndardMoneyCollection(standardMoneyCollectionToUpdate);
            }
            else if (tbItemTravel.IsSelected)
            {
                if (dgTravelPolicies.SelectedItem == null)
                {
                    MessageBox.Show("נא לסמן את הגבייה הרגילה שברצונך לעדכן", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    return;
                }
                lv = dgTravelPolicies;
                //standardMoneyCollectionToSelect = dgTravelPolicies.SelectedItem;
                standardMoneyCollectionToUpdate = (StandardMoneyCollection)dgTravelPolicies.SelectedItem;
                trackings = standardMoneyCollectionLogic.GetTrakcingsByStsndardMoneyCollection(standardMoneyCollectionToUpdate);
            }
            else if (tbItemLife.IsSelected)
            {
                if (dgLife.SelectedItem == null)
                {
                    MessageBox.Show("נא לסמן את הגבייה הרגילה שברצונך לעדכן", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    return;
                }
                lv = dgLife;
                //standardMoneyCollectionToSelect = dgLife.SelectedItem;
                standardMoneyCollectionToUpdate = (StandardMoneyCollection)dgLife.SelectedItem;
                trackings = standardMoneyCollectionLogic.GetTrakcingsByStsndardMoneyCollection(standardMoneyCollectionToUpdate);
            }
            else if (tbItemHealth.IsSelected)
            {
                if (dgHealth.SelectedItem == null)
                {
                    MessageBox.Show("נא לסמן את הגבייה הרגילה שברצונך לעדכן", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    return;
                }
                lv = dgHealth;
                //standardMoneyCollectionToSelect = dgHealth.SelectedItem;
                standardMoneyCollectionToUpdate = (StandardMoneyCollection)dgHealth.SelectedItem;
                trackings = standardMoneyCollectionLogic.GetTrakcingsByStsndardMoneyCollection(standardMoneyCollectionToUpdate);
            }
            else if (tbItemFinance.IsSelected)
            {
                if (dgFinance.SelectedItem == null)
                {
                    MessageBox.Show("נא לסמן את הגבייה הרגילה שברצונך לעדכן", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    return;
                }
                lv = dgFinance;
                //standardMoneyCollectionToSelect = dgFinance.SelectedItem;
                standardMoneyCollectionToUpdate = (StandardMoneyCollection)dgFinance.SelectedItem;
                trackings = standardMoneyCollectionLogic.GetTrakcingsByStsndardMoneyCollection(standardMoneyCollectionToUpdate);
            }
            else if (tbItemForeingWorkers.IsSelected)
            {
                if (dgForeingWorkersPolicies.SelectedItem == null)
                {
                    MessageBox.Show("נא לסמן את הגבייה הרגילה שברצונך לעדכן", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    return;
                }
                lv = dgForeingWorkersPolicies;
                //standardMoneyCollectionToSelect = dgForeingWorkersPolicies.SelectedItem;
                standardMoneyCollectionToUpdate = (StandardMoneyCollection)dgForeingWorkersPolicies.SelectedItem;
                trackings = standardMoneyCollectionLogic.GetTrakcingsByStsndardMoneyCollection(standardMoneyCollectionToUpdate);
            }
            frmStandardMoneyCollectionDetails detailsWindow = new frmStandardMoneyCollectionDetails(standardMoneyCollectionToUpdate, trackings, true, user);
            detailsWindow.ShowDialog();
            GetStandardMoneyCollections();
            SelectStandardMoneyCollectionInDg(standardMoneyCollectionToUpdate, lv);
        }

        private void dgElementary_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            DependencyObject src = (DependencyObject)(e.OriginalSource);
            while (!(src is Control))
            {
                if (!(src is Visual))
                {
                    break;
                }
                src = VisualTreeHelper.GetParent(src);
            }

            if ((src is Control && src.GetType() == typeof(ListViewItem)) || src.GetType() == typeof(Run))
            {
                btnUpdateStandarMoneyCollection_Click(sender, new RoutedEventArgs());
            }
        }

        private void btnUpdatePolicy_Click(object sender, RoutedEventArgs e)
        {
            ListView lv = null;
            object standardMoneyCollectionToSelect = null;
            if (tbItemElementary.IsSelected)
            {
                if (dgElementary.SelectedItem == null)
                {
                    MessageBox.Show("נא לסמן את הפוליסה שברצונך לעדכן", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    return;
                }
                lv = dgElementary;
                standardMoneyCollectionToSelect = dgElementary.SelectedItem;
                var policy = elementaryLogic.GetElementaryPolicyByPolicyId(((StandardMoneyCollection)dgElementary.SelectedItem).ElementaryPolicy.ElementaryPolicyID);
                frmElementary elementaryWindow = new frmElementary(policy, user, false, policy.Client);
                elementaryWindow.ShowDialog();
            }
            else if (tbItemPersonalAccidents.IsSelected)
            {
                if (dgPersonalAccidentsPolicies.SelectedItem == null)
                {
                    MessageBox.Show("נא לסמן את הפוליסה שברצונך לעדכן", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    return;
                }
                lv = dgPersonalAccidentsPolicies;
                standardMoneyCollectionToSelect = dgPersonalAccidentsPolicies.SelectedItem;
                var policy = personalAccidentsLogic.GetPersonalAccidentsPolicyByPolicyId(((StandardMoneyCollection)dgPersonalAccidentsPolicies.SelectedItem).PersonalAccidentsPolicy.PersonalAccidentsPolicyID);
                frmPersonalAccidents personalAccidentsWindow = new frmPersonalAccidents(policy.Client, user, false, policy);
                personalAccidentsWindow.ShowDialog();
            }
            else if (tbItemTravel.IsSelected)
            {
                if (dgTravelPolicies.SelectedItem == null)
                {
                    MessageBox.Show("נא לסמן את הפוליסה שברצונך לעדכן", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    return;
                }
                lv = dgTravelPolicies;
                standardMoneyCollectionToSelect = dgTravelPolicies.SelectedItem;
                var policy = travelLogic.GetTravelPolicyByPolicyId(((StandardMoneyCollection)dgTravelPolicies.SelectedItem).TravelPolicy.TravelPolicyID);
                frmTravel travelWindow = new frmTravel(policy.Client, user, false, policy);
                travelWindow.ShowDialog();
            }
            else if (tbItemLife.IsSelected)
            {
                if (dgLife.SelectedItem == null)
                {
                    MessageBox.Show("נא לסמן את הפוליסה שברצונך לעדכן", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    return;
                }
                lv = dgLife;
                standardMoneyCollectionToSelect = dgLife.SelectedItem;
                var policy = lifeLogic.GetLifePolicyByPolicyId(((StandardMoneyCollection)dgLife.SelectedItem).LifePolicy.LifePolicyID);
                frmLifePolicy lifeWindow = new frmLifePolicy(policy.Client, user, false, policy);
                lifeWindow.ShowDialog();
            }
            else if (tbItemHealth.IsSelected)
            {
                if (dgHealth.SelectedItem == null)
                {
                    MessageBox.Show("נא לסמן את הפוליסה שברצונך לעדכן", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    return;
                }
                lv = dgHealth;
                standardMoneyCollectionToSelect = dgHealth.SelectedItem;
                var policy = healthLogic.GetHealthPolicyByPolicyId(((StandardMoneyCollection)dgHealth.SelectedItem).HealthPolicy.HealthPolicyID);
                frmHealthPolicy healthWindow = new frmHealthPolicy(policy.Client, user, false, policy);
                healthWindow.ShowDialog();
            }
            else if (tbItemFinance.IsSelected)
            {
                if (dgFinance.SelectedItem == null)
                {
                    MessageBox.Show("נא לסמן את הפוליסה שברצונך לעדכן", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    return;
                }
                lv = dgFinance;
                standardMoneyCollectionToSelect = dgFinance.SelectedItem;
                var policy = financeLogic.GetFinancePolicyByPolicyId(((StandardMoneyCollection)dgFinance.SelectedItem).FinancePolicy.FinancePolicyID);
                frmFinance financeWindow = new frmFinance(policy.Client, user, false, policy);
                financeWindow.ShowDialog();
            }
            else if (tbItemForeingWorkers.IsSelected)
            {
                if (dgForeingWorkersPolicies.SelectedItem == null)
                {
                    MessageBox.Show("נא לסמן את הפוליסה שברצונך לעדכן", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    return;
                }
                lv = dgForeingWorkersPolicies;
                standardMoneyCollectionToSelect = dgForeingWorkersPolicies.SelectedItem;
                var policy = foreingWorkersLogic.GetForeingWorkersPolicyByPolicyId(((StandardMoneyCollection)dgForeingWorkersPolicies.SelectedItem).ForeingWorkersPolicy.ForeingWorkersPolicyID);
                frmForeignWorkers foreingWorkersWindow = new frmForeignWorkers(policy.Client, user, false, policy);
                foreingWorkersWindow.ShowDialog();
            }
            GetStandardMoneyCollections();
            SelectStandardMoneyCollectionInDg(standardMoneyCollectionToSelect, lv);
        }

        private void btnClientDetails_Click(object sender, RoutedEventArgs e)
        {
            ListView lv = null;
            object standardMoneyCollectionToSelect = null;
            Client client = null;
            if (tbItemElementary.IsSelected)
            {
                if (dgElementary.SelectedItem == null)
                {
                    MessageBox.Show("נא לסמן גבייה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                }
                else
                {
                    lv = dgElementary;
                    standardMoneyCollectionToSelect = dgElementary.SelectedItem;
                    client = ((StandardMoneyCollection)dgElementary.SelectedItem).ElementaryPolicy.Client;
                }
            }
            else if (tbItemPersonalAccidents.IsSelected)
            {
                if (dgPersonalAccidentsPolicies.SelectedItem == null)
                {
                    MessageBox.Show("נא לסמן גבייה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                }
                else
                {
                    lv = dgPersonalAccidentsPolicies;
                    standardMoneyCollectionToSelect = dgPersonalAccidentsPolicies.SelectedItem;
                    client = ((StandardMoneyCollection)dgPersonalAccidentsPolicies.SelectedItem).PersonalAccidentsPolicy.Client;
                }
            }
            else if (tbItemTravel.IsSelected)
            {
                if (dgTravelPolicies.SelectedItem == null)
                {
                    MessageBox.Show("נא לסמן גבייה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                }
                else
                {
                    lv = dgTravelPolicies;
                    standardMoneyCollectionToSelect = dgTravelPolicies.SelectedItem;
                    client = ((StandardMoneyCollection)dgTravelPolicies.SelectedItem).TravelPolicy.Client;
                }
            }
            else if (tbItemLife.IsSelected)
            {
                if (dgLife.SelectedItem == null)
                {
                    MessageBox.Show("נא לסמן גבייה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                }
                else
                {
                    lv = dgLife;
                    standardMoneyCollectionToSelect = dgLife.SelectedItem;
                    client = ((StandardMoneyCollection)dgLife.SelectedItem).LifePolicy.Client;
                }
            }
            else if (tbItemHealth.IsSelected)
            {
                if (dgHealth.SelectedItem == null)
                {
                    MessageBox.Show("נא לסמן גבייה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                }
                else
                {
                    lv = dgHealth;
                    standardMoneyCollectionToSelect = dgHealth.SelectedItem;
                    client = ((StandardMoneyCollection)dgHealth.SelectedItem).HealthPolicy.Client;
                }
            }
            else if (tbItemFinance.IsSelected)
            {
                if (dgFinance.SelectedItem == null)
                {
                    MessageBox.Show("נא לסמן גבייה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                }
                else
                {
                    lv = dgFinance;
                    standardMoneyCollectionToSelect = dgFinance.SelectedItem;
                    client = ((StandardMoneyCollection)dgFinance.SelectedItem).FinancePolicy.Client;
                }
            }
            else if (tbItemForeingWorkers.IsSelected)
            {
                if (dgForeingWorkersPolicies.SelectedItem == null)
                {
                    MessageBox.Show("נא לסמן גבייה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                }
                else
                {
                    lv = dgForeingWorkersPolicies;
                    standardMoneyCollectionToSelect = dgForeingWorkersPolicies.SelectedItem;
                    client = ((StandardMoneyCollection)dgForeingWorkersPolicies.SelectedItem).ForeingWorkersPolicy.Client;
                }
            }
            if (client != null)
            {
                frmAddClient clientDetailsWindow = new frmAddClient(client, user);
                clientDetailsWindow.ShowDialog();
                GetStandardMoneyCollections();
                SelectStandardMoneyCollectionInDg(standardMoneyCollectionToSelect, lv);
            }

        }

        private void btnOpticArchive_Click(object sender, RoutedEventArgs e)
        {
            Client client = null;
            object policy = null;
            if (tbItemElementary.IsSelected)
            {
                if (dgElementary.SelectedItem == null)
                {
                    MessageBox.Show("נא לסמן גבייה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                }
                else
                {
                    policy = ((StandardMoneyCollection)dgElementary.SelectedItem).ElementaryPolicy;
                    client = ((StandardMoneyCollection)dgElementary.SelectedItem).ElementaryPolicy.Client;
                }
            }
            else if (tbItemPersonalAccidents.IsSelected)
            {
                if (dgPersonalAccidentsPolicies.SelectedItem == null)
                {
                    MessageBox.Show("נא לסמן גבייה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                }
                else
                {
                    policy = ((StandardMoneyCollection)dgPersonalAccidentsPolicies.SelectedItem).PersonalAccidentsPolicy;
                    client = ((StandardMoneyCollection)dgPersonalAccidentsPolicies.SelectedItem).PersonalAccidentsPolicy.Client;
                }
            }
            else if (tbItemTravel.IsSelected)
            {
                if (dgTravelPolicies.SelectedItem == null)
                {
                    MessageBox.Show("נא לסמן גבייה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                }
                else
                {
                    policy = ((StandardMoneyCollection)dgTravelPolicies.SelectedItem).TravelPolicy;
                    client = ((StandardMoneyCollection)dgTravelPolicies.SelectedItem).TravelPolicy.Client;
                }
            }
            else if (tbItemLife.IsSelected)
            {
                if (dgLife.SelectedItem == null)
                {
                    MessageBox.Show("נא לסמן גבייה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                }
                else
                {
                    policy = ((StandardMoneyCollection)dgLife.SelectedItem).LifePolicy;
                    client = ((StandardMoneyCollection)dgLife.SelectedItem).LifePolicy.Client;
                }
            }
            else if (tbItemHealth.IsSelected)
            {
                if (dgHealth.SelectedItem == null)
                {
                    MessageBox.Show("נא לסמן גבייה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                }
                else
                {
                    policy = ((StandardMoneyCollection)dgHealth.SelectedItem).HealthPolicy;
                    client = ((StandardMoneyCollection)dgHealth.SelectedItem).HealthPolicy.Client;
                }
            }
            else if (tbItemFinance.IsSelected)
            {
                if (dgFinance.SelectedItem == null)
                {
                    MessageBox.Show("נא לסמן גבייה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                }
                else
                {
                    policy = ((StandardMoneyCollection)dgFinance.SelectedItem).FinancePolicy;
                    client = ((StandardMoneyCollection)dgFinance.SelectedItem).FinancePolicy.Client;
                }
            }
            else if (tbItemForeingWorkers.IsSelected)
            {
                if (dgForeingWorkersPolicies.SelectedItem == null)
                {
                    MessageBox.Show("נא לסמן גבייה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                }
                else
                {
                    policy = ((StandardMoneyCollection)dgForeingWorkersPolicies.SelectedItem).ForeingWorkersPolicy;
                    client = ((StandardMoneyCollection)dgForeingWorkersPolicies.SelectedItem).ForeingWorkersPolicy.Client;
                }
            }
            if (client != null && policy != null)
            {
                frmScan2 clientArch = new frmScan2(client, policy, "", user);
                clientArch.ShowDialog();
            }
        }

        private void btnSendEmail_Click(object sender, RoutedEventArgs e)
        {
            Client client = null;
            if (tbItemElementary.IsSelected)
            {
                if (dgElementary.SelectedItem == null)
                {
                    MessageBox.Show("נא לסמן גבייה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    return;
                }
                else
                {
                    client = ((StandardMoneyCollection)dgElementary.SelectedItem).ElementaryPolicy.Client;
                }
            }
            else if (tbItemPersonalAccidents.IsSelected)
            {
                if (dgPersonalAccidentsPolicies.SelectedItem == null)
                {
                    MessageBox.Show("נא לסמן גבייה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    return;

                }
                else
                {
                    client = ((StandardMoneyCollection)dgPersonalAccidentsPolicies.SelectedItem).PersonalAccidentsPolicy.Client;
                }
            }
            else if (tbItemTravel.IsSelected)
            {
                if (dgTravelPolicies.SelectedItem == null)
                {
                    MessageBox.Show("נא לסמן גבייה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    return;

                }
                else
                {
                    client = ((StandardMoneyCollection)dgTravelPolicies.SelectedItem).TravelPolicy.Client;
                }
            }
            else if (tbItemLife.IsSelected)
            {
                if (dgLife.SelectedItem == null)
                {
                    MessageBox.Show("נא לסמן גבייה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    return;

                }
                else
                {
                    client = ((StandardMoneyCollection)dgLife.SelectedItem).LifePolicy.Client;
                }
            }
            else if (tbItemHealth.IsSelected)
            {
                if (dgHealth.SelectedItem == null)
                {
                    MessageBox.Show("נא לסמן גבייה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    return;

                }
                else
                {
                    client = ((StandardMoneyCollection)dgHealth.SelectedItem).HealthPolicy.Client;
                }
            }
            else if (tbItemFinance.IsSelected)
            {
                if (dgFinance.SelectedItem == null)
                {
                    MessageBox.Show("נא לסמן גבייה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    return;

                }
                else
                {
                    client = ((StandardMoneyCollection)dgFinance.SelectedItem).FinancePolicy.Client;
                }
            }
            else if (tbItemForeingWorkers.IsSelected)
            {
                if (dgForeingWorkersPolicies.SelectedItem == null)
                {
                    MessageBox.Show("נא לסמן גבייה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    return;

                }
                else
                {
                    client = ((StandardMoneyCollection)dgForeingWorkersPolicies.SelectedItem).ForeingWorkersPolicy.Client;
                }
            }
            if (client == null)
            {
                MessageBox.Show("לא ניתן לשלוח דוא''ל ללקוח", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            if (client.Email == "")
            {
                MessageBox.Show("אין ללקוח כתובת דוא''ל במערכת", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            string[] address = new string[] { client.Email };
            try
            {
                email.SendEmailFromOutlook("", "", null, address);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void dgElementary_MouseDown(object sender, MouseButtonEventArgs e)
        {
            dgElementary.UnselectAll();
        }

        private void dgPersonalAccidentsPolicies_MouseDown(object sender, MouseButtonEventArgs e)
        {
            dgPersonalAccidentsPolicies.UnselectAll();
        }

        private void dgTravelPolicies_MouseDown(object sender, MouseButtonEventArgs e)
        {
            dgTravelPolicies.UnselectAll();
        }

        private void dgLife_MouseDown(object sender, MouseButtonEventArgs e)
        {
            dgLife.UnselectAll();
        }

        private void dgHealth_MouseDown(object sender, MouseButtonEventArgs e)
        {
            dgHealth.UnselectAll();
        }

        private void dgForeingWorkersPolicies_MouseDown(object sender, MouseButtonEventArgs e)
        {
            dgForeingWorkersPolicies.UnselectAll();
        }

        private void dgFinance_MouseDown(object sender, MouseButtonEventArgs e)
        {
            dgFinance.UnselectAll();
        }

        private void mItemExcel_Click(object sender, RoutedEventArgs e)
        {
            if (tbItemElementary.IsSelected)
            {
                excelLogic.StandardMoneyCollectionToExcel((List<StandardMoneyCollection>)dgElementary.ItemsSource);
            }
            else if (tbItemPersonalAccidents.IsSelected)
            {
                excelLogic.StandardMoneyCollectionToExcel((List<StandardMoneyCollection>)dgPersonalAccidentsPolicies.ItemsSource);
            }
            else if (tbItemTravel.IsSelected)
            {
                excelLogic.StandardMoneyCollectionToExcel((List<StandardMoneyCollection>)dgTravelPolicies.ItemsSource);
            }
            else if (tbItemLife.IsSelected)
            {
                excelLogic.StandardMoneyCollectionToExcel((List<StandardMoneyCollection>)dgLife.ItemsSource);
            }
            else if (tbItemHealth.IsSelected)
            {
                excelLogic.StandardMoneyCollectionToExcel((List<StandardMoneyCollection>)dgHealth.ItemsSource);
            }
            else if (tbItemFinance.IsSelected)
            {
                excelLogic.StandardMoneyCollectionToExcel((List<StandardMoneyCollection>)dgFinance.ItemsSource);
            }
            else if (tbItemForeingWorkers.IsSelected)
            {
                excelLogic.StandardMoneyCollectionToExcel((List<StandardMoneyCollection>)dgForeingWorkersPolicies.ItemsSource);
            }
        }
        private void SelectStandardMoneyCollectionInDg(object standardMoneyCollectionToSelect, ListView lv)
        {
            var dgItems = lv.Items;
            foreach (var item in dgItems)
            {
                if (((StandardMoneyCollection)item).StandardMoneyCollectionID == ((StandardMoneyCollection)standardMoneyCollectionToSelect).StandardMoneyCollectionID)
                {
                    lv.SelectedItem = item;
                    lv.ScrollIntoView(item);
                    ListViewItem listViewItem = lv.ItemContainerGenerator.ContainerFromItem(lv.SelectedItem) as ListViewItem;
                    if (listViewItem != null)
                    {
                        listViewItem.Focus();
                    }
                    break;
                }
                //if (standardMoneyCollectionToSelect is ElementaryPolicy)
                //{
                //    ElementaryPolicy policy = (ElementaryPolicy)item;
                //    if (policy.ElementaryPolicyID == ((ElementaryPolicy)standardMoneyCollectionToSelect).ElementaryPolicyID)
                //    {
                //        lv.SelectedItem = item;
                //        lv.ScrollIntoView(item);
                //        ListViewItem listViewItem = lv.ItemContainerGenerator.ContainerFromItem(lv.SelectedItem) as ListViewItem;
                //        if (listViewItem != null)
                //        {
                //            listViewItem.Focus();
                //        }
                //        break;
                //    }
                //}
                //else if (standardMoneyCollectionToSelect is ForeingWorkersPolicy)
                //{
                //    ForeingWorkersPolicy policy = (ForeingWorkersPolicy)item;
                //    if (policy.ForeingWorkersPolicyID == ((ForeingWorkersPolicy)standardMoneyCollectionToSelect).ForeingWorkersPolicyID)
                //    {
                //        lv.SelectedItem = item;
                //        lv.ScrollIntoView(item);
                //        ListViewItem listViewItem = lv.ItemContainerGenerator.ContainerFromItem(lv.SelectedItem) as ListViewItem;
                //        if (listViewItem != null)
                //        {
                //            listViewItem.Focus();
                //        }
                //        break;
                //    }
                //}
                //else if (standardMoneyCollectionToSelect is LifePolicy)
                //{
                //    LifePolicy policy = (LifePolicy)item;
                //    if (policy.LifePolicyID == ((LifePolicy)standardMoneyCollectionToSelect).LifePolicyID)
                //    {
                //        lv.SelectedItem = item;
                //        lv.ScrollIntoView(item);
                //        ListViewItem listViewItem = lv.ItemContainerGenerator.ContainerFromItem(lv.SelectedItem) as ListViewItem;
                //        if (listViewItem != null)
                //        {
                //            listViewItem.Focus();
                //        }
                //        break;
                //    }
                //}
                //else if (standardMoneyCollectionToSelect is HealthPolicy)
                //{
                //    HealthPolicy policy = (HealthPolicy)item;
                //    if (policy.HealthPolicyID == ((HealthPolicy)standardMoneyCollectionToSelect).HealthPolicyID)
                //    {
                //        lv.SelectedItem = item;
                //        lv.ScrollIntoView(item);
                //        ListViewItem listViewItem = lv.ItemContainerGenerator.ContainerFromItem(lv.SelectedItem) as ListViewItem;
                //        if (listViewItem != null)
                //        {
                //            listViewItem.Focus();
                //        }
                //        break;
                //    }
                //}
                //else if (standardMoneyCollectionToSelect is TravelPolicy)
                //{
                //    TravelPolicy policy = (TravelPolicy)item;
                //    if (policy.TravelPolicyID == ((TravelPolicy)standardMoneyCollectionToSelect).TravelPolicyID)
                //    {
                //        lv.SelectedItem = item;
                //        lv.ScrollIntoView(item);
                //        ListViewItem listViewItem = lv.ItemContainerGenerator.ContainerFromItem(lv.SelectedItem) as ListViewItem;
                //        if (listViewItem != null)
                //        {
                //            listViewItem.Focus();
                //        }
                //        break;
                //    }
                //}
                //else if (standardMoneyCollectionToSelect is PersonalAccidentsPolicy)
                //{
                //    PersonalAccidentsPolicy policy = (PersonalAccidentsPolicy)item;
                //    if (policy.PersonalAccidentsPolicyID == ((PersonalAccidentsPolicy)standardMoneyCollectionToSelect).PersonalAccidentsPolicyID)
                //    {
                //        lv.SelectedItem = item;
                //        lv.ScrollIntoView(item);
                //        ListViewItem listViewItem = lv.ItemContainerGenerator.ContainerFromItem(lv.SelectedItem) as ListViewItem;
                //        if (listViewItem != null)
                //        {
                //            listViewItem.Focus();
                //        }
                //        break;
                //    }
                //}
                //else if (standardMoneyCollectionToSelect is FinancePolicy)
                //{
                //    FinancePolicy policy = (FinancePolicy)item;
                //    if (policy.FinancePolicyID == ((FinancePolicy)standardMoneyCollectionToSelect).FinancePolicyID)
                //    {
                //        lv.SelectedItem = item;
                //        lv.ScrollIntoView(item);
                //        ListViewItem listViewItem = lv.ItemContainerGenerator.ContainerFromItem(lv.SelectedItem) as ListViewItem;
                //        if (listViewItem != null)
                //        {
                //            listViewItem.Focus();
                //        }
                //        break;
                //    }
                //}
            }
        }

        private void mItemPrintElementary_Click(object sender, RoutedEventArgs e)
        {
            List<StandardMoneyCollectionReport> reportList = null;
            if (tbItemElementary.IsSelected)
            {
                reportList = ConvertToReportView((List<StandardMoneyCollection>)dgElementary.ItemsSource);
            }
            else if (tbItemFinance.IsSelected)
            {
                reportList = ConvertToReportView((List<StandardMoneyCollection>)dgFinance.ItemsSource);
            }
            else if (tbItemForeingWorkers.IsSelected)
            {
                reportList = ConvertToReportView((List<StandardMoneyCollection>)dgForeingWorkersPolicies.ItemsSource);
            }
            else if (tbItemHealth.IsSelected)
            {
                reportList = ConvertToReportView((List<StandardMoneyCollection>)dgHealth.ItemsSource);
            }
            else if (tbItemLife.IsSelected)
            {
                reportList = ConvertToReportView((List<StandardMoneyCollection>)dgLife.ItemsSource);
            }
            else if (tbItemPersonalAccidents.IsSelected)
            {
                reportList = ConvertToReportView((List<StandardMoneyCollection>)dgPersonalAccidentsPolicies.ItemsSource);
            }
            else if (tbItemTravel.IsSelected)
            {
                reportList = ConvertToReportView((List<StandardMoneyCollection>)dgTravelPolicies.ItemsSource);
            }
            if (reportList != null)
            {
                try
                {
                    frmPrintPreview preview = new frmPrintPreview("StandardMoneyCollectionReport", reportList);
                    preview.ShowDialog();

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private List<StandardMoneyCollectionReport> ConvertToReportView(List<StandardMoneyCollection> list)
        {
            List<StandardMoneyCollectionReport> reportList = new List<StandardMoneyCollectionReport>();
            foreach (var item in list)
            {
                StandardMoneyCollectionReport reportItem = new StandardMoneyCollectionReport();
                if (item.ElementaryPolicy != null)
                {
                    if (item.ElementaryPolicy.Client != null)
                    {
                        reportItem.ClientName = item.ElementaryPolicy.Client.LastName + " " + item.ElementaryPolicy.Client.FirstName;
                        reportItem.Cellphone = item.ElementaryPolicy.Client.CellPhone;
                        reportItem.Phone = item.ElementaryPolicy.Client.PhoneHome;
                    }
                    if (item.ElementaryPolicy.Company != null)
                    {
                        reportItem.CompanyName = item.ElementaryPolicy.Company.CompanyName;
                    }
                    if (item.ElementaryPolicy.EndDate != null)
                    {
                        reportItem.EndDate = ((DateTime)item.ElementaryPolicy.EndDate).ToString("dd/MM/yyyy");
                    }
                    if (item.ElementaryPolicy.StartDate != null)
                    {
                        reportItem.StartDate = ((DateTime)item.ElementaryPolicy.StartDate).ToString("dd/MM/yyyy");
                    }
                    if (item.ElementaryPolicy.TotalPremium != null)
                    {
                        reportItem.Premium = ((decimal)item.ElementaryPolicy.TotalPremium).ToString("0.##");
                    }
                    if (item.ElementaryPolicy.InsuranceIndustry != null)
                    {
                        reportItem.IndustryName = item.ElementaryPolicy.InsuranceIndustry.InsuranceIndustryName;
                    }
                    reportItem.PolicyNumber = item.ElementaryPolicy.PolicyNumber;
                    reportItem.Header = string.Format(@"דו''ח גבייה רגילה {0}{1}", Environment.NewLine, lblCount.Content);

                }
                else if (item.LifePolicy != null)
                {
                    if (item.LifePolicy.Client != null)
                    {
                        reportItem.ClientName = item.LifePolicy.Client.LastName + " " + item.LifePolicy.Client.FirstName;
                        reportItem.Cellphone = item.LifePolicy.Client.CellPhone;
                        reportItem.Phone = item.LifePolicy.Client.PhoneHome;
                    }
                    if (item.LifePolicy.Company != null)
                    {
                        reportItem.CompanyName = item.LifePolicy.Company.CompanyName;
                    }
                    if (item.LifePolicy.EndDate != null)
                    {
                        reportItem.EndDate = ((DateTime)item.LifePolicy.EndDate).ToString("dd/MM/yyyy");
                    }
                    if (item.LifePolicy.StartDate != null)
                    {
                        reportItem.StartDate = ((DateTime)item.LifePolicy.StartDate).ToString("dd/MM/yyyy");
                    }
                    if (item.LifePolicy.TotalMonthlyPayment != null)
                    {
                        reportItem.Premium = ((decimal)item.LifePolicy.TotalMonthlyPayment).ToString("0.##");
                    }
                    if (item.LifePolicy.LifeIndustry != null)
                    {
                        reportItem.IndustryName = item.LifePolicy.LifeIndustry.LifeIndustryName;
                    }
                    reportItem.PolicyNumber = item.LifePolicy.PolicyNumber;
                    reportItem.Header = string.Format(@"דו''ח גבייה רגילה {0}{1}", Environment.NewLine, lblCount.Content);

                }
                else if (item.HealthPolicy != null)
                {
                    if (item.HealthPolicy.Client != null)
                    {
                        reportItem.ClientName = item.HealthPolicy.Client.LastName + " " + item.HealthPolicy.Client.FirstName;
                        reportItem.Cellphone = item.HealthPolicy.Client.CellPhone;
                        reportItem.Phone = item.HealthPolicy.Client.PhoneHome;
                    }
                    if (item.HealthPolicy.Company != null)
                    {
                        reportItem.CompanyName = item.HealthPolicy.Company.CompanyName;
                    }
                    if (item.HealthPolicy.EndDate != null)
                    {
                        reportItem.EndDate = ((DateTime)item.HealthPolicy.EndDate).ToString("dd/MM/yyyy");
                    }
                    if (item.HealthPolicy.StartDate != null)
                    {
                        reportItem.StartDate = ((DateTime)item.HealthPolicy.StartDate).ToString("dd/MM/yyyy");
                    }
                    if (item.HealthPolicy.TotalMonthlyPayment != null)
                    {
                        reportItem.Premium = ((decimal)item.HealthPolicy.TotalMonthlyPayment).ToString("0.##");
                    }
                    reportItem.PolicyNumber = item.HealthPolicy.PolicyNumber;
                    reportItem.Header = string.Format(@"דו''ח גבייה רגילה {0}{1}", Environment.NewLine, lblCount.Content);

                }
                else if (item.TravelPolicy != null)
                {
                    if (item.TravelPolicy.Client != null)
                    {
                        reportItem.ClientName = item.TravelPolicy.Client.LastName + " " + item.TravelPolicy.Client.FirstName;
                        reportItem.Cellphone = item.TravelPolicy.Client.CellPhone;
                        reportItem.Phone = item.TravelPolicy.Client.PhoneHome;
                    }
                    if (item.TravelPolicy.Company != null)
                    {
                        reportItem.CompanyName = item.TravelPolicy.Company.CompanyName;
                    }
                    if (item.TravelPolicy.EndDate != null)
                    {
                        reportItem.EndDate = ((DateTime)item.TravelPolicy.EndDate).ToString("dd/MM/yyyy");
                    }
                    if (item.TravelPolicy.StartDate != null)
                    {
                        reportItem.StartDate = ((DateTime)item.TravelPolicy.StartDate).ToString("dd/MM/yyyy");
                    }
                    if (item.TravelPolicy.Premium != null)
                    {
                        reportItem.Premium = ((decimal)item.TravelPolicy.Premium).ToString("0.##");
                    }
                    reportItem.PolicyNumber = item.TravelPolicy.PolicyNumber;
                    reportItem.Header = string.Format(@"דו''ח גבייה רגילה {0}{1}", Environment.NewLine, lblCount.Content);

                }
                else if (item.PersonalAccidentsPolicy != null)
                {
                    if (item.PersonalAccidentsPolicy.Client != null)
                    {
                        reportItem.ClientName = item.PersonalAccidentsPolicy.Client.LastName + " " + item.PersonalAccidentsPolicy.Client.FirstName;
                        reportItem.Cellphone = item.PersonalAccidentsPolicy.Client.CellPhone;
                        reportItem.Phone = item.PersonalAccidentsPolicy.Client.PhoneHome;
                    }
                    if (item.PersonalAccidentsPolicy.Company != null)
                    {
                        reportItem.CompanyName = item.PersonalAccidentsPolicy.Company.CompanyName;
                    }
                    if (item.PersonalAccidentsPolicy.EndDate != null)
                    {
                        reportItem.EndDate = ((DateTime)item.PersonalAccidentsPolicy.EndDate).ToString("dd/MM/yyyy");
                    }
                    if (item.PersonalAccidentsPolicy.StartDate != null)
                    {
                        reportItem.StartDate = ((DateTime)item.PersonalAccidentsPolicy.StartDate).ToString("dd/MM/yyyy");
                    }
                    if (item.PersonalAccidentsPolicy.TotalPremium != null)
                    {
                        reportItem.Premium = ((decimal)item.PersonalAccidentsPolicy.TotalPremium).ToString("0.##");
                    }
                    reportItem.PolicyNumber = item.PersonalAccidentsPolicy.PolicyNumber;
                    reportItem.Header = string.Format(@"דו''ח גבייה רגילה {0}{1}", Environment.NewLine, lblCount.Content);

                }
                else if (item.FinancePolicy != null)
                {
                    if (item.FinancePolicy.Client != null)
                    {
                        reportItem.ClientName = item.FinancePolicy.Client.LastName + " " + item.FinancePolicy.Client.FirstName;
                        reportItem.Cellphone = item.FinancePolicy.Client.CellPhone;
                        reportItem.Phone = item.FinancePolicy.Client.PhoneHome;
                    }
                    if (item.FinancePolicy.Company != null)
                    {
                        reportItem.CompanyName = item.FinancePolicy.Company.CompanyName;
                    }
                    if (item.FinancePolicy.EndDate != null)
                    {
                        reportItem.EndDate = ((DateTime)item.FinancePolicy.EndDate).ToString("dd/MM/yyyy");
                    }
                    if (item.FinancePolicy.StartDate != null)
                    {
                        reportItem.StartDate = ((DateTime)item.FinancePolicy.StartDate).ToString("dd/MM/yyyy");
                    }
                    if (item.FinancePolicy.FinanceProgramType != null)
                    {
                        reportItem.IndustryName = item.FinancePolicy.FinanceProgramType.ProgramTypeName;
                    }
                    reportItem.PolicyNumber = item.FinancePolicy.AssociateNumber;
                    reportItem.Header = string.Format(@"דו''ח גבייה רגילה {0}{1}", Environment.NewLine, lblCount.Content);

                }
                else if (item.ForeingWorkersPolicy != null)
                {
                    if (item.ForeingWorkersPolicy.Client != null)
                    {
                        reportItem.ClientName = item.ForeingWorkersPolicy.Client.LastName + " " + item.ForeingWorkersPolicy.Client.FirstName;
                        reportItem.Cellphone = item.ForeingWorkersPolicy.Client.CellPhone;
                        reportItem.Phone = item.ForeingWorkersPolicy.Client.PhoneHome;
                    }
                    if (item.ForeingWorkersPolicy.Company != null)
                    {
                        reportItem.CompanyName = item.ForeingWorkersPolicy.Company.CompanyName;
                    }
                    if (item.ForeingWorkersPolicy.EndDate != null)
                    {
                        reportItem.EndDate = ((DateTime)item.ForeingWorkersPolicy.EndDate).ToString("dd/MM/yyyy");
                    }
                    if (item.ForeingWorkersPolicy.StartDate != null)
                    {
                        reportItem.StartDate = ((DateTime)item.ForeingWorkersPolicy.StartDate).ToString("dd/MM/yyyy");
                    }
                    if (item.ForeingWorkersPolicy.TotalPremium != null)
                    {
                        reportItem.Premium = ((decimal)item.ForeingWorkersPolicy.TotalPremium).ToString("0.##");
                    }
                    if (item.ForeingWorkersPolicy.ForeingWorkersIndustry != null)
                    {
                        reportItem.IndustryName = item.ForeingWorkersPolicy.ForeingWorkersIndustry.ForeingWorkersIndustryName;
                    }
                    reportItem.PolicyNumber = item.ForeingWorkersPolicy.PolicyNumber;
                    reportItem.Header = string.Format(@"דו''ח גבייה רגילה {0}{1}", Environment.NewLine, lblCount.Content);

                }

                if (item.MoneyCollectionAmount != null)
                {
                    reportItem.MoneyCollectionAmount = ((decimal)item.MoneyCollectionAmount).ToString("0.##");
                }
                if (item.MoneyCollectionType != null)
                {
                    reportItem.MoneyCollectionType = item.MoneyCollectionType.MoneyCollectionTypeName;
                }
                reportList.Add(reportItem);
            }
            return reportList;
        }
    }
}
