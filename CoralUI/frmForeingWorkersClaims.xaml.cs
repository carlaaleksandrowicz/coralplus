﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using CoralBusinessLogics;
using System.IO;

namespace CoralUI
{
    /// <summary>
    /// Interaction logic for frmForeingWorkersClaims.xaml
    /// </summary>
    public partial class frmForeingWorkersClaims : Window
    {
        Client client = null;
        ForeingWorkersPolicy policy = null;
        User user;
        ForeingWorkersClaimsLogic claimLogic = new ForeingWorkersClaimsLogic();
        InsurancesLogic insuranceLogic = new InsurancesLogic();
        InputsValidations validations = new InputsValidations();
        List<ForeingWorkersWitness> witnesses = new List<ForeingWorkersWitness>();
        List<ForeingWorkersClaimTracking> trackings = new List<ForeingWorkersClaimTracking>();
        TrackingsLogics trackingLogic = new TrackingsLogics();
        private ForeingWorkersClaim foreingWorkersClaim = null;
        ListViewSettings lv = new ListViewSettings();
        string path = "";
        string newPath = "";
        string clientDirPath = "";
        string[] claimPaths = null;
        List<object> nullErrorList = null;
        string[] foreingWorkersPaths = null;
        private bool isChanges = false;
        bool confirmBeforeClosing = true;
        bool cancelClose = false;

        public frmForeingWorkersClaims(Client policyClient, ForeingWorkersPolicy policyClaim, User userAccount)
        {
            InitializeComponent();
            client = policyClient;
            policy = policyClaim;
            user = userAccount;
        }

        public frmForeingWorkersClaims(ForeingWorkersClaim foreingWorkersClaim, Client client, User user)
        {
            InitializeComponent();
            this.foreingWorkersClaim = foreingWorkersClaim;
            this.client = client;
            this.user = user;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            string strAppDir = System.AppDomain.CurrentDomain.BaseDirectory;
            string path = strAppDir + @"Coral Files\ClientsPath.txt";

            if (File.Exists(path))
            {
                clientDirPath = File.ReadAllText(path);
            }
            else
            {
                MessageBox.Show("לא נמצא תיקיית הלקוחות. נא בדוק בהגדרות ראשוניות את נתיב התיקייה");
                Close();
                return;
            }
            if (foreingWorkersClaim != null)
            {
                foreingWorkersPaths = Directory.GetDirectories(clientDirPath + @"/" + client.ClientID + @"/עובדים זרים", "*" + foreingWorkersClaim.ForeingWorkersPolicy.PolicyNumber+" "+ foreingWorkersClaim.ForeingWorkersPolicy.ForeingWorkersIndustry.ForeingWorkersIndustryName + "*");
            }
            else if (policy != null)
            {
                foreingWorkersPaths = Directory.GetDirectories(clientDirPath + @"/" + client.ClientID + @"/עובדים זרים", "*" + policy.PolicyNumber+" "+policy.ForeingWorkersIndustry.ForeingWorkersIndustryName + "*");
            }
            //if (foreingWorkersPaths.Count() > 0)
            //{
            //    claimPaths = Directory.GetDirectories(foreingWorkersPaths[0], "תביעות*");
            //    if (claimPaths.Count() > 0)
            //    {
            //        path = claimPaths[0];
            //    }
            //}
            cbClaimTypeBinding();
            cbClaimConditionBinding();
            lblNameSpace.Content = client.FirstName + " " + client.LastName;
            lblCellPhoneSpace.Content = client.CellPhone;
            lblEmailSpace.Content = client.Email;
            lblIdSpace.Content = client.IdNumber;
            lblPhoneSpace.Content = client.PhoneHome;            
            if (policy != null)
            {
                //lblInsuranceTypeSpace.Content = policy.ForeingWorkersIndustry.ForeingWorkersIndustryName + " " + policy.FundType.FundTypeName;
                lblPhoneSpace.Content = client.PhoneHome;
                lblStartDateSpace.Content = ((DateTime)policy.StartDate).Date;
                lblCompanySpace.Content = policy.Company.CompanyName;
                dpOpenDate.SelectedDate = DateTime.Now;
                txtPolicyNumber.Text = policy.PolicyNumber;
                if (policy.ClinicName!="")
                {
                    lblClinicSpace.Content = policy.ClinicName;
                }
                else
                {
                    lblClinic.Visibility = Visibility.Hidden;
                }
            }
            else
            {
                //lblInsuranceTypeSpace.Content = foreingWorkersClaim.ForeingWorkersPolicy.ForeingWorkersIndustry.ForeingWorkersIndustryName + " " + foreingWorkersClaim.ForeingWorkersPolicy.FundType.FundTypeName;
                lblStartDateSpace.Content = ((DateTime)foreingWorkersClaim.ForeingWorkersPolicy.StartDate).Date;
                lblCompanySpace.Content = foreingWorkersClaim.ForeingWorkersPolicy.Company.CompanyName;
                dpOpenDate.SelectedDate = (DateTime)foreingWorkersClaim.OpenDate;
                txtPolicyNumber.Text = foreingWorkersClaim.ForeingWorkersPolicy.PolicyNumber;
                //var claimsTypes = cbClaimType.Items;
                //foreach (var item in claimsTypes)
                //{
                //    ForeingWorkersClaimType claimType = (ForeingWorkersClaimType)item;
                //    if (claimType.ForeingWorkersClaimTypeID == foreingWorkersClaim.ForeingWorkersClaimTypeID)
                //    {
                //        cbClaimType.SelectedItem = item;
                //        break;
                //    }
                //}
                if (foreingWorkersClaim.ForeingWorkersClaimTypeID != null)
                {
                    SelectItemInCb((int)foreingWorkersClaim.ForeingWorkersClaimTypeID);
                }
                txtClaimNumber.Text = foreingWorkersClaim.ClaimNumber;
                if (foreingWorkersClaim.ClaimStatus == false)
                {
                    cbStatus.SelectedIndex = 1;
                }
                if (foreingWorkersClaim.ForeingWorkersClaimConditionID != null)
                {
                    SelectConditionItemInCb((int)foreingWorkersClaim.ForeingWorkersClaimConditionID);
                }
                dpDeliveryDateToCompany.SelectedDate = foreingWorkersClaim.DeliveredToCompanyDate;
                dpMoneyReceivedDate.SelectedDate = foreingWorkersClaim.MoneyReceivedDate;
                if (foreingWorkersClaim.AmountReceived!=null)
                {
                    txtAmountClaimed.Text = ((decimal)foreingWorkersClaim.ClaimAmount).ToString("0.##");
                }
                if (foreingWorkersClaim.AmountReceived!=null)
                {
                    txtAmountReceived.Text = ((decimal)foreingWorkersClaim.AmountReceived).ToString("0.##");
                }
                dpEventDate.SelectedDate = foreingWorkersClaim.EventDateAndTime;
                tpEventHour.Value = foreingWorkersClaim.EventDateAndTime;
                txtEventPlace.Text = foreingWorkersClaim.EventPlace;
                txtEventDescription.Text = foreingWorkersClaim.EventDescription;
                witnesses = claimLogic.GetWitnessesByClaim(foreingWorkersClaim.ForeingWorkersClaimID);
                dgWitnessesBinding();
                trackings = trackingLogic.GetAllForeingWorkersClaimTrackingsByClaimID(foreingWorkersClaim.ForeingWorkersClaimID);
                dgTrackingsBinding();
            }

        }


        private void cbClaimTypeBinding()
        {
            cbClaimType.ItemsSource = claimLogic.GetActiveForeingWorkersClaimTypes();
            cbClaimType.DisplayMemberPath = "ClaimTypeName";
        }

        private void cbClaimConditionBinding()
        {
            cbClaimCondition.ItemsSource = claimLogic.GetActiveForeingWorkersClaimConditions();
            cbClaimCondition.DisplayMemberPath = "Description";
        }

        private void btnUpdateClaimTypeTable_Click(object sender, RoutedEventArgs e)
        {
            int? selectedItemId = null;
            if (cbClaimType.SelectedItem != null)
            {
                selectedItemId = ((ForeingWorkersClaimType)cbClaimType.SelectedItem).ForeingWorkersClaimTypeID;
            }
            frmUpdateTable updateClaimTypesTable = new frmUpdateTable(new ForeingWorkersClaimType());
            updateClaimTypesTable.ShowDialog();
            cbClaimTypeBinding();
            if (updateClaimTypesTable.ItemAdded != null)
            {
                SelectItemInCb(((ForeingWorkersClaimType)updateClaimTypesTable.ItemAdded).ForeingWorkersClaimTypeID);
            }
            else if (selectedItemId != null)
            {
                SelectItemInCb((int)selectedItemId);
            }
        }

        private void SelectItemInCb(int id)
        {
            var items = cbClaimType.Items;
            foreach (var item in items)
            {
                ForeingWorkersClaimType parsedItem = (ForeingWorkersClaimType)item;
                if (parsedItem.ForeingWorkersClaimTypeID == id)
                {
                    cbClaimType.SelectedItem = item;
                    break;
                }
            }
        }

        private void SelectConditionItemInCb(int id)
        {
            var items = cbClaimCondition.Items;
            foreach (var item in items)
            {
                ForeingWorkersClaimCondition parsedItem = (ForeingWorkersClaimCondition)item;
                if (parsedItem.ForeingWorkersClaimConditionID == id)
                {
                    cbClaimCondition.SelectedItem = item;
                    break;
                }
            }
        }

        private void btnUpdateClaimConditionTable_Click(object sender, RoutedEventArgs e)
        {
            int? selectedItemId = null;
            if (cbClaimCondition.SelectedItem != null)
            {
                selectedItemId = ((ForeingWorkersClaimCondition)cbClaimCondition.SelectedItem).ForeingWorkersClaimConditionID;
            }
            frmUpdateTable updateClaimConditionsTable = new frmUpdateTable(new ForeingWorkersClaimCondition());
            updateClaimConditionsTable.ShowDialog();
            cbClaimConditionBinding();
            if (updateClaimConditionsTable.ItemAdded != null)
            {
                SelectConditionItemInCb(((ForeingWorkersClaimCondition)updateClaimConditionsTable.ItemAdded).ForeingWorkersClaimConditionID);
            }
            else if (selectedItemId != null)
            {
                SelectConditionItemInCb((int)selectedItemId);
            }

        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            if (nullErrorList != null && nullErrorList.Count > 0)
            {
                foreach (var item in nullErrorList)
                {
                    if (item is TextBox)
                    {
                        ((TextBox)item).ClearValue(BorderBrushProperty);
                    }
                    else if (item is ComboBox)
                    {
                        ((ComboBox)item).ClearValue(BorderBrushProperty);
                    }
                    else if (item is DatePicker)
                    {
                        ((DatePicker)item).ClearValue(BorderBrushProperty);
                    }
                }
            }
            nullErrorList = validations.InputNullValidation(new object[] { dpEventDate });
            if (nullErrorList.Count > 0)
            {
                cancelClose = true;
                tbItemGeneral.Focus();
                MessageBox.Show("נא למלאות שדות חובה", "", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (dpMoneyReceivedDate.SelectedDate != null && dpDeliveryDateToCompany.SelectedDate != null)
            {
                if (dpMoneyReceivedDate.SelectedDate < dpDeliveryDateToCompany.SelectedDate)
                {
                    cancelClose = true;
                    MessageBox.Show("תאריך משלוח חייב להיות קטן מתאריך קבלת הכסף", "", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
            }
            int? claimTypeId = null;
            if (cbClaimType.SelectedItem != null)
            {
                claimTypeId = ((ForeingWorkersClaimType)cbClaimType.SelectedItem).ForeingWorkersClaimTypeID;
            }
            int? claimConditionId = null;
            if (cbClaimCondition.SelectedItem != null)
            {
                claimConditionId = ((ForeingWorkersClaimCondition)cbClaimCondition.SelectedItem).ForeingWorkersClaimConditionID;
            }
            bool status = true;
            if (cbStatus.SelectedIndex == 1)
            {
                status = false;
            }
            DateTime? eventDateTime = null;
            if (tpEventHour.Value != null)
            {
                DateTime eventTime = Convert.ToDateTime(tpEventHour.Value.ToString());
                eventDateTime = new DateTime(((DateTime)dpEventDate.SelectedDate).Year, ((DateTime)dpEventDate.SelectedDate).Month, ((DateTime)dpEventDate.SelectedDate).Day, eventTime.Hour, eventTime.Minute, eventTime.Second, eventTime.Millisecond);
            }
            else
            {
                if (dpEventDate.SelectedDate != null)
                {
                    eventDateTime = (DateTime)dpEventDate.SelectedDate;
                }
            }

            FillEmptyTextBoxes(new TextBox[] { txtAmountClaimed, txtAmountReceived });
            try
            {
                int claimId = 0;
                if (foreingWorkersClaim == null)
                {
                    claimId = claimLogic.InsertForeingWorkersClaim(policy.ForeingWorkersPolicyID, claimTypeId, claimConditionId, status, txtClaimNumber.Text, (DateTime)dpOpenDate.SelectedDate, dpDeliveryDateToCompany.SelectedDate, dpMoneyReceivedDate.SelectedDate, decimal.Parse(txtAmountClaimed.Text), decimal.Parse(txtAmountReceived.Text), eventDateTime, txtEventPlace.Text, txtEventDescription.Text);
                    if (foreingWorkersPaths.Count()>0)
                    {
                        path = foreingWorkersPaths[0] + @"/תביעה " + ((DateTime)dpEventDate.SelectedDate).ToString("dd-MM-yyyy") + " " + policy.ForeingWorkersIndustry.ForeingWorkersIndustryName;
                    }
                }
                else
                {
                    claimId = foreingWorkersClaim.ForeingWorkersClaimID;
                    claimLogic.UpdateForeingWorkersClaim(claimId, claimTypeId, claimConditionId, status, txtClaimNumber.Text, dpDeliveryDateToCompany.SelectedDate, dpMoneyReceivedDate.SelectedDate, decimal.Parse(txtAmountClaimed.Text), decimal.Parse(txtAmountReceived.Text), eventDateTime, txtEventPlace.Text, txtEventDescription.Text);
                    if (foreingWorkersPaths.Count() > 0)
                    {
                        newPath = foreingWorkersPaths[0] + @"/תביעה " + ((DateTime)dpEventDate.SelectedDate).ToString("dd-MM-yyyy")+ " " + foreingWorkersClaim.ForeingWorkersPolicy.ForeingWorkersIndustry.ForeingWorkersIndustryName;
                        path = foreingWorkersPaths[0] + @"/תביעה " + ((DateTime)foreingWorkersClaim.EventDateAndTime).ToString("dd-MM-yyyy") + " " + foreingWorkersClaim.ForeingWorkersPolicy.ForeingWorkersIndustry.ForeingWorkersIndustryName;
                    }
                }
                SaveUnsavedWitness();

                if (witnesses.Count > 0)
                {
                    claimLogic.InsertWitnesses(witnesses, claimId);
                }
                if (trackings.Count > 0)
                {
                    trackingLogic.InsertForeingWorkersClaimTrackings(trackings, claimId, user.UserID);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "", MessageBoxButton.OK, MessageBoxImage.Error);
            }

            if (foreingWorkersClaim == null)
            {
                if (path != null && path != "")
                {
                    if (!Directory.Exists(@path))
                    {
                        Directory.CreateDirectory(@path);
                    }
                }
                else
                {
                    System.Windows.Forms.MessageBox.Show("לא ניתן ליצור את תיקיית התביעה");
                }
            }
            else
            {
                if (path != "")
                {
                    if (Directory.Exists(path) && newPath != "")
                    {
                        if (path != newPath)
                        {
                            try
                            {
                                Directory.Move(path, newPath);
                            }
                            catch (Exception)
                            {
                                System.Windows.Forms.MessageBox.Show("לא ניתן למצוא את נתיב התיקייה");
                            }
                        }
                    }
                }
                else
                {
                    if (newPath != "" && newPath != null)
                    {
                        Directory.CreateDirectory(newPath);
                    }
                }
            }

            if (sender is Button)
            {
                Button btn = (Button)sender;
                if (btn.Name == "btnSave")
                {
                    confirmBeforeClosing = false;
                    Close();
                }
            }
        }

        private void SaveUnsavedWitness()
        {
            if (txtWitnessName.Text != "" || txtWitnessAddress.Text != "" || txtWitnessPhone.Text != "" || txtWitnessCellPhone.Text != "")
            {
                btnAddWitness_Click(this, new RoutedEventArgs());
            }
        }

        public void FillEmptyTextBoxes(TextBox[] inputs)
        {
            foreach (var item in inputs)
            {
                if (item.Text == "")
                {
                    item.Text = "0";
                }
            }
        }

        private void txtAmountClaimed_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (sender is TextBox)
            {
                TextBox txtBox = (TextBox)sender;
                txtBox.ClearValue(BorderBrushProperty);
                if (txtBox.Text != "")
                {
                    try
                    {
                        decimal reusult = validations.ConvertStringToDecimal(txtBox.Text);
                    }
                    catch (Exception ex)
                    {
                        txtBox.BorderBrush = System.Windows.Media.Brushes.Red;
                        System.Windows.Forms.MessageBox.Show(ex.Message);
                        txtBox.Text = "";
                    }
                }
            }
        }

        private void dpEventDate_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            if (dpEventDate.SelectedDate != null)
            {
                tpEventHour.IsEnabled = true;
            }
            else
            {
                tpEventHour.IsEnabled = false;
                tpEventHour.Value = null;
            }
        }

        private void btnAddWitness_Click(object sender, RoutedEventArgs e)
        {
            ForeingWorkersWitness witness = new ForeingWorkersWitness() { WitnessName = txtWitnessName.Text, WittnessAddress = txtWitnessAddress.Text, WitnessPhone = txtWitnessPhone.Text, WitnessCellPhone = txtWitnessCellPhone.Text };
            witnesses.Add(witness);
            dgWitnessesBinding();
            txtWitnessAddress.Clear();
            txtWitnessCellPhone.Clear();
            txtWitnessName.Clear();
            txtWitnessPhone.Clear();
        }

        private void dgWitnessesBinding()
        {
            dgWitnesses.ItemsSource = witnesses.ToList();
            lv.AutoSizeColumns(dgWitnesses.View);
        }

        private void dgWitnesses_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ForeingWorkersWitness witnessSelected = (ForeingWorkersWitness)dgWitnesses.SelectedItem;
            if (witnessSelected != null)
            {
                txtWitnessName.Text = witnessSelected.WitnessName;
                txtWitnessAddress.Text = witnessSelected.WittnessAddress;
                txtWitnessPhone.Text = witnessSelected.WitnessPhone;
                txtWitnessCellPhone.Text = witnessSelected.WitnessCellPhone;
            }
        }

        private void dgWitnesses_MouseDown(object sender, MouseButtonEventArgs e)
        {
            dgWitnesses.UnselectAll();
            txtWitnessName.Clear();
            txtWitnessAddress.Clear();
            txtWitnessPhone.Clear();
            txtWitnessCellPhone.Clear();
        }

        private void dgTrackings_MouseDown(object sender, MouseButtonEventArgs e)
        {
            dgTrackings.UnselectAll();
        }

        private void btnNewTracking_Click(object sender, RoutedEventArgs e)
        {
            frmTracking newTrackingWindow = new frmTracking(client, policy, user);
            newTrackingWindow.ShowDialog();
            if (newTrackingWindow.trackingContent != null)
            {
                trackings.Add(new ForeingWorkersClaimTracking { TrackingContent = newTrackingWindow.trackingContent, Date = DateTime.Now, User = user });
                dgTrackingsBinding();
            }
        }

        private void dgTrackingsBinding()
        {
            dgTrackings.ItemsSource = trackings.OrderByDescending(t => t.Date);
            lv.AutoSizeColumns(dgTrackings.View);
        }

        private void btnDeleteTracking_Click(object sender, RoutedEventArgs e)
        {
            if (dgTrackings.SelectedItem == null)
            {
                MessageBox.Show("נא לסמן מעקב בטבלה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            if (MessageBox.Show("למחוק מעקב", "אישור", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                if (((ForeingWorkersClaimTracking)dgTrackings.SelectedItem).ForeingWorkersClaimTrackingID != 0)
                {
                    trackingLogic.DeleteForeingWorkersClaimTracking((ForeingWorkersClaimTracking)dgTrackings.SelectedItem);
                }
                trackings.Remove((ForeingWorkersClaimTracking)dgTrackings.SelectedItem);
                dgTrackingsBinding();
            }
            else
            {
                dgTrackings.UnselectAll();
            }
        }

        private void btnUpdateTracking_Click(object sender, RoutedEventArgs e)
        {
            if (dgTrackings.SelectedItem == null)
            {
                MessageBox.Show("נא לסמן מעקב בטבלה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            ForeingWorkersClaimTracking trackingToUpdate = (ForeingWorkersClaimTracking)dgTrackings.SelectedItem;
            frmTracking updateTrackingWindow = new frmTracking(trackingToUpdate, client, policy, user);
            updateTrackingWindow.ShowDialog();
            trackingToUpdate.TrackingContent = updateTrackingWindow.trackingContent;
            dgTrackingsBinding();
            dgTrackings.UnselectAll();
        }

        private void dgTrackings_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            DependencyObject src = (DependencyObject)(e.OriginalSource);
            while (!(src is Control))
            {
                if (!(src is Visual))
                {
                    break;
                }
                src = VisualTreeHelper.GetParent(src);
            }

            if ((src is Control && src.GetType() == typeof(ListViewItem)) || src.GetType() == typeof(Run))
            {
                if (dgTrackings.SelectedItem == null)
                {
                    ForeingWorkersClaimTracking trackingToUpdate = (ForeingWorkersClaimTracking)dgTrackings.SelectedItem;
                    frmTracking updateTrackingWindow = new frmTracking(trackingToUpdate, client, policy, user);
                    updateTrackingWindow.ShowDialog();
                    trackingToUpdate.TrackingContent = updateTrackingWindow.trackingContent;
                    dgTrackingsBinding();
                    dgTrackings.UnselectAll();
                }
            }
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void GroupBox_GotFocus(object sender, RoutedEventArgs e)
        {
            isChanges = true;
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (confirmBeforeClosing && isChanges)
            {
                if (MessageBox.Show("האם ברצונך לשמור שינוים שבצעת", "אישור", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    btnSave_Click(sender, new RoutedEventArgs());
                    if (cancelClose)
                    {
                        e.Cancel = true;
                        cancelClose = false;
                    }
                }
            }
        }
    }
}


