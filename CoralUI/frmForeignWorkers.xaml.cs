﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using CoralBusinessLogics;
using System.IO;

namespace CoralUI
{
    /// <summary>
    /// Interaction logic for frmForeignWorkers.xaml
    /// </summary>
    public partial class frmForeignWorkers : Window
    {
        private Client client;
        private bool isOffer;
        private User user;
        private string clientDirPath;
        private string foreingWorkersPath;
        InsurancesLogic insuranceLogic = new InsurancesLogic();
        ForeingWorkersIndutryLogic industryLogic = new ForeingWorkersIndutryLogic();
        ForeingWorkersPolicy foreingWorkersPolicy;
        AgentsLogic agentLogic = new AgentsLogic();
        InputsValidations validation = new InputsValidations();
        CountriesLogic countriesLogic = new CountriesLogic();
        List<object> nullErrorList = null;
        ForeingWorkersLogic foreingWorkersLogic = new ForeingWorkersLogic();
        bool isAddition;
        int policyID;
        List<ForeingWorkersTracking> trackings = new List<ForeingWorkersTracking>();
        ListViewSettings lv = new ListViewSettings();
        TrackingsLogics trackingLogics = new TrackingsLogics();
        List<ForeingWorkersCoverage> coverages = new List<ForeingWorkersCoverage>();
        CoveragesLogic coveragesLogic = new CoveragesLogic();
        bool isRenewal;
        ForeingWorkersRenewalQuote foreingWorkersRenewalQuote;
        public bool renewalAccepted = false;
        private string path;
        private string newPath;
        private bool isChanges = false;
        bool confirmBeforeClosing = true;
        bool cancelClose = false;
        StandardMoneyCollection standardMoneyCollection;
        StandardMoneyCollectionLogic moneyCollectionLogic = new StandardMoneyCollectionLogic();
        private List<StandardMoneyCollectionTracking> standardMoneyCollectionTrackings;

        public frmForeignWorkers(Client clientSelected, User user, bool isOffer)
        {
            InitializeComponent();
            this.client = clientSelected;
            this.user = user;
            this.isOffer = isOffer;
        }

        public frmForeignWorkers(Client clientSelected, User user, bool isAddition, ForeingWorkersPolicy selectedItem)
        {
            InitializeComponent();
            this.client = clientSelected;
            this.user = user;
            this.isAddition = isAddition;
            foreingWorkersPolicy = selectedItem;
        }
        public frmForeignWorkers(Client clientSelected, User user, bool isRenewal, ForeingWorkersPolicy oldPolicy, ForeingWorkersRenewalQuote foreingWorkersRenewalQuote)
        {
            InitializeComponent();
            this.client = clientSelected;
            this.user = user;
            this.isRenewal = isRenewal;
            foreingWorkersPolicy = oldPolicy;
            this.foreingWorkersRenewalQuote = foreingWorkersRenewalQuote;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {

            string strAppDir = System.AppDomain.CurrentDomain.BaseDirectory;
            string path = strAppDir + @"Coral Files\ClientsPath.txt";

            if (File.Exists(path))
            {
                clientDirPath = File.ReadAllText(path);
            }
            else
            {
                MessageBox.Show("לא נמצא תיקיית הלקוחות. נא בדוק בהגדרות ראשוניות את נתיב התיקייה");
                Close();
                return;
            }
            foreingWorkersPath = clientDirPath + @"\" + client.ClientID;
            tbItemCoverage.Visibility = Visibility.Collapsed;
            cbCompanyBinding();
            cbIndustryBinding();
            cbInsuredBirthCountryBinding();
            if (client != null)
            {
                string clientDetails;
                if (client.CellPhone != null && client.CellPhone != "")
                {
                    clientDetails = string.Format("{0} {1}  ת.ז: {2} נייד: {3}", client.FirstName, client.LastName, client.IdNumber, client.CellPhone);
                }
                else
                {
                    clientDetails = string.Format("{0} {1}  ת.ז: {2}", client.FirstName, client.LastName, client.IdNumber);
                }
                lblClientNameAndId.Content = clientDetails;
            }
            if (foreingWorkersPolicy == null)
            {
                txtAddition.Text = "0";
                txtAddition.IsEnabled = false;
                DateTime now = DateTime.Now;
                dpOpenDate.SelectedDate = now;
                dpStartDate.SelectedDate = now;
            }
            else
            {
                dpOpenDate.SelectedDate = foreingWorkersPolicy.OpenDate;
                txtPolicyNumber.Text = foreingWorkersPolicy.PolicyNumber;
                dpStartDate.SelectedDate = foreingWorkersPolicy.StartDate;
                dpEndDate.SelectedDate = foreingWorkersPolicy.EndDate;
                isOffer = foreingWorkersPolicy.IsOffer;
                txtAddition.Text = foreingWorkersPolicy.Addition.ToString();
                SelectItemInCb((int)foreingWorkersPolicy.CompanyID, cbCompany);
                var industries = cbIndustry.Items;
                foreach (var item in industries)
                {
                    ForeingWorkersIndustry industry = (ForeingWorkersIndustry)item;
                    if (industry.ForeingWorkersIndustryID == foreingWorkersPolicy.ForeingWorkersIndustryID)
                    {
                        cbIndustry.SelectedItem = item;
                        break;
                    }
                }
                cbIndustry.IsEnabled = false;
                var principalAgentNumbers = cbPrincipalAgentNumber.Items;
                foreach (var item in principalAgentNumbers)
                {
                    AgentNumber principalAgentNumber = (AgentNumber)item;
                    if (principalAgentNumber.AgentNumberID == foreingWorkersPolicy.PrincipalAgentNumberID)
                    {
                        cbPrincipalAgentNumber.SelectedItem = item;
                        break;
                    }
                }
                var secundaryAgentNumbers = cbSecundaryAgentNumber.Items;
                foreach (var item in secundaryAgentNumbers)
                {
                    AgentNumber secundaryAgentNumber = (AgentNumber)item;
                    if (secundaryAgentNumber.AgentNumberID == foreingWorkersPolicy.SubAgentNumberID)
                    {
                        cbSecundaryAgentNumber.SelectedItem = item;
                        break;
                    }
                }
                CurrencySelection();
                PaymentMethodSelection();
                txtPaymentesNumber.Text = foreingWorkersPolicy.PaymentsNumber.ToString();
                dpPaymentDate.SelectedDate = foreingWorkersPolicy.PaymentDate;
                txtTotalPremium.Text = ((decimal)foreingWorkersPolicy.TotalPremium).ToString("G29");
                txtComments.Text = foreingWorkersPolicy.Comments;
                txtContactLastName.Text = foreingWorkersPolicy.ContactLastName;
                txtContactFirstName.Text = foreingWorkersPolicy.ContactName;
                txtContactCellPhone.Text = foreingWorkersPolicy.ContactCellPhone;
                txtContactPhone.Text = foreingWorkersPolicy.ContactPhone;
                txtContactFax.Text = foreingWorkersPolicy.ContactFax;
                txtContactEmail.Text = foreingWorkersPolicy.ContactEmail;
                if (foreingWorkersPolicy.InsuredBirthCountryID != null)
                {
                    SelectCountryInCb((int)foreingWorkersPolicy.InsuredBirthCountryID, cbInsuredBirthCountry);
                }
                txtInsuredFirstName.Text = foreingWorkersPolicy.InsuredFirstName;
                txtInsuredMiddleName.Text = foreingWorkersPolicy.InsuredMiddleName;
                txtInsuredLastName.Text = foreingWorkersPolicy.InsuredLastName;
                dpInsuredBirthDate.SelectedDate = foreingWorkersPolicy.InsuredBirthDate;
                txtInsuredPassportNumber.Text = foreingWorkersPolicy.InsuredPassportNumber;
                if (foreingWorkersPolicy.IsMale == true)
                {
                    rbMale.IsChecked = true;
                }
                else if (foreingWorkersPolicy.IsMale == false)
                {
                    rbFemale.IsChecked = true;
                }
                txtInsuredCellphone.Text = foreingWorkersPolicy.InsuredCellphone;
                txtInsuredHomePhone.Text = foreingWorkersPolicy.InsuredHomePhone;
                txtInsuredHomeAddress.Text = foreingWorkersPolicy.InsuredHomeAddress;
                txtInsuredWorkAddress.Text = foreingWorkersPolicy.InsuredWorkAddress;
                dpFirstEntryToIsrael.SelectedDate = foreingWorkersPolicy.FirstEntryToIsrael;
                dpLastEntryToIsrael.SelectedDate = foreingWorkersPolicy.LastEntryToIsrael;
                chbIsPreviousInsurances.IsChecked = foreingWorkersPolicy.IsPreviousInsurencesInIsrael;
                dpPreviousInsuranceFrom.SelectedDate = foreingWorkersPolicy.PreviousIsuranceFrom;
                dpPreviousInsuranceTo.SelectedDate = foreingWorkersPolicy.PreviousInsuranceTo;
                if (foreingWorkersPolicy.PreviousInsuranceCompanyID != null)
                {
                    SelectItemInCb((int)foreingWorkersPolicy.PreviousInsuranceCompanyID, cbPreviousInsuranceCompany);
                }
                txtPreviousInsurancePolicyNumber.Text = foreingWorkersPolicy.PreviousInsurancePolicyNumber;
                cbKupatJolim.Text = foreingWorkersPolicy.ClinicName;
                txtClinicAddress.Text = foreingWorkersPolicy.ClinicAddress;
                txtClinicNumber.Text = foreingWorkersPolicy.ClinicNumber;

                coverages = coveragesLogic.GetAllForeingWorkersCoveragesByPolicy(foreingWorkersPolicy.ForeingWorkersPolicyID);
                dgCoveragesBinding();

                if (isRenewal)
                {
                    dpEndDate.SelectedDate = null;
                    txtPolicyNumber.Text = foreingWorkersRenewalQuote.PolicyNumber;
                    if (foreingWorkersRenewalQuote.CompanyID != foreingWorkersPolicy.CompanyID)
                    {
                        foreingWorkersPolicy.ForeingWorkersInsuranceTypeID = 0;
                        SelectItemInCb((int)foreingWorkersRenewalQuote.CompanyID, cbCompany);
                    }
                    if (foreingWorkersRenewalQuote.Premium != null)
                    {
                        txtTotalPremium.Text = ((decimal)foreingWorkersRenewalQuote.Premium).ToString("0.##");
                    }
                    dpStartDate.SelectedDate = ((DateTime)foreingWorkersPolicy.EndDate).AddDays(0.5);
                    return;
                }

                trackings = trackingLogics.GetAllForeingWorkersTrackingsByPolicy(foreingWorkersPolicy.ForeingWorkersPolicyID);
                dgTrackingsBinding();

                

                if (foreingWorkersPolicy.Addition != 0)
                {
                    txtPolicyNumber.IsEnabled = false;
                }
                if (int.Parse(foreingWorkersLogic.GetAdditionNumber(foreingWorkersPolicy.PolicyNumber)) - 1 > foreingWorkersPolicy.Addition)
                {
                    spGeneral.IsEnabled = false;
                    spInsured.IsEnabled = false;
                    dpCoverages.IsEnabled = false;
                    dpTrackings.IsEnabled = false;
                }
                if (isAddition)
                {
                    txtPolicyNumber.IsEnabled = false;
                    txtAddition.Text = foreingWorkersLogic.GetAdditionNumber(foreingWorkersPolicy.PolicyNumber);
                    DateTime now = DateTime.Now;
                    dpOpenDate.SelectedDate = now;
                    dpStartDate.SelectedDate = now;
                    cbCompany.IsEnabled = false;
                }
                else
                {
                    standardMoneyCollection = moneyCollectionLogic.GetStandardMoneyColletionByPolicy(foreingWorkersPolicy);
                    if (standardMoneyCollection != null)
                    {
                        chbStandardMoneyCollection.IsChecked = true;
                        chbStandardMoneyCollection.IsEnabled = false;
                        standardMoneyCollectionTrackings = moneyCollectionLogic.GetTrakcingsByStsndardMoneyCollection(standardMoneyCollection);
                    }
                }
            }

        }

        private void PaymentMethodSelection()
        {
            var paymentMethods = cbPaymentMode.Items;
            foreach (var item in paymentMethods)
            {
                if (foreingWorkersPolicy.PaymentMethod == ((ComboBoxItem)item).Content.ToString())
                {
                    cbPaymentMode.SelectedItem = item;
                    break;
                }
            }
        }
        private void CurrencySelection()
        {
            var currencies = cbCurrency.Items;
            foreach (var item in currencies)
            {
                if (foreingWorkersPolicy.Currency == ((ComboBoxItem)item).Content.ToString())
                {
                    cbCurrency.SelectedItem = item;
                    break;
                }
            }
        }
        private void cbInsuredBirthCountryBinding()
        {
            cbInsuredBirthCountry.ItemsSource = countriesLogic.GetActiveCountries();
            cbInsuredBirthCountry.DisplayMemberPath = "CountryName";
        }

        private void cbIndustryBinding()
        {
            cbIndustry.ItemsSource = industryLogic.GetAllForeingWorkersIndustries();
            cbIndustry.DisplayMemberPath = "ForeingWorkersIndustryName";
        }

        private void cbCompanyBinding()
        {
            int? companyId = null;
            if (cbCompany.SelectedItem != null)
            {
                companyId = ((InsuranceCompany)cbCompany.SelectedItem).CompanyID;
            }
            int? insuredPreviousCompanyId = null;
            if (cbPreviousInsuranceCompany.SelectedItem != null)
            {
                insuredPreviousCompanyId = ((InsuranceCompany)cbPreviousInsuranceCompany.SelectedItem).CompanyID;
            }
            var companies = insuranceLogic.GetActiveCompaniesByInsurance("עובדים זרים");
            cbCompany.ItemsSource = companies;
            cbCompany.DisplayMemberPath = "Company.CompanyName";
            cbPreviousInsuranceCompany.ItemsSource = companies;
            cbPreviousInsuranceCompany.DisplayMemberPath = "Company.CompanyName";
            if (companyId != null)
            {
                SelectItemInCb((int)companyId, cbCompany);
            }
            if (insuredPreviousCompanyId != null)
            {
                SelectItemInCb((int)insuredPreviousCompanyId, cbPreviousInsuranceCompany);
            }
        }

        private void chbIsPreviousInsurances_Checked(object sender, RoutedEventArgs e)
        {
            dpPreviousInsuranceFrom.IsEnabled = true;
            dpPreviousInsuranceTo.IsEnabled = true;
            spPreviousInsuranceCompany.IsEnabled = true;
            txtPreviousInsurancePolicyNumber.IsEnabled = true;
        }

        private void chbIsPreviousInsurances_Unchecked(object sender, RoutedEventArgs e)
        {
            dpPreviousInsuranceFrom.IsEnabled = false;
            dpPreviousInsuranceTo.IsEnabled = false;
            spPreviousInsuranceCompany.IsEnabled = false;
            txtPreviousInsurancePolicyNumber.IsEnabled = false;
        }

        private void btnUpdateCompanyTable_Click(object sender, RoutedEventArgs e)
        {
            string objName = ((Button)sender).Name;
            int? selectedItemId = null;
            ComboBox cb = null;
            if (objName == "btnUpdateCompanyTable")
            {
                if (cbCompany.SelectedItem != null)
                {
                    selectedItemId = ((InsuranceCompany)cbCompany.SelectedItem).CompanyID;
                }
                cb = cbCompany;
            }
            else if (objName == "btnUpdatePreviousCompanyTable")
            {
                if (cbPreviousInsuranceCompany.SelectedItem != null)
                {
                    selectedItemId = ((InsuranceCompany)cbPreviousInsuranceCompany.SelectedItem).CompanyID;
                }
                cb = cbPreviousInsuranceCompany;
            }

            int insuranceID = insuranceLogic.GetInsuranceID("עובדים זרים");
            InsuranceCompany company = new InsuranceCompany() { InsuranceID = insuranceID };
            frmUpdateTable updateCompaniesTable = new frmUpdateTable(company);
            updateCompaniesTable.ShowDialog();
            cbCompanyBinding();
            if (updateCompaniesTable.ItemAdded != null)
            {
                SelectItemInCb(((InsuranceCompany)updateCompaniesTable.ItemAdded).CompanyID, cb);
            }
            else if (selectedItemId != null)
            {
                SelectItemInCb((int)selectedItemId, cb);
            }
        }
        private void SelectItemInCb(int id, ComboBox cb)
        {
            var items = cb.Items;
            foreach (var item in items)
            {
                InsuranceCompany parsedItem = (InsuranceCompany)item;
                if (parsedItem.CompanyID == id)
                {
                    cb.SelectedItem = item;
                    break;
                }
            }
        }

        private void cbIndustry_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cbIndustry.SelectedItem == null)
            {
                return;
            }
            if (cbCompany.SelectedItem != null)
            {
                cbInsuranceType.IsEnabled = true;
                btnUpdateInsuranceTypeTable.IsEnabled = true;
                cbInsuranceTypeBinding();
                if (foreingWorkersPolicy != null)
                {
                    SelectInsuranceTypeItemInCb((int)foreingWorkersPolicy.ForeingWorkersInsuranceTypeID, cbInsuranceType);
                }
            }
        }

        private void cbInsuranceTypeBinding()
        {
            cbInsuranceType.ItemsSource = industryLogic.GetActiveForeingWorkersInsuranceTypesByIndustryAndCompany(((ForeingWorkersIndustry)cbIndustry.SelectedItem).ForeingWorkersIndustryID, ((InsuranceCompany)cbCompany.SelectedItem).CompanyID);
            cbInsuranceType.DisplayMemberPath = "ForeingWorkersInsuranceTypeName";
        }

        private void cbCompany_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cbCompany.SelectedItem == null)
            {
                cbPrincipalAgentNumber.SelectedItem = null;
                cbSecundaryAgentNumber.SelectedItem = null;
                cbPrincipalAgentNumber.IsEnabled = false;
                cbSecundaryAgentNumber.IsEnabled = false;
                return;
            }
            if (cbIndustry.SelectedItem != null)
            {
                cbInsuranceType.IsEnabled = true;
                btnUpdateInsuranceTypeTable.IsEnabled = true;
                cbInsuranceTypeBinding();
                if (foreingWorkersPolicy != null)
                {
                    SelectInsuranceTypeItemInCb((int)foreingWorkersPolicy.ForeingWorkersInsuranceTypeID, cbInsuranceType);
                }
            }
            int insuranceID = insuranceLogic.GetInsuranceID("עובדים זרים");
            if (client != null)
            {
                cbPrincipalAgentNumber.ItemsSource = agentLogic.GetAgentNumbersByCompanyAndInsurance(client.PrincipalAgentID, (InsuranceCompany)cbCompany.SelectedItem, insuranceID);
                cbPrincipalAgentNumber.DisplayMemberPath = "Number";
                cbPrincipalAgentNumber.IsEnabled = true;
                cbPrincipalAgentNumber.SelectedIndex = 0;
                cbSecundaryAgentNumber.ItemsSource = agentLogic.GetAgentNumbersByCompanyAndInsurance(client.SecundaryAgentID, (InsuranceCompany)cbCompany.SelectedItem, insuranceID);
                cbSecundaryAgentNumber.DisplayMemberPath = "Number";
                cbSecundaryAgentNumber.IsEnabled = true;
                cbSecundaryAgentNumber.SelectedIndex = 0;
            }
        }

        private void btnUpdateInsuranceTypeTable_Click(object sender, RoutedEventArgs e)
        {
            int? selectedItemId = null;
            if (cbInsuranceType.SelectedItem != null)
            {
                selectedItemId = ((ForeingWorkersInsuranceType)cbInsuranceType.SelectedItem).ForeingWorkersInsuranceTypeID;
            }
            ForeingWorkersInsuranceType type = new ForeingWorkersInsuranceType() { ForeingWorkersIndustryID = ((ForeingWorkersIndustry)cbIndustry.SelectedItem).ForeingWorkersIndustryID, CompanyID = ((InsuranceCompany)cbCompany.SelectedItem).CompanyID };
            frmUpdateTable updateInsuranceTypeTable = new frmUpdateTable(type);
            updateInsuranceTypeTable.ShowDialog();
            cbInsuranceTypeBinding();
            if (updateInsuranceTypeTable.ItemAdded != null)
            {
                SelectInsuranceTypeItemInCb(((ForeingWorkersInsuranceType)updateInsuranceTypeTable.ItemAdded).ForeingWorkersInsuranceTypeID, cbInsuranceType);
            }
            else if (selectedItemId != null)
            {
                SelectInsuranceTypeItemInCb((int)selectedItemId, cbInsuranceType);
            }
        }

        private void SelectInsuranceTypeItemInCb(int id, ComboBox cb)
        {
            var items = cb.Items;
            foreach (var item in items)
            {
                ForeingWorkersInsuranceType parsedItem = (ForeingWorkersInsuranceType)item;
                if (parsedItem.ForeingWorkersInsuranceTypeID == id)
                {
                    cb.SelectedItem = item;
                    break;
                }
            }
        }

        private void txtTotalPremium_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (sender is TextBox)
            {
                TextBox txtBox = (TextBox)sender;
                txtBox.ClearValue(BorderBrushProperty);
                if (txtBox.Text != "")
                {
                    try
                    {
                        decimal reusult = validation.ConvertStringToDecimal(txtBox.Text);
                    }
                    catch (Exception ex)
                    {
                        txtBox.BorderBrush = System.Windows.Media.Brushes.Red;
                        System.Windows.Forms.MessageBox.Show(ex.Message);
                        txtBox.Text = "";
                    }
                }
            }
        }

        private void btnUpdateCountriesTable_Click(object sender, RoutedEventArgs e)
        {
            int? selectedItemId = null;
            if (cbInsuredBirthCountry.SelectedItem != null)
            {
                selectedItemId = ((Country)cbInsuredBirthCountry.SelectedItem).CountryID;
            }


            frmUpdateTable updateCountriesTable = new frmUpdateTable(new Country());
            updateCountriesTable.ShowDialog();
            cbInsuredBirthCountryBinding();
            if (updateCountriesTable.ItemAdded != null)
            {
                SelectCountryInCb(((Country)updateCountriesTable.ItemAdded).CountryID, cbInsuredBirthCountry);
            }
            else if (selectedItemId != null)
            {
                SelectCountryInCb((int)selectedItemId, cbInsuredBirthCountry);
            }
        }
        private void SelectCountryInCb(int id, ComboBox cb)
        {
            var items = cb.Items;
            foreach (var item in items)
            {
                Country parsedItem = (Country)item;
                if (parsedItem.CountryID == id)
                {
                    cb.SelectedItem = item;
                    break;
                }
            }
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            if (nullErrorList != null && nullErrorList.Count > 0)
            {
                foreach (var item in nullErrorList)
                {
                    if (item is TextBox)
                    {
                        ((TextBox)item).ClearValue(BorderBrushProperty);
                    }
                    else if (item is ComboBox)
                    {
                        ((ComboBox)item).ClearValue(BorderBrushProperty);
                    }
                    else if (item is DatePicker)
                    {
                        ((DatePicker)item).ClearValue(BorderBrushProperty);
                    }
                }
            }
            if (!validation.IsDigitsOnly(txtPolicyNumber.Text))
            {
                cancelClose = true;
                MessageBox.Show("מס' פוליסה יכול להכיל מספרים בלבד", "", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            nullErrorList = validation.InputNullValidation(new object[] { txtPolicyNumber, txtAddition, cbIndustry, cbCompany, cbInsuranceType, dpStartDate, dpEndDate, txtTotalPremium, txtInsuredFirstName, txtInsuredLastName, txtInsuredPassportNumber });
            if (nullErrorList.Count > 0)
            {
                cancelClose = true;
                tbItemGeneral.Focus();
                MessageBox.Show("נא למלאות שדות חובה", "", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (dpEndDate.SelectedDate <= dpStartDate.SelectedDate)
            {
                cancelClose = true;
                tbItemGeneral.Focus();
                MessageBox.Show("תאריך סיום חייב להיות גדול מתאריך תחילה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            int addition = Convert.ToInt32(txtAddition.Text);
            int? principalAgentNumberID = null;
            if ((AgentNumber)cbPrincipalAgentNumber.SelectedItem != null)
            {
                principalAgentNumberID = ((AgentNumber)cbPrincipalAgentNumber.SelectedItem).AgentNumberID;
            }
            int? subAgentNumberID = null;
            if ((AgentNumber)cbSecundaryAgentNumber.SelectedItem != null)
            {
                subAgentNumberID = ((AgentNumber)cbSecundaryAgentNumber.SelectedItem).AgentNumberID;
            }
            int? paymentNumbers = null;
            if (txtPaymentesNumber.Text != "")
            {
                paymentNumbers = int.Parse(txtPaymentesNumber.Text);
            }
            decimal? premium = null;
            if (txtTotalPremium.Text != "")
            {
                premium = decimal.Parse(txtTotalPremium.Text);
            }
            string paymentMode = null;
            if (cbPaymentMode.SelectedItem != null)
            {
                paymentMode = ((ComboBoxItem)cbPaymentMode.SelectedItem).Content.ToString();
            }
            int? countryId = null;
            if (cbInsuredBirthCountry.SelectedItem != null)
            {
                countryId = ((Country)cbInsuredBirthCountry.SelectedItem).CountryID;
            }
            bool? isMale = null;
            if (rbFemale.IsChecked == true)
            {
                isMale = false;
            }
            else if (rbMale.IsChecked == true)
            {
                isMale = true;
            }
            int? previousCompanyId = null;
            if (cbPreviousInsuranceCompany.SelectedItem != null)
            {
                previousCompanyId = ((InsuranceCompany)cbPreviousInsuranceCompany.SelectedItem).CompanyID;
            }
            try
            {
                if (foreingWorkersPolicy == null || isAddition == true || isRenewal == true)
                {
                    if (isRenewal == true)
                    {
                        renewalAccepted = true;
                    }
                    policyID = foreingWorkersLogic.InsertForeingWorkersPolicy(client.ClientID, isOffer, ((ForeingWorkersIndustry)cbIndustry.SelectedItem).ForeingWorkersIndustryID, ((ForeingWorkersInsuranceType)cbInsuranceType.SelectedItem).ForeingWorkersInsuranceTypeID, txtPolicyNumber.Text, addition, (DateTime)dpOpenDate.SelectedDate, (DateTime)dpStartDate.SelectedDate, (DateTime)dpEndDate.SelectedDate, ((InsuranceCompany)cbCompany.SelectedItem).CompanyID, principalAgentNumberID, subAgentNumberID, paymentMode, ((ComboBoxItem)cbCurrency.SelectedItem).Content.ToString(), paymentNumbers, dpPaymentDate.SelectedDate, (decimal)premium, txtContactLastName.Text, txtContactFirstName.Text, txtContactPhone.Text, txtContactCellPhone.Text, txtContactFax.Text, txtContactEmail.Text, countryId, txtInsuredFirstName.Text, txtInsuredMiddleName.Text, txtInsuredLastName.Text, dpInsuredBirthDate.SelectedDate, txtInsuredPassportNumber.Text, isMale, txtInsuredCellphone.Text, txtInsuredHomePhone.Text, txtInsuredHomeAddress.Text, txtInsuredWorkAddress.Text, dpFirstEntryToIsrael.SelectedDate, dpLastEntryToIsrael.SelectedDate, chbIsPreviousInsurances.IsChecked, dpPreviousInsuranceFrom.SelectedDate, dpPreviousInsuranceTo.SelectedDate, previousCompanyId, txtPreviousInsurancePolicyNumber.Text, cbKupatJolim.Text, txtClinicNumber.Text, txtClinicAddress.Text, txtComments.Text);
                    if (!isOffer)
                    {
                        path = foreingWorkersPath + @"\עובדים זרים\" + txtPolicyNumber.Text + " " + ((ForeingWorkersIndustry)cbIndustry.SelectedItem).ForeingWorkersIndustryName;
                    }
                    else
                    {
                        path = foreingWorkersPath + @"\הצעות\עובדים זרים\" + txtPolicyNumber.Text + " " + ((ForeingWorkersIndustry)cbIndustry.SelectedItem).ForeingWorkersIndustryName;
                    }
                    if (coverages.Count > 0)
                    {
                        coveragesLogic.InsertCoveragesToForeingWorkersPolicy(policyID, coverages, isAddition);
                    }
                    if (trackings.Count > 0)
                    {
                        trackingLogics.InsertForeingWorkersTrackings(policyID, trackings, isAddition);
                    }
                    if (chbStandardMoneyCollection.IsChecked == true)
                    {
                        int standardMoneyCollectionId = moneyCollectionLogic.InsertStandardMoneyCollection(standardMoneyCollection, new Insurance() { InsuranceName = "עובדים זרים" }, policyID);
                        if (standardMoneyCollectionTrackings != null)
                        {
                            moneyCollectionLogic.InsertTrackingsToStandardMoneyCollection(standardMoneyCollectionId, standardMoneyCollectionTrackings);
                        }
                    }
                }
                else
                {
                    foreingWorkersLogic.UpdateForeingWorkersPolicy(foreingWorkersPolicy.ForeingWorkersPolicyID, txtPolicyNumber.Text, dpStartDate.SelectedDate, dpEndDate.SelectedDate, ((InsuranceCompany)cbCompany.SelectedItem).CompanyID, principalAgentNumberID, subAgentNumberID, ((ForeingWorkersInsuranceType)cbInsuranceType.SelectedItem).ForeingWorkersInsuranceTypeID, paymentMode, ((ComboBoxItem)cbCurrency.SelectedItem).Content.ToString(), paymentNumbers, dpPaymentDate.SelectedDate, (decimal)premium, txtContactLastName.Text, txtContactFirstName.Text, txtContactPhone.Text, txtContactCellPhone.Text, txtContactFax.Text, txtContactEmail.Text, countryId, txtInsuredFirstName.Text, txtInsuredMiddleName.Text, txtInsuredLastName.Text, dpInsuredBirthDate.SelectedDate, txtInsuredPassportNumber.Text, isMale, txtInsuredCellphone.Text, txtInsuredHomePhone.Text, txtInsuredHomeAddress.Text, txtInsuredWorkAddress.Text, dpFirstEntryToIsrael.SelectedDate, dpLastEntryToIsrael.SelectedDate, chbIsPreviousInsurances.IsChecked, dpPreviousInsuranceFrom.SelectedDate, dpPreviousInsuranceTo.SelectedDate, previousCompanyId, txtPreviousInsurancePolicyNumber.Text, cbKupatJolim.Text, txtClinicNumber.Text, txtClinicAddress.Text, txtComments.Text);
                    if (!isOffer)
                    {
                        string[] dirs = Directory.GetDirectories(foreingWorkersPath + @"\עובדים זרים", "*" + foreingWorkersPolicy.PolicyNumber + " " + foreingWorkersPolicy.ForeingWorkersIndustry.ForeingWorkersIndustryName + "*");
                        if (dirs.Count() > 0)
                        {
                            path = dirs[0];
                        }
                        else
                        {
                            dirs = Directory.GetDirectories(foreingWorkersPath + @"\עובדים זרים", "*" + foreingWorkersPolicy.PolicyNumber + "*");
                            if (dirs.Count() > 0)
                            {
                                path = dirs[0];
                            }
                        }
                        newPath = foreingWorkersPath + @"\עובדים זרים\" + txtPolicyNumber.Text + " " + ((ForeingWorkersIndustry)cbIndustry.SelectedItem).ForeingWorkersIndustryName;
                    }
                    else
                    {
                        string[] dirs = Directory.GetDirectories(foreingWorkersPath + @"\הצעות\עובדים זרים", "*" + foreingWorkersPolicy.PolicyNumber + " " + foreingWorkersPolicy.ForeingWorkersIndustry.ForeingWorkersIndustryName + "*");
                        if (dirs.Count() > 0)
                        {
                            path = dirs[0];
                        }
                        else
                        {
                            dirs = Directory.GetDirectories(foreingWorkersPath + @"\הצעות\עובדים זרים", "*" + foreingWorkersPolicy.PolicyNumber + "*");
                            if (dirs.Count() > 0)
                            {
                                path = dirs[0];
                            }
                        }
                        newPath = foreingWorkersPath + @"\הצעות\עובדים זרים\" + txtPolicyNumber.Text + " " + ((ForeingWorkersIndustry)cbIndustry.SelectedItem).ForeingWorkersIndustryName;
                    }
                    trackingLogics.InsertForeingWorkersTrackings(foreingWorkersPolicy.ForeingWorkersPolicyID, trackings, isAddition);
                    coveragesLogic.InsertCoveragesToForeingWorkersPolicy(foreingWorkersPolicy.ForeingWorkersPolicyID, coverages, isAddition);
                    if (chbStandardMoneyCollection.IsChecked == true)
                    {
                        int standardMoneyCollectionId = moneyCollectionLogic.InsertStandardMoneyCollection(standardMoneyCollection, new Insurance() { InsuranceName = "עובדים זרים" }, foreingWorkersPolicy.ForeingWorkersPolicyID);
                        moneyCollectionLogic.InsertTrackingsToStandardMoneyCollection(standardMoneyCollectionId, standardMoneyCollectionTrackings);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }

            if (isAddition == false)
            {
                if (foreingWorkersPolicy == null || isRenewal)
                {
                    if (!Directory.Exists(@path))
                    {
                        Directory.CreateDirectory(@path);
                    }
                }
                else
                {
                    if (path == null && newPath != null)
                    {
                        Directory.CreateDirectory(newPath);
                    }
                    else if (path != newPath)
                    {
                        try
                        {
                            Directory.Move(path, newPath);
                        }
                        catch (Exception)
                        {
                            System.Windows.Forms.MessageBox.Show("לא ניתן למצוא את נתיב התיקייה");
                        }
                    }
                }
            }
            if (sender is Button)
            {
                Button btn = (Button)sender;
                if (btn.Name == "btnSave")
                {
                    confirmBeforeClosing = false;
                    Close();
                }
            }
        }

        private void cbInsuranceType_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cbInsuranceType.SelectedItem != null)
            {
                tbItemCoverage.Visibility = Visibility.Visible;
            }
            else
            {
                tbItemCoverage.Visibility = Visibility.Collapsed;
            }
        }

        private void btnNewTracking_Click(object sender, RoutedEventArgs e)
        {
            frmTracking newTrackingWindow = new frmTracking(client, foreingWorkersPolicy, user);
            newTrackingWindow.ShowDialog();
            if (newTrackingWindow.trackingContent != null)
            {
                trackings.Add(new ForeingWorkersTracking() { TrackingContent = newTrackingWindow.trackingContent, Date = DateTime.Now, UserID = user.UserID, User = user });
                dgTrackingsBinding();
            }
        }

        private void dgTrackingsBinding()
        {
            dgTrackings.ItemsSource = trackings.OrderByDescending(t => t.Date);
            lv.AutoSizeColumns(dgTrackings.View);
        }

        private void btnDeleteTracking_Click(object sender, RoutedEventArgs e)
        {
            if (dgTrackings.SelectedItem == null)
            {
                MessageBox.Show("נא לסמן מעקב בטבלה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            if (MessageBox.Show("למחוק מעקב", "אישור", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                trackings.Remove((ForeingWorkersTracking)dgTrackings.SelectedItem);
                dgTrackingsBinding();
            }
            else
            {
                dgTrackings.UnselectAll();
            }
        }

        private void btnUpdateTracking_Click(object sender, RoutedEventArgs e)
        {
            if (dgTrackings.SelectedItem == null)
            {
                MessageBox.Show("נא לסמן מעקב בטבלה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            ForeingWorkersTracking trackingToUpdate = (ForeingWorkersTracking)dgTrackings.SelectedItem;
            frmTracking updateTrackingWindow = new frmTracking(trackingToUpdate, client, foreingWorkersPolicy, user);
            updateTrackingWindow.ShowDialog();
            trackingToUpdate.TrackingContent = updateTrackingWindow.trackingContent;
            dgTrackingsBinding();
            dgTrackings.UnselectAll();
        }

        private void dgTrackings_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            DependencyObject src = (DependencyObject)(e.OriginalSource);
            while (!(src is Control))
            {
                if (!(src is Visual))
                {
                    break;
                }
                src = VisualTreeHelper.GetParent(src);
            }

            if ((src is Control && src.GetType() == typeof(ListViewItem)) || src.GetType() == typeof(Run))
            {
                if (dgTrackings.SelectedItem == null)
                {
                    return;
                }
                ForeingWorkersTracking trackingToUpdate = (ForeingWorkersTracking)dgTrackings.SelectedItem;
                frmTracking updateTrackingWindow = new frmTracking(trackingToUpdate, client, foreingWorkersPolicy, user);
                updateTrackingWindow.ShowDialog();
                trackingToUpdate.TrackingContent = updateTrackingWindow.trackingContent;
                dgTrackingsBinding();
                dgTrackings.UnselectAll();
            }
        }

        private void btnNewCoverage_Click(object sender, RoutedEventArgs e)
        {
            frmForeingWorkersCoverage newCoverageWindow = new frmForeingWorkersCoverage();
            newCoverageWindow.ShowDialog();
            if (newCoverageWindow.NewCoverage != null)
            {
                coverages.Add(newCoverageWindow.NewCoverage);
            }
            dgCoveragesBinding();
        }

        private void dgCoveragesBinding()
        {
            dgCoverages.ItemsSource = coverages.ToList();
            lv.AutoSizeColumns(dgCoverages.View);
        }

        private void btnDeleteCoverage_Click(object sender, RoutedEventArgs e)
        {
            if (dgCoverages.SelectedItem == null)
            {
                MessageBox.Show("נא לסמן כיסוי בטבלה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            if (MessageBox.Show("למחוק כיסוי", "אישור", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                coverages.Remove((ForeingWorkersCoverage)dgCoverages.SelectedItem);
                dgCoveragesBinding();
            }
            else
            {
                dgCoverages.UnselectAll();
            }
        }

        private void btnUpdateCoverage_Click(object sender, RoutedEventArgs e)
        {
            if (dgCoverages.SelectedItem == null)
            {
                MessageBox.Show("נא לסמן כיסוי בטבלה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            ForeingWorkersCoverage coverageToUpdate = (ForeingWorkersCoverage)dgCoverages.SelectedItem;
            frmForeingWorkersCoverage updateCoverageWindow = new frmForeingWorkersCoverage(coverageToUpdate);
            updateCoverageWindow.ShowDialog();
            //coverageToUpdate = updateCoverageWindow.NewCoverage;
            coverageToUpdate.ForeingWorkersCoverageType = updateCoverageWindow.NewCoverage.ForeingWorkersCoverageType;
            coverageToUpdate.ForeingWorkersCoverageTypeID = updateCoverageWindow.NewCoverage.ForeingWorkersCoverageTypeID;
            coverageToUpdate.CoverageAmount = updateCoverageWindow.NewCoverage.CoverageAmount;
            coverageToUpdate.Porcentage = updateCoverageWindow.NewCoverage.Porcentage;
            coverageToUpdate.premium = updateCoverageWindow.NewCoverage.premium;
            coverageToUpdate.Comments = updateCoverageWindow.NewCoverage.Comments;
            dgCoveragesBinding();
            dgCoverages.UnselectAll();
        }

        private void dgCoverages_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            DependencyObject src = (DependencyObject)(e.OriginalSource);
            while (!(src is Control))
            {
                if (!(src is Visual))
                {
                    break;
                }
                src = VisualTreeHelper.GetParent(src);
            }

            if ((src is Control && src.GetType() == typeof(ListViewItem)) || src.GetType() == typeof(Run))
            {
                btnUpdateCoverage_Click(sender, new RoutedEventArgs());
            }
        }

        private void dgCoverages_MouseDown(object sender, MouseButtonEventArgs e)
        {
            dgCoverages.UnselectAll();
        }

        private void dgTrackings_MouseDown(object sender, MouseButtonEventArgs e)
        {
            dgTrackings.UnselectAll();
        }

        private void txtPolicyNumber_GotFocus(object sender, RoutedEventArgs e)
        {
            isChanges = true;
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (confirmBeforeClosing && isChanges)
            {
                if (MessageBox.Show("האם ברצונך לשמור שינוים שבצעת", "אישור", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    btnSave_Click(sender, new RoutedEventArgs());
                    if (cancelClose)
                    {
                        e.Cancel = true;
                        cancelClose = false;
                    }
                }
            }
        }

        private void chbStandardMoneyCollection_Checked(object sender, RoutedEventArgs e)
        {
            btnStandardMoneyCollectionDetails.IsEnabled = true;
        }
        private void chbStandardMoneyCollection_Unchecked(object sender, RoutedEventArgs e)
        {
            btnStandardMoneyCollectionDetails.IsEnabled = false;
        }

        private void btnStandardMoneyCollectionDetails_Click(object sender, RoutedEventArgs e)
        {
            frmStandardMoneyCollectionDetails window = null;
            if (standardMoneyCollection == null)
            {
                window = new frmStandardMoneyCollectionDetails(false, user);
            }
            else
            {
                window = new frmStandardMoneyCollectionDetails(standardMoneyCollection, standardMoneyCollectionTrackings, false, user);
            }
            window.ShowDialog();
            if (window.StandardMoneyCollection != null)
            {
                standardMoneyCollection = window.StandardMoneyCollection;
            }
            if (window.Trackings != null)
            {
                standardMoneyCollectionTrackings = window.Trackings;
            }
        }
    }
}
