﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using CoralBusinessLogics;
using System.IO;

namespace CoralUI
{
    /// <summary>
    /// Interaction logic for frmAddClient.xaml
    /// </summary>
    public partial class frmAddClient : Window
    {
        ClientsLogic clientsLogic = new ClientsLogic();
        AgentsLogic agentLogic = new AgentsLogic();
        TabItem tab;
        public ClientType clientType { get; set; }
        List<RelativeViewModel> relatives = new List<RelativeViewModel>();
        //Client clientToUpdate = null;
        public Client clientToUpdate { get; set; }
        public Client clientAdded { get; set; }
        public Client clientUpdated { get; set; }
        User user;

        ListViewSettings lvSettings = new ListViewSettings();
        string clientDirPath = null;
        private bool isChanges=false;
        bool confirmBeforeClosing = true;
        bool cancelClose = false;

        BrushConverter bc = new BrushConverter();

        //בנאי להוספת לקוח חדש
        public frmAddClient(TabItem tabSelected,User user)
        {
            InitializeComponent();
            this.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            tab = tabSelected;
            dpOpenDate.SelectedDate = DateTime.Now;
            this.user = user;
        }

        //בנאי לעדכון לקוח קיים
        public frmAddClient(Client client, User user)
        {
            InitializeComponent();
            this.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            clientToUpdate = client;
            this.user = user;
        }

        //טעינת החלון באופן שונה להוספה ולעדכון לקוח
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (user.IsClientProfilePermission==false)
            {
                dpGeneral.IsEnabled = false;
                dpOtherDetails.IsEnabled = false;
                dpRelatives.IsEnabled = false;
            }
            string strAppDir = System.AppDomain.CurrentDomain.BaseDirectory;
            string path = strAppDir + @"Coral Files\ClientsPath.txt";

            if (File.Exists(path))
            {
                clientDirPath = File.ReadAllText(path);
            }
            else
            {
                MessageBox.Show("לא נמצא תיקיית הלקוחות. נא בדוק בהגדרות ראשוניות את נתיב התיקייה");
                Close();
                return;
            }
            if (clientToUpdate != null)
            {
                clientType = clientsLogic.GetClientTypeForClient(clientToUpdate.ClientTypeID);
            }
            ClientTypeCbBinding();
            var types = cbClientType.Items;
            if (clientToUpdate == null)
            {
                foreach (var item in types)
                {
                    ClientType type = (ClientType)item;
                    if (type.ClientTypeName == tab.Header.ToString())
                    {
                        cbClientType.SelectedItem = item;
                        break;
                    }
                }
            }
            CategoryCbBinding();
            CbAgentsBinding();
            cbHobbyBinding();
            cbReferedByBinding();
            DataContext = relatives;
            //ממלא את פרטי הלקוח שרוצים לעדכן
            if (clientToUpdate != null)
            {
                cbClientType.IsEnabled = !clientsLogic.isActivePoliciesToClient(clientToUpdate);
                foreach (var item in types)
                {
                    ClientType type = (ClientType)item;
                    if (type.ClientTypeID == clientToUpdate.ClientTypeID)
                    {
                        cbClientType.SelectedItem = item;
                        break;
                    }
                }
                SelectCategory(clientToUpdate.CategoryID);
                dpOpenDate.SelectedDate = clientToUpdate.OpenDate;
                txtClientNumber.Text = clientToUpdate.ClientNumber;
                var principalAgents = cbPrincipalAgent.Items;
                foreach (var item in principalAgents)
                {
                    Agent agent = (Agent)item;
                    if (agent.AgentID == clientToUpdate.PrincipalAgentID)
                    {
                        cbPrincipalAgent.SelectedItem = item;
                        break;
                    }
                }
                var secundaryAgents = cbSecundaryAgent.Items;
                foreach (var item in secundaryAgents)
                {
                    Agent agent = (Agent)item;
                    if (agent.AgentID == clientToUpdate.SecundaryAgentID)
                    {
                        cbSecundaryAgent.SelectedItem = item;
                        break;
                    }
                }
                txtLastName.Text = clientToUpdate.LastName;
                txtFirstName.Text = clientToUpdate.FirstName;
                txtId.Text = clientToUpdate.IdNumber;
                //txtId.IsEnabled = false;
                dpBirthDate.SelectedDate = clientToUpdate.BirthDate;
                cbMaritalStatus.SelectedValue = clientToUpdate.MaritalStatus;
                txtCity.Text = clientToUpdate.City;
                txtStreet.Text = clientToUpdate.Street;
                txtHouseNumber.Text = clientToUpdate.HomeNumber;
                txtAptNumber.Text = clientToUpdate.ApartmentNumber;
                txtZipCode.Text = clientToUpdate.ZipCode;
                txtHomePhone.Text = clientToUpdate.PhoneHome;
                txtCellPhone.Text = clientToUpdate.CellPhone;
                txtWorkPhone.Text = clientToUpdate.PhoneWork;
                txtFax.Text = clientToUpdate.Fax;
                txtEmail.Text = clientToUpdate.Email;
                txtEmail2.Text = clientToUpdate.Email2;
                txtMailingAddress.Text = clientToUpdate.MailingAddress;
                txtCompanyName.Text = clientToUpdate.BusinessName;
                txtCompanyAddress.Text = clientToUpdate.CompanyAddress;
                txtWebsite.Text = clientToUpdate.Website;
                txtPassportNumber.Text = clientToUpdate.PassportNumber;
                cbKupatJolim.Text = clientToUpdate.KupatJolim;
                txtProfession.Text = clientToUpdate.Profession;
                cbWorkerType.Text = clientToUpdate.WorkerType;
                txtOcupation.Text = clientToUpdate.Ocupation;
                dpJobStartDate.SelectedDate = clientToUpdate.JobStartDate;
                if (clientToUpdate.DangerousHobbyID != null)
                {
                    SelectItemInCb((int)clientToUpdate.DangerousHobbyID);
                }
                txtLicenseNumber.Text = clientToUpdate.DrivingLicenseNumber;
                dpLicenseDate.SelectedDate = clientToUpdate.DrivingLicenseDate;
                chbDrivesInShabbat.IsChecked = clientToUpdate.DrivesInShabbat;
                chbSmokes.IsChecked = clientToUpdate.Smokes;
                chbIsNew.IsChecked = clientToUpdate.isNew;
                txtComments.Text = clientToUpdate.Remarks;
                List<ClientRelative> clientRelatives = clientsLogic.GetAllRelativesForClient(clientToUpdate);
                foreach (ClientRelative item in clientRelatives)
                {
                    string gender = "";
                    if (item.Relative.Gender == false)
                    {
                        gender = "נקבה";
                    }
                    else if (item.Relative.Gender == true)
                    {
                        gender = "זכר";
                    }
                    RelativeViewModel relativeFromDb = new RelativeViewModel() { Relationship = item.Relationship, LastName = item.Relative.LastName, FirstName = item.Relative.FirstName, IdNumber = item.Relative.IdNumber, BirthDate = item.Relative.BirthDate, LicenseNumber = item.Relative.LicenseNumber, LicenseDate = item.Relative.LicenseDate, Profession = item.Relative.Profession, Gender = gender, PassportNumber = item.Relative.PassportNumber };
                    relatives.Add(relativeFromDb);
                }
                DataGridRelativesBinding();
                var cbClientsItems = cbReferedBy.Items;
                foreach (var item in cbClientsItems)
                {
                    Client clientItem = (Client)item;
                    if (clientItem.ClientID == clientToUpdate.ReferedByClientID)
                    {
                        cbReferedBy.SelectedItem = item;
                        break;
                    }
                }
                var relationItems = cbReferedByRelation.Items;
                foreach (var item in relationItems)
                {
                    if (((ComboBoxItem)item).Content.ToString() == clientToUpdate.ReferedByRelation)
                    {
                        cbReferedByRelation.SelectedItem = item;
                        break;
                    }
                }
            }
        }

        private void cbReferedByBinding()
        {
            var clients = clientsLogic.GetAllClients();
            cbReferedBy.ItemsSource = clients.OrderBy(c => c.LastName);
        }


        //סנכרון של קומבובוקסים של הסוכנים עם בסיס הנתונים
        private void CbAgentsBinding()
        {
            cbPrincipalAgent.ItemsSource = agentLogic.GetActivePrincipalAgents();
            //if (cbPrincipalAgent.SelectedItem!=null)
            //{
            //    cbSecundaryAgent.ItemsSource = agentLogic.GetActiveSubAgentsByPrincipalAgent((Agent)cbPrincipalAgent.SelectedItem);                
            //}
        }

        //סנכרון של קומבו בוקס של סוגי לקוחות עם בסיס הנתונים
        private void ClientTypeCbBinding()
        {
            cbClientType.ItemsSource = clientsLogic.GetAllTypes();
            cbClientType.DisplayMemberPath = "ClientTypeName";
        }

        //סנכרון של קומבו בוקס של קטגוריות לקוחות עם בסיס הנתונים
        private void CategoryCbBinding()
        {
            cbCategory.ItemsSource = clientsLogic.GetActiveCategories();
            cbCategory.DisplayMemberPath = "CategoryName";
        }

        //פתיחת חךון עדכון טבלאות
        private void btnUpdateCategoriesTable_Click(object sender, RoutedEventArgs e)
        {
            int? selectedItemId = null;
            if (cbCategory.SelectedItem != null)
            {
                selectedItemId = ((Category)cbCategory.SelectedItem).CategoryID;
            }
            Category category = new Category();
            frmUpdateTable updateTable = new frmUpdateTable(category);
            updateTable.ShowDialog();
            CategoryCbBinding();
            if (updateTable.ItemAdded != null)
            {
                SelectCategory(((Category)updateTable.ItemAdded).CategoryID);
            }
            else if (selectedItemId != null)
            {
                SelectCategory((int)selectedItemId);
            }
        }

        private void SelectCategory(int categoryId)
        {
            var categories = cbCategory.Items;
            foreach (var item in categories)
            {
                Category category = (Category)item;
                if (category.CategoryID == categoryId)
                {
                    cbCategory.SelectedItem = item;
                    break;
                }
            }
        }

        //סוגר את החלון
        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        //שומר/מעדכן לקוח
        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            cbCategoryBorder.BorderBrush= (Brush)bc.ConvertFrom("#FF030F40");
            cbPrincipalAgentBorder.BorderBrush = (Brush)bc.ConvertFrom("#FF030F40");
            txtLastName.ClearValue(BorderBrushProperty);
            txtFirstName.ClearValue(BorderBrushProperty);
            txtId.ClearValue(BorderBrushProperty);
            if (InputNullValidation() == false)
            {
                cancelClose = true;
                MessageBox.Show("נא למלאות שדות חובה", "", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            bool drivesInShabbat = false;
            if (chbDrivesInShabbat.IsChecked == true)
            {
                drivesInShabbat = true;
            }
            bool smokes = false;
            if (chbSmokes.IsChecked == true)
            {
                smokes = true;
            }
            bool isNew = false;
            if (chbIsNew.IsChecked == true)
            {
                isNew = true;
            }
            Agent secundaryAgent = null;
            if (cbSecundaryAgent.SelectedItem != null)
            {
                secundaryAgent = (Agent)cbSecundaryAgent.SelectedItem;
            }
            string maritalStatus = null;
            if (cbMaritalStatus.SelectedItem != null)
            {
                maritalStatus = (cbMaritalStatus.SelectedItem as ComboBoxItem).Content.ToString();
            }
            string kupatJolim = null;
            if (cbKupatJolim.SelectedItem != null)
            {
                kupatJolim = (cbKupatJolim.SelectedItem as ComboBoxItem).Content.ToString();
            }
            else
            {
                kupatJolim = cbKupatJolim.Text;
            }
            string workerType = null;
            if (cbWorkerType.SelectedItem != null)
            {
                workerType = (cbWorkerType.SelectedItem as ComboBoxItem).Content.ToString();
            }
            else
            {
                workerType = cbWorkerType.Text;
            }
            DateTime? openDate = null;
            if (dpOpenDate.SelectedDate != null)
            {
                openDate = (DateTime)dpOpenDate.SelectedDate;
            }
            clientType = (ClientType)cbClientType.SelectedItem;
            int? hobbyId = null;
            if (cbHobby.SelectedItem != null)
            {
                hobbyId = ((DangerousHobby)cbHobby.SelectedItem).DangerousHobbyID;
            }
            int? referedById = null;
            if (cbReferedBy.SelectedItem != null)
            {
                referedById = ((Client)cbReferedBy.SelectedItem).ClientID;
            }
            string referedByRelation = "";
            if (cbReferedByRelation.SelectedItem != null)
            {
                referedByRelation = ((ComboBoxItem)cbReferedByRelation.SelectedItem).Content.ToString();
            }
            int clientId=0;
            if (clientToUpdate == null)
            //הוספת לקוח חדש
            {
                //בודק שלת.ז יש מס' בלבד
                foreach (char c in txtId.Text)
                {
                    if (c < '0' || c > '9')
                    {
                        cancelClose = true;
                        MessageBox.Show("ת.ז חייבת להיות בעלת מספרים בלבד", "שגיאה", MessageBoxButton.OK, MessageBoxImage.Error);
                        txtId.Clear();
                        return;
                    }
                }
                try
                {
                    //הוספת פרטי לקוח
                    clientAdded = clientsLogic.InsertClient(clientType, (Category)cbCategory.SelectedItem, openDate, txtClientNumber.Text, (Agent)cbPrincipalAgent.SelectedItem, secundaryAgent, txtLastName.Text, txtFirstName.Text, txtId.Text, dpBirthDate.SelectedDate, maritalStatus, txtCity.Text, txtStreet.Text, txtHouseNumber.Text, txtAptNumber.Text, txtZipCode.Text, txtHomePhone.Text, txtCellPhone.Text, txtWorkPhone.Text, txtFax.Text, txtEmail.Text, txtEmail2.Text, txtMailingAddress.Text, txtCompanyName.Text, txtCompanyAddress.Text, txtWebsite.Text, txtPassportNumber.Text, kupatJolim, txtProfession.Text, workerType, txtOcupation.Text, dpJobStartDate.SelectedDate, hobbyId, txtLicenseNumber.Text, dpLicenseDate.SelectedDate, drivesInShabbat, smokes, txtComments.Text, referedById, referedByRelation, isNew);
                    SaveUnsavedRelatives();
                    if (relatives.Count > 0)
                    {
                        //הוספת בני משפחה ללקוח
                        clientsLogic.InsertRelativesToClient(relatives, clientAdded);
                    }
                    clientId = clientAdded.ClientID;
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "שגיאה", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            else
            {
                try
                {
                    clientUpdated = clientsLogic.UpdateClient(clientToUpdate,clientType, (Category)cbCategory.SelectedItem, openDate, txtClientNumber.Text, (Agent)cbPrincipalAgent.SelectedItem, secundaryAgent, txtLastName.Text, txtFirstName.Text, txtId.Text, dpBirthDate.SelectedDate, maritalStatus, txtCity.Text, txtStreet.Text, txtHouseNumber.Text, txtAptNumber.Text, txtZipCode.Text, txtHomePhone.Text, txtCellPhone.Text, txtWorkPhone.Text, txtFax.Text, txtEmail.Text, txtEmail2.Text, txtMailingAddress.Text, txtCompanyName.Text, txtCompanyAddress.Text, txtWebsite.Text, txtPassportNumber.Text, kupatJolim, txtProfession.Text, workerType, txtOcupation.Text, dpJobStartDate.SelectedDate, hobbyId, txtLicenseNumber.Text, dpLicenseDate.SelectedDate, drivesInShabbat, smokes, txtComments.Text, referedById, referedByRelation, isNew);
                    SaveUnsavedRelatives();
                    if (relatives.Count > 0)
                    {
                        clientsLogic.InsertRelativesToClient(relatives, clientUpdated);
                    }
                    clientId = clientUpdated.ClientID;
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "שגיאה", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            
            string path = clientDirPath + @"\" + clientId+ @"\כללי";                   

            if (!Directory.Exists(@path))
            {
                Directory.CreateDirectory(@path);
            }

            if (sender is Button)
            {
                Button btn = (Button)sender;
                if (btn.Name == "btnSave")
                {
                    confirmBeforeClosing = false;
                    Close();
                }
            }
        }

        private void SaveUnsavedRelatives()
        {
            if (cbRelation.SelectedItem!=null|| txtRelativeLastName.Text!=""|| txtRelativeFirstName.Text != "" || txtRelativeId.Text != "" || dpRelativeBirthDate.SelectedDate!=null|| txtRelativeLisenceNumber.Text != "" || dpRelativeLicenseDate.SelectedDate!=null|| cbGender.SelectedItem!=null|| txtRelativePassportNumber.Text != "" || txtRelativeProfession.Text != "")
            {
                btnAddRelative_Click(this, new RoutedEventArgs());
            }
        }

        //בודק ששדות החובה הם לא ריקים
        public bool InputNullValidation()
        {
            bool isValid = true;
            if (cbCategory.SelectedItem == null)
            {
                cbCategoryBorder.BorderBrush = Brushes.Red;
                //cbCategory.Background = Brushes.Red;
                isValid = false;
            }
            if (cbPrincipalAgent.SelectedItem == null)
            {
                cbPrincipalAgentBorder.BorderBrush = Brushes.Red;
                isValid = false;
            }
            if (txtLastName.Text == "")
            {
                txtLastName.BorderBrush = Brushes.Red;
                isValid = false;
            }
            if (txtFirstName.Text == "")
            {
                txtFirstName.BorderBrush = Brushes.Red;
                isValid = false;
            }
            if (txtId.Text == "")
            {
                txtId.BorderBrush = Brushes.Red;
                isValid = false;
            }
            return isValid;
        }

        //מוסיף/מעדכן בן משפחה 
        private void btnAddRelative_Click(object sender, RoutedEventArgs e)
        {
            cbRelation.ClearValue(BorderBrushProperty);
            txtRelativeLastName.ClearValue(BorderBrushProperty);
            txtRelativeFirstName.ClearValue(BorderBrushProperty);
            txtRelativeId.ClearValue(BorderBrushProperty);
            if (!RelativeInputNullValidation())
            {
                if (sender is Window)
                {
                    MessageBox.Show("שים לב, פרטי בן המשפחה לא עודכנו במערכת", "מידע", MessageBoxButton.OK, MessageBoxImage.Information);
                }
                return;
            }
            string gender = "";
            if (cbGender.SelectedItem != null)
            {
                gender = (cbGender.SelectedItem as ComboBoxItem).Content.ToString();
            }
            if (txtBtnAddRelative.Text != "עדכן בן משפחה")
            {
                foreach (char c in txtRelativeId.Text)
                {
                    if (c < '0' || c > '9')
                    {
                        MessageBox.Show("ת.ז חייבת להיות בעלת מספרים בלבד", "שגיאה", MessageBoxButton.OK, MessageBoxImage.Error);
                        txtRelativeId.Clear();
                        return;
                    }
                }
                RelativeViewModel relative = new RelativeViewModel() { Relationship = (cbRelation.SelectedItem as ComboBoxItem).Content.ToString(), LastName = txtRelativeLastName.Text, FirstName = txtRelativeFirstName.Text, IdNumber = txtRelativeId.Text, BirthDate = dpRelativeBirthDate.SelectedDate, LicenseNumber = txtRelativeLisenceNumber.Text, LicenseDate = dpRelativeLicenseDate.SelectedDate, Profession = txtRelativeProfession.Text, Gender = gender, PassportNumber = txtRelativePassportNumber.Text };
                relatives.Add(relative);
                DataGridRelativesBinding();
            }
            else
            {
                RelativeViewModel relativeToUpdate = relatives.FirstOrDefault(r => r.IdNumber == ((RelativeViewModel)dgRelatives.SelectedItem).IdNumber);
                relativeToUpdate.Relationship = (cbRelation.SelectedItem as ComboBoxItem).Content.ToString();
                relativeToUpdate.LastName = txtRelativeLastName.Text;
                relativeToUpdate.FirstName = txtRelativeFirstName.Text;
                relativeToUpdate.BirthDate = dpRelativeBirthDate.SelectedDate;
                relativeToUpdate.LicenseNumber = txtRelativeLisenceNumber.Text;
                relativeToUpdate.LicenseDate = dpRelativeLicenseDate.SelectedDate;
                relativeToUpdate.Profession = txtRelativeProfession.Text;
                relativeToUpdate.Gender = gender;
                relativeToUpdate.PassportNumber = txtRelativePassportNumber.Text;
                DataGridRelativesBinding();
            }
            CleanRelativeDetails();
            dgRelatives.UnselectAll();
        }

        //בודק ששדות החובה אינם ריקים
        private bool RelativeInputNullValidation()
        {
            bool isRelativeValid = true;
            if (cbRelation.SelectedItem == null)
            {
                cbRelation.BorderBrush = Brushes.Red;
                isRelativeValid = false;
            }
            if (txtRelativeLastName.Text == "")
            {
                txtRelativeLastName.BorderBrush = Brushes.Red;
                isRelativeValid = false;
            }
            if (txtRelativeFirstName.Text == "")
            {
                txtRelativeFirstName.BorderBrush = Brushes.Red;
                isRelativeValid = false;
            }
            if (txtRelativeId.Text == "")
            {
                txtRelativeId.BorderBrush = Brushes.Red;
                isRelativeValid = false;
            }
            return isRelativeValid;
        }

        //מנקה את פרטי הבן משפחה כשהוא לא מסומן 
        private void CleanRelativeDetails()
        {
            txtBtnAddRelative.Text = "הוסף בן משפחה";
            var addUriSource = new Uri(@"/IconsMind/Add_24px.png", UriKind.Relative);
            imgBtnAddRelative.Source = new BitmapImage(addUriSource);
            cbRelation.SelectedItem = null;
            txtRelativeLastName.Clear();
            txtRelativeFirstName.Clear();
            txtRelativeId.Clear();
            txtRelativeId.IsEnabled = true;
            dpRelativeBirthDate.SelectedDate = null;
            txtRelativeLisenceNumber.Clear();
            dpRelativeLicenseDate.SelectedDate = null;
            txtRelativeProfession.Clear();
            cbGender.SelectedItem = null;
            txtRelativePassportNumber.Clear();
        }

        //מסנכרן את טבלת הבני משפחה עם ימה
        private void DataGridRelativesBinding()
        {
            dgRelatives.ItemsSource = relatives.OrderBy(r => r.LastName);
            lvSettings.AutoSizeColumns(dgRelatives.View);

        }


        private void dgRelatives_MouseDown(object sender, MouseButtonEventArgs e)
        {
            dgRelatives.UnselectAll();
        }

        //ממלא/מוחק את פרטי הלקוח לפי שינוים בחירה בטבלת הבני משפחה
        private void dgRelatives_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (dgRelatives.SelectedItem == null)
            {
                CleanRelativeDetails();
                return;
            }
            txtBtnAddRelative.Text = "עדכן בן משפחה";
            var updateUriSource = new Uri(@"/IconsMind/Edit_24px.png", UriKind.Relative);
            imgBtnAddRelative.Source = new BitmapImage(updateUriSource);
            RelativeViewModel relativeSelected = (RelativeViewModel)dgRelatives.SelectedItem;
            cbRelation.SelectedValue = relativeSelected.Relationship;
            txtRelativeLastName.Text = relativeSelected.LastName;
            txtRelativeFirstName.Text = relativeSelected.FirstName;
            txtRelativeId.Text = relativeSelected.IdNumber;
            txtRelativeId.IsEnabled = false;
            dpRelativeBirthDate.SelectedDate = relativeSelected.BirthDate;
            txtRelativeLisenceNumber.Text = relativeSelected.LicenseNumber;
            dpRelativeLicenseDate.SelectedDate = relativeSelected.LicenseDate;
            txtRelativeProfession.Text = relativeSelected.Profession;
            cbGender.SelectedValue = relativeSelected.Gender;
            txtRelativePassportNumber.Text = relativeSelected.PassportNumber;
        }

        private void txtEmail_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (txtEmail.Text != "")
            {
                txtEmail2.IsEnabled = true;
            }
            else
            {
                txtEmail2.IsEnabled = false;
            }
        }

        private void cbPrincipalAgent_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cbPrincipalAgent.SelectedItem != null)
            {
                cbSecundaryAgent.ItemsSource = agentLogic.GetActiveSubAgentsByPrincipalAgent((Agent)cbPrincipalAgent.SelectedItem);
            }
        }

        private void btnUpdateHobbiesTable_Click(object sender, RoutedEventArgs e)
        {
            int? selectedItemId = null;
            if (cbHobby.SelectedItem != null)
            {
                selectedItemId = ((DangerousHobby)cbHobby.SelectedItem).DangerousHobbyID;
            }
            frmUpdateTable updateTable = new frmUpdateTable(new DangerousHobby());
            updateTable.ShowDialog();
            cbHobbyBinding();
            if (updateTable.ItemAdded != null)
            {
                SelectItemInCb(((DangerousHobby)updateTable.ItemAdded).DangerousHobbyID);
            }
            else if (selectedItemId != null)
            {
                SelectItemInCb((int)selectedItemId);
            }
        }

        private void SelectItemInCb(int id)
        {
            var items = cbHobby.Items;
            foreach (var item in items)
            {
                DangerousHobby parsedItem = (DangerousHobby)item;
                if (parsedItem.DangerousHobbyID == id)
                {
                    cbHobby.SelectedItem = item;
                    break;
                }
            }
        }

        private void cbHobbyBinding()
        {
            cbHobby.ItemsSource = clientsLogic.GetActiveHobbies();
            cbHobby.DisplayMemberPath = "HobbyName";
        }

        private void cbClientType_GotFocus(object sender, RoutedEventArgs e)
        {
            isChanges = true;
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (confirmBeforeClosing && isChanges)
            {
                if (MessageBox.Show("האם ברצונך לשמור שינוים שבצעת", "אישור", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    btnSave_Click(sender, new RoutedEventArgs());
                    if (cancelClose)
                    {
                        e.Cancel = true;
                        cancelClose = false;
                    }
                }
            }
        }
    }
}
