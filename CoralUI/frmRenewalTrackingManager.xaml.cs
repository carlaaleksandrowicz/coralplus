﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using CoralBusinessLogics;

namespace CoralUI
{
    /// <summary>
    /// Interaction logic for frmRenewalTrackingManager.xaml
    /// </summary>
    public partial class frmRenewalTrackingManager : Window
    {
        ElementaryPolicy elementaryPolicy;
        List<ElementaryPolicy> elementaryPolicies;
        ForeingWorkersPolicy foreingWorkersPolicy;
        List<ForeingWorkersPolicy> foreingWorkersPolicies;
        public static event EventHandler TrackingsCountChange;

        User user;
        RenewalsLogic renewalLogic = new RenewalsLogic();
        ListViewSettings lv = new ListViewSettings();
        public frmRenewalTrackingManager(ElementaryPolicy policy, List<ElementaryPolicy> policies, User user)
        {
            InitializeComponent();
            this.elementaryPolicy = policy;
            this.elementaryPolicies = policies;
            this.user = user;
        }
        public frmRenewalTrackingManager(ForeingWorkersPolicy policy, List<ForeingWorkersPolicy> policies, User user)
        {
            InitializeComponent();
            this.foreingWorkersPolicy = policy;
            this.foreingWorkersPolicies = policies;
            this.user = user;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            dgTrackingBinding();
        }

        private void dgTrackingBinding()
        {
            if (elementaryPolicy != null)
            {
                dgTracking.ItemsSource = renewalLogic.GetTrackingsByRenewalId(elementaryPolicy.ElementaryPolicyID);
            }
            else if (foreingWorkersPolicy != null)
            {
                dgTracking.ItemsSource = renewalLogic.GetForeingWorkersRenewalTrackingsByPolicyId(foreingWorkersPolicy.ForeingWorkersPolicyID);
            }
            lv.AutoSizeColumns(dgTracking.View);
        }

        private void btnAddTracking_Click(object sender, RoutedEventArgs e)
        {
            frmTracking newTrackingWindow=null;
            if (elementaryPolicy!=null)
            {
                newTrackingWindow = new frmTracking(elementaryPolicy.Client, elementaryPolicy, user);
            }
            else
            {
                newTrackingWindow = new frmTracking(foreingWorkersPolicy.Client, foreingWorkersPolicy, user);
            }
            newTrackingWindow.ShowDialog();
            if (newTrackingWindow.trackingContent != null)
            {
                DateTime date = DateTime.Now;
                if (elementaryPolicies!=null)
                {
                    foreach (var item in elementaryPolicies)
                    {
                        renewalLogic.InsertRenewalTracking(item.ElementaryPolicyID, user.UserID, date, newTrackingWindow.trackingContent);
                    }
                }
                else if (foreingWorkersPolicies!=null)
                {
                    foreach (var item in foreingWorkersPolicies)
                    {
                        renewalLogic.InsertForeingWorkersRenewalTracking(item.ForeingWorkersPolicyID, user.UserID, date, newTrackingWindow.trackingContent);
                    }
                }
                dgTrackingBinding();
                OnTrackingdCountChange();
            }
        }

        private void OnTrackingdCountChange()
        {
            if (TrackingsCountChange != null)
            {
                TrackingsCountChange(this, new EventArgs());
            }
        }

        private void btnDeleteTracking_Click(object sender, RoutedEventArgs e)
        {
            if (dgTracking.SelectedItem == null)
            {
                MessageBox.Show("נא לסמן מעקב", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            if (elementaryPolicy!=null)
            {
                renewalLogic.DeleteTracking((RenewalTracking)dgTracking.SelectedItem);
            }
            else if (foreingWorkersPolicy!=null)
            {
                renewalLogic.DeleteForeingWorkersRenewalTracking((ForeingWorkersRenewalTracking)dgTracking.SelectedItem);
            }
            dgTrackingBinding();
            OnTrackingdCountChange();

        }

        private void btnUpdateTracking_Click(object sender, RoutedEventArgs e)
        {
            if (dgTracking.SelectedItem == null)
            {
                MessageBox.Show("נא לסמן מעקב", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            frmTracking updateTrackingWindow = null;
            if (elementaryPolicy!=null)
            {
                updateTrackingWindow = new frmTracking((RenewalTracking)dgTracking.SelectedItem, elementaryPolicy.Client, elementaryPolicy, user);
            updateTrackingWindow.ShowDialog();
            renewalLogic.UpdateRenewalTracking((RenewalTracking)dgTracking.SelectedItem, updateTrackingWindow.trackingContent);

            }
            else if (foreingWorkersPolicy!=null)
            {
                updateTrackingWindow = new frmTracking((ForeingWorkersRenewalTracking)dgTracking.SelectedItem, foreingWorkersPolicy.Client, foreingWorkersPolicy, user);
                updateTrackingWindow.ShowDialog();
                renewalLogic.UpdateForeingWorkersRenewalTracking((ForeingWorkersRenewalTracking)dgTracking.SelectedItem, updateTrackingWindow.trackingContent);

            }
            dgTrackingBinding();
            dgTracking.UnselectAll();
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }


    }
}
