﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using CoralBusinessLogics;

namespace CoralUI
{
    /// <summary>
    /// Interaction logic for frmCoverage.xaml
    /// </summary>
    public partial class frmCoverage : Window
    {
        ElementaryInsuranceType insurance = new ElementaryInsuranceType();
        CoveragesLogic coverageLogic = new CoveragesLogic();
        InputsValidations validations = new InputsValidations();
        List<TextBox> errorList = null;
        private LifeInsuranceType lifeInsuranceType;
        private LifeCoverage coverageToUpdate;
        private bool isChanges=false;
        bool confirmBeforeClosing = true;
        bool cancelClose = false;
        
        //ElementaryCoverage coverageToUpdate=null;

        public ElementaryCoverage NewCoverage { get; set; }
        public frmCoverage(ElementaryInsuranceType insuranceType)
        {
            InitializeComponent();
            this.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            insurance = insuranceType;
            cbCoverageTypeBinding();
        }

        public frmCoverage(ElementaryInsuranceType insuranceType, ElementaryCoverage coverage)
        {
            InitializeComponent();
            this.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            NewCoverage = coverage;
            insurance = insuranceType;
            cbCoverageTypeBinding();            
            SelectItemInCb(NewCoverage.ElementaryCoverageTypeID);
            txtCoverageCode.Text = NewCoverage.ElementaryCoverageType.ElementaryCoverageCode;
            if (NewCoverage.CoverageAmount!=null)
            {
                txtCoverageAmount.Text = ((decimal)NewCoverage.CoverageAmount).ToString("0.##");
            }
            if (NewCoverage.Porcentage!=null)
            {
                txtCoverageRate.Text = ((decimal)NewCoverage.Porcentage).ToString("0.##");
            }
            if (NewCoverage.premium!=null)
            {
                txtPremium.Text = ((decimal)NewCoverage.premium).ToString("0.##");
            }
            txtComments.Text = NewCoverage.Comments;
        }


        private void cbCoverageTypeBinding()
        {
            cbCoverageType.ItemsSource = coverageLogic.GetAllActiveElementaryCoverageTypesByIndustry(insurance.InsuranceIndustryID);
            cbCoverageType.DisplayMemberPath = "ElementaryCoverageTypeName";
        }

        private void btnUpdateTable_Click(object sender, RoutedEventArgs e)
        {
            int? selectedItemId = null;
            if (cbCoverageType.SelectedItem != null)
            {
                selectedItemId = ((ElementaryCoverageType)cbCoverageType.SelectedItem).ElementaryCoverageTypeID;
            }
            ElementaryCoverageType coverageType = new ElementaryCoverageType() { InsuranceIndustryID = insurance.InsuranceIndustryID };
            frmUpdateTable updateElementaryCoverageTypesTable = new frmUpdateTable(coverageType);
            updateElementaryCoverageTypesTable.ShowDialog();
            cbCoverageTypeBinding();
            if (updateElementaryCoverageTypesTable.ItemAdded != null)
            {
                SelectItemInCb(((ElementaryCoverageType)updateElementaryCoverageTypesTable.ItemAdded).ElementaryCoverageTypeID);
            }
            else if (selectedItemId != null)
            {
                SelectItemInCb((int)selectedItemId);
            }
        }

        private void SelectItemInCb(int id)
        {
            var items = cbCoverageType.Items;
            foreach (var item in items)
            {
                ElementaryCoverageType parsedItem = (ElementaryCoverageType)item;
                if (parsedItem.ElementaryCoverageTypeID == id)
                {
                    cbCoverageType.SelectedItem = item;
                    break;
                }
            }
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            if (errorList != null && errorList.Count > 0)
            {
                foreach (TextBox item in errorList)
                {
                    item.ClearValue(BorderBrushProperty);
                }
            }
            if (cbCoverageType.SelectedItem == null)
            {
                cancelClose = true;
                MessageBox.Show("סוג כיסוי הינו שדה חובה", "", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            errorList = validations.OnlyNumbersValidation(new TextBox[] { txtCoverageAmount, txtCoverageRate, txtPremium });
            if (errorList.Count > 0)
            {
                cancelClose = true;
                MessageBox.Show("נא להזין מספרים או נקודה בלבד", "", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            NewCoverage = new ElementaryCoverage();
            NewCoverage.ElementaryCoverageType = (ElementaryCoverageType)cbCoverageType.SelectedItem;
            NewCoverage.ElementaryCoverageTypeID = ((ElementaryCoverageType)cbCoverageType.SelectedItem).ElementaryCoverageTypeID;
            if (txtCoverageAmount.Text != "")
            {
                NewCoverage.CoverageAmount = decimal.Parse(txtCoverageAmount.Text);

            }
            if (txtCoverageRate.Text != "")
            {
                NewCoverage.Porcentage = decimal.Parse(txtCoverageRate.Text);

            }
            if (txtPremium.Text != "")
            {
                NewCoverage.premium = decimal.Parse(txtPremium.Text);
            }
            NewCoverage.Comments = txtComments.Text;
            NewCoverage.Date = DateTime.Now;

            if (sender is Button)
            {
                Button btn = (Button)sender;
                if (btn.Name == "btnSave")
                {
                    confirmBeforeClosing = false;
                    Close();
                }
            }
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void cbCoverageType_GotFocus(object sender, RoutedEventArgs e)
        {
            isChanges = true;
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (confirmBeforeClosing && isChanges)
            {
                if (MessageBox.Show("האם ברצונך לשמור שינוים שבצעת", "אישור", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    btnSave_Click(sender, new RoutedEventArgs());
                    if (cancelClose)
                    {
                        e.Cancel = true;
                        cancelClose = false;
                    }
                }
            }
        }
    }
}
