﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using GdPicture10;
using System.Windows.Interop;
using System.IO;

namespace CoralUI
{
    /// <summary>
    /// Interaction logic for frmScanSettings.xaml
    /// </summary>
    public partial class frmScanSettings : Window
    {
        public GdPictureImaging gdPicture = new GdPictureImaging();
        string strAppDir = System.AppDomain.CurrentDomain.BaseDirectory;
        bool confirmBeforeClosing = true;
        private bool isChanges = false;


        public frmScanSettings()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            cbScanSourceBinding();
            string settingsPath = strAppDir + @"Coral Files\";
            if (Directory.Exists(settingsPath))
            {
                SelectSource(settingsPath);
                SelectResolution(settingsPath);
                SelectFileType(settingsPath);
                SelectColor(settingsPath);
                SelectOtherSettings(settingsPath);
            }
        }

        private void SelectOtherSettings(string settingsPath)
        {
            if (!File.Exists(settingsPath + "ScanDisplayUI.txt"))
            {
                return;
            }
            string displayUI= File.ReadAllText(settingsPath + "ScanDisplayUI.txt");
            if (displayUI=="true")
            {
                chbDisplayUI.IsChecked = true;
            }
            else
            {
                chbDisplayUI.IsChecked = false;
            }

            if (!File.Exists(settingsPath + "ScanDuplex.txt"))
            {
                return;
            }
            string duplex = File.ReadAllText(settingsPath + "ScanDuplex.txt");
            if (duplex == "true")
            {
                chbEnabledDuplex.IsChecked = true;
            }
            else
            {
                chbEnabledDuplex.IsChecked = false;
            }
        }

        private void SelectColor(string settingsPath)
        {
            if (!File.Exists(settingsPath + "ScanColor.txt"))
            {
                return;
            }
            string color = File.ReadAllText(settingsPath + "ScanColor.txt");
            if (color == "color")
            {
                rbColor.IsChecked = true;
            }
            else if (color=="gray")
            {
                rbGrayScale.IsChecked = true;
            }
            else
            {
                rbBlackAndWhite.IsChecked = true;
            }
        }

        private void SelectFileType(string settingsPath)
        {
            if (!File.Exists(settingsPath + "ScanFileType.txt"))
            {
                return;
            }
            string fileType= File.ReadAllText(settingsPath + "ScanFileType.txt");
            if (fileType=="tiff")
            {
                rbTIFF.IsChecked = true;
            }
            else
            {
                rbPDF.IsChecked = true;
            }
        }

        private void SelectResolution(string settingsPath)
        {
            if (!File.Exists(settingsPath + "ScanResolution.txt"))
            {
                return;
            }
            string resolution = File.ReadAllText(settingsPath + "ScanResolution.txt");
            if (resolution != "")
            {
                var items = cbResolution.Items;
                foreach (var item in items)
                {
                    if (resolution == ((ComboBoxItem)item).Content.ToString())
                    {
                        cbResolution.SelectedItem = item;
                        break;
                    }
                }
            }
        }

        private void SelectSource(string settingsPath)
        {
            if (!File.Exists(settingsPath + "ScanSource.txt"))
            {
                return;
            }
            string source = File.ReadAllText(settingsPath + "ScanSource.txt");
            if (source!="")
            {
                var items = cbScanSource.ItemsSource;
                foreach (var item in items)
                {
                    if (source==item.ToString())
                    {
                        cbScanSource.SelectedItem = item;
                        break;
                    }
                }
            }
        }

        private void cbScanSourceBinding()
        {
            int sourcesNumber = gdPicture.TwainGetSourceCount(new WindowInteropHelper(this).Handle);
            List<string> sourcesName = new List<string>();
            for (int i = 1; i <= sourcesNumber; i++)
            {
                sourcesName.Add(gdPicture.TwainGetSourceName(new WindowInteropHelper(this).Handle, i));
            }
            cbScanSource.ItemsSource = sourcesName.ToList();
            cbScanSource.SelectedIndex = 0;
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            string scanSource = "";
            if (cbScanSource.SelectedItem != null)
            {
                scanSource = cbScanSource.SelectedItem.ToString();
            }
            string scanSourcePath = strAppDir + @"Coral Files\ScanSource.txt";
            File.WriteAllText(scanSourcePath, scanSource);

            string scanResolution = ((ComboBoxItem)cbResolution.SelectedItem).Content.ToString();
            string scanResolutionPath = strAppDir + @"Coral Files\ScanResolution.txt";
            File.WriteAllText(scanResolutionPath, scanResolution);

            string fileType = "pdf";
            if (rbTIFF.IsChecked == true)
            {
                fileType = "tiff";
            }
            string fileTypePath = strAppDir + @"Coral Files\ScanFileType.txt";
            File.WriteAllText(fileTypePath, fileType);

            string scanColor = "blackAndWhite";
            if (rbColor.IsChecked == true)
            {
                scanColor = "color";
            }
            else if (rbGrayScale.IsChecked == true)
            {
                scanColor = "gray";
            }
            string colorPath = strAppDir + @"Coral Files\ScanColor.txt";
            File.WriteAllText(colorPath, scanColor);

            string displayUI = "false";
            if (chbDisplayUI.IsChecked == true)
            {
                displayUI = "true";
            }
            string displayUIPath = strAppDir + @"Coral Files\ScanDisplayUI.txt";
            File.WriteAllText(displayUIPath, displayUI);

            string duplex = "false";
            if (chbEnabledDuplex.IsChecked == true)
            {
                duplex = "true";
            }
            string duplexPath = strAppDir + @"Coral Files\ScanDuplex.txt";
            File.WriteAllText(duplexPath, duplex);

            if (sender is Button)
            {
                Button btn = (Button)sender;
                if (btn.Name == "btnSave")
                {
                    confirmBeforeClosing = false;
                    Close();
                }
            }
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (confirmBeforeClosing && isChanges)
            {
                if (MessageBox.Show("האם ברצונך לשמור שינוים שבצעת", "אישור", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    btnSave_Click(sender, new RoutedEventArgs());
                    //if (cancelClose)
                    //{
                    //    e.Cancel = true;
                    //    cancelClose = false;
                    //}
                }
            }
        }

        private void StackPanel_GotFocus(object sender, RoutedEventArgs e)
        {
            isChanges = true;
        }
    }
}
