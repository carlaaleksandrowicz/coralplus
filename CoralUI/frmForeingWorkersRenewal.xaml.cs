﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using CoralBusinessLogics;

namespace CoralUI
{
    /// <summary>
    /// Interaction logic for frmForeingWorkersRenewal.xaml
    /// </summary>
    public partial class frmForeingWorkersRenewal : Window
    {
        RenewalsLogic renewalLogic = new RenewalsLogic();
        Client client;
        ForeingWorkersPolicy policy;
        InsurancesLogic insuranceLogic = new InsurancesLogic();
        User user;
        ForeingWorkersLogic policiesLogics = new ForeingWorkersLogic();
        ListViewSettings lvSettings = new ListViewSettings();
        InputsValidations validation = new InputsValidations();
        frmAddClient clientDetailsWindow;
        frmRenewalTrackingManager trackingWindow;
        List<ForeingWorkersPolicy> tempList = new List<ForeingWorkersPolicy>();
        ClientsLogic clientLogic = new ClientsLogic();
        public frmForeingWorkersRenewal(Client clientRenewal, ForeingWorkersPolicy policyToRenew, User userAccount)
        {
            InitializeComponent();
            client = clientRenewal;
            user = userAccount;
            policy = policyToRenew;
            tempList.Add(policy);
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (policy.RenewalStatus.RenewalStatusName == "חודש")
            {
                cbRenewalStatus.IsEnabled = false;
                gbQuotes.IsEnabled = false;
                btnPolicyRenewal.IsEnabled = false;
                btnDeleteQuote.IsEnabled = false;

                //spQuotesButtons.IsEnabled = false;
                //spTrackingButtons.IsEnabled = false;
            }
            else if (policy.RenewalStatus.RenewalStatusName == "לא טופל")
            {
                string status = "פתוח לעבודה";
                policy.RenewalStatusID = policiesLogics.UpdatePolicyRenewalStatus(policy, status);
            }
            cbRenewalStatusBinding();
            cbCompanyBinding();
            SetClientDetails();
            cbAdditionBinding();
            txtPolicyNumber.Text = policy.PolicyNumber;
            
            txtInsuranceCompany.Text = policy.Company.CompanyName;
            
            if (policy.Country != null)
            {
                txtInsuredBirthCountry.Text = policy.Country.CountryName;
            }
            if (policy.InsuredBirthDate != null)
            {
                txtInsuredBirthDate.Text = ((DateTime)policy.InsuredBirthDate).ToString("dd/MM/yyyy");
            }
            txtName.Text = string.Format("{2} {1} {0}",policy.InsuredFirstName,policy.InsuredLastName,policy.InsuredMiddleName);
            
            txtInsuredPassportNumber.Text = policy.InsuredPassportNumber;
            dgQuotesBinding();
            frmRenewalTrackingManager.TrackingsCountChange += new EventHandler(renewalsWindow_TrackingsCountChange);

            lblTrackingCountBinding();
        }
        private void SetClientDetails()
        {
            txtClientName.Text = client.FirstName + " " + client.LastName;
            txtEmail.Text = client.Email;
            txtPhone.Text = client.PhoneHome;
            txtCellPhone.Text = client.CellPhone;
            string clientDetails;
            if (client.CellPhone != null && client.CellPhone != "")
            {
                clientDetails = string.Format("{0} {1}  ת.ז: {2} נייד: {3}", client.FirstName, client.LastName, client.IdNumber, client.CellPhone);
            }
            else
            {
                clientDetails = string.Format("{0} {1}  ת.ז: {2}", client.FirstName, client.LastName, client.IdNumber);
            }
            lblClientNameAndId.Content = clientDetails;
        }

        void renewalsWindow_TrackingsCountChange(object sender, EventArgs e)
        {
            lblTrackingCountBinding();
        }


        private void lblTrackingCountBinding()
        {
            lblTrackingCount.Content = (renewalLogic.GetForeingWorkersTrackingsCountByPolicyId(policy.ForeingWorkersPolicyID)).ToString();
        }

        private void cbAdditionBinding()
        {
            List<ForeingWorkersPolicy> policies = policiesLogics.GetPoliciesByPolicyNumberAndIndustry(policy.PolicyNumber, policy.ForeingWorkersIndustryID);
            cbAddition.ItemsSource = policies;
            cbAddition.DisplayMemberPath = "Addition";
            SelectAddition();
            decimal? totalPremium = policies.Sum(p => p.TotalPremium);
            lblTotalPremium.Content = string.Format("{0} ש''ח", ((decimal)totalPremium).ToString("0.##"));
        }

        private void SelectAddition()
        {
            var items = cbAddition.Items;
            foreach (var item in items)
            {
                ForeingWorkersPolicy tempItem = (ForeingWorkersPolicy)item;
                if (tempItem.ForeingWorkersPolicyID == policy.ForeingWorkersPolicyID)
                {
                    cbAddition.SelectedItem = item;
                    break;
                }
            }
        }

        private void cbCompanyBinding()
        {
            cbCompany.ItemsSource = insuranceLogic.GetActiveCompaniesByInsurance("עובדים זרים");
            cbCompany.DisplayMemberPath = "Company.CompanyName";
        }

        private void cbRenewalStatusBinding()
        {
            cbRenewalStatus.ItemsSource = renewalLogic.GetAllActiveRenewalStatuses();
            cbRenewalStatus.DisplayMemberPath = "RenewalStatusName";
            string statusName;
            statusName = (renewalLogic.GetRenewalStatusByRenewalStatusId((int)policy.RenewalStatusID)).RenewalStatusName;
            SelectStatusInCb(statusName);
        }

        private void SelectStatusInCb(string statusName)
        {
            var renewalStatuses = cbRenewalStatus.Items;
            foreach (var item in renewalStatuses)
            {
                if (((RenewalStatus)item).RenewalStatusName == statusName)
                {
                    cbRenewalStatus.SelectedItem = item;
                    break;
                }
            }
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void btnAddQuote_Click(object sender, RoutedEventArgs e)
        {
            if (txtNewPremia.Text == "" || cbCompany.SelectedItem == null || txtNewPolicyNumber.Text == "")
            {
                MessageBox.Show("נא למלאות את כל שדות ההצעה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            renewalLogic.InsertForeingWorkersRenewalQuote(policy, txtNewPolicyNumber.Text, int.Parse(txtNewPremia.Text), (InsuranceCompany)cbCompany.SelectedItem);
            dgQuotesBinding();
            txtNewPolicyNumber.Clear();
            txtNewPremia.Clear();
            cbCompany.SelectedIndex = -1;
        }

        private void dgQuotesBinding()
        {
            //get all quotes
            dgQuotes.ItemsSource = renewalLogic.GetForeingWorkersRenewalQuotesByRenewalId(policy.ForeingWorkersPolicyID);
            lvSettings.AutoSizeColumns(dgQuotes.View);
        }

        private void btnUpdateCompanyTable_Click(object sender, RoutedEventArgs e)
        {
            int? selectedItemId = null;
            if (cbCompany.SelectedItem != null)
            {
                selectedItemId = ((InsuranceCompany)cbCompany.SelectedItem).CompanyID;
            }
            int insuranceID = insuranceLogic.GetInsuranceID("עובדים זרים");
            InsuranceCompany company = new InsuranceCompany() { InsuranceID = insuranceID };
            frmUpdateTable updateCompaniesTable = new frmUpdateTable(company);
            updateCompaniesTable.ShowDialog();
            cbCompanyBinding();
            if (updateCompaniesTable.ItemAdded != null)
            {
                SelectItemInCb(((InsuranceCompany)updateCompaniesTable.ItemAdded).CompanyID);
            }
            else if (selectedItemId != null)
            {
                SelectItemInCb((int)selectedItemId);
            }
        }

        private void SelectItemInCb(int id)
        {
            var items = cbCompany.Items;
            foreach (var item in items)
            {
                InsuranceCompany parsedItem = (InsuranceCompany)item;
                if (parsedItem.CompanyID == id)
                {
                    cbCompany.SelectedItem = item;
                    break;
                }
            }
        }

        private void btnDeleeQuote_Click(object sender, RoutedEventArgs e)
        {
            if (dgQuotes.SelectedItem != null)
            {
                renewalLogic.DeleteForeingWorkersQuote((ForeingWorkersRenewalQuote)dgQuotes.SelectedItem);
                dgQuotesBinding();
            }
            else
            {
                MessageBox.Show("נא לסמן הצעה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
        }

        //private void btnAddTracking_Click(object sender, RoutedEventArgs e)
        //{
        //    frmTracking newTrackingWindow = new frmTracking(client, policy, user);
        //    newTrackingWindow.ShowDialog();
        //    if (newTrackingWindow.trackingContent != null)
        //    {
        //        renewalLogic.InsertForeingWorkersRenewalTracking(policy.ForeingWorkersPolicyID, user.UserID, DateTime.Now, newTrackingWindow.trackingContent);
        //        dgTrackingBinding();
        //    }
        //}

        //private void dgTrackingBinding()
        //{
        //    dgTracking.ItemsSource = renewalLogic.GetForeingWorkersRenewalTrackingsByPolicyId(policy.ForeingWorkersPolicyID);
        //    lvSettings.AutoSizeColumns(dgTracking.View);
        //}

        //private void btnDeleteTracking_Click(object sender, RoutedEventArgs e)
        //{
        //    if (dgTracking.SelectedItem == null)
        //    {
        //        MessageBox.Show("נא לסמן מעקב", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
        //        return;
        //    }
        //    renewalLogic.DeleteForeingWorkersRenewalTracking((ForeingWorkersRenewalTracking)dgTracking.SelectedItem);
        //    dgTrackingBinding();
        //}

        //private void btnUpdateTracking_Click(object sender, RoutedEventArgs e)
        //{
        //    UpdateTracking();
        //}

        private void btnPolicyArchive_Click(object sender, RoutedEventArgs e)
        {
            frmScan2 archiveWindow = new frmScan2(client, policy, null, user);
            archiveWindow.ShowDialog();
        }

        private void btnPolicyRenewal_Click(object sender, RoutedEventArgs e)
        {
            if (dgQuotes.SelectedItem == null)
            {
                MessageBox.Show("נא לסמן הצעה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            frmForeignWorkers renewPolicy = new frmForeignWorkers(client, user, true, policy, (ForeingWorkersRenewalQuote)dgQuotes.SelectedItem);
            renewPolicy.ShowDialog();
            if (renewPolicy.renewalAccepted)
            {
                policy.RenewalStatusID = policiesLogics.UpdatePolicyRenewalStatus(policy, "חודש");
                SelectStatusInCb("חודש");
                dgQuotes.UnselectAll();
                dgQuotes.IsEnabled = false;
                cbRenewalStatus.IsEnabled = false;
                gbQuotes.IsEnabled = false;
            }
        }

        private void cbRenewalStatus_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            policy.RenewalStatusID = policiesLogics.UpdatePolicyRenewalStatus(policy, ((RenewalStatus)cbRenewalStatus.SelectedItem).RenewalStatusName);
        }

        private void txtNewPremia_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (sender is TextBox)
            {
                TextBox txtBox = (TextBox)sender;
                txtBox.ClearValue(BorderBrushProperty);
                if (txtBox.Text != "")
                {
                    try
                    {
                        decimal reusult = validation.ConvertStringToDecimal(txtBox.Text);
                    }
                    catch (Exception ex)
                    {
                        txtBox.BorderBrush = System.Windows.Media.Brushes.Red;
                        System.Windows.Forms.MessageBox.Show(ex.Message);
                        txtBox.Text = "";
                    }
                }
            }
        }

        //private void dgTracking_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        //{
        //    UpdateTracking();
        //}

        //private void UpdateTracking()
        //{
        //    if (dgTracking.SelectedItem == null)
        //    {
        //        MessageBox.Show("נא לסמן מעקב", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
        //        return;
        //    }
        //    frmTracking updateTrackingWindow = new frmTracking((ForeingWorkersRenewalTracking)dgTracking.SelectedItem, client, policy, user);
        //    updateTrackingWindow.ShowDialog();
        //    renewalLogic.UpdateForeingWorkersRenewalTracking((ForeingWorkersRenewalTracking)dgTracking.SelectedItem, updateTrackingWindow.trackingContent);
        //    dgTrackingBinding();
        //    dgTracking.UnselectAll();
        //}

        private void btnCalculator_Click(object sender, RoutedEventArgs e)
        {
            System.Diagnostics.Process.Start("calc");
        }

        private void dgQuotes_MouseDown(object sender, MouseButtonEventArgs e)
        {
            dgQuotes.UnselectAll();
        }

        //private void dgTracking_MouseDown(object sender, MouseButtonEventArgs e)
        //{
        //    dgTracking.UnselectAll();
        //}

        private void cbAddition_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            txtPremia.Text = ((decimal)((ForeingWorkersPolicy)cbAddition.SelectedItem).TotalPremium).ToString("0.##");
        }

        private void btnClientDetails_Click(object sender, RoutedEventArgs e)
        {
            if (client != null)
            {
                clientDetailsWindow = new frmAddClient(client, user);
                clientDetailsWindow.ShowDialog();
                client = clientLogic.GetClientByClientID(client.ClientID);
                SetClientDetails();
            }
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (trackingWindow != null && trackingWindow.IsLoaded)
            {
                trackingWindow.Close();
            }
            if (clientDetailsWindow != null && clientDetailsWindow.IsLoaded)
            {
                clientDetailsWindow.Close();
            }
        }

        private void btnTracking_Click(object sender, RoutedEventArgs e)
        {
            trackingWindow = new frmRenewalTrackingManager(policy, tempList, user);
            trackingWindow.Show();
        }

    }
}
