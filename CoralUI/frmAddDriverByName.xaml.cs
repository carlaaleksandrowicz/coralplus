﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using CoralBusinessLogics;


namespace CoralUI
{
    /// <summary>
    /// Interaction logic for frmAddDriverByName.xaml
    /// </summary>
    public partial class frmAddDriverByName : Window
    {
        DriversByNameLogic driversLogic = new DriversByNameLogic();        
        public DeclaredDriver newDriver;
        public DeclaredDriver driverToUpdate;
        private bool isChanges=false;
        bool confirmBeforeClosing = true;
        bool cancelClose = false;

        //בנאי להוספת נהג נקוב בשם
        public frmAddDriverByName()
        {
            InitializeComponent();
            txtLastName.Focus();
        }

        //בנאי לעדכון נהג נקוב בשם
        public frmAddDriverByName(DeclaredDriver driver)
        {
            InitializeComponent();
            driverToUpdate = driver;
            txtLastName.Text = driverToUpdate.DriverLastName;
            txtFirstName.Text = driverToUpdate.DriverFirstName;
            txtIdNumber.Text = driverToUpdate.DriverIdNumber;
            txtIdNumber.IsEnabled = false;
            dpBirthDate.SelectedDate = driverToUpdate.BirthDate;
            txtLicenseNumber.Text = driverToUpdate.LicenseNumber;
            dpLicenseDate.SelectedDate = driverToUpdate.LicenseDate;
            txtComments.Text = driverToUpdate.Remarks;
            //txtBtnSave.Text = "עדכן";
            //var addUriSource = new Uri(@"/Images/jidushim (1).png", UriKind.Relative);
            //imgBtnSave.Source = new BitmapImage(addUriSource);
        }

        //לחיצה על שמירת הנתונים של נהג נקוב בשם 
        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            txtLastName.ClearValue(BorderBrushProperty);
            txtFirstName.ClearValue(BorderBrushProperty);
            txtIdNumber.ClearValue(BorderBrushProperty);
            //שולח לבדיקת שדות חובה
            if (!InputNullValidation())
            {
                cancelClose = true;
                MessageBox.Show("נא למלאות שדות חובה", "", MessageBoxButton.OK, MessageBoxImage.Error);
                return; 
            }
            //אם מדובר על הוספת נהג נקוב בשם:
            if (driverToUpdate==null)
            {
                //בודק שלת.ז יש מס' בלבד
                foreach (char c in txtIdNumber.Text)
                {
                    if (c < '0' || c > '9')
                    {
                        cancelClose = true;
                        MessageBox.Show("ת.ז חייבת להיות בעלת מספרים בלבד", "שגיאה", MessageBoxButton.OK, MessageBoxImage.Error);
                        txtIdNumber.Clear();
                        return;
                    }
                }
                newDriver = new DeclaredDriver() { DriverLastName = txtLastName.Text, DriverFirstName = txtFirstName.Text, DriverIdNumber = txtIdNumber.Text, BirthDate = dpBirthDate.SelectedDate, LicenseNumber = txtLicenseNumber.Text, LicenseDate = dpLicenseDate.SelectedDate, Remarks = txtComments.Text };                
            }
            //אם מדובר על עדכון נהג נקוב בשם:
            else
            {
                driverToUpdate.DriverLastName = txtLastName.Text;
                driverToUpdate.DriverFirstName = txtFirstName.Text;                
                driverToUpdate.BirthDate = dpBirthDate.SelectedDate;
                driverToUpdate.LicenseNumber = txtLicenseNumber.Text;
                driverToUpdate.LicenseDate = dpLicenseDate.SelectedDate;
                driverToUpdate.Remarks = txtComments.Text;
            }
            if (sender is Button)
            {
                Button btn = (Button)sender;
                if (btn.Name == "btnSave")
                {
                    confirmBeforeClosing = false;
                    Close();
                }
            }
        }

        //בודק שדות חובה
        private bool InputNullValidation()
        {
            bool isValid = true;
            if (txtLastName.Text=="")
            {
                txtLastName.BorderBrush = Brushes.Red;
                isValid = false;
            }
            if (txtFirstName.Text == "")
            {
                txtFirstName.BorderBrush = Brushes.Red;
                isValid = false;
            }
            if (txtIdNumber.Text == "")
            {
                txtIdNumber.BorderBrush = Brushes.Red;
                isValid = false;
            }
            return isValid;
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void txtLastName_GotFocus(object sender, RoutedEventArgs e)
        {
            isChanges = true;
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (confirmBeforeClosing && isChanges)
            {
                if (MessageBox.Show("האם ברצונך לשמור שינוים שבצעת", "אישור", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    btnSave_Click(sender, new RoutedEventArgs());
                    if (cancelClose)
                    {
                        e.Cancel = true;
                        cancelClose = false;
                    }
                }
            }
        }
    }
}
