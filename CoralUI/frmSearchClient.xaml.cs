﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CoralUI
{
    /// <summary>
    /// Interaction logic for frmSearchClient.xaml
    /// </summary>
    public partial class frmSearchClient : Window
    {
        public frmSearchClient()
        {
            InitializeComponent();

            dpBirthDate.SelectedDate = DateTime.Now;
        }
    }
}
