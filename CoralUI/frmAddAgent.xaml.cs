﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using CoralBusinessLogics;
using System.Collections.ObjectModel;


namespace CoralUI
{
    public class AgentNumberViewModel
    {
        public string InsuranceName { get; set; }
        public string CompanyName { get; set; }
        public string Number { get; set; }
    }
    /// <summary>
    /// Interaction logic for frmAddAgent.xaml
    /// </summary>
    public partial class frmAddAgent : Window
    {
        AgentsLogic agentsLogic = new AgentsLogic();
        InsurancesLogic insuranceLogics = new InsurancesLogic();
        CompaniesLogic companyLogic = new CompaniesLogic();
        ObservableCollection<AgentNumber> agentNumbers = new ObservableCollection<AgentNumber>();
        ObservableCollection<AgentNumberViewModel> agentNumbersViewModel = new ObservableCollection<AgentNumberViewModel>();
        Agent agentToUpdate = null;
        List<AgentNumber> existingNumbers = null;
        ListViewSettings lvSettings = new ListViewSettings();
        bool confirmBeforeClosing = true;
        bool cancelClose = false;

        //בנאי להוספת סוכן חדש
        public frmAddAgent()
        {
            InitializeComponent();
            dpBirthDate.SelectedDate = DateTime.Now;
        }

        //בנאי לעדכון סוכן קיים
        public frmAddAgent(Agent agent)
        {
            InitializeComponent();
            agentToUpdate = agent;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            CbSubortinateToBinding();
            CbInsuranceTypeBinding();
            if (agentToUpdate == null)
            {
                cbLevel.SelectedIndex = 0;
                DataContext = agentNumbers;
            }
            else
            //ממלא את פירטי הסוכן שרוצים לעדכן
            {
                txtLastName.Text = agentToUpdate.LastName;
                txtFirstName.Text = agentToUpdate.FirstName;
                txtid.Text = agentToUpdate.IdNumber;
                txtid.IsEnabled = false;
                dpBirthDate.SelectedDate = agentToUpdate.BirthDate;
                if (agentToUpdate.AgentLevel)
                {
                    cbLevel.SelectedIndex = 0;
                }
                else
                {
                    cbLevel.SelectedIndex = 1;
                }
                //cbSubordinateTo.SelectedItem = (agentsLogic.GetAllAgents()).FirstOrDefault(a => a.AgentID == agentToUpdate.SubordinateToAgentID);
                var principalAgents = cbSubordinateTo.Items;
                foreach (var item in principalAgents)
                {
                    Agent principalAgent = (Agent)item;
                    if (principalAgent.AgentID == agentToUpdate.SubordinateToAgentID)
                    {
                        cbSubordinateTo.SelectedItem = item;
                        break;
                    }
                }
                //var categories = cbCategory.Items;
                //foreach (var item in categories)
                //{
                //    Category category = (Category)item;
                //    if (category.CategoryID == clientToUpdate.CategoryID)
                //    {
                //        cbCategory.SelectedItem = item;
                //        break;
                //    }
                //}
                txtAgencyName.Text = agentToUpdate.AgencyName;
                if ((bool)agentToUpdate.Status)
                {
                    cbStatus.SelectedIndex = 0;
                }
                else
                {
                    cbStatus.SelectedIndex = 1;
                }
                txtHomePhone.Text = agentToUpdate.HomePhone;
                txtCellPhone.Text = agentToUpdate.CellPhone;
                txtWorkPhone.Text = agentToUpdate.WorkPhone;
                txtFax.Text = agentToUpdate.Fax;
                txtEmail.Text = agentToUpdate.Email;
                txtCity.Text = agentToUpdate.City;
                txtStreet.Text = agentToUpdate.Street;
                txtHouseNumber.Text = agentToUpdate.HomeNumber;
                txtAptNumber.Text = agentToUpdate.ApartmentNumber;
                txtZipCode.Text = agentToUpdate.ZipCode;
                existingNumbers = agentsLogic.GetNumbersByAgent(agentToUpdate);
                foreach (AgentNumber item in existingNumbers)
                {
                    AgentNumberViewModel existingNumber = new AgentNumberViewModel() { InsuranceName = item.Insurance.InsuranceName, CompanyName = item.Company.CompanyName, Number = item.Number };
                    agentNumbersViewModel.Add(existingNumber);
                }
                DataGridNumbersBinding();
            }

        }

        //מסנכרן את טבלת מספרי הסוכן עם בסיס הנתונים
        private void DataGridNumbersBinding()
        {
            dgNumbers.ItemsSource = agentNumbersViewModel.OrderBy(n => n.InsuranceName);
            lvSettings.AutoSizeColumns(dgNumbers.View);
            //dgColInsuranceType.Binding = new Binding("InsuranceName");
            //dgColInsuranceCompany.Binding = new Binding("CompanyName");
            //dgColNumber.Binding = new Binding("Number");
        }

        //public void AutoSizeColumns()
        //{
        //    GridView gv = dgNumbers.View as GridView;
        //    if (gv != null)
        //    {
        //        foreach (var c in gv.Columns)
        //        {
        //            // Code below was found in GridViewColumnHeader.OnGripperDoubleClicked() event handler (using Reflector)
        //            // i.e. it is the same code that is executed when the gripper is double clicked
        //            if (double.IsNaN(c.Width))
        //            {
        //                c.Width = c.ActualWidth;
        //            }
        //            c.Width = double.NaN;
        //        }
        //    }
        //}

        //מסנכרן את הקומבו בוקס של בחירת סוכן הכפופים לו עם בסיס הנתונים
        private void CbSubortinateToBinding()
        {
            cbSubordinateTo.ItemsSource = agentsLogic.GetActivePrincipalAgents();
        }

        //מסנכרן את הקומבו בוקס לבחירת סוג ביטוח עם בסיס הנתונים
        private void CbInsuranceTypeBinding()
        {
            cbInsuranceType.ItemsSource = insuranceLogics.GetAllInsurances();
            cbInsuranceType.DisplayMemberPath = "InsuranceName";
        }

        //מסנכרן את הקומבו בוקס לבחירת חברה עם בסיס הנתונים לפי סוג הביטוח הנבחר
        private void CbInsuranceCompanyBinding(bool isUpdate)
        {
            if (!isUpdate)
            {
                cbInsuranceCompany.ItemsSource = insuranceLogics.GetActiveCompaniesByInsurance(((Insurance)cbInsuranceType.SelectedItem).InsuranceID);
            }
            else
            {
                cbInsuranceCompany.ItemsSource = insuranceLogics.GetCompaniesByInsurance(((Insurance)cbInsuranceType.SelectedItem).InsuranceID);
            }
            cbInsuranceCompany.DisplayMemberPath = "Company.CompanyName";
        }

        //שומר את פרטי הסוכן החדש/המעודכן
        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            txtLastName.ClearValue(BorderBrushProperty);
            txtFirstName.ClearValue(BorderBrushProperty);
            txtid.ClearValue(BorderBrushProperty);
            //שולח לולידציה את הקלט
            if (!AgentInputNullValidation())
            {
                cancelClose = true;
                return;
            }

            bool level;
            bool status;
            Agent subordinateTo = null;
            if (cbLevel.SelectedIndex == 0)
            {
                level = true;
            }
            else
            {
                level = false;
            }
            if (cbStatus.SelectedIndex == 0)
            {
                status = true;
            }
            else
            {
                status = false;
            }
            if (cbSubordinateTo.IsEnabled)
            {
                subordinateTo = (Agent)cbSubordinateTo.SelectedItem;
            }
            else
            {
                subordinateTo = null;

            }
            DateTime birthDate = (DateTime)dpBirthDate.SelectedDate;
            try
            {
                if (agentToUpdate == null)
                {
                    //בודק שמס' זהות יהיה בעל מספרים בלבד
                    foreach (char c in txtid.Text)
                    {
                        if (c < '0' || c > '9')
                        {
                            MessageBox.Show("ת.ז חייבת להיות בעלת מספרים בלבד", "שגיאה", MessageBoxButton.OK, MessageBoxImage.Error);
                            txtid.Clear();
                            cancelClose = true;
                            return;
                        }
                    }
                    //שולח להוספת סוכן חדש
                    agentsLogic.InsertAgent(level, subordinateTo, txtLastName.Text, txtFirstName.Text, txtid.Text, birthDate, txtCity.Text, txtStreet.Text, txtHouseNumber.Text, txtAptNumber.Text, txtZipCode.Text, txtHomePhone.Text, txtCellPhone.Text, txtWorkPhone.Text, txtFax.Text, txtEmail.Text, txtAgencyName.Text, status);
                }
                else
                {
                    //שמר עדכון פרטים ושולח אותם כדי לעדכן אותם בבסיס הנתונים
                    agentToUpdate.AgencyName = txtAgencyName.Text;
                    agentToUpdate.AgentLevel = level;
                    agentToUpdate.ApartmentNumber = txtAptNumber.Text;
                    agentToUpdate.BirthDate = birthDate;
                    agentToUpdate.CellPhone = txtCellPhone.Text;
                    agentToUpdate.City = txtCity.Text;
                    agentToUpdate.Email = txtEmail.Text;
                    agentToUpdate.Fax = txtFax.Text;
                    agentToUpdate.FirstName = txtFirstName.Text;
                    agentToUpdate.HomeNumber = txtHouseNumber.Text;
                    agentToUpdate.HomePhone = txtHomePhone.Text;
                    agentToUpdate.LastName = txtLastName.Text;
                    agentToUpdate.Status = status;
                    agentToUpdate.Street = txtStreet.Text;
                    if (subordinateTo != null)
                    {
                        agentToUpdate.SubordinateToAgentID = subordinateTo.AgentID;
                    }
                    else
                    {
                        agentToUpdate.SubordinateToAgentID = null;
                        agentToUpdate.Agent1 = null;
                    }
                    agentToUpdate.WorkPhone = txtWorkPhone.Text;
                    agentToUpdate.ZipCode = txtZipCode.Text;
                    agentsLogic.UpdateAgent(agentToUpdate);
                }
                SaveUnsavedAgentNumber();
                agentsLogic.InsertNumbersToAgent(txtid.Text, agentNumbers);
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
            }

            if (sender is Button)
            {
                Button btn = (Button)sender;
                if (btn.Name == "btnSave")
                {
                    confirmBeforeClosing = false;
                    Close();
                }
            }
        }

        private void SaveUnsavedAgentNumber()
        {
            if (cbInsuranceType.SelectedItem!=null|| txtNumber.Text!="")
            {
                btnAddNumber_Click(this, new RoutedEventArgs());
            }
        }

        //ולידציה של שדות חובה. אם חסרים פרטים, צובע באדום את מסגרת הפרטים החסרים
        private bool AgentInputNullValidation()
        {
            bool isValid = true;
            if (txtLastName.Text == "")
            {
                txtLastName.BorderBrush = Brushes.Red;
                isValid = false;
            }
            if (txtFirstName.Text == "")
            {
                txtFirstName.BorderBrush = Brushes.Red;
                isValid = false;
            }
            if (txtid.Text == "")
            {
                txtid.BorderBrush = Brushes.Red;
                isValid = false;
            }
            return isValid;
        }

        //מאפשר/מגביל את בחירת סוכן הכפופים לו 
        private void cbLevel_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cbLevel.SelectedIndex == 0)
            {
                cbSubordinateTo.IsEnabled = false;
            }
            else
            {
                cbSubordinateTo.IsEnabled = true;
            }
        }

        //מוסיף/מעדכן מס' לסוכן 
        private void btnAddNumber_Click(object sender, RoutedEventArgs e)
        {
            cbInsuranceType.ClearValue(BorderBrushProperty);
            cbInsuranceCompany.ClearValue(BorderBrushProperty);
            txtNumber.ClearValue(BorderBrushProperty);
            //שלוח לבדיקת שדות חובה
            if (!NumberInputsNullValidation())
            {
                if (sender is Window)
                {
                    MessageBox.Show("שים לב, פרטי מספר הסוכן לא עודכנו במערכת", "מידע", MessageBoxButton.OK, MessageBoxImage.Information);
                }
                return;
            }
            //מוסיף מס' חדש
            if (txtBtnAddNumber.Text != "עדכן מספר")
            {

                AgentNumber agentNumber = new AgentNumber { InsuranceID = ((Insurance)cbInsuranceType.SelectedItem).InsuranceID, CompanyID = ((InsuranceCompany)cbInsuranceCompany.SelectedItem).CompanyID, Number = txtNumber.Text };
                agentNumbers.Add(agentNumber);
                AgentNumberViewModel agentNumberViewModel = new AgentNumberViewModel { InsuranceName = ((Insurance)cbInsuranceType.SelectedItem).InsuranceName, CompanyName = ((InsuranceCompany)cbInsuranceCompany.SelectedItem).Company.CompanyName, Number = txtNumber.Text };
                agentNumbersViewModel.Add(agentNumberViewModel);
                DataGridNumbersBinding();
            }
            //מעדכן מס' קיים אצל הסוכן
            else
            {
                try
                {
                    AgentNumberViewModel numberToUpdate = agentNumbersViewModel.FirstOrDefault(n => n.Number == ((AgentNumberViewModel)dgNumbers.SelectedItem).Number);
                    var numberInDb = agentNumbers.FirstOrDefault(n => n.Number == numberToUpdate.Number);
                    if (numberInDb == null)
                    {
                        agentsLogic.UpdateNumberToAgent(agentToUpdate, numberToUpdate.Number, txtNumber.Text, numberToUpdate.CompanyName);
                    }
                    else
                    {
                        numberInDb.Number = txtNumber.Text;
                    }
                    numberToUpdate.Number = txtNumber.Text;
                    DataGridNumbersBinding();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "שגיאה", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            CleanGbNumbers();
            dgNumbers.UnselectAll();
        }

        //ולידמיה של שדות חובה
        private bool NumberInputsNullValidation()
        {
            bool isValid = true;
            if (cbInsuranceType.SelectedItem == null)
            {
                cbInsuranceType.BorderBrush = Brushes.Red;
                isValid = false;
            }
            if (cbInsuranceCompany.SelectedItem == null)
            {
                cbInsuranceCompany.BorderBrush = Brushes.Red;
                isValid = false;
            }
            if (txtNumber.Text == "")
            {
                txtNumber.BorderBrush = Brushes.Red;
                isValid = false;
            }
            return isValid;
        }

        //מנקה את תיבות הטקסט ומחזיר לברירת מחדל
        private void CleanGbNumbers()
        {
            txtBtnAddNumber.Text = "הוסף מספר";
            var addUriSource = new Uri(@"/IconsMind/Add_24px.png", UriKind.Relative);
            imgBtnAddNumber.Source = new BitmapImage(addUriSource);
            cbInsuranceType.SelectedItem = null;
            cbInsuranceType.IsEnabled = true;
            cbInsuranceCompany.SelectedItem = null;
            txtNumber.Clear();
        }

        //מאפשר בחירת חברת ביטוח לאחר שנבחר סוג ביטוח
        private void cbInsuranceType_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cbInsuranceType.SelectedItem != null)
            {
                cbInsuranceCompany.IsEnabled = true;
                btnUpdateCompaniesTable.IsEnabled = true;
                try
                {
                    CbInsuranceCompanyBinding(false);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "שגיאה", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            else
            {
                cbInsuranceCompany.IsEnabled = false;
                btnUpdateCompaniesTable.IsEnabled = false;
            }
        }

        //פותח את חלון עדכון חברות ביטוח
        private void btnUpdateCompaniesTable_Click(object sender, RoutedEventArgs e)
        {
            int? selectedItemId = null;
            if (cbInsuranceCompany.SelectedItem != null)
            {
                selectedItemId = ((InsuranceCompany)cbInsuranceCompany.SelectedItem).CompanyID;
            }
            InsuranceCompany company = new InsuranceCompany() { InsuranceID = ((Insurance)cbInsuranceType.SelectedItem).InsuranceID };
            frmUpdateTable updateTable = new frmUpdateTable(company);
            updateTable.ShowDialog();
            CbInsuranceCompanyBinding(false);
            if (updateTable.ItemAdded != null)
            {
                SelectItemInCb(((InsuranceCompany)updateTable.ItemAdded).CompanyID);
            }
            else if (selectedItemId != null)
            {
                SelectItemInCb((int)selectedItemId);
            }           
        }

        private void SelectItemInCb(int id)
        {
            var items = cbInsuranceCompany.Items;
            foreach (var item in items)
            {
                InsuranceCompany parsedItem = (InsuranceCompany)item;
                if (parsedItem.CompanyID == id)
                {
                    cbInsuranceCompany.SelectedItem = item;
                    break;
                }
            }
        }

        //ממלא את תיבות הטקסט לפי המספר המומן בטבלת המספרים
        private void dgNumbers_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (dgNumbers.SelectedItem == null)
            {
                CleanGbNumbers();
                return;
            }
            txtBtnAddNumber.Text = "עדכן מספר";
            var updateUriSource = new Uri(@"/IconsMind/Edit_24px.png", UriKind.Relative);
            imgBtnAddNumber.Source = new BitmapImage(updateUriSource);
            CbInsuranceTypeBinding();
            cbInsuranceType.IsEnabled = false;

            if (dgNumbers.SelectedItem is AgentNumber)
            {
                AgentNumber numberSelected = (AgentNumber)dgNumbers.SelectedItem;
                cbInsuranceType.SelectedItem = (insuranceLogics.GetAllInsurances()).FirstOrDefault(i => i.InsuranceID == numberSelected.InsuranceID);
                CbInsuranceCompanyBinding(true);
                cbInsuranceCompany.SelectedItem = (insuranceLogics.GetCompaniesByInsurance(((Insurance)cbInsuranceType.SelectedItem).InsuranceID)).FirstOrDefault(c => c.CompanyID == numberSelected.CompanyID);
                cbInsuranceCompany.IsEnabled = false;
                txtNumber.Text = numberSelected.Number;
            }
            if (dgNumbers.SelectedItem is AgentNumberViewModel)
            {
                AgentNumberViewModel numberSelected = (AgentNumberViewModel)dgNumbers.SelectedItem;
                var insuranceTypes = cbInsuranceType.Items;
                foreach (var item in insuranceTypes)
                {
                    Insurance insuranceType = (Insurance)item;
                    if (insuranceType.InsuranceName == numberSelected.InsuranceName)
                    {
                        cbInsuranceType.SelectedItem = item;
                        break;
                    }
                }
                //cbInsuranceType.SelectedItem = (insuranceLogics.GetAllInsurances()).FirstOrDefault(i => i.InsuranceName == numberSelected.InsuranceName);
                CbInsuranceCompanyBinding(true);
                var companies = cbInsuranceCompany.Items;
                foreach (var item in companies)
                {
                    InsuranceCompany company = (InsuranceCompany)item;
                    if (company.Company.CompanyName == numberSelected.CompanyName)
                    {
                        cbInsuranceCompany.SelectedItem = item;
                        break;
                    }
                }
                //cbInsuranceCompany.SelectedItem = (insuranceLogics.GetCompaniesByInsurance(((Insurance)cbInsuranceType.SelectedItem).InsuranceID)).FirstOrDefault(c => c.Company.CompanyName == numberSelected.CompanyName);
                cbInsuranceCompany.IsEnabled = false;
                btnUpdateCompaniesTable.IsEnabled = false;
                txtNumber.Text = numberSelected.Number;
            }
        }        

        //סוגר את החלון
        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        object selectedItem = null;
        private bool isChanges=false;

        private void dgNumbers_PreviewMouseUp(object sender, MouseButtonEventArgs e)
        {            
            var item = (sender as ListView).SelectedItem;
            if (item != null&&item==selectedItem)
            {
                dgNumbers.UnselectAll();
            }
            selectedItem = dgNumbers.SelectedItem;
        }

        private void txtLastName_GotFocus(object sender, RoutedEventArgs e)
        {
            isChanges = true;
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (confirmBeforeClosing && isChanges)
            {
                if (MessageBox.Show("האם ברצונך לשמור שינוים שבצעת", "אישור", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    btnSave_Click(sender, new RoutedEventArgs());
                    if (cancelClose)
                    {
                        e.Cancel = true;
                        cancelClose = false;
                    }
                }
            }
        }
    }
}
