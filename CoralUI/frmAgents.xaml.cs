﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using CoralBusinessLogics;


namespace CoralUI
{
    /// <summary>
    /// Interaction logic for frmAgents.xaml
    /// </summary>
    public partial class frmAgents : Window
    {
        AgentsLogic logic = new AgentsLogic();
        ListViewSettings lvSettings = new ListViewSettings();
        Email email = new Email();
        public frmAgents()
        {
            InitializeComponent();
            this.SizeToContent = SizeToContent.Manual;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            DataGridBinding();
        }

        //מסנכרן את טבלת הסוכנים עם בסיס הנתונים
        private void DataGridBinding()
        {
            dgAgents.UnselectAll();
            dgAgents.ItemsSource = logic.GetAllAgents();
            lvSettings.AutoSizeColumns(dgAgents.View);
        }

        //סוגר את החלון
        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        //פותח את החלון של הוספת סוכן חדש
        private void btnAddAgent_Click(object sender, RoutedEventArgs e)
        {
            frmAddAgent addAgentWindow = new frmAddAgent();
            addAgentWindow.ShowDialog();
            DataGridBinding();
        }

        //פותח את החלון של עדכון סוכן חדש
        private void btnUpdateAgent_Click(object sender, RoutedEventArgs e)
        {
            if (dgAgents.SelectedItem != null)
            {
                frmAddAgent updateAgentWindow = new frmAddAgent((Agent)dgAgents.SelectedItem);
                updateAgentWindow.ShowDialog();
                DataGridBinding();
            }
            else
            {
                MessageBox.Show("נא לסמן את הסוכן שברצונך לעדכן", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
        }

        //מחפש סוכנים המתאימים לטקסט הרשום תוך כדי הקלדה
        private void txtSearchAgents_TextChanged(object sender, TextChangedEventArgs e)
        {
            dgAgents.ItemsSource = logic.FindAgentByString(txtSearchAgents.Text);
        }

        //מוחק את מילות החיפוש ומחזיר רשימה של כל הסוכנים
        private void btnDisplayAll_Click(object sender, RoutedEventArgs e)
        {
            txtSearchAgents.Clear();
        }

        private void dgAgents_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (dgAgents.SelectedItem != null)
            {
                frmAddAgent updateAgentWindow = new frmAddAgent((Agent)dgAgents.SelectedItem);
                updateAgentWindow.ShowDialog();
                DataGridBinding();
            }
            else
            {
                MessageBox.Show("נא לסמן את הסוכן שברצונך לעדכן", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
        }

        private void btnSendEmail_Click(object sender, RoutedEventArgs e)
        {
            if (dgAgents.SelectedItem == null)
            {
                MessageBox.Show("נא לסמן סוכן", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            Agent agent = (Agent)dgAgents.SelectedItem;
            if (agent.Email == "" )
            {
                MessageBox.Show("אין לסוכן כתובת דוא''ל במערכת", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            string[] address = new string[] { agent.Email};
            try
            {
                email.SendEmailFromOutlook("", "", null, address);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void dgAgents_MouseDown(object sender, MouseButtonEventArgs e)
        {
            dgAgents.UnselectAll();
        }
    }
}
