﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using CoralBusinessLogics;
using System.IO;

namespace CoralUI
{
    /// <summary>
    /// Interaction logic for frmHealthClaim.xaml
    /// </summary>
    public partial class frmHealthClaim : Window
    {
        Client client = null;
        HealthPolicy policy = null;
        User user;
        HealthClaimsLogic claimLogic = new HealthClaimsLogic();
        InputsValidations validations = new InputsValidations();
        List<HealthWitness> witnesses = new List<HealthWitness>();
        List<HealthClaimTracking> trackings = new List<HealthClaimTracking>();
        TrackingsLogics trackingLogic = new TrackingsLogics();
        private HealthClaim healthClaim = null;
        ListViewSettings lv = new ListViewSettings();
        string path = "";
        string newPath = "";
        string clientDirPath = "";
        string[] claimPaths = null;
        List<object> nullErrorList = null;
        string[] healthPaths = null;
        private bool isChanges=false;
        HealthWitness witnessSelected;
        bool confirmBeforeClosing = true;
        bool cancelClose = false;
        public frmHealthClaim(Client policyClient, HealthPolicy policyClaim, User userAccount)
        {
            InitializeComponent();
            client = policyClient;
            policy = policyClaim;
            user = userAccount;
        }

        public frmHealthClaim(HealthClaim healthClaim, Client client, User user)
        {
            InitializeComponent();
            this.healthClaim = healthClaim;
            this.client = client;
            this.user = user;
        }


        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            string strAppDir = System.AppDomain.CurrentDomain.BaseDirectory;
            string path = strAppDir + @"Coral Files\ClientsPath.txt";

            if (File.Exists(path))
            {
                clientDirPath = File.ReadAllText(path);
            }
            else
            {
                MessageBox.Show("לא נמצא תיקיית הלקוחות. נא בדוק בהגדרות ראשוניות את נתיב התיקייה");
                Close();
                return;
            }
            if (healthClaim != null)
            {
                healthPaths = Directory.GetDirectories(clientDirPath + @"/" + client.ClientID + @"/בריאות", "*" + healthClaim.HealthPolicy.PolicyNumber + "*");
            }
            else if (policy != null)
            {
                healthPaths = Directory.GetDirectories(clientDirPath + @"/" + client.ClientID + @"/בריאות", "*" + policy.PolicyNumber + "*");
            }
            //if (lifePaths.Count() > 0)
            //{
            //    claimPaths = Directory.GetDirectories(lifePaths[0], "תביעות*");
            //    if (claimPaths.Count() > 0)
            //    {
            //        path = claimPaths[0];
            //    }
            //}
            cbClaimTypeBinding();
            cbClaimConditionBinding();
            lblNameSpace.Content = client.FirstName + " " + client.LastName;
            lblCellPhoneSpace.Content = client.CellPhone;
            lblEmailSpace.Content = client.Email;
            lblIdSpace.Content = client.IdNumber;
            lblPhoneSpace.Content = client.PhoneHome;
            if (policy != null)
            {
                lblInsuranceTypeSpace.Content = policy.HealthInsuranceType.HealthInsuranceTypeName;
                lblPhoneSpace.Content = client.PhoneHome;
                lblStartDateSpace.Content = ((DateTime)policy.StartDate).Date;
                lblCompanySpace.Content = policy.Company.CompanyName;
                dpOpenDate.SelectedDate = DateTime.Now;
                txtPolicyNumber.Text = policy.PolicyNumber;
            }
            else
            {
                lblInsuranceTypeSpace.Content = healthClaim.HealthPolicy.HealthInsuranceType.HealthInsuranceTypeName;
                lblStartDateSpace.Content = ((DateTime)healthClaim.HealthPolicy.StartDate).Date;
                lblCompanySpace.Content = healthClaim.HealthPolicy.Company.CompanyName;
                dpOpenDate.SelectedDate = (DateTime)healthClaim.OpenDate;
                txtPolicyNumber.Text = healthClaim.HealthPolicy.PolicyNumber;
                //var claimsTypes = cbClaimType.Items;
                //foreach (var item in claimsTypes)
                //{
                //    HealthClaimType claimType = (HealthClaimType)item;
                //    if (claimType.HealthClaimTypeID == healthClaim.HealthClaimTypeID)
                //    {
                //        cbClaimType.SelectedItem = item;
                //        break;
                //    }
                //}
                if (healthClaim.HealthClaimTypeID != null)
                {
                    SelectItemInCb((int)healthClaim.HealthClaimTypeID);
                }
                txtClaimNumber.Text = healthClaim.ClaimNumber;
                if (healthClaim.ClaimStatus == false)
                {
                    cbStatus.SelectedIndex = 1;
                }
                //var claimsConditions = cbClaimCondition.Items;
                //foreach (var item in claimsConditions)
                //{
                //    HealthClaimCondition claimCondition = (HealthClaimCondition)item;
                //    if (claimCondition.HealthClaimConditionID == healthClaim.HealthClaimConditionID)
                //    {
                //        cbClaimCondition.SelectedItem = item;
                //        break;
                //    }
                //}
                if (healthClaim.HealthClaimConditionID!=null)
                {
                    SelectConditionItemInCb((int)healthClaim.HealthClaimConditionID);
                }
                dpDeliveryDateToCompany.SelectedDate = healthClaim.DeliveredToCompanyDate;
                dpMoneyReceivedDate.SelectedDate = healthClaim.MoneyReceivedDate;
                if (healthClaim.ClaimAmount!=null)
                {
                    txtAmountClaimed.Text = ((decimal)healthClaim.ClaimAmount).ToString("0.##");
                }
                if (healthClaim.AmountReceived!=null)
                {
                    txtAmountReceived.Text = ((decimal)healthClaim.AmountReceived).ToString("0.##");
                }
                dpEventDate.SelectedDate = healthClaim.EventDateAndTime;
                tpEventHour.Value = healthClaim.EventDateAndTime;
                txtEventPlace.Text = healthClaim.EventPlace;
                txtEventDescription.Text = healthClaim.EventDescription;
                witnesses = claimLogic.GetWitnessesByClaim(healthClaim.HealthClaimID);
                dgWitnessesBinding();
                trackings = trackingLogic.GetAllHealthClaimTrackingsByClaimID(healthClaim.HealthClaimID);
                dgTrackingsBinding();
            }

        }

        private void cbClaimConditionBinding()
        {
            cbClaimCondition.ItemsSource = claimLogic.GetActiveHealthClaimConditions();
            cbClaimCondition.DisplayMemberPath = "Description";
        }

        private void cbClaimTypeBinding()
        {
            cbClaimType.ItemsSource = claimLogic.GetActiveHealthClaimTypes();
            cbClaimType.DisplayMemberPath = "ClaimTypeName";
        }

        private void btnUpdateClaimTypeTable_Click(object sender, RoutedEventArgs e)
        {
            int? selectedItemId = null;
            if (cbClaimType.SelectedItem != null)
            {
                selectedItemId = ((HealthClaimType)cbClaimType.SelectedItem).HealthClaimTypeID;
            }
            frmUpdateTable updateClaimTypesTable = new frmUpdateTable(new HealthClaimType());
            updateClaimTypesTable.ShowDialog();
            cbClaimTypeBinding();
            if (updateClaimTypesTable.ItemAdded != null)
            {
                SelectItemInCb(((HealthClaimType)updateClaimTypesTable.ItemAdded).HealthClaimTypeID);
            }
            else if (selectedItemId != null)
            {
                SelectItemInCb((int)selectedItemId);
            }
        }

        private void SelectItemInCb(int id)
        {
            var items = cbClaimType.Items;
            foreach (var item in items)
            {
                HealthClaimType parsedItem = (HealthClaimType)item;
                if (parsedItem.HealthClaimTypeID == id)
                {
                    cbClaimType.SelectedItem = item;
                    break;
                }
            }
        }
        private void btnUpdateClaimConditionTable_Click(object sender, RoutedEventArgs e)
        {
            int? selectedItemId = null;
            if (cbClaimCondition.SelectedItem != null)
            {
                selectedItemId = ((HealthClaimCondition)cbClaimCondition.SelectedItem).HealthClaimConditionID;
            }
            frmUpdateTable updateClaimConditionsTable = new frmUpdateTable(new HealthClaimCondition());
            updateClaimConditionsTable.ShowDialog();
            cbClaimConditionBinding();
            if (updateClaimConditionsTable.ItemAdded != null)
            {
                SelectConditionItemInCb(((HealthClaimCondition)updateClaimConditionsTable.ItemAdded).HealthClaimConditionID);
            }
            else if (selectedItemId != null)
            {
                SelectConditionItemInCb((int)selectedItemId);
            }
        }

        private void SelectConditionItemInCb(int id)
        {
            var items = cbClaimCondition.Items;
            foreach (var item in items)
            {
                HealthClaimCondition parsedItem = (HealthClaimCondition)item;
                if (parsedItem.HealthClaimConditionID == id)
                {
                    cbClaimCondition.SelectedItem = item;
                    break;
                }
            }
        }
        public void FillEmptyTextBoxes(TextBox[] inputs)
        {
            foreach (var item in inputs)
            {
                if (item.Text == "")
                {
                    item.Text = "0";
                }
            }
        }

        private void txtAmountClaimed_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (sender is TextBox)
            {
                TextBox txtBox = (TextBox)sender;
                txtBox.ClearValue(BorderBrushProperty);
                if (txtBox.Text != "")
                {
                    try
                    {
                        decimal reusult = validations.ConvertStringToDecimal(txtBox.Text);
                    }
                    catch (Exception ex)
                    {
                        txtBox.BorderBrush = System.Windows.Media.Brushes.Red;
                        System.Windows.Forms.MessageBox.Show(ex.Message);
                        txtBox.Text = "";
                    }
                }
            }
        }

        private void dpEventDate_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            if (dpEventDate.SelectedDate != null)
            {
                tpEventHour.IsEnabled = true;
            }
            else
            {
                tpEventHour.IsEnabled = false;
                tpEventHour.Value = null;
            }
        }

        private void btnAddWitness_Click(object sender, RoutedEventArgs e)
        {
            if (witnessSelected == null)
            {
                HealthWitness witness = new HealthWitness() { WitnessName = txtWitnessName.Text, WittnessAddress = txtWitnessAddress.Text, WitnessPhone = txtWitnessPhone.Text, WitnessCellPhone = txtWitnessCellPhone.Text };
                witnesses.Add(witness);
            }
            else
            {
                witnessSelected.WitnessCellPhone = txtWitnessCellPhone.Text;
                witnessSelected.WitnessName = txtWitnessName.Text;
                witnessSelected.WitnessPhone = txtWitnessPhone.Text;
                witnessSelected.WittnessAddress = txtWitnessAddress.Text;
            }
            dgWitnessesBinding();
            dgWitnesses.UnselectAll();

            txtWitnessAddress.Clear();
            txtWitnessCellPhone.Clear();
            txtWitnessName.Clear();
            txtWitnessPhone.Clear();
        }

        private void dgWitnessesBinding()
        {
            dgWitnesses.ItemsSource = witnesses.ToList();
            lv.AutoSizeColumns(dgWitnesses.View);
        }

        private void dgWitnesses_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            witnessSelected = (HealthWitness)dgWitnesses.SelectedItem;
            if (witnessSelected != null)
            {
                txtWitnessName.Text = witnessSelected.WitnessName;
                txtWitnessAddress.Text = witnessSelected.WittnessAddress;
                txtWitnessPhone.Text = witnessSelected.WitnessPhone;
                txtWitnessCellPhone.Text = witnessSelected.WitnessCellPhone;
            }
        }

        private void dgWitnesses_MouseDown(object sender, MouseButtonEventArgs e)
        {
            dgWitnesses.UnselectAll();
            txtWitnessName.Clear();
            txtWitnessAddress.Clear();
            txtWitnessPhone.Clear();
            txtWitnessCellPhone.Clear();
        }

        private void dgTrackings_MouseDown(object sender, MouseButtonEventArgs e)
        {
            dgTrackings.UnselectAll();
        }

        private void btnNewTracking_Click(object sender, RoutedEventArgs e)
        {
            frmTracking newTrackingWindow = new frmTracking(client, policy, user);
            newTrackingWindow.ShowDialog();
            if (newTrackingWindow.trackingContent != null)
            {
                trackings.Add(new HealthClaimTracking { TrackingContent = newTrackingWindow.trackingContent, Date = DateTime.Now, User = user });
                dgTrackingsBinding();
            }
        }

        private void dgTrackingsBinding()
        {
            dgTrackings.ItemsSource = trackings.OrderByDescending(t => t.Date);
            lv.AutoSizeColumns(dgTrackings.View);
        }

        private void btnDeleteTracking_Click(object sender, RoutedEventArgs e)
        {
            if (dgTrackings.SelectedItem == null)
            {
                MessageBox.Show("נא לסמן מעקב בטבלה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            if (MessageBox.Show("למחוק מעקב", "אישור", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                if (((HealthClaimTracking)dgTrackings.SelectedItem).HealthClaimTrackingID != 0)
                {
                    trackingLogic.DeleteHealthClaimTracking((HealthClaimTracking)dgTrackings.SelectedItem);
                }
                trackings.Remove((HealthClaimTracking)dgTrackings.SelectedItem);
                dgTrackingsBinding();
            }
            else
            {
                dgTrackings.UnselectAll();
            }
        }

        private void btnUpdateTracking_Click(object sender, RoutedEventArgs e)
        {
            if (dgTrackings.SelectedItem == null)
            {
                MessageBox.Show("נא לסמן מעקב בטבלה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            HealthClaimTracking trackingToUpdate = (HealthClaimTracking)dgTrackings.SelectedItem;
            frmTracking updateTrackingWindow = new frmTracking(trackingToUpdate, client, policy, user);
            updateTrackingWindow.ShowDialog();
            trackingToUpdate.TrackingContent = updateTrackingWindow.trackingContent;
            dgTrackingsBinding();
            dgTrackings.UnselectAll();
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            if (nullErrorList != null && nullErrorList.Count > 0)
            {
                foreach (var item in nullErrorList)
                {
                    if (item is TextBox)
                    {
                        ((TextBox)item).ClearValue(BorderBrushProperty);
                    }
                    else if (item is ComboBox)
                    {
                        ((ComboBox)item).ClearValue(BorderBrushProperty);
                    }
                    else if (item is DatePicker)
                    {
                        ((DatePicker)item).ClearValue(BorderBrushProperty);
                    }
                }
            }
            nullErrorList = validations.InputNullValidation(new object[] { dpEventDate });
            if (nullErrorList.Count > 0)
            {
                cancelClose = true;
                tbItemGeneral.Focus();
                MessageBox.Show("נא למלאות שדות חובה", "", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (dpMoneyReceivedDate.SelectedDate != null && dpDeliveryDateToCompany.SelectedDate != null)
            {
                if (dpMoneyReceivedDate.SelectedDate < dpDeliveryDateToCompany.SelectedDate)
                {
                    cancelClose = true;
                    MessageBox.Show("תאריך משלוח חייב להיות קטן מתאריך קבלת הכסף", "", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
            }
            int? claimTypeId = null;
            if (cbClaimType.SelectedItem != null)
            {
                claimTypeId = ((HealthClaimType)cbClaimType.SelectedItem).HealthClaimTypeID;
            }
            int? claimConditionId = null;
            if (cbClaimCondition.SelectedItem != null)
            {
                claimConditionId = ((HealthClaimCondition)cbClaimCondition.SelectedItem).HealthClaimConditionID;
            }
            bool status = true;
            if (cbStatus.SelectedIndex == 1)
            {
                status = false;
            }
            DateTime? eventDateTime = null;
            if (tpEventHour.Value != null)
            {
                DateTime eventTime = Convert.ToDateTime(tpEventHour.Value.ToString());
                eventDateTime = new DateTime(((DateTime)dpEventDate.SelectedDate).Year, ((DateTime)dpEventDate.SelectedDate).Month, ((DateTime)dpEventDate.SelectedDate).Day, eventTime.Hour, eventTime.Minute, eventTime.Second, eventTime.Millisecond);
            }
            else
            {
                if (dpEventDate.SelectedDate != null)
                {
                    eventDateTime = (DateTime)dpEventDate.SelectedDate;
                }
            }

            FillEmptyTextBoxes(new TextBox[] { txtAmountClaimed, txtAmountReceived });
            try
            {
                int claimId = 0;
                if (healthClaim == null)
                {
                    claimId = claimLogic.InsertHealthClaim(policy.HealthPolicyID, claimTypeId, claimConditionId, status, txtClaimNumber.Text, (DateTime)dpOpenDate.SelectedDate, dpDeliveryDateToCompany.SelectedDate, dpMoneyReceivedDate.SelectedDate, decimal.Parse(txtAmountClaimed.Text), decimal.Parse(txtAmountReceived.Text), eventDateTime, txtEventPlace.Text, txtEventDescription.Text);
                    if (healthPaths.Count()>0)
                    {
                        path = healthPaths[0] + @"/תביעה " + ((DateTime)dpEventDate.SelectedDate).ToString("dd-MM-yyyy");
                    }
                }
                else
                {
                    claimId = healthClaim.HealthClaimID;
                    claimLogic.UpdateHealthClaim(claimId, claimTypeId, claimConditionId, status, txtClaimNumber.Text, dpDeliveryDateToCompany.SelectedDate, dpMoneyReceivedDate.SelectedDate, decimal.Parse(txtAmountClaimed.Text), decimal.Parse(txtAmountReceived.Text), eventDateTime, txtEventPlace.Text, txtEventDescription.Text);
                    if (healthPaths.Count() > 0)
                    {
                        newPath = healthPaths[0] + @"/תביעה " + ((DateTime)dpEventDate.SelectedDate).ToString("dd-MM-yyyy");
                        path = healthPaths[0] + @"/תביעה " + ((DateTime)healthClaim.EventDateAndTime).ToString("dd-MM-yyyy");
                    }
                }
                SaveUnsavedWitness();

                if (witnesses.Count > 0)
                {
                    claimLogic.InsertWitnesses(witnesses, claimId);
                }
                if (trackings.Count > 0)
                {
                    trackingLogic.InsertHealthClaimTrackings(trackings, claimId, user.UserID);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "", MessageBoxButton.OK, MessageBoxImage.Error);
            }

            if (healthClaim == null)
            {
                if (path != null && path != "")
                {
                    if (!Directory.Exists(@path))
                    {
                        Directory.CreateDirectory(@path);
                    }
                }
                else
                {
                    System.Windows.Forms.MessageBox.Show("לא ניתן ליצור את תיקיית התביעה");
                }
            }
            else
            {
                if (path != "")
                {
                    if (Directory.Exists(path) && newPath != "")
                    {
                        if (path != newPath)
                        {
                            try
                            {
                                Directory.Move(path, newPath);
                            }
                            catch (Exception)
                            {
                                System.Windows.Forms.MessageBox.Show("לא ניתן למצוא את נתיב התיקייה");
                            }
                        }
                    }
                }
                else
                {
                    if (newPath != "" && newPath != null)
                    {
                        Directory.CreateDirectory(newPath);
                    }
                }
            }

            if (sender is Button)
            {
                Button btn = (Button)sender;
                if (btn.Name == "btnSave")
                {
                    confirmBeforeClosing = false;
                    Close();
                }
            }
        }

        private void SaveUnsavedWitness()
        {
            if (txtWitnessName.Text != "" || txtWitnessAddress.Text != "" || txtWitnessPhone.Text != "" || txtWitnessCellPhone.Text != "")
            {
                btnAddWitness_Click(this, new RoutedEventArgs());
            }
        }

        private void dgTrackings_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            DependencyObject src = (DependencyObject)(e.OriginalSource);
            while (!(src is Control))
            {
                if (!(src is Visual))
                {
                    break;
                }
                src = VisualTreeHelper.GetParent(src);
            }

            if ((src is Control && src.GetType() == typeof(ListViewItem)) || src.GetType() == typeof(Run))
            {
                if (dgTrackings.SelectedItem == null)
                {
                    HealthClaimTracking trackingToUpdate = (HealthClaimTracking)dgTrackings.SelectedItem;
                    frmTracking updateTrackingWindow = new frmTracking(trackingToUpdate, client, policy, user);
                    updateTrackingWindow.ShowDialog();
                    trackingToUpdate.TrackingContent = updateTrackingWindow.trackingContent;
                    dgTrackingsBinding();
                    dgTrackings.UnselectAll();
                }
            }
        }

        private void GroupBox_GotFocus(object sender, RoutedEventArgs e)
        {
            isChanges = true;
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (confirmBeforeClosing && isChanges)
            {
                if (MessageBox.Show("האם ברצונך לשמור שינוים שבצעת", "אישור", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    btnSave_Click(sender, new RoutedEventArgs());
                    if (cancelClose)
                    {
                        e.Cancel = true;
                        cancelClose = false;
                    }
                }
            }
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
