﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace CoralUI
{
    public class DateTimeConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            DateTime? selectedDate = value as DateTime?;

            if (selectedDate != null)
            {
                string dateTimeFormat = parameter as string;
                return selectedDate.Value.ToString(dateTimeFormat);
            }

            return "בחר תאריך";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            try
            {
                var valueToConvert = value as string;
                if (!string.IsNullOrEmpty(valueToConvert))
                {
                    string[] splitedValue = valueToConvert.Split('-');
                    return new DateTime(int.Parse(splitedValue[2]),int.Parse(splitedValue[1]),int.Parse(splitedValue[0]));
                    
                    //var retorno = DateTime.Parse(valor);
                    //return retorno;
                }

                return null;
            }
            catch
            {
                return DependencyProperty.UnsetValue;
            }
        }
    }
}
