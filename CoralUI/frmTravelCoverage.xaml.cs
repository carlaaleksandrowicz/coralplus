﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using CoralBusinessLogics;

namespace CoralUI
{
    /// <summary>
    /// Interaction logic for frmTravelCoverage.xaml
    /// </summary>
    public partial class frmTravelCoverage : Window
    {
        //TravelInsuranceType insurance = new TravelInsuranceType();
        CoveragesLogic coverageLogic = new CoveragesLogic();
        InputsValidations validations = new InputsValidations();
        List<TextBox> errorList = null;
        private bool isChanges=false;
        private bool confirmBeforeClosing=true;
        private bool cancelClose=false;

        public TravelCoverage NewCoverage { get; set; }
        public frmTravelCoverage()
        {
            InitializeComponent();
            this.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            cbCoverageTypeBinding();
        }

        public frmTravelCoverage(TravelCoverage coverage)
        {
            InitializeComponent();
            this.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            NewCoverage = coverage;
            cbCoverageTypeBinding();
            SelectItemInCb(NewCoverage.TravelCoverageTypeID);
            txtCoverageCode.Text = NewCoverage.TravelCoverageType.TravelCoverageCode;
            txtCoverageAmount.Text = validations.ConvertDecimalToString(NewCoverage.CoverageAmount);
            txtCoverageRate.Text = validations.ConvertDecimalToString(NewCoverage.Porcentage);
            txtPremium.Text = validations.ConvertDecimalToString(NewCoverage.premium);
            txtComments.Text = NewCoverage.Comments;
        }


        private void cbCoverageTypeBinding()
        {
            cbCoverageType.ItemsSource = coverageLogic.GetAllActiveTravelCoverageTypes();
            cbCoverageType.DisplayMemberPath = "TravelCoverageTypeName";
        }

        private void btnUpdateTable_Click(object sender, RoutedEventArgs e)
        {
            int? selectedItemId = null;
            if (cbCoverageType.SelectedItem != null)
            {
                selectedItemId = ((TravelCoverageType)cbCoverageType.SelectedItem).TravelCoverageTypeID;
            }
            TravelCoverageType coverageType = new TravelCoverageType();
            frmUpdateTable updateTravelCoverageTypesTable = new frmUpdateTable(coverageType);
            updateTravelCoverageTypesTable.ShowDialog();
            cbCoverageTypeBinding();
            if (updateTravelCoverageTypesTable.ItemAdded != null)
            {
                SelectItemInCb(((TravelCoverageType)updateTravelCoverageTypesTable.ItemAdded).TravelCoverageTypeID);
            }
            else if (selectedItemId != null)
            {
                SelectItemInCb((int)selectedItemId);
            }
        }

        private void SelectItemInCb(int id)
        {
            var items = cbCoverageType.Items;
            foreach (var item in items)
            {
                TravelCoverageType parsedItem = (TravelCoverageType)item;
                if (parsedItem.TravelCoverageTypeID == id)
                {
                    cbCoverageType.SelectedItem = item;
                    break;
                }
            }
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            if (errorList != null && errorList.Count > 0)
            {
                foreach (TextBox item in errorList)
                {
                    item.ClearValue(BorderBrushProperty);
                }
            }
            if (cbCoverageType.SelectedItem == null)
            {
                MessageBox.Show("סוג כיסוי הינו שדה חובה", "", MessageBoxButton.OK, MessageBoxImage.Error);
                cancelClose = true;
                return;
            }
            errorList = validations.OnlyNumbersValidation(new TextBox[] { txtCoverageAmount, txtCoverageRate, txtPremium });
            if (errorList.Count > 0)
            {
                MessageBox.Show("נא להזין מספרים או נקודה בלבד", "", MessageBoxButton.OK, MessageBoxImage.Error);
                cancelClose = true;
                return;
            }
            NewCoverage = new TravelCoverage();
            NewCoverage.TravelCoverageType = (TravelCoverageType)cbCoverageType.SelectedItem;
            NewCoverage.TravelCoverageTypeID = ((TravelCoverageType)cbCoverageType.SelectedItem).TravelCoverageTypeID;
            if (txtCoverageAmount.Text != "")
            {
                NewCoverage.CoverageAmount = decimal.Parse(txtCoverageAmount.Text);

            }
            if (txtCoverageRate.Text != "")
            {
                NewCoverage.Porcentage = decimal.Parse(txtCoverageRate.Text);

            }
            if (txtPremium.Text != "")
            {
                NewCoverage.premium = decimal.Parse(txtPremium.Text);
            }
            NewCoverage.Comments = txtComments.Text;
            NewCoverage.Date = DateTime.Now;

            if (sender is Button)
            {
                Button btn = (Button)sender;
                if (btn.Name == "btnSave")
                {
                    confirmBeforeClosing = false;
                    Close();
                }
            }
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void cbCoverageType_GotFocus(object sender, RoutedEventArgs e)
        {
            isChanges = true;
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (confirmBeforeClosing && isChanges)
            {
                if (MessageBox.Show("האם ברצונך לשמור שינוים שבצעת", "אישור", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    btnSave_Click(sender, new RoutedEventArgs());
                    if (cancelClose)
                    {
                        e.Cancel = true;
                        cancelClose = false;
                    }
                }
            }
        }
    }
}
