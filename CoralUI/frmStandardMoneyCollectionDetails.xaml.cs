﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using CoralBusinessLogics;

namespace CoralUI
{
    /// <summary>
    /// Interaction logic for frmStandardMoneyCollectionDetails.xaml
    /// </summary>
    public partial class frmStandardMoneyCollectionDetails : Window
    {
        InputsValidations validations = new InputsValidations();
        StandardMoneyCollectionLogic moneyCollectionLogic = new StandardMoneyCollectionLogic();
        private bool isChanges;
        bool confirmBeforeClosing = true;
        bool cancelClose = false;
        private object cbCompany;
        bool autoDbUpdate;
        Client client;
        User user;
        ListViewSettings lv = new ListViewSettings();


        public StandardMoneyCollection StandardMoneyCollection { get; set; }
        public List<StandardMoneyCollectionTracking> Trackings { get; set; }

        public frmStandardMoneyCollectionDetails(bool autoDbUpdate, User user)
        {
            InitializeComponent();
            this.autoDbUpdate = autoDbUpdate;
            this.user = user;
        }
        public frmStandardMoneyCollectionDetails(StandardMoneyCollection standardMoneyCollection, List<StandardMoneyCollectionTracking> trackings, bool autoDbUpdate, User user)
        {
            InitializeComponent();
            StandardMoneyCollection = standardMoneyCollection;
            Trackings = trackings;
            this.autoDbUpdate = autoDbUpdate;
            this.user = user;
        }


        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            cbMoneyCollectionTypeBinding();
            if (StandardMoneyCollection != null)
            {
                if (StandardMoneyCollection.MoneyCollectionAmount != null)
                {
                    txtMoneyCollectionAmount.Text = ((decimal)StandardMoneyCollection.MoneyCollectionAmount).ToString("0.##");
                }
                if (StandardMoneyCollection.MoneyCollectionTypeID != null)
                {
                    SelectMoneyCollectionInCb((int)StandardMoneyCollection.MoneyCollectionTypeID);
                }
                if (StandardMoneyCollection.IsStandardMoneyCollectionOpen == false || StandardMoneyCollection.IsStandardMoneyCollectionOpen == null)
                {
                    cbMoneyCollectionStatus.SelectedIndex = 1;
                }
                chbIsFirstLetterSended.IsChecked = StandardMoneyCollection.IsFirstLetterSended;
                chbIsPolicyCanceled.IsChecked = StandardMoneyCollection.IsPolicyCanceled;
                chbIsSecondLetterSended.IsChecked = StandardMoneyCollection.IsSecondLetterSended;
                dpPotencialCancelationDate.SelectedDate = StandardMoneyCollection.PotencialCancelationDate;
                txtComments.Text = StandardMoneyCollection.Comments;

                //Trackings = moneyCollectionLogic.GetTrakcingsByStsndardMoneyCollectio(StandardMoneyCollection);
                dgTrackingsBinding();
            }
        }

        private void SelectMoneyCollectionInCb(int moneyCollectionTypeID)
        {
            var moneyCollectionTypes = cbMoneyCollectionType.Items;
            foreach (var item in moneyCollectionTypes)
            {
                MoneyCollectionType type = (MoneyCollectionType)item;
                if (type.MoneyCollectionTypeID == moneyCollectionTypeID)
                {
                    cbMoneyCollectionType.SelectedItem = item;
                    break;
                }
            }
        }

        private void cbMoneyCollectionTypeBinding()
        {
            cbMoneyCollectionType.ItemsSource = moneyCollectionLogic.GetActiveMoneyCollectionTypes();
            cbMoneyCollectionType.DisplayMemberPath = "MoneyCollectionTypeName";
        }

        private void txtMoneyCollectionAmount_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (sender is TextBox)
            {
                TextBox txtBox = (TextBox)sender;
                txtBox.ClearValue(BorderBrushProperty);
                if (txtBox.Text != "")
                {
                    try
                    {
                        decimal reusult = validations.ConvertStringToDecimal(txtBox.Text);
                    }
                    catch (Exception ex)
                    {
                        txtBox.BorderBrush = System.Windows.Media.Brushes.Red;
                        System.Windows.Forms.MessageBox.Show(ex.Message);
                        txtBox.Text = "";
                    }
                }
            }
        }

        private void btnUpdateMoneyCollectionTypesTable_Click(object sender, RoutedEventArgs e)
        {
            int? selectedItemId = null;
            if (cbMoneyCollectionType.SelectedItem != null)
            {
                selectedItemId = ((MoneyCollectionType)cbMoneyCollectionType.SelectedItem).MoneyCollectionTypeID;
            }
            frmUpdateTable updateMoneyCollectionTypesTable = new frmUpdateTable(new MoneyCollectionType());
            updateMoneyCollectionTypesTable.ShowDialog();
            cbMoneyCollectionTypeBinding();

            if (updateMoneyCollectionTypesTable.ItemAdded != null)
            {
                SelectItemInCb(((MoneyCollectionType)updateMoneyCollectionTypesTable.ItemAdded).MoneyCollectionTypeID);
            }
            else if (selectedItemId != null)
            {
                SelectItemInCb((int)selectedItemId);
            }
        }

        private void SelectItemInCb(int id)
        {
            var items = cbMoneyCollectionType.Items;
            foreach (var item in items)
            {
                MoneyCollectionType parsedItem = (MoneyCollectionType)item;
                if (parsedItem.MoneyCollectionTypeID == id)
                {
                    cbMoneyCollectionType.SelectedItem = item;
                    break;
                }
            }
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            bool? isStandardMoneyCollectionOpen = true;
            if (cbMoneyCollectionStatus.SelectedIndex == 1)
            {
                isStandardMoneyCollectionOpen = false;
            }
            decimal? moneyCollectionAmount = null;
            if (txtMoneyCollectionAmount.Text != "")
            {
                moneyCollectionAmount = decimal.Parse(txtMoneyCollectionAmount.Text);
            }
            int? moneyCollectionTypeId = null;
            if (cbMoneyCollectionType.SelectedItem != null)
            {
                moneyCollectionTypeId = ((MoneyCollectionType)cbMoneyCollectionType.SelectedItem).MoneyCollectionTypeID;
            }
            if (StandardMoneyCollection == null)
            {
                StandardMoneyCollection = new StandardMoneyCollection()
                {
                    Comments = txtComments.Text,
                    IsFirstLetterSended = chbIsFirstLetterSended.IsChecked,
                    IsPolicyCanceled = chbIsPolicyCanceled.IsChecked,
                    IsSecondLetterSended = chbIsSecondLetterSended.IsChecked,
                    IsStandardMoneyCollectionOpen = isStandardMoneyCollectionOpen,
                    MoneyCollectionAmount = moneyCollectionAmount,
                    MoneyCollectionTypeID = moneyCollectionTypeId,
                    PotencialCancelationDate = dpPotencialCancelationDate.SelectedDate
                };
            }
            else
            {
                StandardMoneyCollection.Comments = txtComments.Text;
                StandardMoneyCollection.IsFirstLetterSended = chbIsFirstLetterSended.IsChecked;
                StandardMoneyCollection.IsPolicyCanceled = chbIsPolicyCanceled.IsChecked;
                StandardMoneyCollection.IsSecondLetterSended = chbIsSecondLetterSended.IsChecked;
                StandardMoneyCollection.IsStandardMoneyCollectionOpen = isStandardMoneyCollectionOpen;
                StandardMoneyCollection.MoneyCollectionAmount = moneyCollectionAmount;
                StandardMoneyCollection.MoneyCollectionTypeID = moneyCollectionTypeId;
                StandardMoneyCollection.PotencialCancelationDate = dpPotencialCancelationDate.SelectedDate;

                if (autoDbUpdate)
                {
                    moneyCollectionLogic.UpdateStandardMoneyCollection(StandardMoneyCollection);
                    moneyCollectionLogic.InsertTrackingsToStandardMoneyCollection(StandardMoneyCollection.StandardMoneyCollectionID,Trackings);
                }
            }
            if (sender is Button)
            {
                Button btn = (Button)sender;
                if (btn.Name == "btnSave")
                {
                    confirmBeforeClosing = false;
                    Close();
                }
            }
        }

        private void GroupBox_GotFocus(object sender, RoutedEventArgs e)
        {
            isChanges = true;
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (confirmBeforeClosing && isChanges)
            {
                if (MessageBox.Show("האם ברצונך לשמור שינוים שבצעת", "אישור", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    btnSave_Click(sender, new RoutedEventArgs());
                    if (cancelClose)
                    {
                        e.Cancel = true;
                        cancelClose = false;
                    }
                }
            }
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void btnNewTracking_Click(object sender, RoutedEventArgs e)
        {
            frmTracking trackingWindow = new frmTracking(false);
            trackingWindow.ShowDialog();
            if (trackingWindow.trackingContent != null)
            {
                if (Trackings==null)
                {
                    Trackings = new List<StandardMoneyCollectionTracking>();
                }
                Trackings.Add(new StandardMoneyCollectionTracking { TrackingContent = trackingWindow.trackingContent, Date = DateTime.Now, User = user, UserID=user.UserID });
                dgTrackingsBinding();
            }
        }

        private void dgTrackingsBinding()
        {
            dgTrackings.ItemsSource = Trackings.OrderByDescending(t => t.Date);
            lv.AutoSizeColumns(dgTrackings.View);
        }

        private void btnDeleteTracking_Click(object sender, RoutedEventArgs e)
        {
            if (dgTrackings.SelectedItem == null)
            {
                MessageBox.Show("נא לסמן מעקב בטבלה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            if (MessageBox.Show("למחוק מעקב", "אישור", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                Trackings.Remove((StandardMoneyCollectionTracking)dgTrackings.SelectedItem);
                dgTrackingsBinding();
            }
            else
            {
                dgTrackings.UnselectAll();
            }
        }

        private void btnUpdateTracking_Click(object sender, RoutedEventArgs e)
        {
            if (dgTrackings.SelectedItem == null)
            {
                MessageBox.Show("נא לסמן מעקב בטבלה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            StandardMoneyCollectionTracking trackingToUpdate = (StandardMoneyCollectionTracking)dgTrackings.SelectedItem;
            frmTracking updateTrackingWindow = new frmTracking(trackingToUpdate);
            updateTrackingWindow.ShowDialog();
            trackingToUpdate.TrackingContent = updateTrackingWindow.trackingContent;
            dgTrackingsBinding();
            dgTrackings.UnselectAll();
        }
    }
}
