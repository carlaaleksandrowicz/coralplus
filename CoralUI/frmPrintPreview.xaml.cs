﻿using Microsoft.Reporting.WinForms;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;
using CoralBusinessLogics;
using System.Collections.Generic;
using System.Drawing.Printing;
using System;
using System.IO;

namespace CoralUI
{
    /// <summary>
    /// Interaction logic for frmPrintPreview.xaml
    /// </summary>
    public partial class frmPrintPreview : Window
    {
        //System.Windows.Controls.ListView lstView;
        string reportName = "";
        PageSettings pg = new PageSettings();
        object data;
        string exeFolder = System.Windows.Forms.Application.StartupPath;
        string reportPath = "";


        //public frmPrintPreview(System.Windows.Controls.ListView lvToPrint)
        //{
        //    InitializeComponent();
        //    lstView = lvToPrint;
        //    data = lstView.ItemsSource;
        //}
        public frmPrintPreview(string reportName, object data)
        {
            try
            {
                InitializeComponent();
                this.reportName = reportName;
                this.data = data;
            }
            catch(Exception ex)
            {
                System.Windows.Forms.MessageBox.Show("3" + ex.Message);
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                rpViewer.ProcessingMode = ProcessingMode.Local;
                LocalReport localReport = rpViewer.LocalReport;
                rpViewer.SetDisplayMode(DisplayMode.PrintLayout);
                rpViewer.RightToLeft = RightToLeft.Yes;
                ReportDataSource datasource = null;


                if (reportName == "ClientsReport")
                {
                    //localReport.ReportPath = "/RDLC/Clients.rdlc";
                    //localReport.ReportPath =  AppDomain.CurrentDomain.BaseDirectory + @"RDLC\Clients.rdlc";
                    //reportPath = Path.Combine(exeFolder, @"RDLC\Clients.rdlc");
                    //reportPath = AppDomain.CurrentDomain.BaseDirectory + @"RDLC\Clients.rdlc";
                    rpViewer.LocalReport.ReportEmbeddedResource = "CoralUI.RDLC.Clients.rdlc";
                  //  rpViewer.LocalReport.ReportPath = AppDomain.CurrentDomain.BaseDirectory + @"RDLC\Clients.rdlc";
                    datasource = new ReportDataSource("ClientDataSet", data);
                    pg.Landscape = true;
                }
                else if (reportName == "ContactsReport")
                {
                    //localReport.ReportPath = @"C:\repo\coral\CoralUI\RDLC\Contacts.rdlc";
                    //localReport.ReportPath = AppDomain.CurrentDomain.BaseDirectory + @"RDLC\Contacts.rdlc";
                    //reportPath = Path.Combine(exeFolder, @"RDLC\Contacts.rdlc");
                    //reportPath = AppDomain.CurrentDomain.BaseDirectory + @"RDLC\Contacts.rdlc";
                    rpViewer.LocalReport.ReportEmbeddedResource = "CoralUI.RDLC.Contacts.rdlc";
                    datasource = new ReportDataSource("ContactsDataSet", data);
                    pg.Landscape = true;
                }
                else if (reportName == "StandardMoneyCollectionReport")
                {
                    //localReport.ReportPath = AppDomain.CurrentDomain.BaseDirectory + @"RDLC\StandardMoneyCollection.rdlc";
                    //reportPath = Path.Combine(exeFolder, @"RDLC\StandardMoneyCollection.rdlc");
                    rpViewer.LocalReport.ReportEmbeddedResource = "CoralUI.RDLC.StandardMoneyCollection.rdlc";
                    datasource = new ReportDataSource("StandardMoneyCollectionDataSet", data);
                    pg.Landscape = true;
                }
                else if (reportName == "RenewalsReport")
                {
                    //localReport.ReportPath = AppDomain.CurrentDomain.BaseDirectory + @"RDLC\Renewals.rdlc";
                    //reportPath = Path.Combine(exeFolder, @"RDLC\Renewals.rdlc");
                    rpViewer.LocalReport.ReportEmbeddedResource = "CoralUI.RDLC.Renewals.rdlc";
                    datasource = new ReportDataSource("RenewalsDataSet", data);
                    pg.Landscape = true;
                }
                else if (reportName == "PoliciesReport")
                {
                    //localReport.ReportPath = AppDomain.CurrentDomain.BaseDirectory + @"RDLC\Policies.rdlc";
                    //reportPath = Path.Combine(exeFolder, @"RDLC\Policies.rdlc");
                    rpViewer.LocalReport.ReportEmbeddedResource = "CoralUI.RDLC.Policies.rdlc";
                    datasource = new ReportDataSource("PoliciesDataSet", data);
                    pg.Landscape = true;
                }
                else if (reportName == "ClaimsReport")
                {
                    //localReport.ReportPath = AppDomain.CurrentDomain.BaseDirectory + @"RDLC\Claims.rdlc";
                    //reportPath = Path.Combine(exeFolder, @"RDLC\Claims.rdlc");
                    rpViewer.LocalReport.ReportEmbeddedResource = "CoralUI.RDLC.Claims.rdlc";
                    datasource = new ReportDataSource("ClaimsDataSet", data);
                    pg.Landscape = true;
                }
                if (reportPath != "")
                {
                    localReport.ReportPath = reportPath;
                }
                rpViewer.LocalReport.DataSources.Clear();
                if (datasource != null)
                {
                    rpViewer.LocalReport.DataSources.Add(datasource);
                }

                rpViewer.SetPageSettings(pg);
                rpViewer.RefreshReport();
            }
            catch(Exception ex)
            {
                System.Windows.Forms.MessageBox.Show("2" + ex.Message);
            }
        }
    }
}
