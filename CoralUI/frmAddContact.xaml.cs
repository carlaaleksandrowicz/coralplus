﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using CoralBusinessLogics;

namespace CoralUI
{
    /// <summary>
    /// Interaction logic for frmAddContact.xaml
    /// </summary>
    public partial class frmAddContact : Window
    {
        ContactsLogic logic;
        ContactCategoriesLogic categoryLogic = new ContactCategoriesLogic();
        Contact contactToUpdate = null;
        bool isAdvanceSearch = false;
        public List<Contact> searchResult = null;
        private bool isChanges = false;
        bool confirmBeforeClosing = true;


        //בנאי להוספת איש קשר חדש
        public frmAddContact()
        {
            InitializeComponent();
        }

        //בנאי לעדכון איש קשר קיים
        public frmAddContact(Contact contact)
        {
            InitializeComponent();
            contactToUpdate = contact;
        }

        //בנאי לחיפוש מתקדם
        public frmAddContact(bool isSearch)
        {
            InitializeComponent();
            isAdvanceSearch = isSearch;
            lblAddContact.Content = "חפש איש קשר";
            //txtBtnSave.Text = "חפש";
            var updateUriSource = new Uri(@"/IconsMind/Magnifi-Glass_30px.png", UriKind.Relative);
            imgBtnSave.Source = new BitmapImage(updateUriSource);
        }
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            ComboBoxBinding();
            if (contactToUpdate != null)
            //ממלא את השדות לפי פרטי איש הקשר שרוצוי לעדכן
            {
                txtLastName.Text = contactToUpdate.LastName;
                txtFirstName.Text = contactToUpdate.FirstName;
                txtCompany.Text = contactToUpdate.CompanyName;
                txtCity.Text = contactToUpdate.City;
                txtStreet.Text = contactToUpdate.Street;
                txtHouseNumber.Text = contactToUpdate.HomeNumber;
                txtZipCode.Text = contactToUpdate.ZipCode;
                txtPhone.Text = contactToUpdate.Phone;
                txtCellPhone.Text = contactToUpdate.CellPhone;
                txtFax1.Text = contactToUpdate.Fax;
                txtEmail.Text = contactToUpdate.Email;
                txtWebsite.Text = contactToUpdate.Website;
                txtComments.Text = contactToUpdate.Remarks;
                if (contactToUpdate.ContactCategoryID != null)
                {
                    SelectCategory((int)contactToUpdate.ContactCategoryID);
                }
            }
        }

        //מקשר את הקומבו בוקס עם רשימת הקטגוריות הפעילות של אנשי הקשר
        private void ComboBoxBinding()
        {
            cbCategory.ItemsSource = categoryLogic.GetActiveCategories();
            cbCategory.DisplayMemberPath = "ContactCategoryName";
        }

        //סוגר את החלון
        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        //שומר / מעדכן / מחפש איש קשר
        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                logic = new ContactsLogic();
                ContactCategory category = (ContactCategory)cbCategory.SelectedItem;
                if (contactToUpdate == null && !isAdvanceSearch)
                {
                    logic.InsertContact(txtLastName.Text, txtFirstName.Text, txtCompany.Text, category, txtCity.Text, txtStreet.Text, txtHouseNumber.Text, txtZipCode.Text, txtPhone.Text, txtCellPhone.Text, txtFax1.Text, txtEmail.Text, txtWebsite.Text, txtComments.Text);
                }
                if (contactToUpdate != null)
                {
                    logic.UpdateContact(contactToUpdate.ContactID, txtLastName.Text, txtFirstName.Text, txtCompany.Text, category, txtCity.Text, txtStreet.Text, txtHouseNumber.Text, txtZipCode.Text, txtPhone.Text, txtCellPhone.Text, txtFax1.Text, txtEmail.Text, txtWebsite.Text, txtComments.Text);
                }
                if (isAdvanceSearch)
                {
                    searchResult = logic.FindContactAdvanceSearch(txtLastName.Text, txtFirstName.Text, txtCompany.Text, category, txtCity.Text, txtStreet.Text, txtHouseNumber.Text, txtZipCode.Text, txtPhone.Text, txtCellPhone.Text, txtFax1.Text, txtEmail.Text, txtWebsite.Text, txtComments.Text);
                }
                if (sender is Button)
                {
                    Button btn = (Button)sender;
                    if (btn.Name == "btnSave")
                    {
                        confirmBeforeClosing = false;
                        Close();
                    }
                }
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
            }
        }

        //פותח את חלון עדכון טבלאות
        private void btnUpdateCategoriesTable_Click(object sender, RoutedEventArgs e)
        {
            int? selectedItemId = null;
            if (cbCategory.SelectedItem != null)
            {
                selectedItemId = ((ContactCategory)cbCategory.SelectedItem).ContactCategoryID;
            }
            ContactCategory contactCategory = new ContactCategory();
            frmUpdateTable updateTable = new frmUpdateTable(contactCategory);
            updateTable.ShowDialog();
            ComboBoxBinding();
            if (updateTable.ItemAdded != null)
            {
                SelectCategory(((ContactCategory)updateTable.ItemAdded).ContactCategoryID);
            }
            else if (selectedItemId != null)
            {
                SelectCategory((int)selectedItemId);
            }
        }

        private void SelectCategory(int categoryId)
        {
            var categories = cbCategory.Items;
            foreach (var item in categories)
            {
                ContactCategory category = (ContactCategory)item;
                if (category.ContactCategoryID == categoryId)
                {
                    cbCategory.SelectedItem = item;
                    break;
                }
            }
        }

        private void txtLastName_GotFocus(object sender, RoutedEventArgs e)
        {
            isChanges = true;
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (confirmBeforeClosing && isChanges)
            {
                if (MessageBox.Show("האם ברצונך לשמור שינוים שבצעת", "אישור", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    btnSave_Click(sender, new RoutedEventArgs());
                    //if (cancelClose)
                    //{
                    //    e.Cancel = true;
                    //    cancelClose = false;
                    //}
                }
            }
        }
    }
}
