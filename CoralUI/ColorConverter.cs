﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Windows.Data;
using CoralBusinessLogics;

namespace CoralUI
{
    public class ColorConverter:IValueConverter
    {
        public Color Gray { get; set; }
        public Color Green { get; set; }
        public Color Yellow { get; set; }
        public Color Red { get; set; }
        public Color Orange { get; set; }
        public Color Purple { get; set; }


        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value!=null)
            {
                if (value is Request)
                {
                    if (((Request)value).RequestStatus==true)
                    {
                        if (((Request)value).RequestDueDate > DateTime.Now)
                        {
                            return Green.ToString();
                        }
                        else if (((Request)value).RequestDueDate< DateTime.Now)
                        {
                            return Red.ToString();
                        }
                        else
                        {
                            return null;
                        } 
                    }
                    else
                    {
                        return Gray.ToString();
                    }
                }
                else
                {
                    if (value.ToString() == "לא חודש")
                    {
                        return Gray.ToString();
                    }
                    else if (value.ToString() == "חודש")
                    {
                        return Green.ToString();
                    }
                    else if (value.ToString() == "בתהליך")
                    {
                        return Yellow.ToString();
                    }
                    else if (value.ToString() == "לא טופל")
                    {
                        return Red.ToString();
                    }
                    else if (value.ToString()=="בהפקה")
                    {
                        return Purple.ToString();
                    }
                    else
                    {
                        return Orange.ToString();
                    }
                } 
            }
            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

}
