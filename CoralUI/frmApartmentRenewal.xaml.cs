﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using CoralBusinessLogics;

namespace CoralUI
{
    /// <summary>
    /// Interaction logic for frmApartmentRenewal.xaml
    /// </summary>
    public partial class frmApartmentRenewal : Window
    {
        RenewalsLogic renewalLogic = new RenewalsLogic();
        Client client;
        ElementaryPolicy policy;
        StructureTypesLogic structuresLogic = new StructureTypesLogic();
        //Renewal renewal = null;
        InsurancesLogic insuranceLogic = new InsurancesLogic();
        User user;
        PoliciesLogic policiesLogics = new PoliciesLogic();
        ListViewSettings lvSettings = new ListViewSettings();
        InputsValidations validation = new InputsValidations();
        frmAddClient clientDetailsWindow;
        frmRenewalTrackingManager trackingWindow;
        List<ElementaryPolicy> tempList = new List<ElementaryPolicy>();
        ClientsLogic clientLogic = new ClientsLogic();

        public frmApartmentRenewal(Client clientRenewal, ElementaryPolicy policyToRenew, User userAccount)
        {
            InitializeComponent();
            client = clientRenewal;
            user = userAccount;
            policy = policyToRenew;            
            tempList.Add(policy);
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (policy.RenewalStatus.RenewalStatusName == "חודש")
            {
                cbRenewalStatus.IsEnabled = false;
                gbQuotes.IsEnabled = false;
                btnPolicyRenewal.IsEnabled = false;
                btnDeleteQuote.IsEnabled = false;

                //spQuotesButtons.IsEnabled = false;
                //spTrackingButtons.IsEnabled = false;
            }
            else if (policy.RenewalStatus.RenewalStatusName == "לא טופל")
            {
                string status = "פתוח לעבודה";
                policy.RenewalStatusID = policiesLogics.UpdatePolicyRenewalStatus(policy, status);
            }
            cbRenewalStatusBinding();
            cbCompanyBinding();
            SetClientDetails();
            cbAdditionBinding();
            txtPolicyNumber.Text = policy.PolicyNumber;            
            txtInsuranceCompany.Text = policy.Company.CompanyName;            
            if (policy.IsMortgaged == true)
            {
                chbIsMortgaged.IsChecked = true;
                chbIsMortgaged.FontWeight = FontWeights.Bold;
            }
            if (policy.ApartmentPolicy != null)
            {
                txtAddress.Text = string.Format("{0} {1}, {2}", policy.ApartmentPolicy.Street, policy.ApartmentPolicy.HomeNumber, policy.ApartmentPolicy.City);
                //string structure = (structuresLogic.GetStructureTypeById((int)policy.ApartmentPolicy.StructureTypeID)).StructureTypeName;
                if (policy.ApartmentPolicy.StructureTypeID != null)
                {
                    StructureType structure = (structuresLogic.GetStructureTypeById((int)policy.ApartmentPolicy.StructureTypeID));
                    if (structure != null)
                    {
                        txtStructureType.Text = structure.StructureTypeName;
                    }
                }
            }
            else if (policy.BusinessPolicy != null)
            {
                txtAddress.Text = string.Format("{0} {1}, {2}", policy.BusinessPolicy.Street, policy.BusinessPolicy.HomeNumber, policy.BusinessPolicy.City);
                if (policy.BusinessPolicy.StructureTypeID != null)
                {
                    StructureType structure = (structuresLogic.GetStructureTypeById((int)policy.BusinessPolicy.StructureTypeID));
                    if (structure != null)
                    {
                        txtStructureType.Text = structure.StructureTypeName;
                    }
                }
            }
            dgQuotesBinding();
            frmRenewalTrackingManager.TrackingsCountChange += new EventHandler(renewalsWindow_TrackingsCountChange);

            lblTrackingCountBinding();

        }

        private void SetClientDetails()
        {
            txtClientName.Text = client.FirstName + " " + client.LastName;
            txtEmail.Text = client.Email;
            txtPhone.Text = client.PhoneHome;
            txtCellPhone.Text = client.CellPhone;
            string clientDetails;
            if (client.CellPhone != null && client.CellPhone != "")
            {
                clientDetails = string.Format("{0} {1}  ת.ז: {2} נייד: {3}", client.FirstName, client.LastName, client.IdNumber, client.CellPhone);
            }
            else
            {
                clientDetails = string.Format("{0} {1}  ת.ז: {2}", client.FirstName, client.LastName, client.IdNumber);
            }
            lblClientNameAndId.Content = clientDetails;
        }

        void renewalsWindow_TrackingsCountChange(object sender, EventArgs e)
        {
            lblTrackingCountBinding();
        }
        private void lblTrackingCountBinding()
        {
            lblTrackingCount.Content = (renewalLogic.GetTrackingsCountByPolicyId(policy.ElementaryPolicyID)).ToString();
        }

        private void cbAdditionBinding()
        {
            List<ElementaryPolicy> policies= policiesLogics.GetPoliciesByPolicyNumberAndIndustry(policy.PolicyNumber, policy.InsuranceIndustryID);
            cbAddition.ItemsSource = policies;
            cbAddition.DisplayMemberPath = "Addition";
            SelectAddition();
            decimal? totalPremium = policies.Sum(p => p.TotalPremium);
            lblTotalPremium.Content = string.Format("{0} ש''ח",((decimal)totalPremium).ToString("0.##"));
        }

        private void SelectAddition()
        {
            var items = cbAddition.Items;
            foreach (var item in items)
            {
                ElementaryPolicy tempItem = (ElementaryPolicy)item;
                if (tempItem.ElementaryPolicyID == policy.ElementaryPolicyID)
                {
                    cbAddition.SelectedItem = item;
                    break;
                }
            }
        }

        private void cbCompanyBinding()
        {
            cbCompany.ItemsSource = insuranceLogic.GetActiveCompaniesByInsurance("אלמנטרי");
            cbCompany.DisplayMemberPath = "Company.CompanyName";
        }

        private void btnUpdateStatusTable_Click(object sender, RoutedEventArgs e)
        {
            //frmUpdateTable updateRenewalStatusTable = new frmUpdateTable(new RenewalStatus());
            //updateRenewalStatusTable.ShowDialog();
            //cbRenewalStatusBinding();
        }

        private void cbRenewalStatusBinding()
        {
            cbRenewalStatus.ItemsSource = renewalLogic.GetAllActiveRenewalStatuses();
            cbRenewalStatus.DisplayMemberPath = "RenewalStatusName";
            string statusName;
            statusName = (renewalLogic.GetRenewalStatusByRenewalStatusId((int)policy.RenewalStatusID)).RenewalStatusName;
            SelectStatusInCb(statusName);
        }

        private void SelectStatusInCb(string statusName)
        {
            var renewalStatuses = cbRenewalStatus.Items;
            foreach (var item in renewalStatuses)
            {
                if (((RenewalStatus)item).RenewalStatusName == statusName)
                {
                    cbRenewalStatus.SelectedItem = item;
                    break;
                }
            }
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void btnAddQuote_Click(object sender, RoutedEventArgs e)
        {
            if (txtNewPremia.Text == "" || cbCompany.SelectedItem == null || txtNewPolicyNumber.Text == "")
            {
                MessageBox.Show("נא למלאות את כל שדות ההצעה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            renewalLogic.InsertAptOrBusinessRenewalQuote(policy, txtNewPolicyNumber.Text, int.Parse(txtNewPremia.Text), (InsuranceCompany)cbCompany.SelectedItem);
            dgQuotesBinding();
            txtNewPolicyNumber.Clear();
            txtNewPremia.Clear();
            cbCompany.SelectedIndex = -1;
        }

        private void dgQuotesBinding()
        {
            //get all quotes
            dgQuotes.ItemsSource = renewalLogic.GetAptOrBusinessRenewalQuotesByRenewalId(policy.ElementaryPolicyID);
            lvSettings.AutoSizeColumns(dgQuotes.View);
        }

        private void btnUpdateCompanyTable_Click(object sender, RoutedEventArgs e)
        {
            int? selectedItemId = null;
            if (cbCompany.SelectedItem != null)
            {
                selectedItemId = ((InsuranceCompany)cbCompany.SelectedItem).CompanyID;
            }
            int insuranceID = insuranceLogic.GetInsuranceID("אלמנטרי");
            InsuranceCompany company = new InsuranceCompany() { InsuranceID = insuranceID };
            frmUpdateTable updateCompaniesTable = new frmUpdateTable(company);
            updateCompaniesTable.ShowDialog();
            cbCompanyBinding();
            if (updateCompaniesTable.ItemAdded != null)
            {
                SelectItemInCb(((InsuranceCompany)updateCompaniesTable.ItemAdded).CompanyID);
            }
            else if (selectedItemId != null)
            {
                SelectItemInCb((int)selectedItemId);
            }
        }

        private void SelectItemInCb(int id)
        {
            var items = cbCompany.Items;
            foreach (var item in items)
            {
                InsuranceCompany parsedItem = (InsuranceCompany)item;
                if (parsedItem.CompanyID == id)
                {
                    cbCompany.SelectedItem = item;
                    break;
                }
            }
        }

        private void btnDeleeQuote_Click(object sender, RoutedEventArgs e)
        {
            if (dgQuotes.SelectedItem != null)
            {
                renewalLogic.DeleteAptOrBusinessQuote((ApartmentRenewalQuote)dgQuotes.SelectedItem);
                dgQuotesBinding();
            }
            else
            {
                MessageBox.Show("נא לסמן הצעה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
        }

        private void btnAddTracking_Click(object sender, RoutedEventArgs e)
        {
            frmTracking newTrackingWindow = new frmTracking(client, policy, user);
            newTrackingWindow.ShowDialog();
            if (newTrackingWindow.trackingContent != null)
            {
                renewalLogic.InsertRenewalTracking(policy.ElementaryPolicyID, user.UserID, DateTime.Now, newTrackingWindow.trackingContent);
                //dgTrackingBinding();
            }
        }

        //private void dgTrackingBinding()
        //{
        //    dgTracking.ItemsSource = renewalLogic.GetTrackingsByRenewalId(policy.ElementaryPolicyID);
        //    lvSettings.AutoSizeColumns(dgTracking.View);

        //}

        //private void btnDeleteTracking_Click(object sender, RoutedEventArgs e)
        //{
        //    if (dgTracking.SelectedItem == null)
        //    {
        //        MessageBox.Show("נא לסמן מעקב", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
        //        return;
        //    }
        //    renewalLogic.DeleteTracking((RenewalTracking)dgTracking.SelectedItem);
        //    dgTrackingBinding();
        //}

        //private void btnUpdateTracking_Click(object sender, RoutedEventArgs e)
        //{
        //    if (dgTracking.SelectedItem == null)
        //    {
        //        MessageBox.Show("נא לסמן מעקב", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
        //        return;
        //    }
        //    frmTracking updateTrackingWindow = new frmTracking((RenewalTracking)dgTracking.SelectedItem, client, policy, user);
        //    updateTrackingWindow.ShowDialog();
        //    renewalLogic.UpdateRenewalTracking((RenewalTracking)dgTracking.SelectedItem, updateTrackingWindow.trackingContent);
        //    dgTrackingBinding();
        //    dgTracking.UnselectAll();

        //}

        private void btnPolicyArchive_Click(object sender, RoutedEventArgs e)
        {
            frmScan2 archiveWindow = new frmScan2(client, policy, null,user);
            archiveWindow.ShowDialog();
        }

        private void btnPolicyRenewal_Click(object sender, RoutedEventArgs e)
        {
            if (dgQuotes.SelectedItem == null)
            {
                MessageBox.Show("נא לסמן הצעה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            frmElementary renewElementaryPolicy = new frmElementary(client, user, true, policy, (ApartmentRenewalQuote)dgQuotes.SelectedItem, null);
            renewElementaryPolicy.ShowDialog();
            if (renewElementaryPolicy.renewalAccepted)
            {
                policy.RenewalStatusID = policiesLogics.UpdatePolicyRenewalStatus(policy, "חודש");
                SelectStatusInCb("חודש");
                dgQuotes.UnselectAll();
                dgQuotes.IsEnabled = false;
                cbRenewalStatus.IsEnabled = false;
                gbQuotes.IsEnabled = false;
            }
        }

        private void cbRenewalStatus_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (true)
            {
                policy.RenewalStatusID = policiesLogics.UpdatePolicyRenewalStatus(policy, ((RenewalStatus)cbRenewalStatus.SelectedItem).RenewalStatusName);

            }            //if (renewal == null)
                         //{
                         //    renewal=renewalLogic.InsertRenewal(policy.ElementaryPolicyID, ((RenewalStatus)cbRenewalStatus.SelectedItem).RenewalStatusID);

            //}
            //else
            //{
            //    if (cbRenewalStatus.SelectedItem!=null)
            //    {
            //        renewalLogic.UpdateRenewal(renewal, ((RenewalStatus)cbRenewalStatus.SelectedItem).RenewalStatusID);

            //    }
            //}
        }

        private void txtNewPremia_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (sender is TextBox)
            {
                TextBox txtBox = (TextBox)sender;
                txtBox.ClearValue(BorderBrushProperty);
                if (txtBox.Text != "")
                {
                    try
                    {
                        decimal reusult = validation.ConvertStringToDecimal(txtBox.Text);
                    }
                    catch (Exception ex)
                    {
                        txtBox.BorderBrush = System.Windows.Media.Brushes.Red;
                        System.Windows.Forms.MessageBox.Show(ex.Message);
                        txtBox.Text = "";
                    }
                }
            }
        }

        private void dgQuotes_MouseDown(object sender, MouseButtonEventArgs e)
        {
            dgQuotes.UnselectAll();
        }

        //private void dgTracking_MouseDown(object sender, MouseButtonEventArgs e)
        //{
        //    dgTracking.UnselectAll();
        //}

        private void btnCalculator_Click(object sender, RoutedEventArgs e)
        {
            System.Diagnostics.Process.Start("calc");
        }

        private void btnClientDetails_Click(object sender, RoutedEventArgs e)
        {
            if (client != null)
            {
                clientDetailsWindow = new frmAddClient(client, user);
                clientDetailsWindow.ShowDialog();
                client = clientLogic.GetClientByClientID(client.ClientID);
                SetClientDetails();         
            }
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (trackingWindow != null && trackingWindow.IsLoaded)
            {
                trackingWindow.Close();
            }
            if (clientDetailsWindow != null && clientDetailsWindow.IsLoaded)
            {
                clientDetailsWindow.Close();
            }
        }

        private void btnTracking_Click(object sender, RoutedEventArgs e)
        {            
            trackingWindow = new frmRenewalTrackingManager(policy, tempList, user);
            trackingWindow.Show();
        }

        private void cbAddition_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            txtPremia.Text = ((decimal)((ElementaryPolicy)cbAddition.SelectedItem).TotalPremium).ToString("0.##");
        }
    }
}
