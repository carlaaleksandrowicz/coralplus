﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using LogicNP.FileViewControl;
using LogicNP;
using CoralBusinessLogics;
using GdPicture10;
using System.Windows.Interop;
using System.Drawing;
using System.IO;
using System.Drawing.Printing;

namespace CoralUI
{
    /// <summary>
    /// Interaction logic for frmScan2.xaml
    /// </summary>
    public partial class frmScan2 : Window
    {
        public GdPictureImaging gdPicture = new GdPictureImaging();
        Client clientArchive;
        frmAddClient clientDetailsWindow = null;
        PoliciesLogic policyLogic = new PoliciesLogic();
        HealthPoliciesLogic healthPolicyLogic = new HealthPoliciesLogic();
        LifePoliciesLogic lifePolicyLogic = new LifePoliciesLogic();
        ClientsLogic clientLogic = new ClientsLogic();
        DocumentNamesLogic docNameLogic = new DocumentNamesLogic();
        FinancePoliciesLogic financeLogic = new FinancePoliciesLogic();
        PersonalAccidentsLogic personalAccidentLogic = new PersonalAccidentsLogic();
        TravelLogic travelLogic = new TravelLogic();
        ForeingWorkersLogic foreingWorkersLogic = new ForeingWorkersLogic();
        Email email = new Email();
        object policySelected = null;
        string rootPath;
        string folderPath;
        string clientsPath;
        string strAppDir;
        User user;
        private PolicyOwner policyOwner;

        public frmScan2(Client client, object policy, string path, User user)
        {
            InitializeComponent();
            string prefix = @"\\192.168.1.1\IntraNet\";
            //string licKey = prefix + "IQAAAFNoYXJvbiBTaGFvdWwqKmluZm9AMnN0YXJzLmNvLmlsACfI/cKGLL4yJKKGeabUrfiw4WU+d9b0TSxrCYxfeH8gLye4U+r6ffQxI3x9MifeTA==";
            string licKey = prefix + "IP?>AELlYWHsbh@QaFDsdVunKljrZl7>MmL-YWHwLlLsLljpABdF/bIDLK2vJJIDe``Rregt4VS(d8`-TRvoCXvceG6dLxc1U*p3feOuI2v6MhdbT@";

            folderView.SelectNode(licKey, false, false);
            //string fileLicKey = prefix + "IQAAAFNoYXJvbiBTaGFvdWwqKmluZm9AMnN0YXJzLmNvLmlsAGxqaM8iRWKUaO4ZzPRfRcQs9Z7ZRc1LyzXMvSgqMG3tmRHlsCsM4slMzSedisseKA==";
            string fileLicKey = prefix + "IP?>AELlYWHsbh@QaFDsdVunKljrZl7>MmL-YWHwLlLsLljpAFvnaL6fRVIRaN2WzOPcRbOp9Y5WRb/IyyVJvRenMF1qmQFisBqJ4rjJzRcairqbK@";

            fileView.CurrentFolder = fileLicKey;
            this.SizeToContent = SizeToContent.Manual;
            clientArchive = client;
            lblTitle_ClientName.Content = clientArchive.FirstName + " " + clientArchive.LastName;
            policySelected = policy;
            cbPolicyDetailsBinding();
            cbDocumentNameBinding();
            cbScanSourceBinding();
            //Creating a License Manager object
            LicenseManager oLicenseManager = new LicenseManager();
            oLicenseManager.RegisterKEY("132999999928386951626123255863780");
            folderPath = path;
            this.user = user;
            fileView.SortByColumn(null, 2, SortOrders.Descending);
        }

        public frmScan2(PolicyOwner policyOwner)
        {
            InitializeComponent();
            string prefix = @"\\192.168.1.1\IntraNet\";
            //string licKey = prefix + "IQAAAFNoYXJvbiBTaGFvdWwqKmluZm9AMnN0YXJzLmNvLmlsACfI/cKGLL4yJKKGeabUrfiw4WU+d9b0TSxrCYxfeH8gLye4U+r6ffQxI3x9MifeTA==";
            string licKey = prefix + "IP?>AELlYWHsbh@QaFDsdVunKljrZl7>MmL-YWHwLlLsLljpABdF/bIDLK2vJJIDe``Rregt4VS(d8`-TRvoCXvceG6dLxc1U*p3feOuI2v6MhdbT@";

            folderView.SelectNode(licKey, false, false);
            //string fileLicKey = prefix + "IQAAAFNoYXJvbiBTaGFvdWwqKmluZm9AMnN0YXJzLmNvLmlsAGxqaM8iRWKUaO4ZzPRfRcQs9Z7ZRc1LyzXMvSgqMG3tmRHlsCsM4slMzSedisseKA==";
            string fileLicKey = prefix + "IP?>AELlYWHsbh@QaFDsdVunKljrZl7>MmL-YWHwLlLsLljpAFvnaL6fRVIRaN2WzOPcRbOp9Y5WRb/IyyVJvRenMF1qmQFisBqJ4rjJzRcairqbK@";

            fileView.CurrentFolder = fileLicKey;
            this.SizeToContent = SizeToContent.Manual;
            //clientArchive = client;
            lblTitle_ClientName.Content = policyOwner.PolicyOwnerName;
            //policySelected = policy;
            //cbPolicyDetailsBinding();
            cbDocumentNameBinding();
            cbScanSourceBinding();
            //Creating a License Manager object
            LicenseManager oLicenseManager = new LicenseManager();
            oLicenseManager.RegisterKEY("132999999928386951626123255863780");
            //folderPath = path;
            //this.user = user;
            fileView.SortByColumn(null, 2, SortOrders.Descending);

            this.policyOwner = policyOwner;
        }

        private void scanWindow_Loaded(object sender, RoutedEventArgs e)
        {

            cbResolution.SelectedIndex = 1;
            folderView.FileView = fileView;

            strAppDir = System.AppDomain.CurrentDomain.BaseDirectory;
            string path = strAppDir + @"Coral Files\ClientsPath.txt";

            if (!File.Exists(path))
            {
                MessageBox.Show("לא נמצא תיקיית הלקוחות. נא בדוק בהגדרות ראשוניות את נתיב התיקייה");
                Close();
                return;
            }
            clientsPath = File.ReadAllText(path);
            m_GDViewer.ZoomMode = ViewerZoomMode.ZoomModeWidthViewer;


            if (clientArchive != null)
            {
                cbChooseClient.ItemsSource = clientLogic.GetAllClients();
                rootPath = clientsPath + @"\" + clientArchive.ClientID;
            }
            else
            {
                cbChooseClient.IsEnabled = false;
                btnMoveToClient.IsEnabled = false;
                rootPath = clientsPath + @"\PolicyOwners\" + policyOwner.PolicyOwnerID;
            }

            if (policySelected != null)
            {
                SelectRootFolder(policySelected);
            }
            else
            {
                folderView.RootFolder = @rootPath;
            }

            fileView.AllowMultipleSelection = true;
            if (File.Exists(strAppDir + @"Coral Files\MailSettings.txt") && File.ReadAllText(strAppDir + @"Coral Files\MailSettings.txt") == rbMailToClient.Content.ToString())
            {
                rbMailToClient.IsChecked = true;
            }
            else
            {
                rbMailWithoutAddress.IsChecked = true;
            }

            SetSettings();

        }

        private void SetSettings()
        {
            if (!Directory.Exists(strAppDir + @"Coral Files\"))
            {
                Directory.CreateDirectory(strAppDir + @"Coral Files\");
            }
            SelectSource(strAppDir + @"Coral Files\");
            SelectResolution(strAppDir + @"Coral Files\");
            SelectFileType(strAppDir + @"Coral Files\");
            SelectColor(strAppDir + @"Coral Files\");
            SelectOtherSettings(strAppDir + @"Coral Files\");
        }

        private void SelectOtherSettings(string settingsPath)
        {
            if (!File.Exists(settingsPath + "ScanDisplayUI.txt"))
            {
                return;
            }
            string displayUI = File.ReadAllText(settingsPath + "ScanDisplayUI.txt");
            if (displayUI == "true")
            {
                chbDisplayUI.IsChecked = true;
            }
            else
            {
                chbDisplayUI.IsChecked = false;
            }

            if (!File.Exists(settingsPath + "ScanDuplex.txt"))
            {
                return;
            }
            string duplex = File.ReadAllText(settingsPath + "ScanDuplex.txt");
            if (duplex == "true")
            {
                chbEnabledDuplex.IsChecked = true;
            }
            else
            {
                chbEnabledDuplex.IsChecked = false;
            }
        }

        private void SelectColor(string settingsPath)
        {
            if (!File.Exists(settingsPath + "ScanColor.txt"))
            {
                return;
            }
            string color = File.ReadAllText(settingsPath + "ScanColor.txt");
            if (color == "color")
            {
                rbColor.IsChecked = true;
            }
            else if (color == "gray")
            {
                rbGrayScale.IsChecked = true;
            }
            else
            {
                rbBlackAndWhite.IsChecked = true;
            }
        }

        private void SelectFileType(string settingsPath)
        {
            if (!File.Exists(settingsPath + "ScanFileType.txt"))
            {
                return;
            }
            string fileType = File.ReadAllText(settingsPath + "ScanFileType.txt");
            if (fileType == "tiff")
            {
                rbTIFF.IsChecked = true;
            }
            else
            {
                rbPDF.IsChecked = true;
            }
        }

        private void SelectResolution(string settingsPath)
        {
            if (!File.Exists(settingsPath + "ScanResolution.txt"))
            {
                return;
            }
            string resolution = File.ReadAllText(settingsPath + "ScanResolution.txt");
            if (resolution != "")
            {
                var items = cbResolution.Items;
                foreach (var item in items)
                {
                    if (resolution == ((ComboBoxItem)item).Content.ToString())
                    {
                        cbResolution.SelectedItem = item;
                        break;
                    }
                }
            }
        }

        private void SelectSource(string settingsPath)
        {
            if (!File.Exists(settingsPath + "ScanSource.txt"))
            {
                return;
            }
            string source = File.ReadAllText(settingsPath + "ScanSource.txt");
            if (source != "")
            {
                var items = cbScanSource.ItemsSource;
                foreach (var item in items)
                {
                    if (source == item.ToString())
                    {
                        cbScanSource.SelectedItem = item;
                        break;
                    }
                }
            }
        }
        private void SelectRootFolder(object policySelected)
        {
            string dirPath = "";
            string policyNumber = "";
            string industry = "";
            if (this.IsLoaded)
            {
                if (policySelected is ElementaryPolicy)
                {
                    ElementaryPolicy policy = (ElementaryPolicy)policySelected;
                    policyNumber = policy.PolicyNumber;
                    industry = policy.InsuranceIndustry.InsuranceIndustryName;
                    //if (industry.Contains("רכב"))
                    //{
                    //    industry = "רכב";
                    //}
                    if (!policy.IsOffer)
                    {
                        if (policy.InsuranceIndustry.InsuranceIndustryName == "דירה")
                        {
                            dirPath = rootPath + "\\אלמנטרי\\דירה\\";
                        }
                        else if (policy.InsuranceIndustry.InsuranceIndustryName == "עסק")
                        {
                            dirPath = rootPath + "\\אלמנטרי\\עסק\\";
                        }
                        else if (policy.InsuranceIndustry.InsuranceIndustryName.Contains("רכב"))
                        {
                            dirPath = rootPath + "\\אלמנטרי\\רכב\\";
                        }

                    }
                    else
                    {
                        dirPath = rootPath + "\\הצעות\\אלמנטרי\\";
                    }

                }
                else if (policySelected is LifePolicy)
                {
                    LifePolicy policy = (LifePolicy)policySelected;
                    policyNumber = policy.PolicyNumber;
                    industry = policy.LifeIndustry.LifeIndustryName;
                    if (!policy.IsOffer)
                    {
                        dirPath = rootPath + "\\חיים\\";
                    }
                    else
                    {
                        dirPath = rootPath + "\\הצעות\\חיים\\";
                    }
                }
                else if (policySelected is HealthPolicy)
                {
                    HealthPolicy policy = (HealthPolicy)policySelected;
                    policyNumber = policy.PolicyNumber;
                    if (!policy.IsOffer)
                    {
                        dirPath = rootPath + "\\בריאות\\";
                    }
                    else
                    {
                        dirPath = rootPath + "\\הצעות\\בריאות\\";
                    }
                }
                else if (policySelected is PersonalAccidentsPolicy)
                {
                    PersonalAccidentsPolicy policy = (PersonalAccidentsPolicy)policySelected;
                    policyNumber = policy.PolicyNumber;
                    if (!policy.IsOffer)
                    {
                        dirPath = rootPath + "\\תאונות אישיות\\";
                    }
                    else
                    {
                        dirPath = rootPath + "\\הצעות\\תאונות אישיות\\";
                    }
                }
                else if (policySelected is FinancePolicy)
                {
                    FinancePolicy policy = (FinancePolicy)policySelected;
                    policyNumber = policy.AssociateNumber;
                    industry = policy.FinanceProgramType.ProgramTypeName;
                    if (!policy.IsOffer)
                    {
                        dirPath = rootPath + "\\פיננסים\\";
                    }
                    else
                    {
                        dirPath = rootPath + "\\הצעות\\פיננסים\\";
                    }
                }

                else if (policySelected is TravelPolicy)
                {
                    TravelPolicy policy = (TravelPolicy)policySelected;
                    policyNumber = policy.PolicyNumber;
                    if (!policy.IsOffer)
                    {
                        dirPath = rootPath + "\\נסיעות לחו''ל\\";
                    }
                    else
                    {
                        dirPath = rootPath + "\\הצעות\\נסיעות לחו''ל\\";
                    }
                }
                else if (policySelected is ForeingWorkersPolicy)
                {
                    ForeingWorkersPolicy policy = (ForeingWorkersPolicy)policySelected;
                    policyNumber = policy.PolicyNumber;
                    industry = policy.ForeingWorkersIndustry.ForeingWorkersIndustryName;
                    if (!policy.IsOffer)
                    {
                        dirPath = rootPath + "\\עובדים זרים\\";
                    }
                    else
                    {
                        dirPath = rootPath + "\\הצעות\\עובדים זרים\\";
                    }
                }
                else if (policySelected is ElementaryClaim)
                {
                    ElementaryClaim policy = (ElementaryClaim)policySelected;
                    policyNumber = policy.ElementaryPolicy.PolicyNumber;
                    industry = policy.ElementaryPolicy.InsuranceIndustry.InsuranceIndustryName;
                    if (policy.ElementaryPolicy.InsuranceIndustry.InsuranceIndustryName == "דירה")
                    {
                        dirPath = rootPath + "\\אלמנטרי\\דירה\\";
                    }
                    else if (policy.ElementaryPolicy.InsuranceIndustry.InsuranceIndustryName == "עסק")
                    {
                        dirPath = rootPath + "\\אלמנטרי\\עסק\\";
                    }
                    else if (policy.ElementaryPolicy.InsuranceIndustry.InsuranceIndustryName.Contains("רכב"))
                    {
                        dirPath = rootPath + "\\אלמנטרי\\רכב\\";
                    }

                }
                else if (policySelected is LifeClaim)
                {
                    LifeClaim policy = (LifeClaim)policySelected;
                    policyNumber = policy.LifePolicy.PolicyNumber;
                    industry = policy.LifePolicy.LifeIndustry.LifeIndustryName;
                    dirPath = rootPath + "\\חיים\\";
                }
                else if (policySelected is HealthClaim)
                {
                    HealthClaim policy = (HealthClaim)policySelected;
                    policyNumber = policy.HealthPolicy.PolicyNumber;
                    dirPath = rootPath + "\\בריאות\\";
                }
                else if (policySelected is PersonalAccidentsClaim)
                {
                    PersonalAccidentsClaim policy = (PersonalAccidentsClaim)policySelected;
                    policyNumber = policy.PersonalAccidentsPolicy.PolicyNumber;
                    dirPath = rootPath + "\\תאונות אישיות\\";
                }
                else if (policySelected is TravelClaim)
                {
                    TravelClaim policy = (TravelClaim)policySelected;
                    policyNumber = policy.TravelPolicy.PolicyNumber;
                    dirPath = rootPath + "\\נסיעות לחו''ל\\";
                }
                else if (policySelected is ForeingWorkersClaim)
                {
                    ForeingWorkersClaim policy = (ForeingWorkersClaim)policySelected;
                    policyNumber = policy.ForeingWorkersPolicy.PolicyNumber;
                    industry = policy.ForeingWorkersPolicy.ForeingWorkersIndustry.ForeingWorkersIndustryName;
                    dirPath = rootPath + "\\עובדים זרים\\";
                }

            }
            if (Directory.Exists(dirPath))
            {
                //string[] dirs = Directory.GetDirectories(dirPath, "*" + industry + "*");
                //if (dirs.Count() > 0)
                //{
                //    string selectedDir = dirs.FirstOrDefault(d => d.Contains(policyNumber));
                //    if (selectedDir==null)
                //    {
                //        selectedDir= @rootPath;
                //    }
                //    //string selectedDir = dirs[0];
                //    //for (int i = 0; i < dirs.Count(); i++)
                //    //{
                //    //    if (dirs[i].Contains(policyNumber))
                //    //    {
                //    //        selectedDir = dirs[i];
                //    //        break;
                //    //    }
                //    //}
                string[] dirs= Directory.GetDirectories(dirPath, "*" + @policyNumber + "*");
                if (dirs.Count() > 0)
                {
                    Dictionary<string, string> lastDirectoryList = new Dictionary<string, string>();
                    foreach (var item in dirs)
                    {
                        lastDirectoryList.Add(item,item.Split(System.IO.Path.DirectorySeparatorChar).Last());
                    }
                    string selectedDir = lastDirectoryList.FirstOrDefault(d => d.Value.Contains(industry)).Key;
                    if (selectedDir == null)
                    {
                        selectedDir = dirs[0];
                    }
                    if (policySelected is ElementaryClaim || policySelected is LifeClaim || policySelected is HealthClaim || policySelected is PersonalAccidentsClaim || policySelected is TravelClaim || policySelected is ForeingWorkersClaim)
                    {
                        string[] claimsDir = Directory.GetDirectories(selectedDir, "תביעה*");
                        if (claimsDir.Count() > 0 && Directory.Exists(claimsDir[0]))
                        {
                            string[] claimdirs = null;
                            if (policySelected is ElementaryClaim)
                            {
                                claimdirs = Directory.GetDirectories(claimsDir[0], "*" + ((DateTime)((ElementaryClaim)policySelected).EventDate).ToString("dd-MM-yyyy") + "*");
                            }
                            else if (policySelected is LifeClaim)
                            {
                                claimdirs = Directory.GetDirectories(claimsDir[0], "*" + ((DateTime)((LifeClaim)policySelected).EventDateAndTime).ToString("dd-MM-yyyy") + "*");
                            }
                            else if (policySelected is HealthClaim)
                            {
                                claimdirs = Directory.GetDirectories(claimsDir[0], "*" + ((DateTime)((HealthClaim)policySelected).EventDateAndTime).ToString("dd-MM-yyyy") + "*");
                            }
                            else if (policySelected is PersonalAccidentsClaim)
                            {
                                claimdirs = Directory.GetDirectories(claimsDir[0], "*" + ((DateTime)((PersonalAccidentsClaim)policySelected).EventDateAndTime).ToString("dd-MM-yyyy") + "*");
                            }
                            else if (policySelected is TravelClaim)
                            {
                                claimdirs = Directory.GetDirectories(claimsDir[0], "*" + ((DateTime)((TravelClaim)policySelected).EventDateAndTime).ToString("dd-MM-yyyy") + "*");
                            }
                            else if (policySelected is ForeingWorkersClaim)
                            {
                                claimdirs = Directory.GetDirectories(claimsDir[0], "*" + ((DateTime)((ForeingWorkersClaim)policySelected).EventDateAndTime).ToString("dd-MM-yyyy") + "*");
                            }
                            if (claimdirs.Count() == 1)
                            {
                                folderView.RootFolder = claimdirs[0];
                            }
                            else
                            {
                                folderView.RootFolder = selectedDir;
                            }
                        }
                        else
                        {
                            folderView.RootFolder = selectedDir;
                        }
                    }
                    else
                    {
                        folderView.RootFolder = selectedDir;
                    }
                }
                else
                {
                    folderView.RootFolder = @dirPath;
                }
            }
            else
            {
                folderView.RootFolder = @rootPath;
            }
        }

        private void cbScanSourceBinding()
        {
            int sourcesNumber = gdPicture.TwainGetSourceCount(new WindowInteropHelper(this).Handle);
            List<string> sourcesName = new List<string>();
            for (int i = 1; i <= sourcesNumber; i++)
            {
                sourcesName.Add(gdPicture.TwainGetSourceName(new WindowInteropHelper(this).Handle, i));
            }
            cbScanSource.ItemsSource = sourcesName.ToList();
            cbScanSource.SelectedIndex = 0;
        }

        private void cbPolicyDetailsBinding()
        {
            List<object> allPolicies = new List<object>();
            //var allPolicies = policyLogic.GetAllActiveElementaryPoliciesByClient(clientArchive);
            var elementaryPolicies = policyLogic.GetAllActiveElementaryPoliciesWithoutAdditionsByClient(clientArchive);
            foreach (var item in elementaryPolicies)
            {
                allPolicies.Add(item);
            }
            var lifePolicies = lifePolicyLogic.GetActiveLifePoliciesWithoutAdditionsByClient(clientArchive);
            foreach (var item in lifePolicies)
            {
                allPolicies.Add(item);
            }
            var healthPolicies = healthPolicyLogic.GetActiveHealthPoliciesWithoutAdditionsByClient(clientArchive);
            foreach (var item in healthPolicies)
            {
                allPolicies.Add(item);
            }
            var financePolicies = financeLogic.GetActiveFinancePoliciesWithoutAdditionByClient(clientArchive);
            foreach (var item in financePolicies)
            {
                allPolicies.Add(item);
            }
            var personalAccidentsPolicies = personalAccidentLogic.GetActivePersonalAccidentsPoliciesWithoutAdditionByClient(clientArchive);
            foreach (var item in personalAccidentsPolicies)
            {
                allPolicies.Add(item);
            }
            var travelPolicies = travelLogic.GetActiveTravelPoliciesWithoutAdditionByClient(clientArchive);
            foreach (var item in travelPolicies)
            {
                allPolicies.Add(item);
            }
            var foreingWorkersPolicies = foreingWorkersLogic.GetActiveForeingWorkersPoliciesWithoutAdditionByClient(clientArchive);
            foreach (var item in foreingWorkersPolicies)
            {
                allPolicies.Add(item);
            }
            cbPolicyDetails.ItemsSource = allPolicies.ToList();
            if (policySelected != null)
            {
                var cbItems = cbPolicyDetails.Items;
                foreach (var item in cbItems)
                {
                    if (policySelected is ElementaryPolicy && item is ElementaryPolicy)
                    {
                        ElementaryPolicy cbPolicy = (ElementaryPolicy)item;
                        ElementaryPolicy policy = (ElementaryPolicy)policySelected;
                        if (cbPolicy.PolicyNumber == policy.PolicyNumber)
                        {
                            cbPolicyDetails.SelectedItem = item;
                            break;
                        }
                    }
                    else if (policySelected is LifePolicy && item is LifePolicy)
                    {
                        LifePolicy cbPolicy = (LifePolicy)item;
                        LifePolicy policy = (LifePolicy)policySelected;
                        if (cbPolicy.PolicyNumber == policy.PolicyNumber)
                        {
                            cbPolicyDetails.SelectedItem = item;
                            break;
                        }
                    }
                    else if (policySelected is HealthPolicy && item is HealthPolicy)
                    {
                        HealthPolicy cbPolicy = (HealthPolicy)item;
                        HealthPolicy policy = (HealthPolicy)policySelected;
                        if (cbPolicy.PolicyNumber == policy.PolicyNumber)
                        {
                            cbPolicyDetails.SelectedItem = item;
                            break;
                        }
                    }
                    else if (policySelected is FinancePolicy && item is FinancePolicy)
                    {
                        FinancePolicy cbPolicy = (FinancePolicy)item;
                        FinancePolicy policy = (FinancePolicy)policySelected;
                        if (cbPolicy.AssociateNumber == policy.AssociateNumber)
                        {
                            cbPolicyDetails.SelectedItem = item;
                            break;
                        }
                    }
                    else if (policySelected is PersonalAccidentsPolicy && item is PersonalAccidentsPolicy)
                    {
                        PersonalAccidentsPolicy cbPolicy = (PersonalAccidentsPolicy)item;
                        PersonalAccidentsPolicy policy = (PersonalAccidentsPolicy)policySelected;
                        if (cbPolicy.PolicyNumber == policy.PolicyNumber)
                        {
                            cbPolicyDetails.SelectedItem = item;
                            break;
                        }
                    }
                    else if (policySelected is TravelPolicy && item is TravelPolicy)
                    {
                        TravelPolicy cbPolicy = (TravelPolicy)item;
                        TravelPolicy policy = (TravelPolicy)policySelected;
                        if (cbPolicy.PolicyNumber == policy.PolicyNumber)
                        {
                            cbPolicyDetails.SelectedItem = item;
                            break;
                        }
                    }
                    else if (policySelected is ForeingWorkersPolicy && item is ForeingWorkersPolicy)
                    {
                        ForeingWorkersPolicy cbPolicy = (ForeingWorkersPolicy)item;
                        ForeingWorkersPolicy policy = (ForeingWorkersPolicy)policySelected;
                        if (cbPolicy.PolicyNumber == policy.PolicyNumber)
                        {
                            cbPolicyDetails.SelectedItem = item;
                            break;
                        }
                    }
                }
            }
        }

        private void FileViewWPF_BeforeColumnAdd(object sender, LogicNP.FileViewControl.ColumnAddEventArgs e)
        {
            fileView.SetColumnDisplayIndex("Date Modified", 2, 1);
            fileView.SetColumnWidth("Name", 0, 200);
            fileView.SetColumnName("Name", 0, "שם קובץ");
            fileView.SetColumnName("Date Modified", 1, "תאריך עדכון");
            fileView.SetColumnName("Size", 2, "גודל");

            if (e.ColumnName == "Type")
            {
                e.Cancel = true;
            }

        }

        private void btnClientDetails_Click(object sender, RoutedEventArgs e)
        {
            if (clientArchive != null)
            {
                clientDetailsWindow = new frmAddClient(clientArchive, user);
                clientDetailsWindow.Show();
            }
            else if (policyOwner != null)
            {
                frmAddPolicyOwner ownerDetails = new frmAddPolicyOwner(policyOwner);
                ownerDetails.Show();
            }
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (clientDetailsWindow != null)
            {
                clientDetailsWindow.Close();
            }
            m_GDViewer.CloseDocument();
        }

        private void btnUpdateDocNamesTable_Click(object sender, RoutedEventArgs e)
        {
            int? selectedItemId = null;
            if (cbDocumentName.SelectedItem != null)
            {
                selectedItemId = ((DocumentName)cbDocumentName.SelectedItem).DocumentNameID;
            }
            frmUpdateTable updateDocNamesTable = new frmUpdateTable(new DocumentName());
            updateDocNamesTable.ShowDialog();
            cbDocumentNameBinding();
            if (updateDocNamesTable.ItemAdded != null)
            {
                SelectItemInCb(((DocumentName)updateDocNamesTable.ItemAdded).DocumentNameID);
            }
            else if (selectedItemId != null)
            {
                SelectItemInCb((int)selectedItemId);
            }

        }

        private void SelectItemInCb(int id)
        {
            var items = cbDocumentName.Items;
            foreach (var item in items)
            {
                DocumentName parsedItem = (DocumentName)item;
                if (parsedItem.DocumentNameID == id)
                {
                    cbDocumentName.SelectedItem = item;
                    break;
                }
            }
        }

        private void cbDocumentNameBinding()
        {
            cbDocumentName.ItemsSource = docNameLogic.GetAllActiveDocumentNames();
            cbDocumentName.DisplayMemberPath = "DocName";
        }

        private void btnScan_Click(object sender, RoutedEventArgs e)
        {
            string savingPath = null;
            int ImageID = 0;
            //Variable to hold created multipage image handle
            int multipageHandle = 0;
            //Number of images
            int imageCount = 0;
            if (gdPicture.TwainOpenSource(new WindowInteropHelper(this).Handle, cbScanSource.SelectedItem.ToString()))
            {
                SetScanSettings();

                //else if (rbTIFF.IsChecked == true)
                //{
                string folderPath = null;
                do
                {
                    ImageID = gdPicture.TwainAcquireToGdPictureImage(new WindowInteropHelper(this).Handle);

                    if (ImageID != 0)
                    {
                        imageCount++;
                        //If it is first image, then create a new multipage tiff file
                        if (imageCount == 1)
                        {
                            //DisplayImage(ImageID);
                            multipageHandle = ImageID;
                            folderPath = folderView.SelectedNode.Path;
                            int index = 1;
                            do
                            {

                                if (index == 1)
                                {
                                    savingPath = @folderPath + @"\" + cbDocumentName.Text + ".tif";
                                }
                                else
                                {
                                    savingPath = @folderPath + @"\" + cbDocumentName.Text + index + ".tif";
                                }
                                index++;

                            } while (File.Exists(savingPath));
                            gdPicture.TiffSaveAsMultiPageFile(multipageHandle, savingPath, TiffCompression.TiffCompressionAUTO);
                        }
                        else //If it is second image or more, add it to previously created multipage tiff file
                        {
                            gdPicture.TiffAddToMultiPageFile(multipageHandle, ImageID);
                            //Release current image to minimize memory usage
                            gdPicture.ReleaseGdPictureImage(ImageID);
                        }
                    }
                    else
                    {
                        MessageBox.Show("Image Could Not Be Loaded! " + gdPicture.GetStat().ToString());
                        return;
                    }
                } while (gdPicture.TwainGetState() > TwainStatus.TWAIN_SOURCE_ENABLED);

                //Clean up resources
                gdPicture.TwainCloseSource(); //Closing the source
                gdPicture.TiffCloseMultiPageFile(multipageHandle); //Closing multipage file
                gdPicture.ReleaseGdPictureImage(multipageHandle); //Releasing the multipage image

                //}
                if (rbPDF.IsChecked == true)
                {
                    GdPictureImaging oGdPictureImaging = new GdPictureImaging();

                    int pdfImageID = oGdPictureImaging.CreateGdPictureImageFromFile(savingPath);
                    string pdfSavingPath;
                    int index = 1;
                    do
                    {
                        if (index == 1)
                        {
                            pdfSavingPath = @folderPath + @"\" + cbDocumentName.Text + ".pdf";
                        }
                        else
                        {
                            pdfSavingPath = @folderPath + @"\" + cbDocumentName.Text + index + ".pdf";
                        }
                        index++;

                    } while (File.Exists(pdfSavingPath));

                    if (oGdPictureImaging.TiffIsMultiPage(pdfImageID))
                    {
                        oGdPictureImaging.PdfCreateFromMultipageTIFF(pdfImageID, pdfSavingPath, true, cbDocumentName.Text, null, null, null, null);
                    }
                    else
                    {
                        oGdPictureImaging.SaveAsPDF(pdfImageID, pdfSavingPath, true, cbDocumentName.Text, null, null, null, null);
                    }
                    oGdPictureImaging.ReleaseGdPictureImage(pdfImageID);
                    File.Delete(savingPath);
                    savingPath = pdfSavingPath;
                }

                //else if (rbJPEG.IsChecked == true)
                //{
                //    GdPictureImaging oGdPictureImaging = new GdPictureImaging();

                //    int jpegImageID = oGdPictureImaging.CreateGdPictureImageFromFile(savingPath);
                //    int NumberOfPages = oGdPictureImaging.TiffGetPageCount(jpegImageID);
                //    //ProgressBar1.Maximum = NumberOfPages;
                //    for (int i = 1; i <= NumberOfPages; i++)
                //    {
                //        oGdPictureImaging.TiffSelectPage(jpegImageID, i);
                //        string jpegSavingPath = folderPath + @"\" + cbDocumentName.Text + "_Split_" + i.ToString() + ".jpg";
                //        //m_DestinationPath = Path.Combine(m_DestinationFolder, m_Name + "_Split_" + i.ToString() + m_OutPutExtension);
                //        oGdPictureImaging.SaveAsJPEG(jpegImageID, jpegSavingPath);
                //        //if (m_Status != GdPictureStatus.OK)
                //        //{
                //        //    MessageBox.Show("error: " + m_Status.ToString());
                //        //}
                //        //if (m_Status != GdPictureStatus.OK)
                //        //{
                //        //    m_Status = GdPictureStatus.OK;
                //        //    ProgressBar1.Value = 0;
                //        //    return;
                //        //}
                //    }
                //   // 
                //   // 
                //   // string jpegSavingPath = folderPath + @"\" + cbDocumentName.Text + ".jpg";
                //   // //if (oGdPictureImaging.TiffIsMultiPage(jpegImageID))
                //   // //{
                //   //    // oGdPictureImaging.PdfCreateFromMultipageTIFF(jpegImageID, jpegSavingPath, true, cbDocumentName.Text, null, null, null, null);
                //   // //}
                //   // //else
                //   // //{
                //   //     oGdPictureImaging.SaveAsJPEG(jpegImageID, jpegSavingPath);
                //   //// }
                //    // oGdPictureImaging.ReleaseGdPictureImage(jpegImageID);
                //    oGdPictureImaging.ReleaseGdPictureImage(jpegImageID);
                //    File.Delete(savingPath);
                //    savingPath = folderPath + @"\" + cbDocumentName.Text + ".jpg";             
                //    //do
                //    //{
                //    //    ImageID = gdPicture.TwainAcquireToGdPictureImage(new WindowInteropHelper(this).Handle);
                //    //    if (ImageID != 0)
                //    //    {
                //    //        DisplayImage(ImageID);
                //    //        imageCount = imageCount + 1;
                //    //        gdPicture.SaveAsJPEG(ImageID, @"image" + imageCount.ToString().Trim() + ".jpg", 75);
                //    //        gdPicture.ReleaseGdPictureImage(ImageID);
                //    //    }
                //    //} while (gdPicture.TwainGetState() > TwainStatus.TWAIN_SOURCE_ENABLED);
                //    //gdPicture.TwainCloseSource();
                //    //MessageBox.Show("Done !");
                //}
                folderView.RefreshTree();
                fileView.RefreshView();
                string fileName = System.IO.Path.GetFileNameWithoutExtension(@savingPath);
                var item = fileView.GetItemFromName(fileName);
                if (item != null)
                {
                    fileView.GetListItem(item.Index).Selected = true;
                }
                else
                {
                    if (fileView.GetListItem(1) != null)
                    {
                        var newItem = fileView.GetListItem(1);
                        newItem.Selected = true;
                    }
                }
                if (savingPath != null)
                {
                    m_GDViewer.DisplayFromFile(savingPath);
                    ShowCurrentPage();
                }
            }
            else
            {
                MessageBox.Show("Can't open default source, twain state is: " + gdPicture.TwainGetState().ToString());
            }

        }

        private void DisplayImage(int ImageID)
        {
            m_GDViewer.DisplayFromGdPictureImage(ImageID);
            m_GDViewer.CloseDocument(true);
            System.Windows.Forms.Application.DoEvents();
        }

        private void btnNewFolder_Click(object sender, RoutedEventArgs e)
        {
            folderView.CreateNewFolder(true);
        }

        private void btnRename_Click(object sender, RoutedEventArgs e)
        {
            if (fileView.FirstSelectedItem != null)
            {
                m_GDViewer.CloseDocument();
                fileView.FirstSelectedItem.ExecuteShellCommand(ShellCommands.Rename);
                //fileView.RefreshView();
            }
            else if (folderView.SelectedNode != null)
            {
                m_GDViewer.CloseDocument();
                folderView.SelectedNode.ExecuteShellCommand(LogicNP.FolderViewControl.ShellCommands.Rename);
                //folderView.RefreshTree();
            }
            else
            {
                MessageBox.Show("נא סמן פריט לעדכון שם");
            }
        }

        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            if (fileView.FirstSelectedItem != null)
            {
                m_GDViewer.CloseDocument();
                fileView.FirstSelectedItem.ExecuteShellCommand(ShellCommands.Delete);
                fileView.RefreshView();
            }
            else if (folderView.SelectedNode != null)
            {
                m_GDViewer.CloseDocument();
                folderView.SelectedNode.ExecuteShellCommand(LogicNP.FolderViewControl.ShellCommands.Delete);
                folderView.RefreshTree();
            }
            else
            {
                MessageBox.Show("נא סמן פריט למחיקה");
            }

        }

        private void btnFirstPage_Click(object sender, RoutedEventArgs e)
        {
            if (m_GDViewer.DisplayFirstPage() != GdPictureStatus.OK)
                MessageBox.Show("Error : " + m_GDViewer.GetStat().ToString());
            else
                ShowCurrentPage();
        }

        private void btnPreviousPage_Click(object sender, RoutedEventArgs e)
        {
            if (m_GDViewer.DisplayPreviousPage() != GdPictureStatus.OK)
                MessageBox.Show("Error : " + m_GDViewer.GetStat().ToString());
            else
                ShowCurrentPage();
        }

        private void btnLastPage_Click(object sender, RoutedEventArgs e)
        {
            if (m_GDViewer.DisplayLastPage() != GdPictureStatus.OK)
                MessageBox.Show("Error : " + m_GDViewer.GetStat().ToString());
            else
                ShowCurrentPage();
        }

        private void btnNextPage_Click(object sender, RoutedEventArgs e)
        {
            if (m_GDViewer.DisplayNextPage() != GdPictureStatus.OK)
                MessageBox.Show("Error : " + m_GDViewer.GetStat().ToString());
            else
                ShowCurrentPage();
        }

        private void fileView_ItemClick(object sender, FileViewEventArgs e)
        {
            if (fileView.FirstSelectedItem != null)
            {
                m_GDViewer.ZoomMode = ViewerZoomMode.ZoomModeWidthViewer;
                m_GDViewer.DisplayFromFile(fileView.FirstSelectedItem.Path);
                ShowCurrentPage();
                //string extension = System.IO.Path.GetExtension(fileView.FirstSelectedItem.Path);
                //if (extension == ".tif")
                //{
                //    btnDeleteCurrentPage.IsEnabled = true;
                //    btnAddScanPage.IsEnabled = true;
                //}
                //else
                //{
                //    btnDeleteCurrentPage.IsEnabled = false;
                //    btnAddScanPage.IsEnabled = false;
                //}
            }
        }

        private void folderView_NodeClick(object sender, LogicNP.FolderViewControl.FolderViewEventArgs e)
        {
            fileView.SelectItems(SelectTypes.None);
        }
        private void ShowCurrentPage()
        {
            if (m_GDViewer.CurrentPage != 0)
                lblCurrentPage.Content = m_GDViewer.CurrentPage.ToString() + " / " + m_GDViewer.PageCount;
            else
                lblCurrentPage.Content = "";
        }

        private void btnCut_Click(object sender, RoutedEventArgs e)
        {
            if (fileView.FirstSelectedItem != null)
            {
                m_GDViewer.CloseDocument();
                fileView.FirstSelectedItem.ExecuteShellCommand(ShellCommands.Cut);
                fileView.RefreshView();
            }
            else if (folderView.SelectedNode != null)
            {
                m_GDViewer.CloseDocument();
                folderView.SelectedNode.ExecuteShellCommand(LogicNP.FolderViewControl.ShellCommands.Cut);
                folderView.RefreshTree();
            }
            else
            {
                MessageBox.Show("נא סמן פריט לגזירה");
            }
        }

        private void btnCopy_Click(object sender, RoutedEventArgs e)
        {
            if (fileView.FirstSelectedItem != null)
            {
                //m_GDViewer.CloseDocument();
                fileView.FirstSelectedItem.ExecuteShellCommand(ShellCommands.Copy);
                //fileView.RefreshView();
            }
            else if (folderView.SelectedNode != null)
            {
                m_GDViewer.CloseDocument();
                folderView.SelectedNode.ExecuteShellCommand(LogicNP.FolderViewControl.ShellCommands.Copy);
                folderView.RefreshTree();
            }
            else
            {
                MessageBox.Show("נא סמן פריט להעתקה");
            }
        }

        private void btnPaste_Click(object sender, RoutedEventArgs e)
        {
            if (fileView.SelectedItems != null)
            {
                //m_GDViewer.CloseDocument();
                //fileView.FirstSelectedItem.ExecuteShellCommand(ShellCommands.Paste);
                fileView.ExecuteCmdForFolder(ShellCommands.Paste);
                fileView.RefreshView();
            }
            //else if (folderView.SelectedNode != null)
            //{
            //    //m_GDViewer.CloseDocument();
            //    //folderView.SelectedNode.ExecuteShellCommand(LogicNP.FolderViewControl.ShellCommands.Copy);

            //    folderView.RefreshTree();
            //}
        }

        private void m_GDViewer_DocumentClosed()
        {
            ShowCurrentPage();
        }

        private void btnOpenWindows_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("הנך נכנס לחלון סייר חלונות! שינוי כלשהו בקבצים מהווה סכנה לתפקוד תקין של הארכיון האופטי", "אישור", MessageBoxButton.OKCancel, MessageBoxImage.Exclamation) == MessageBoxResult.OK)
            {
                System.Diagnostics.Process.Start(rootPath);
            }
        }

        private void btnDisplay_Click(object sender, RoutedEventArgs e)
        {
            if (fileView.ViewStyle != ViewStyles.LargeIcon)
            {
                fileView.ViewStyle = ViewStyles.LargeIcon;
            }
            else
            {
                fileView.ViewStyle = ViewStyles.Report;
            }
        }

        private void btnZoomFitToViewer_Click(object sender, RoutedEventArgs e)
        {
            m_GDViewer.ZoomMode = ViewerZoomMode.ZoomModeFitToViewer;
        }

        private void btnZoom100_Click(object sender, RoutedEventArgs e)
        {
            m_GDViewer.Zoom = 1;
        }

        private void zoomSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            m_GDViewer.Zoom = zoomSlider.Value;
        }

        private void m_GDViewer_AfterZoomChange()
        {
            zoomSlider.Value = m_GDViewer.Zoom;
        }

        private void btnRotateRight_Click(object sender, RoutedEventArgs e)
        {
            m_GDViewer.Rotate(RotateFlipType.Rotate90FlipNone);
            if (m_GDViewer.GetStat() != GdPictureStatus.OK)
            {
                MessageBox.Show("Error: " + m_GDViewer.GetStat());
            }
        }

        private void btnRotateLeft_Click(object sender, RoutedEventArgs e)
        {
            m_GDViewer.Rotate(RotateFlipType.Rotate270FlipNone);
            if (m_GDViewer.GetStat() != GdPictureStatus.OK)
            {
                MessageBox.Show("Error: " + m_GDViewer.GetStat());
            }
        }

        private void btnFocus_Click(object sender, RoutedEventArgs e)
        {
            m_GDViewer.MouseMode = ViewerMouseMode.MouseModeAreaSelection;
        }

        private void m_GDViewer_MouseUp(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            if (m_GDViewer.MouseMode == ViewerMouseMode.MouseModeAreaSelection)
            {
                m_GDViewer.ZoomRect();
                m_GDViewer.ClearRect();
                m_GDViewer.MouseMode = ViewerMouseMode.MouseModeDefault;
            }
        }

        private void cbPolicyDetails_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            object policyFromCb = cbPolicyDetails.SelectedItem;
            SelectRootFolder(policyFromCb);

            //var cbItems = cbPolicyDetails.Items;
            //foreach (var item in cbItems)
            //{
            //    if (policySelected is ElementaryPolicy)
            //    {
            //        ElementaryPolicy cbPolicy = (ElementaryPolicy)item;
            //        ElementaryPolicy policy = (ElementaryPolicy)policySelected;
            //        if (cbPolicy.PolicyNumber == policy.PolicyNumber)
            //        {
            //            cbPolicyDetails.SelectedItem = item;
            //            break;
            //        }
            //    }
            //}

            //if (policyFromCb is ElementaryPolicy)
            //{
            //    ElementaryPolicy policy = (ElementaryPolicy)policyFromCb;
            //    if (policy.InsuranceIndustry.InsuranceIndustryName == "דירה")
            //    {
            //        string path = rootPath + "\\אלמנטרי\\דירה\\" + policy.PolicyNumber;
            //        folderView.RootFolder = path;
            //    }
            //    if (policy.InsuranceIndustry.InsuranceIndustryName == "עסק")
            //    {
            //        string path = rootPath + "\\אלמנטרי\\עסק\\" + policy.PolicyNumber;
            //        folderView.RootFolder = path;
            //    }
            //    //if (policy.InsuranceIndustry.InsuranceIndustryName == "רכב" || policy.InsuranceIndustry.InsuranceIndustryName == "רכב חובה")
            //    if (policy.InsuranceIndustry.InsuranceIndustryName.Contains("רכב"))
            //    {
            //        string path = rootPath + "\\אלמנטרי\\רכב\\" + policy.PolicyNumber;
            //        folderView.RootFolder = path;
            //    }
            //    if (policy.InsuranceIndustry.InsuranceIndustryName == "נסיעות לחו''ל")
            //    {
            //        string path = rootPath + "\\אלמנטרי\\נסיעות לחו''ל\\" + policy.PolicyNumber;
            //        folderView.RootFolder = path;
            //    }

            //}
        }

        private void btnSearch_Click(object sender, RoutedEventArgs e)
        {
            if (txtSearch.Text == "")
            {
                MessageBox.Show("נא להזין מילת חיפוש", "שגיאה", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            DirectoryInfo clientRoot = new DirectoryInfo(rootPath);
            List<DirectoryInfo> directories = clientRoot.GetDirectories("*.*", System.IO.SearchOption.AllDirectories).ToList();
            directories.Add(clientRoot);
            FileInfo[] files = null;
            List<DirectoryInfo> dirResults = new List<DirectoryInfo>();
            List<FileInfo> fileResults = new List<FileInfo>();

            foreach (DirectoryInfo dir in directories)
            {
                if (rbFilesAndDirectories.IsChecked == true)
                {
                    if (dir.Name.Contains(txtSearch.Text))
                    {
                        dirResults.Add(dir);
                    }
                }
                files = dir.GetFiles("*" + txtSearch.Text + "*");
                if (files != null)
                {
                    foreach (FileInfo file in files)
                    {
                        fileResults.Add(file);
                    }
                }
            }
            m_GDViewer.CloseDocument();
            frmSearchResults resultsWindow = new frmSearchResults(dirResults, fileResults, txtSearch.Text, clientArchive);
            resultsWindow.ShowDialog();
            folderView.RefreshView();
            fileView.RefreshView();

            //DirectoryInfo searchResults;
            //string targetPath = Directory.GetCurrentDirectory()+@"\SearchResults";
            //if (fileResults.Count > 0 || dirResults.Count > 0)
            //{
            //    if (!System.IO.Directory.Exists(targetPath))
            //    {
            //        searchResults = Directory.CreateDirectory(targetPath);
            //    }
            //    searchResults = new DirectoryInfo(targetPath);
            //    if (fileResults.Count > 0)
            //    {
            //        int index = 0;
            //        foreach (FileInfo file in fileResults)
            //        {
            //            index++;
            //            string sourceFile = file.FullName;
            //            string destFile = System.IO.Path.Combine(targetPath, file.Name + "Copy" + index.ToString());
            //            File.Copy(sourceFile, destFile, true);
            //        }                    
            //    }
            //    if (dirResults.Count > 0)
            //    {
            //        foreach (DirectoryInfo item in dirResults)
            //        {
            //            DirectoryCopy(item.FullName, System.IO.Path.Combine(targetPath, item.Name));
            //        }

            //    }
            //    frmSearchResults window = new frmSearchResults(searchResults);
            //    window.ShowDialog();
            //}
            //else
            //{
            //    MessageBox.Show("אין תוצאות מתאימות");
            //}
            //if (System.IO.Directory.Exists(targetPath))
            //{
            //    Directory.Delete(targetPath, true); 
            //}
        }

        //    private static void DirectoryCopy(string sourceDirName, string destDirName)
        //    {
        //        string destDir = destDirName;  
        //        DirectoryInfo dir = new DirectoryInfo(sourceDirName);
        //        DirectoryInfo[] dirs = dir.GetDirectories();

        //        if (!dir.Exists)
        //        {
        //            throw new DirectoryNotFoundException(
        //                "התיקייה לא נמצאה "
        //                + dir.Name);
        //        }

        ////        // If the destination directory doesn't exist, create it. 
        //        if (!Directory.Exists(destDir))
        //        {
        //            Directory.CreateDirectory(destDir);
        //        }

        ////        // Get the files in the directory and copy them to the new location.
        //        FileInfo[] dirFiles = dir.GetFiles();
        //        foreach (FileInfo file in dirFiles)
        //        {
        //            string temppath = System.IO.Path.Combine(destDir, file.Name);
        //            file.CopyTo(temppath, false);
        //        }

        ////        // If copying subdirectories, copy them and their contents to new location. 

        //        foreach (DirectoryInfo subdir in dirs)
        //        {
        //            string temppath = System.IO.Path.Combine(destDirName, subdir.Name);
        //            DirectoryCopy(subdir.FullName, temppath);
        //        }

        //    }

        private void btnCopyCurrentPage_Click(object sender, RoutedEventArgs e)
        {
            m_GDViewer.CopyToClipboard();
        }

        private void btnPrintCurrent_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                m_GDViewer.PrintSetFromToPage(m_GDViewer.CurrentPage, m_GDViewer.CurrentPage);
                m_GDViewer.Print();
            }
            catch (Exception)
            {
                System.Windows.Forms.MessageBox.Show("לא ניתן להדפיס");
                return;
            }
        }

        private void btnPrintAll_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (m_GDViewer.PageCount > 0)
                {                    
                    m_GDViewer.PrintDialog();
                }

                //If oGdPicturePDF.LoadFromFile("test.pdf", False) = GdPictureStatus.OK Then
                //                If oGdPicturePDF.PrintDialog() Then
                //      MessageBox.Show("The PDF is printed")
                //   Else
                //      MessageBox.Show("The PDF is not printed. " + oGdPicturePDF.GetStat().ToString)
                //   End If
                //Else
                //   MessageBox.Show("The file can't be opened")
                //End If
            }
            catch (Exception)
            {
                System.Windows.Forms.MessageBox.Show("לא ניתן להדפיס");
                return;
            }
            //            PrintDocument pd = new PrintDocument();
            //            pd.PrintPage += new PrintPageEventHandler(PrintPage);
            //            PrintDialog pdi = new PrintDialog();
            //            pdi.Document = pd;
            //            if (pdi.ShowDialog() == DialogResult.OK)
            //            {
            //                pd.Print();
            //            }
            //            else
            //            {
            //                MessageBox.Show("Print Cancelled");
            //            }
            //            Edited(from Comment)

            //On 64 - bit Windows and with some versions of .NET you may have to set pdi.UseExDialog = true; for the dialog window to appear.
        }

        private void txtSearch_GotFocus(object sender, RoutedEventArgs e)
        {
            btnSearch.IsDefault = true;
        }

        private void txtSearch_LostFocus(object sender, RoutedEventArgs e)
        {
            btnSearch.IsDefault = false;
        }

        private void m_GDViewer_PageChanged()
        {
            ShowCurrentPage();
        }

        private void btnCheckBox_Click(object sender, RoutedEventArgs e)
        {
            if (fileView.ShowCheckBoxes == false)
            {
                fileView.ShowCheckBoxes = true;
            }
            else
            {
                fileView.ShowCheckBoxes = false;
            }
        }

        private void btnMoveToClient_Click(object sender, RoutedEventArgs e)
        {
            if (cbChooseClient.SelectedItem == null)
            {
                System.Windows.Forms.MessageBox.Show("נא בחר לקוח");
                return;
            }
            string sourcePath = null;
            if (fileView.FirstSelectedItem != null)
            {
                sourcePath = fileView.FirstSelectedItem.Path;

            }
            else
            {
                System.Windows.Forms.MessageBox.Show("נא בחר קובץ להעברה");
                return;
            }
            //string sourcePath = ((Result)dgSearchResults.SelectedItem).DocPath;
            if (MessageBox.Show("העבר את הקובץ " + sourcePath + " ללקוח " + ((Client)cbChooseClient.SelectedItem).LastName + " " + ((Client)cbChooseClient.SelectedItem).FirstName, "אישור", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                try
                {
                    string fileOrDirName = System.IO.Path.GetFileName(sourcePath);
                    string destPath;
                    int index = 1;
                    do
                    {
                        if (index == 1)
                        {
                            destPath = clientsPath + @"\" + ((Client)cbChooseClient.SelectedItem).ClientID + @"\" + fileOrDirName;
                        }
                        else
                        {
                            destPath = clientsPath + @"\" + ((Client)cbChooseClient.SelectedItem).ClientID + @"\" + index + fileOrDirName;
                        }
                        index++;

                    } while (File.Exists(destPath));
                    //destPath = Directory.GetCurrentDirectory() + @"\Clients\" + ((Client)cbChooseClient.SelectedItem).ClientID + @"\" + fileOrDirName;
                    m_GDViewer.CloseDocument();
                    Directory.Move(sourcePath, destPath);
                    //results.Remove((Result)dgSearchResults.Selected;Item)
                    //dgSearchResultsBinding();
                    fileView.RefreshView();
                }
                catch (Exception ex)
                {
                    System.Windows.Forms.MessageBox.Show(ex.Message);
                }
            }
        }

        private void fileView_BeforeLabelEdit(object sender, ItemLabelEditEventArgs e)
        {
            m_GDViewer.CloseDocument();
        }

        private void btnDeleteCurrentPage_Click(object sender, RoutedEventArgs e)
        {
            if (fileView.FirstSelectedItem != null)
            {
                if (m_GDViewer.PageCount > 0 && System.IO.Path.GetExtension(fileView.FirstSelectedItem.Path) == ".tif")
                {
                    if (MessageBox.Show("למחוק עמוד זה", "אישור", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                    {
                        GdPictureImaging oGdPictureImaging = new GdPictureImaging();
                        string pathToDeleteFile = fileView.FirstSelectedItem.Path;
                        int DocImageID = oGdPictureImaging.CreateGdPictureImageFromFile(pathToDeleteFile);
                        oGdPictureImaging.TiffDeletePage(DocImageID, m_GDViewer.CurrentPage);
                        string newPath;
                        int index = 2;
                        do
                        {
                            newPath = System.IO.Path.GetDirectoryName(pathToDeleteFile) + "\\(" + index + ")" + System.IO.Path.GetFileName(pathToDeleteFile);
                            index++;
                        } while (File.Exists(newPath));
                        oGdPictureImaging.TiffSaveMultiPageToFile(DocImageID, newPath, TiffCompression.TiffCompressionCCITT4);
                        oGdPictureImaging.ReleaseGdPictureImage(DocImageID);
                        folderView.RefreshTree();
                        fileView.RefreshView();
                        m_GDViewer.DisplayFromFile(newPath);
                        ShowCurrentPage();
                    }
                }
                else
                {
                    System.Windows.Forms.MessageBox.Show("נא בחר קובץ טיף");
                }
            }
            else
            {
                System.Windows.Forms.MessageBox.Show("נא סמן קובץ למחיקה");
            }
        }

        private void btnAddScanPage_Click(object sender, RoutedEventArgs e)
        {
            if (m_GDViewer.PageCount > 0 && System.IO.Path.GetExtension(fileView.FirstSelectedItem.Path) == ".tif")
            {
                string savingPath = "temp.tif";
                int ImageID = 0;
                //Variable to hold created multipage image handle
                int multipageHandle = 0;
                //Number of images
                int imageCount = 0;
                if (gdPicture.TwainOpenSource(new WindowInteropHelper(this).Handle, cbScanSource.SelectedItem.ToString()))
                {
                    SetScanSettings();

                    //else if (rbTIFF.IsChecked == true)
                    //{
                    //string folderPath = null;
                    do
                    {
                        ImageID = gdPicture.TwainAcquireToGdPictureImage(new WindowInteropHelper(this).Handle);

                        if (ImageID != 0)
                        {
                            imageCount++;
                            //If it is first image, then create a new multipage tiff file
                            if (imageCount == 1)
                            {
                                //DisplayImage(ImageID);
                                multipageHandle = ImageID;
                                //folderPath = folderView.SelectedNode.Path;
                                //int index = 1;
                                //do
                                //{

                                //    if (index == 1)
                                //    {
                                //        savingPath = folderPath + @"\" + cbDocumentName.Text + ".tif";
                                //    }
                                //    else
                                //    {
                                //        savingPath = folderPath + @"\" + cbDocumentName.Text + index + ".tif";
                                //    }
                                //    index++;

                                //} while (File.Exists(savingPath));

                                gdPicture.TiffSaveAsMultiPageFile(multipageHandle, savingPath, TiffCompression.TiffCompressionAUTO);
                            }
                            else //If it is second image or more, add it to previously created multipage tiff file
                            {
                                gdPicture.TiffAddToMultiPageFile(multipageHandle, ImageID);
                                //Release current image to minimize memory usage
                                gdPicture.ReleaseGdPictureImage(ImageID);
                            }
                        }
                        else
                        {
                            MessageBox.Show("Image Could Not Be Loaded! " + gdPicture.GetStat().ToString());
                            return;
                        }
                    } while (gdPicture.TwainGetState() > TwainStatus.TWAIN_SOURCE_ENABLED);

                    //Clean up resources
                    gdPicture.TwainCloseSource(); //Closing the source
                    gdPicture.TiffCloseMultiPageFile(multipageHandle); //Closing multipage file
                    gdPicture.ReleaseGdPictureImage(multipageHandle); //Releasing the multipage image

                    //מוסיף את הסריקה לקובץ הרצוי
                    GdPictureImaging oGdPictureImaging = new GdPictureImaging();
                    string pathToAddFile = fileView.FirstSelectedItem.Path;
                    string newPath;
                    int index = 2;
                    do
                    {
                        newPath = System.IO.Path.GetDirectoryName(pathToAddFile) + "\\(" + index + ")" + System.IO.Path.GetFileName(pathToAddFile);
                        index++;
                    } while (File.Exists(newPath));
                    int DocImageID = 0;
                    int AddImgID = 0;
                    DocImageID = oGdPictureImaging.CreateGdPictureImageFromFile(pathToAddFile);
                    AddImgID = oGdPictureImaging.CreateGdPictureImageFromFile(savingPath);
                    oGdPictureImaging.TiffInsertPageFromGdPictureImage(DocImageID, m_GDViewer.CurrentPage, AddImgID);
                    m_GDViewer.CloseDocument();
                    oGdPictureImaging.TiffSaveMultiPageToFile(DocImageID, newPath, TiffCompression.TiffCompressionAUTO);
                    oGdPictureImaging.ReleaseGdPictureImage(ImageID);
                    oGdPictureImaging.ReleaseGdPictureImage(AddImgID);
                    //oGdPictureImaging.TiffInsertPageFromFile(DocImageID, m_GDViewer.CurrentPage, "temp.tif");
                    //oGdPictureImaging.TiffSaveMultiPageToFile(DocImageID, fileView.FirstSelectedItem.Path, TiffCompression.TiffCompressionAUTO);
                    //oGdPictureImaging.ReleaseGdPictureImage(DocImageID);
                    File.Delete(savingPath);
                    folderView.RefreshTree();
                    fileView.RefreshView();
                    var file = fileView.GetItemFromName(System.IO.Path.GetFileNameWithoutExtension(newPath));
                    fileView.GetListItem(file.Index).Selected = true;
                    m_GDViewer.DisplayFromFile(newPath);
                    ShowCurrentPage();

                }

            }
            else
            {
                System.Windows.Forms.MessageBox.Show("נא בחר קובץ טיף");
            }


            //private void fileView_AfterLabelEdit(object sender, ItemLabelEditEventArgs e)
            //{
            //    if (fileView.FirstSelectedItem!=null)
            //    {
            //        m_GDViewer.ZoomMode = ViewerZoomMode.ZoomModeWidthViewer;
            //        m_GDViewer.DisplayFromFile(fileView.FirstSelectedItem.Path);
            //        ShowCurrentPage();
            //    }
            //}






            //    static void WalkDirectoryTree(string root, string text)
            //    {

            //        string[] files = null;
            //        string[] subDirs = null;

            //        //First, process all the files directly under this folder
            //        try
            //        {
            //            files = Directory.GetFiles(root, "*" + text + "*");
            //        }
            //        //This is thrown if even one of the files requires permissions greater
            //        //than the application provides.
            //        catch (UnauthorizedAccessException ex)
            //        {
            //            //This code just writes out the message and continues to recurse.
            //            //You may decide to do something different here. For example, you
            //            //can try to elevate your privileges and access the file again.
            //            MessageBox.Show(ex.Message);

            //        }

            //        catch (System.IO.DirectoryNotFoundException ex)
            //        {
            //            Console.WriteLine(ex.Message);
            //        }

            //        if (files != null && files.Length != 0)
            //        {
            //            foreach (string fi in files)
            //            {
            //                //In this example, we only access the existing FileInfo object. If we
            //                //want to open, delete or modify the file, then
            //                //a try-catch block is required here to handle the case
            //                //where the file has been deleted since the call to TraverseTree().
            //                MessageBox.Show(fi);
            //                //Console.WriteLine(fi.FullName);
            //            }
            //        }

            //        //Now find all the subdirectories under this directory.
            //        subDirs = Directory.GetDirectories(root);

            //        foreach (string dirInfo in subDirs)
            //        {
            //            if (dirInfo.Contains(text) == true)
            //            {
            //                MessageBox.Show(dirInfo);
            //            }
            //            //Recursive call for each subdirectory.
            //            WalkDirectoryTree(dirInfo, text);
            //        }
            //    }

            //}
        }

        private void SetScanSettings()
        {
            gdPicture.TwainSetAutoFeed(true); //Enable AutoFeed
            gdPicture.TwainSetAutoScan(true); //To achieve the maximum scanning rate                   
            gdPicture.TwainSetHideUI(!(bool)chbDisplayUI.IsChecked); //Asks to the device to hide his GUI
            int resolution = int.Parse(((ComboBoxItem)cbResolution.SelectedItem).Content.ToString());
            gdPicture.TwainSetResolution(resolution);//Set scanning resolution
            if (rbBlackAndWhite.IsChecked == true)
            {
                gdPicture.TwainSetPixelType(TwainPixelType.TWPT_BW); // Black & White
            }
            else if (rbColor.IsChecked == true)
            {
                gdPicture.TwainSetPixelType(TwainPixelType.TWPT_RGB);//color
            }
            else if (rbGrayScale.IsChecked == true)
            {
                gdPicture.TwainSetPixelType(TwainPixelType.TWPT_GRAY);//Gray Scale
            }
            gdPicture.TwainSetBitDepth(1); //  1 bpp
        }

        private void btnGoToClientFolder_Click(object sender, RoutedEventArgs e)
        {
            folderView.RootFolder = @rootPath;
            policySelected = null;
            cbPolicyDetailsBinding();
        }

        private void btnSendMail_Click(object sender, RoutedEventArgs e)
        {
            string[] address = null;
            if (rbMailToClient.IsChecked == true)
            {
                if (clientArchive == null && policyOwner == null)
                {
                    MessageBox.Show("לא ניתן לחבר ללקוח", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                }
                else if (clientArchive != null)
                {
                    if (clientArchive.Email == "")
                    {
                        MessageBox.Show("אין ללקוח כתובת דוא''ל במערכת", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                        //return;
                    }
                    else
                    {
                        address = new string[] { clientArchive.Email };
                    }
                }
                else if (policyOwner != null)
                {
                    if (policyOwner.Email == "")
                    {
                        MessageBox.Show("אין למפעל כתובת דוא''ל במערכת", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                        //return;
                    }
                    else
                    {
                        address = new string[] { policyOwner.Email };
                    }
                }
            }
            var selectedFiles = fileView.SelectedItems;
            List<string> attachments = new List<string>();

            foreach (var item in selectedFiles)
            {
                attachments.Add(((LogicNP.FileViewControl.ListItem)item).Path);
            }
            email.SendEmailFromOutlook("", "", attachments.ToArray(), address);
        }

        private void rbMailToClient_Checked(object sender, RoutedEventArgs e)
        {
            string rbContent = ((RadioButton)sender).Content.ToString();
            string rbContentPath = strAppDir + @"Coral Files\MailSettings.txt";
            File.WriteAllText(rbContentPath, rbContent);
        }

        private void btnSettings_Click(object sender, RoutedEventArgs e)
        {
            frmScanSettings settingsWindow = new frmScanSettings();
            settingsWindow.ShowDialog();
            SetSettings();
        }

        private void rbMailWithoutAddress_Checked(object sender, RoutedEventArgs e)
        {
            string rbContent = ((RadioButton)sender).Content.ToString();
            string rbContentPath = strAppDir + @"Coral Files\MailSettings.txt";
            File.WriteAllText(rbContentPath, rbContent);
        }

        private void fileView_FileViewDragDrop(object sender, FileViewDragDropEventArgs e)
        {
            
        }

        private void fileView_AfterItemAdd(object sender, FileViewEventArgs e)
        {

        }
    }
}



