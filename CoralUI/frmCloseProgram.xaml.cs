﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Media;
using System.Threading;

namespace CoralUI
{
    /// <summary>
    /// Interaction logic for frmCloseProgram.xaml
    /// </summary>
    public partial class frmCloseProgram : Window
    {
        public bool confirm { get; set; }        
        public frmCloseProgram()
        {
            confirm = false;
            InitializeComponent();
            SystemSounds.Beep.Play();           
        }

        //מבטל את סגירת התוכנה
        private void btnNo_Click(object sender, RoutedEventArgs e)
        {            
            confirm=true;
            this.Close();
           
        }

        //גורם לסגירת התוכנה
        private void btnYes_Click(object sender, RoutedEventArgs e)
        {            
            this.Close();
        }
    }

}

