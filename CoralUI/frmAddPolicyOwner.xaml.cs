﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using CoralBusinessLogics;
using System.IO;

namespace CoralUI
{
    /// <summary>
    /// Interaction logic for frmAddPolicyOwner.xaml
    /// </summary>
    public partial class frmAddPolicyOwner : Window
    {
        PolicyHoldersLogic policyHolderLogics = new PolicyHoldersLogic();
        InsurancesLogic insuranceLogics = new InsurancesLogic();
        List<FactoryNumber> policyHolderNumbers = new List<FactoryNumber>();
        ListViewSettings lvSettings = new ListViewSettings();
        private bool isChanges = false;
        bool confirmBeforeClosing = true;
        bool cancelClose = false;
        PolicyOwner policyOwner = null;
        List<object> nullErrorList = null;
        InputsValidations validations = new InputsValidations();
        string clientDirPath = null;


        public frmAddPolicyOwner()
        {
            InitializeComponent();
        }
        public frmAddPolicyOwner(PolicyOwner policyOwner)
        {
            InitializeComponent();
            this.policyOwner = policyOwner;
        }
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            string strAppDir = System.AppDomain.CurrentDomain.BaseDirectory;
            string path = strAppDir + @"Coral Files\ClientsPath.txt";
            if (File.Exists(path))
            {
                clientDirPath = File.ReadAllText(path);
            }
            else
            {
                MessageBox.Show("לא נמצא תיקיית הלקוחות. נא בדוק בהגדרות ראשוניות את נתיב התיקייה");
                Close();
                return;
            }
            cbInsuranceTypeBinding();
            if (policyOwner!=null)
            {
                txtAptNumber.Text = policyOwner.AptNumber;
                txtCellPhone.Text = policyOwner.CellPhone;
                txtCity.Text = policyOwner.City;
                txtContactName.Text = policyOwner.ContactPerson;
                txtEmail.Text = policyOwner.Email;
                txtFactoryId.Text = policyOwner.IdNumber;
                txtFactoryName.Text = policyOwner.PolicyOwnerName;
                txtFax.Text = policyOwner.Fax;
                txtHouseNumber.Text = policyOwner.HomeNumber;
                txtMailingAddress.Text = policyOwner.MailingAddress;
                txtStreet.Text = policyOwner.Street;
                txtWorkPhone.Text = policyOwner.Phone;
                txtZipCode.Text = policyOwner.ZipCode;

                policyHolderNumbers = policyHolderLogics.GetNumbersByPolicyHolder((policyOwner).PolicyOwnerID);
                dgNumbersBinding();
            }
            //dgPolicyOwnersBinding();

        }

        //private void dgPolicyOwnersBinding()
        //{
        //    var policyOwners=policyHolderLogics.GetAllPolicyHolders();
        //    dgPolicyOwners.ItemsSource = policyOwners;
        //    if (policyOwner!=null)
        //    {
        //        dgPolicyOwners.SelectedItem = policyOwners.FirstOrDefault(o => o.PolicyOwnerID == policyOwner.PolicyOwnerID);
        //    }
        //}
        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            if (nullErrorList != null && nullErrorList.Count > 0)
            {
                foreach (var item in nullErrorList)
                {
                    if (item is TextBox)
                    {
                        ((TextBox)item).ClearValue(BorderBrushProperty);
                    }
                    else if (item is ComboBox)
                    {
                        ((ComboBox)item).ClearValue(BorderBrushProperty);
                    }
                    else if (item is DatePicker)
                    {
                        ((DatePicker)item).ClearValue(BorderBrushProperty);
                    }
                }
            }
            int policyHolderId=0;
            try
            {
                nullErrorList = validations.InputNullValidation(new object[] { txtFactoryId, txtFactoryName});
                if (nullErrorList.Count > 0)
                {
                    tbItemGeneral.Focus();
                    cancelClose = true;
                    MessageBox.Show("נא למלאות שדות חובה", "", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }

                
                if (policyOwner == null)
                {
                    policyHolderId = policyHolderLogics.InsertPolicyHolder(txtFactoryName.Text, txtFactoryId.Text, txtCity.Text, txtStreet.Text, txtHouseNumber.Text, txtAptNumber.Text, txtZipCode.Text, txtMailingAddress.Text, txtWorkPhone.Text, txtCellPhone.Text, txtFax.Text, txtEmail.Text, txtContactName.Text);

                }
                else
                {
                    policyHolderId = policyHolderLogics.UpdatePolicyHolder(policyOwner, txtFactoryName.Text, txtFactoryId.Text, txtCity.Text, txtStreet.Text, txtHouseNumber.Text, txtAptNumber.Text, txtZipCode.Text, txtMailingAddress.Text, txtWorkPhone.Text, txtCellPhone.Text, txtFax.Text, txtEmail.Text, txtContactName.Text);
                }
                SaveUnsavedNumber();
                policyHolderLogics.InsertNumbersToPolicyHolder(policyHolderId, policyHolderNumbers);
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
            }

            string path = clientDirPath + @"\PolicyOwners\" + policyHolderId;

            if (!Directory.Exists(@path))
            {
                Directory.CreateDirectory(@path);
            }

            if (sender is Button)
                {
                    Button btn = (Button)sender;
                    if (btn.Name == "btnSave")
                    {
                        confirmBeforeClosing = false;
                        Close();
                    }
                }
            
        }

        private void SaveUnsavedNumber()
        {
            if (cbInsuranceType.SelectedItem!=null|| cbInsuranceCompany.SelectedItem != null || txtNumber.Text!="")
            {
                btnAddNumber_Click(this, new RoutedEventArgs());
            }
        }

        private void btnAddNumber_Click(object sender, RoutedEventArgs e)
        {
            cbInsuranceType.ClearValue(BorderBrushProperty);
            cbInsuranceCompany.ClearValue(BorderBrushProperty);
            txtNumber.ClearValue(BorderBrushProperty);
            //שלוח לבדיקת שדות חובה
            if (!NumberInputsNullValidation())
            {
                if (sender is Window)
                {
                    MessageBox.Show("שים לב, פרטי מספר המפעל לא עודכנו במערכת", "מידע", MessageBoxButton.OK, MessageBoxImage.Information);
                }
                return;
            }
            //מוסיף מס' חדש
            if (txtBtnAddNumber.Text != "עדכן מספר")
            {
                FactoryNumber newNumber = new FactoryNumber() { InsuranceID = ((Insurance)cbInsuranceType.SelectedItem).InsuranceID, Insurance = (Insurance)cbInsuranceType.SelectedItem, CompanyID = ((InsuranceCompany)cbInsuranceCompany.SelectedItem).CompanyID, Company = ((InsuranceCompany)cbInsuranceCompany.SelectedItem).Company, Number = txtNumber.Text };
                policyHolderNumbers.Add(newNumber);
                dgNumbersBinding();
            }
            //מעדכן מס' קיים 
            else
            {
                try
                {
                    FactoryNumber numberToUpdate = policyHolderNumbers.FirstOrDefault(n => n.Number == ((FactoryNumber)dgNumbers.SelectedItem).Number);
                    //var numberInDb = agentNumbers.FirstOrDefault(n => n.Number == numberToUpdate.Number);
                    //if (numberInDb == null)
                    //{
                    //    agentsLogic.UpdateNumberToAgent(agentToUpdate, numberToUpdate.Number, txtNumber.Text, numberToUpdate.CompanyName);
                    //}
                    //else
                    //{
                    //    numberInDb.Number = txtNumber.Text;
                    //}
                    numberToUpdate.Number = txtNumber.Text;
                    dgNumbersBinding();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "שגיאה", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            CleanGbNumbers();
            dgNumbers.UnselectAll();

        }

        private void dgNumbersBinding()
        {
            dgNumbers.ItemsSource = policyHolderNumbers.ToList();
            lvSettings.AutoSizeColumns(dgNumbers.View);
        }



        private bool NumberInputsNullValidation()
        {
            bool isValid = true;
            if (cbInsuranceType.SelectedItem == null)
            {
                cbInsuranceType.BorderBrush = Brushes.Red;
                isValid = false;
            }
            if (cbInsuranceCompany.SelectedItem == null)
            {
                cbInsuranceCompany.BorderBrush = Brushes.Red;
                isValid = false;
            }
            if (txtNumber.Text == "")
            {
                txtNumber.BorderBrush = Brushes.Red;
                isValid = false;
            }
            return isValid;
        }

        private void CleanGbNumbers()
        {
            txtBtnAddNumber.Text = "הוסף מספר";
            var addUriSource = new Uri(@"/Images/addPolicy.png", UriKind.Relative);
            imgBtnAddNumber.Source = new BitmapImage(addUriSource);
            cbInsuranceType.SelectedItem = null;
            cbInsuranceType.IsEnabled = true;
            cbInsuranceCompany.SelectedItem = null;
            txtNumber.Clear();
        }

        private void dgNumbers_MouseDown(object sender, MouseButtonEventArgs e)
        {
            dgNumbers.UnselectAll();
        }

        private void dgNumbers_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (dgNumbers.SelectedItem == null)
            {
                CleanGbNumbers();
                return;
            }
            txtBtnAddNumber.Text = "עדכן מספר";
            var updateUriSource = new Uri(@"/Images/jidushim (1).png", UriKind.Relative);
            imgBtnAddNumber.Source = new BitmapImage(updateUriSource);
            cbInsuranceTypeBinding();
            cbInsuranceType.IsEnabled = false;
            FactoryNumber numberSelected = (FactoryNumber)dgNumbers.SelectedItem;
            var insuranceTypes = cbInsuranceType.Items;
            foreach (var item in insuranceTypes)
            {
                Insurance insuranceType = (Insurance)item;
                if (insuranceType.InsuranceID == numberSelected.InsuranceID)
                {
                    cbInsuranceType.SelectedItem = item;
                    break;
                }
            }
            //cbInsuranceType.SelectedItem = (insuranceLogics.GetAllInsurances()).FirstOrDefault(i => i.InsuranceID == numberSelected.InsuranceID);
            cbInsuranceCompanyBinding(true);
            var companies = cbInsuranceCompany.Items;
            foreach (var item in companies)
            {
                InsuranceCompany company = (InsuranceCompany)item;
                if (company.CompanyID == numberSelected.CompanyID)
                {
                    cbInsuranceCompany.SelectedItem = item;
                    break;
                }
            }
            //cbInsuranceCompany.SelectedItem = (insuranceLogics.GetCompaniesByInsurance(((Insurance)cbInsuranceType.SelectedItem).InsuranceID)).FirstOrDefault(c => c.CompanyID == numberSelected.CompanyID);
            cbInsuranceCompany.IsEnabled = false;
            btnUpdateCompaniesTable.IsEnabled = false;
            txtNumber.Text = numberSelected.Number;
        }


        private void cbInsuranceTypeBinding()
        {
            cbInsuranceType.ItemsSource = insuranceLogics.GetAllInsurances().Where(i => i.InsuranceName == "חיים" || i.InsuranceName == "פיננסים" || i.InsuranceName == "בריאות");
            cbInsuranceType.DisplayMemberPath = "InsuranceName";
        }

        private void cbInsuranceCompanyBinding(bool isUpdate)
        {
            if (!isUpdate)
            {
                cbInsuranceCompany.ItemsSource = insuranceLogics.GetActiveCompaniesByInsurance(((Insurance)cbInsuranceType.SelectedItem).InsuranceID);
            }
            else
            {
                cbInsuranceCompany.ItemsSource = insuranceLogics.GetCompaniesByInsurance(((Insurance)cbInsuranceType.SelectedItem).InsuranceID);
            }
            cbInsuranceCompany.DisplayMemberPath = "Company.CompanyName";
        }

        private void cbInsuranceType_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cbInsuranceType.SelectedItem != null)
            {
                cbInsuranceCompany.IsEnabled = true;
                btnUpdateCompaniesTable.IsEnabled = true;
                try
                {
                    cbInsuranceCompanyBinding(false);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "שגיאה", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            else
            {
                cbInsuranceCompany.IsEnabled = false;
                btnUpdateCompaniesTable.IsEnabled = false;
            }
        }

        //private void dgPolicyOwners_SelectionChanged(object sender, SelectionChangedEventArgs e)
        //{
        //    if (dgPolicyOwners.SelectedItem != null)
        //    {
        //        gboxAddress.IsEnabled = false;
        //        gboxContactDetails.IsEnabled = false;
        //        gboxGeneral.IsEnabled = false;
        //        PolicyOwner owner = (PolicyOwner)dgPolicyOwners.SelectedItem;
        //        txtAptNumber.Text = owner.AptNumber;
        //        txtCellPhone.Text = owner.CellPhone;
        //        txtCity.Text = owner.City;
        //        txtContactName.Text = owner.ContactPerson;
        //        txtEmail.Text = owner.Email;
        //        txtFactoryId.Text = owner.IdNumber;
        //        txtFactoryName.Text = owner.PolicyOwnerName;
        //        txtFax.Text = owner.Fax;
        //        txtHouseNumber.Text = owner.HomeNumber;
        //        txtMailingAddress.Text = owner.MailingAddress;
        //        txtStreet.Text = owner.Street;
        //        txtWorkPhone.Text = owner.Phone;
        //        txtZipCode.Text = owner.ZipCode;

        //        policyHolderNumbers = policyHolderLogics.GetNumbersByPolicyHolder((owner).PolicyOwnerID);
        //        dgNumbersBinding();
        //    }
        //}

        //private void dgPolicyOwners_MouseDown(object sender, MouseButtonEventArgs e)
        //{
        //    dgPolicyOwners.UnselectAll();
        //    txtAptNumber.Clear();
        //    txtCellPhone.Clear();
        //    txtCity.Clear();
        //    txtContactName.Clear();
        //    txtEmail.Clear();
        //    txtFactoryId.Clear();
        //    txtFactoryName.Clear(); ;
        //    txtFax.Clear();
        //    txtHouseNumber.Clear();
        //    txtMailingAddress.Clear();
        //    txtStreet.Clear();
        //    txtWorkPhone.Clear();
        //    txtZipCode.Clear();

        //    gboxAddress.IsEnabled = true;
        //    gboxContactDetails.IsEnabled = true;
        //    gboxGeneral.IsEnabled = true;

        //    policyHolderNumbers.Clear();
        //    dgNumbersBinding();

        //}

        private void btnUpdateCompaniesTable_Click(object sender, RoutedEventArgs e)
        {
            int? selectedItemId = null;
            if (cbInsuranceCompany.SelectedItem != null)
            {
                selectedItemId = ((InsuranceCompany)cbInsuranceCompany.SelectedItem).CompanyID;
            }
            InsuranceCompany company = new InsuranceCompany() { InsuranceID = ((Insurance)cbInsuranceType.SelectedItem).InsuranceID };
            frmUpdateTable updateTable = new frmUpdateTable(company);
            updateTable.ShowDialog();
            cbInsuranceCompanyBinding(false);
            if (updateTable.ItemAdded != null)
            {
                SelectItemInCb(((InsuranceCompany)updateTable.ItemAdded).CompanyID);
            }
            else if (selectedItemId != null)
            {
                SelectItemInCb((int)selectedItemId);
            }
        }

        private void SelectItemInCb(int id)
        {
            var items = cbInsuranceCompany.Items;
            foreach (var item in items)
            {
                InsuranceCompany parsedItem = (InsuranceCompany)item;
                if (parsedItem.CompanyID == id)
                {
                    cbInsuranceCompany.SelectedItem = item;
                    break;
                }
            }
        }

        private void txtFactoryName_GotFocus(object sender, RoutedEventArgs e)
        {
            isChanges = true;
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (confirmBeforeClosing && isChanges)
            {
                if (MessageBox.Show("האם ברצונך לשמור שינוים שבצעת", "אישור", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    btnSave_Click(sender, new RoutedEventArgs());
                    if (cancelClose)
                    {
                        e.Cancel = true;
                        cancelClose = false;
                    }
                }
            }
        }
    }
}
