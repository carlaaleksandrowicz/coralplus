﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using CoralBusinessLogics;

namespace CoralUI
{
    /// <summary>
    /// Interaction logic for frmAllOffers.xaml
    /// </summary>
    public partial class frmAllOffers : Window
    {
        PoliciesLogic elementaryLogic = new PoliciesLogic();
        PersonalAccidentsLogic personalAccidentsLogic = new PersonalAccidentsLogic();
        TravelLogic travelLogic = new TravelLogic();
        LifePoliciesLogic lifeLogic = new LifePoliciesLogic();
        HealthPoliciesLogic healthLogic = new HealthPoliciesLogic();
        FinancePoliciesLogic financeLogic = new FinancePoliciesLogic();
        ClientsLogic clientsLogic = new ClientsLogic();
        IndustriesLogic industryLogic = new IndustriesLogic();
        LifeIndustryLogic lifeIndustryLogic = new LifeIndustryLogic();
        FinanceIndustryLogic financeIndustryLogic = new FinanceIndustryLogic();
        InsurancesLogic insuranceLogic = new InsurancesLogic();
        ForeingWorkersLogic foreingWorkersLogic = new ForeingWorkersLogic();
        ForeingWorkersIndutryLogic foreingWorkersIndustryLogic = new ForeingWorkersIndutryLogic();
        User user;
        Email email = new Email();
        ExcelLogic excelLogic = new ExcelLogic();
        public frmAllOffers(User user)
        {
            InitializeComponent();
            this.SizeToContent = SizeToContent.Manual;
            this.user = user;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            cbStatus.SelectedIndex = 0;
            cbCompanyBinding();
            dtxtPeriod.Value = DateTime.Now;
            SetPermissions();
        }

        private void SetPermissions()
        {
            if (user.IsElementaryPermission == false)
            {
                tbItemElementary.IsEnabled = false;
                dgElementaryOffer.ItemsSource = null;
                dgElementaryOffer.IsEnabled = false;
                tbItemRenewals.IsEnabled = false;
            }
            if (user.IsFinancePermission == false)
            {
                tbItemFinance.IsEnabled = false;
            }
            if (user.IsForeingWorkersPermission == false)
            {
                tbItemForeingWorkers.IsEnabled = false;
            }
            if (user.IsHealthPermission == false)
            {
                tbItemHealth.IsEnabled = false;
            }
            if (user.IsLifePermission == false)
            {
                tbItemLife.IsEnabled = false;
            }
            if (user.IsPersonalAccidentsPermission == false)
            {
                tbItemPersonalAccidents.IsEnabled = false;
            }
            if (user.IsRenewalsPermission == false)
            {
                tbItemRenewals.IsEnabled = false;
            }
            if (user.IsScanPermission == false)
            {
                btnOpticArchive.IsEnabled = false;
            }
            if (user.IsTravelPermission == false)
            {
                tbItemTravel.IsEnabled = false;
            }
        }

        private void cbCompanyBinding()
        {
            cbCompany.ItemsSource = insuranceLogic.GetAllCompanies();
            cbCompany.DisplayMemberPath = "CompanyName";
        }

        private void cbIndustryBinding()
        {
            if (tbItemElementary.IsSelected)
            {
                cbIndustry.ItemsSource = industryLogic.GetAllIndustries();
                cbIndustry.DisplayMemberPath = "InsuranceIndustryName";
                cbIndustry.IsEnabled = true;
            }
            else if (tbItemLife.IsSelected)
            {
                cbIndustry.ItemsSource = lifeIndustryLogic.GetAllLifeIndustries();
                cbIndustry.DisplayMemberPath = "LifeIndustryName";
                cbIndustry.IsEnabled = true;
            }
            else if (tbItemForeingWorkers.IsSelected)
            {
                cbIndustry.ItemsSource = foreingWorkersIndustryLogic.GetAllForeingWorkersIndustries();
                cbIndustry.DisplayMemberPath = "ForeingWorkersIndustryName";
                cbIndustry.IsEnabled = true;
            }
            else if (tbItemFinance.IsSelected)
            {
                cbIndustry.ItemsSource = financeIndustryLogic.GetAllFinanceProgramTypes();
                cbIndustry.DisplayMemberPath = "ProgramTypeName";
                cbIndustry.IsEnabled = true;
            }
            else if (tbItemRenewals.IsSelected)
            {
                cbIndustry.ItemsSource = industryLogic.GetAllIndustries();
                cbIndustry.DisplayMemberPath = "InsuranceIndustryName";
                cbIndustry.IsEnabled = true;
            }
            else
            {
                cbIndustry.IsEnabled = false;
            }
        }

        private void policiesTabs_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.Source is TabControl)
            {
                ClearSearchInputs();
                cbIndustryBinding();
                OffersBinding();
            }
        }

        private void OffersBinding()
        {
            int count = 0;
            DateTime now = DateTime.Now;
            if (tbItemElementary.IsSelected)
            {
                cbStatus.IsEnabled = true;
                dtxtPeriod.IsEnabled = false;
                var elementaryPolicies = elementaryLogic.GetOffers();
                if (elementaryPolicies != null)
                {
                    if (cbIndustry.SelectedItem != null)
                    {
                        int industryId = ((InsuranceIndustry)cbIndustry.SelectedItem).InsuranceIndustryID;
                        elementaryPolicies = elementaryPolicies.Where(p => p.InsuranceIndustryID == industryId).ToList();
                    }
                    if (cbCompany.SelectedItem != null)
                    {
                        int companyId = ((Company)cbCompany.SelectedItem).CompanyID;
                        elementaryPolicies = elementaryPolicies.Where(p => p.CompanyID == companyId).ToList();
                    }
                    if (txtSearch.Text != "")
                    {
                        string key = txtSearch.Text;
                        elementaryPolicies = elementaryPolicies.Where(p => p.Client.FirstName.StartsWith(key) || p.Client.LastName.StartsWith(key) || (p.Client.LastName + " " + p.Client.FirstName).StartsWith(key) || (p.Client.FirstName + " " + p.Client.LastName).StartsWith(key) || p.Client.IdNumber.StartsWith(key) || p.PolicyNumber.StartsWith(key)).ToList();
                    }
                    if (cbStatus.SelectedIndex == 0)
                    {
                        dgElementaryOffer.ItemsSource = elementaryPolicies.Where(p => p.EndDate >= now).ToList();
                    }
                    else if (cbStatus.SelectedIndex == 1)
                    {
                        dgElementaryOffer.ItemsSource = elementaryPolicies.Where(p => p.EndDate < now).ToList();
                    }
                    else
                    {
                        dgElementaryOffer.ItemsSource = elementaryPolicies;
                    }
                    count = elementaryPolicies.Count;
                }
            }
            else if (tbItemForeingWorkers.IsSelected)
            {
                cbStatus.IsEnabled = true;
                dtxtPeriod.IsEnabled = false;
                var foreingWorkersPolicies = foreingWorkersLogic.GetOffers();
                if (foreingWorkersPolicies != null)
                {
                    if (cbIndustry.SelectedItem != null)
                    {
                        int industryId = ((ForeingWorkersIndustry)cbIndustry.SelectedItem).ForeingWorkersIndustryID;
                        foreingWorkersPolicies = foreingWorkersPolicies.Where(p => p.ForeingWorkersIndustryID == industryId).ToList();
                    }
                    if (cbCompany.SelectedItem != null)
                    {
                        int companyId = ((Company)cbCompany.SelectedItem).CompanyID;
                        foreingWorkersPolicies = foreingWorkersPolicies.Where(p => p.CompanyID == companyId).ToList();
                    }
                    if (txtSearch.Text != "")
                    {
                        string key = txtSearch.Text;
                        foreingWorkersPolicies = foreingWorkersPolicies.Where(p => p.Client.FirstName.StartsWith(key) || p.Client.LastName.StartsWith(key) || (p.Client.LastName + " " + p.Client.FirstName).StartsWith(key) || (p.Client.FirstName + " " + p.Client.LastName).StartsWith(key) || p.Client.IdNumber.StartsWith(key) || p.PolicyNumber.StartsWith(key)).ToList();
                    }
                    if (cbStatus.SelectedIndex == 0)
                    {
                        dgForeingWorkersPolicies.ItemsSource = foreingWorkersPolicies.Where(p => p.EndDate >= now).ToList();
                    }
                    else if (cbStatus.SelectedIndex == 1)
                    {
                        dgForeingWorkersPolicies.ItemsSource = foreingWorkersPolicies.Where(p => p.EndDate < now).ToList();
                    }
                    else
                    {
                        dgForeingWorkersPolicies.ItemsSource = foreingWorkersPolicies;
                    }
                    count = foreingWorkersPolicies.Count;
                }
            }
            else if (tbItemPersonalAccidents.IsSelected)
            {
                cbStatus.IsEnabled = true;
                dtxtPeriod.IsEnabled = false;
                var personalAccidentsPolicies = personalAccidentsLogic.GetOffers();
                if (personalAccidentsPolicies != null)
                {
                    if (cbCompany.SelectedItem != null)
                    {
                        int companyId = ((Company)cbCompany.SelectedItem).CompanyID;
                        personalAccidentsPolicies = personalAccidentsPolicies.Where(p => p.CompanyID == companyId).ToList();
                    }
                    if (txtSearch.Text != "")
                    {
                        string key = txtSearch.Text;
                        personalAccidentsPolicies = personalAccidentsPolicies.Where(p => p.Client.FirstName.StartsWith(key) || p.Client.LastName.StartsWith(key) || (p.Client.LastName + " " + p.Client.FirstName).StartsWith(key) || (p.Client.FirstName + " " + p.Client.LastName).StartsWith(key) || p.Client.IdNumber.StartsWith(key) || p.PolicyNumber.StartsWith(key)).ToList();
                    }
                    if (cbStatus.SelectedIndex == 0)
                    {
                        dgPersonalAccidentsPolicies.ItemsSource = personalAccidentsPolicies.Where(p => p.EndDate >= now).ToList();
                    }
                    else if (cbStatus.SelectedIndex == 1)
                    {
                        dgPersonalAccidentsPolicies.ItemsSource = personalAccidentsPolicies.Where(p => p.EndDate < now).ToList();
                    }
                    else
                    {
                        dgPersonalAccidentsPolicies.ItemsSource = personalAccidentsPolicies;
                    }
                    count = personalAccidentsPolicies.Count;
                }
            }
            else if (tbItemTravel.IsSelected)
            {
                cbStatus.IsEnabled = true;
                dtxtPeriod.IsEnabled = false;
                var travelPolicies = travelLogic.GetOffers();
                if (travelPolicies != null)
                {
                    if (cbCompany.SelectedItem != null)
                    {
                        int companyId = ((Company)cbCompany.SelectedItem).CompanyID;
                        travelPolicies = travelPolicies.Where(p => p.CompanyID == companyId).ToList();
                    }
                    if (txtSearch.Text != "")
                    {
                        string key = txtSearch.Text;
                        travelPolicies = travelPolicies.Where(p => p.Client.FirstName.StartsWith(key) || p.Client.LastName.StartsWith(key) || (p.Client.LastName + " " + p.Client.FirstName).StartsWith(key) || (p.Client.FirstName + " " + p.Client.LastName).StartsWith(key) || p.Client.IdNumber.StartsWith(key) || p.PolicyNumber.StartsWith(key)).ToList();
                    }
                    if (cbStatus.SelectedIndex == 0)
                    {
                        dgTravelPolicies.ItemsSource = travelPolicies.Where(p => p.EndDate >= now).ToList();
                    }
                    else if (cbStatus.SelectedIndex == 1)
                    {
                        dgTravelPolicies.ItemsSource = travelPolicies.Where(p => p.EndDate < now).ToList();
                    }
                    else
                    {
                        dgTravelPolicies.ItemsSource = travelPolicies;
                    }
                    count = travelPolicies.Count;

                }
            }
            else if (tbItemLife.IsSelected)
            {
                cbStatus.IsEnabled = true;
                dtxtPeriod.IsEnabled = false;
                var lifePolicies = lifeLogic.GetOffers();
                if (lifePolicies != null)
                {
                    if (cbIndustry.SelectedItem != null)
                    {
                        int industryId = ((LifeIndustry)cbIndustry.SelectedItem).LifeIndustryID;
                        lifePolicies = lifePolicies.Where(p => p.LifeIndustryID == industryId).ToList();
                    }
                    if (cbCompany.SelectedItem != null)
                    {
                        int companyId = ((Company)cbCompany.SelectedItem).CompanyID;
                        lifePolicies = lifePolicies.Where(p => p.CompanyID == companyId).ToList();
                    }
                    if (txtSearch.Text != "")
                    {
                        string key = txtSearch.Text;
                        lifePolicies = lifePolicies.Where(p => p.Client.FirstName.StartsWith(key) || p.Client.LastName.StartsWith(key) || (p.Client.LastName + " " + p.Client.FirstName).StartsWith(key) || (p.Client.FirstName + " " + p.Client.LastName).StartsWith(key) || p.Client.IdNumber.StartsWith(key) || p.PolicyNumber.StartsWith(key)).ToList();
                    }
                    if (cbStatus.SelectedIndex == 0)
                    {
                        dgLife.ItemsSource = lifePolicies.Where(p => p.EndDate >= now).ToList();
                    }
                    else if (cbStatus.SelectedIndex == 1)
                    {
                        dgLife.ItemsSource = lifePolicies.Where(p => p.EndDate < now).ToList();
                    }
                    else
                    {
                        dgLife.ItemsSource = lifePolicies;
                    }
                    count = lifePolicies.Count;

                }
            }
            else if (tbItemHealth.IsSelected)
            {
                cbStatus.IsEnabled = true;
                dtxtPeriod.IsEnabled = false;
                var healthPolicies = healthLogic.GetOffers();
                if (healthPolicies != null)
                {
                    if (cbCompany.SelectedItem != null)
                    {
                        int companyId = ((Company)cbCompany.SelectedItem).CompanyID;
                        healthPolicies = healthPolicies.Where(p => p.CompanyID == companyId).ToList();
                    }
                    if (txtSearch.Text != "")
                    {
                        string key = txtSearch.Text;
                        healthPolicies = healthPolicies.Where(p => p.Client.FirstName.StartsWith(key) || p.Client.LastName.StartsWith(key) || (p.Client.LastName + " " + p.Client.FirstName).StartsWith(key) || (p.Client.FirstName + " " + p.Client.LastName).StartsWith(key) || p.Client.IdNumber.StartsWith(key) || p.PolicyNumber.StartsWith(key)).ToList();
                    }
                    if (cbStatus.SelectedIndex == 0)
                    {
                        dgHealth.ItemsSource = healthPolicies.Where(p => p.EndDate >= now).ToList();
                    }
                    else if (cbStatus.SelectedIndex == 1)
                    {
                        dgHealth.ItemsSource = healthPolicies.Where(p => p.EndDate < now).ToList();
                    }
                    else
                    {
                        dgHealth.ItemsSource = healthPolicies;
                    }
                    count = healthPolicies.Count;

                }
            }
            else if (tbItemFinance.IsSelected)
            {
                cbStatus.IsEnabled = true;
                dtxtPeriod.IsEnabled = false;
                var financePolicies = financeLogic.GetOffers();
                if (financePolicies != null)
                {
                    if (cbIndustry.SelectedItem != null)
                    {
                        int industryId = ((FinanceProgramType)cbIndustry.SelectedItem).ProgramTypeID;
                        financePolicies = financePolicies.Where(p => p.ProgramTypeID == industryId).ToList();
                    }
                    if (cbCompany.SelectedItem != null)
                    {
                        int companyId = ((Company)cbCompany.SelectedItem).CompanyID;
                        financePolicies = financePolicies.Where(p => p.CompanyID == companyId).ToList();
                    }
                    if (txtSearch.Text != "")
                    {
                        string key = txtSearch.Text;
                        financePolicies = financePolicies.Where(p => p.Client.FirstName.StartsWith(key) || p.Client.LastName.StartsWith(key) || (p.Client.LastName + " " + p.Client.FirstName).StartsWith(key) || (p.Client.FirstName + " " + p.Client.LastName).StartsWith(key) || p.Client.IdNumber.StartsWith(key) || p.AssociateNumber.StartsWith(key)).ToList();
                    }
                    if (cbStatus.SelectedIndex == 0)
                    {
                        dgFinance.ItemsSource = financePolicies.Where(p => p.EndDate >= now).ToList();
                    }
                    else if (cbStatus.SelectedIndex == 1)
                    {
                        dgFinance.ItemsSource = financePolicies.Where(p => p.EndDate < now).ToList();
                    }
                    else
                    {
                        dgFinance.ItemsSource = financePolicies;
                    }
                    count = financePolicies.Count;

                }
            }
            else if (tbItemRenewals.IsSelected)
            {
                cbStatus.IsEnabled = false;
                dtxtPeriod.IsEnabled = true;
                DateTime period = Convert.ToDateTime(dtxtPeriod.Value);
                int month = period.Month + 1;
                int year = period.Year - 1;

                var elementaryPolicies = elementaryLogic.GetOffers().Where(p => ((DateTime)p.StartDate).Month == month && ((DateTime)p.StartDate).Year == year).ToList();
                if (elementaryPolicies != null)
                {
                    if (cbIndustry.SelectedItem != null)
                    {
                        int industryId = ((InsuranceIndustry)cbIndustry.SelectedItem).InsuranceIndustryID;
                        elementaryPolicies = elementaryPolicies.Where(p => p.InsuranceIndustryID == industryId).ToList();
                    }
                    if (cbCompany.SelectedItem != null)
                    {
                        int companyId = ((Company)cbCompany.SelectedItem).CompanyID;
                        elementaryPolicies = elementaryPolicies.Where(p => p.CompanyID == companyId).ToList();
                    }
                    if (txtSearch.Text != "")
                    {
                        string key = txtSearch.Text;
                        elementaryPolicies = elementaryPolicies.Where(p => p.Client.FirstName.StartsWith(key) || p.Client.LastName.StartsWith(key) || (p.Client.LastName + " " + p.Client.FirstName).StartsWith(key) || (p.Client.FirstName + " " + p.Client.LastName).StartsWith(key) || p.Client.IdNumber.StartsWith(key) || p.PolicyNumber.StartsWith(key)).ToList();
                    }
                    dgElementaryRenewals.ItemsSource = elementaryPolicies;
                    count = elementaryPolicies.Count;
                }
            }
            lblCount.Content = string.Format("סה''כ רשומות: {0}", count.ToString());
        }


        private void btnConvertToPolicy_Click(object sender, RoutedEventArgs e)
        {
            Client client = null;
            if (tbItemRenewals.IsSelected)
            {
                MessageBox.Show("לא ניתן לבצע פעולה זאת על חידוש הצעה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            if (tbItemElementary.IsSelected)
            {
                if (dgElementaryOffer.SelectedItem == null)
                {
                    MessageBox.Show("נא לסמן הצעה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    return;
                }
                elementaryLogic.ChangeElementaryPolicyOfferStatus(((ElementaryPolicy)dgElementaryOffer.SelectedItem).ElementaryPolicyID,((ElementaryPolicy)dgElementaryOffer.SelectedItem).PolicyNumber, ((ElementaryPolicy)dgElementaryOffer.SelectedItem).InsuranceIndustryID, false);
                client = ((ElementaryPolicy)dgElementaryOffer.SelectedItem).Client;
            }
            else if (tbItemPersonalAccidents.IsSelected)
            {
                if (dgPersonalAccidentsPolicies.SelectedItem == null)
                {
                    MessageBox.Show("נא לסמן הצעה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    return;
                }
                personalAccidentsLogic.ChangePersonalAccidentsPolicyOfferStatus(((PersonalAccidentsPolicy)dgPersonalAccidentsPolicies.SelectedItem).PersonalAccidentsPolicyID, false);
                client = ((PersonalAccidentsPolicy)dgPersonalAccidentsPolicies.SelectedItem).Client;
            }
            else if (tbItemTravel.IsSelected)
            {
                if (dgTravelPolicies.SelectedItem == null)
                {
                    MessageBox.Show("נא לסמן הצעה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    return;
                }
                travelLogic.ChangeTravelPolicyOfferStatus(((TravelPolicy)dgTravelPolicies.SelectedItem).TravelPolicyID, false);
                client = ((TravelPolicy)dgTravelPolicies.SelectedItem).Client;
            }
            else if (tbItemForeingWorkers.IsSelected)
            {
                if (dgForeingWorkersPolicies.SelectedItem == null)
                {
                    MessageBox.Show("נא לסמן הצעה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    return;
                }
                foreingWorkersLogic.ChangeForeingWorkersPolicyOfferStatus(((ForeingWorkersPolicy)dgForeingWorkersPolicies.SelectedItem).ForeingWorkersPolicyID, false);
                client = ((ForeingWorkersPolicy)dgForeingWorkersPolicies.SelectedItem).Client;
            }
            else if (tbItemLife.IsSelected)
            {
                if (dgLife.SelectedItem == null)
                {
                    MessageBox.Show("נא לסמן הצעה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    return;
                }
                lifeLogic.ChangeLifePolicyOfferStatus(((LifePolicy)dgLife.SelectedItem).LifePolicyID, false);
                client = ((LifePolicy)dgLife.SelectedItem).Client;
            }
            else if (tbItemHealth.IsSelected)
            {
                if (dgHealth.SelectedItem == null)
                {
                    MessageBox.Show("נא לסמן הצעה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    return;
                }
                healthLogic.ChangeHealthPolicyOfferStatus(((HealthPolicy)dgHealth.SelectedItem).HealthPolicyID, false);
                client = ((HealthPolicy)dgHealth.SelectedItem).Client;
            }
            else if (tbItemFinance.IsSelected)
            {
                if (dgFinance.SelectedItem == null)
                {
                    MessageBox.Show("נא לסמן הצעה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    return;
                }
                financeLogic.ChangeFinancePolicyOfferStatus(((FinancePolicy)dgFinance.SelectedItem).FinancePolicyID, false);
                client = ((FinancePolicy)dgFinance.SelectedItem).Client;
            }
            if (client.ClientTypeID != 1)
            {
                clientsLogic.UpdateClientType(client, new ClientType { ClientTypeID = 1 });
            }
            OffersBinding();
            MessageBox.Show("ההצעה נהפכה לפוליסה בהצלחה", "", MessageBoxButton.OK, MessageBoxImage.Information);
        }


        private void cbStatus_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            OffersBinding();
        }

        private void cbIndustry_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            OffersBinding();
        }

        private void btnClear_Click(object sender, RoutedEventArgs e)
        {
            ClearSearchInputs();
        }
        private void ClearSearchInputs()
        {
            cbIndustry.SelectedIndex = -1;
            cbCompany.SelectedIndex = -1;
            txtSearch.Clear();
        }

        private void cbCompany_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            OffersBinding();
        }

        private void dtxtPeriod_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            OffersBinding();
        }

        private void txtSearchClaim_TextChanged(object sender, TextChangedEventArgs e)
        {
            OffersBinding();
        }

        private void btnUpdateOffer_Click(object sender, RoutedEventArgs e)
        {
            UpdateOffer();
        }


        private void SelectOfferInDg(object offerToSelect, ListView lv)
        {
            var dgItems = lv.Items;
            foreach (var item in dgItems)
            {
                if (offerToSelect is ElementaryPolicy)
                {
                    ElementaryPolicy policy = (ElementaryPolicy)item;
                    if (policy.ElementaryPolicyID == ((ElementaryPolicy)offerToSelect).ElementaryPolicyID)
                    {
                        lv.SelectedItem = item;
                        lv.ScrollIntoView(item);
                        ListViewItem listViewItem = lv.ItemContainerGenerator.ContainerFromItem(lv.SelectedItem) as ListViewItem;
                        if (listViewItem != null)
                        {
                            listViewItem.Focus();
                        }
                        break;
                    }
                }
                else if (offerToSelect is ForeingWorkersPolicy)
                {
                    ForeingWorkersPolicy policy = (ForeingWorkersPolicy)item;
                    if (policy.ForeingWorkersPolicyID == ((ForeingWorkersPolicy)offerToSelect).ForeingWorkersPolicyID)
                    {
                        lv.SelectedItem = item;
                        lv.ScrollIntoView(item);
                        ListViewItem listViewItem = lv.ItemContainerGenerator.ContainerFromItem(lv.SelectedItem) as ListViewItem;
                        if (listViewItem != null)
                        {
                            listViewItem.Focus();
                        }
                        break;
                    }
                }
                else if (offerToSelect is FinancePolicy)
                {
                    FinancePolicy policy = (FinancePolicy)item;
                    if (policy.FinancePolicyID == ((FinancePolicy)offerToSelect).FinancePolicyID)
                    {
                        lv.SelectedItem = item;
                        lv.ScrollIntoView(item);
                        ListViewItem listViewItem = lv.ItemContainerGenerator.ContainerFromItem(lv.SelectedItem) as ListViewItem;
                        if (listViewItem != null)
                        {
                            listViewItem.Focus();
                        }
                        break;
                    }
                }
                else if (offerToSelect is LifePolicy)
                {
                    LifePolicy policy = (LifePolicy)item;
                    if (policy.LifePolicyID == ((LifePolicy)offerToSelect).LifePolicyID)
                    {
                        lv.SelectedItem = item;
                        lv.ScrollIntoView(item);
                        ListViewItem listViewItem = lv.ItemContainerGenerator.ContainerFromItem(lv.SelectedItem) as ListViewItem;
                        if (listViewItem != null)
                        {
                            listViewItem.Focus();
                        }
                        break;
                    }
                }
                else if (offerToSelect is HealthPolicy)
                {
                    HealthPolicy policy = (HealthPolicy)item;
                    if (policy.HealthPolicyID == ((HealthPolicy)offerToSelect).HealthPolicyID)
                    {
                        lv.SelectedItem = item;
                        lv.ScrollIntoView(item);
                        ListViewItem listViewItem = lv.ItemContainerGenerator.ContainerFromItem(lv.SelectedItem) as ListViewItem;
                        if (listViewItem != null)
                        {
                            listViewItem.Focus();
                        }
                        break;
                    }
                }
                else if (offerToSelect is TravelPolicy)
                {
                    TravelPolicy policy = (TravelPolicy)item;
                    if (policy.TravelPolicyID == ((TravelPolicy)offerToSelect).TravelPolicyID)
                    {
                        lv.SelectedItem = item;
                        lv.ScrollIntoView(item);
                        ListViewItem listViewItem = lv.ItemContainerGenerator.ContainerFromItem(lv.SelectedItem) as ListViewItem;
                        if (listViewItem != null)
                        {
                            listViewItem.Focus();
                        }
                        break;
                    }
                }
                else if (offerToSelect is PersonalAccidentsPolicy)
                {
                    PersonalAccidentsPolicy policy = (PersonalAccidentsPolicy)item;
                    if (policy.PersonalAccidentsPolicyID == ((PersonalAccidentsPolicy)offerToSelect).PersonalAccidentsPolicyID)
                    {
                        lv.SelectedItem = item;
                        lv.ScrollIntoView(item);
                        ListViewItem listViewItem = lv.ItemContainerGenerator.ContainerFromItem(lv.SelectedItem) as ListViewItem;
                        if (listViewItem != null)
                        {
                            listViewItem.Focus();
                        }
                        break;
                    }
                }
            }
        }


        private void UpdateOffer()
        {
            object offerToSelect = null;
            ListView lv = null;
            if (tbItemElementary.IsSelected)
            {
                if (dgElementaryOffer.SelectedItem == null)
                {
                    MessageBox.Show("נא לסמן את ההצעה שברצונך לעדכן", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    return;
                }
                offerToSelect = (ElementaryPolicy)dgElementaryOffer.SelectedItem;
                lv = dgElementaryOffer;
                frmElementary updateElementaryPolicy = new frmElementary((ElementaryPolicy)dgElementaryOffer.SelectedItem, user, false, ((ElementaryPolicy)dgElementaryOffer.SelectedItem).Client);
                updateElementaryPolicy.ShowDialog();
            }
            else if (tbItemPersonalAccidents.IsSelected)
            {
                if (dgPersonalAccidentsPolicies.SelectedItem == null)
                {
                    MessageBox.Show("נא לסמן את ההצעה שברצונך לעדכן", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    return;
                }
                offerToSelect = (PersonalAccidentsPolicy)dgPersonalAccidentsPolicies.SelectedItem;
                lv = dgPersonalAccidentsPolicies;

                frmPersonalAccidents updatePersonalAccidentsPolicy = new frmPersonalAccidents(((PersonalAccidentsPolicy)dgPersonalAccidentsPolicies.SelectedItem).Client, user, false, (PersonalAccidentsPolicy)dgPersonalAccidentsPolicies.SelectedItem);
                updatePersonalAccidentsPolicy.ShowDialog();
            }
            else if (tbItemTravel.IsSelected)
            {
                if (dgTravelPolicies.SelectedItem == null)
                {
                    MessageBox.Show("נא לסמן את ההצעה שברצונך לעדכן", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    return;
                }
                offerToSelect = (TravelPolicy)dgTravelPolicies.SelectedItem;
                lv = dgTravelPolicies;
                frmTravel updateTravelPolicy = new frmTravel(((TravelPolicy)dgTravelPolicies.SelectedItem).Client, user, false, (TravelPolicy)dgTravelPolicies.SelectedItem);
                updateTravelPolicy.ShowDialog();
            }
            else if (tbItemForeingWorkers.IsSelected)
            {
                if (dgForeingWorkersPolicies.SelectedItem == null)
                {
                    MessageBox.Show("נא לסמן את ההצעה שברצונך לעדכן", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    return;
                }
                offerToSelect = (ForeingWorkersPolicy)dgForeingWorkersPolicies.SelectedItem;
                lv = dgForeingWorkersPolicies;
                frmForeignWorkers updateForeingWorkersPolicy = new frmForeignWorkers(((ForeingWorkersPolicy)dgForeingWorkersPolicies.SelectedItem).Client, user, false, (ForeingWorkersPolicy)dgForeingWorkersPolicies.SelectedItem);
                updateForeingWorkersPolicy.ShowDialog();
            }
            else if (tbItemLife.IsSelected)
            {
                if (dgLife.SelectedItem == null)
                {
                    MessageBox.Show("נא לסמן את ההצעה שברצונך לעדכן", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    return;
                }
                offerToSelect = (LifePolicy)dgLife.SelectedItem;
                lv = dgLife;
                frmLifePolicy updateLifePolicy = new frmLifePolicy(((LifePolicy)dgLife.SelectedItem).Client, user, false, (LifePolicy)dgLife.SelectedItem);
                updateLifePolicy.ShowDialog();
            }
            else if (tbItemHealth.IsSelected)
            {
                if (dgHealth.SelectedItem == null)
                {
                    MessageBox.Show("נא לסמן את ההצעה שברצונך לעדכן", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    return;
                }
                offerToSelect = (HealthPolicy)dgHealth.SelectedItem;
                lv = dgHealth;
                frmHealthPolicy updateHealthPolicy = new frmHealthPolicy(((HealthPolicy)dgHealth.SelectedItem).Client, user, false, (HealthPolicy)dgHealth.SelectedItem);
                updateHealthPolicy.ShowDialog();
            }
            else if (tbItemFinance.IsSelected)
            {
                if (dgFinance.SelectedItem == null)
                {
                    MessageBox.Show("נא לסמן את ההצעה שברצונך לעדכן", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    return;
                }
                offerToSelect = (FinancePolicy)dgFinance.SelectedItem;
                lv = dgFinance;
                frmFinance updateFinancePolicy = new frmFinance(((FinancePolicy)dgFinance.SelectedItem).Client, user, false, (FinancePolicy)dgFinance.SelectedItem);
                updateFinancePolicy.ShowDialog();
            }
            OffersBinding();
            if (offerToSelect != null && lv != null)
            {
                SelectOfferInDg(offerToSelect, lv);
            }
        }

        private void btnSendEmail_Click(object sender, RoutedEventArgs e)
        {
            Client client = null;
            if (tbItemElementary.IsSelected)
            {
                if (dgElementaryOffer.SelectedItem == null)
                {
                    MessageBox.Show("נא לסמן הצעה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    return;
                }
                client = ((ElementaryPolicy)dgElementaryOffer.SelectedItem).Client;
            }
            else if (tbItemPersonalAccidents.IsSelected)
            {
                if (dgPersonalAccidentsPolicies.SelectedItem == null)
                {
                    MessageBox.Show("נא לסמן הצעה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    return;
                }
                client = ((PersonalAccidentsPolicy)dgPersonalAccidentsPolicies.SelectedItem).Client;
            }
            else if (tbItemTravel.IsSelected)
            {
                if (dgTravelPolicies.SelectedItem == null)
                {
                    MessageBox.Show("נא לסמן הצעה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    return;
                }
                client = ((TravelPolicy)dgTravelPolicies.SelectedItem).Client;
            }
            else if (tbItemForeingWorkers.IsSelected)
            {
                if (dgForeingWorkersPolicies.SelectedItem == null)
                {
                    MessageBox.Show("נא לסמן הצעה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    return;
                }
                client = ((ForeingWorkersPolicy)dgForeingWorkersPolicies.SelectedItem).Client;
            }
            else if (tbItemLife.IsSelected)
            {
                if (dgLife.SelectedItem == null)
                {
                    MessageBox.Show("נא לסמן הצעה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    return;
                }
                client = ((LifePolicy)dgLife.SelectedItem).Client;
            }
            else if (tbItemHealth.IsSelected)
            {
                if (dgHealth.SelectedItem == null)
                {
                    MessageBox.Show("נא לסמן הצעה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    return;
                }
                client = ((HealthPolicy)dgHealth.SelectedItem).Client;
            }
            else if (tbItemFinance.IsSelected)
            {
                if (dgFinance.SelectedItem == null)
                {
                    MessageBox.Show("נא לסמן הצעה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    return;
                }
                client = ((FinancePolicy)dgFinance.SelectedItem).Client;
            }
            else if (tbItemRenewals.IsSelected)
            {
                if (dgElementaryRenewals.SelectedItem == null)
                {
                    MessageBox.Show("נא לסמן הצעה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    return;
                }
                client = ((ElementaryPolicy)dgElementaryRenewals.SelectedItem).Client;
            }
            if (client == null)
            {
                MessageBox.Show("נא לסמן לקוח", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            if (client.Email == "")
            {
                MessageBox.Show("אין ללקוח כתובת דוא''ל במערכת", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            string[] address = new string[] { client.Email };
            try
            {
                email.SendEmailFromOutlook("", "", null, address);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void mItemExcel_Click(object sender, RoutedEventArgs e)
        {
            excelLogic.ElementaryPoliciesToExcel((List<ElementaryPolicy>)dgElementaryOffer.ItemsSource);
        }

        private void mItemPersonalAccidentsExcel_Click(object sender, RoutedEventArgs e)
        {
            excelLogic.PersonalAccidentsPoliciesToExcel((List<PersonalAccidentsPolicy>)dgPersonalAccidentsPolicies.ItemsSource);
        }

        private void mItemTravelExcel_Click(object sender, RoutedEventArgs e)
        {
            excelLogic.TravelPoliciesToExcel((List<TravelPolicy>)dgTravelPolicies.ItemsSource);
        }

        private void mItemLifeExcel_Click(object sender, RoutedEventArgs e)
        {
            excelLogic.LifePoliciesToExcel((List<LifePolicy>)dgLife.ItemsSource);
        }

        private void mItemHealthExcel_Click(object sender, RoutedEventArgs e)
        {
            excelLogic.HealthPoliciesToExcel((List<HealthPolicy>)dgHealth.ItemsSource);
        }

        private void mItemForeingWorkersExcel_Click(object sender, RoutedEventArgs e)
        {
            excelLogic.ForeingWorkersPoliciesToExcel((List<ForeingWorkersPolicy>)dgForeingWorkersPolicies.ItemsSource);
        }

        private void mItemFinanceExcel_Click(object sender, RoutedEventArgs e)
        {
            excelLogic.FinancePoliciesToExcel((List<FinancePolicy>)dgFinance.ItemsSource);
        }

        private void mItemRenewalsExcel_Click(object sender, RoutedEventArgs e)
        {
            excelLogic.ElementaryPoliciesToExcel((List<ElementaryPolicy>)dgElementaryRenewals.ItemsSource);
        }

        private void dgElementaryOffer_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            DependencyObject src = (DependencyObject)(e.OriginalSource);
            while (!(src is Control))
            {
                if (!(src is Visual))
                {
                    break;
                }
                src = VisualTreeHelper.GetParent(src);
            }

            if ((src is Control && src.GetType() == typeof(ListViewItem)) || src.GetType() == typeof(Run))
            {
                UpdateOffer();
            }
        }

        private void dgElementaryOffer_MouseDown(object sender, MouseButtonEventArgs e)
        {
            dgElementaryOffer.UnselectAll();
        }

        private void dgPersonalAccidentsPolicies_MouseDown(object sender, MouseButtonEventArgs e)
        {
            //dgPersonalAccidentsPolicies.UnselectAll();
            (sender as ListView).UnselectAll();
        }

        private void btnOpticArchive_Click(object sender, RoutedEventArgs e)
        {
            Client client = null;
            object offer = null;
            if (tbItemElementary.IsSelected)
            {
                if (dgElementaryOffer.SelectedItem == null)
                {
                    MessageBox.Show("נא לסמן הצעה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    return;
                }
                client = ((ElementaryPolicy)dgElementaryOffer.SelectedItem).Client;
                offer = dgElementaryOffer.SelectedItem;
            }
            else if (tbItemPersonalAccidents.IsSelected)
            {
                if (dgPersonalAccidentsPolicies.SelectedItem == null)
                {
                    MessageBox.Show("נא לסמן הצעה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    return;
                }
                client = ((PersonalAccidentsPolicy)dgPersonalAccidentsPolicies.SelectedItem).Client;
                offer = dgPersonalAccidentsPolicies.SelectedItem;

            }
            else if (tbItemTravel.IsSelected)
            {
                if (dgTravelPolicies.SelectedItem == null)
                {
                    MessageBox.Show("נא לסמן הצעה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    return;
                }
                client = ((TravelPolicy)dgTravelPolicies.SelectedItem).Client;
                offer = dgTravelPolicies.SelectedItem;

            }
            else if (tbItemForeingWorkers.IsSelected)
            {
                if (dgForeingWorkersPolicies.SelectedItem == null)
                {
                    MessageBox.Show("נא לסמן הצעה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    return;
                }
                client = ((ForeingWorkersPolicy)dgForeingWorkersPolicies.SelectedItem).Client;
                offer = dgForeingWorkersPolicies.SelectedItem;

            }
            else if (tbItemLife.IsSelected)
            {
                if (dgLife.SelectedItem == null)
                {
                    MessageBox.Show("נא לסמן הצעה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    return;
                }
                client = ((LifePolicy)dgLife.SelectedItem).Client;
                offer = dgLife.SelectedItem;

            }
            else if (tbItemHealth.IsSelected)
            {
                if (dgHealth.SelectedItem == null)
                {
                    MessageBox.Show("נא לסמן הצעה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    return;
                }
                client = ((HealthPolicy)dgHealth.SelectedItem).Client;
                offer = dgHealth.SelectedItem;

            }
            else if (tbItemFinance.IsSelected)
            {
                if (dgFinance.SelectedItem == null)
                {
                    MessageBox.Show("נא לסמן הצעה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    return;
                }
                client = ((FinancePolicy)dgFinance.SelectedItem).Client;
                offer = dgFinance.SelectedItem;

            }
            else if (tbItemRenewals.IsSelected)
            {
                if (dgElementaryRenewals.SelectedItem == null)
                {
                    MessageBox.Show("נא לסמן הצעה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    return;
                }
                client = ((ElementaryPolicy)dgElementaryRenewals.SelectedItem).Client;
                offer = dgElementaryRenewals.SelectedItem;

            }
            frmScan2 archiveWindow = new frmScan2(client, offer, "", user);
            archiveWindow.ShowDialog();

        }
    }
}
