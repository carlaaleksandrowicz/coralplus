﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using CoralBusinessLogics;

namespace CoralUI
{
    /// <summary>
    /// Interaction logic for frmSettings.xaml
    /// </summary>
    public partial class frmSettings : Window
    {
        User user;
        RolesLogic rolesLogic = new RolesLogic();
        public frmSettings(User user)
        {
            InitializeComponent();
            this.user = user;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (!rolesLogic.IsUserInRole(user.Usermame,"מנהל ראשי"))
            {
                this.IsEnabled = false;
            }
            string strAppDir = System.AppDomain.CurrentDomain.BaseDirectory;
            string path = strAppDir + @"Coral Files\ClientsPath.txt";
            string offerPath = strAppDir + @"Coral Files\OffersPath.txt";

            if (File.Exists(path))
            {
                txtArchPath.Text = File.ReadAllText(path);
            }
            if (File.Exists(offerPath))
            {
                txtOffersPath.Text = File.ReadAllText(offerPath);
            }
        }

        private void btnBrowse_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Forms.FolderBrowserDialog dlg = new System.Windows.Forms.FolderBrowserDialog();
            System.Windows.Forms.DialogResult result = dlg.ShowDialog();
            txtArchPath.Text = dlg.SelectedPath;
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            if (!Directory.Exists(txtArchPath.Text))
            {
                MessageBox.Show("נתיב תקיית לקוחות לא קיים במחשב");
            }
            else if (!Directory.Exists(txtOffersPath.Text))
            {
                MessageBox.Show("נתיב תקיית ההצעות לא קיים במחשב");
            }
            else
            {
                try
                {
                    string strAppDir = System.AppDomain.CurrentDomain.BaseDirectory;
                    string coralFilePath = strAppDir + @"Coral Files";
                    if (!Directory.Exists(coralFilePath))
                    {
                        Directory.CreateDirectory(coralFilePath);
                    }
                    string path = coralFilePath + @"\ClientsPath.txt";
                    if (File.Exists(path))
                    {
                        File.Delete(path);
                    }
                    File.WriteAllText(path, txtArchPath.Text);
                    string offerPath = coralFilePath + @"\OffersPath.txt";
                    if (File.Exists(offerPath))
                    {
                        File.Delete(offerPath);
                    }
                    File.WriteAllText(offerPath, txtOffersPath.Text);
                    Close();
                }
                catch (Exception)
                {
                    MessageBox.Show("לא ניתן לפתוח את התיקייה המבוקשת");
                }
            }
        }

        private void btnBrowseOffers_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Forms.FolderBrowserDialog dlg = new System.Windows.Forms.FolderBrowserDialog();
            System.Windows.Forms.DialogResult result = dlg.ShowDialog();
            txtOffersPath.Text = dlg.SelectedPath;
        }
    }
}
