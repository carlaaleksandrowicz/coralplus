﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using CoralBusinessLogics;
using System.IO;

namespace CoralUI
{
    /// <summary>
    /// Interaction logic for frmTravel.xaml
    /// </summary>
    public partial class frmTravel : Window
    {
        private Client client;
        private User user;
        string clientDirPath = null;
        private string travelPath = null;
        InsurancesLogic insuranceLogic = new InsurancesLogic();
        CountriesLogic countriesLogic = new CountriesLogic();
        AgentsLogic agentLogic = new AgentsLogic();
        TravelInsuranceLogic travelInsuranceLogic = new TravelInsuranceLogic();
        List<object> nullErrorList = null;
        InputsValidations validations = new InputsValidations();
        int policyID;
        TravelLogic travelLogic = new TravelLogic();
        private string path;
        List<TravelAdditionalInsured> insureds = new List<TravelAdditionalInsured>();
        ListViewSettings lv = new ListViewSettings();
        Brush gray;
        bool isAddition = false;
        List<TravelInsuranceDetail> insuranceDetails = new List<TravelInsuranceDetail>();
        private bool v;
        private TravelPolicy travelPolicy = null;
        private string newPath;
        List<TravelCoverage> coverages = new List<TravelCoverage>();
        CoveragesLogic coveragesLogic = new CoveragesLogic();
        List<TravelTracking> trackings = new List<TravelTracking>();
        TrackingsLogics trackingLogics = new TrackingsLogics();
        bool isOffer;
        private bool isChanges = false;
        bool confirmBeforeClosing = true;
        bool cancelClose = false;
        StandardMoneyCollection standardMoneyCollection;
        StandardMoneyCollectionLogic moneyCollectionLogic = new StandardMoneyCollectionLogic();
        private List<StandardMoneyCollectionTracking> standardMoneyCollectionTrackings;


        public frmTravel()
        {
            InitializeComponent();
        }

        public frmTravel(Client clientSelected, User user, bool isOffer)
        {
            InitializeComponent();
            client = clientSelected;
            this.user = user;
            this.isOffer = isOffer;
        }

        public frmTravel(Client clientSelected, User user, bool isAddition, TravelPolicy travelPolicy)
        {
            InitializeComponent();
            client = clientSelected;
            this.user = user;
            this.isAddition = isAddition;
            this.travelPolicy = travelPolicy;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            string strAppDir = System.AppDomain.CurrentDomain.BaseDirectory;
            string path = strAppDir + @"Coral Files\ClientsPath.txt";
            if (File.Exists(path))
            {
                clientDirPath = File.ReadAllText(path);
            }
            else
            {
                MessageBox.Show("לא נמצא תיקיית הלקוחות. נא בדוק בהגדרות ראשוניות את נתיב התיקייה");
                Close();
                return;
            }
            BrushConverter bc = new BrushConverter();
            gray = (Brush)bc.ConvertFrom("#FFB4B4B4");
            travelPath = clientDirPath + @"\" + client.ClientID; /*+ @"/נסיעות לחו''ל/";*/
            tbItemCoverage.Visibility = Visibility.Collapsed;
            cbCompanyBinding();
            cbCountriesBinding();
            if (client != null)
            {
                string clientDetails;
                if (client.CellPhone != null && client.CellPhone != "")
                {
                    clientDetails = string.Format("{0} {1}  ת.ז: {2} נייד: {3}", client.FirstName, client.LastName, client.IdNumber, client.CellPhone);
                }
                else
                {
                    clientDetails = string.Format("{0} {1}  ת.ז: {2}", client.FirstName, client.LastName, client.IdNumber);
                }
                lblClientNameAndId.Content = clientDetails;
                txtRelativeLastName.Text = client.LastName;
                lblInsured1.Content = client.FirstName + " " + client.LastName;
                //lblInsured1.Foreground = "#030f40";
            }
            if (travelPolicy == null)
            {
                txtAddition.Text = "0";
                txtAddition.IsEnabled = false;
                DateTime now = DateTime.Now;
                dpOpenDate.SelectedDate = now;
            }
            else
            {
                dpOpenDate.SelectedDate = travelPolicy.OpenDate;
                txtPolicyNumber.Text = travelPolicy.PolicyNumber;
                dpStartDate.SelectedDate = travelPolicy.StartDate;
                dpEndDate.SelectedDate = travelPolicy.EndDate;
                isOffer = travelPolicy.IsOffer;
                txtAddition.Text = travelPolicy.Addition.ToString();
                CompanySelection((int)travelPolicy.CompanyID);
                var principalAgentNumbers = cbPrincipalAgentNumber.Items;
                foreach (var item in principalAgentNumbers)
                {
                    AgentNumber principalAgentNumber = (AgentNumber)item;
                    if (principalAgentNumber.AgentNumberID == travelPolicy.PrincipalAgentNumberID)
                    {
                        cbPrincipalAgentNumber.SelectedItem = item;
                        break;
                    }
                }
                var secundaryAgentNumbers = cbSecundaryAgentNumber.Items;
                foreach (var item in secundaryAgentNumbers)
                {
                    AgentNumber secundaryAgentNumber = (AgentNumber)item;
                    if (secundaryAgentNumber.AgentNumberID == travelPolicy.SubAgentNumberID)
                    {
                        cbSecundaryAgentNumber.SelectedItem = item;
                        break;
                    }
                }
                TraverPurposeSelection();
                chbEnglishAuthorizationRequired.IsChecked = travelPolicy.IsEnglishAuthorizationRequired;
                CurrencySelection();
                PaymentMethodSelection();
                txtPaymentesNumber.Text = travelPolicy.PaymentsNumber.ToString();
                dpPaymentDate.SelectedDate = travelPolicy.PaymentDate;
                if (travelPolicy.Premium != null)
                {
                    txtPremium.Text = ConvertDecimalToString(travelPolicy.Premium);
                }
                if (travelPolicy.Country1ID != null)
                {
                    SelectCountryInCb((int)travelPolicy.Country1ID, cbCountry1);
                }
                if (travelPolicy.Country2ID != null)
                {
                    SelectCountryInCb((int)travelPolicy.Country2ID, cbCountry2);
                }
                if (travelPolicy.Country3ID != null)
                {
                    SelectCountryInCb((int)travelPolicy.Country3ID, cbCountry3);
                }
                if (travelPolicy.Country4ID != null)
                {
                    SelectCountryInCb((int)travelPolicy.Country4ID, cbCountry4);
                }
                txtOthersCountries.Text = travelPolicy.OtherCountry;
                dpFrom1.SelectedDate = travelPolicy.Country1FromDate;
                dpFrom2.SelectedDate = travelPolicy.Country2FromDate;
                dpFrom3.SelectedDate = travelPolicy.Country3FromDate;
                dpFrom4.SelectedDate = travelPolicy.Country4FromDate;
                dpFrom5.SelectedDate = travelPolicy.OtherCountryFromDate;
                dpTo1.SelectedDate = travelPolicy.Country1ToDate;
                dpTo2.SelectedDate = travelPolicy.Country2ToDate;
                dpTo3.SelectedDate = travelPolicy.Country3ToDate;
                dpTo4.SelectedDate = travelPolicy.Country4ToDate;
                dpTo5.SelectedDate = travelPolicy.OtherCountryToDate;
                txtComments.Text = travelPolicy.Comments;
                insureds = travelLogic.GetAdditionalInsuredsByPolicyId(travelPolicy.TravelPolicyID);
                foreach (var item in insureds)
                {
                    EnableInsuranceDetail(item);
                }
                dgInsuredsBinding();
                insuranceDetails = travelLogic.GetInsuranceDetailsByPolicyId(travelPolicy.TravelPolicyID);
                DisplayInsuranceDetails();
                coverages = coveragesLogic.GetAllTravelCoveragesByPolicy(travelPolicy.TravelPolicyID);
                dgCoveragesBinding();
                trackings = trackingLogics.GetAllTravelTrackingsByPolicy(travelPolicy.TravelPolicyID);
                dgTrackingsBinding();

                if (travelPolicy.Addition != 0)
                {
                    txtPolicyNumber.IsEnabled = false;
                }
                if (int.Parse(travelLogic.GetAdditionNumber(travelPolicy.PolicyNumber)) - 1 > travelPolicy.Addition)
                {
                    spGeneral.IsEnabled = false;
                    dpInsureds.IsEnabled = false;
                    spInsuranceDetails.IsEnabled = false;
                    dpCoverages.IsEnabled = false;
                    dpTrackings.IsEnabled = false;
                }
                if (isAddition)
                {
                    txtPolicyNumber.IsEnabled = false;
                    txtAddition.Text = travelLogic.GetAdditionNumber(travelPolicy.PolicyNumber);
                    DateTime now = DateTime.Now;
                    dpOpenDate.SelectedDate = now;
                    dpStartDate.SelectedDate = now;
                    cbCompany.IsEnabled = false;
                }
                else
                {
                    standardMoneyCollection = moneyCollectionLogic.GetStandardMoneyColletionByPolicy(travelPolicy);
                    if (standardMoneyCollection != null)
                    {
                        chbStandardMoneyCollection.IsChecked = true;
                        chbStandardMoneyCollection.IsEnabled = false;
                        standardMoneyCollectionTrackings = moneyCollectionLogic.GetTrakcingsByStsndardMoneyCollection(standardMoneyCollection);
                    }
                }
            }
        }

        private void DisplayInsuranceDetails()
        {
            var principalInsuranceDetails = insuranceDetails.FirstOrDefault(d => d.TravelAdditionalInsuredID == null);
            if (principalInsuranceDetails.TravelInsuranceTypeID != null)
            {
                SelectItemInCb((int)principalInsuranceDetails.TravelInsuranceTypeID, cbInsuranceTypeInsured1);
            }
            chbWinterSport1.IsChecked = principalInsuranceDetails.IsWinterSport;
            chbExtremeSport1.IsChecked = principalInsuranceDetails.IsExtremeSport;
            chbSearchAndRescue1.IsChecked = principalInsuranceDetails.IsSearchAndRescue;
            chbPregnancy1.IsChecked = principalInsuranceDetails.IsPregnancy;
            chbExistingMedicalCondition1.IsChecked = principalInsuranceDetails.IsExistingMedicalCondition;
            chbBaggage1.IsChecked = principalInsuranceDetails.IsBaggage;
            chbComputerOrTablet1.IsChecked = principalInsuranceDetails.IsComputerOrTablet;
            chbPhoneOrGPS1.IsChecked = principalInsuranceDetails.IsPhoneOrGPS;
            chbBicycle1.IsChecked = principalInsuranceDetails.IsBicycle;
            chbOtherInsuranceDetails1.IsChecked = principalInsuranceDetails.IsOther;
            txtOtherName1.Text = principalInsuranceDetails.OtherName;
            if (insuranceDetails.Count > 1)
            {
                //EnableInsuranceDetail(insuranceDetails[1].TravelAdditionalInsured);
                if (insuranceDetails[1].TravelInsuranceTypeID != null)
                {
                    SelectItemInCb((int)insuranceDetails[1].TravelInsuranceTypeID, cbInsuranceTypeInsured2);
                }
                chbWinterSport2.IsChecked = insuranceDetails[1].IsWinterSport;
                chbExtremeSport2.IsChecked = insuranceDetails[1].IsExtremeSport;
                chbSearchAndRescue2.IsChecked = insuranceDetails[1].IsSearchAndRescue;
                chbPregnancy2.IsChecked = insuranceDetails[1].IsPregnancy;
                chbExistingMedicalCondition2.IsChecked = insuranceDetails[1].IsExistingMedicalCondition;
                chbBaggage2.IsChecked = insuranceDetails[1].IsBaggage;
                chbComputerOrTablet2.IsChecked = insuranceDetails[1].IsComputerOrTablet;
                chbPhoneOrGPS2.IsChecked = insuranceDetails[1].IsPhoneOrGPS;
                chbBicycle2.IsChecked = insuranceDetails[1].IsBicycle;
                chbOtherInsuranceDetails2.IsChecked = insuranceDetails[1].IsOther;
                txtOtherName2.Text = insuranceDetails[1].OtherName;
                if (insuranceDetails.Count > 2)
                {
                    //EnableInsuranceDetail(insuranceDetails[2].TravelAdditionalInsured);
                    if (insuranceDetails[2].TravelInsuranceTypeID != null)
                    {
                        SelectItemInCb((int)insuranceDetails[2].TravelInsuranceTypeID, cbInsuranceTypeInsured3);
                    }
                    chbWinterSport3.IsChecked = insuranceDetails[2].IsWinterSport;
                    chbExtremeSport3.IsChecked = insuranceDetails[2].IsExtremeSport;
                    chbSearchAndRescue3.IsChecked = insuranceDetails[2].IsSearchAndRescue;
                    chbPregnancy3.IsChecked = insuranceDetails[2].IsPregnancy;
                    chbExistingMedicalCondition3.IsChecked = insuranceDetails[2].IsExistingMedicalCondition;
                    chbBaggage3.IsChecked = insuranceDetails[2].IsBaggage;
                    chbComputerOrTablet3.IsChecked = insuranceDetails[2].IsComputerOrTablet;
                    chbPhoneOrGPS3.IsChecked = insuranceDetails[2].IsPhoneOrGPS;
                    chbBicycle3.IsChecked = insuranceDetails[2].IsBicycle;
                    chbOtherInsuranceDetails3.IsChecked = insuranceDetails[2].IsOther;
                    txtOtherName3.Text = insuranceDetails[2].OtherName;
                    if (insuranceDetails.Count > 3)
                    {
                        //EnableInsuranceDetail(insuranceDetails[3].TravelAdditionalInsured);
                        if (insuranceDetails[3].TravelInsuranceTypeID != null)
                        {
                            SelectItemInCb((int)insuranceDetails[3].TravelInsuranceTypeID, cbInsuranceTypeInsured4);
                        }
                        chbWinterSport4.IsChecked = insuranceDetails[3].IsWinterSport;
                        chbExtremeSport4.IsChecked = insuranceDetails[3].IsExtremeSport;
                        chbSearchAndRescue4.IsChecked = insuranceDetails[3].IsSearchAndRescue;
                        chbPregnancy4.IsChecked = insuranceDetails[3].IsPregnancy;
                        chbExistingMedicalCondition4.IsChecked = insuranceDetails[3].IsExistingMedicalCondition;
                        chbBaggage4.IsChecked = insuranceDetails[3].IsBaggage;
                        chbComputerOrTablet4.IsChecked = insuranceDetails[3].IsComputerOrTablet;
                        chbPhoneOrGPS4.IsChecked = insuranceDetails[3].IsPhoneOrGPS;
                        chbBicycle4.IsChecked = insuranceDetails[3].IsBicycle;
                        chbOtherInsuranceDetails4.IsChecked = insuranceDetails[3].IsOther;
                        txtOtherName4.Text = insuranceDetails[3].OtherName;
                        if (insuranceDetails.Count > 4)
                        {
                            //EnableInsuranceDetail(insuranceDetails[4].TravelAdditionalInsured);
                            if (insuranceDetails[4].TravelInsuranceTypeID != null)
                            {
                                SelectItemInCb((int)insuranceDetails[4].TravelInsuranceTypeID, cbInsuranceTypeInsured5);
                            }
                            chbWinterSport5.IsChecked = insuranceDetails[4].IsWinterSport;
                            chbExtremeSport5.IsChecked = insuranceDetails[4].IsExtremeSport;
                            chbSearchAndRescue5.IsChecked = insuranceDetails[4].IsSearchAndRescue;
                            chbPregnancy5.IsChecked = insuranceDetails[4].IsPregnancy;
                            chbExistingMedicalCondition5.IsChecked = insuranceDetails[4].IsExistingMedicalCondition;
                            chbBaggage5.IsChecked = insuranceDetails[4].IsBaggage;
                            chbComputerOrTablet5.IsChecked = insuranceDetails[4].IsComputerOrTablet;
                            chbPhoneOrGPS5.IsChecked = insuranceDetails[4].IsPhoneOrGPS;
                            chbBicycle5.IsChecked = insuranceDetails[4].IsBicycle;
                            chbOtherInsuranceDetails5.IsChecked = insuranceDetails[4].IsOther;
                            txtOtherName5.Text = insuranceDetails[4].OtherName;
                            if (insuranceDetails.Count > 5)
                            {
                                //EnableInsuranceDetail(insuranceDetails[5].TravelAdditionalInsured);
                                if (insuranceDetails[5].TravelInsuranceTypeID != null)
                                {
                                    SelectItemInCb((int)insuranceDetails[5].TravelInsuranceTypeID, cbInsuranceTypeInsured6);
                                }
                                chbWinterSport6.IsChecked = insuranceDetails[5].IsWinterSport;
                                chbExtremeSport6.IsChecked = insuranceDetails[5].IsExtremeSport;
                                chbSearchAndRescue6.IsChecked = insuranceDetails[5].IsSearchAndRescue;
                                chbPregnancy6.IsChecked = insuranceDetails[5].IsPregnancy;
                                chbExistingMedicalCondition6.IsChecked = insuranceDetails[5].IsExistingMedicalCondition;
                                chbBaggage6.IsChecked = insuranceDetails[5].IsBaggage;
                                chbComputerOrTablet6.IsChecked = insuranceDetails[5].IsComputerOrTablet;
                                chbPhoneOrGPS6.IsChecked = insuranceDetails[5].IsPhoneOrGPS;
                                chbBicycle6.IsChecked = insuranceDetails[5].IsBicycle;
                                chbOtherInsuranceDetails6.IsChecked = insuranceDetails[5].IsOther;
                                txtOtherName6.Text = insuranceDetails[5].OtherName;
                                if (insuranceDetails.Count > 6)
                                {
                                    //EnableInsuranceDetail(insuranceDetails[6].TravelAdditionalInsured);
                                    if (insuranceDetails[6].TravelInsuranceTypeID != null)
                                    {
                                        SelectItemInCb((int)insuranceDetails[6].TravelInsuranceTypeID, cbInsuranceTypeInsured7);
                                    }
                                    chbWinterSport7.IsChecked = insuranceDetails[6].IsWinterSport;
                                    chbExtremeSport7.IsChecked = insuranceDetails[6].IsExtremeSport;
                                    chbSearchAndRescue7.IsChecked = insuranceDetails[6].IsSearchAndRescue;
                                    chbPregnancy7.IsChecked = insuranceDetails[6].IsPregnancy;
                                    chbExistingMedicalCondition7.IsChecked = insuranceDetails[6].IsExistingMedicalCondition;
                                    chbBaggage7.IsChecked = insuranceDetails[6].IsBaggage;
                                    chbComputerOrTablet7.IsChecked = insuranceDetails[6].IsComputerOrTablet;
                                    chbPhoneOrGPS7.IsChecked = insuranceDetails[6].IsPhoneOrGPS;
                                    chbBicycle7.IsChecked = insuranceDetails[6].IsBicycle;
                                    chbOtherInsuranceDetails7.IsChecked = insuranceDetails[6].IsOther;
                                    txtOtherName7.Text = insuranceDetails[6].OtherName;
                                    if (insuranceDetails.Count > 7)
                                    {
                                        //EnableInsuranceDetail(insuranceDetails[7].TravelAdditionalInsured);
                                        if (insuranceDetails[7].TravelInsuranceTypeID != null)
                                        {
                                            SelectItemInCb((int)insuranceDetails[7].TravelInsuranceTypeID, cbInsuranceTypeInsured8);
                                        }
                                        chbWinterSport8.IsChecked = insuranceDetails[7].IsWinterSport;
                                        chbExtremeSport8.IsChecked = insuranceDetails[7].IsExtremeSport;
                                        chbSearchAndRescue8.IsChecked = insuranceDetails[7].IsSearchAndRescue;
                                        chbPregnancy8.IsChecked = insuranceDetails[7].IsPregnancy;
                                        chbExistingMedicalCondition8.IsChecked = insuranceDetails[7].IsExistingMedicalCondition;
                                        chbBaggage8.IsChecked = insuranceDetails[7].IsBaggage;
                                        chbComputerOrTablet8.IsChecked = insuranceDetails[7].IsComputerOrTablet;
                                        chbPhoneOrGPS8.IsChecked = insuranceDetails[7].IsPhoneOrGPS;
                                        chbBicycle8.IsChecked = insuranceDetails[7].IsBicycle;
                                        chbOtherInsuranceDetails8.IsChecked = insuranceDetails[7].IsOther;
                                        txtOtherName8.Text = insuranceDetails[7].OtherName;
                                        if (insuranceDetails.Count > 8)
                                        {
                                            //EnableInsuranceDetail(insuranceDetails[8].TravelAdditionalInsured);
                                            if (insuranceDetails[8].TravelInsuranceTypeID != null)
                                            {
                                                SelectItemInCb((int)insuranceDetails[8].TravelInsuranceTypeID, cbInsuranceTypeInsured9);
                                            }
                                            chbWinterSport9.IsChecked = insuranceDetails[8].IsWinterSport;
                                            chbExtremeSport9.IsChecked = insuranceDetails[8].IsExtremeSport;
                                            chbSearchAndRescue9.IsChecked = insuranceDetails[8].IsSearchAndRescue;
                                            chbPregnancy9.IsChecked = insuranceDetails[8].IsPregnancy;
                                            chbExistingMedicalCondition9.IsChecked = insuranceDetails[8].IsExistingMedicalCondition;
                                            chbBaggage9.IsChecked = insuranceDetails[8].IsBaggage;
                                            chbComputerOrTablet9.IsChecked = insuranceDetails[8].IsComputerOrTablet;
                                            chbPhoneOrGPS9.IsChecked = insuranceDetails[8].IsPhoneOrGPS;
                                            chbBicycle9.IsChecked = insuranceDetails[8].IsBicycle;
                                            chbOtherInsuranceDetails9.IsChecked = insuranceDetails[8].IsOther;
                                            txtOtherName9.Text = insuranceDetails[8].OtherName;
                                            if (insuranceDetails.Count > 9)
                                            {
                                                //EnableInsuranceDetail(insuranceDetails[9].TravelAdditionalInsured);
                                                if (insuranceDetails[9].TravelInsuranceTypeID != null)
                                                {
                                                    SelectItemInCb((int)insuranceDetails[9].TravelInsuranceTypeID, cbInsuranceTypeInsured10);
                                                }
                                                chbWinterSport10.IsChecked = insuranceDetails[9].IsWinterSport;
                                                chbExtremeSport10.IsChecked = insuranceDetails[9].IsExtremeSport;
                                                chbSearchAndRescue10.IsChecked = insuranceDetails[9].IsSearchAndRescue;
                                                chbPregnancy10.IsChecked = insuranceDetails[9].IsPregnancy;
                                                chbExistingMedicalCondition10.IsChecked = insuranceDetails[9].IsExistingMedicalCondition;
                                                chbBaggage10.IsChecked = insuranceDetails[9].IsBaggage;
                                                chbComputerOrTablet10.IsChecked = insuranceDetails[9].IsComputerOrTablet;
                                                chbPhoneOrGPS10.IsChecked = insuranceDetails[9].IsPhoneOrGPS;
                                                chbBicycle10.IsChecked = insuranceDetails[9].IsBicycle;
                                                chbOtherInsuranceDetails10.IsChecked = insuranceDetails[9].IsOther;
                                                txtOtherName10.Text = insuranceDetails[9].OtherName;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        private string ConvertDecimalToString(decimal? number)
        {
            if (number == null)
            {
                return null;
            }
            return ((decimal)number).ToString("G29");
        }

        private void PaymentMethodSelection()
        {
            var paymentMethods = cbPaymentMode.Items;
            foreach (var item in paymentMethods)
            {
                if (travelPolicy.PaymentMethod == ((ComboBoxItem)item).Content.ToString())
                {
                    cbPaymentMode.SelectedItem = item;
                    break;
                }
            }
        }

        private void CurrencySelection()
        {
            var currencies = cbCurrency.Items;
            foreach (var item in currencies)
            {
                if (travelPolicy.Currency == ((ComboBoxItem)item).Content.ToString())
                {
                    cbCurrency.SelectedItem = item;
                    break;
                }
            }
        }

        private void TraverPurposeSelection()
        {
            var purposes = cbTravelPurpose.Items;
            foreach (var item in purposes)
            {
                if (travelPolicy.TravelPurpose == ((ComboBoxItem)item).Content.ToString())
                {
                    cbTravelPurpose.SelectedItem = item;
                    break;
                }
            }
        }

        private void cbCountriesBinding()
        {
            int? country1Id = null;
            if (cbCountry1.SelectedItem != null)
            {
                country1Id = ((Country)cbCountry1.SelectedItem).CountryID;
            }
            int? country2Id = null;
            if (cbCountry2.SelectedItem != null)
            {
                country2Id = ((Country)cbCountry2.SelectedItem).CountryID;
            }
            int? country3Id = null;
            if (cbCountry3.SelectedItem != null)
            {
                country3Id = ((Country)cbCountry3.SelectedItem).CountryID;
            }
            int? country4Id = null;
            if (cbCountry4.SelectedItem != null)
            {
                country4Id = ((Country)cbCountry4.SelectedItem).CountryID;
            }
            var countries = countriesLogic.GetActiveCountries();
            cbCountry1.ItemsSource = countries;
            cbCountry1.DisplayMemberPath = "CountryName";
            cbCountry2.ItemsSource = countries;
            cbCountry2.DisplayMemberPath = "CountryName";
            cbCountry3.ItemsSource = countries;
            cbCountry3.DisplayMemberPath = "CountryName";
            cbCountry4.ItemsSource = countries;
            cbCountry4.DisplayMemberPath = "CountryName";
            if (country1Id != null)
            {
                SelectCountryInCb((int)country1Id, cbCountry1);
            }
            if (country2Id != null)
            {
                SelectCountryInCb((int)country2Id, cbCountry2);
            }
            if (country3Id != null)
            {
                SelectCountryInCb((int)country3Id, cbCountry3);
            }
            if (country4Id != null)
            {
                SelectCountryInCb((int)country4Id, cbCountry4);
            }
        }

        private void cbCompanyBinding()
        {
            cbCompany.ItemsSource = insuranceLogic.GetActiveCompaniesByInsurance("נסיעות לחו''ל");
            cbCompany.DisplayMemberPath = "Company.CompanyName";
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            if (nullErrorList != null && nullErrorList.Count > 0)
            {
                foreach (var item in nullErrorList)
                {
                    if (item is TextBox)
                    {
                        ((TextBox)item).ClearValue(BorderBrushProperty);
                    }
                    else if (item is ComboBox)
                    {
                        ((ComboBox)item).ClearValue(BorderBrushProperty);
                    }
                    else if (item is DatePicker)
                    {
                        ((DatePicker)item).ClearValue(BorderBrushProperty);
                    }
                }
            }
            if (!validations.IsDigitsOnly(txtPolicyNumber.Text))
            {
                cancelClose = true;
                MessageBox.Show("מס' פוליסה יכול להכיל מספרים בלבד", "", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            //בדיקת שדות חובה
            nullErrorList = validations.InputNullValidation(new object[] { txtPolicyNumber, txtAddition, dpStartDate, dpEndDate, cbCompany, txtPremium });
            if (nullErrorList.Count > 0)
            {
                cancelClose = true;
                //tbItemGeneral.Focus();
                MessageBox.Show("נא למלאות שדות חובה", "", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            if (dpEndDate.SelectedDate <= dpStartDate.SelectedDate)
            {
                cancelClose = true;
                tbItemGeneral.Focus();
                MessageBox.Show("תאריך סיום חייב להיות גדול מתאריך תחילה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }

            int addition = Convert.ToInt32(txtAddition.Text);
            int? paymentNumbers = null;
            if (txtPaymentesNumber.Text != "")
            {
                paymentNumbers = int.Parse(txtPaymentesNumber.Text);
            }
            decimal? premium = null;
            if (txtPremium.Text != "")
            {
                premium = decimal.Parse(txtPremium.Text);
            }
            string paymentMode = "";
            if (cbPaymentMode.SelectedItem != null)
            {
                paymentMode = ((ComboBoxItem)cbPaymentMode.SelectedItem).Content.ToString();
            }
            int? principalAgentNumberID = null;
            if ((AgentNumber)cbPrincipalAgentNumber.SelectedItem != null)
            {
                principalAgentNumberID = ((AgentNumber)cbPrincipalAgentNumber.SelectedItem).AgentNumberID;
            }
            int? subAgentNumberID = null;
            if ((AgentNumber)cbSecundaryAgentNumber.SelectedItem != null)
            {
                subAgentNumberID = ((AgentNumber)cbSecundaryAgentNumber.SelectedItem).AgentNumberID;
            }
            string travelPurpose = "";
            if (cbTravelPurpose.SelectedItem != null)
            {
                travelPurpose = ((ComboBoxItem)cbTravelPurpose.SelectedItem).Content.ToString();
            }
            int? countryId1 = null;
            if (cbCountry1.SelectedItem != null)
            {
                countryId1 = ((Country)cbCountry1.SelectedItem).CountryID;
            }
            int? countryId2 = null;
            if (cbCountry2.SelectedItem != null)
            {
                countryId2 = ((Country)cbCountry2.SelectedItem).CountryID;
            }
            int? countryId3 = null;
            if (cbCountry3.SelectedItem != null)
            {
                countryId3 = ((Country)cbCountry3.SelectedItem).CountryID;
            }
            int? countryId4 = null;
            if (cbCountry4.SelectedItem != null)
            {
                countryId4 = ((Country)cbCountry4.SelectedItem).CountryID;
            }
            int? insuranceTypeId1 = null;
            if (cbInsuranceTypeInsured1.SelectedItem != null)
            {
                insuranceTypeId1 = ((TravelInsuranceType)cbInsuranceTypeInsured1.SelectedItem).TravelInsuranceTypeID;
            }
            int? insuranceTypeId2 = null;
            if (cbInsuranceTypeInsured2.SelectedItem != null)
            {
                insuranceTypeId2 = ((TravelInsuranceType)cbInsuranceTypeInsured2.SelectedItem).TravelInsuranceTypeID;
            }
            int? insuranceTypeId3 = null;
            if (cbInsuranceTypeInsured3.SelectedItem != null)
            {
                insuranceTypeId3 = ((TravelInsuranceType)cbInsuranceTypeInsured3.SelectedItem).TravelInsuranceTypeID;
            }
            int? insuranceTypeId4 = null;
            if (cbInsuranceTypeInsured4.SelectedItem != null)
            {
                insuranceTypeId4 = ((TravelInsuranceType)cbInsuranceTypeInsured4.SelectedItem).TravelInsuranceTypeID;
            }
            int? insuranceTypeId5 = null;
            if (cbInsuranceTypeInsured5.SelectedItem != null)
            {
                insuranceTypeId5 = ((TravelInsuranceType)cbInsuranceTypeInsured5.SelectedItem).TravelInsuranceTypeID;
            }
            int? insuranceTypeId6 = null;
            if (cbInsuranceTypeInsured6.SelectedItem != null)
            {
                insuranceTypeId6 = ((TravelInsuranceType)cbInsuranceTypeInsured6.SelectedItem).TravelInsuranceTypeID;
            }
            int? insuranceTypeId7 = null;
            if (cbInsuranceTypeInsured7.SelectedItem != null)
            {
                insuranceTypeId7 = ((TravelInsuranceType)cbInsuranceTypeInsured7.SelectedItem).TravelInsuranceTypeID;
            }
            int? insuranceTypeId8 = null;
            if (cbInsuranceTypeInsured8.SelectedItem != null)
            {
                insuranceTypeId8 = ((TravelInsuranceType)cbInsuranceTypeInsured8.SelectedItem).TravelInsuranceTypeID;
            }
            int? insuranceTypeId9 = null;
            if (cbInsuranceTypeInsured9.SelectedItem != null)
            {
                insuranceTypeId9 = ((TravelInsuranceType)cbInsuranceTypeInsured9.SelectedItem).TravelInsuranceTypeID;
            }
            int? insuranceTypeId10 = null;
            if (cbInsuranceTypeInsured10.SelectedItem != null)
            {
                insuranceTypeId10 = ((TravelInsuranceType)cbInsuranceTypeInsured10.SelectedItem).TravelInsuranceTypeID;
            }
            try
            {
                if (travelPolicy == null || isAddition == true)
                {
                    policyID = travelLogic.InsertTravelPolicy(client, isOffer, (DateTime)dpOpenDate.SelectedDate, txtPolicyNumber.Text, addition, principalAgentNumberID, subAgentNumberID, dpStartDate.SelectedDate, dpEndDate.SelectedDate, ((InsuranceCompany)cbCompany.SelectedItem).CompanyID, travelPurpose, chbEnglishAuthorizationRequired.IsChecked, ((ComboBoxItem)cbCurrency.SelectedItem).Content.ToString(), paymentMode, paymentNumbers, dpPaymentDate.SelectedDate, (decimal)premium, countryId1, countryId2, countryId3, countryId4, txtOthersCountries.Text, dpFrom1.SelectedDate, dpFrom2.SelectedDate, dpFrom3.SelectedDate, dpFrom4.SelectedDate, dpFrom5.SelectedDate, dpTo1.SelectedDate, dpTo2.SelectedDate, dpTo3.SelectedDate, dpTo4.SelectedDate, dpTo5.SelectedDate, txtComments.Text);
                    if (!isOffer)
                    {
                        path = travelPath + @"\נסיעות לחו''ל\נסיעות לחו''ל " + txtPolicyNumber.Text;// +" " + ((LifeIndustry)cbLifeIndustry.SelectedItem).LifeIndustryName;

                    }
                    else
                    {
                        path = travelPath + @"\הצעות\נסיעות לחו''ל\" + txtPolicyNumber.Text;

                    }
                    TravelInsuranceDetail Insured1Details = new TravelInsuranceDetail()
                    {
                        IsBaggage = chbBaggage1.IsChecked,
                        IsBicycle = chbBicycle1.IsChecked,
                        IsComputerOrTablet = chbComputerOrTablet1.IsChecked,
                        IsExistingMedicalCondition = chbExistingMedicalCondition1.IsChecked,
                        IsExtremeSport = chbExtremeSport1.IsChecked,
                        IsOther = chbOtherInsuranceDetails1.IsChecked,
                        IsPhoneOrGPS = chbPhoneOrGPS1.IsChecked,
                        IsPregnancy = chbPregnancy1.IsChecked,
                        IsSearchAndRescue = chbSearchAndRescue1.IsChecked,
                        IsWinterSport = chbWinterSport1.IsChecked,
                        OtherName = txtOtherName1.Text,
                        TravelInsuranceTypeID = insuranceTypeId1,
                        TravelPolicyID = policyID
                    };
                    insuranceDetails.Add(Insured1Details);
                    SaveUnsavedInsured();

                    if (insureds.Count > 0)
                    {
                        travelLogic.InsertAdditionalInsureds(policyID, insureds, isAddition);

                        if (spInsuranceTypeInsured2.IsEnabled)
                        {
                            int? additionalId = travelLogic.GetAdditionalInsuredByName(policyID, lblInsured2.Content.ToString());
                            TravelInsuranceDetail Insured2Details = new TravelInsuranceDetail()
                            {
                                IsBaggage = chbBaggage2.IsChecked,
                                IsBicycle = chbBicycle2.IsChecked,
                                IsComputerOrTablet = chbComputerOrTablet2.IsChecked,
                                IsExistingMedicalCondition = chbExistingMedicalCondition2.IsChecked,
                                IsExtremeSport = chbExtremeSport2.IsChecked,
                                IsOther = chbOtherInsuranceDetails2.IsChecked,
                                IsPhoneOrGPS = chbPhoneOrGPS2.IsChecked,
                                IsPregnancy = chbPregnancy2.IsChecked,
                                IsSearchAndRescue = chbSearchAndRescue2.IsChecked,
                                IsWinterSport = chbWinterSport2.IsChecked,
                                OtherName = txtOtherName2.Text,
                                TravelInsuranceTypeID = insuranceTypeId2,
                                TravelPolicyID = policyID,
                                TravelAdditionalInsuredID = additionalId
                            };
                            insuranceDetails.Add(Insured2Details);
                        }
                        if (spInsuranceTypeInsured3.IsEnabled)
                        {
                            int? additionalId = travelLogic.GetAdditionalInsuredByName(policyID, lblInsured3.Content.ToString());
                            TravelInsuranceDetail Insured3Details = new TravelInsuranceDetail()
                            {
                                IsBaggage = chbBaggage3.IsChecked,
                                IsBicycle = chbBicycle3.IsChecked,
                                IsComputerOrTablet = chbComputerOrTablet3.IsChecked,
                                IsExistingMedicalCondition = chbExistingMedicalCondition3.IsChecked,
                                IsExtremeSport = chbExtremeSport3.IsChecked,
                                IsOther = chbOtherInsuranceDetails3.IsChecked,
                                IsPhoneOrGPS = chbPhoneOrGPS3.IsChecked,
                                IsPregnancy = chbPregnancy3.IsChecked,
                                IsSearchAndRescue = chbSearchAndRescue3.IsChecked,
                                IsWinterSport = chbWinterSport3.IsChecked,
                                OtherName = txtOtherName3.Text,
                                TravelInsuranceTypeID = insuranceTypeId3,
                                TravelPolicyID = policyID,
                                TravelAdditionalInsuredID = additionalId
                            };
                            insuranceDetails.Add(Insured3Details);
                        }
                        if (spInsuranceTypeInsured4.IsEnabled)
                        {
                            int? additionalId = travelLogic.GetAdditionalInsuredByName(policyID, lblInsured4.Content.ToString());
                            TravelInsuranceDetail Insured4Details = new TravelInsuranceDetail()
                            {
                                IsBaggage = chbBaggage4.IsChecked,
                                IsBicycle = chbBicycle4.IsChecked,
                                IsComputerOrTablet = chbComputerOrTablet4.IsChecked,
                                IsExistingMedicalCondition = chbExistingMedicalCondition4.IsChecked,
                                IsExtremeSport = chbExtremeSport4.IsChecked,
                                IsOther = chbOtherInsuranceDetails4.IsChecked,
                                IsPhoneOrGPS = chbPhoneOrGPS4.IsChecked,
                                IsPregnancy = chbPregnancy4.IsChecked,
                                IsSearchAndRescue = chbSearchAndRescue4.IsChecked,
                                IsWinterSport = chbWinterSport4.IsChecked,
                                OtherName = txtOtherName4.Text,
                                TravelInsuranceTypeID = insuranceTypeId4,
                                TravelPolicyID = policyID,
                                TravelAdditionalInsuredID = additionalId
                            };
                            insuranceDetails.Add(Insured4Details);
                        }
                        if (spInsuranceTypeInsured5.IsEnabled)
                        {
                            int? additionalId = travelLogic.GetAdditionalInsuredByName(policyID, lblInsured5.Content.ToString());
                            TravelInsuranceDetail Insured5Details = new TravelInsuranceDetail()
                            {
                                IsBaggage = chbBaggage5.IsChecked,
                                IsBicycle = chbBicycle5.IsChecked,
                                IsComputerOrTablet = chbComputerOrTablet5.IsChecked,
                                IsExistingMedicalCondition = chbExistingMedicalCondition5.IsChecked,
                                IsExtremeSport = chbExtremeSport5.IsChecked,
                                IsOther = chbOtherInsuranceDetails5.IsChecked,
                                IsPhoneOrGPS = chbPhoneOrGPS5.IsChecked,
                                IsPregnancy = chbPregnancy5.IsChecked,
                                IsSearchAndRescue = chbSearchAndRescue5.IsChecked,
                                IsWinterSport = chbWinterSport5.IsChecked,
                                OtherName = txtOtherName5.Text,
                                TravelInsuranceTypeID = insuranceTypeId5,
                                TravelPolicyID = policyID,
                                TravelAdditionalInsuredID = additionalId
                            };
                            insuranceDetails.Add(Insured5Details);
                        }
                        if (spInsuranceTypeInsured6.IsEnabled)
                        {
                            int? additionalId = travelLogic.GetAdditionalInsuredByName(policyID, lblInsured6.Content.ToString());
                            TravelInsuranceDetail Insured6Details = new TravelInsuranceDetail()
                            {
                                IsBaggage = chbBaggage6.IsChecked,
                                IsBicycle = chbBicycle6.IsChecked,
                                IsComputerOrTablet = chbComputerOrTablet6.IsChecked,
                                IsExistingMedicalCondition = chbExistingMedicalCondition6.IsChecked,
                                IsExtremeSport = chbExtremeSport6.IsChecked,
                                IsOther = chbOtherInsuranceDetails6.IsChecked,
                                IsPhoneOrGPS = chbPhoneOrGPS6.IsChecked,
                                IsPregnancy = chbPregnancy6.IsChecked,
                                IsSearchAndRescue = chbSearchAndRescue6.IsChecked,
                                IsWinterSport = chbWinterSport6.IsChecked,
                                OtherName = txtOtherName6.Text,
                                TravelInsuranceTypeID = insuranceTypeId6,
                                TravelPolicyID = policyID,
                                TravelAdditionalInsuredID = additionalId
                            };
                            insuranceDetails.Add(Insured6Details);
                        }
                        if (spInsuranceTypeInsured7.IsEnabled)
                        {
                            int? additionalId = travelLogic.GetAdditionalInsuredByName(policyID, lblInsured7.Content.ToString());
                            TravelInsuranceDetail Insured7Details = new TravelInsuranceDetail()
                            {
                                IsBaggage = chbBaggage7.IsChecked,
                                IsBicycle = chbBicycle7.IsChecked,
                                IsComputerOrTablet = chbComputerOrTablet7.IsChecked,
                                IsExistingMedicalCondition = chbExistingMedicalCondition7.IsChecked,
                                IsExtremeSport = chbExtremeSport7.IsChecked,
                                IsOther = chbOtherInsuranceDetails7.IsChecked,
                                IsPhoneOrGPS = chbPhoneOrGPS7.IsChecked,
                                IsPregnancy = chbPregnancy7.IsChecked,
                                IsSearchAndRescue = chbSearchAndRescue7.IsChecked,
                                IsWinterSport = chbWinterSport7.IsChecked,
                                OtherName = txtOtherName7.Text,
                                TravelInsuranceTypeID = insuranceTypeId7,
                                TravelPolicyID = policyID,
                                TravelAdditionalInsuredID = additionalId
                            };
                            insuranceDetails.Add(Insured7Details);
                        }
                        if (spInsuranceTypeInsured8.IsEnabled)
                        {
                            int? additionalId = travelLogic.GetAdditionalInsuredByName(policyID, lblInsured8.Content.ToString());
                            TravelInsuranceDetail Insured8Details = new TravelInsuranceDetail()
                            {
                                IsBaggage = chbBaggage8.IsChecked,
                                IsBicycle = chbBicycle8.IsChecked,
                                IsComputerOrTablet = chbComputerOrTablet8.IsChecked,
                                IsExistingMedicalCondition = chbExistingMedicalCondition8.IsChecked,
                                IsExtremeSport = chbExtremeSport8.IsChecked,
                                IsOther = chbOtherInsuranceDetails8.IsChecked,
                                IsPhoneOrGPS = chbPhoneOrGPS8.IsChecked,
                                IsPregnancy = chbPregnancy8.IsChecked,
                                IsSearchAndRescue = chbSearchAndRescue8.IsChecked,
                                IsWinterSport = chbWinterSport8.IsChecked,
                                OtherName = txtOtherName8.Text,
                                TravelInsuranceTypeID = insuranceTypeId8,
                                TravelPolicyID = policyID,
                                TravelAdditionalInsuredID = additionalId
                            };
                            insuranceDetails.Add(Insured8Details);
                        }
                        if (spInsuranceTypeInsured9.IsEnabled)
                        {
                            int? additionalId = travelLogic.GetAdditionalInsuredByName(policyID, lblInsured9.Content.ToString());
                            TravelInsuranceDetail Insured9Details = new TravelInsuranceDetail()
                            {
                                IsBaggage = chbBaggage9.IsChecked,
                                IsBicycle = chbBicycle9.IsChecked,
                                IsComputerOrTablet = chbComputerOrTablet9.IsChecked,
                                IsExistingMedicalCondition = chbExistingMedicalCondition9.IsChecked,
                                IsExtremeSport = chbExtremeSport9.IsChecked,
                                IsOther = chbOtherInsuranceDetails9.IsChecked,
                                IsPhoneOrGPS = chbPhoneOrGPS9.IsChecked,
                                IsPregnancy = chbPregnancy9.IsChecked,
                                IsSearchAndRescue = chbSearchAndRescue9.IsChecked,
                                IsWinterSport = chbWinterSport9.IsChecked,
                                OtherName = txtOtherName9.Text,
                                TravelInsuranceTypeID = insuranceTypeId9,
                                TravelPolicyID = policyID,
                                TravelAdditionalInsuredID = additionalId
                            };
                            insuranceDetails.Add(Insured9Details);
                        }
                        if (spInsuranceTypeInsured10.IsEnabled)
                        {
                            int? additionalId = travelLogic.GetAdditionalInsuredByName(policyID, lblInsured10.Content.ToString());
                            TravelInsuranceDetail Insured10Details = new TravelInsuranceDetail()
                            {
                                IsBaggage = chbBaggage10.IsChecked,
                                IsBicycle = chbBicycle10.IsChecked,
                                IsComputerOrTablet = chbComputerOrTablet10.IsChecked,
                                IsExistingMedicalCondition = chbExistingMedicalCondition10.IsChecked,
                                IsExtremeSport = chbExtremeSport10.IsChecked,
                                IsOther = chbOtherInsuranceDetails10.IsChecked,
                                IsPhoneOrGPS = chbPhoneOrGPS10.IsChecked,
                                IsPregnancy = chbPregnancy10.IsChecked,
                                IsSearchAndRescue = chbSearchAndRescue10.IsChecked,
                                IsWinterSport = chbWinterSport10.IsChecked,
                                OtherName = txtOtherName10.Text,
                                TravelInsuranceTypeID = insuranceTypeId10,
                                TravelPolicyID = policyID,
                                TravelAdditionalInsuredID = additionalId
                            };
                            insuranceDetails.Add(Insured10Details);
                        }
                    }
                    if (coverages.Count > 0)
                    {
                        coveragesLogic.InsertCoveragesToTravelPolicy(policyID, coverages, isAddition);
                    }
                    if (trackings.Count > 0)
                    {
                        trackingLogics.InsertTravelTrackings(policyID, trackings, isAddition);
                    }
                    if (chbStandardMoneyCollection.IsChecked == true)
                    {
                        int standardMoneyCollectionId = moneyCollectionLogic.InsertStandardMoneyCollection(standardMoneyCollection, new Insurance() { InsuranceName = "נסיעות לחו''ל" }, policyID);
                        if (standardMoneyCollectionTrackings != null)
                        {
                            moneyCollectionLogic.InsertTrackingsToStandardMoneyCollection(standardMoneyCollectionId, standardMoneyCollectionTrackings);
                        }
                    }
                }

                else
                {
                    travelLogic.UpdateTravelPolicy(travelPolicy.TravelPolicyID, txtPolicyNumber.Text, addition, principalAgentNumberID, subAgentNumberID, dpStartDate.SelectedDate, dpEndDate.SelectedDate, ((InsuranceCompany)cbCompany.SelectedItem).CompanyID, travelPurpose, chbEnglishAuthorizationRequired.IsChecked, ((ComboBoxItem)cbCurrency.SelectedItem).Content.ToString(), paymentMode, paymentNumbers, dpPaymentDate.SelectedDate, (decimal)premium, countryId1, countryId2, countryId3, countryId4, txtOthersCountries.Text, dpFrom1.SelectedDate, dpFrom2.SelectedDate, dpFrom3.SelectedDate, dpFrom4.SelectedDate, dpFrom5.SelectedDate, dpTo1.SelectedDate, dpTo2.SelectedDate, dpTo3.SelectedDate, dpTo4.SelectedDate, dpTo5.SelectedDate, txtComments.Text);
                    if (!isOffer)
                    {
                        string[] dirs = Directory.GetDirectories(travelPath + @"\נסיעות לחו''ל", "*" + travelPolicy.PolicyNumber + "*");
                        if (dirs.Count() > 0)
                        {
                            path = dirs[0];
                        }
                        newPath = travelPath + @"\נסיעות לחו''ל\נסיעות לחו''ל " + txtPolicyNumber.Text;
                    }
                    else
                    {
                        string[] dirs = Directory.GetDirectories(travelPath + @"\הצעות\נסיעות לחו''ל", "*" + travelPolicy.PolicyNumber + "*");
                        if (dirs.Count() > 0)
                        {
                            path = dirs[0];
                        }
                        newPath = travelPath + @"\הצעות\נסיעות לחו''ל\" + txtPolicyNumber.Text;
                    }
                    SaveUnsavedInsured();

                    travelLogic.InsertAdditionalInsureds(travelPolicy.TravelPolicyID, insureds, isAddition);
                    var principalInsuranceDetails = insuranceDetails.FirstOrDefault(d => d.TravelAdditionalInsuredID == null);
                    principalInsuranceDetails.IsBaggage = chbBaggage1.IsChecked;
                    principalInsuranceDetails.IsBicycle = chbBicycle1.IsChecked;
                    principalInsuranceDetails.IsComputerOrTablet = chbComputerOrTablet1.IsChecked;
                    principalInsuranceDetails.IsExistingMedicalCondition = chbExistingMedicalCondition1.IsChecked;
                    principalInsuranceDetails.IsExtremeSport = chbExtremeSport1.IsChecked;
                    principalInsuranceDetails.IsOther = chbOtherInsuranceDetails1.IsChecked;
                    principalInsuranceDetails.IsPhoneOrGPS = chbPhoneOrGPS1.IsChecked;
                    principalInsuranceDetails.IsPregnancy = chbPregnancy1.IsChecked;
                    principalInsuranceDetails.IsSearchAndRescue = chbSearchAndRescue1.IsChecked;
                    principalInsuranceDetails.IsWinterSport = chbWinterSport1.IsChecked;
                    principalInsuranceDetails.OtherName = txtOtherName1.Text;
                    principalInsuranceDetails.TravelInsuranceTypeID = insuranceTypeId1;
                    if (spInsuranceTypeInsured2.IsEnabled)
                    {
                        int? additionalId = travelLogic.GetAdditionalInsuredByName(travelPolicy.TravelPolicyID, lblInsured2.Content.ToString());
                        var insuranceDetail = insuranceDetails.FirstOrDefault(d => d.TravelAdditionalInsuredID == additionalId);
                        if (insuranceDetail != null)
                        {
                            insuranceDetail.IsBaggage = chbBaggage2.IsChecked;
                            insuranceDetail.IsBicycle = chbBicycle2.IsChecked;
                            insuranceDetail.IsComputerOrTablet = chbComputerOrTablet2.IsChecked;
                            insuranceDetail.IsExistingMedicalCondition = chbExistingMedicalCondition2.IsChecked;
                            insuranceDetail.IsExtremeSport = chbExtremeSport2.IsChecked;
                            insuranceDetail.IsOther = chbOtherInsuranceDetails2.IsChecked;
                            insuranceDetail.IsPhoneOrGPS = chbPhoneOrGPS2.IsChecked;
                            insuranceDetail.IsPregnancy = chbPregnancy2.IsChecked;
                            insuranceDetail.IsSearchAndRescue = chbSearchAndRescue2.IsChecked;
                            insuranceDetail.IsWinterSport = chbWinterSport2.IsChecked;
                            insuranceDetail.OtherName = txtOtherName2.Text;
                            insuranceDetail.TravelInsuranceTypeID = insuranceTypeId2;
                        }
                        else
                        {
                            TravelInsuranceDetail Insured2Details = new TravelInsuranceDetail()
                            {
                                IsBaggage = chbBaggage2.IsChecked,
                                IsBicycle = chbBicycle2.IsChecked,
                                IsComputerOrTablet = chbComputerOrTablet2.IsChecked,
                                IsExistingMedicalCondition = chbExistingMedicalCondition2.IsChecked,
                                IsExtremeSport = chbExtremeSport2.IsChecked,
                                IsOther = chbOtherInsuranceDetails2.IsChecked,
                                IsPhoneOrGPS = chbPhoneOrGPS2.IsChecked,
                                IsPregnancy = chbPregnancy2.IsChecked,
                                IsSearchAndRescue = chbSearchAndRescue2.IsChecked,
                                IsWinterSport = chbWinterSport2.IsChecked,
                                OtherName = txtOtherName2.Text,
                                TravelInsuranceTypeID = insuranceTypeId2,
                                TravelPolicyID = travelPolicy.TravelPolicyID,
                                TravelAdditionalInsuredID = additionalId
                            };
                            insuranceDetails.Add(Insured2Details);
                        }
                    }
                    if (spInsuranceTypeInsured3.IsEnabled)
                    {
                        int? additionalId = travelLogic.GetAdditionalInsuredByName(travelPolicy.TravelPolicyID, lblInsured3.Content.ToString());
                        var insuranceDetail = insuranceDetails.FirstOrDefault(d => d.TravelAdditionalInsuredID == additionalId);
                        if (insuranceDetail != null)
                        {
                            insuranceDetail.IsBaggage = chbBaggage3.IsChecked;
                            insuranceDetail.IsBicycle = chbBicycle3.IsChecked;
                            insuranceDetail.IsComputerOrTablet = chbComputerOrTablet3.IsChecked;
                            insuranceDetail.IsExistingMedicalCondition = chbExistingMedicalCondition3.IsChecked;
                            insuranceDetail.IsExtremeSport = chbExtremeSport3.IsChecked;
                            insuranceDetail.IsOther = chbOtherInsuranceDetails3.IsChecked;
                            insuranceDetail.IsPhoneOrGPS = chbPhoneOrGPS3.IsChecked;
                            insuranceDetail.IsPregnancy = chbPregnancy3.IsChecked;
                            insuranceDetail.IsSearchAndRescue = chbSearchAndRescue3.IsChecked;
                            insuranceDetail.IsWinterSport = chbWinterSport3.IsChecked;
                            insuranceDetail.OtherName = txtOtherName3.Text;
                            insuranceDetail.TravelInsuranceTypeID = insuranceTypeId3;
                        }
                        else
                        {
                            TravelInsuranceDetail Insured3Details = new TravelInsuranceDetail()
                            {
                                IsBaggage = chbBaggage3.IsChecked,
                                IsBicycle = chbBicycle3.IsChecked,
                                IsComputerOrTablet = chbComputerOrTablet3.IsChecked,
                                IsExistingMedicalCondition = chbExistingMedicalCondition3.IsChecked,
                                IsExtremeSport = chbExtremeSport3.IsChecked,
                                IsOther = chbOtherInsuranceDetails3.IsChecked,
                                IsPhoneOrGPS = chbPhoneOrGPS3.IsChecked,
                                IsPregnancy = chbPregnancy3.IsChecked,
                                IsSearchAndRescue = chbSearchAndRescue3.IsChecked,
                                IsWinterSport = chbWinterSport3.IsChecked,
                                OtherName = txtOtherName3.Text,
                                TravelInsuranceTypeID = insuranceTypeId3,
                                TravelPolicyID = travelPolicy.TravelPolicyID,
                                TravelAdditionalInsuredID = additionalId
                            };
                            insuranceDetails.Add(Insured3Details);
                        }
                    }

                    if (spInsuranceTypeInsured4.IsEnabled)
                    {
                        int? additionalId = travelLogic.GetAdditionalInsuredByName(travelPolicy.TravelPolicyID, lblInsured4.Content.ToString());
                        var insuranceDetail = insuranceDetails.FirstOrDefault(d => d.TravelAdditionalInsuredID == additionalId);
                        if (insuranceDetail != null)
                        {
                            insuranceDetail.IsBaggage = chbBaggage4.IsChecked;
                            insuranceDetail.IsBicycle = chbBicycle4.IsChecked;
                            insuranceDetail.IsComputerOrTablet = chbComputerOrTablet4.IsChecked;
                            insuranceDetail.IsExistingMedicalCondition = chbExistingMedicalCondition4.IsChecked;
                            insuranceDetail.IsExtremeSport = chbExtremeSport4.IsChecked;
                            insuranceDetail.IsOther = chbOtherInsuranceDetails4.IsChecked;
                            insuranceDetail.IsPhoneOrGPS = chbPhoneOrGPS4.IsChecked;
                            insuranceDetail.IsPregnancy = chbPregnancy4.IsChecked;
                            insuranceDetail.IsSearchAndRescue = chbSearchAndRescue4.IsChecked;
                            insuranceDetail.IsWinterSport = chbWinterSport4.IsChecked;
                            insuranceDetail.OtherName = txtOtherName4.Text;
                            insuranceDetail.TravelInsuranceTypeID = insuranceTypeId4;
                        }
                        else
                        {
                            TravelInsuranceDetail Insured4Details = new TravelInsuranceDetail()
                            {
                                IsBaggage = chbBaggage4.IsChecked,
                                IsBicycle = chbBicycle4.IsChecked,
                                IsComputerOrTablet = chbComputerOrTablet4.IsChecked,
                                IsExistingMedicalCondition = chbExistingMedicalCondition4.IsChecked,
                                IsExtremeSport = chbExtremeSport4.IsChecked,
                                IsOther = chbOtherInsuranceDetails4.IsChecked,
                                IsPhoneOrGPS = chbPhoneOrGPS4.IsChecked,
                                IsPregnancy = chbPregnancy4.IsChecked,
                                IsSearchAndRescue = chbSearchAndRescue4.IsChecked,
                                IsWinterSport = chbWinterSport4.IsChecked,
                                OtherName = txtOtherName4.Text,
                                TravelInsuranceTypeID = insuranceTypeId4,
                                TravelPolicyID = travelPolicy.TravelPolicyID,
                                TravelAdditionalInsuredID = additionalId
                            };
                            insuranceDetails.Add(Insured4Details);
                        }

                    }
                    if (spInsuranceTypeInsured5.IsEnabled)
                    {
                        int? additionalId = travelLogic.GetAdditionalInsuredByName(travelPolicy.TravelPolicyID, lblInsured5.Content.ToString());
                        var insuranceDetail = insuranceDetails.FirstOrDefault(d => d.TravelAdditionalInsuredID == additionalId);
                        if (insuranceDetail != null)
                        {
                            insuranceDetail.IsBaggage = chbBaggage5.IsChecked;
                            insuranceDetail.IsBicycle = chbBicycle5.IsChecked;
                            insuranceDetail.IsComputerOrTablet = chbComputerOrTablet5.IsChecked;
                            insuranceDetail.IsExistingMedicalCondition = chbExistingMedicalCondition5.IsChecked;
                            insuranceDetail.IsExtremeSport = chbExtremeSport5.IsChecked;
                            insuranceDetail.IsOther = chbOtherInsuranceDetails5.IsChecked;
                            insuranceDetail.IsPhoneOrGPS = chbPhoneOrGPS5.IsChecked;
                            insuranceDetail.IsPregnancy = chbPregnancy5.IsChecked;
                            insuranceDetail.IsSearchAndRescue = chbSearchAndRescue5.IsChecked;
                            insuranceDetail.IsWinterSport = chbWinterSport5.IsChecked;
                            insuranceDetail.OtherName = txtOtherName5.Text;
                            insuranceDetail.TravelInsuranceTypeID = insuranceTypeId5;
                        }
                        else
                        {
                            TravelInsuranceDetail Insured5Details = new TravelInsuranceDetail()
                            {
                                IsBaggage = chbBaggage5.IsChecked,
                                IsBicycle = chbBicycle5.IsChecked,
                                IsComputerOrTablet = chbComputerOrTablet5.IsChecked,
                                IsExistingMedicalCondition = chbExistingMedicalCondition5.IsChecked,
                                IsExtremeSport = chbExtremeSport5.IsChecked,
                                IsOther = chbOtherInsuranceDetails5.IsChecked,
                                IsPhoneOrGPS = chbPhoneOrGPS5.IsChecked,
                                IsPregnancy = chbPregnancy5.IsChecked,
                                IsSearchAndRescue = chbSearchAndRescue5.IsChecked,
                                IsWinterSport = chbWinterSport5.IsChecked,
                                OtherName = txtOtherName5.Text,
                                TravelInsuranceTypeID = insuranceTypeId5,
                                TravelPolicyID = travelPolicy.TravelPolicyID,
                                TravelAdditionalInsuredID = additionalId
                            };
                            insuranceDetails.Add(Insured5Details);
                        }
                    }
                    if (spInsuranceTypeInsured6.IsEnabled)
                    {
                        int? additionalId = travelLogic.GetAdditionalInsuredByName(travelPolicy.TravelPolicyID, lblInsured6.Content.ToString());
                        var insuranceDetail = insuranceDetails.FirstOrDefault(d => d.TravelAdditionalInsuredID == additionalId);
                        if (insuranceDetail != null)
                        {
                            insuranceDetail.IsBaggage = chbBaggage6.IsChecked;
                            insuranceDetail.IsBicycle = chbBicycle6.IsChecked;
                            insuranceDetail.IsComputerOrTablet = chbComputerOrTablet6.IsChecked;
                            insuranceDetail.IsExistingMedicalCondition = chbExistingMedicalCondition6.IsChecked;
                            insuranceDetail.IsExtremeSport = chbExtremeSport6.IsChecked;
                            insuranceDetail.IsOther = chbOtherInsuranceDetails6.IsChecked;
                            insuranceDetail.IsPhoneOrGPS = chbPhoneOrGPS6.IsChecked;
                            insuranceDetail.IsPregnancy = chbPregnancy6.IsChecked;
                            insuranceDetail.IsSearchAndRescue = chbSearchAndRescue6.IsChecked;
                            insuranceDetail.IsWinterSport = chbWinterSport6.IsChecked;
                            insuranceDetail.OtherName = txtOtherName6.Text;
                            insuranceDetail.TravelInsuranceTypeID = insuranceTypeId6;
                        }
                        else
                        {
                            TravelInsuranceDetail Insured6Details = new TravelInsuranceDetail()
                            {
                                IsBaggage = chbBaggage6.IsChecked,
                                IsBicycle = chbBicycle6.IsChecked,
                                IsComputerOrTablet = chbComputerOrTablet6.IsChecked,
                                IsExistingMedicalCondition = chbExistingMedicalCondition6.IsChecked,
                                IsExtremeSport = chbExtremeSport6.IsChecked,
                                IsOther = chbOtherInsuranceDetails6.IsChecked,
                                IsPhoneOrGPS = chbPhoneOrGPS6.IsChecked,
                                IsPregnancy = chbPregnancy6.IsChecked,
                                IsSearchAndRescue = chbSearchAndRescue6.IsChecked,
                                IsWinterSport = chbWinterSport6.IsChecked,
                                OtherName = txtOtherName6.Text,
                                TravelInsuranceTypeID = insuranceTypeId6,
                                TravelPolicyID = travelPolicy.TravelPolicyID,
                                TravelAdditionalInsuredID = additionalId
                            };
                            insuranceDetails.Add(Insured6Details);
                        }
                    }
                    if (spInsuranceTypeInsured7.IsEnabled)
                    {
                        int? additionalId = travelLogic.GetAdditionalInsuredByName(travelPolicy.TravelPolicyID, lblInsured7.Content.ToString());
                        var insuranceDetail = insuranceDetails.FirstOrDefault(d => d.TravelAdditionalInsuredID == additionalId);
                        if (insuranceDetail != null)
                        {
                            insuranceDetail.IsBaggage = chbBaggage7.IsChecked;
                            insuranceDetail.IsBicycle = chbBicycle7.IsChecked;
                            insuranceDetail.IsComputerOrTablet = chbComputerOrTablet7.IsChecked;
                            insuranceDetail.IsExistingMedicalCondition = chbExistingMedicalCondition7.IsChecked;
                            insuranceDetail.IsExtremeSport = chbExtremeSport7.IsChecked;
                            insuranceDetail.IsOther = chbOtherInsuranceDetails7.IsChecked;
                            insuranceDetail.IsPhoneOrGPS = chbPhoneOrGPS7.IsChecked;
                            insuranceDetail.IsPregnancy = chbPregnancy7.IsChecked;
                            insuranceDetail.IsSearchAndRescue = chbSearchAndRescue7.IsChecked;
                            insuranceDetail.IsWinterSport = chbWinterSport7.IsChecked;
                            insuranceDetail.OtherName = txtOtherName7.Text;
                            insuranceDetail.TravelInsuranceTypeID = insuranceTypeId7;
                        }
                        else
                        {
                            TravelInsuranceDetail Insured7Details = new TravelInsuranceDetail()
                            {
                                IsBaggage = chbBaggage7.IsChecked,
                                IsBicycle = chbBicycle7.IsChecked,
                                IsComputerOrTablet = chbComputerOrTablet7.IsChecked,
                                IsExistingMedicalCondition = chbExistingMedicalCondition7.IsChecked,
                                IsExtremeSport = chbExtremeSport7.IsChecked,
                                IsOther = chbOtherInsuranceDetails7.IsChecked,
                                IsPhoneOrGPS = chbPhoneOrGPS7.IsChecked,
                                IsPregnancy = chbPregnancy7.IsChecked,
                                IsSearchAndRescue = chbSearchAndRescue7.IsChecked,
                                IsWinterSport = chbWinterSport7.IsChecked,
                                OtherName = txtOtherName7.Text,
                                TravelInsuranceTypeID = insuranceTypeId7,
                                TravelPolicyID = travelPolicy.TravelPolicyID,
                                TravelAdditionalInsuredID = additionalId
                            };
                            insuranceDetails.Add(Insured7Details);
                        }
                    }
                    if (spInsuranceTypeInsured8.IsEnabled)
                    {
                        int? additionalId = travelLogic.GetAdditionalInsuredByName(travelPolicy.TravelPolicyID, lblInsured8.Content.ToString());
                        var insuranceDetail = insuranceDetails.FirstOrDefault(d => d.TravelAdditionalInsuredID == additionalId);
                        if (insuranceDetail != null)
                        {
                            insuranceDetail.IsBaggage = chbBaggage8.IsChecked;
                            insuranceDetail.IsBicycle = chbBicycle8.IsChecked;
                            insuranceDetail.IsComputerOrTablet = chbComputerOrTablet8.IsChecked;
                            insuranceDetail.IsExistingMedicalCondition = chbExistingMedicalCondition8.IsChecked;
                            insuranceDetail.IsExtremeSport = chbExtremeSport8.IsChecked;
                            insuranceDetail.IsOther = chbOtherInsuranceDetails8.IsChecked;
                            insuranceDetail.IsPhoneOrGPS = chbPhoneOrGPS8.IsChecked;
                            insuranceDetail.IsPregnancy = chbPregnancy8.IsChecked;
                            insuranceDetail.IsSearchAndRescue = chbSearchAndRescue8.IsChecked;
                            insuranceDetail.IsWinterSport = chbWinterSport8.IsChecked;
                            insuranceDetail.OtherName = txtOtherName8.Text;
                            insuranceDetail.TravelInsuranceTypeID = insuranceTypeId8;
                        }
                        else
                        {
                            TravelInsuranceDetail Insured8Details = new TravelInsuranceDetail()
                            {
                                IsBaggage = chbBaggage8.IsChecked,
                                IsBicycle = chbBicycle8.IsChecked,
                                IsComputerOrTablet = chbComputerOrTablet8.IsChecked,
                                IsExistingMedicalCondition = chbExistingMedicalCondition8.IsChecked,
                                IsExtremeSport = chbExtremeSport8.IsChecked,
                                IsOther = chbOtherInsuranceDetails8.IsChecked,
                                IsPhoneOrGPS = chbPhoneOrGPS8.IsChecked,
                                IsPregnancy = chbPregnancy8.IsChecked,
                                IsSearchAndRescue = chbSearchAndRescue8.IsChecked,
                                IsWinterSport = chbWinterSport8.IsChecked,
                                OtherName = txtOtherName8.Text,
                                TravelInsuranceTypeID = insuranceTypeId8,
                                TravelPolicyID = travelPolicy.TravelPolicyID,
                                TravelAdditionalInsuredID = additionalId
                            };
                            insuranceDetails.Add(Insured8Details);
                        }
                    }
                    if (spInsuranceTypeInsured9.IsEnabled)
                    {
                        int? additionalId = travelLogic.GetAdditionalInsuredByName(travelPolicy.TravelPolicyID, lblInsured9.Content.ToString());
                        var insuranceDetail = insuranceDetails.FirstOrDefault(d => d.TravelAdditionalInsuredID == additionalId);
                        if (insuranceDetail != null)
                        {
                            insuranceDetail.IsBaggage = chbBaggage9.IsChecked;
                            insuranceDetail.IsBicycle = chbBicycle9.IsChecked;
                            insuranceDetail.IsComputerOrTablet = chbComputerOrTablet9.IsChecked;
                            insuranceDetail.IsExistingMedicalCondition = chbExistingMedicalCondition9.IsChecked;
                            insuranceDetail.IsExtremeSport = chbExtremeSport9.IsChecked;
                            insuranceDetail.IsOther = chbOtherInsuranceDetails9.IsChecked;
                            insuranceDetail.IsPhoneOrGPS = chbPhoneOrGPS9.IsChecked;
                            insuranceDetail.IsPregnancy = chbPregnancy9.IsChecked;
                            insuranceDetail.IsSearchAndRescue = chbSearchAndRescue9.IsChecked;
                            insuranceDetail.IsWinterSport = chbWinterSport9.IsChecked;
                            insuranceDetail.OtherName = txtOtherName9.Text;
                            insuranceDetail.TravelInsuranceTypeID = insuranceTypeId9;
                        }
                        else
                        {
                            TravelInsuranceDetail Insured9Details = new TravelInsuranceDetail()
                            {
                                IsBaggage = chbBaggage9.IsChecked,
                                IsBicycle = chbBicycle9.IsChecked,
                                IsComputerOrTablet = chbComputerOrTablet9.IsChecked,
                                IsExistingMedicalCondition = chbExistingMedicalCondition9.IsChecked,
                                IsExtremeSport = chbExtremeSport9.IsChecked,
                                IsOther = chbOtherInsuranceDetails9.IsChecked,
                                IsPhoneOrGPS = chbPhoneOrGPS9.IsChecked,
                                IsPregnancy = chbPregnancy9.IsChecked,
                                IsSearchAndRescue = chbSearchAndRescue9.IsChecked,
                                IsWinterSport = chbWinterSport9.IsChecked,
                                OtherName = txtOtherName9.Text,
                                TravelInsuranceTypeID = insuranceTypeId9,
                                TravelPolicyID = travelPolicy.TravelPolicyID,
                                TravelAdditionalInsuredID = additionalId
                            };
                            insuranceDetails.Add(Insured9Details);
                        }
                    }
                    if (spInsuranceTypeInsured10.IsEnabled)
                    {
                        int? additionalId = travelLogic.GetAdditionalInsuredByName(travelPolicy.TravelPolicyID, lblInsured10.Content.ToString());
                        var insuranceDetail = insuranceDetails.FirstOrDefault(d => d.TravelAdditionalInsuredID == additionalId);
                        if (insuranceDetail != null)
                        {
                            insuranceDetail.IsBaggage = chbBaggage10.IsChecked;
                            insuranceDetail.IsBicycle = chbBicycle10.IsChecked;
                            insuranceDetail.IsComputerOrTablet = chbComputerOrTablet10.IsChecked;
                            insuranceDetail.IsExistingMedicalCondition = chbExistingMedicalCondition10.IsChecked;
                            insuranceDetail.IsExtremeSport = chbExtremeSport10.IsChecked;
                            insuranceDetail.IsOther = chbOtherInsuranceDetails10.IsChecked;
                            insuranceDetail.IsPhoneOrGPS = chbPhoneOrGPS10.IsChecked;
                            insuranceDetail.IsPregnancy = chbPregnancy10.IsChecked;
                            insuranceDetail.IsSearchAndRescue = chbSearchAndRescue10.IsChecked;
                            insuranceDetail.IsWinterSport = chbWinterSport10.IsChecked;
                            insuranceDetail.OtherName = txtOtherName10.Text;
                            insuranceDetail.TravelInsuranceTypeID = insuranceTypeId10;
                        }
                        else
                        {
                            TravelInsuranceDetail Insured10Details = new TravelInsuranceDetail()
                            {
                                IsBaggage = chbBaggage10.IsChecked,
                                IsBicycle = chbBicycle10.IsChecked,
                                IsComputerOrTablet = chbComputerOrTablet10.IsChecked,
                                IsExistingMedicalCondition = chbExistingMedicalCondition10.IsChecked,
                                IsExtremeSport = chbExtremeSport10.IsChecked,
                                IsOther = chbOtherInsuranceDetails10.IsChecked,
                                IsPhoneOrGPS = chbPhoneOrGPS10.IsChecked,
                                IsPregnancy = chbPregnancy10.IsChecked,
                                IsSearchAndRescue = chbSearchAndRescue10.IsChecked,
                                IsWinterSport = chbWinterSport10.IsChecked,
                                OtherName = txtOtherName10.Text,
                                TravelInsuranceTypeID = insuranceTypeId10,
                                TravelPolicyID = travelPolicy.TravelPolicyID,
                                TravelAdditionalInsuredID = additionalId
                            };
                            insuranceDetails.Add(Insured10Details);
                        }
                    }
                    coveragesLogic.InsertCoveragesToTravelPolicy(travelPolicy.TravelPolicyID, coverages, isAddition);
                    trackingLogics.InsertTravelTrackings(travelPolicy.TravelPolicyID, trackings, isAddition);
                    if (chbStandardMoneyCollection.IsChecked == true)
                    {
                        int standardMoneyCollectionId = moneyCollectionLogic.InsertStandardMoneyCollection(standardMoneyCollection, new Insurance() { InsuranceName = "נסיעות לחו''ל" }, travelPolicy.TravelPolicyID);
                        moneyCollectionLogic.InsertTrackingsToStandardMoneyCollection(standardMoneyCollectionId, standardMoneyCollectionTrackings);
                    }
                }
                travelLogic.InsertInsurancesDetails(insuranceDetails.ToArray());

            }


            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }

            if (isAddition == false)
            {
                if (travelPolicy == null)
                {
                    if (!Directory.Exists(@path))
                    {
                        Directory.CreateDirectory(@path);
                    }
                }
                else
                {
                    if (path == null && newPath != null)
                    {
                        Directory.CreateDirectory(newPath);
                    }
                    else if (path != newPath)
                    {
                        try
                        {
                            Directory.Move(path, newPath);
                        }
                        catch (Exception)
                        {
                            System.Windows.Forms.MessageBox.Show("לא ניתן למצוא את נתיב התיקייה");
                        }
                    }
                }
            }

            if (sender is Button)
            {
                Button btn = (Button)sender;
                if (btn.Name == "btnSave")
                {
                    confirmBeforeClosing = false;
                    Close();
                }
            }
        }

        private void SaveUnsavedInsured()
        {
            if (cbInsuredType.SelectedItem != null || txtRelativeId.Text != "" || txtRelativeFirstName.Text != "" || dpRelativeBirthDate.SelectedDate != null || cbRelativeMaritalStatus.SelectedItem != null || cbRelativeGender.SelectedItem != null || txtRelativeProfession.Text != "")
            {
                if (dgInsureds.SelectedItem == null)
                {
                    btnAddInsured_Click(this, new RoutedEventArgs());
                }
                else
                {
                    btnUpdateInsured_Click(this, new RoutedEventArgs());
                }
            }
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void btnUpdateCompanyTable_Click(object sender, RoutedEventArgs e)
        {
            int? selectedItemId = null;
            if (cbCompany.SelectedItem != null)
            {
                selectedItemId = ((InsuranceCompany)cbCompany.SelectedItem).CompanyID;
            }
            int insuranceID = insuranceLogic.GetInsuranceID("נסיעות לחו''ל");
            InsuranceCompany company = new InsuranceCompany() { InsuranceID = insuranceID };
            frmUpdateTable updateCompaniesTable = new frmUpdateTable(company);
            updateCompaniesTable.ShowDialog();
            cbCompanyBinding();
            if (updateCompaniesTable.ItemAdded != null)
            {
                CompanySelection(((InsuranceCompany)updateCompaniesTable.ItemAdded).CompanyID);
            }
            else if (selectedItemId != null)
            {
                CompanySelection((int)selectedItemId);
            }
        }

        private void CompanySelection(int companyID)
        {
            var companies = cbCompany.Items;
            foreach (var item in companies)
            {
                InsuranceCompany company = (InsuranceCompany)item;
                if (company.CompanyID == companyID)
                {
                    cbCompany.SelectedItem = item;
                    break;
                }
            }
        }

        private void btnAddInsured_Click(object sender, RoutedEventArgs e)
        {
            if (nullErrorList != null && nullErrorList.Count > 0)
            {
                foreach (var item in nullErrorList)
                {
                    if (item is TextBox)
                    {
                        ((TextBox)item).ClearValue(BorderBrushProperty);
                    }
                    else if (item is ComboBox)
                    {
                        ((ComboBox)item).ClearValue(BorderBrushProperty);
                    }
                }
            }

            //בדיקת שדות חובה
            nullErrorList = validations.InputNullValidation(new object[] { cbInsuredType, txtRelativeLastName, txtRelativeFirstName, txtRelativeId });
            if (nullErrorList.Count > 0)
            {
                if (sender is Window)
                {
                    MessageBox.Show("שים לב, פרטי מבוטח הנוסף לא עודכנו במערכת", "מידע", MessageBoxButton.OK, MessageBoxImage.Information);
                    return;
                }
                MessageBox.Show("נא למלאות שדות חובה", "", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            bool? isMale = null;
            if (cbRelativeGender.SelectedIndex == 1)
            {
                isMale = false;
            }
            else if (cbRelativeGender.SelectedIndex == 0)
            {
                isMale = true;
            }
            string maritalStatus = "";
            if (cbRelativeMaritalStatus.SelectedItem != null)
            {
                maritalStatus = ((ComboBoxItem)cbRelativeMaritalStatus.SelectedItem).Content.ToString();
            }
            TravelAdditionalInsured insured = new TravelAdditionalInsured() { BirthDate = dpRelativeBirthDate.SelectedDate, FirstName = txtRelativeFirstName.Text, IdNumber = txtRelativeId.Text, Relationship = ((ComboBoxItem)cbInsuredType.SelectedItem).Content.ToString(), IsMale = isMale, LastName = txtRelativeLastName.Text, MaritalStatus = maritalStatus, Profession = txtRelativeProfession.Text };
            insureds.Add(insured);
            EnableInsuranceDetail(insured);
            dgInsuredsBinding();
            ClearInsuredInputs();
        }

        private void ClearInsuredInputs()
        {
            cbInsuredType.SelectedIndex = -1;
            txtRelativeId.Clear();
            txtRelativeLastName.Text = client.LastName;
            txtRelativeFirstName.Clear();
            dpRelativeBirthDate.SelectedDate = null;
            cbRelativeMaritalStatus.SelectedIndex = -1;
            cbRelativeGender.SelectedIndex = -1;
            txtRelativeProfession.Clear();
        }

        private void EnableInsuranceDetail(TravelAdditionalInsured insuredAdded)
        {
            if (spInsuranceTypeInsured2.IsEnabled == false)
            {
                lblInsured2.Content = insuredAdded.FirstName + " " + insuredAdded.LastName;
                //lblInsured2.Foreground = Brushes.White;
                spInsuranceTypeInsured2.IsEnabled = true;
                chbWinterSport2.IsEnabled = true;
                chbExtremeSport2.IsEnabled = true;
                chbSearchAndRescue2.IsEnabled = true;
                chbPregnancy2.IsEnabled = true;
                chbExistingMedicalCondition2.IsEnabled = true;
                chbBaggage2.IsEnabled = true;
                chbComputerOrTablet2.IsEnabled = true;
                chbPhoneOrGPS2.IsEnabled = true;
                chbBicycle2.IsEnabled = true;
                chbOtherInsuranceDetails2.IsEnabled = true;
            }
            else if (spInsuranceTypeInsured3.IsEnabled == false)
            {
                lblInsured3.Content = insuredAdded.FirstName + " " + insuredAdded.LastName;
                //lblInsured3.Foreground = Brushes.White;
                spInsuranceTypeInsured3.IsEnabled = true;
                chbWinterSport3.IsEnabled = true;
                chbExtremeSport3.IsEnabled = true;
                chbSearchAndRescue3.IsEnabled = true;
                chbPregnancy3.IsEnabled = true;
                chbExistingMedicalCondition3.IsEnabled = true;
                chbBaggage3.IsEnabled = true;
                chbComputerOrTablet3.IsEnabled = true;
                chbPhoneOrGPS3.IsEnabled = true;
                chbBicycle3.IsEnabled = true;
                chbOtherInsuranceDetails3.IsEnabled = true;
            }
            else if (spInsuranceTypeInsured4.IsEnabled == false)
            {
                lblInsured4.Content = insuredAdded.FirstName + " " + insuredAdded.LastName;
                //lblInsured4.Foreground = Brushes.White;
                spInsuranceTypeInsured4.IsEnabled = true;
                chbWinterSport4.IsEnabled = true;
                chbExtremeSport4.IsEnabled = true;
                chbSearchAndRescue4.IsEnabled = true;
                chbPregnancy4.IsEnabled = true;
                chbExistingMedicalCondition4.IsEnabled = true;
                chbBaggage4.IsEnabled = true;
                chbComputerOrTablet4.IsEnabled = true;
                chbPhoneOrGPS4.IsEnabled = true;
                chbBicycle4.IsEnabled = true;
                chbOtherInsuranceDetails4.IsEnabled = true;
            }
            else if (spInsuranceTypeInsured5.IsEnabled == false)
            {
                lblInsured5.Content = insuredAdded.FirstName + " " + insuredAdded.LastName;
                //lblInsured5.Foreground = Brushes.White;
                spInsuranceTypeInsured5.IsEnabled = true;
                chbWinterSport5.IsEnabled = true;
                chbExtremeSport5.IsEnabled = true;
                chbSearchAndRescue5.IsEnabled = true;
                chbPregnancy5.IsEnabled = true;
                chbExistingMedicalCondition5.IsEnabled = true;
                chbBaggage5.IsEnabled = true;
                chbComputerOrTablet5.IsEnabled = true;
                chbPhoneOrGPS5.IsEnabled = true;
                chbBicycle5.IsEnabled = true;
                chbOtherInsuranceDetails5.IsEnabled = true;
            }
            else if (spInsuranceTypeInsured6.IsEnabled == false)
            {
                lblInsured6.Content = insuredAdded.FirstName + " " + insuredAdded.LastName;
                //lblInsured6.Foreground = Brushes.White;
                spInsuranceTypeInsured6.IsEnabled = true;
                chbWinterSport6.IsEnabled = true;
                chbExtremeSport6.IsEnabled = true;
                chbSearchAndRescue6.IsEnabled = true;
                chbPregnancy6.IsEnabled = true;
                chbExistingMedicalCondition6.IsEnabled = true;
                chbBaggage6.IsEnabled = true;
                chbComputerOrTablet6.IsEnabled = true;
                chbPhoneOrGPS6.IsEnabled = true;
                chbBicycle6.IsEnabled = true;
                chbOtherInsuranceDetails6.IsEnabled = true;
            }
            else if (spInsuranceTypeInsured7.IsEnabled == false)
            {
                lblInsured7.Content = insuredAdded.FirstName + " " + insuredAdded.LastName;
                //lblInsured7.Foreground = Brushes.White;
                spInsuranceTypeInsured7.IsEnabled = true;
                chbWinterSport7.IsEnabled = true;
                chbExtremeSport7.IsEnabled = true;
                chbSearchAndRescue7.IsEnabled = true;
                chbPregnancy7.IsEnabled = true;
                chbExistingMedicalCondition7.IsEnabled = true;
                chbBaggage7.IsEnabled = true;
                chbComputerOrTablet7.IsEnabled = true;
                chbPhoneOrGPS7.IsEnabled = true;
                chbBicycle7.IsEnabled = true;
                chbOtherInsuranceDetails7.IsEnabled = true;
            }
            else if (spInsuranceTypeInsured8.IsEnabled == false)
            {
                lblInsured8.Content = insuredAdded.FirstName + " " + insuredAdded.LastName;
                //lblInsured8.Foreground = Brushes.White;
                spInsuranceTypeInsured8.IsEnabled = true;
                chbWinterSport8.IsEnabled = true;
                chbExtremeSport8.IsEnabled = true;
                chbSearchAndRescue8.IsEnabled = true;
                chbPregnancy8.IsEnabled = true;
                chbExistingMedicalCondition8.IsEnabled = true;
                chbBaggage8.IsEnabled = true;
                chbComputerOrTablet8.IsEnabled = true;
                chbPhoneOrGPS8.IsEnabled = true;
                chbBicycle8.IsEnabled = true;
                chbOtherInsuranceDetails8.IsEnabled = true;
            }
            else if (spInsuranceTypeInsured9.IsEnabled == false)
            {
                lblInsured9.Content = insuredAdded.FirstName + " " + insuredAdded.LastName;
                //lblInsured9.Foreground = Brushes.White;
                spInsuranceTypeInsured9.IsEnabled = true;
                chbWinterSport9.IsEnabled = true;
                chbExtremeSport9.IsEnabled = true;
                chbSearchAndRescue9.IsEnabled = true;
                chbPregnancy9.IsEnabled = true;
                chbExistingMedicalCondition9.IsEnabled = true;
                chbBaggage9.IsEnabled = true;
                chbComputerOrTablet9.IsEnabled = true;
                chbPhoneOrGPS9.IsEnabled = true;
                chbBicycle9.IsEnabled = true;
                chbOtherInsuranceDetails9.IsEnabled = true;
            }
            else if (spInsuranceTypeInsured10.IsEnabled == false)
            {
                lblInsured10.Content = insuredAdded.FirstName + " " + insuredAdded.LastName;
                //lblInsured10.Foreground = Brushes.White;
                spInsuranceTypeInsured10.IsEnabled = true;
                chbWinterSport10.IsEnabled = true;
                chbExtremeSport10.IsEnabled = true;
                chbSearchAndRescue10.IsEnabled = true;
                chbPregnancy10.IsEnabled = true;
                chbExistingMedicalCondition10.IsEnabled = true;
                chbBaggage10.IsEnabled = true;
                chbComputerOrTablet10.IsEnabled = true;
                chbPhoneOrGPS10.IsEnabled = true;
                chbBicycle10.IsEnabled = true;
                chbOtherInsuranceDetails10.IsEnabled = true;
            }
        }

        private void DesableInsuranceDetail(string insuredName)
        {
            if (lblInsured2.Content.ToString() == insuredName)
            {
                lblInsured2.Foreground = gray;
                lblInsured2.Content = "שם המבוטח";
                spInsuranceTypeInsured2.IsEnabled = false;
                cbInsuranceTypeInsured2.SelectedItem = null;
                chbWinterSport2.IsEnabled = false;
                chbWinterSport2.IsChecked = false;
                chbExtremeSport2.IsEnabled = false;
                chbExtremeSport2.IsChecked = false;
                chbSearchAndRescue2.IsEnabled = false;
                chbSearchAndRescue2.IsChecked = false;
                chbPregnancy2.IsEnabled = false;
                chbPregnancy2.IsChecked = false;
                chbExistingMedicalCondition2.IsEnabled = false;
                chbExistingMedicalCondition2.IsChecked = false;
                chbBaggage2.IsEnabled = false;
                chbBaggage2.IsChecked = false;
                chbComputerOrTablet2.IsEnabled = false;
                chbComputerOrTablet2.IsChecked = false;
                chbPhoneOrGPS2.IsEnabled = false;
                chbPhoneOrGPS2.IsChecked = false;
                chbBicycle2.IsEnabled = false;
                chbBicycle2.IsChecked = false;
                chbOtherInsuranceDetails2.IsEnabled = false;
                chbOtherInsuranceDetails2.IsChecked = false;
            }
            else if (lblInsured3.Content.ToString() == insuredName)
            {
                lblInsured3.Foreground = gray;
                lblInsured3.Content = "שם המבוטח";
                spInsuranceTypeInsured3.IsEnabled = false;
                cbInsuranceTypeInsured3.SelectedItem = null;
                chbWinterSport3.IsEnabled = false;
                chbWinterSport3.IsChecked = false;
                chbExtremeSport3.IsEnabled = false;
                chbExtremeSport3.IsChecked = false;
                chbSearchAndRescue3.IsEnabled = false;
                chbSearchAndRescue3.IsChecked = false;
                chbPregnancy3.IsEnabled = false;
                chbPregnancy3.IsChecked = false;
                chbExistingMedicalCondition3.IsEnabled = false;
                chbExistingMedicalCondition3.IsChecked = false;
                chbBaggage3.IsEnabled = false;
                chbBaggage3.IsChecked = false;
                chbComputerOrTablet3.IsEnabled = false;
                chbComputerOrTablet3.IsChecked = false;
                chbPhoneOrGPS3.IsEnabled = false;
                chbPhoneOrGPS3.IsChecked = false;
                chbBicycle3.IsEnabled = false;
                chbBicycle3.IsChecked = false;
                chbOtherInsuranceDetails3.IsEnabled = false;
                chbOtherInsuranceDetails3.IsChecked = false;
            }
            else if (lblInsured4.Content.ToString() == insuredName)
            {
                lblInsured4.Foreground = gray;
                lblInsured4.Content = "שם המבוטח";
                spInsuranceTypeInsured4.IsEnabled = false;
                cbInsuranceTypeInsured4.SelectedItem = null;
                chbWinterSport4.IsEnabled = false;
                chbWinterSport4.IsChecked = false;
                chbExtremeSport4.IsEnabled = false;
                chbExtremeSport4.IsChecked = false;
                chbSearchAndRescue4.IsEnabled = false;
                chbSearchAndRescue4.IsChecked = false;
                chbPregnancy4.IsEnabled = false;
                chbPregnancy4.IsChecked = false;
                chbExistingMedicalCondition4.IsEnabled = false;
                chbExistingMedicalCondition4.IsChecked = false;
                chbBaggage4.IsEnabled = false;
                chbBaggage4.IsChecked = false;
                chbComputerOrTablet4.IsEnabled = false;
                chbComputerOrTablet4.IsChecked = false;
                chbPhoneOrGPS4.IsEnabled = false;
                chbPhoneOrGPS4.IsChecked = false;
                chbBicycle4.IsEnabled = false;
                chbBicycle4.IsChecked = false;
                chbOtherInsuranceDetails4.IsEnabled = false;
                chbOtherInsuranceDetails4.IsChecked = false;
            }
            else if (lblInsured5.Content.ToString() == insuredName)
            {
                lblInsured5.Foreground = gray;
                lblInsured5.Content = "שם המבוטח";
                spInsuranceTypeInsured5.IsEnabled = false;
                cbInsuranceTypeInsured5.SelectedItem = null;
                chbWinterSport5.IsEnabled = false;
                chbWinterSport5.IsChecked = false;
                chbExtremeSport5.IsEnabled = false;
                chbExtremeSport5.IsChecked = false;
                chbSearchAndRescue5.IsEnabled = false;
                chbSearchAndRescue5.IsChecked = false;
                chbPregnancy5.IsEnabled = false;
                chbPregnancy5.IsChecked = false;
                chbExistingMedicalCondition5.IsEnabled = false;
                chbExistingMedicalCondition5.IsChecked = false;
                chbBaggage5.IsEnabled = false;
                chbBaggage5.IsChecked = false;
                chbComputerOrTablet5.IsEnabled = false;
                chbComputerOrTablet5.IsChecked = false;
                chbPhoneOrGPS5.IsEnabled = false;
                chbPhoneOrGPS5.IsChecked = false;
                chbBicycle5.IsEnabled = false;
                chbBicycle5.IsChecked = false;
                chbOtherInsuranceDetails5.IsEnabled = false;
                chbOtherInsuranceDetails5.IsChecked = false;
            }
            else if (lblInsured6.Content.ToString() == insuredName)
            {
                lblInsured6.Foreground = gray;
                lblInsured6.Content = "שם המבוטח";
                spInsuranceTypeInsured6.IsEnabled = false;
                cbInsuranceTypeInsured6.SelectedItem = null;
                chbWinterSport6.IsEnabled = false;
                chbWinterSport6.IsChecked = false;
                chbExtremeSport6.IsEnabled = false;
                chbExtremeSport6.IsChecked = false;
                chbSearchAndRescue6.IsEnabled = false;
                chbSearchAndRescue6.IsChecked = false;
                chbPregnancy6.IsEnabled = false;
                chbPregnancy6.IsChecked = false;
                chbExistingMedicalCondition6.IsEnabled = false;
                chbExistingMedicalCondition6.IsChecked = false;
                chbBaggage6.IsEnabled = false;
                chbBaggage6.IsChecked = false;
                chbComputerOrTablet6.IsEnabled = false;
                chbComputerOrTablet6.IsChecked = false;
                chbPhoneOrGPS6.IsEnabled = false;
                chbPhoneOrGPS6.IsChecked = false;
                chbBicycle6.IsEnabled = false;
                chbBicycle6.IsChecked = false;
                chbOtherInsuranceDetails6.IsEnabled = false;
                chbOtherInsuranceDetails6.IsChecked = false;
            }
            else if (lblInsured7.Content.ToString() == insuredName)
            {
                lblInsured7.Foreground = gray;
                lblInsured7.Content = "שם המבוטח";
                spInsuranceTypeInsured7.IsEnabled = false;
                cbInsuranceTypeInsured7.SelectedItem = null;
                chbWinterSport7.IsEnabled = false;
                chbWinterSport7.IsChecked = false;
                chbExtremeSport7.IsEnabled = false;
                chbExtremeSport7.IsChecked = false;
                chbSearchAndRescue7.IsEnabled = false;
                chbSearchAndRescue7.IsChecked = false;
                chbPregnancy7.IsEnabled = false;
                chbPregnancy7.IsChecked = false;
                chbExistingMedicalCondition7.IsEnabled = false;
                chbExistingMedicalCondition7.IsChecked = false;
                chbBaggage7.IsEnabled = false;
                chbBaggage7.IsChecked = false;
                chbComputerOrTablet7.IsEnabled = false;
                chbComputerOrTablet7.IsChecked = false;
                chbPhoneOrGPS7.IsEnabled = false;
                chbPhoneOrGPS7.IsChecked = false;
                chbBicycle7.IsEnabled = false;
                chbBicycle7.IsChecked = false;
                chbOtherInsuranceDetails7.IsEnabled = false;
                chbOtherInsuranceDetails7.IsChecked = false;
            }
            else if (lblInsured8.Content.ToString() == insuredName)
            {
                lblInsured8.Foreground = gray;
                lblInsured8.Content = "שם המבוטח";
                spInsuranceTypeInsured8.IsEnabled = false;
                cbInsuranceTypeInsured8.SelectedItem = null;
                chbWinterSport8.IsEnabled = false;
                chbWinterSport8.IsChecked = false;
                chbExtremeSport8.IsEnabled = false;
                chbExtremeSport8.IsChecked = false;
                chbSearchAndRescue8.IsEnabled = false;
                chbSearchAndRescue8.IsChecked = false;
                chbPregnancy8.IsEnabled = false;
                chbPregnancy8.IsChecked = false;
                chbExistingMedicalCondition8.IsEnabled = false;
                chbExistingMedicalCondition8.IsChecked = false;
                chbBaggage8.IsEnabled = false;
                chbBaggage8.IsChecked = false;
                chbComputerOrTablet8.IsEnabled = false;
                chbComputerOrTablet8.IsChecked = false;
                chbPhoneOrGPS8.IsEnabled = false;
                chbPhoneOrGPS8.IsChecked = false;
                chbBicycle8.IsEnabled = false;
                chbBicycle8.IsChecked = false;
                chbOtherInsuranceDetails8.IsEnabled = false;
                chbOtherInsuranceDetails8.IsChecked = false;
            }
            else if (lblInsured9.Content.ToString() == insuredName)
            {
                lblInsured9.Foreground = gray;
                lblInsured9.Content = "שם המבוטח";
                spInsuranceTypeInsured9.IsEnabled = false;
                cbInsuranceTypeInsured9.SelectedItem = null;
                chbWinterSport9.IsEnabled = false;
                chbWinterSport9.IsChecked = false;
                chbExtremeSport9.IsEnabled = false;
                chbExtremeSport9.IsChecked = false;
                chbSearchAndRescue9.IsEnabled = false;
                chbSearchAndRescue9.IsChecked = false;
                chbPregnancy9.IsEnabled = false;
                chbPregnancy9.IsChecked = false;
                chbExistingMedicalCondition9.IsEnabled = false;
                chbExistingMedicalCondition9.IsChecked = false;
                chbBaggage9.IsEnabled = false;
                chbBaggage9.IsChecked = false;
                chbComputerOrTablet9.IsEnabled = false;
                chbComputerOrTablet9.IsChecked = false;
                chbPhoneOrGPS9.IsEnabled = false;
                chbPhoneOrGPS9.IsChecked = false;
                chbBicycle9.IsEnabled = false;
                chbBicycle9.IsChecked = false;
                chbOtherInsuranceDetails9.IsEnabled = false;
                chbOtherInsuranceDetails9.IsChecked = false;
            }
            else if (lblInsured10.Content.ToString() == insuredName)
            {
                lblInsured10.Foreground = gray;
                lblInsured10.Content = "שם המבוטח";
                spInsuranceTypeInsured10.IsEnabled = false;
                cbInsuranceTypeInsured10.SelectedItem = null;
                chbWinterSport10.IsEnabled = false;
                chbWinterSport10.IsChecked = false;
                chbExtremeSport10.IsEnabled = false;
                chbExtremeSport10.IsChecked = false;
                chbSearchAndRescue10.IsEnabled = false;
                chbSearchAndRescue10.IsChecked = false;
                chbPregnancy10.IsEnabled = false;
                chbPregnancy10.IsChecked = false;
                chbExistingMedicalCondition10.IsEnabled = false;
                chbExistingMedicalCondition10.IsChecked = false;
                chbBaggage10.IsEnabled = false;
                chbBaggage10.IsChecked = false;
                chbComputerOrTablet10.IsEnabled = false;
                chbComputerOrTablet10.IsChecked = false;
                chbPhoneOrGPS10.IsEnabled = false;
                chbPhoneOrGPS10.IsChecked = false;
                chbBicycle10.IsEnabled = false;
                chbBicycle10.IsChecked = false;
                chbOtherInsuranceDetails10.IsEnabled = false;
                chbOtherInsuranceDetails10.IsChecked = false;
            }
        }

        private void dgInsuredsBinding()
        {
            dgInsureds.ItemsSource = insureds.ToList();
            lv.AutoSizeColumns(dgInsureds.View);
        }

        private void btnDeleteInsured_Click(object sender, RoutedEventArgs e)
        {
            if (dgInsureds.SelectedItem == null)
            {
                MessageBox.Show("נא לסמן מבוטח", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            if (MessageBox.Show("למחוק מבוטח", "אישור", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                insureds.Remove((TravelAdditionalInsured)dgInsureds.SelectedItem);
                string name = ((TravelAdditionalInsured)dgInsureds.SelectedItem).FirstName + " " + ((TravelAdditionalInsured)dgInsureds.SelectedItem).LastName;
                DesableInsuranceDetail(name);
                dgInsuredsBinding();

            }
            else
            {
                dgInsureds.UnselectAll();
            }
        }

        private void btnUpdateInsured_Click(object sender, RoutedEventArgs e)
        {
            if (dgInsureds.SelectedItem == null)
            {
                MessageBox.Show("נא לסמן מבוטח", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            TravelAdditionalInsured insured = (TravelAdditionalInsured)dgInsureds.SelectedItem;
            if (insured.FirstName != txtRelativeFirstName.Text || insured.LastName != txtRelativeLastName.Text)
            {
                UpdateInsuredName(insured.FirstName + " " + insured.LastName, txtRelativeFirstName.Text + " " + txtRelativeLastName.Text);
            }
            insured.BirthDate = dpRelativeBirthDate.SelectedDate;
            insured.FirstName = txtRelativeFirstName.Text;
            insured.IdNumber = txtRelativeId.Text;
            insured.Relationship = ((ComboBoxItem)cbInsuredType.SelectedItem).Content.ToString();
            bool isMale = true;
            if (cbRelativeGender.SelectedIndex == 1)
            {
                isMale = false;
            }
            string maritalStatus = "";
            if (cbRelativeMaritalStatus.SelectedItem != null)
            {
                maritalStatus = ((ComboBoxItem)cbRelativeMaritalStatus.SelectedItem).Content.ToString();
            }
            insured.IsMale = isMale;
            insured.LastName = txtRelativeLastName.Text;
            insured.MaritalStatus = maritalStatus;
            insured.Profession = txtRelativeProfession.Text;
            dgInsuredsBinding();
        }

        private void UpdateInsuredName(string oldName, string newName)
        {
            if (lblInsured2.Content.ToString() == oldName)
            {
                lblInsured2.Content = newName;
            }
            else if (lblInsured3.Content.ToString() == oldName)
            {
                lblInsured3.Content = newName;
            }
            else if (lblInsured4.Content.ToString() == oldName)
            {
                lblInsured4.Content = newName;
            }
            else if (lblInsured5.Content.ToString() == oldName)
            {
                lblInsured5.Content = newName;
            }
            else if (lblInsured6.Content.ToString() == oldName)
            {
                lblInsured6.Content = newName;
            }
            else if (lblInsured7.Content.ToString() == oldName)
            {
                lblInsured7.Content = newName;
            }
            else if (lblInsured8.Content.ToString() == oldName)
            {
                lblInsured8.Content = newName;
            }
            else if (lblInsured9.Content.ToString() == oldName)
            {
                lblInsured9.Content = newName;
            }
            else if (lblInsured10.Content.ToString() == oldName)
            {
                lblInsured10.Content = newName;
            }
        }

        private void dgInsureds_MouseDown(object sender, MouseButtonEventArgs e)
        {
            dgInsureds.UnselectAll();
        }

        private void dgInsureds_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (dgInsureds.SelectedItem != null)
            {
                DisplayInsuredDetails();
            }
            else
            {
                ClearInsuredInputs();
            }
        }

        private void DisplayInsuredDetails()
        {
            TravelAdditionalInsured insured = (TravelAdditionalInsured)dgInsureds.SelectedItem;
            var insuredTypes = cbInsuredType.Items;
            foreach (var item in insuredTypes)
            {
                ComboBoxItem type = (ComboBoxItem)item;
                if (type.Content.ToString() == insured.Relationship)
                {
                    cbInsuredType.SelectedItem = item;
                    break;
                }
            }
            txtRelativeId.Text = insured.IdNumber;
            txtRelativeLastName.Text = insured.LastName;
            txtRelativeFirstName.Text = insured.FirstName;
            dpRelativeBirthDate.SelectedDate = insured.BirthDate;
            var maritalStatus = cbRelativeMaritalStatus.Items;
            foreach (var item in maritalStatus)
            {
                ComboBoxItem status = (ComboBoxItem)item;
                if (status.Content.ToString() == insured.MaritalStatus)
                {
                    cbRelativeMaritalStatus.SelectedItem = item;
                    break;
                }
            }
            if (insured.IsMale == true)
            {
                cbRelativeGender.SelectedIndex = 0;
            }
            else if (insured.IsMale == false)
            {
                cbRelativeGender.SelectedIndex = 1;
            }
            txtRelativeProfession.Text = insured.Profession;
        }

        private void btnNewCoverage_Click(object sender, RoutedEventArgs e)
        {
            frmTravelCoverage newCoverageWindow = new frmTravelCoverage();
            newCoverageWindow.ShowDialog();
            if (newCoverageWindow.NewCoverage != null)
            {
                coverages.Add(newCoverageWindow.NewCoverage);
            }
            dgCoveragesBinding();
        }

        private void dgCoveragesBinding()
        {
            dgCoverages.ItemsSource = coverages.ToList();
            lv.AutoSizeColumns(dgCoverages.View);
        }

        private void btnDeleteCoverage_Click(object sender, RoutedEventArgs e)
        {
            if (dgCoverages.SelectedItem == null)
            {
                MessageBox.Show("נא לסמן כיסוי בטבלה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            if (MessageBox.Show("למחוק כיסוי", "אישור", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                coverages.Remove((TravelCoverage)dgCoverages.SelectedItem);
                dgCoveragesBinding();
            }
            else
            {
                dgCoverages.UnselectAll();
            }
        }

        private void btnUpdateCoverage_Click(object sender, RoutedEventArgs e)
        {
            if (dgCoverages.SelectedItem == null)
            {
                MessageBox.Show("נא לסמן כיסוי בטבלה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            TravelCoverage coverageToUpdate = (TravelCoverage)dgCoverages.SelectedItem;
            frmTravelCoverage updateCoverageWindow = new frmTravelCoverage(coverageToUpdate);
            updateCoverageWindow.ShowDialog();
            //coverageToUpdate = updateCoverageWindow.NewCoverage;
            coverageToUpdate.TravelCoverageType = updateCoverageWindow.NewCoverage.TravelCoverageType;
            coverageToUpdate.TravelCoverageTypeID = updateCoverageWindow.NewCoverage.TravelCoverageTypeID;
            coverageToUpdate.CoverageAmount = updateCoverageWindow.NewCoverage.CoverageAmount;
            coverageToUpdate.Porcentage = updateCoverageWindow.NewCoverage.Porcentage;
            coverageToUpdate.premium = updateCoverageWindow.NewCoverage.premium;
            coverageToUpdate.Comments = updateCoverageWindow.NewCoverage.Comments;
            dgCoveragesBinding();
            dgCoverages.UnselectAll();
        }

        private void btnNewTracking_Click(object sender, RoutedEventArgs e)
        {
            frmTracking newTrackingWindow = new frmTracking(client, travelPolicy, user);
            newTrackingWindow.ShowDialog();
            if (newTrackingWindow.trackingContent != null)
            {
                trackings.Add(new TravelTracking() { TrackingContent = newTrackingWindow.trackingContent, Date = DateTime.Now, UserID = user.UserID, User = user });
                dgTrackingsBinding();
            }
        }
        private void dgTrackingsBinding()
        {
            dgTrackings.ItemsSource = trackings.OrderByDescending(t => t.Date);
            lv.AutoSizeColumns(dgTrackings.View);
        }

        private void btnDeleteTracking_Click(object sender, RoutedEventArgs e)
        {
            if (dgTrackings.SelectedItem == null)
            {
                MessageBox.Show("נא לסמן מעקב בטבלה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            if (MessageBox.Show("למחוק מעקב", "אישור", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                trackings.Remove((TravelTracking)dgTrackings.SelectedItem);
                dgTrackingsBinding();
            }
            else
            {
                dgTrackings.UnselectAll();
            }
        }

        private void btnUpdateTracking_Click(object sender, RoutedEventArgs e)
        {
            if (dgTrackings.SelectedItem == null)
            {
                MessageBox.Show("נא לסמן מעקב בטבלה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            TravelTracking trackingToUpdate = (TravelTracking)dgTrackings.SelectedItem;
            frmTracking updateTrackingWindow = new frmTracking(trackingToUpdate, client, travelPolicy, user);
            updateTrackingWindow.ShowDialog();
            trackingToUpdate.TrackingContent = updateTrackingWindow.trackingContent;
            dgTrackingsBinding();
            dgTrackings.UnselectAll();
        }

        private void dgTrackings_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            DependencyObject src = (DependencyObject)(e.OriginalSource);
            while (!(src is Control))
            {
                if (!(src is Visual))
                {
                    break;
                }
                src = VisualTreeHelper.GetParent(src);
            }

            if ((src is Control && src.GetType() == typeof(ListViewItem)) || src.GetType() == typeof(Run))
            {
                if (dgTrackings.SelectedItem == null)
                {
                    return;
                }
                TravelTracking trackingToUpdate = (TravelTracking)dgTrackings.SelectedItem;
                frmTracking updateTrackingWindow = new frmTracking(trackingToUpdate, client, travelPolicy, user);
                updateTrackingWindow.ShowDialog();
                trackingToUpdate.TrackingContent = updateTrackingWindow.trackingContent;
                dgTrackingsBinding();
                dgTrackings.UnselectAll();
            }
        }

        private void dgCoverages_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            DependencyObject src = (DependencyObject)(e.OriginalSource);
            while (!(src is Control))
            {
                if (!(src is Visual))
                {
                    break;
                }
                src = VisualTreeHelper.GetParent(src);
            }

            if ((src is Control && src.GetType() == typeof(ListViewItem)) || src.GetType() == typeof(Run))
            {
                if (dgCoverages.SelectedItem == null)
                {
                    return;
                }
                TravelCoverage coverageToUpdate = (TravelCoverage)dgCoverages.SelectedItem;
                frmTravelCoverage updateCoverageWindow = new frmTravelCoverage(coverageToUpdate);
                updateCoverageWindow.ShowDialog();
                //coverageToUpdate = updateCoverageWindow.NewCoverage;
                coverageToUpdate.TravelCoverageType = updateCoverageWindow.NewCoverage.TravelCoverageType;
                coverageToUpdate.TravelCoverageTypeID = updateCoverageWindow.NewCoverage.TravelCoverageTypeID;
                coverageToUpdate.CoverageAmount = updateCoverageWindow.NewCoverage.CoverageAmount;
                coverageToUpdate.Porcentage = updateCoverageWindow.NewCoverage.Porcentage;
                coverageToUpdate.premium = updateCoverageWindow.NewCoverage.premium;
                coverageToUpdate.Comments = updateCoverageWindow.NewCoverage.Comments;
                dgCoveragesBinding();
                dgCoverages.UnselectAll();
            }
        }

        private void btnUpdateCountry1Table_Click(object sender, RoutedEventArgs e)
        {
            string objName = ((Button)sender).Name;
            int? selectedItemId = null;
            ComboBox cb = null;
            if (objName == "btnUpdateCountry1Table")
            {
                if (cbCountry1.SelectedItem != null)
                {
                    selectedItemId = ((Country)cbCountry1.SelectedItem).CountryID;
                }
                cb = cbCountry1;
            }
            else if (objName == "btnUpdateCountry2Table")
            {
                if (cbCountry2.SelectedItem != null)
                {
                    selectedItemId = ((Country)cbCountry2.SelectedItem).CountryID;
                }
                cb = cbCountry2;
            }
            else if (objName == "btnUpdateCountry3Table")
            {
                if (cbCountry3.SelectedItem != null)
                {
                    selectedItemId = ((Country)cbCountry3.SelectedItem).CountryID;
                }
                cb = cbCountry3;
            }
            else if (objName == "btnUpdateCountry4Table")
            {
                if (cbCountry4.SelectedItem != null)
                {
                    selectedItemId = ((Country)cbCountry4.SelectedItem).CountryID;
                }
                cb = cbCountry4;
            }

            frmUpdateTable updateCountriesTable = new frmUpdateTable(new Country());
            updateCountriesTable.ShowDialog();
            cbCountriesBinding();
            if (updateCountriesTable.ItemAdded != null)
            {
                SelectCountryInCb(((Country)updateCountriesTable.ItemAdded).CountryID, cb);
            }
            else if (selectedItemId != null)
            {
                SelectCountryInCb((int)selectedItemId, cb);
            }
        }

        private void SelectCountryInCb(int id, ComboBox cb)
        {
            var items = cb.Items;
            foreach (var item in items)
            {
                Country parsedItem = (Country)item;
                if (parsedItem.CountryID == id)
                {
                    cb.SelectedItem = item;
                    break;
                }
            }
        }

        private void cbCompany_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cbCompany.SelectedItem == null)
            {
                cbPrincipalAgentNumber.SelectedItem = null;
                cbSecundaryAgentNumber.SelectedItem = null;
                cbPrincipalAgentNumber.IsEnabled = false;
                cbSecundaryAgentNumber.IsEnabled = false;
                return;
            }
            cbInsurenceTypeBinding();
            int insuranceID = insuranceLogic.GetInsuranceID("נסיעות לחו''ל");
            if (client != null)
            {
                cbPrincipalAgentNumber.ItemsSource = agentLogic.GetAgentNumbersByCompanyAndInsurance(client.PrincipalAgentID, (InsuranceCompany)cbCompany.SelectedItem, insuranceID);
                cbPrincipalAgentNumber.DisplayMemberPath = "Number";
                cbPrincipalAgentNumber.IsEnabled = true;
                cbPrincipalAgentNumber.SelectedIndex = 0;
                cbSecundaryAgentNumber.ItemsSource = agentLogic.GetAgentNumbersByCompanyAndInsurance(client.SecundaryAgentID, (InsuranceCompany)cbCompany.SelectedItem, insuranceID);
                cbSecundaryAgentNumber.DisplayMemberPath = "Number";
                cbSecundaryAgentNumber.IsEnabled = true;
                cbSecundaryAgentNumber.SelectedIndex = 0;
            }
            tbItemCoverage.Visibility = Visibility.Visible;
        }

        private void cbInsurenceTypeBinding()
        {
            int? principalId1 = SaveSelectedItem(cbInsuranceTypeInsured1);
            int? principalId2 = SaveSelectedItem(cbInsuranceTypeInsured2);
            int? principalId3 = SaveSelectedItem(cbInsuranceTypeInsured3);
            int? principalId4 = SaveSelectedItem(cbInsuranceTypeInsured4);
            int? principalId5 = SaveSelectedItem(cbInsuranceTypeInsured5);
            int? principalId6 = SaveSelectedItem(cbInsuranceTypeInsured6);
            int? principalId7 = SaveSelectedItem(cbInsuranceTypeInsured7);
            int? principalId8 = SaveSelectedItem(cbInsuranceTypeInsured8);
            int? principalId9 = SaveSelectedItem(cbInsuranceTypeInsured9);
            int? principalId10 = SaveSelectedItem(cbInsuranceTypeInsured10);

            var insurances = travelInsuranceLogic.GetActiveTravelInsuranceTypesByCompany(((InsuranceCompany)cbCompany.SelectedItem).CompanyID);
            cbInsuranceTypeInsured1.ItemsSource = insurances;
            cbInsuranceTypeInsured1.DisplayMemberPath = "TravelInsuranceTypeName";
            cbInsuranceTypeInsured2.ItemsSource = insurances;
            cbInsuranceTypeInsured2.DisplayMemberPath = "TravelInsuranceTypeName";
            cbInsuranceTypeInsured3.ItemsSource = insurances;
            cbInsuranceTypeInsured3.DisplayMemberPath = "TravelInsuranceTypeName";
            cbInsuranceTypeInsured4.ItemsSource = insurances;
            cbInsuranceTypeInsured4.DisplayMemberPath = "TravelInsuranceTypeName";
            cbInsuranceTypeInsured5.ItemsSource = insurances;
            cbInsuranceTypeInsured5.DisplayMemberPath = "TravelInsuranceTypeName";
            cbInsuranceTypeInsured6.ItemsSource = insurances;
            cbInsuranceTypeInsured6.DisplayMemberPath = "TravelInsuranceTypeName";
            cbInsuranceTypeInsured7.ItemsSource = insurances;
            cbInsuranceTypeInsured7.DisplayMemberPath = "TravelInsuranceTypeName";
            cbInsuranceTypeInsured8.ItemsSource = insurances;
            cbInsuranceTypeInsured8.DisplayMemberPath = "TravelInsuranceTypeName";
            cbInsuranceTypeInsured9.ItemsSource = insurances;
            cbInsuranceTypeInsured9.DisplayMemberPath = "TravelInsuranceTypeName";
            cbInsuranceTypeInsured10.ItemsSource = insurances;
            cbInsuranceTypeInsured10.DisplayMemberPath = "TravelInsuranceTypeName";

            if (principalId1 != null)
            {
                SelectItemInCb((int)principalId1, cbInsuranceTypeInsured1);
            }
            if (principalId2 != null)
            {
                SelectItemInCb((int)principalId2, cbInsuranceTypeInsured2);
            }
            if (principalId3 != null)
            {
                SelectItemInCb((int)principalId3, cbInsuranceTypeInsured3);
            }
            if (principalId4 != null)
            {
                SelectItemInCb((int)principalId4, cbInsuranceTypeInsured4);
            }
            if (principalId5 != null)
            {
                SelectItemInCb((int)principalId5, cbInsuranceTypeInsured5);
            }
            if (principalId6 != null)
            {
                SelectItemInCb((int)principalId6, cbInsuranceTypeInsured6);
            }
            if (principalId7 != null)
            {
                SelectItemInCb((int)principalId7, cbInsuranceTypeInsured7);
            }
            if (principalId8 != null)
            {
                SelectItemInCb((int)principalId8, cbInsuranceTypeInsured8);
            }
            if (principalId9 != null)
            {
                SelectItemInCb((int)principalId9, cbInsuranceTypeInsured9);
            }
            if (principalId10 != null)
            {
                SelectItemInCb((int)principalId10, cbInsuranceTypeInsured10);
            }
        }

        private void SelectItemInCb(int id, ComboBox cb)
        {
            var items = cb.Items;
            foreach (var item in items)
            {
                TravelInsuranceType parsedItem = (TravelInsuranceType)item;
                if (parsedItem.TravelInsuranceTypeID == id)
                {
                    cb.SelectedItem = item;
                    break;
                }
            }
        }

        public int? SaveSelectedItem(ComboBox cb)
        {
            if (cb.SelectedItem != null)
            {
                return ((TravelInsuranceType)cb.SelectedItem).TravelInsuranceTypeID;
            }
            else
            {
                return null;
            }
        }

        private void btnUpdateInsuranceTypeInsured1_Click(object sender, RoutedEventArgs e)
        {
            string objName = ((Button)sender).Name;
            int? selectedItemId = null;
            if (cbCompany.SelectedItem == null)
            {
                MessageBox.Show("נא בחר חברת ביטוח");
                return;
            }
            ComboBox cb = null;
            if (objName == "btnUpdateInsuranceTypeInsured1")
            {
                selectedItemId = SaveSelectedItem(cbInsuranceTypeInsured1);
                cb = cbInsuranceTypeInsured1;
            }
            else if (objName == "btnUpdateInsuranceTypeInsured2")
            {
                selectedItemId = SaveSelectedItem(cbInsuranceTypeInsured2);
                cb = cbInsuranceTypeInsured2;
            }
            else if (objName == "btnUpdateInsuranceTypeInsured3")
            {
                selectedItemId = SaveSelectedItem(cbInsuranceTypeInsured3);
                cb = cbInsuranceTypeInsured3;
            }
            else if (objName == "btnUpdateInsuranceTypeInsured4")
            {
                selectedItemId = SaveSelectedItem(cbInsuranceTypeInsured4);
                cb = cbInsuranceTypeInsured4;
            }
            else if (objName == "btnUpdateInsuranceTypeInsured5")
            {
                selectedItemId = SaveSelectedItem(cbInsuranceTypeInsured5);
                cb = cbInsuranceTypeInsured5;
            }
            else if (objName == "btnUpdateInsuranceTypeInsured6")
            {
                selectedItemId = SaveSelectedItem(cbInsuranceTypeInsured6);
                cb = cbInsuranceTypeInsured6;
            }
            else if (objName == "btnUpdateInsuranceTypeInsured7")
            {
                selectedItemId = SaveSelectedItem(cbInsuranceTypeInsured7);
                cb = cbInsuranceTypeInsured7;
            }
            else if (objName == "btnUpdateInsuranceTypeInsured8")
            {
                selectedItemId = SaveSelectedItem(cbInsuranceTypeInsured8);
                cb = cbInsuranceTypeInsured8;
            }
            else if (objName == "btnUpdateInsuranceTypeInsured9")
            {
                selectedItemId = SaveSelectedItem(cbInsuranceTypeInsured9);
                cb = cbInsuranceTypeInsured9;
            }
            else if (objName == "btnUpdateInsuranceTypeInsured10")
            {
                selectedItemId = SaveSelectedItem(cbInsuranceTypeInsured10);
                cb = cbInsuranceTypeInsured10;
            }

            frmTravelInsuranceTypes programs = new frmTravelInsuranceTypes((InsuranceCompany)cbCompany.SelectedItem);
            programs.ShowDialog();
            cbInsurenceTypeBinding();

            if (programs.typeAdded != null)
            {
                SelectItemInCb((programs.typeAdded).TravelInsuranceTypeID, cb);
            }
            else if (selectedItemId != null)
            {
                SelectItemInCb((int)selectedItemId, cb);
            }
        }

        private void txtPremium_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (sender is TextBox)
            {
                TextBox txtBox = (TextBox)sender;
                txtBox.ClearValue(BorderBrushProperty);
                if (txtBox.Text != "")
                {
                    try
                    {
                        decimal reusult = validations.ConvertStringToDecimal(txtBox.Text);
                    }
                    catch (Exception ex)
                    {
                        txtBox.BorderBrush = System.Windows.Media.Brushes.Red;
                        System.Windows.Forms.MessageBox.Show(ex.Message);
                        txtBox.Text = "";
                    }
                }
            }
        }

        private void txtPaymentesNumber_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (sender is TextBox)
            {
                TextBox txtBox = (TextBox)sender;
                txtBox.ClearValue(BorderBrushProperty);
                if (txtBox.Text != "")
                {
                    try
                    {
                        decimal reusult = validations.ConvertStringToInt(txtBox.Text);
                    }
                    catch (Exception ex)
                    {
                        txtBox.BorderBrush = System.Windows.Media.Brushes.Red;
                        System.Windows.Forms.MessageBox.Show(ex.Message);
                        txtBox.Text = "";
                    }
                }
            }
        }

        private void cbCompany_SelectionChanged_1(object sender, SelectionChangedEventArgs e)
        {
            if (cbCompany.SelectedItem == null)
            {
                cbPrincipalAgentNumber.SelectedItem = null;
                cbSecundaryAgentNumber.SelectedItem = null;
                cbPrincipalAgentNumber.IsEnabled = false;
                cbSecundaryAgentNumber.IsEnabled = false;
                return;
            }
            cbInsurenceTypeBinding();
            int insuranceID = insuranceLogic.GetInsuranceID("נסיעות לחו''ל");
            if (client != null)
            {
                cbPrincipalAgentNumber.ItemsSource = agentLogic.GetAgentNumbersByCompanyAndInsurance(client.PrincipalAgentID, (InsuranceCompany)cbCompany.SelectedItem, insuranceID);
                cbPrincipalAgentNumber.DisplayMemberPath = "Number";
                cbPrincipalAgentNumber.IsEnabled = true;
                cbPrincipalAgentNumber.SelectedIndex = 0;
                cbSecundaryAgentNumber.ItemsSource = agentLogic.GetAgentNumbersByCompanyAndInsurance(client.SecundaryAgentID, (InsuranceCompany)cbCompany.SelectedItem, insuranceID);
                cbSecundaryAgentNumber.DisplayMemberPath = "Number";
                cbSecundaryAgentNumber.IsEnabled = true;
                cbSecundaryAgentNumber.SelectedIndex = 0;
            }
            tbItemCoverage.Visibility = Visibility.Visible;
        }

        private void cbInsuranceTypeInsured1_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cbInsuranceTypeInsured1.SelectedItem != null)
            {
                TravelInsuranceType type = (TravelInsuranceType)cbInsuranceTypeInsured1.SelectedItem;

                chbWinterSport1.IsChecked = type.IsWinterSport;
                chbExtremeSport1.IsChecked = type.IsExtremeSport;
                chbExistingMedicalCondition1.IsChecked = type.IsExistingMedicalCondition;
                chbSearchAndRescue1.IsChecked = type.IsSearchAndRescue;
                chbPregnancy1.IsChecked = type.IsPregnancy;
                chbBaggage1.IsChecked = type.IsBaggage;
                chbBicycle1.IsChecked = type.IsBicycle;
                chbComputerOrTablet1.IsChecked = type.IsComputerOrTablet;
                chbPhoneOrGPS1.IsChecked = type.IsPhoneOrGPS;
                chbOtherInsuranceDetails1.IsChecked = type.IsOther;
                txtOtherName1.Text = type.OtherName;
            }
        }

        private void cbInsuranceTypeInsured2_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cbInsuranceTypeInsured2.SelectedItem != null)
            {
                TravelInsuranceType type = (TravelInsuranceType)cbInsuranceTypeInsured2.SelectedItem;

                chbWinterSport2.IsChecked = type.IsWinterSport;
                chbExtremeSport2.IsChecked = type.IsExtremeSport;
                chbExistingMedicalCondition2.IsChecked = type.IsExistingMedicalCondition;
                chbSearchAndRescue2.IsChecked = type.IsSearchAndRescue;
                chbPregnancy2.IsChecked = type.IsPregnancy;
                chbBaggage2.IsChecked = type.IsBaggage;
                chbBicycle2.IsChecked = type.IsBicycle;
                chbComputerOrTablet2.IsChecked = type.IsComputerOrTablet;
                chbPhoneOrGPS2.IsChecked = type.IsPhoneOrGPS;
                chbOtherInsuranceDetails2.IsChecked = type.IsOther;
                txtOtherName2.Text = type.OtherName;
            }
        }

        private void cbInsuranceTypeInsured3_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cbInsuranceTypeInsured3.SelectedItem != null)
            {
                TravelInsuranceType type = (TravelInsuranceType)cbInsuranceTypeInsured3.SelectedItem;

                chbWinterSport3.IsChecked = type.IsWinterSport;
                chbExtremeSport3.IsChecked = type.IsExtremeSport;
                chbExistingMedicalCondition3.IsChecked = type.IsExistingMedicalCondition;
                chbSearchAndRescue3.IsChecked = type.IsSearchAndRescue;
                chbPregnancy3.IsChecked = type.IsPregnancy;
                chbBaggage3.IsChecked = type.IsBaggage;
                chbBicycle3.IsChecked = type.IsBicycle;
                chbComputerOrTablet3.IsChecked = type.IsComputerOrTablet;
                chbPhoneOrGPS3.IsChecked = type.IsPhoneOrGPS;
                chbOtherInsuranceDetails3.IsChecked = type.IsOther;
                txtOtherName3.Text = type.OtherName;
            }
        }

        private void cbInsuranceTypeInsured4_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cbInsuranceTypeInsured4.SelectedItem != null)
            {
                TravelInsuranceType type = (TravelInsuranceType)cbInsuranceTypeInsured4.SelectedItem;

                chbWinterSport4.IsChecked = type.IsWinterSport;
                chbExtremeSport4.IsChecked = type.IsExtremeSport;
                chbExistingMedicalCondition4.IsChecked = type.IsExistingMedicalCondition;
                chbSearchAndRescue4.IsChecked = type.IsSearchAndRescue;
                chbPregnancy4.IsChecked = type.IsPregnancy;
                chbBaggage4.IsChecked = type.IsBaggage;
                chbBicycle4.IsChecked = type.IsBicycle;
                chbComputerOrTablet4.IsChecked = type.IsComputerOrTablet;
                chbPhoneOrGPS4.IsChecked = type.IsPhoneOrGPS;
                chbOtherInsuranceDetails4.IsChecked = type.IsOther;
                txtOtherName4.Text = type.OtherName;
            }
        }

        private void cbInsuranceTypeInsured5_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cbInsuranceTypeInsured5.SelectedItem != null)
            {
                TravelInsuranceType type = (TravelInsuranceType)cbInsuranceTypeInsured5.SelectedItem;

                chbWinterSport5.IsChecked = type.IsWinterSport;
                chbExtremeSport5.IsChecked = type.IsExtremeSport;
                chbExistingMedicalCondition5.IsChecked = type.IsExistingMedicalCondition;
                chbSearchAndRescue5.IsChecked = type.IsSearchAndRescue;
                chbPregnancy5.IsChecked = type.IsPregnancy;
                chbBaggage5.IsChecked = type.IsBaggage;
                chbBicycle5.IsChecked = type.IsBicycle;
                chbComputerOrTablet5.IsChecked = type.IsComputerOrTablet;
                chbPhoneOrGPS5.IsChecked = type.IsPhoneOrGPS;
                chbOtherInsuranceDetails5.IsChecked = type.IsOther;
                txtOtherName5.Text = type.OtherName;
            }
        }

        private void cbInsuranceTypeInsured6_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cbInsuranceTypeInsured6.SelectedItem != null)
            {
                TravelInsuranceType type = (TravelInsuranceType)cbInsuranceTypeInsured6.SelectedItem;

                chbWinterSport6.IsChecked = type.IsWinterSport;
                chbExtremeSport6.IsChecked = type.IsExtremeSport;
                chbExistingMedicalCondition6.IsChecked = type.IsExistingMedicalCondition;
                chbSearchAndRescue6.IsChecked = type.IsSearchAndRescue;
                chbPregnancy6.IsChecked = type.IsPregnancy;
                chbBaggage6.IsChecked = type.IsBaggage;
                chbBicycle6.IsChecked = type.IsBicycle;
                chbComputerOrTablet6.IsChecked = type.IsComputerOrTablet;
                chbPhoneOrGPS6.IsChecked = type.IsPhoneOrGPS;
                chbOtherInsuranceDetails6.IsChecked = type.IsOther;
                txtOtherName6.Text = type.OtherName;
            }
        }

        private void cbInsuranceTypeInsured7_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cbInsuranceTypeInsured7.SelectedItem != null)
            {
                TravelInsuranceType type = (TravelInsuranceType)cbInsuranceTypeInsured7.SelectedItem;

                chbWinterSport7.IsChecked = type.IsWinterSport;
                chbExtremeSport7.IsChecked = type.IsExtremeSport;
                chbExistingMedicalCondition7.IsChecked = type.IsExistingMedicalCondition;
                chbSearchAndRescue7.IsChecked = type.IsSearchAndRescue;
                chbPregnancy7.IsChecked = type.IsPregnancy;
                chbBaggage7.IsChecked = type.IsBaggage;
                chbBicycle7.IsChecked = type.IsBicycle;
                chbComputerOrTablet7.IsChecked = type.IsComputerOrTablet;
                chbPhoneOrGPS7.IsChecked = type.IsPhoneOrGPS;
                chbOtherInsuranceDetails7.IsChecked = type.IsOther;
                txtOtherName7.Text = type.OtherName;
            }
        }

        private void cbInsuranceTypeInsured8_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cbInsuranceTypeInsured8.SelectedItem != null)
            {
                TravelInsuranceType type = (TravelInsuranceType)cbInsuranceTypeInsured8.SelectedItem;

                chbWinterSport8.IsChecked = type.IsWinterSport;
                chbExtremeSport8.IsChecked = type.IsExtremeSport;
                chbExistingMedicalCondition8.IsChecked = type.IsExistingMedicalCondition;
                chbSearchAndRescue8.IsChecked = type.IsSearchAndRescue;
                chbPregnancy8.IsChecked = type.IsPregnancy;
                chbBaggage8.IsChecked = type.IsBaggage;
                chbBicycle8.IsChecked = type.IsBicycle;
                chbComputerOrTablet8.IsChecked = type.IsComputerOrTablet;
                chbPhoneOrGPS8.IsChecked = type.IsPhoneOrGPS;
                chbOtherInsuranceDetails8.IsChecked = type.IsOther;
                txtOtherName8.Text = type.OtherName;
            }
        }

        private void cbInsuranceTypeInsured9_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cbInsuranceTypeInsured9.SelectedItem != null)
            {
                TravelInsuranceType type = (TravelInsuranceType)cbInsuranceTypeInsured9.SelectedItem;

                chbWinterSport9.IsChecked = type.IsWinterSport;
                chbExtremeSport9.IsChecked = type.IsExtremeSport;
                chbExistingMedicalCondition9.IsChecked = type.IsExistingMedicalCondition;
                chbSearchAndRescue9.IsChecked = type.IsSearchAndRescue;
                chbPregnancy9.IsChecked = type.IsPregnancy;
                chbBaggage9.IsChecked = type.IsBaggage;
                chbBicycle9.IsChecked = type.IsBicycle;
                chbComputerOrTablet9.IsChecked = type.IsComputerOrTablet;
                chbPhoneOrGPS9.IsChecked = type.IsPhoneOrGPS;
                chbOtherInsuranceDetails9.IsChecked = type.IsOther;
                txtOtherName9.Text = type.OtherName;
            }
        }

        private void cbInsuranceTypeInsured10_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cbInsuranceTypeInsured10.SelectedItem != null)
            {
                TravelInsuranceType type = (TravelInsuranceType)cbInsuranceTypeInsured10.SelectedItem;

                chbWinterSport10.IsChecked = type.IsWinterSport;
                chbExtremeSport10.IsChecked = type.IsExtremeSport;
                chbExistingMedicalCondition10.IsChecked = type.IsExistingMedicalCondition;
                chbSearchAndRescue10.IsChecked = type.IsSearchAndRescue;
                chbPregnancy10.IsChecked = type.IsPregnancy;
                chbBaggage10.IsChecked = type.IsBaggage;
                chbBicycle10.IsChecked = type.IsBicycle;
                chbComputerOrTablet10.IsChecked = type.IsComputerOrTablet;
                chbPhoneOrGPS10.IsChecked = type.IsPhoneOrGPS;
                chbOtherInsuranceDetails10.IsChecked = type.IsOther;
                txtOtherName10.Text = type.OtherName;
            }
        }

        private void GroupBox_GotFocus(object sender, RoutedEventArgs e)
        {
            isChanges = true;
        }

        private void dgTrackings_MouseDown(object sender, MouseButtonEventArgs e)
        {
            dgTrackings.UnselectAll();
        }

        private void dgCoverages_MouseDown(object sender, MouseButtonEventArgs e)
        {
            dgCoverages.UnselectAll();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (confirmBeforeClosing && isChanges)
            {
                if (MessageBox.Show("האם ברצונך לשמור שינוים שבצעת", "אישור", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    btnSave_Click(sender, new RoutedEventArgs());
                    if (cancelClose)
                    {
                        e.Cancel = true;
                        cancelClose = false;
                    }
                }
            }
        }

        private void chbStandardMoneyCollection_Checked(object sender, RoutedEventArgs e)
        {
            btnStandardMoneyCollectionDetails.IsEnabled = true;
        }
        private void chbStandardMoneyCollection_Unchecked(object sender, RoutedEventArgs e)
        {
            btnStandardMoneyCollectionDetails.IsEnabled = false;
        }
        private void btnStandardMoneyCollectionDetails_Click(object sender, RoutedEventArgs e)
        {
            frmStandardMoneyCollectionDetails window = null;
            if (standardMoneyCollection == null)
            {
                window = new frmStandardMoneyCollectionDetails(false, user);
            }
            else
            {
                window = new frmStandardMoneyCollectionDetails(standardMoneyCollection, standardMoneyCollectionTrackings, false, user);
            }
            window.ShowDialog();
            if (window.StandardMoneyCollection != null)
            {
                standardMoneyCollection = window.StandardMoneyCollection;
            }
            if (window.Trackings != null)
            {
                standardMoneyCollectionTrackings = window.Trackings;
            }
        }


    }
}
