﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using CoralBusinessLogics;
using System.IO;

namespace CoralUI
{
    public class AnnualDeposits
    {
        public DateTime Year { get; set; }
        public decimal TotalDepositsAmount { get; set; }
    }
    /// <summary>
    /// Interaction logic for frmFinance.xaml
    /// </summary>
    public partial class frmFinance : Window
    {
        Client client = null;
        User user = null;
        string path = null;
        string newPath = null;
        string clientDirPath = null;
        string financePath = null;
        FinanceIndustryLogic financeIndustryLogic = new FinanceIndustryLogic();
        InsurancesLogic insuranceLogic = new InsurancesLogic();
        PolicyHoldersLogic policyHoldersLogic = new PolicyHoldersLogic();
        AgentsLogic agentLogic = new AgentsLogic();
        InputsValidations validation = new InputsValidations();
        List<object> nullErrorList = null;
        int policyId;
        FinancePoliciesLogic financeLogic = new FinancePoliciesLogic();
        private bool isAddition;
        private FinancePolicy financePolicy;
        FundsLogic fundsLogics = new FundsLogic();
        FinancePolicyTransfer transferSelected = null;
        //decimal transferingAmount = 0;
        List<FinancePolicyTransfer> transfers = new List<FinancePolicyTransfer>();
        ListViewSettings lv = new ListViewSettings();
        List<FinancePolicyMonthlyDeposit> deposits = new List<FinancePolicyMonthlyDeposit>();
        List<AnnualDeposits> annualDeposits = new List<AnnualDeposits>();
        List<FinanceTracking> trackings = new List<FinanceTracking>();
        TrackingsLogics trackingLogics = new TrackingsLogics();
        bool isOffer;
        private bool isChanges = false;
        bool confirmBeforeClosing = true;
        bool cancelClose = false;
        StandardMoneyCollection standardMoneyCollection;
        StandardMoneyCollectionLogic moneyCollectionLogic = new StandardMoneyCollectionLogic();
        private List<StandardMoneyCollectionTracking> standardMoneyCollectionTrackings;


        public frmFinance(Client clientToAddPolicy, User user, bool isOffer)
        {
            InitializeComponent();
            client = clientToAddPolicy;
            this.user = user;
            this.isOffer = isOffer;
        }

        public frmFinance(Client client, User user, bool isAddition, FinancePolicy financePolicy)
        {
            InitializeComponent();
            this.client = client;
            this.user = user;
            this.isAddition = isAddition;
            this.financePolicy = financePolicy;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            string strAppDir = System.AppDomain.CurrentDomain.BaseDirectory;
            string path = strAppDir + @"Coral Files\ClientsPath.txt";

            if (File.Exists(path))
            {
                clientDirPath = File.ReadAllText(path);
            }
            else
            {
                MessageBox.Show("לא נמצא תיקיית הלקוחות. נא בדוק בהגדרות ראשוניות את נתיב התיקייה");
                Close();
                return;
            }
            financePath = clientDirPath + @"\" + client.ClientID;
            cbProgramTypeBinding();
            cbCompanyBinding();
            cbPolicyHolderBinding();
            FundsBinding();
            dpDepositDate.SelectedDate = DateTime.Now;
            dtxtForMonth.Value = DateTime.Now;
            if (client != null)
            {
                string clientDetails;
                if (client.CellPhone != null && client.CellPhone != "")
                {
                    clientDetails = string.Format("{0} {1}  ת.ז: {2} נייד: {3}", client.FirstName, client.LastName, client.IdNumber, client.CellPhone);
                }
                else
                {
                    clientDetails = string.Format("{0} {1}  ת.ז: {2}", client.FirstName, client.LastName, client.IdNumber);
                }
                lblClientNameAndId.Content = clientDetails;
            }
            if (financePolicy == null)
            {
                DateTime now = DateTime.Now;
                dpOpenDate.SelectedDate = now;
                dpStartDate.SelectedDate = now;
                dpEmployeeStartDate.SelectedDate = now;
                dpIndependentStartDate.SelectedDate = now;
                txtAddition.Text = "0";
            }
            else
            {
                dpOpenDate.SelectedDate = financePolicy.OpenDate;
                dpStartDate.SelectedDate = financePolicy.StartDate;
                dpEndDate.SelectedDate = financePolicy.EndDate;
                txtAssociateNumber.Text = financePolicy.AssociateNumber;
                isOffer = financePolicy.IsOffer;
                txtAddition.Text = financePolicy.Addition.ToString();
                SelectItemInCb((int)financePolicy.CompanyID, cbCompany);
                txtEmployerIdNumber.Text = financePolicy.PolicyOwnerIdNumber;
                txtEmployerName.Text = financePolicy.ManagerName;
                txtEmployerAddress.Text = financePolicy.PolicyOwnerAddress;
                txtEmployerPhone.Text = financePolicy.PolicyOwnerPhone;
                txtEmployerFax.Text = financePolicy.PolicyOwnerFax;
                txtEmployerCellPhone.Text = financePolicy.PolicyOwnerCellphone;
                txtEmployerEmail.Text = financePolicy.PolicyOwnerEmail;
                if (financePolicy.ReportedSalary != null)
                {
                    txtSalary.Text = ((decimal)financePolicy.ReportedSalary).ToString("0.##");
                }
                dpEmployeeStartDate.SelectedDate = financePolicy.StartDateEmployee;
                if (financePolicy.AdministrationFeesEmployee != null)
                {
                    txtEmployeeAdministrationFees.Text = ((decimal)financePolicy.AdministrationFeesEmployee).ToString("0.##");
                }
                if (financePolicy.AggregateFeesEmployee != null)
                {
                    txtEmployeeAggregateFee.Text = ((decimal)financePolicy.AggregateFeesEmployee).ToString("0.##");
                }
                if (financePolicy.WorkerDeposit != null)
                {
                    txtWorkerDeposit.Text = ((decimal)financePolicy.WorkerDeposit).ToString("0.##");
                }
                if (financePolicy.EmployerDeposit != null)
                {
                    txtEmployerDeposit.Text = ((decimal)financePolicy.EmployerDeposit).ToString("0.##");
                }
                if (financePolicy.Compansation != null)
                {
                    txtCompensation.Text = ((decimal)financePolicy.Compansation).ToString("0.##");
                }
                if (financePolicy.DepositAmountIndependent != null)
                {
                    txtIndependentDepositAmount.Text = ((decimal)financePolicy.DepositAmountIndependent).ToString("0.##");
                }
                if (financePolicy.DepositAmountEmployee != null)
                {
                    txtEmployeeDepositAmount.Text = ((decimal)financePolicy.DepositAmountEmployee).ToString("0.##");
                }
                dpIndependentStartDate.SelectedDate = financePolicy.StartDateIndependent;
                if (financePolicy.AdministrationFeesIndependent != null)
                {
                    txtIndependentAdministrationFees.Text = ((decimal)financePolicy.AdministrationFeesIndependent).ToString("0.##");
                }
                if (financePolicy.AggregateFeesIndependent != null)
                {
                    txtIndependentAggregateFee.Text = ((decimal)financePolicy.AggregateFeesIndependent).ToString("0.##");
                }
                txtComments.Text = financePolicy.Comments;
                var principalAgentNumbers = cbPrimaryAgentNumber.Items;
                foreach (var item in principalAgentNumbers)
                {
                    AgentNumber principalAgentNumber = (AgentNumber)item;
                    if (principalAgentNumber.AgentNumberID == financePolicy.PrincipalAgentNumberID)
                    {
                        cbPrimaryAgentNumber.SelectedItem = item;
                        break;
                    }
                }
                var secundaryAgentNumbers = cbSecundaryAgentNumber.Items;
                foreach (var item in secundaryAgentNumbers)
                {
                    AgentNumber secundaryAgentNumber = (AgentNumber)item;
                    if (secundaryAgentNumber.AgentNumberID == financePolicy.SubAgentNumberID)
                    {
                        cbSecundaryAgentNumber.SelectedItem = item;
                        break;
                    }
                }
                var paymentMethods = cbPaymentMethod.Items;
                foreach (var item in paymentMethods)
                {
                    if (financePolicy.PaymentMethod == ((ComboBoxItem)item).Content.ToString())
                    {
                        cbPaymentMethod.SelectedItem = item;
                        break;
                    }
                }
                var programTypes = cbProgramType.Items;
                foreach (var item in programTypes)
                {
                    FinanceProgramType programType = (FinanceProgramType)item;
                    if (programType.ProgramTypeID == financePolicy.ProgramTypeID)
                    {
                        cbProgramType.SelectedItem = item;
                        if (programType.ProgramTypeName != "שגויים")
                        {
                            cbProgramType.IsEnabled = false;
                        }
                        break;
                    }
                }

                var policyHolders = cbPolicyHolder.Items;
                foreach (var item in policyHolders)
                {
                    PolicyOwner holder = (PolicyOwner)item;
                    if (holder.PolicyOwnerID == financePolicy.PolicyOwnerID)
                    {
                        cbPolicyHolder.SelectedItem = item;
                        break;
                    }
                }
                if (rbEmployee.Content.ToString() == financePolicy.AssociateStatus)
                {
                    rbEmployee.IsChecked = true;
                }
                else if (rbIndependent.Content.ToString() == financePolicy.AssociateStatus)
                {
                    rbIndependent.IsChecked = true;
                }
                else if (rbShareholderEmployee.Content.ToString() == financePolicy.AssociateStatus)
                {
                    rbShareholderEmployee.IsChecked = true;
                }
                else if (rbKibutzMember.Content.ToString() == financePolicy.AssociateStatus)
                {
                    rbKibutzMember.IsChecked = true;
                }
                transfers = financeLogic.GetTransfersByPolicy(financePolicy.FinancePolicyID);
                dgTransferencesBinding();
                deposits = financeLogic.GetMonthlyDepositsByPolicy(financePolicy.FinancePolicyID);
                foreach (var item in deposits)
                {
                    var annualDepositToUpdate = annualDeposits.FirstOrDefault(d => d.Year.Year == ((DateTime)item.DepositForTheMonth).Year);
                    if (annualDepositToUpdate != null)
                    {
                        annualDepositToUpdate.TotalDepositsAmount = annualDepositToUpdate.TotalDepositsAmount + (decimal)item.DepositAmount;
                    }
                    else
                    {
                        AnnualDeposits newAnnuealDeposit = new AnnualDeposits() { Year = (DateTime)item.DepositForTheMonth, TotalDepositsAmount = (decimal)item.DepositAmount };
                        annualDeposits.Add(newAnnuealDeposit);
                    }
                }
                dgMonthlyDepositsBinding();
                trackings = trackingLogics.GetAllFinanceTrackingsByPolicy(financePolicy.FinancePolicyID);
                dgTrackingsBinding();
                standardMoneyCollection = moneyCollectionLogic.GetStandardMoneyColletionByPolicy(financePolicy);
                if (standardMoneyCollection != null)
                {
                    chbStandardMoneyCollection.IsChecked = true;
                    chbStandardMoneyCollection.IsEnabled = false;
                    standardMoneyCollectionTrackings = moneyCollectionLogic.GetTrakcingsByStsndardMoneyCollection(standardMoneyCollection);
                }
            }
        }

        private void dgMonthlyDepositsBinding()
        {
            dgMonthlyDeposits.UnselectAll();
            dgAnnualDeposits.UnselectAll();
            dgMonthlyDeposits.ItemsSource = deposits.OrderByDescending(d => d.DepositForTheMonth).ToList();
            dgAnnualDeposits.ItemsSource = annualDeposits.OrderByDescending(d => d.Year).ToList();
        }

        private void FundsBinding()
        {
            int? transferringId = null;
            if (cbTransferringFund.SelectedItem != null)
            {
                transferringId = ((InsuranceCompany)cbTransferringFund.SelectedItem).CompanyID;
            }
            int? transferedId = null;
            if (cbTransferedTo.SelectedItem != null)
            {
                transferedId = ((InsuranceCompany)cbTransferedTo.SelectedItem).CompanyID;
            }
            var funds = insuranceLogic.GetActiveCompaniesByInsurance("פיננסים");
            cbTransferringFund.ItemsSource = funds;
            cbTransferringFund.DisplayMemberPath = "Company.CompanyName";
            cbTransferedTo.ItemsSource = funds;
            cbTransferedTo.DisplayMemberPath = "Company.CompanyName";
            if (transferringId != null)
            {
                SelectItemInCb((int)transferringId, cbTransferringFund);
            }
            if (transferedId != null)
            {
                SelectItemInCb((int)transferedId, cbTransferedTo);
            }
        }

        private void cbPolicyHolderBinding()
        {
            cbPolicyHolder.ItemsSource = policyHoldersLogic.GetAllPolicyHolders();
            cbPolicyHolder.DisplayMemberPath = "PolicyOwnerName";
        }

        private void cbCompanyBinding()
        {
            cbCompany.ItemsSource = insuranceLogic.GetActiveCompaniesByInsurance("פיננסים");
            cbCompany.DisplayMemberPath = "Company.CompanyName";
        }

        private void cbProgramTypeBinding()
        {
            cbProgramType.ItemsSource = financeIndustryLogic.GetAllFinanceProgramTypes();
            cbProgramType.DisplayMemberPath = "ProgramTypeName";
        }

        private void rbIndependent_Checked(object sender, RoutedEventArgs e)
        {
            spDepositsIndependent.IsEnabled = true;
            spDepositsEmployee.IsEnabled = false;
            spEmployerDetails.IsEnabled = false;
            ClearEmployeeImputs();
        }

        private void rbEmployee_Checked(object sender, RoutedEventArgs e)
        {
            spDepositsIndependent.IsEnabled = false;
            spDepositsEmployee.IsEnabled = true;
            spEmployerDetails.IsEnabled = true;
            cbPolicyHolderNumber.IsEnabled = false;
            ClearIndependentInputs();
        }

        private void btnUpdaeCompaniesTable_Click(object sender, RoutedEventArgs e)
        {
            int? selectedItemId = null;
            if (cbCompany.SelectedItem != null)
            {
                selectedItemId = ((InsuranceCompany)cbCompany.SelectedItem).CompanyID;
            }
            int insuranceID = insuranceLogic.GetInsuranceID("פיננסים");
            InsuranceCompany company = new InsuranceCompany() { InsuranceID = insuranceID };
            frmUpdateTable updateCompaniesTable = new frmUpdateTable(company);
            updateCompaniesTable.ShowDialog();
            cbCompanyBinding();
            if (updateCompaniesTable.ItemAdded != null)
            {
                SelectItemInCb(((InsuranceCompany)updateCompaniesTable.ItemAdded).CompanyID, cbCompany);
            }
            else if (selectedItemId != null)
            {
                SelectItemInCb((int)selectedItemId, cbCompany);
            }
        }

        private void SelectItemInCb(int id, ComboBox cb)
        {
            var items = cb.Items;
            foreach (var item in items)
            {
                InsuranceCompany parsedItem = (InsuranceCompany)item;
                if (parsedItem.CompanyID == id)
                {
                    cb.SelectedItem = item;
                    break;
                }
            }
        }

        private void btnUpdateProgramTable_Click(object sender, RoutedEventArgs e)
        {
            int? selectedItemId = null;
            if (cbProgram.SelectedItem != null)
            {
                selectedItemId = ((FinanceProgram)cbProgram.SelectedItem).ProgramID;
            }
            //int insuranceID = insuranceLogic.GetInsuranceID("חיים");
            FinanceProgram program = new FinanceProgram() { ProgramTypeID = ((FinanceProgramType)cbProgramType.SelectedItem).ProgramTypeID, CompanyID = ((InsuranceCompany)cbCompany.SelectedItem).CompanyID };
            frmUpdateTable updateProgramTable = new frmUpdateTable(program);
            updateProgramTable.ShowDialog();
            cbProgramBinding();
            //TransferringFundsTypeAdmBinding();
            //TransferedFundsTypeAdmBinding();
            if (updateProgramTable.ItemAdded != null)
            {
                SelectProgramItemInCb(((FinanceProgram)updateProgramTable.ItemAdded).ProgramID, cbProgram);
            }
            else if (selectedItemId != null)
            {
                SelectProgramItemInCb((int)selectedItemId, cbProgram);
            }
        }

        private void SelectProgramItemInCb(int id, ComboBox cb)
        {
            var items = cb.Items;
            foreach (var item in items)
            {
                FinanceProgram parsedItem = (FinanceProgram)item;
                if (parsedItem.ProgramID == id)
                {
                    cb.SelectedItem = item;
                    break;
                }
            }
        }

        private void cbProgramBinding()
        {
            cbProgram.ItemsSource = financeIndustryLogic.GetActiveFinanceProgramsByProgramType(((FinanceProgramType)cbProgramType.SelectedItem).ProgramTypeID, ((InsuranceCompany)cbCompany.SelectedItem).CompanyID);
            cbProgram.DisplayMemberPath = "ProgramName";
        }

        private void cbProgramType_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //LifeIndustry industrySelected = (LifeIndustry)cbLifeIndustry.SelectedItem;
            if ((FinanceProgramType)cbProgramType.SelectedItem == null)
            {
                spTransfers.IsEnabled = false;
                return;
            }
            if (cbCompany.SelectedItem != null)
            {
                cbProgram.IsEnabled = true;
                btnUpdateProgramTable.IsEnabled = true;
                cbProgramBinding();
                if (financePolicy != null)
                {
                    SelectProgramItemInCb((int)financePolicy.ProgramID, cbProgram);
                }
            }
            if (((FinanceProgramType)cbProgramType.SelectedItem).ProgramTypeName == "תיק השקעות")
            {
                rbEmployee.IsChecked = false;
                rbIndependent.IsChecked = false;
                rbKibutzMember.IsChecked = false;
                rbShareholderEmployee.IsChecked = false;
                spAssociateStatus.IsEnabled = false;
                spDepositsEmployee.IsEnabled = false;
                spDepositsIndependent.IsEnabled = false;
                spEmployerDetails.IsEnabled = false;
                ClearEmployeeImputs();
                ClearIndependentInputs();
            }
            else
            {
                spAssociateStatus.IsEnabled = true;
            }
            spTransfers.IsEnabled = true;
            cbTransferedToFundType.IsEnabled = false;
            btnUpdateFundTypeTable.IsEnabled = false;
            cbFundType.IsEnabled = false;
            btnUpdateTransferedToFundTypeTable.IsEnabled = false;
        }

        private void ClearEmployeeImputs()
        {
            cbPolicyHolder.SelectedItem = null;
            cbPolicyHolderNumber.SelectedItem = null;
            txtEmployerIdNumber.Clear();
            txtEmployerName.Clear();
            txtEmployerAddress.Clear();
            txtEmployerPhone.Clear();
            txtEmployerFax.Clear();
            txtEmployerCellPhone.Clear();
            txtEmployerEmail.Clear();
            txtSalary.Clear();
            dpEmployeeStartDate.SelectedDate = null;
            //dpIndependentStartDate.SelectedDate = null;
            txtEmployeeAdministrationFees.Clear();
            txtEmployeeAggregateFee.Clear();
            txtWorkerDeposit.Clear();
            txtEmployerDeposit.Clear();
            txtCompensation.Clear();
        }

        private void ClearIndependentInputs()
        {
            txtIndependentDepositAmount.Clear();
            dpIndependentStartDate.SelectedDate = null;
            txtIndependentAdministrationFees.Clear();
            txtIndependentAggregateFee.Clear();
            cbPaymentMethod.SelectedItem = null;
        }


        private void cbCompany_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cbCompany.SelectedItem == null)
            {
                cbPrimaryAgentNumber.SelectedItem = null;
                cbSecundaryAgentNumber.SelectedItem = null;
                cbPrimaryAgentNumber.IsEnabled = false;
                cbSecundaryAgentNumber.IsEnabled = false;
                return;
            }
            if (cbProgramType.SelectedItem != null)
            {
                cbProgram.IsEnabled = true;
                btnUpdateProgramTable.IsEnabled = true;
                cbProgramBinding();
                if (financePolicy != null)
                {
                    //var insuranceTypes = cbInsuranceType.Items;
                    //foreach (var item in insuranceTypes)
                    //{
                    //    FundType insuranceType = (FundType)item;
                    //    if (insuranceType.FundTypeID == lifePolicy.FundTypeID)
                    //    {
                    //        cbInsuranceType.SelectedItem = item;
                    //        break;
                    //    }
                    //}
                    SelectProgramItemInCb((int)financePolicy.ProgramID, cbProgram);
                }
            }
            int insuranceID = insuranceLogic.GetInsuranceID("פיננסים");
            if (client != null)
            {
                cbPrimaryAgentNumber.ItemsSource = agentLogic.GetAgentNumbersByCompanyAndInsurance(client.PrincipalAgentID, (InsuranceCompany)cbCompany.SelectedItem, insuranceID);
                cbPrimaryAgentNumber.DisplayMemberPath = "Number";
                cbPrimaryAgentNumber.IsEnabled = true;
                cbPrimaryAgentNumber.SelectedIndex = 0;
                cbSecundaryAgentNumber.ItemsSource = agentLogic.GetAgentNumbersByCompanyAndInsurance(client.SecundaryAgentID, (InsuranceCompany)cbCompany.SelectedItem, insuranceID);
                cbSecundaryAgentNumber.DisplayMemberPath = "Number";
                cbSecundaryAgentNumber.IsEnabled = true;
                cbSecundaryAgentNumber.SelectedIndex = 0;
            }
            if (cbCompany.SelectedItem != null && cbPolicyHolder.SelectedItem != null)
            {
                //cbPolicyHolderNumber.IsEnabled = true;
                //cbPolicyHolderNumber.ItemsSource = policyHoldersLogic.GetNumbersByPolicyHolderAndCompany(((PolicyOwner)cbPolicyHolder.SelectedItem).PolicyOwnerID, ((InsuranceCompany)cbCompany.SelectedItem).CompanyID, insuranceID);
                //cbPolicyHolderNumber.DisplayMemberPath = "Number";
                //cbPolicyHolderNumber.SelectedIndex = 0;
                DisplayPolicyOwnerDetails(insuranceID);
            }
            else
            {
                cbPolicyHolderNumber.SelectedItem = null;
                cbPolicyHolderNumber.IsEnabled = false;
            }
        }

        private void txtSalary_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (sender is TextBox)
            {
                TextBox txtBox = (TextBox)sender;
                txtBox.ClearValue(BorderBrushProperty);
                if (txtBox.Text != "")
                {
                    try
                    {
                        decimal reusult = validation.ConvertStringToDecimal(txtBox.Text);
                    }
                    catch (Exception ex)
                    {
                        txtBox.BorderBrush = System.Windows.Media.Brushes.Red;
                        System.Windows.Forms.MessageBox.Show(ex.Message);
                        txtBox.Text = "";
                    }
                }
            }
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            if (nullErrorList != null && nullErrorList.Count > 0)
            {
                foreach (var item in nullErrorList)
                {
                    if (item is TextBox)
                    {
                        ((TextBox)item).ClearValue(BorderBrushProperty);
                    }
                    else if (item is ComboBox)
                    {
                        ((ComboBox)item).ClearValue(BorderBrushProperty);
                    }
                    else if (item is DatePicker)
                    {
                        ((DatePicker)item).ClearValue(BorderBrushProperty);
                    }
                }
            }
            if (!validation.IsDigitsOnly(txtAssociateNumber.Text))
            {
                cancelClose = true;
                MessageBox.Show("מס' פוליסה יכול להכיל מספרים בלבד", "", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            nullErrorList = validation.InputNullValidation(new object[] { txtAssociateNumber, txtAddition, cbProgramType, cbCompany, cbProgram, dpStartDate, dpEndDate });
            if (nullErrorList.Count > 0)
            {
                cancelClose = true;
                tbItemGeneral.Focus();
                MessageBox.Show("נא למלאות שדות חובה", "", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (dpEndDate.SelectedDate <= dpStartDate.SelectedDate)
            {
                cancelClose = true;
                tbItemGeneral.Focus();
                MessageBox.Show("תאריך סיום חייב להיות גדול מתאריך תחילה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            int addition = Convert.ToInt32(txtAddition.Text);
            int? principalAgentNumberID = null;
            if ((AgentNumber)cbPrimaryAgentNumber.SelectedItem != null)
            {
                principalAgentNumberID = ((AgentNumber)cbPrimaryAgentNumber.SelectedItem).AgentNumberID;
            }
            int? subAgentNumberID = null;
            if ((AgentNumber)cbSecundaryAgentNumber.SelectedItem != null)
            {
                subAgentNumberID = ((AgentNumber)cbSecundaryAgentNumber.SelectedItem).AgentNumberID;
            }
            int? policyOwnerID = null;
            if ((PolicyOwner)cbPolicyHolder.SelectedItem != null)
            {
                policyOwnerID = ((PolicyOwner)cbPolicyHolder.SelectedItem).PolicyOwnerID;
            }
            int? factoryNumberID = null;
            if ((FactoryNumber)cbPolicyHolderNumber.SelectedItem != null)
            {
                factoryNumberID = ((FactoryNumber)cbPolicyHolderNumber.SelectedItem).FactoryNumberID;
            }
            string paymentMode = null;
            if (cbPaymentMethod.SelectedItem != null)
            {
                paymentMode = ((ComboBoxItem)cbPaymentMethod.SelectedItem).Content.ToString();
            }
            decimal? admFeesEmployee = null;
            if (txtEmployeeAdministrationFees.Text != "")
            {
                admFeesEmployee = decimal.Parse(txtEmployeeAdministrationFees.Text);
            }
            decimal? aggregateFeesEmployee = null;
            if (txtEmployeeAggregateFee.Text != "")
            {
                aggregateFeesEmployee = decimal.Parse(txtEmployeeAggregateFee.Text);
            }
            decimal? admFeesIndependent = null;
            if (txtIndependentAdministrationFees.Text != "")
            {
                admFeesIndependent = decimal.Parse(txtIndependentAdministrationFees.Text);
            }
            decimal? aggregateFeesIndependent = null;
            if (txtIndependentAggregateFee.Text != "")
            {
                aggregateFeesIndependent = decimal.Parse(txtIndependentAggregateFee.Text);
            }
            decimal? workerDeposit = null;
            if (txtWorkerDeposit.Text != "")
            {
                workerDeposit = decimal.Parse(txtWorkerDeposit.Text);
            }
            decimal? reportedSalary = null;
            if (txtSalary.Text != "")
            {
                reportedSalary = decimal.Parse(txtSalary.Text);
            }
            decimal? employerDeposit = null;
            if (txtEmployerDeposit.Text != "")
            {
                employerDeposit = decimal.Parse(txtEmployerDeposit.Text);
            }
            decimal? independentDepositAmount = null;
            if (txtIndependentDepositAmount.Text != "")
            {
                independentDepositAmount = decimal.Parse(txtIndependentDepositAmount.Text);
            }
            decimal? employeeDepositAmount = null;
            if (txtEmployeeDepositAmount.Text != "")
            {
                employeeDepositAmount = decimal.Parse(txtEmployeeDepositAmount.Text);
            }
            decimal? compensation = null;
            if (txtCompensation.Text != "")
            {
                compensation = decimal.Parse(txtCompensation.Text);
            }
            string associateStatus = "";
            if (rbEmployee.IsChecked == true)
            {
                associateStatus = rbEmployee.Content.ToString();
            }
            else if (rbIndependent.IsChecked == true)
            {
                associateStatus = rbIndependent.Content.ToString();
            }
            else if (rbKibutzMember.IsChecked == true)
            {
                associateStatus = rbKibutzMember.Content.ToString();
            }
            else if (rbShareholderEmployee.IsChecked == true)
            {
                associateStatus = rbShareholderEmployee.Content.ToString();
            }
            try
            {
                if (financePolicy == null)
                {
                    policyId = financeLogic.InsertFinancePolicy(client.ClientID, isOffer, ((FinanceProgramType)cbProgramType.SelectedItem).ProgramTypeID, txtAssociateNumber.Text, addition, (DateTime)dpOpenDate.SelectedDate, dpStartDate.SelectedDate, dpEndDate.SelectedDate, ((InsuranceCompany)cbCompany.SelectedItem).CompanyID, principalAgentNumberID, subAgentNumberID, ((FinanceProgram)cbProgram.SelectedItem).ProgramID, policyOwnerID, factoryNumberID, paymentMode, txtComments.Text, admFeesEmployee, workerDeposit, dpIndependentStartDate.SelectedDate, dpEmployeeStartDate.SelectedDate, reportedSalary, txtEmployerPhone.Text, txtEmployerIdNumber.Text, txtEmployerFax.Text, txtEmployerEmail.Text, txtEmployerCellPhone.Text, txtEmployerAddress.Text, txtEmployerName.Text, employerDeposit, independentDepositAmount, employeeDepositAmount, compensation, associateStatus, aggregateFeesIndependent, aggregateFeesEmployee, admFeesIndependent);
                    if (!isOffer)
                    {
                        path = financePath + @"\פיננסים\" + txtAssociateNumber.Text + " " + ((FinanceProgramType)cbProgramType.SelectedItem).ProgramTypeName;
                    }
                    else
                    {
                        path = financePath + @"\הצעות\פיננסים\" + txtAssociateNumber.Text + " " + ((FinanceProgramType)cbProgramType.SelectedItem).ProgramTypeName;
                    }
                    SaveUnsavedTransfer();
                    if (transfers.Count > 0)
                    {
                        financeLogic.InsertPolicyTransfer(policyId, transfers, isAddition);
                    }
                    SaveUnsavedDeposit();
                    if (deposits.Count > 0)
                    {
                        financeLogic.InsertMonthlyDeposit(policyId, deposits, isAddition);
                    }
                    if (trackings.Count > 0)
                    {
                        trackingLogics.InsertFinanceTrackings(policyId, trackings, isAddition);
                    }
                    if (chbStandardMoneyCollection.IsChecked == true)
                    {
                        int standardMoneyCollectionId = moneyCollectionLogic.InsertStandardMoneyCollection(standardMoneyCollection, new Insurance() { InsuranceName = "פיננסים" }, policyId);
                        if (standardMoneyCollectionTrackings != null)
                        {
                            moneyCollectionLogic.InsertTrackingsToStandardMoneyCollection(standardMoneyCollectionId, standardMoneyCollectionTrackings);
                        }
                    }
                }
                else
                {
                    financeLogic.UpdateFinancePolicy(financePolicy.FinancePolicyID, txtAssociateNumber.Text, (FinanceProgramType)cbProgramType.SelectedItem, dpStartDate.SelectedDate, dpEndDate.SelectedDate, ((InsuranceCompany)cbCompany.SelectedItem).CompanyID, principalAgentNumberID, subAgentNumberID, ((FinanceProgram)cbProgram.SelectedItem).ProgramID, policyOwnerID, factoryNumberID, paymentMode, txtComments.Text, admFeesEmployee, workerDeposit, dpIndependentStartDate.SelectedDate, dpEmployeeStartDate.SelectedDate, reportedSalary, txtEmployerPhone.Text, txtEmployerIdNumber.Text, txtEmployerFax.Text, txtEmployerEmail.Text, txtEmployerCellPhone.Text, txtEmployerAddress.Text, txtEmployerName.Text, employerDeposit, independentDepositAmount, employeeDepositAmount, compensation, associateStatus, aggregateFeesIndependent, aggregateFeesEmployee, admFeesIndependent);
                    if (!isOffer)
                    {
                        string[] dirs = Directory.GetDirectories(financePath + @"\פיננסים", "*" + financePolicy.AssociateNumber + " " + financePolicy.FinanceProgramType.ProgramTypeName + "*");
                        if (dirs.Count() > 0)
                        {
                            path = dirs[0];
                        }
                        else
                        {
                            dirs = Directory.GetDirectories(financePath + @"\פיננסים", "*" + financePolicy.AssociateNumber + "*");
                            if (dirs.Count() > 0)
                            {
                                path = dirs[0];
                            }
                        }
                        newPath = financePath + @"\פיננסים\" + txtAssociateNumber.Text + " " + ((FinanceProgramType)cbProgramType.SelectedItem).ProgramTypeName;
                    }
                    else
                    {
                        string[] dirs = Directory.GetDirectories(financePath + @"\הצעות\פיננסים", "*" + financePolicy.AssociateNumber + " " + financePolicy.FinanceProgramType.ProgramTypeName + "*");
                        if (dirs.Count() > 0)
                        {
                            path = dirs[0];
                        }
                        else
                        {
                            dirs = Directory.GetDirectories(financePath + @"\הצעות\פיננסים", "*" + financePolicy.AssociateNumber + "*");
                            if (dirs.Count() > 0)
                            {
                                path = dirs[0];
                            }
                        }
                        newPath = financePath + @"\הצעות\פיננסים\" + txtAssociateNumber.Text + " " + ((FinanceProgramType)cbProgramType.SelectedItem).ProgramTypeName;
                    }
                    SaveUnsavedTransfer();
                    financeLogic.InsertPolicyTransfer(financePolicy.FinancePolicyID, transfers, isAddition);
                    SaveUnsavedDeposit();
                    financeLogic.InsertMonthlyDeposit(financePolicy.FinancePolicyID, deposits, isAddition);
                    trackingLogics.InsertFinanceTrackings(financePolicy.FinancePolicyID, trackings, isAddition);
                    if (chbStandardMoneyCollection.IsChecked == true)
                    {
                        int standardMoneyCollectionId = moneyCollectionLogic.InsertStandardMoneyCollection(standardMoneyCollection, new Insurance() { InsuranceName = "פיננסים" }, financePolicy.FinancePolicyID);
                        moneyCollectionLogic.InsertTrackingsToStandardMoneyCollection(standardMoneyCollectionId, standardMoneyCollectionTrackings);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }

            if (isAddition == false)
            {
                if (financePolicy == null)
                {
                    if (!Directory.Exists(@path))
                    {
                        Directory.CreateDirectory(@path);
                    }
                }
                else
                {
                    if (path == null && newPath != null)
                    {
                        Directory.CreateDirectory(newPath);
                    }
                    else if (path != newPath)
                    {
                        try
                        {
                            Directory.Move(path, newPath);
                        }
                        catch (Exception)
                        {
                            System.Windows.Forms.MessageBox.Show("לא ניתן למצוא את נתיב התיקייה");
                        }
                    }
                }
            }
            if (sender is Button)
            {
                Button btn = (Button)sender;
                if (btn.Name == "btnSave")
                {
                    confirmBeforeClosing = false;
                    Close();
                }
            }
        }

        private void SaveUnsavedDeposit()
        {
            if (txtDepositComments.Text != "" || txtDepositAmount.Text != "" || txtDepositTransferedTo.Text != "")
            {
                if (dgMonthlyDeposits.SelectedItem == null)
                {
                    btnAddMonthlyDeposit_Click(this, new RoutedEventArgs());
                }
                else
                {
                    btnUpdateMonthlyDeposit_Click(this, new RoutedEventArgs());
                }
            }
        }

        private void SaveUnsavedTransfer()
        {
            if (cbTransferringFund.SelectedItem != null || dpTransferringDate.SelectedDate != null || cbTransferedTo.SelectedItem != null || txtTransferringAmount.Text != "")
            {
                btnAddTransference_Click(this, new RoutedEventArgs());
            }
        }

        private void btnUpdatePolicyHolderTable_Click(object sender, RoutedEventArgs e)
        {
            frmAddPolicyOwner policyOwnerWindow = new frmAddPolicyOwner();
            policyOwnerWindow.ShowDialog();
            cbPolicyHolderBinding();
        }

        private void cbPolicyHolder_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            int insuranceId = insuranceLogic.GetInsuranceID("פיננסים");
            if (cbCompany.SelectedItem != null && cbPolicyHolder.SelectedItem != null)
            {
                DisplayPolicyOwnerDetails(insuranceId);
            }
            else
            {
                cbPolicyHolderNumber.SelectedItem = null;
                cbPolicyHolderNumber.IsEnabled = false;
            }
        }

        private void DisplayPolicyOwnerDetails(int insuranceId)
        {
            PolicyOwner policyOwner = (PolicyOwner)cbPolicyHolder.SelectedItem;
            cbPolicyHolderNumber.IsEnabled = true;
            cbPolicyHolderNumber.ItemsSource = policyHoldersLogic.GetNumbersByPolicyHolderAndCompany(policyOwner.PolicyOwnerID, ((InsuranceCompany)cbCompany.SelectedItem).CompanyID, insuranceId);
            cbPolicyHolderNumber.DisplayMemberPath = "Number";
            cbPolicyHolderNumber.SelectedIndex = 0;
            txtEmployerIdNumber.Text = policyOwner.IdNumber;
            txtEmployerName.Text = policyOwner.ContactPerson;
            txtEmployerAddress.Text = string.Format("{0} {1} {2}, {3}", policyOwner.Street, policyOwner.HomeNumber, policyOwner.AptNumber, policyOwner.City);
            txtEmployerPhone.Text = policyOwner.Phone;
            txtEmployerFax.Text = policyOwner.Fax;
            txtEmployerCellPhone.Text = policyOwner.CellPhone;
            txtEmployerEmail.Text = policyOwner.Email;
        }

        private void btnUpdteTransferringFundTable_Click(object sender, RoutedEventArgs e)
        {
            string objName = ((Button)sender).Name;
            int? selectedItemId = null;
            ComboBox cb = null;
            if (objName == "btnUpdteTransferringFundTable")
            {
                if (cbTransferringFund.SelectedItem != null)
                {
                    selectedItemId = ((InsuranceCompany)cbTransferringFund.SelectedItem).CompanyID;
                }
                cb = cbTransferringFund;
            }
            else if (objName == "btnUpdteTransferedToFundTable")
            {
                if (cbTransferedTo.SelectedItem != null)
                {
                    selectedItemId = ((InsuranceCompany)cbTransferedTo.SelectedItem).CompanyID;
                }
                cb = cbTransferedTo;
            }
            int insuranceID = insuranceLogic.GetInsuranceID("פיננסים");
            InsuranceCompany company = new InsuranceCompany() { InsuranceID = insuranceID };
            frmUpdateTable updateCompaniesTable = new frmUpdateTable(company);
            updateCompaniesTable.ShowDialog();
            FundsBinding();
            if (updateCompaniesTable.ItemAdded != null)
            {
                SelectItemInCb(((InsuranceCompany)updateCompaniesTable.ItemAdded).CompanyID, cb);
            }
            else if (selectedItemId != null)
            {
                SelectItemInCb((int)selectedItemId, cb);
            }
        }

        private void cbTransferringFund_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cbTransferringFund.SelectedItem != null)
            {
                cbFundType.IsEnabled = true;
                btnUpdateFundTypeTable.IsEnabled = true;
                TransferringFundsTypeBinding();
            }
            else
            {
                cbFundType.IsEnabled = false;
                btnUpdateFundTypeTable.IsEnabled = false;
            }
        }

        private void TransferringFundsTypeBinding()
        {
            if (cbTransferringFund.SelectedItem != null)
            {
                int? selectedItemId = null;
                if (cbFundType.SelectedItem != null)
                {
                    selectedItemId = ((FinanceFundType)cbFundType.SelectedItem).FinanceFundTypeID;
                }
                cbFundType.ItemsSource = fundsLogics.GetActiveFinanceFundTypesByProgramTypeAndCompany(((FinanceProgramType)cbProgramType.SelectedItem).ProgramTypeID, ((InsuranceCompany)cbTransferringFund.SelectedItem).CompanyID);
                cbFundType.DisplayMemberPath = "FundTypeName";
                if (selectedItemId != null)
                {
                    SelectFundItemInCb((int)selectedItemId, cbFundType);
                }
            }
        }

        private void SelectFundItemInCb(int id, ComboBox cb)
        {
            var items = cb.Items;
            foreach (var item in items)
            {
                FinanceFundType parsedItem = (FinanceFundType)item;
                if (parsedItem.FinanceFundTypeID == id)
                {
                    cb.SelectedItem = item;
                    break;
                }
            }
        }

        private void cbTransferedTo_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cbTransferedTo.SelectedItem != null)
            {
                cbTransferedToFundType.IsEnabled = true;
                btnUpdateTransferedToFundTypeTable.IsEnabled = true;
                TransferedFundsTypeBinding();
            }
            else
            {
                cbTransferedToFundType.IsEnabled = false;
                btnUpdateTransferedToFundTypeTable.IsEnabled = false;
            }
        }

        private void TransferedFundsTypeBinding()
        {
            if (cbTransferedTo.SelectedItem != null)
            {
                int? selectedItemId = null;
                if (cbTransferedToFundType.SelectedItem != null)
                {
                    selectedItemId = ((FinanceFundType)cbTransferedToFundType.SelectedItem).FinanceFundTypeID;
                }
                cbTransferedToFundType.ItemsSource = fundsLogics.GetActiveFinanceFundTypesByProgramTypeAndCompany(((FinanceProgramType)cbProgramType.SelectedItem).ProgramTypeID, ((InsuranceCompany)cbTransferedTo.SelectedItem).CompanyID);
                cbTransferedToFundType.DisplayMemberPath = "FundTypeName";
                if (selectedItemId != null)
                {
                    SelectFundItemInCb((int)selectedItemId, cbTransferedToFundType);
                }
            }
        }

        private void btnUpdateFundTypeTable_Click(object sender, RoutedEventArgs e)
        {
            int? selectedItemId = null;
            if (cbFundType.SelectedItem != null)
            {
                selectedItemId = ((FinanceFundType)cbFundType.SelectedItem).FinanceFundTypeID;
            }
            //int insuranceID = insuranceLogic.GetInsuranceID("חיים");
            FinanceFundType fundType = new FinanceFundType() { ProgramTypeID = ((FinanceProgramType)cbProgramType.SelectedItem).ProgramTypeID, CompanyID = ((InsuranceCompany)cbTransferringFund.SelectedItem).CompanyID };
            frmUpdateTable updateFundTypesTable = new frmUpdateTable(fundType);
            updateFundTypesTable.ShowDialog();
            TransferringFundsTypeBinding();
            TransferedFundsTypeBinding();
            if (updateFundTypesTable.ItemAdded != null)
            {
                SelectFundItemInCb(((FinanceFundType)updateFundTypesTable.ItemAdded).FinanceFundTypeID, cbFundType);
            }
            else if (selectedItemId != null)
            {
                SelectFundItemInCb((int)selectedItemId, cbFundType);
            }
        }

        private void btnUpdateTransferedToFundTypeTable_Click(object sender, RoutedEventArgs e)
        {
            int? selectedItemId = null;
            if (cbTransferedToFundType.SelectedItem != null)
            {
                selectedItemId = ((FinanceFundType)cbTransferedToFundType.SelectedItem).FinanceFundTypeID;
            }
            //int insuranceID = insuranceLogic.GetInsuranceID("חיים");
            FinanceFundType fundType = new FinanceFundType() { ProgramTypeID = ((FinanceProgramType)cbProgramType.SelectedItem).ProgramTypeID, CompanyID = ((InsuranceCompany)cbTransferedTo.SelectedItem).CompanyID };
            frmUpdateTable updateFundTypesTable = new frmUpdateTable(fundType);
            updateFundTypesTable.ShowDialog();
            TransferringFundsTypeBinding();
            TransferedFundsTypeBinding();
            if (updateFundTypesTable.ItemAdded != null)
            {
                SelectFundItemInCb(((FinanceFundType)updateFundTypesTable.ItemAdded).FinanceFundTypeID, cbTransferedToFundType);
            }
            else if (selectedItemId != null)
            {
                SelectFundItemInCb((int)selectedItemId, cbTransferedToFundType);
            }
        }

        private void btnAddTransference_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                int? transferringCompanyId = null;
                Company transferringCompany = null;
                if (cbTransferringFund.SelectedItem != null)
                {
                    transferringCompanyId = ((InsuranceCompany)cbTransferringFund.SelectedItem).CompanyID;
                    transferringCompany = ((InsuranceCompany)cbTransferringFund.SelectedItem).Company;
                }
                int? transferringFundTypeId = null;
                FinanceFundType transferrinfFundType = null;
                if (cbFundType.SelectedItem != null)
                {
                    transferringFundTypeId = ((FinanceFundType)cbFundType.SelectedItem).FinanceFundTypeID;
                    transferrinfFundType = (FinanceFundType)cbFundType.SelectedItem;
                }
                int? transferedToCompanyId = null;
                Company transferedToCompany = null;
                if (cbTransferedTo.SelectedItem != null)
                {
                    transferedToCompanyId = ((InsuranceCompany)cbTransferedTo.SelectedItem).CompanyID;
                    transferedToCompany = ((InsuranceCompany)cbTransferedTo.SelectedItem).Company;
                }
                int? transferedToFundTypeId = null;
                FinanceFundType transferedToFundType = null;
                if (cbTransferedToFundType.SelectedItem != null)
                {
                    transferedToFundTypeId = ((FinanceFundType)cbTransferedToFundType.SelectedItem).FinanceFundTypeID;
                    transferedToFundType = (FinanceFundType)cbTransferedToFundType.SelectedItem;
                }
                decimal? transferingAmount = null;
                if (txtTransferringAmount.Text != "")
                {
                    transferingAmount = decimal.Parse(txtTransferringAmount.Text);
                }
                if (txtBtnAddTransfer.Text.Contains("הוסף"))
                {
                    transferSelected = null;
                    FinancePolicyTransfer newTransfer = new FinancePolicyTransfer() { TransferringCompanyID = transferringCompanyId, FinanceFundTypeID = transferringFundTypeId, TransferDate = dpTransferringDate.SelectedDate, TransferAmount = transferingAmount, TransferedToCompanyID = transferedToCompanyId, TransferedToFinanceFundTypeID = transferedToFundTypeId, Company = transferringCompany, FinanceFundType = transferrinfFundType, Company1 = transferedToCompany, FinanceFundType1 = transferedToFundType };
                    transfers.Add(newTransfer);
                }
                else
                {
                    if (transferSelected != null)
                    {
                        transferSelected.TransferringCompanyID = transferringCompanyId;
                        transferSelected.FinanceFundTypeID = transferringFundTypeId;
                        transferSelected.TransferDate = dpTransferringDate.SelectedDate;
                        transferSelected.TransferAmount = transferingAmount;
                        transferSelected.TransferedToCompanyID = transferedToCompanyId;
                        transferSelected.TransferedToFinanceFundTypeID = transferedToFundTypeId;
                        transferSelected.Company = transferringCompany;
                        transferSelected.FinanceFundType = transferrinfFundType;
                        transferSelected.Company1 = transferedToCompany;
                        transferSelected.FinanceFundType1 = transferedToFundType;
                    }
                }
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
            }
            dgTransferencesBinding();
            txtBtnAddTransfer.Text = "הוסף העברה";
            ClearInputs(new object[] { cbTransferringFund, cbFundType, dpTransferringDate, txtTransferringAmount, cbTransferedTo, cbTransferedToFundType });
            //transferingAmount = 0;
        }

        private void dgTransferencesBinding()
        {
            dgTransferences.ItemsSource = transfers.OrderByDescending(t => t.TransferDate).ToList();
            lv.AutoSizeColumns(dgTransferences.View);
        }

        private void ClearInputs(object[] inputsToClear)
        {

            foreach (var item in inputsToClear)
            {
                if (item is TextBox)
                {
                    ((TextBox)item).Clear();
                }
                else if (item is ComboBox)
                {
                    ((ComboBox)item).SelectedIndex = -1;
                    ((ComboBox)item).Text = "";
                }
                else if (item is DatePicker)
                {
                    ((DatePicker)item).SelectedDate = null;
                }
            }
        }

        private void dgTransferences_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (dgTransferences.SelectedItem != null)
            {
                txtBtnAddTransfer.Text = "עדכן העברה";
                transferSelected = (FinancePolicyTransfer)dgTransferences.SelectedItem;
                var funds = cbTransferringFund.Items;
                foreach (var item in funds)
                {
                    InsuranceCompany fund = (InsuranceCompany)item;
                    if (fund.CompanyID == transferSelected.TransferringCompanyID)
                    {
                        cbTransferringFund.SelectedItem = item;
                    }
                    if (fund.CompanyID == transferSelected.TransferedToCompanyID)
                    {
                        cbTransferedTo.SelectedItem = item;
                    }
                }
                var fundTypes = cbFundType.Items;
                foreach (var item in fundTypes)
                {
                    FinanceFundType type = (FinanceFundType)item;
                    if (type.FinanceFundTypeID == transferSelected.FinanceFundTypeID)
                    {
                        cbFundType.SelectedItem = item;
                        break;
                    }
                }
                var transferedToFundTypes = cbTransferedToFundType.Items;
                foreach (var item in transferedToFundTypes)
                {
                    FinanceFundType type = (FinanceFundType)item;
                    if (type.FinanceFundTypeID == transferSelected.TransferedToFinanceFundTypeID)
                    {
                        cbTransferedToFundType.SelectedItem = item;
                        break;
                    }
                }
                dpTransferringDate.SelectedDate = transferSelected.TransferDate;
                if (transferSelected.TransferAmount != null)
                {
                    txtTransferringAmount.Text = ((decimal)transferSelected.TransferAmount).ToString("0.##");
                }
            }
        }

        private void btnDeleteTransferenceAdm_Click(object sender, RoutedEventArgs e)
        {
            if (dgTransferences.SelectedItem != null)
            {
                if (MessageBox.Show("למחוק העברה", "אישור", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    transfers.Remove((FinancePolicyTransfer)dgTransferences.SelectedItem);
                    dgTransferencesBinding();
                    ClearInputs(new object[] { cbTransferringFund, cbFundType, dpTransferringDate, txtTransferringAmount, cbTransferedTo, cbTransferedToFundType });
                    txtBtnAddTransfer.Text = "הוסף העברה";
                }
            }
            else
            {
                System.Windows.Forms.MessageBox.Show("נא סמן את ההעברה שברצונך לבטל");
            }
        }

        private void btnAddMonthlyDeposit_Click(object sender, RoutedEventArgs e)
        {
            if (dtxtForMonth.Value == null || txtDepositAmount.Text == "")
            {
                if (sender is Window)
                {
                    MessageBox.Show("שים לב, פרטי שינוי השכר לא עודכנו במערכת", "מידע", MessageBoxButton.OK, MessageBoxImage.Information);
                    return;
                }
                System.Windows.Forms.MessageBox.Show("נא למלות שדות חובה: עבור חודש וסכום הפקדה");
                return;
            }
            decimal? depositAmount = null;
            if (txtDepositAmount.Text != "")
            {
                depositAmount = decimal.Parse(txtDepositAmount.Text);
            }
            FinancePolicyMonthlyDeposit newDeposit = new FinancePolicyMonthlyDeposit() { Comments = txtDepositComments.Text, DepositAmount = depositAmount, DepositForTheMonth = dtxtForMonth.Value, DepositDate = dpDepositDate.SelectedDate, TransferredTo = txtDepositTransferedTo.Text };
            deposits.Add(newDeposit);
            var annualDepositToUpdate = annualDeposits.FirstOrDefault(d => d.Year.Year == ((DateTime)newDeposit.DepositForTheMonth).Year);
            if (annualDepositToUpdate != null)
            {
                annualDepositToUpdate.TotalDepositsAmount = annualDepositToUpdate.TotalDepositsAmount + (decimal)newDeposit.DepositAmount;
            }
            else
            {
                AnnualDeposits newAnnuealDeposit = new AnnualDeposits() { Year = (DateTime)newDeposit.DepositForTheMonth, TotalDepositsAmount = (decimal)newDeposit.DepositAmount };
                annualDeposits.Add(newAnnuealDeposit);
            }
            dgMonthlyDepositsBinding();
            //financeLogic.InsertMonthlyDeposit(policyId, dpDepositDate.SelectedDate, dtxtForMonth.Text, depositAmount, dtxtForMonth.Text, txtDepositComments.Text);
            ClearInputs(new object[] { txtDepositAmount, txtDepositTransferedTo, txtDepositComments });
            dpDepositDate.SelectedDate = DateTime.Now;
            dtxtForMonth.Value = DateTime.Now;
        }

        private void btnUpdateMonthlyDeposit_Click(object sender, RoutedEventArgs e)
        {
            if (dgMonthlyDeposits.SelectedItem != null)
            {
                if (dtxtForMonth.Value == null || txtDepositAmount.Text == "")
                {
                    if (sender is Window)
                    {
                        MessageBox.Show("שים לב, פרטי שינוי השכר לא עודכנו במערכת", "מידע", MessageBoxButton.OK, MessageBoxImage.Information);
                        return;
                    }
                    System.Windows.Forms.MessageBox.Show("נא למלות שדות חובה: עבור חודש וסכום הפקדה");
                    return;
                }
                var depositToUpdate = (FinancePolicyMonthlyDeposit)dgMonthlyDeposits.SelectedItem;
                var annualDepositToUpdate = annualDeposits.FirstOrDefault(d => d.Year.Year == ((DateTime)depositToUpdate.DepositForTheMonth).Year);
                if (annualDepositToUpdate != null)
                {
                    annualDepositToUpdate.TotalDepositsAmount = annualDepositToUpdate.TotalDepositsAmount - (decimal)depositToUpdate.DepositAmount;
                }
                if (annualDepositToUpdate.TotalDepositsAmount == 0)
                {
                    annualDeposits.Remove(annualDepositToUpdate);
                }
                decimal? depositAmount = null;
                if (txtDepositAmount.Text != "")
                {
                    depositAmount = decimal.Parse(txtDepositAmount.Text);
                }
                depositToUpdate.Comments = txtDepositComments.Text;
                depositToUpdate.DepositAmount = depositAmount;
                depositToUpdate.DepositDate = dpDepositDate.SelectedDate;
                depositToUpdate.DepositForTheMonth = dtxtForMonth.Value;
                depositToUpdate.TransferredTo = txtDepositTransferedTo.Text;
                annualDepositToUpdate = annualDeposits.FirstOrDefault(d => d.Year.Year == ((DateTime)depositToUpdate.DepositForTheMonth).Year);
                if (annualDepositToUpdate != null)
                {
                    annualDepositToUpdate.TotalDepositsAmount = annualDepositToUpdate.TotalDepositsAmount + (decimal)depositToUpdate.DepositAmount;
                }
                else
                {
                    AnnualDeposits newAnnuealDeposit = new AnnualDeposits() { Year = (DateTime)depositToUpdate.DepositForTheMonth, TotalDepositsAmount = (decimal)depositToUpdate.DepositAmount };
                    annualDeposits.Add(newAnnuealDeposit);
                }
                dgMonthlyDepositsBinding();
                ClearInputs(new object[] { txtDepositAmount, txtDepositTransferedTo, txtDepositComments });
                dpDepositDate.SelectedDate = DateTime.Now;
                dtxtForMonth.Value = DateTime.Now;
            }
        }

        private void dgMonthlyDeposits_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (dgMonthlyDeposits.SelectedItem != null)
            {
                var depositToUpdate = (FinancePolicyMonthlyDeposit)dgMonthlyDeposits.SelectedItem;
                dpDepositDate.SelectedDate = depositToUpdate.DepositDate;
                dtxtForMonth.Value = depositToUpdate.DepositForTheMonth;
                if (depositToUpdate.DepositAmount != null)
                {
                    txtDepositAmount.Text = ((decimal)depositToUpdate.DepositAmount).ToString("0.##");
                }
                txtDepositTransferedTo.Text = depositToUpdate.TransferredTo;
                txtDepositComments.Text = depositToUpdate.Comments;
            }
            else
            {
                ClearInputs(new object[] { txtDepositAmount, txtDepositTransferedTo, txtDepositComments });
                dpDepositDate.SelectedDate = DateTime.Now;
                dtxtForMonth.Value = DateTime.Now;
            }
        }

        private void dgMonthlyDeposits_MouseDown(object sender, MouseButtonEventArgs e)
        {
            dgMonthlyDeposits.UnselectAll();
        }

        private void dgAnnualDeposits_MouseDown(object sender, MouseButtonEventArgs e)
        {
            dgAnnualDeposits.UnselectAll();
        }

        private void btnDeleteMonthlyDeposit_Click(object sender, RoutedEventArgs e)
        {
            if (dgMonthlyDeposits.SelectedItem != null)
            {
                if (MessageBox.Show("למחוק הפקדה", "אישור", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    var depositToUpdate = (FinancePolicyMonthlyDeposit)dgMonthlyDeposits.SelectedItem;
                    deposits.Remove(depositToUpdate);
                    var annualDepositToUpdate = annualDeposits.FirstOrDefault(d => d.Year.Year == ((DateTime)depositToUpdate.DepositForTheMonth).Year);
                    if (annualDepositToUpdate != null)
                    {
                        annualDepositToUpdate.TotalDepositsAmount = annualDepositToUpdate.TotalDepositsAmount - (decimal)depositToUpdate.DepositAmount;
                        if (annualDepositToUpdate.TotalDepositsAmount == 0)
                        {
                            annualDeposits.Remove(annualDepositToUpdate);
                        }
                    }
                    dgMonthlyDepositsBinding();
                    ClearInputs(new object[] { txtDepositAmount, txtDepositTransferedTo, txtDepositComments });
                    dpDepositDate.SelectedDate = DateTime.Now;
                    dtxtForMonth.Value = DateTime.Now;
                }
            }
            else
            {
                System.Windows.Forms.MessageBox.Show("נא סמן את ההעברה שברצונך לבטל");
            }
        }

        private void btnNewTracking_Click(object sender, RoutedEventArgs e)
        {
            frmTracking newTrackingWindow = new frmTracking(client, financePolicy, user);
            newTrackingWindow.ShowDialog();
            if (newTrackingWindow.trackingContent != null)
            {
                trackings.Add(new FinanceTracking() { TrackingContent = newTrackingWindow.trackingContent, Date = DateTime.Now, UserID = user.UserID, User = user });
                dgTrackingsBinding();
            }
        }

        private void dgTrackingsBinding()
        {
            dgFinanceTrackings.ItemsSource = trackings.OrderByDescending(t => t.Date);
            lv.AutoSizeColumns(dgFinanceTrackings.View);
        }

        private void btnDeleteTracking_Click(object sender, RoutedEventArgs e)
        {
            if (dgFinanceTrackings.SelectedItem == null)
            {
                MessageBox.Show("נא לסמן מעקב בטבלה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            if (MessageBox.Show("למחוק מעקב", "אישור", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                trackings.Remove((FinanceTracking)dgFinanceTrackings.SelectedItem);
                dgTrackingsBinding();
            }
            else
            {
                dgFinanceTrackings.UnselectAll();
            }
        }

        private void btnUpdateTracking_Click(object sender, RoutedEventArgs e)
        {
            if (dgFinanceTrackings.SelectedItem == null)
            {
                MessageBox.Show("נא לסמן מעקב בטבלה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            FinanceTracking trackingToUpdate = (FinanceTracking)dgFinanceTrackings.SelectedItem;
            frmTracking updateTrackingWindow = new frmTracking(trackingToUpdate, client, financePolicy, user);
            updateTrackingWindow.ShowDialog();
            trackingToUpdate.TrackingContent = updateTrackingWindow.trackingContent;
            dgTrackingsBinding();
            dgFinanceTrackings.UnselectAll();
        }

        private void dgFinanceTrackings_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            DependencyObject src = (DependencyObject)(e.OriginalSource);
            while (!(src is Control))
            {
                if (!(src is Visual))
                {
                    break;
                }
                src = VisualTreeHelper.GetParent(src);
            }

            if ((src is Control && src.GetType() == typeof(ListViewItem)) || src.GetType() == typeof(Run))
            {
                if (dgFinanceTrackings.SelectedItem != null)
                {
                    FinanceTracking trackingToUpdate = (FinanceTracking)dgFinanceTrackings.SelectedItem;
                    frmTracking updateTrackingWindow = new frmTracking(trackingToUpdate, client, financePolicy, user);
                    updateTrackingWindow.ShowDialog();
                    trackingToUpdate.TrackingContent = updateTrackingWindow.trackingContent;
                    dgTrackingsBinding();
                    dgFinanceTrackings.UnselectAll();
                }
            }
        }

        private void dgFinanceTrackings_MouseDown(object sender, MouseButtonEventArgs e)
        {
            dgFinanceTrackings.UnselectAll();
        }

        private void GroupBox_GotFocus(object sender, RoutedEventArgs e)
        {
            isChanges = true;
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (confirmBeforeClosing && isChanges)
            {
                if (MessageBox.Show("האם ברצונך לשמור שינוים שבצעת", "אישור", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    btnSave_Click(sender, new RoutedEventArgs());
                    if (cancelClose)
                    {
                        e.Cancel = true;
                        cancelClose = false;
                    }
                }
            }
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void chbStandardMoneyCollection_Checked(object sender, RoutedEventArgs e)
        {
            btnStandardMoneyCollectionDetails.IsEnabled = true;
        }
        private void chbStandardMoneyCollection_Unchecked(object sender, RoutedEventArgs e)
        {
            btnStandardMoneyCollectionDetails.IsEnabled = false;
        }

        private void btnStandardMoneyCollectionDetails_Click(object sender, RoutedEventArgs e)
        {
            frmStandardMoneyCollectionDetails window = null;
            if (standardMoneyCollection == null)
            {
                window = new frmStandardMoneyCollectionDetails(false, user);
            }
            else
            {
                window = new frmStandardMoneyCollectionDetails(standardMoneyCollection, standardMoneyCollectionTrackings, false, user);
            }
            window.ShowDialog();
            if (window.StandardMoneyCollection != null)
            {
                standardMoneyCollection = window.StandardMoneyCollection;
            }
            if (window.Trackings != null)
            {
                standardMoneyCollectionTrackings = window.Trackings;
            }
        }
    }
}
