﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using CoralBusinessLogics;

namespace CoralUI
{
    /// <summary>
    /// Interaction logic for frmReminder.xaml
    /// </summary>
    public partial class frmReminder : Window
    {
        WorkTasksLogic workTaskLogic = new WorkTasksLogic();
        Request request;
        User user;
        Reminders remindersClass = null;
        bool isNonSeenReminder;
        public frmReminder(Request requestToRemind,User userAccount, bool isOldReminder)
        {
            InitializeComponent();
            request = requestToRemind;
            user = userAccount;
            isNonSeenReminder = isOldReminder;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (request.Client != null)
            {
                lblClientName.Content = "-" + request.Client.FirstName + " " + request.Client.LastName;
            }
            lblDateTimeNow.Content = ((DateTime)request.ReminderDate).ToString("dd/MM/yyyy") + " " + ((TimeSpan)request.ReminderHour).ToString(@"hh\:mm");
            txtMessage.Text = request.RequestSubject;
            UpdateRequestViewStatus(true);
            System.Media.SystemSounds.Beep.Play();
            if (user.IsTasksPermission==false)
            {
                btnOpenRequest.IsEnabled = false;
            }
        }

        private void UpdateRequestViewStatus(bool wasSeen)
        {
            workTaskLogic.UpdateRequestViewedStatus(request, wasSeen);
        }

        private void chbResetReminder_Checked(object sender, RoutedEventArgs e)
        {
            dpReminderDate.IsEnabled = true;
            dpReminderDate.SelectedDate = DateTime.Now;
            tpReminderTime.IsEnabled = true;
            tpReminderTime.Value = Convert.ToDateTime(DateTime.Now.TimeOfDay.ToString());
        }

        private void chbResetReminder_Unchecked(object sender, RoutedEventArgs e)
        {
            dpReminderDate.IsEnabled = false;
            tpReminderTime.IsEnabled = false;
            dpReminderDate.SelectedDate = null;
            tpReminderTime.Value = null;
        }

        private void btnOpenRequest_Click(object sender, RoutedEventArgs e)
        {
            frmWorkQueue requestWindow = new frmWorkQueue(request);
            requestWindow.ShowDialog();
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            if (chbResetReminder.IsChecked == true)
            {
                if (dpReminderDate.SelectedDate == null || tpReminderTime.Value == null)
                {
                    MessageBox.Show("נא להזין תאריך ושעת התזכורת", "שדה חובה", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    return;
                }
                TimeSpan reminderTime = ((DateTime)tpReminderTime.Value).TimeOfDay;                
                request=workTaskLogic.ResetReminder(request, (DateTime)dpReminderDate.SelectedDate, reminderTime);
                UpdateRequestViewStatus(false);
                remindersClass = new Reminders(user);
                remindersClass.ResetReminderTimer(request, isNonSeenReminder, (DateTime)dpReminderDate.SelectedDate, reminderTime);
            }
            Close();
        }
    }
}
