﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using CoralBusinessLogics;
using System.Windows.Controls.Primitives;


namespace CoralUI
{
    /// <summary>
    /// Interaction logic for frmRenewals.xaml
    /// </summary>
    public partial class frmRenewals : Window
    {
        PoliciesLogic policyLogic = new PoliciesLogic();
        ClientsLogic clientLogic = new ClientsLogic();
        RenewalsLogic renewalLogic = new RenewalsLogic();
        InsurancesLogic insuranceLogic = new InsurancesLogic();
        IndustriesLogic industryLogic = new IndustriesLogic();
        ForeingWorkersIndutryLogic foreingWorkersIndustryLogic = new ForeingWorkersIndutryLogic();
        ForeingWorkersLogic foreingWorkersLogic = new ForeingWorkersLogic();
        Client client;
        User user;
        Email email = new Email();
        ExcelLogic excelLogic = new ExcelLogic();
        Client renewalsClient;
        public frmRenewals(User userAccount, Client renewalsClient)
        {
            InitializeComponent();
            this.SizeToContent = SizeToContent.Manual;
            dtxtPeriod.Value = DateTime.Now;
            user = userAccount;
            this.renewalsClient = renewalsClient;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            cbCategoryBinding();
            cbStatusBinding();
            SetPermissions();
            if (renewalsClient != null)
            {
                dtxtPeriod.Visibility = Visibility.Collapsed;
                dtxtPeriod.Value = null;
                lblClientName.Visibility = Visibility.Visible;
                lblClientName.Content = renewalsClient.LastName + " " + renewalsClient.FirstName;
            }
        }

        private void SetPermissions()
        {
            if (user.IsElementaryPermission == false)
            {
                tbItemElementary.IsEnabled = false;
                dgRenewals.ItemsSource = null;
                dgRenewals.IsEnabled = false;
            }
            if (user.IsForeingWorkersPermission == false)
            {
                tbItemForeingWorkers.IsEnabled = false;
            }
            if (user.IsScanPermission == false)
            {
                btnOpticArchive.IsEnabled = false;
            }
        }

        private void policiesTabs_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.Source is TabControl)
            {
                cbInsuranceCompanyBinding();
                cbPolicyTypeBinding();
                if (tbItemElementary.IsSelected)
                {
                    dgRenewalsBinding();
                }
                else
                {
                    dgForeingWorkersRenewalsBinding();
                }
            }
        }

        private void dgForeingWorkersRenewalsBinding()
        {
            DateTime? endDate = null;
            List<ForeingWorkersPolicy> policiesToRenew = new List<ForeingWorkersPolicy>();
            if (renewalsClient == null)
            {
                DateTime date = (DateTime)dtxtPeriod.Value;
                endDate = new DateTime(date.Year, date.Month, DateTime.DaysInMonth(date.Year, date.Month), 23, 59, 59);
                policiesToRenew = foreingWorkersLogic.GetPoliciesByEndDate((DateTime)endDate);
            }
            else
            {
                policiesToRenew = foreingWorkersLogic.GetAllForeingWorkersPoliciesByClient(renewalsClient);
            }
            List<ForeingWorkersPolicy> tempList = new List<ForeingWorkersPolicy>();
            tempList.AddRange(policiesToRenew);
            foreach (var item in tempList)
            {
                if (!foreingWorkersLogic.IsLastPolicyAddition(item, policiesToRenew))
                {
                    policiesToRenew.Remove(item);
                }
            }
            if (cbStatus.SelectedItem != null)
            {
                policiesToRenew = policiesToRenew.Where(p => p.RenewalStatusID == ((RenewalStatus)cbStatus.SelectedItem).RenewalStatusID).ToList();
            }
            if (cbPolicyType.SelectedItem != null)
            {
                policiesToRenew = policiesToRenew.Where(p => p.ForeingWorkersIndustryID == ((ForeingWorkersIndustry)cbPolicyType.SelectedItem).ForeingWorkersIndustryID).ToList();
            }
            if (cbCategory.SelectedItem != null)
            {
                policiesToRenew = policiesToRenew.Where(p => p.Client.CategoryID == ((Category)cbCategory.SelectedItem).CategoryID).ToList();
            }
            if (cbInsuranceCompany.SelectedItem != null)
            {
                policiesToRenew = policiesToRenew.Where(p => p.CompanyID == ((InsuranceCompany)cbInsuranceCompany.SelectedItem).CompanyID).ToList();
            }
            if (txtSearch.Text != "")
            {
                policiesToRenew = policiesToRenew.Where(p => p.Client.LastName.StartsWith(txtSearch.Text) || p.Client.FirstName.StartsWith(txtSearch.Text) || (p.Client.LastName + " " + p.Client.FirstName).StartsWith(txtSearch.Text) || (p.Client.FirstName + " " + p.Client.LastName).StartsWith(txtSearch.Text) || p.Client.IdNumber.StartsWith(txtSearch.Text) || p.PolicyNumber.StartsWith(txtSearch.Text)).ToList();
            }

            if (renewalsClient == null)
            {
                dgForeingWorkersRenewals.ItemsSource = policiesToRenew.OrderBy(p => p.EndDate).ThenByDescending(p => p.RenewalStatus.RenewalStatusName == "לא טופל").ThenByDescending(p => p.RenewalStatus.RenewalStatusName == "פתוח לעבודה").ThenByDescending(p => p.RenewalStatus.RenewalStatusName == "בתהליך").ThenByDescending(p => p.RenewalStatus.RenewalStatusName == "בהפקה").ThenByDescending(p => p.RenewalStatus.RenewalStatusName == "חודש").ThenByDescending(p => p.RenewalStatus.RenewalStatusName == "לא חודש").ThenBy(p => p.Client.LastName).ToList();
            }
            else
            {
                dgForeingWorkersRenewals.ItemsSource = policiesToRenew.OrderByDescending(p => p.EndDate).ThenByDescending(p => p.RenewalStatus.RenewalStatusName == "לא טופל").ThenByDescending(p => p.RenewalStatus.RenewalStatusName == "פתוח לעבודה").ThenByDescending(p => p.RenewalStatus.RenewalStatusName == "בתהליך").ThenByDescending(p => p.RenewalStatus.RenewalStatusName == "בהפקה").ThenByDescending(p => p.RenewalStatus.RenewalStatusName == "חודש").ThenByDescending(p => p.RenewalStatus.RenewalStatusName == "לא חודש").ThenBy(p => p.Client.LastName).ToList();
            }
            MarketingInfoBinding(endDate, policiesToRenew);
        }

        private void MarketingInfoBinding(DateTime? endDate, object policiesToRenew)
        {
            if (policiesTabs.SelectedItem != null)
            {
                if (tbItemElementary.IsSelected)
                {
                    lblIndustry.Content = (cbPolicyType.SelectedItem != null) ? ((InsuranceIndustry)cbPolicyType.SelectedItem).InsuranceIndustryName : "הכל";
                }
                else
                {
                    lblIndustry.Content = (cbPolicyType.SelectedItem != null) ? ((ForeingWorkersIndustry)cbPolicyType.SelectedItem).ForeingWorkersIndustryName : "הכל";
                }
                lblCompany.Content = (cbInsuranceCompany.SelectedItem != null) ? ((InsuranceCompany)cbInsuranceCompany.SelectedItem).Company.CompanyName : "הכל";

                decimal totalPremium;
                if (tbItemElementary.IsSelected)
                {
                    totalPremium = renewalLogic.GetTotalPremium(endDate,(List<ElementaryPolicy>)policiesToRenew, renewalsClient);
                    lblPremium.Text = string.Format("סה''כ רשומות: {0} סה''כ פרמיה: {1} ש''ח", dgRenewals.Items.Count.ToString(), totalPremium.ToString("0.##"));
                }
                else
                {
                    totalPremium = renewalLogic.GetForeingWorkersTotalPremium(endDate, (List<ForeingWorkersPolicy>)policiesToRenew, renewalsClient);
                    lblPremium.Text = string.Format("סה''כ רשומות: {0} סה''כ פרמיה: {1} ש''ח", dgForeingWorkersRenewals.Items.Count.ToString(), totalPremium.ToString("0.##"));
                }
            }
        }

        private void cbStatusBinding()
        {
            cbStatus.ItemsSource = renewalLogic.GetAllActiveRenewalStatuses();
            cbStatus.DisplayMemberPath = "RenewalStatusName";
        }

        private void cbPolicyTypeBinding()
        {
            if (tbItemElementary.IsSelected)
            {
                cbPolicyType.ItemsSource = industryLogic.GetAllIndustries();
                cbPolicyType.DisplayMemberPath = "InsuranceIndustryName";
            }
            else
            {
                cbPolicyType.ItemsSource = foreingWorkersIndustryLogic.GetAllForeingWorkersIndustries();
                cbPolicyType.DisplayMemberPath = "ForeingWorkersIndustryName";
            }
        }


        private void cbInsuranceCompanyBinding()
        {
            if (tbItemElementary.IsSelected)
            {
                cbInsuranceCompany.ItemsSource = insuranceLogic.GetActiveCompaniesByInsurance("אלמנטרי");
            }
            else
            {
                cbInsuranceCompany.ItemsSource = insuranceLogic.GetActiveCompaniesByInsurance("עובדים זרים");
            }
            cbInsuranceCompany.DisplayMemberPath = "Company.CompanyName";
        }

        //private void cbClientTypeBinding()
        //{
        //    cbClientType.ItemsSource = clientLogic.GetAllTypes();
        //    cbClientType.DisplayMemberPath = "ClientTypeName";
        //}

        private void cbCategoryBinding()
        {
            cbCategory.ItemsSource = clientLogic.GetAllCategories();
            cbCategory.DisplayMemberPath = "CategoryName";
        }

        private void dgRenewalsBinding()
        {
            DateTime? endDate = null;
            List<ElementaryPolicy> policiesToRenew = new List<ElementaryPolicy>();
            if (renewalsClient == null)
            {
                DateTime date = (DateTime)dtxtPeriod.Value;
                endDate = new DateTime(date.Year, date.Month, DateTime.DaysInMonth(date.Year, date.Month), 23, 59, 59);
                policiesToRenew = policyLogic.GetPoliciesByEndDate((DateTime)endDate);
            }
            else
            {
                policiesToRenew = policyLogic.GetAllElementaryPoliciesByClient(renewalsClient);
            }
            List<ElementaryPolicy> tempList = new List<ElementaryPolicy>();
            tempList.AddRange(policiesToRenew);
            foreach (var item in tempList)
            {
                if (!policyLogic.IsLastPolicyAddition(item, policiesToRenew))
                {
                    policiesToRenew.Remove(item);
                }
            }
            if (cbStatus.SelectedItem != null)
            {
                policiesToRenew = policiesToRenew.Where(p => p.RenewalStatusID == ((RenewalStatus)cbStatus.SelectedItem).RenewalStatusID).ToList();
            }
            if (cbPolicyType.SelectedItem != null)
            {
                policiesToRenew = policiesToRenew.Where(p => p.InsuranceIndustryID == ((InsuranceIndustry)cbPolicyType.SelectedItem).InsuranceIndustryID).ToList();
            }
            if (cbCategory.SelectedItem != null)
            {
                policiesToRenew = policiesToRenew.Where(p => p.Client.CategoryID == ((Category)cbCategory.SelectedItem).CategoryID).ToList();
            }
            if (cbInsuranceCompany.SelectedItem != null)
            {
                policiesToRenew = policiesToRenew.Where(p => p.CompanyID == ((InsuranceCompany)cbInsuranceCompany.SelectedItem).CompanyID).ToList();
            }
            if (txtSearch.Text != "")
            {
                policiesToRenew = policiesToRenew.Where(p => p.Client.LastName.StartsWith(txtSearch.Text) || p.Client.FirstName.StartsWith(txtSearch.Text) || (p.Client.LastName + " " + p.Client.FirstName).StartsWith(txtSearch.Text) || (p.Client.FirstName + " " + p.Client.LastName).StartsWith(txtSearch.Text) || p.Client.IdNumber.StartsWith(txtSearch.Text) || p.PolicyNumber.StartsWith(txtSearch.Text) || (p.CarPolicy != null && p.CarPolicy.RegistrationNumber != null && p.CarPolicy.RegistrationNumber.StartsWith(txtSearch.Text))).ToList();
            }

            if (renewalsClient == null)
            {
                dgRenewals.ItemsSource = policiesToRenew.OrderByDescending(p => p.RenewalStatus.RenewalStatusName == "לא טופל").ThenByDescending(p => p.RenewalStatus.RenewalStatusName == "פתוח לעבודה").ThenByDescending(p => p.RenewalStatus.RenewalStatusName == "בתהליך").ThenByDescending(p => p.RenewalStatus.RenewalStatusName == "בהפקה").ThenByDescending(p => p.RenewalStatus.RenewalStatusName == "חודש").ThenByDescending(p => p.RenewalStatus.RenewalStatusName == "לא חודש").ThenBy(p => p.Client.LastName).ToList();
            }
            else
            {
                dgRenewals.ItemsSource = policiesToRenew.OrderByDescending(p => p.EndDate).ThenByDescending(p => p.RenewalStatus.RenewalStatusName == "לא טופל").ThenByDescending(p => p.RenewalStatus.RenewalStatusName == "פתוח לעבודה").ThenByDescending(p => p.RenewalStatus.RenewalStatusName == "בתהליך").ThenByDescending(p => p.RenewalStatus.RenewalStatusName == "בהפקה").ThenByDescending(p => p.RenewalStatus.RenewalStatusName == "חודש").ThenByDescending(p => p.RenewalStatus.RenewalStatusName == "לא חודש").ThenBy(p => p.Client.LastName).ToList();
            }
            MarketingInfoBinding(endDate, policiesToRenew);
        }


        private void btnClientDetails_Click(object sender, RoutedEventArgs e)
        {
            ListView lv = null;
            object renewalToSelect = null;
            if (tbItemElementary.IsSelected)
            {
                if (dgRenewals.SelectedItem == null)
                {
                    MessageBox.Show("נא לסמן פוליסה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                }
                else
                {
                    lv = dgRenewals;
                    renewalToSelect = dgRenewals.SelectedItem;
                    client = clientLogic.GetClientByClientID(((ElementaryPolicy)dgRenewals.SelectedItem).ClientID);
                }
            }
            else
            {
                if (dgForeingWorkersRenewals.SelectedItem == null)
                {
                    MessageBox.Show("נא לסמן פוליסה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                }
                else
                {
                    lv = dgForeingWorkersRenewals;
                    renewalToSelect = dgForeingWorkersRenewals.SelectedItem;
                    client = clientLogic.GetClientByClientID(((ForeingWorkersPolicy)dgForeingWorkersRenewals.SelectedItem).ClientID);
                }
            }

            frmAddClient clientDetailsWindow = new frmAddClient(client, user);
            clientDetailsWindow.ShowDialog();
            if (tbItemElementary.IsSelected)
            {
                dgRenewalsBinding();
            }
            else
            {
                dgForeingWorkersRenewalsBinding();
            }

            if (renewalToSelect != null && lv != null)
            {
                SelectRenewalInDg(renewalToSelect, lv);
            }
        }

        private void btnPolicyDetails_Click(object sender, RoutedEventArgs e)
        {
            ListView lv = null;
            object renewalToSelect = null;
            if (tbItemElementary.IsSelected)
            {
                if (dgRenewals.SelectedItem == null)
                {
                    MessageBox.Show("נא לסמן פוליסה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                }
                else
                {
                    lv = dgRenewals;
                    renewalToSelect = dgRenewals.SelectedItem;
                    client = clientLogic.GetClientByClientID(((ElementaryPolicy)dgRenewals.SelectedItem).ClientID);
                    frmElementary policyDetails = new frmElementary((ElementaryPolicy)dgRenewals.SelectedItem, user, false, client);
                    policyDetails.ShowDialog();
                }
            }
            else
            {
                if (dgForeingWorkersRenewals.SelectedItem == null)
                {
                    MessageBox.Show("נא לסמן פוליסה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                }
                else
                {
                    lv = dgForeingWorkersRenewals;
                    renewalToSelect = dgForeingWorkersRenewals.SelectedItem;
                    client = clientLogic.GetClientByClientID(((ForeingWorkersPolicy)dgForeingWorkersRenewals.SelectedItem).ClientID);
                    frmForeignWorkers policyDetails = new frmForeignWorkers(client, user, false, ((ForeingWorkersPolicy)dgForeingWorkersRenewals.SelectedItem));
                    policyDetails.ShowDialog();
                }
            }
            if (tbItemElementary.IsSelected)
            {
                dgRenewalsBinding();
            }
            else
            {
                dgForeingWorkersRenewalsBinding();
            }

            if (renewalToSelect != null && lv != null)
            {
                SelectRenewalInDg(renewalToSelect, lv);
            }
        }

        private void btnNewRenewal_Click(object sender, RoutedEventArgs e)
        {
            object renewalSelected = null;
            ListView lv = null;
            if (tbItemElementary.IsSelected)
            {
                if (dgRenewals.SelectedItem == null)
                {
                    MessageBox.Show("נא לסמן פוליסה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                }
                //else if (((ElementaryPolicy)dgRenewals.SelectedItem).RenewalStatus.RenewalStatusName == "חודש")
                //{
                //    //MessageBox.Show("פוליסה מחודשת. לביצוע שינוים נא כנס דרך חלון הפוליסות", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                //}
                else
                {
                    renewalSelected = (ElementaryPolicy)dgRenewals.SelectedItem;
                    lv = dgRenewals;
                    client = clientLogic.GetClientByClientID(((ElementaryPolicy)dgRenewals.SelectedItem).ClientID);
                    if (((ElementaryPolicy)dgRenewals.SelectedItem).InsuranceIndustry.InsuranceIndustryName.Contains("רכב"))
                    {
                        frmNewRenewal carRenewal;
                        carRenewal = new frmNewRenewal(client, (ElementaryPolicy)dgRenewals.SelectedItem, user);
                        carRenewal.ShowDialog();
                    }
                    else
                    {
                        frmApartmentRenewal aptOrBusinessRenewal;
                        aptOrBusinessRenewal = new frmApartmentRenewal(client, (ElementaryPolicy)dgRenewals.SelectedItem, user);
                        aptOrBusinessRenewal.ShowDialog();
                    }
                    dgRenewalsBinding();
                }
            }
            else
            {

                if (dgForeingWorkersRenewals.SelectedItem == null)
                {
                    MessageBox.Show("נא לסמן פוליסה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                }

                //else if (((ForeingWorkersPolicy)dgForeingWorkersRenewals.SelectedItem).RenewalStatus.RenewalStatusName == "חודש")
                //{
                //    MessageBox.Show("פוליסה מחודשת. לביצוע שינוים נא כנס דרך חלון הפוליסות", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                //}
                else
                {
                    renewalSelected = (ForeingWorkersPolicy)dgForeingWorkersRenewals.SelectedItem;
                    lv = dgForeingWorkersRenewals;
                    client = clientLogic.GetClientByClientID(((ForeingWorkersPolicy)dgForeingWorkersRenewals.SelectedItem).ClientID);
                    frmForeingWorkersRenewal renewal = new frmForeingWorkersRenewal(client, (ForeingWorkersPolicy)dgForeingWorkersRenewals.SelectedItem, user);
                    renewal.ShowDialog();
                    dgForeingWorkersRenewalsBinding();
                }
            }
            if (renewalSelected != null && lv != null)
            {
                SelectRenewalInDg(renewalSelected, lv);
            }
        }

        private void SelectRenewalInDg(object renewalToSelect, ListView lv)
        {
            var dgItems = lv.Items;
            foreach (var item in dgItems)
            {
                if (renewalToSelect is ElementaryPolicy)
                {
                    ElementaryPolicy policy = (ElementaryPolicy)item;
                    if (policy.ElementaryPolicyID == ((ElementaryPolicy)renewalToSelect).ElementaryPolicyID)
                    {
                        lv.SelectedItem = item;
                        lv.ScrollIntoView(item);
                        ListViewItem listViewItem = lv.ItemContainerGenerator.ContainerFromItem(lv.SelectedItem) as ListViewItem;
                        if (listViewItem != null)
                        {
                            listViewItem.Focus();
                        }
                        break;
                    }
                }
                else if (renewalToSelect is ForeingWorkersPolicy)
                {
                    ForeingWorkersPolicy policy = (ForeingWorkersPolicy)item;
                    if (policy.ForeingWorkersPolicyID == ((ForeingWorkersPolicy)renewalToSelect).ForeingWorkersPolicyID)
                    {
                        lv.SelectedItem = item;
                        lv.ScrollIntoView(item);
                        ListViewItem listViewItem = lv.ItemContainerGenerator.ContainerFromItem(lv.SelectedItem) as ListViewItem;
                        if (listViewItem != null)
                        {
                            listViewItem.Focus();
                        }
                        break;
                    }
                }

            }
        }

        private void btnOpticArchive_Click(object sender, RoutedEventArgs e)
        {
            if (dgRenewals.SelectedItem == null)
            {
                MessageBox.Show("נא לסמן פוליסה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            client = clientLogic.GetClientByClientID(((ElementaryPolicy)dgRenewals.SelectedItem).ClientID);
            frmScan2 archiveWindow = new frmScan2(client, (ElementaryPolicy)dgRenewals.SelectedItem, null, user);
            archiveWindow.ShowDialog();

        }

        private void dgRenewals_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            dgRenewals.UnselectAll();
        }

        //private void btnFilterRenewal_Click(object sender, RoutedEventArgs e)
        //{
        //    dgRenewals.ItemsSource = policyLogic.GetPoliciesToRenewByFilter((InsuranceCompany)cbInsuranceCompany.SelectedItem, (Category)cbCategory.SelectedItem, (DateTime)dtxtPeriod.Value, (InsuranceIndustry)cbPolicyType.SelectedItem, (ClientType)cbClientType.SelectedItem, (RenewalStatus)cbStatus.SelectedItem);
        //}

        private void btnClear_Click(object sender, RoutedEventArgs e)
        {
            cbInsuranceCompany.SelectedIndex = -1;
            cbCategory.SelectedIndex = -1;
            cbPolicyType.SelectedIndex = -1;
            //cbClientType.SelectedIndex = -1;
            cbStatus.SelectedIndex = -1;
            lblIndustry.Content = "הכל";
            lblCompany.Content = "הכל";
            txtSearch.Clear();
            //dgRenewalsBinding();
        }

        private void cbPolicyType_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (tbItemElementary.IsSelected)
            {
                dgRenewalsBinding();
            }
            else
            {
                dgForeingWorkersRenewalsBinding();
            }
        }

        private void DisplayPoliciesByFilter()
        {
            //string policyStatus = null;
            //if (cbPolicyStatus.SelectedItem != null)
            //{
            //    policyStatus = ((ComboBoxItem)cbPolicyStatus.SelectedItem).Content.ToString();

            //}
            //dgRenewals.ItemsSource = policyLogic.GetPoliciesToRenewByFilter((InsuranceCompany)cbInsuranceCompany.SelectedItem, (Category)cbCategory.SelectedItem, (DateTime)dtxtPeriod.Value, (InsuranceIndustry)cbPolicyType.SelectedItem, policyStatus, (RenewalStatus)cbStatus.SelectedItem);
            //MarketingInfoBinding();
        }

        private void cbPolicyType_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {

            //foreach (var item in cbPolicyType.Items)
            //{
            //    ComboBoxItem i = (ComboBoxItem)item;
            //    if (i.Content.ToString().ToUpper().StartsWith(e.Text.ToUpper()))
            //    {
            //        cbPolicyType.SelectedItem = i;
            //        break;
            //    }
            //}
            //e.Handled = true;
            var industries = cbPolicyType.Items;
            foreach (var item in industries)
            {
                InsuranceIndustry industry = (InsuranceIndustry)item;
                if (industry.InsuranceIndustryName.ToUpper().StartsWith(e.Text.ToUpper()))
                {
                    cbPolicyType.SelectedItem = item;
                    cbPolicyType.IsDropDownOpen = true;
                    break;
                }
            }
            //cbIndustry.IsEnabled = false;
        }

        private void btnExcel_Click(object sender, RoutedEventArgs e)
        {
            //dgRenewals.SelectAllCells();
            //dgRenewals.ClipboardCopyMode = DataGridClipboardCopyMode.IncludeHeader;
            //ApplicationCommands.Copy.Execute(null, dgRenewals);
            //String resultat = (string)Clipboard.GetData(DataFormats.CommaSeparatedValue);
            //String result = (string)Clipboard.GetData(DataFormats.Text);
            //dgRenewals.UnselectAllCells();
            //Encoding utf8 = Encoding.UTF8;
            //System.IO.StreamWriter file = new System.IO.StreamWriter(@"C:\Users\test.xls", false, utf8);
            //file.WriteLine(result.Replace(',', ' '));
            //file.Close();

            //MessageBox.Show(" Exporting DataGrid data to Excel file created");
        }

        private void dtxtPeriod_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            if (dtxtPeriod.Value != null)
            {
                lblMonthYear.Content = ((DateTime)dtxtPeriod.Value).ToString("MM/yyyy");
            }
            else
            {
                lblMonthYear.Content = "הכל";
            }
            if (tbItemElementary.IsSelected)
            {
                dgRenewalsBinding();
            }
            else
            {
                dgForeingWorkersRenewalsBinding();
            }

            ////dgRenewalsBinding((DateTime)dtxtPeriod.Value);
            //string policyStatus = null;
            //if (cbPolicyStatus.SelectedItem != null)
            //{
            //    policyStatus = ((ComboBoxItem)cbPolicyStatus.SelectedItem).Content.ToString();

            //}
            //dgRenewals.ItemsSource = policyLogic.GetPoliciesToRenewByFilter((InsuranceCompany)cbInsuranceCompany.SelectedItem, (Category)cbCategory.SelectedItem, (DateTime)dtxtPeriod.Value, (InsuranceIndustry)cbPolicyType.SelectedItem, policyStatus, (RenewalStatus)cbStatus.SelectedItem);
            //MarketingInfoBinding();
        }

        private void btnSendEmail_Click(object sender, RoutedEventArgs e)
        {
            if (tbItemElementary.IsSelected)
            {
                if (dgRenewals.SelectedItem == null)
                {
                    MessageBox.Show("נא לסמן פוליסה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                }
                else
                {
                    client = clientLogic.GetClientByClientID(((ElementaryPolicy)dgRenewals.SelectedItem).ClientID);
                }
            }
            else
            {
                if (dgForeingWorkersRenewals.SelectedItem == null)
                {
                    MessageBox.Show("נא לסמן פוליסה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                }
                else
                {
                    client = clientLogic.GetClientByClientID(((ForeingWorkersPolicy)dgForeingWorkersRenewals.SelectedItem).ClientID);
                }
            }

            if (client != null)
            {
                if (client.Email == "")
                {
                    MessageBox.Show("אין ללקוח כתובת דוא''ל במערכת", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    return;
                }
                string[] address = new string[] { client.Email };
                try
                {
                    email.SendEmailFromOutlook("", "", null, address);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        private void mItemForeingWorkersExcel_Click(object sender, RoutedEventArgs e)
        {
            excelLogic.ForeingWorkersPoliciesToExcel((List<ForeingWorkersPolicy>)dgForeingWorkersRenewals.ItemsSource);
        }
        private void mItemExcel_Click(object sender, RoutedEventArgs e)
        {
            excelLogic.ElementaryPoliciesToExcel((List<ElementaryPolicy>)dgRenewals.ItemsSource);
        }

        private void txtSearchClaim_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (tbItemElementary.IsSelected)
            {
                dgRenewalsBinding();
            }
            else
            {
                dgForeingWorkersRenewalsBinding();
            }
        }

        private void dgRenewals_MouseDown(object sender, MouseButtonEventArgs e)
        {
            (sender as ListView).UnselectAll();
        }

        private void dgRenewals_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            DependencyObject src = (DependencyObject)(e.OriginalSource);
            while (!(src is Control))
            {
                if (!(src is Visual))
                {
                    break;
                }
                src = VisualTreeHelper.GetParent(src);
            }

            if ((src is Control && src.GetType() == typeof(ListViewItem)) || src.GetType() == typeof(Run))
            {
                btnNewRenewal_Click(sender, new RoutedEventArgs());
            }
        }

        private void mItemPrintElementary_Click(object sender, RoutedEventArgs e)
        {
            List<RenewalsReport> reportList = null;
            if (tbItemElementary.IsSelected)
            {
                reportList = ConvertToReportView((List<ElementaryPolicy>)dgRenewals.ItemsSource);
            }
            else if (tbItemForeingWorkers.IsSelected)
            {
                reportList = ConvertToReportView((List<ForeingWorkersPolicy>)dgForeingWorkersRenewals.ItemsSource);
            }

            if (reportList != null)
            {
                try
                {
                    frmPrintPreview preview = new frmPrintPreview("RenewalsReport", reportList);
                    preview.ShowDialog();

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private List<RenewalsReport> ConvertToReportView(object list)
        {
            List<RenewalsReport> reportList = new List<RenewalsReport>();
            if (list is List<ElementaryPolicy>)
            {
                foreach (var item in (List<ElementaryPolicy>)list)
                {
                    RenewalsReport reportItem = new RenewalsReport();
                    if (item.Client != null)
                    {
                        reportItem.ClientName = item.Client.LastName + " " + item.Client.FirstName;
                        reportItem.IdNumber = item.Client.IdNumber;
                    }
                    if (item.Company != null)
                    {
                        reportItem.CompanyName = item.Company.CompanyName;
                    }
                    if (item.EndDate != null)
                    {
                        reportItem.EndDate = ((DateTime)item.EndDate).ToString("dd/MM/yyyy");
                    }
                    if (item.InsuranceIndustry != null)
                    {
                        reportItem.Industry = item.InsuranceIndustry.InsuranceIndustryName;
                    }
                    if (item.IsMortgaged == true)
                    {
                        reportItem.Mortgaged = "כן";
                    }
                    else
                    {
                        reportItem.Mortgaged = "לא";
                    }
                    reportItem.PolicyNumber = item.PolicyNumber;
                    if (item.TotalPremium != null)
                    {
                        reportItem.Premium = ((decimal)item.TotalPremium).ToString("0.##");
                    }
                    if (item.CarPolicy != null)
                    {
                        reportItem.RegistrationNumber = item.CarPolicy.RegistrationNumber;
                    }
                    if (item.StartDate != null)
                    {
                        reportItem.StartDate = ((DateTime)item.StartDate).ToString("dd/MM/yyyy");
                    }
                    if (item.RenewalStatus != null)
                    {
                        reportItem.Status = item.RenewalStatus.RenewalStatusName;
                    }
                    reportItem.Header = string.Format(@"רשימת חידושים לתקופה: {0} {4} ענף: {1}, ח.ביטוח: {2} {4} {3}",lblMonthYear.Content, lblIndustry.Content, lblCompany.Content,lblPremium.Text, Environment.NewLine);
                    //reportItem.Header = string.Format(@"רשימת חידושים לתקופה: {0} 
                    //ענף: {1} ח.ביטוח: {2} 
                    //{3}"
                    //,lblMonthYear.Content, lblIndustry.Content, lblCompany.Content, lblPremium.Text);

                    reportList.Add(reportItem);
                }
            }
            else if (list is List<ForeingWorkersPolicy>)
            {
                foreach (var item in (List<ForeingWorkersPolicy>)list)
                {
                    RenewalsReport reportItem = new RenewalsReport();
                    if (item.Client != null)
                    {
                        reportItem.ClientName = item.Client.LastName + " " + item.Client.FirstName;
                        reportItem.IdNumber = item.Client.IdNumber;
                    }
                    if (item.Company != null)
                    {
                        reportItem.CompanyName = item.Company.CompanyName;
                    }
                    if (item.EndDate != null)
                    {
                        reportItem.EndDate = ((DateTime)item.EndDate).ToString("dd/MM/yyyy");
                    }
                    if (item.ForeingWorkersIndustry != null)
                    {
                        reportItem.Industry = item.ForeingWorkersIndustry.ForeingWorkersIndustryName;
                    }
                    reportItem.PolicyNumber = item.PolicyNumber;
                    if (item.TotalPremium != null)
                    {
                        reportItem.Premium = ((decimal)item.TotalPremium).ToString("0.##");
                    }
                    if (item.StartDate != null)
                    {
                        reportItem.StartDate = ((DateTime)item.StartDate).ToString("dd/MM/yyyy");
                    }
                    if (item.RenewalStatus != null)
                    {
                        reportItem.Status = item.RenewalStatus.RenewalStatusName;
                    }
                    reportItem.Header = string.Format(@"רשימת חידושים לתקופה: {0} {4} ענף: {1}, ח.ביטוח: {2} {4} {3}", lblMonthYear.Content, lblIndustry.Content, lblCompany.Content, lblPremium.Text, Environment.NewLine);

                    reportList.Add(reportItem);
                }
            }
            return reportList;
        }
    }




}
