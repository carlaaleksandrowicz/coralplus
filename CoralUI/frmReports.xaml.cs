﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using CoralBusinessLogics;

namespace CoralUI
{
    /// <summary>
    /// Interaction logic for frmReports.xaml
    /// </summary>
    public partial class frmReports : Window
    {
        InsurancesLogic insuranceLogic = new InsurancesLogic();
        IndustriesLogic elementaryIndustryLogic = new IndustriesLogic();
        LifeIndustryLogic lifeIndustryLogic = new LifeIndustryLogic();
        ForeingWorkersIndutryLogic foreingWorkersIndustryLogic = new ForeingWorkersIndutryLogic();
        FinanceIndustryLogic financeIndustryLogic = new FinanceIndustryLogic();
        HealthIndustriesLogic healthIndustryLogic = new HealthIndustriesLogic();
        FundsLogic fundLogic = new FundsLogic();
        ClientsLogic clientLogic = new ClientsLogic();
        AgentsLogic agentLogic = new AgentsLogic();
        PoliciesLogic elementaryLogic = new PoliciesLogic();
        PersonalAccidentsLogic personalAccidentsLogic = new PersonalAccidentsLogic();
        TravelLogic travelLogic = new TravelLogic();
        LifePoliciesLogic lifeLogic = new LifePoliciesLogic();
        HealthPoliciesLogic healthLogic = new HealthPoliciesLogic();
        FinancePoliciesLogic financeLogic = new FinancePoliciesLogic();
        ForeingWorkersLogic foreingWorkersLogic = new ForeingWorkersLogic();
        List<ElementaryPolicy> elementaryPolicies;
        List<PersonalAccidentsPolicy> personalAccidentsPolicies;
        List<TravelPolicy> travelPolicies;
        List<LifePolicy> lifePolicies;
        List<HealthPolicy> healthPolicies;
        int selected;
        List<FinancePolicy> financePolicies;
        User user;
        ListViewSettings lv = new ListViewSettings();
        List<Client> clients;
        List<Client> filteredClients;
        DateTime reportFromDate;
        DateTime reportToDate;
        private List<Client> filteredSegmentation;
        InputsValidations validations = new InputsValidations();
        Email email = new Email();
        ExcelLogic excelLogic = new ExcelLogic();
        private List<ForeingWorkersPolicy> foreingWorkersPolicies;

        public frmReports(User user)
        {
            InitializeComponent();
            this.SizeToContent = SizeToContent.Manual;
            this.user = user;
        }
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            //מגדיר כברירת מחדל בקונטרולרים "מתאריך" ו"עד תאריך" תחילה וסוף החודש הנוכחי
            reportFromDate = SetFirstDayOfMonth().AddYears(-1);
            dpFromDate.SelectedDate = reportFromDate;
            reportToDate = SetLastDayOfMonth().AddMonths(-1);
            dpToDate.SelectedDate = reportToDate;

            //GetPolicies();
            //GetAllClients();
            cbInsuranceBinding();
            cbCompanyBinding(null);
            cbAgentBinding();
            cbClientTypeBinding();
            rbPoliciesReport.IsChecked = true;
        }

        private void GetAllClients()
        {
            clients = clientLogic.GetAllClients();
        }

        private void dgIsntReportsBinding()
        {
            //filteredClients = clients.Where(c => c.ClientTypeID == (((ClientType)cbClientsClientType.SelectedItem).ClientTypeID)).ToList();
            filteredClients = clientLogic.GetAllClientsByType(((ClientType)cbClientsClientType.SelectedItem).ClientTypeName);

            if (cbClientsAgent.SelectedItem != null)
            {
                filteredClients = filteredClients.Where(c => c.PrincipalAgentID == (((Agent)cbClientsAgent.SelectedItem).AgentID) || c.SecundaryAgentID == (((Agent)cbClientsAgent.SelectedItem).AgentID)).ToList();
            }
            string insuranceName = null;
            if (cbClientsInsurance.SelectedItem != null)
            {
                insuranceName = ((Insurance)cbClientsInsurance.SelectedItem).InsuranceName;
            }
            List<FinancePolicy> financeActive;
            List<HealthPolicy> healthActive;
            List<ElementaryPolicy> elementaryActive;
            List<PersonalAccidentsPolicy> personalAccidentsActive;
            List<TravelPolicy> travelActive;
            List<LifePolicy> lifeActive;
            List<ForeingWorkersPolicy> foreingWorkersActive;

            if (insuranceName != null)
            {
                List<Client> clientsByInsurance = new List<Client>();
                clientsByInsurance.AddRange(filteredClients);

                switch (insuranceName)
                {
                    //case null:
                    //    financeActive = financeLogic.GetActivePoliciesWithoutOffers();
                    //    healthActive = healthLogic.GetActivePoliciesWithoutOffers();
                    //    elementaryActive = elementaryLogic.GetActivePoliciesWithoutOffers();
                    //    personalAccidentsActive = personalAccidentsLogic.GetActivePoliciesWithoutOffers();
                    //    travelActive = travelLogic.GetActivePoliciesWithoutOffers();
                    //    lifeActive = lifeLogic.GetActivePoliciesWithoutOffers();
                    //    break;
                    case "אלמנטרי":
                        elementaryActive = elementaryLogic.GetActivePoliciesWithoutOffers();
                        foreach (var item in clientsByInsurance)
                        {
                            if (cbClientsInsuranceType.SelectedItem != null)
                            {
                                int id = ((ElementaryInsuranceType)cbClientsInsuranceType.SelectedItem).ElementaryInsuranceTypeID;
                                elementaryActive = elementaryActive.Where(p => p.ElementaryInsuranceTypeID == id).ToList();
                            }
                            else if (cbClientsIndustry.SelectedItem != null)
                            {
                                int id = ((InsuranceIndustry)cbClientsIndustry.SelectedItem).InsuranceIndustryID;
                                elementaryActive = elementaryActive.Where(p => p.InsuranceIndustryID == id).ToList();
                            }
                            var clientPolicy = elementaryActive.FirstOrDefault(p => p.ClientID == item.ClientID);
                            if (clientPolicy != null)
                            {
                                filteredClients.Remove(item);
                            }
                        }
                        break;
                    case "תאונות אישיות":
                        personalAccidentsActive = personalAccidentsLogic.GetActivePoliciesWithoutOffers();
                        foreach (var item in clientsByInsurance)
                        {
                            var clientPolicy = personalAccidentsActive.FirstOrDefault(p => p.ClientID == item.ClientID);
                            if (clientPolicy != null)
                            {
                                filteredClients.Remove(item);
                            }
                        }
                        break;
                    case "נסיעות לחו''ל":
                        travelActive = travelLogic.GetActivePoliciesWithoutOffers();
                        foreach (var item in clientsByInsurance)
                        {
                            var clientPolicy = travelActive.FirstOrDefault(p => p.ClientID == item.ClientID);
                            if (clientPolicy != null)
                            {
                                filteredClients.Remove(item);
                            }
                        }
                        break;
                    case "חיים":
                        lifeActive = lifeLogic.GetActivePoliciesWithoutOffers();
                        foreach (var item in clientsByInsurance)
                        {
                            if (cbClientsInsuranceType.SelectedItem != null)
                            {
                                int id = ((FundType)cbClientsInsuranceType.SelectedItem).FundTypeID;
                                lifeActive = lifeActive.Where(p => p.FundTypeID == id).ToList();
                            }
                            else if (cbClientsIndustry.SelectedItem != null)
                            {
                                int id = ((LifeIndustry)cbClientsIndustry.SelectedItem).LifeIndustryID;
                                lifeActive = lifeActive.Where(p => p.LifeIndustryID == id).ToList();
                            }
                            var clientPolicy = lifeActive.FirstOrDefault(p => p.ClientID == item.ClientID);
                            if (clientPolicy != null)
                            {
                                filteredClients.Remove(item);
                            }
                        }
                        break;
                    case "בריאות":
                        healthActive = healthLogic.GetActivePoliciesWithoutOffers();
                        foreach (var item in clientsByInsurance)
                        {
                            if (cbClientsInsuranceType.SelectedItem != null)
                            {
                                int id = ((HealthInsuranceType)cbClientsInsuranceType.SelectedItem).HealthInsuranceTypeID;
                                healthActive = healthActive.Where(p => p.HealthInsuranceTypeID == id).ToList();
                            }
                            var clientPolicy = healthActive.FirstOrDefault(p => p.ClientID == item.ClientID);
                            if (clientPolicy != null)
                            {
                                filteredClients.Remove(item);
                            }
                        }
                        break;
                    case "פיננסים":
                        financeActive = financeLogic.GetActivePoliciesWithoutOffers();
                        foreach (var item in clientsByInsurance)
                        {
                            if (cbClientsInsuranceType.SelectedItem != null)
                            {
                                int id = ((FinanceProgram)cbClientsInsuranceType.SelectedItem).ProgramID;
                                financeActive = financeActive.Where(p => p.ProgramID == id).ToList();
                            }
                            else if (cbClientsIndustry.SelectedItem != null)
                            {
                                int id = ((FinanceProgramType)cbClientsIndustry.SelectedItem).ProgramTypeID;
                                financeActive = financeActive.Where(p => p.ProgramTypeID == id).ToList();
                            }
                            var clientPolicy = financeActive.FirstOrDefault(p => p.ClientID == item.ClientID);
                            if (clientPolicy != null)
                            {
                                filteredClients.Remove(item);
                            }
                        }
                        break;
                    case "עובדים זרים":
                        foreingWorkersActive = foreingWorkersLogic.GetActivePoliciesWithoutOffers();
                        foreach (var item in clientsByInsurance)
                        {
                            if (cbClientsInsuranceType.SelectedItem != null)
                            {
                                int id = ((ForeingWorkersInsuranceType)cbClientsInsuranceType.SelectedItem).ForeingWorkersInsuranceTypeID;
                                foreingWorkersActive = foreingWorkersActive.Where(p => p.ForeingWorkersInsuranceTypeID == id).ToList();
                            }
                            else if (cbClientsIndustry.SelectedItem != null)
                            {
                                int id = ((ForeingWorkersIndustry)cbClientsIndustry.SelectedItem).ForeingWorkersIndustryID;
                                foreingWorkersActive = foreingWorkersActive.Where(p => p.ForeingWorkersIndustryID == id).ToList();
                            }
                            var clientPolicy = foreingWorkersActive.FirstOrDefault(p => p.ClientID == item.ClientID);
                            if (clientPolicy != null)
                            {
                                filteredClients.Remove(item);
                            }
                        }
                        break;
                }
            }
            dgClientsReports.ItemsSource = filteredClients;
            SetlblClientsFilterDetails();
        }

        private void cbAgentBinding()
        {
            var agents = agentLogic.GetAllAgents();
            cbAgent.ItemsSource = agents.ToList();
            cbClientsAgent.ItemsSource = agents.ToList();
            cbSegmentationAgent.ItemsSource = agents.ToList();
        }

        private void cbClientTypeBinding()
        {
            var clientTypes = clientLogic.GetAllTypes();
            cbClientType.ItemsSource = clientTypes.Where(c => c.ClientTypeName != "פוטנציאליים").ToList();
            cbClientType.DisplayMemberPath = "ClientTypeName";
            SelectClientType(cbClientType);
            cbClientsClientType.ItemsSource = clientTypes.ToList();
            cbClientsClientType.DisplayMemberPath = "ClientTypeName";
            SelectClientType(cbClientsClientType);
            cbSegmentationClientType.ItemsSource = clientTypes.ToList();
            cbSegmentationClientType.DisplayMemberPath = "ClientTypeName";
            SelectClientType(cbSegmentationClientType);
        }

        private void SelectClientType(ComboBox cb)
        {
            var types = cb.Items;
            foreach (var item in types)
            {
                ClientType type = (ClientType)item;
                if (type.ClientTypeName == "לקוחות")
                {
                    cb.SelectedItem = item;
                    break;
                }
            }
        }

        private void GetPolicies()
        {
            elementaryPolicies = elementaryLogic.GetAllElementaryPoliciesWithoutOffers();
            personalAccidentsPolicies = personalAccidentsLogic.GetAllPersonalAccidentsPoliciesWithoutOffers();
            travelPolicies = travelLogic.GetAllTravelPoliciesWithoutOffers();
            lifePolicies = lifeLogic.GetAllLifePoliciesWithoutOffers();
            healthPolicies = healthLogic.GetAllHealthPoliciesWithoutOffers();
            financePolicies = financeLogic.GetAllFinancePoliciesWithoutOffers();
            foreingWorkersPolicies = foreingWorkersLogic.GetAllForeingWorkersPoliciesWithoutOffers();
        }

        private void dgReportsBinding()
        {
            dpFromDate.SelectedDate = reportFromDate;
            dpToDate.SelectedDate = reportToDate;
            List<ElementaryPolicy> elementaryFiltered;
            List<PersonalAccidentsPolicy> personalAccidentsFiltered;
            List<TravelPolicy> travelFiltered;
            List<LifePolicy> lifeFiltered;
            List<HealthPolicy> healthFiltered;
            List<FinancePolicy> financeFiltered;
            List<ForeingWorkersPolicy> foreingWorkersFiltered;

            List<object> filteredPolicies = new List<object>();

            decimal premium = 0;

            string insuranceName = null;
            if (cbInsurance.SelectedItem != null)
            {
                insuranceName = ((Insurance)cbInsurance.SelectedItem).InsuranceName;
            }
            int? companyId = null;
            if (cbInsuranceCompany.SelectedItem != null)
            {
                if (cbInsurance.SelectedItem == null)
                {
                    companyId = ((Company)cbInsuranceCompany.SelectedItem).CompanyID;
                }
                else
                {
                    companyId = ((InsuranceCompany)cbInsuranceCompany.SelectedItem).CompanyID;
                }
            }
            int? clientTypeid = null;
            if (cbClientType.SelectedItem != null)
            {
                clientTypeid = ((ClientType)cbClientType.SelectedItem).ClientTypeID;
            }
            int? agentId = null;
            if (cbAgent.SelectedItem != null)
            {
                agentId = ((Agent)cbAgent.SelectedItem).AgentID;
            }

            if (user.IsElementaryPermission != false && (insuranceName == "אלמנטרי" || insuranceName == null))
            {
                //elementaryFiltered = elementaryPolicies.Where(p =>p.StartDate < dpFromDate.SelectedDate && p.EndDate >= dpFromDate.SelectedDate).ToList();
                elementaryFiltered = elementaryLogic.GetPoliciesWithoutOffersByStartDate((DateTime)dpFromDate.SelectedDate, dpToDate.SelectedDate);
                if (companyId != null)
                {
                    elementaryFiltered = elementaryFiltered.Where(p => p.CompanyID == companyId).ToList();
                }
                if (cbIndustry.SelectedItem != null)
                {
                    int id = ((InsuranceIndustry)cbIndustry.SelectedItem).InsuranceIndustryID;
                    elementaryFiltered = elementaryFiltered.Where(p => p.InsuranceIndustryID == id).ToList();
                }
                if (cbInsuranceType.SelectedItem != null && cbInsuranceType.SelectedItem is ElementaryInsuranceType)
                {
                    int id = ((ElementaryInsuranceType)cbInsuranceType.SelectedItem).ElementaryInsuranceTypeID;
                    elementaryFiltered = elementaryFiltered.Where(p => p.ElementaryInsuranceTypeID == id).ToList();
                }
                if (clientTypeid != null)
                {
                    elementaryFiltered = elementaryFiltered.Where(p => p.Client.ClientTypeID == clientTypeid).ToList();
                }
                if (agentId != null)
                {
                    elementaryFiltered = elementaryFiltered.Where(p => (p.Client.Agent != null && p.Client.Agent.AgentID == agentId) || (p.Client.Agent1 != null && p.Client.Agent1.AgentID == agentId)).ToList();
                }
                premium = premium + elementaryFiltered.Where(p => p.TotalPremium != null).Sum(p => (decimal)p.TotalPremium);
                foreach (var item in elementaryFiltered)
                {
                    filteredPolicies.Add(item);
                }
            }
            if (user.IsPersonalAccidentsPermission != false && (insuranceName == "תאונות אישיות" || insuranceName == null))
            {
                //personalAccidentsFiltered = personalAccidentsPolicies.Where(p => p.StartDate < dpFromDate.SelectedDate && p.EndDate >= dpFromDate.SelectedDate).ToList();
                personalAccidentsFiltered = personalAccidentsLogic.GetPoliciesWithoutOffersByStartDate((DateTime)dpFromDate.SelectedDate, dpToDate.SelectedDate);

                if (companyId != null)
                {
                    personalAccidentsFiltered = personalAccidentsFiltered.Where(p => p.CompanyID == companyId).ToList();
                }
                if (clientTypeid != null)
                {
                    personalAccidentsFiltered = personalAccidentsFiltered.Where(p => p.Client.ClientTypeID == clientTypeid).ToList();
                }
                if (agentId != null)
                {
                    personalAccidentsFiltered = personalAccidentsFiltered.Where(p => (p.Client.Agent != null && p.Client.Agent.AgentID == agentId) || (p.Client.Agent1 != null && p.Client.Agent1.AgentID == agentId)).ToList();
                }

                premium = premium + (personalAccidentsFiltered.Sum(p => (decimal)p.TotalPremium)) * 12;

                foreach (var item in personalAccidentsFiltered)
                {
                    filteredPolicies.Add(item);
                }
            }
            if (user.IsTravelPermission != false && (insuranceName == "נסיעות לחו''ל" || insuranceName == null))
            {
                //travelFiltered = travelPolicies.Where(p => p.StartDate < dpFromDate.SelectedDate && p.EndDate >= dpFromDate.SelectedDate).ToList();
                travelFiltered = travelLogic.GetPoliciesWithoutOffersByStartDate((DateTime)dpFromDate.SelectedDate, dpToDate.SelectedDate);

                if (companyId != null)
                {
                    travelFiltered = travelFiltered.Where(p => p.CompanyID == companyId).ToList();
                }
                if (clientTypeid != null)
                {
                    travelFiltered = travelFiltered.Where(p => p.Client.ClientTypeID == clientTypeid).ToList();
                }
                if (agentId != null)
                {
                    travelFiltered = travelFiltered.Where(p => (p.Client.Agent != null && p.Client.Agent.AgentID == agentId) || (p.Client.Agent1 != null && p.Client.Agent1.AgentID == agentId)).ToList();
                }

                premium = premium + travelFiltered.Sum(p => (decimal)p.Premium);

                foreach (var item in travelFiltered)
                {
                    filteredPolicies.Add(item);
                }
            }
            if (user.IsLifePermission != false && (insuranceName == "חיים" || insuranceName == null))
            {
                //lifeFiltered = lifePolicies.Where(p => p.StartDate < dpFromDate.SelectedDate && p.EndDate >= dpFromDate.SelectedDate).ToList();
                lifeFiltered = lifeLogic.GetPoliciesWithoutOffersByStartDate((DateTime)dpFromDate.SelectedDate, dpToDate.SelectedDate);

                if (companyId != null)
                {
                    lifeFiltered = lifeFiltered.Where(p => p.CompanyID == companyId).ToList();
                }
                if (cbIndustry.SelectedItem != null)
                {
                    int id = ((LifeIndustry)cbIndustry.SelectedItem).LifeIndustryID;
                    lifeFiltered = lifeFiltered.Where(p => p.LifeIndustryID == id).ToList();
                }
                if (cbInsuranceType.SelectedItem != null)
                {
                    int id = ((FundType)cbInsuranceType.SelectedItem).FundTypeID;
                    lifeFiltered = lifeFiltered.Where(p => p.FundTypeID == id).ToList();
                }
                if (clientTypeid != null)
                {
                    lifeFiltered = lifeFiltered.Where(p => p.Client.ClientTypeID == clientTypeid).ToList();
                }
                if (agentId != null)
                {
                    lifeFiltered = lifeFiltered.Where(p => (p.Client.Agent != null && p.Client.Agent.AgentID == agentId) || (p.Client.Agent1 != null && p.Client.Agent1.AgentID == agentId)).ToList();
                }

                premium = premium + (lifeFiltered.Sum(p => (decimal)p.TotalMonthlyPayment)) * 12;

                foreach (var item in lifeFiltered)
                {
                    filteredPolicies.Add(item);
                }
            }
            if (user.IsHealthPermission != false && (insuranceName == "בריאות" || insuranceName == null))
            {
                //healthFiltered = healthPolicies.Where(p => p.StartDate < dpFromDate.SelectedDate && p.EndDate >= dpFromDate.SelectedDate).ToList();
                healthFiltered = healthLogic.GetPoliciesWithoutOffersByStartDate((DateTime)dpFromDate.SelectedDate, dpToDate.SelectedDate);

                if (companyId != null)
                {
                    healthFiltered = healthFiltered.Where(p => p.CompanyID == companyId).ToList();
                }
                if (cbInsuranceType.SelectedItem != null)
                {
                    int id = ((HealthInsuranceType)cbInsuranceType.SelectedItem).HealthInsuranceTypeID;
                    healthFiltered = healthFiltered.Where(p => p.HealthInsuranceTypeID == id).ToList();
                }
                if (clientTypeid != null)
                {
                    healthFiltered = healthFiltered.Where(p => p.Client.ClientTypeID == clientTypeid).ToList();
                }
                if (agentId != null)
                {
                    healthFiltered = healthFiltered.Where(p => (p.Client.Agent != null && p.Client.Agent.AgentID == agentId) || (p.Client.Agent1 != null && p.Client.Agent1.AgentID == agentId)).ToList();
                }

                premium = premium + (healthFiltered.Sum(p => (decimal)p.TotalMonthlyPayment)) * 12;

                foreach (var item in healthFiltered)
                {
                    filteredPolicies.Add(item);
                }
            }
            if (user.IsFinancePermission != false && (insuranceName == "פיננסים" || insuranceName == null))
            {
                //financeFiltered = financePolicies.Where(p => p.StartDate < dpFromDate.SelectedDate && p.EndDate >= dpFromDate.SelectedDate).ToList();
                financeFiltered = financeLogic.GetPoliciesWithoutOffersByStartDate((DateTime)dpFromDate.SelectedDate, dpToDate.SelectedDate);

                if (companyId != null)
                {
                    financeFiltered = financeFiltered.Where(p => p.CompanyID == companyId).ToList();
                }
                if (cbIndustry.SelectedItem != null)
                {
                    int id = ((FinanceProgramType)cbIndustry.SelectedItem).ProgramTypeID;
                    financeFiltered = financeFiltered.Where(p => p.ProgramTypeID == id).ToList();
                }
                if (cbInsuranceType.SelectedItem != null)
                {
                    int id = ((FinanceProgram)cbInsuranceType.SelectedItem).ProgramID;
                    financeFiltered = financeFiltered.Where(p => p.ProgramID == id).ToList();
                }
                if (clientTypeid != null)
                {
                    financeFiltered = financeFiltered.Where(p => p.Client.ClientTypeID == clientTypeid).ToList();
                }
                if (agentId != null)
                {
                    financeFiltered = financeFiltered.Where(p => (p.Client.Agent != null && p.Client.Agent.AgentID == agentId) || (p.Client.Agent1 != null && p.Client.Agent1.AgentID == agentId)).ToList();
                }

                foreach (var item in financeFiltered)
                {
                    if ((item.DepositAmountIndependent != null || item.DepositAmountEmployee != null))
                    {
                        if (item.DepositAmountIndependent == null)
                        {
                            premium = premium + (decimal)item.DepositAmountEmployee;
                        }
                        else if (item.DepositAmountEmployee == null)
                        {
                            premium = premium + (decimal)item.DepositAmountIndependent;
                        }
                        else
                        {
                            premium = premium + (decimal)item.DepositAmountEmployee + (decimal)item.DepositAmountIndependent;
                        }
                    }
                    filteredPolicies.Add(item);
                }
            }
            if (user.IsForeingWorkersPermission != false && (insuranceName == "עובדים זרים" || insuranceName == null))
            {
                //financeFiltered = financePolicies.Where(p => p.StartDate < dpFromDate.SelectedDate && p.EndDate >= dpFromDate.SelectedDate).ToList();
                foreingWorkersFiltered = foreingWorkersLogic.GetPoliciesWithoutOffersByStartDate((DateTime)dpFromDate.SelectedDate, dpToDate.SelectedDate);

                if (companyId != null)
                {
                    foreingWorkersFiltered = foreingWorkersFiltered.Where(p => p.CompanyID == companyId).ToList();
                }
                if (cbIndustry.SelectedItem != null)
                {
                    int id = ((ForeingWorkersIndustry)cbIndustry.SelectedItem).ForeingWorkersIndustryID;
                    foreingWorkersFiltered = foreingWorkersFiltered.Where(p => p.ForeingWorkersIndustryID == id).ToList();
                }
                if (cbInsuranceType.SelectedItem != null)
                {
                    int id = ((ForeingWorkersInsuranceType)cbInsuranceType.SelectedItem).ForeingWorkersInsuranceTypeID;
                    foreingWorkersFiltered = foreingWorkersFiltered.Where(p => p.ForeingWorkersInsuranceTypeID == id).ToList();
                }
                if (clientTypeid != null)
                {
                    foreingWorkersFiltered = foreingWorkersFiltered.Where(p => p.Client.ClientTypeID == clientTypeid).ToList();
                }
                if (agentId != null)
                {
                    foreingWorkersFiltered = foreingWorkersFiltered.Where(p => (p.Client.Agent != null && p.Client.Agent.AgentID == agentId) || (p.Client.Agent1 != null && p.Client.Agent1.AgentID == agentId)).ToList();
                }

                premium = premium + (foreingWorkersFiltered.Sum(p => (decimal)p.TotalPremium)) * 12;

                foreach (var item in foreingWorkersFiltered)
                {
                    filteredPolicies.Add(item);
                }
            }

            dgReports.ItemsSource = filteredPolicies.ToList();
            DisplayReportDetails(premium);
        }

        private void DisplayReportDetails(decimal premium)
        {

            string from = ((DateTime)dpFromDate.SelectedDate).ToString("dd/MM/yyyy");
            string to = ((DateTime)dpToDate.SelectedDate).ToString("dd/MM/yyyy");

            lblFilterDetails.Content = string.Format("מתאריך: {0} עד תאריך {1} סה''כ {2} רשומות", from, to, dgReports.Items.Count);
            lblTotalPremium.Content = premium.ToString("n") + " " + "ש''ח";
        }

        private void cbInsuranceBinding()
        {
            var insurances = insuranceLogic.GetAllInsurances();
            cbClientsInsurance.ItemsSource = insurances.ToList();
            cbClientsInsurance.DisplayMemberPath = "InsuranceName";
            cbSegmentationInsurance.ItemsSource = insurances.ToList();
            cbSegmentationInsurance.DisplayMemberPath = "InsuranceName";
            if (user.IsElementaryPermission == false)
            {
                insurances = insurances.Where(i => i.InsuranceName != "אלמנטרי").ToList();
            }
            if (user.IsFinancePermission == false)
            {
                insurances = insurances.Where(i => i.InsuranceName != "פיננסים").ToList();
            }
            if (user.IsForeingWorkersPermission == false)
            {
                insurances = insurances.Where(i => i.InsuranceName != "עובדים זרים").ToList();
            }
            if (user.IsHealthPermission == false)
            {
                insurances = insurances.Where(i => i.InsuranceName != "בריאות").ToList();
            }
            if (user.IsLifePermission == false)
            {
                insurances = insurances.Where(i => i.InsuranceName != "חיים").ToList();
            }
            if (user.IsPersonalAccidentsPermission == false)
            {
                insurances = insurances.Where(i => i.InsuranceName != "תאונות אישיות").ToList();
            }
            if (user.IsTravelPermission == false)
            {
                insurances = insurances.Where(i => i.InsuranceName != "נסיעות לחו''ל").ToList();
            }
            if (user.IsForeingWorkersPermission == false)
            {
                insurances = insurances.Where(i => i.InsuranceName != "עובדים זרים").ToList();
            }
            cbInsurance.ItemsSource = insurances.ToList();
            cbInsurance.DisplayMemberPath = "InsuranceName";
        }


        //מגדיר את היום הראשון בחודש הנוכחי
        public DateTime SetFirstDayOfMonth()
        {
            DateTime today = DateTime.Today;
            return new DateTime(today.Year, today.Month, 1);
        }
        //מגדיר את היום האחרון בחודש הנוכחי
        public DateTime SetLastDayOfMonth()
        {
            DateTime today = DateTime.Today;
            return new DateTime(today.Year, today.Month, 1).AddMonths(1).AddDays(-1);
        }

        private void cbInsurance_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            cbIndustry.SelectedIndex = -1;
            cbInsuranceCompany.SelectedIndex = -1;
            cbInsuranceType.SelectedIndex = -1;
            cbInsuranceType.IsEnabled = false;
            if (cbInsurance.SelectedItem != null)
            {
                cbIndustryBinding(((Insurance)cbInsurance.SelectedItem).InsuranceName);
                cbCompanyBinding(((Insurance)cbInsurance.SelectedItem).InsuranceID);
            }
            else
            {
                cbIndustry.IsEnabled = false;
            }
            dgReportsBinding();
        }

        private void cbCompanyBinding(int? insuranceId)
        {
            if (insuranceId != null)
            {
                cbInsuranceCompany.ItemsSource = insuranceLogic.GetCompaniesByInsurance((int)insuranceId);
                cbInsuranceCompany.DisplayMemberPath = "Company.CompanyName";
            }
            else
            {
                cbInsuranceCompany.ItemsSource = insuranceLogic.GetAllCompanies();
                cbInsuranceCompany.DisplayMemberPath = "CompanyName";
            }
        }

        //private void cbClientsCompanyBinding(int? insuranceId)
        //{
        //    if (insuranceId != null)
        //    {
        //        cbClientsInsuranceCompany.ItemsSource = insuranceLogic.GetCompaniesByInsurance((int)insuranceId);
        //        cbClientsInsuranceCompany.DisplayMemberPath = "Company.CompanyName";
        //    }
        //    else
        //    {
        //        cbClientsInsuranceCompany.ItemsSource = insuranceLogic.GetAllCompanies();
        //        cbClientsInsuranceCompany.DisplayMemberPath = "CompanyName";
        //    }
        //}

        private void cbIndustryBinding(string insuranceName)
        {
            switch (insuranceName)
            {
                case "אלמנטרי":
                    cbIndustry.ItemsSource = elementaryIndustryLogic.GetAllIndustries();
                    cbIndustry.DisplayMemberPath = "InsuranceIndustryName";
                    cbIndustry.IsEnabled = true;
                    break;
                case "חיים":
                    cbIndustry.ItemsSource = lifeIndustryLogic.GetAllLifeIndustries();
                    cbIndustry.DisplayMemberPath = "LifeIndustryName";
                    cbIndustry.IsEnabled = true;
                    break;
                case "עובדים זרים":
                    cbIndustry.ItemsSource = foreingWorkersIndustryLogic.GetAllForeingWorkersIndustries();
                    cbIndustry.DisplayMemberPath = "ForeingWorkersIndustryName";
                    cbIndustry.IsEnabled = true;
                    break;
                case "פיננסים":
                    cbIndustry.ItemsSource = financeIndustryLogic.GetAllFinanceProgramTypes();
                    cbIndustry.DisplayMemberPath = "ProgramTypeName";
                    cbIndustry.IsEnabled = true;
                    break;
                default:
                    cbIndustry.IsEnabled = false;
                    break;
            }
        }

        private void cbClientsIndustryBinding(string insuranceName)
        {
            switch (insuranceName)
            {
                case "אלמנטרי":
                    cbClientsIndustry.ItemsSource = elementaryIndustryLogic.GetAllIndustries();
                    cbClientsIndustry.DisplayMemberPath = "InsuranceIndustryName";
                    cbClientsIndustry.IsEnabled = true;
                    break;
                case "חיים":
                    cbClientsIndustry.ItemsSource = lifeIndustryLogic.GetAllLifeIndustries();
                    cbClientsIndustry.DisplayMemberPath = "LifeIndustryName";
                    cbClientsIndustry.IsEnabled = true;
                    break;
                case "עובדים זרים":
                    cbClientsIndustry.ItemsSource = foreingWorkersIndustryLogic.GetAllForeingWorkersIndustries();
                    cbClientsIndustry.DisplayMemberPath = "ForeingWorkersIndustryName";
                    cbClientsIndustry.IsEnabled = true;
                    break;
                case "בריאות":
                    cbClientsInsuranceType.IsEnabled = true;
                    cbClientsInsuranceType.ItemsSource = healthIndustryLogic.GetAllHealthInsuranceTypes();
                    cbClientsInsuranceType.DisplayMemberPath = "HealthInsuranceTypeName";
                    cbClientsIndustry.IsEnabled = false;
                    break;
                case "פיננסים":
                    cbClientsIndustry.ItemsSource = financeIndustryLogic.GetAllFinanceProgramTypes();
                    cbClientsIndustry.DisplayMemberPath = "ProgramTypeName";
                    cbClientsIndustry.IsEnabled = true;
                    break;
                default:
                    cbClientsIndustry.IsEnabled = false;
                    break;
            }
        }

        private void cbInsuranceCompany_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cbInsurance.SelectedItem != null)
            {
                string insuranceName = ((Insurance)cbInsurance.SelectedItem).InsuranceName;
                switch (insuranceName)
                {
                    //case "אלמנטרי":
                    //    if (cbIndustry.SelectedItem != null)
                    //    {
                    //        cbInsuranceType.IsEnabled = true;
                    //        cbInsuranceType.ItemsSource = elementaryIndustryLogic.GetAllInsuranceTypesByIndustry(((InsuranceIndustry)cbIndustry.SelectedItem).InsuranceIndustryID);
                    //        cbInsuranceType.DisplayMemberPath = "InsuranceTypeName";
                    //    }
                    //    break;
                    case "בריאות":
                        cbInsuranceType.IsEnabled = true;
                        cbInsuranceType.ItemsSource = healthIndustryLogic.GetHealthInsuranceTypesByCompany(((InsuranceCompany)cbInsuranceCompany.SelectedItem).CompanyID);
                        cbInsuranceType.DisplayMemberPath = "HealthInsuranceTypeName";
                        break;
                    case "חיים":
                        if (cbIndustry.SelectedItem != null)
                        {
                            cbInsuranceType.IsEnabled = true;
                            cbInsuranceType.ItemsSource = fundLogic.GetAllFundTypesByInsuranceAndCompany(((Insurance)cbInsurance.SelectedItem).InsuranceID, ((LifeIndustry)cbIndustry.SelectedItem).LifeIndustryID, ((InsuranceCompany)cbInsuranceCompany.SelectedItem).CompanyID);
                            cbInsuranceType.DisplayMemberPath = "FundTypeName";
                        }
                        break;
                    case "עובדים זרים":
                        if (cbIndustry.SelectedItem != null)
                        {
                            cbInsuranceType.IsEnabled = true;
                            cbInsuranceType.ItemsSource = foreingWorkersIndustryLogic.GetForeingWorkersInsuranceTypesByIndustryAndCompany(((ForeingWorkersIndustry)cbIndustry.SelectedItem).ForeingWorkersIndustryID, ((InsuranceCompany)cbInsuranceCompany.SelectedItem).CompanyID);
                            cbInsuranceType.DisplayMemberPath = "ForeingWorkersInsuranceTypeName";
                        }
                        break;
                    case "פיננסים":
                        if (cbIndustry.SelectedItem != null)
                        {
                            cbInsuranceType.IsEnabled = true;
                            cbInsuranceType.ItemsSource = financeIndustryLogic.GetFinanceProgramsByProgramTypeAndCompany(((FinanceProgramType)cbIndustry.SelectedItem).ProgramTypeID, ((InsuranceCompany)cbInsuranceCompany.SelectedItem).CompanyID);
                            cbInsuranceType.DisplayMemberPath = "ProgramName";
                        }
                        break;
                        //default:
                        //    cbInsuranceType.IsEnabled = false;
                        //    break;
                }
            }
            dgReportsBinding();
        }

        private void cbIndustry_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cbIndustry.SelectedItem == null)
            {
                return;
            }
            string insuranceName = ((Insurance)cbInsurance.SelectedItem).InsuranceName;
            switch (insuranceName)
            {
                case "אלמנטרי":
                    cbInsuranceType.IsEnabled = true;
                    cbInsuranceType.ItemsSource = elementaryIndustryLogic.GetAllInsuranceTypesByIndustry(((InsuranceIndustry)cbIndustry.SelectedItem).InsuranceIndustryID);
                    cbInsuranceType.DisplayMemberPath = "ElementaryInsuranceTypeName";
                    break;

                case "חיים":
                    if (cbInsuranceCompany.SelectedItem != null)
                    {
                        cbInsuranceType.IsEnabled = true;
                        cbInsuranceType.ItemsSource = fundLogic.GetAllFundTypesByInsuranceAndCompany(((Insurance)cbInsurance.SelectedItem).InsuranceID, ((LifeIndustry)cbIndustry.SelectedItem).LifeIndustryID, ((InsuranceCompany)cbInsuranceCompany.SelectedItem).CompanyID);
                        cbInsuranceType.DisplayMemberPath = "FundTypeName";
                    }
                    break;
                case "עובדים זרים":
                    if (cbInsuranceCompany.SelectedItem != null)
                    {
                        cbInsuranceType.IsEnabled = true;
                        cbInsuranceType.ItemsSource = foreingWorkersIndustryLogic.GetForeingWorkersInsuranceTypesByIndustryAndCompany(((ForeingWorkersIndustry)cbIndustry.SelectedItem).ForeingWorkersIndustryID, ((InsuranceCompany)cbInsuranceCompany.SelectedItem).CompanyID);
                        cbInsuranceType.DisplayMemberPath = "ForeingWorkersInsuranceTypeName";
                    }
                    break;
                case "פיננסים":
                    if (cbInsuranceCompany.SelectedItem != null)
                    {
                        cbInsuranceType.IsEnabled = true;
                        cbInsuranceType.ItemsSource = financeIndustryLogic.GetFinanceProgramsByProgramTypeAndCompany(((FinanceProgramType)cbIndustry.SelectedItem).ProgramTypeID, ((InsuranceCompany)cbInsuranceCompany.SelectedItem).CompanyID);
                        cbInsuranceType.DisplayMemberPath = "ProgramName";
                    }
                    break;
                default:
                    cbInsuranceType.IsEnabled = false;
                    break;
            }
            dgReportsBinding();
        }

        private void btnFilterReports_Click(object sender, RoutedEventArgs e)
        {
            dgReportsBinding();
        }

        private void btnRefresh_Click(object sender, RoutedEventArgs e)
        {
            cbInsuranceType.SelectedIndex = -1;
            cbInsurance.SelectedIndex = -1;
            cbAgent.SelectedIndex = -1;
            cbInsuranceCompany.SelectedIndex = -1;
            SelectClientType(cbClientType);
            //dpFromDate.SelectedDate = DateTime.Now;
        }

        private void btnClientDetails_Click(object sender, RoutedEventArgs e)
        {
            ListView lv = null;
            object reportToSelect = null;
            if (dgReports.SelectedItem == null)
            {
                MessageBox.Show("נא לסמן רשומה בטבלה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            Client client = GetClient();
            frmAddClient clientDetailsWindow = new frmAddClient(client, user);
            clientDetailsWindow.Closed += (object o, EventArgs ea) =>
            {
                lv = dgReports;
                reportToSelect = dgReports.SelectedItem;
                dgReportsBinding();
                SelectReportInDg(lv, reportToSelect);
            };
            clientDetailsWindow.Show();
        }

        private void SelectReportInDg(ListView lv, object reportToSelect)
        {
            if (reportToSelect is Client)
            {
                var items = lv.Items;
                foreach (var item in items)
                {
                    if (((Client)item).ClientID == ((Client)reportToSelect).ClientID)
                    {
                        lv.SelectedItem = item;
                        break;
                    }
                }
            }
            else
            {
                var items = lv.Items;
                foreach (var item in items)
                {
                    if (item is ElementaryPolicy && reportToSelect is ElementaryPolicy)
                    {
                        if (((ElementaryPolicy)item).ElementaryPolicyID == ((ElementaryPolicy)reportToSelect).ElementaryPolicyID)
                        {
                            lv.SelectedItem = item;
                            break;
                        }
                    }
                    else if (item is LifePolicy && reportToSelect is LifePolicy)
                    {
                        if (((LifePolicy)item).LifePolicyID == ((LifePolicy)reportToSelect).LifePolicyID)
                        {
                            lv.SelectedItem = item;
                            break;
                        }
                    }
                    else if (item is HealthPolicy && reportToSelect is HealthPolicy)
                    {
                        if (((HealthPolicy)item).HealthPolicyID == ((HealthPolicy)reportToSelect).HealthPolicyID)
                        {
                            lv.SelectedItem = item;
                            break;
                        }
                    }
                    else if (item is TravelPolicy && reportToSelect is TravelPolicy)
                    {
                        if (((TravelPolicy)item).TravelPolicyID == ((TravelPolicy)reportToSelect).TravelPolicyID)
                        {
                            lv.SelectedItem = item;
                            break;
                        }
                    }
                    else if (item is PersonalAccidentsPolicy && reportToSelect is PersonalAccidentsPolicy)
                    {
                        if (((PersonalAccidentsPolicy)item).PersonalAccidentsPolicyID == ((PersonalAccidentsPolicy)reportToSelect).PersonalAccidentsPolicyID)
                        {
                            lv.SelectedItem = item;
                            break;
                        }
                    }
                    else if (item is FinancePolicy && reportToSelect is FinancePolicy)
                    {
                        if (((FinancePolicy)item).FinancePolicyID == ((FinancePolicy)reportToSelect).FinancePolicyID)
                        {
                            lv.SelectedItem = item;
                            break;
                        }
                    }
                    else if (item is ForeingWorkersPolicy && reportToSelect is ForeingWorkersPolicy)
                    {
                        if (((ForeingWorkersPolicy)item).ForeingWorkersPolicyID == ((ForeingWorkersPolicy)reportToSelect).ForeingWorkersPolicyID)
                        {
                            lv.SelectedItem = item;
                            break;
                        }
                    }
                }
            }
        }

        private Client GetClient()
        {
            Client client = null;
            if (dgReports.SelectedItem is ElementaryPolicy)
            {
                client = clientLogic.GetClientByClientID(((ElementaryPolicy)dgReports.SelectedItem).ClientID);
            }
            else if (dgReports.SelectedItem is PersonalAccidentsPolicy)
            {
                client = clientLogic.GetClientByClientID(((PersonalAccidentsPolicy)dgReports.SelectedItem).ClientID);
            }
            else if (dgReports.SelectedItem is TravelPolicy)
            {
                client = clientLogic.GetClientByClientID(((TravelPolicy)dgReports.SelectedItem).ClientID);
            }
            else if (dgReports.SelectedItem is LifePolicy)
            {
                client = clientLogic.GetClientByClientID(((LifePolicy)dgReports.SelectedItem).ClientID);
            }
            else if (dgReports.SelectedItem is HealthPolicy)
            {
                client = clientLogic.GetClientByClientID(((HealthPolicy)dgReports.SelectedItem).ClientID);
            }
            else if (dgReports.SelectedItem is FinancePolicy)
            {
                client = clientLogic.GetClientByClientID(((FinancePolicy)dgReports.SelectedItem).ClientID);
            }
            else if (dgReports.SelectedItem is ForeingWorkersPolicy)
            {
                client = clientLogic.GetClientByClientID(((ForeingWorkersPolicy)dgReports.SelectedItem).ClientID);
            }

            return client;
        }

        private void btnPolicyDetails_Click(object sender, RoutedEventArgs e)
        {
            if (dgReports.SelectedItem == null)
            {
                MessageBox.Show("נא לסמן רשומה בטבלה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            Client client = null;
            if (dgReports.SelectedItem is ElementaryPolicy)
            {
                client = clientLogic.GetClientByClientID(((ElementaryPolicy)dgReports.SelectedItem).ClientID);
                frmElementary policyDetails = new frmElementary(((ElementaryPolicy)dgReports.SelectedItem), user, false, client);
                policyDetails.ShowDialog();
            }
            else if (dgReports.SelectedItem is PersonalAccidentsPolicy)
            {
                client = clientLogic.GetClientByClientID(((PersonalAccidentsPolicy)dgReports.SelectedItem).ClientID);
                frmPersonalAccidents policyDetails = new frmPersonalAccidents(client, user, false, ((PersonalAccidentsPolicy)dgReports.SelectedItem));
                policyDetails.ShowDialog();
            }
            else if (dgReports.SelectedItem is TravelPolicy)
            {
                client = clientLogic.GetClientByClientID(((TravelPolicy)dgReports.SelectedItem).ClientID);
                frmTravel policyDetails = new frmTravel(client, user, false, ((TravelPolicy)dgReports.SelectedItem));
                policyDetails.ShowDialog();
            }
            else if (dgReports.SelectedItem is LifePolicy)
            {
                client = clientLogic.GetClientByClientID(((LifePolicy)dgReports.SelectedItem).ClientID);
                frmLifePolicy policyDetails = new frmLifePolicy(client, user, false, ((LifePolicy)dgReports.SelectedItem));
                policyDetails.ShowDialog();
            }
            else if (dgReports.SelectedItem is HealthPolicy)
            {
                client = clientLogic.GetClientByClientID(((HealthPolicy)dgReports.SelectedItem).ClientID);
                frmHealthPolicy policyDetails = new frmHealthPolicy(client, user, false, ((HealthPolicy)dgReports.SelectedItem));
                policyDetails.ShowDialog();
            }
            else if (dgReports.SelectedItem is FinancePolicy)
            {
                client = clientLogic.GetClientByClientID(((FinancePolicy)dgReports.SelectedItem).ClientID);
                frmFinance policyDetails = new frmFinance(client, user, false, (FinancePolicy)dgReports.SelectedItem);
                policyDetails.ShowDialog();
            }
            else if (dgReports.SelectedItem is ForeingWorkersPolicy)
            {
                client = clientLogic.GetClientByClientID(((ForeingWorkersPolicy)dgReports.SelectedItem).ClientID);
                frmForeignWorkers policyDetails = new frmForeignWorkers(client, user, false, ((ForeingWorkersPolicy)dgReports.SelectedItem));
                policyDetails.ShowDialog();
            }

            ListView lv = dgReports;
            object reportToSelect = dgReports.SelectedItem;
            dgReportsBinding();
            SelectReportInDg(lv, reportToSelect);
        }

        private void rbPoliciesReport_Checked(object sender, RoutedEventArgs e)
        {
            dpPoliciesReport.Visibility = Visibility.Visible;
            dpIsNewClient.Visibility = Visibility.Collapsed;
            dpIsNewPolicy.Visibility = Visibility.Collapsed;
            dpClientsReport.Visibility = Visibility.Collapsed;
            gbFilterByPolicies.Visibility = Visibility.Visible;
            gbFilterByIsn_t.Visibility = Visibility.Collapsed;
            gbFilterByClientSegmentation.Visibility = Visibility.Collapsed;
            gbNews.Visibility = Visibility.Collapsed;
            //dgReports.Height = this.ActualHeight - 330;
        }

        private void rbClientsReports_Checked(object sender, RoutedEventArgs e)
        {
            dpIsNewPolicy.Visibility = Visibility.Collapsed;
            dpPoliciesReport.Visibility = Visibility.Collapsed;
            dpIsNewClient.Visibility = Visibility.Collapsed;
            dpClientsReport.Visibility = Visibility.Visible;
            gbFilterByPolicies.Visibility = Visibility.Collapsed;
            gbFilterByIsn_t.Visibility = Visibility.Visible;
            gbFilterByClientSegmentation.Visibility = Visibility.Collapsed;
            gbNews.Visibility = Visibility.Collapsed;
            dgIsntReportsBinding();
            lv.AutoSizeColumns(dgClientsReports.View);
        }

        private void rbClientsSegmentation_Checked(object sender, RoutedEventArgs e)
        {
            dpIsNewPolicy.Visibility = Visibility.Collapsed;
            dpPoliciesReport.Visibility = Visibility.Collapsed;
            dpClientsReport.Visibility = Visibility.Visible;
            dpIsNewClient.Visibility = Visibility.Collapsed;
            gbFilterByPolicies.Visibility = Visibility.Collapsed;
            gbFilterByIsn_t.Visibility = Visibility.Collapsed;
            gbFilterByClientSegmentation.Visibility = Visibility.Visible;
            gbNews.Visibility = Visibility.Collapsed;
            dgClientsSegmentationBinding();
            lv.AutoSizeColumns(dgClientsReports.View);
        }
        private void cbClientsInsurance_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            cbClientsIndustry.SelectedIndex = -1;
            //cbClientsInsuranceCompany.SelectedIndex = -1;
            cbClientsInsuranceType.SelectedIndex = -1;
            cbClientsInsuranceType.IsEnabled = false;
            if (cbClientsInsurance.SelectedItem != null)
            {
                cbClientsIndustryBinding(((Insurance)cbClientsInsurance.SelectedItem).InsuranceName);
                //cbClientsCompanyBinding(((Insurance)cbClientsInsurance.SelectedItem).InsuranceID);
            }
            else
            {
                cbClientsIndustry.IsEnabled = false;
            }
            dgIsntReportsBinding();
        }

        private void cbClientsIndustry_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //txtSalaryFrom.IsEnabled = false;
            //txtSalaryTo.IsEnabled = false;
            if (cbClientsIndustry.SelectedItem == null)
            {
                return;
            }
            string insuranceName = ((Insurance)cbClientsInsurance.SelectedItem).InsuranceName;
            switch (insuranceName)
            {
                case "אלמנטרי":
                    cbClientsInsuranceType.IsEnabled = true;
                    cbClientsInsuranceType.ItemsSource = elementaryIndustryLogic.GetAllInsuranceTypesByIndustry(((InsuranceIndustry)cbClientsIndustry.SelectedItem).InsuranceIndustryID);
                    cbClientsInsuranceType.DisplayMemberPath = "ElementaryInsuranceTypeName";
                    break;

                case "חיים":
                    cbClientsInsuranceType.IsEnabled = true;
                    cbClientsInsuranceType.ItemsSource = fundLogic.GetAllFundTypesByInsuranceAndIndustry(((Insurance)cbClientsInsurance.SelectedItem).InsuranceID, ((LifeIndustry)cbClientsIndustry.SelectedItem).LifeIndustryID);
                    cbClientsInsuranceType.DisplayMemberPath = "FundTypeName";
                    break;
                case "עובדים זרים":
                    cbClientsInsuranceType.IsEnabled = true;
                    cbClientsInsuranceType.ItemsSource = foreingWorkersIndustryLogic.GetForeingWorkersInsuranceTypesByIndustry(((ForeingWorkersIndustry)cbClientsIndustry.SelectedItem).ForeingWorkersIndustryID);
                    cbClientsInsuranceType.DisplayMemberPath = "ForeingWorkersInsuranceTypeName";
                    break;
                case "פיננסים":
                    cbClientsInsuranceType.IsEnabled = true;
                    cbClientsInsuranceType.ItemsSource = financeIndustryLogic.GetFinanceProgramsByProgramType(((FinanceProgramType)cbClientsIndustry.SelectedItem).ProgramTypeID);
                    cbClientsInsuranceType.DisplayMemberPath = "ProgramName";
                    break;
                default:
                    cbClientsInsuranceType.IsEnabled = false;
                    break;

            }
            dgIsntReportsBinding();
        }

        private void dgClientByNewBinding()
        {

            dpPoliciesReport.Visibility = Visibility.Collapsed;
            dpIsNewPolicy.Visibility = Visibility.Visible;
            dpClientsReport.Visibility = Visibility.Collapsed;
            gbFilterByPolicies.Visibility = Visibility.Collapsed;
            gbFilterByIsn_t.Visibility = Visibility.Collapsed;
            gbFilterByClientSegmentation.Visibility = Visibility.Collapsed;
            gbNews.Visibility = Visibility.Visible;
            int selected = int.Parse(cbIsNew.SelectedIndex.ToString());

            


            if (selected == 0 || selected == -1)
            {
                dpIsNewPolicy.Visibility = Visibility.Visible;
                dpIsNewClient.Visibility = Visibility.Collapsed;


                //filteredSegmentation = clientLogic.GetAllClients().ToList();
                List<ElementaryPolicy> elementaryActive = elementaryLogic.GetActivePoliciesWithoutOffers();

                foreach (var item in elementaryActive)
                {
                    var clientPolicy = elementaryActive.FirstOrDefault(p => p.ClientID == item.ClientID);
                    DateTime date = clientPolicy.OpenDate;
                    TimeSpan ts = (new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.Today.Day)) - date;
                    int days = ts.Days;

                    if (days >= 365)
                    {
                        elementaryActive.Remove(item);
                    }
                    else if (clientPolicy.isNew == null || clientPolicy.isNew == false)
                    {
                        elementaryActive.Remove(item);
                    }
                    

                }
                dgIsNew.ItemsSource = elementaryActive;

            }
            else if (selected == 1)
            {
                dpIsNewPolicy.Visibility = Visibility.Collapsed;
                dpIsNewClient.Visibility = Visibility.Visible;


                filteredSegmentation = clientLogic.GetAllClients().ToList();
                /*
                List<FinancePolicy> financeActive = financeLogic.GetActivePoliciesWithoutOffers();
                List<HealthPolicy> healthActive = healthLogic.GetActivePoliciesWithoutOffers();
                List<ElementaryPolicy> elementaryActive = elementaryLogic.GetActivePoliciesWithoutOffers();
                List<PersonalAccidentsPolicy> personalAccidentsActive = personalAccidentsLogic.GetActivePoliciesWithoutOffers();
                List<TravelPolicy> travelActive = travelLogic.GetActivePoliciesWithoutOffers();
                List<LifePolicy> lifeActive = lifeLogic.GetActivePoliciesWithoutOffers();
                List<ForeingWorkersPolicy> foreingWorkersActive = foreingWorkersLogic.GetActivePoliciesWithoutOffers();
                */
                List<Client> clientsByInsurance = new List<Client>();
                clientsByInsurance.AddRange(filteredSegmentation);

                foreach (var item in clientsByInsurance)
                {
                    var clientPolicy = filteredSegmentation.FirstOrDefault(p => p.ClientID == item.ClientID);
                    DateTime date = clientPolicy.OpenDate.Value;
                    DateTime year = dpFromDat.SelectedDate.Value;
                    TimeSpan ts = year - date;
                    int days = ts.Days;
                    
                    if (days >= 365)
                        {
                            filteredSegmentation.Remove(item);
                        }
                        else if (clientPolicy.isNew == null || clientPolicy.isNew == false)
                        {
                            filteredSegmentation.Remove(item);
                        }
                        
                        
                    }
                
                dgIsNews.ItemsSource = filteredSegmentation;
            }

            
        }

        private void dgClientsSegmentationBinding()
        {
            filteredSegmentation = clientLogic.GetAllClientsByType(((ClientType)cbSegmentationClientType.SelectedItem).ClientTypeName).ToList();
            if (cbSegmentationAgent.SelectedItem != null)
            {
                filteredSegmentation = filteredSegmentation.Where(c => c.PrincipalAgentID == (((Agent)cbSegmentationAgent.SelectedItem).AgentID) || c.SecundaryAgentID == (((Agent)cbSegmentationAgent.SelectedItem).AgentID)).ToList();
            }
            string insuranceName = null;
            if (cbSegmentationInsurance.SelectedItem != null)
            {
                insuranceName = ((Insurance)cbSegmentationInsurance.SelectedItem).InsuranceName;
            }
            List<FinancePolicy> financeActive;
            List<HealthPolicy> healthActive;
            List<ElementaryPolicy> elementaryActive;
            List<PersonalAccidentsPolicy> personalAccidentsActive;
            List<TravelPolicy> travelActive;
            List<LifePolicy> lifeActive;
            List<ForeingWorkersPolicy> foreingWorkersActive;

            if (insuranceName != null)
            {
                List<Client> clientsByInsurance = new List<Client>();
                clientsByInsurance.AddRange(filteredSegmentation);
                switch (insuranceName)
                {
                    case "אלמנטרי":
                        elementaryActive = elementaryLogic.GetActivePoliciesWithoutOffers();

                        foreach (var item in clientsByInsurance)
                        {
                            if (cbSegmentationInsuranceType.SelectedItem != null)
                            {
                                int id = ((ElementaryInsuranceType)cbSegmentationInsuranceType.SelectedItem).ElementaryInsuranceTypeID;
                                elementaryActive = elementaryActive.Where(p => p.ElementaryInsuranceTypeID == id).ToList();
                            }
                            else if (cbSegmentationIndustry.SelectedItem != null)
                            {
                                int id = ((InsuranceIndustry)cbSegmentationIndustry.SelectedItem).InsuranceIndustryID;
                                elementaryActive = elementaryActive.Where(p => p.InsuranceIndustryID == id).ToList();
                            }
                            var clientPolicy = elementaryActive.FirstOrDefault(p => p.ClientID == item.ClientID);
                            if (clientPolicy == null)
                            {
                                filteredSegmentation.Remove(item);
                            }
                        }
                        break;
                    case "תאונות אישיות":
                        personalAccidentsActive = personalAccidentsLogic.GetActivePoliciesWithoutOffers();

                        foreach (var item in clientsByInsurance)
                        {
                            var clientPolicy = personalAccidentsActive.FirstOrDefault(p => p.ClientID == item.ClientID);
                            if (clientPolicy == null)
                            {
                                filteredSegmentation.Remove(item);
                            }
                        }
                        break;
                    case "נסיעות לחו''ל":
                        travelActive = travelLogic.GetActivePoliciesWithoutOffers();

                        foreach (var item in clientsByInsurance)
                        {
                            var clientPolicy = travelActive.FirstOrDefault(p => p.ClientID == item.ClientID);
                            if (clientPolicy == null)
                            {
                                filteredSegmentation.Remove(item);
                            }
                        }
                        break;
                    case "חיים":
                        lifeActive = lifeLogic.GetActivePoliciesWithoutOffers();

                        decimal? fromSalary = null;
                        decimal? toSalary = null;
                        if (txtSalaryFrom.Text != "" && txtSalaryTo.Text != "")
                        {
                            fromSalary = Convert.ToDecimal(txtSalaryFrom.Text);
                            toSalary = Convert.ToDecimal(txtSalaryTo.Text);
                        }
                        else if (txtSalaryFrom.Text != "")
                        {
                            fromSalary = Convert.ToDecimal(txtSalaryFrom.Text);
                        }
                        else if (txtSalaryTo.Text != "")
                        {
                            fromSalary = Convert.ToDecimal(txtSalaryTo.Text);
                        }
                        foreach (var item in clientsByInsurance)
                        {
                            if (cbSegmentationInsuranceType.SelectedItem != null)
                            {
                                int id = ((FundType)cbSegmentationInsuranceType.SelectedItem).FundTypeID;
                                lifeActive = lifeActive.Where(p => p.FundTypeID == id).ToList();
                            }
                            else if (cbSegmentationIndustry.SelectedItem != null)
                            {
                                int id = ((LifeIndustry)cbSegmentationIndustry.SelectedItem).LifeIndustryID;
                                lifeActive = lifeActive.Where(p => p.LifeIndustryID == id).ToList();
                            }
                            var clientPolicy = lifeActive.Where(p => p.ClientID == item.ClientID);
                            if (clientPolicy.Count() == 0)
                            {
                                filteredSegmentation.Remove(item);
                            }
                            else
                            {
                                object salaryPolicy = null;
                                if (fromSalary != null && toSalary != null)
                                {
                                    if (((LifeIndustry)cbSegmentationIndustry.SelectedItem).LifeIndustryName == "פנסיה")
                                    {
                                        salaryPolicy = clientPolicy.FirstOrDefault(p => p.PensionLifeIndustry != null && p.PensionLifeIndustry.EmployeeSalary >= fromSalary && p.PensionLifeIndustry.EmployeeSalary <= toSalary);
                                    }
                                    else if (((LifeIndustry)cbSegmentationIndustry.SelectedItem).LifeIndustryName == "מנהלים")
                                    {
                                        salaryPolicy = lifeLogic.GetSalaryChangesByClient(item.ClientID).FirstOrDefault(s => s.TotalSalary >= fromSalary && s.TotalSalary <= toSalary);
                                    }
                                    if (salaryPolicy == null)
                                    {
                                        filteredSegmentation.Remove(item);
                                    }
                                }
                                else if (fromSalary != null)
                                {
                                    if (((LifeIndustry)cbSegmentationIndustry.SelectedItem).LifeIndustryName == "פנסיה")
                                    {
                                        salaryPolicy = clientPolicy.FirstOrDefault(p => p.PensionLifeIndustry.EmployeeSalary == fromSalary);
                                    }
                                    else if (((LifeIndustry)cbSegmentationIndustry.SelectedItem).LifeIndustryName == "מנהלים")
                                    {
                                        salaryPolicy = lifeLogic.GetSalaryChangesByClient(item.ClientID).FirstOrDefault(s => s.TotalSalary == fromSalary);
                                    }
                                    if (salaryPolicy == null)
                                    {
                                        filteredSegmentation.Remove(item);
                                    }
                                }
                            }
                        }
                        break;
                    case "בריאות":
                        healthActive = healthLogic.GetActivePoliciesWithoutOffers();

                        foreach (var item in clientsByInsurance)
                        {
                            if (cbSegmentationInsuranceType.SelectedItem != null)
                            {
                                int id = ((HealthInsuranceType)cbSegmentationInsuranceType.SelectedItem).HealthInsuranceTypeID;
                                healthActive = healthActive.Where(p => p.HealthInsuranceTypeID == id).ToList();
                            }
                            var clientPolicy = healthActive.FirstOrDefault(p => p.ClientID == item.ClientID);
                            if (clientPolicy == null)
                            {
                                filteredSegmentation.Remove(item);
                            }
                        }
                        break;
                    case "פיננסים":
                        financeActive = financeLogic.GetActivePoliciesWithoutOffers();

                        foreach (var item in clientsByInsurance)
                        {
                            if (cbSegmentationInsuranceType.SelectedItem != null)
                            {
                                int id = ((FinanceProgram)cbSegmentationInsuranceType.SelectedItem).ProgramID;
                                financeActive = financeActive.Where(p => p.ProgramID == id).ToList();
                            }
                            else if (cbSegmentationIndustry.SelectedItem != null)
                            {
                                int id = ((FinanceProgramType)cbSegmentationIndustry.SelectedItem).ProgramTypeID;
                                financeActive = financeActive.Where(p => p.ProgramTypeID == id).ToList();
                            }
                            var clientPolicy = financeActive.FirstOrDefault(p => p.ClientID == item.ClientID);
                            if (clientPolicy == null)
                            {
                                filteredSegmentation.Remove(item);
                            }
                        }
                        break;
                    case "עובדים זרים":
                        foreingWorkersActive = foreingWorkersLogic.GetActivePoliciesWithoutOffers();

                        foreach (var item in clientsByInsurance)
                        {
                            if (cbSegmentationInsuranceType.SelectedItem != null)
                            {
                                int id = ((ForeingWorkersInsuranceType)cbSegmentationInsuranceType.SelectedItem).ForeingWorkersInsuranceTypeID;
                                foreingWorkersActive = foreingWorkersActive.Where(p => p.ForeingWorkersInsuranceTypeID == id).ToList();
                            }
                            else if (cbSegmentationIndustry.SelectedItem != null)
                            {
                                int id = ((ForeingWorkersIndustry)cbSegmentationIndustry.SelectedItem).ForeingWorkersIndustryID;
                                foreingWorkersActive = foreingWorkersActive.Where(p => p.ForeingWorkersIndustryID == id).ToList();
                            }
                            var clientPolicy = foreingWorkersActive.FirstOrDefault(p => p.ClientID == item.ClientID);
                            if (clientPolicy == null)
                            {
                                filteredSegmentation.Remove(item);
                            }
                        }
                        break;
                }
            }
            if (txtZone.Text != "")
            {
                filteredSegmentation = filteredSegmentation.Where(c => c.City == txtZone.Text).ToList();
            }
            DateTime? fromDate = null;
            DateTime? toDate = null;
            if (txtAgeFrom.Text != "" && txtAgeTo.Text != "")
            {
                fromDate = DateTime.Now.AddYears(-Convert.ToInt32(txtAgeTo.Text)).AddMonths(-6);
                toDate = DateTime.Now.AddYears(-Convert.ToInt32(txtAgeFrom.Text)).AddMonths(6);
            }
            else if (txtAgeFrom.Text != "")
            {
                fromDate = DateTime.Now.AddYears(-Convert.ToInt32(txtAgeFrom.Text)).AddMonths(-6);
                toDate = DateTime.Now.AddYears(-Convert.ToInt32(txtAgeFrom.Text)).AddMonths(6);
            }
            else if (txtAgeTo.Text != "")
            {
                fromDate = DateTime.Now.AddYears(-Convert.ToInt32(txtAgeTo.Text)).AddMonths(-6);
                toDate = DateTime.Now.AddYears(-Convert.ToInt32(txtAgeTo.Text)).AddMonths(6);
            }
            if (fromDate != null && toDate != null)
            {
                filteredSegmentation = filteredSegmentation.Where(c => c.BirthDate >= fromDate && c.BirthDate <= toDate).ToList();
            }
            DateTime? fromBirthday = dpFromBirthDate.SelectedDate;
            DateTime? toBirthday = dpToBirthDate.SelectedDate;
            if (fromBirthday != null && toBirthday != null)
            {
                int mFrom = ((DateTime)fromBirthday).Month;
                int dFrom = ((DateTime)fromBirthday).Day;
                int mTo = ((DateTime)toBirthday).Month;
                int dTo = ((DateTime)toBirthday).Day;
                filteredSegmentation = filteredSegmentation.Where(c => c.BirthDate != null &&
                ((((DateTime)c.BirthDate).Month > mFrom || (((DateTime)c.BirthDate).Month == mFrom && ((DateTime)c.BirthDate).Day >= dFrom)) &&
                (((DateTime)c.BirthDate).Month < mTo || (((DateTime)c.BirthDate).Month == mTo && ((DateTime)c.BirthDate).Day <= dTo)))).ToList();
            }
            else if (fromBirthday != null)
            {
                int month = ((DateTime)fromBirthday).Month;
                int day = ((DateTime)fromBirthday).Day;
                filteredSegmentation = filteredSegmentation.Where(c => ((DateTime)c.BirthDate).Month == month && ((DateTime)c.BirthDate).Day == day).ToList();
            }
            else if (toBirthday != null)
            {
                int month = ((DateTime)toBirthday).Month;
                int day = ((DateTime)toBirthday).Day;
                filteredSegmentation = filteredSegmentation.Where(c => ((DateTime)c.BirthDate).Month == month && ((DateTime)c.BirthDate).Day == day).ToList();
            }

            dgClientsReports.ItemsSource = filteredSegmentation;
            SetlblClientsFilterDetails();
        }

        private void SetlblClientsFilterDetails()
        {
            int itemsCount = dgClientsReports.Items.Count;
            lblClientsFilterDetails.Content = string.Format("סה''כ {0} רשומות", itemsCount.ToString());
        }

        private void btnSetDates_Click(object sender, RoutedEventArgs e)
        {
            if (dpFromDate.SelectedDate == null || dpToDate.SelectedDate == null)
            {
                MessageBox.Show("נא להכניס תאריך תחילה ותאריך סיום", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            else if (dpToDate.SelectedDate <= dpFromDate.SelectedDate)
            {
                MessageBox.Show("תאריך סיום חייב להיות גדול מתאריך תחילה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            else
            {
                reportFromDate = (DateTime)dpFromDate.SelectedDate;
                reportToDate = (DateTime)dpToDate.SelectedDate;
                dgReportsBinding();
            }
        }

        private void cbClientType_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            dgReportsBinding();
        }

        private void cbClientsClientType_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (rbClientsReports.IsChecked == true)
            {
                dgIsntReportsBinding();
            }
        }

        private void cbSegmentationClientType_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (rbClientsSegmentation.IsChecked == true)
            {
                dgClientsSegmentationBinding();
            }
        }

        private void cbSegmentationInsurance_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            cbSegmentationIndustry.SelectedIndex = -1;
            //cbClientsInsuranceCompany.SelectedIndex = -1;
            cbSegmentationInsuranceType.SelectedIndex = -1;
            cbSegmentationInsuranceType.IsEnabled = false;
            if (cbSegmentationInsurance.SelectedItem != null)
            {
                cbSegmentationIndustryBinding(((Insurance)cbSegmentationInsurance.SelectedItem).InsuranceName);
                //cbClientsCompanyBinding(((Insurance)cbClientsInsurance.SelectedItem).InsuranceID);
            }
            else
            {
                cbSegmentationIndustry.IsEnabled = false;
            }
            dgClientsSegmentationBinding();
        }

        private void cbSegmentationIndustryBinding(string insuranceName)
        {
            switch (insuranceName)
            {
                case "אלמנטרי":
                    cbSegmentationIndustry.ItemsSource = elementaryIndustryLogic.GetAllIndustries();
                    cbSegmentationIndustry.DisplayMemberPath = "InsuranceIndustryName";
                    cbSegmentationIndustry.IsEnabled = true;
                    break;
                case "חיים":
                    cbSegmentationIndustry.ItemsSource = lifeIndustryLogic.GetAllLifeIndustries();
                    cbSegmentationIndustry.DisplayMemberPath = "LifeIndustryName";
                    cbSegmentationIndustry.IsEnabled = true;
                    break;
                case "עובדים זרים":
                    cbSegmentationIndustry.ItemsSource = foreingWorkersIndustryLogic.GetAllForeingWorkersIndustries();
                    cbSegmentationIndustry.DisplayMemberPath = "ForeingWorkersIndustryName";
                    cbSegmentationIndustry.IsEnabled = true;
                    break;
                case "בריאות":
                    cbSegmentationInsuranceType.IsEnabled = true;
                    cbSegmentationInsuranceType.ItemsSource = healthIndustryLogic.GetAllHealthInsuranceTypes();
                    cbSegmentationInsuranceType.DisplayMemberPath = "HealthInsuranceTypeName";
                    cbSegmentationIndustry.IsEnabled = false;
                    break;
                case "פיננסים":
                    cbSegmentationIndustry.ItemsSource = financeIndustryLogic.GetAllFinanceProgramTypes();
                    cbSegmentationIndustry.DisplayMemberPath = "ProgramTypeName";
                    cbSegmentationIndustry.IsEnabled = true;
                    break;
                default:
                    cbSegmentationIndustry.IsEnabled = false;
                    break;
            }
        }

        private void cbSegmentationIndustry_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            txtSalaryFrom.IsEnabled = false;
            txtSalaryTo.IsEnabled = false;
            if (cbSegmentationIndustry.SelectedItem == null)
            {
                return;
            }
            string insuranceName = ((Insurance)cbSegmentationInsurance.SelectedItem).InsuranceName;
            switch (insuranceName)
            {
                case "אלמנטרי":
                    cbSegmentationInsuranceType.IsEnabled = true;
                    cbSegmentationInsuranceType.ItemsSource = elementaryIndustryLogic.GetAllInsuranceTypesByIndustry(((InsuranceIndustry)cbSegmentationIndustry.SelectedItem).InsuranceIndustryID);
                    cbSegmentationInsuranceType.DisplayMemberPath = "ElementaryInsuranceTypeName";
                    break;

                case "עובדים זרים":
                    cbSegmentationInsuranceType.IsEnabled = true;
                    cbSegmentationInsuranceType.ItemsSource = foreingWorkersIndustryLogic.GetForeingWorkersInsuranceTypesByIndustry(((ForeingWorkersIndustry)cbSegmentationIndustry.SelectedItem).ForeingWorkersIndustryID);
                    cbSegmentationInsuranceType.DisplayMemberPath = "ForeingWorkersInsuranceTypeName";
                    break;

                case "חיים":
                    cbSegmentationInsuranceType.IsEnabled = true;
                    cbSegmentationInsuranceType.ItemsSource = fundLogic.GetAllFundTypesByInsuranceAndIndustry(((Insurance)cbSegmentationInsurance.SelectedItem).InsuranceID, ((LifeIndustry)cbSegmentationIndustry.SelectedItem).LifeIndustryID);
                    cbSegmentationInsuranceType.DisplayMemberPath = "FundTypeName";
                    if (((LifeIndustry)cbSegmentationIndustry.SelectedItem).LifeIndustryName == "מנהלים" || ((LifeIndustry)cbSegmentationIndustry.SelectedItem).LifeIndustryName == "פנסיה")
                    {
                        txtSalaryFrom.IsEnabled = true;
                        txtSalaryTo.IsEnabled = true;
                    }
                    break;
                case "פיננסים":
                    cbSegmentationInsuranceType.IsEnabled = true;
                    cbSegmentationInsuranceType.ItemsSource = financeIndustryLogic.GetFinanceProgramsByProgramType(((FinanceProgramType)cbSegmentationIndustry.SelectedItem).ProgramTypeID);
                    cbSegmentationInsuranceType.DisplayMemberPath = "ProgramName";
                    break;
                default:
                    cbSegmentationInsuranceType.IsEnabled = false;
                    break;

            }
            dgClientsSegmentationBinding();
        }

        private void btnSetMonthBirthdays_Click(object sender, RoutedEventArgs e)
        {
            dpFromBirthDate.SelectedDate = SetFirstDayOfMonth();
            dpToBirthDate.SelectedDate = SetLastDayOfMonth();
        }

        private void btnSetSegmentationDetails_Click(object sender, RoutedEventArgs e)
        {
            if (txtAgeFrom.Text != "" && txtAgeTo.Text != "" && Convert.ToInt32(txtAgeFrom.Text) > Convert.ToInt32(txtAgeTo.Text))
            {
                MessageBox.Show("גיל סיום חייב להיות גדול מגיל תחילה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            if (txtSalaryFrom.Text != "" && txtSalaryTo.Text != "" && Convert.ToInt32(txtSalaryFrom.Text) > Convert.ToInt32(txtSalaryTo.Text))
            {
                MessageBox.Show("שכר סיום חייב להיות גדול משכר תחילה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            if (dpToBirthDate.SelectedDate < dpFromBirthDate.SelectedDate)
            {
                MessageBox.Show("תאריך סיום חייב להיות גדול מתאריך תחילה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            dgClientsSegmentationBinding();
        }

        private void txtAgeFrom_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (sender is TextBox)
            {
                TextBox txtBox = (TextBox)sender;
                txtBox.ClearValue(BorderBrushProperty);
                if (txtBox.Text != "")
                {
                    try
                    {
                        int reusult = validations.ConvertStringToInt(txtBox.Text);
                    }
                    catch (Exception ex)
                    {
                        txtBox.BorderBrush = System.Windows.Media.Brushes.Red;
                        System.Windows.Forms.MessageBox.Show(ex.Message);
                        txtBox.Text = "";
                    }
                }
            }
        }

        private void txtSalaryFrom_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (sender is TextBox)
            {
                TextBox txtBox = (TextBox)sender;
                txtBox.ClearValue(BorderBrushProperty);
                if (txtBox.Text != "")
                {
                    try
                    {
                        decimal reusult = validations.ConvertStringToDecimal(txtBox.Text);
                    }
                    catch (Exception ex)
                    {
                        txtBox.BorderBrush = System.Windows.Media.Brushes.Red;
                        System.Windows.Forms.MessageBox.Show(ex.Message);
                        txtBox.Text = "";
                    }
                }
            }
        }

        private void btnClientsRefresh_Click(object sender, RoutedEventArgs e)
        {
            if (rbClientsReports.IsChecked == true)
            {
                SelectClientType(cbClientsClientType);
                cbClientsAgent.SelectedIndex = -1;
                cbClientsInsurance.SelectedIndex = -1;
            }
            else
            {
                SelectClientType(cbSegmentationClientType);
                cbSegmentationAgent.SelectedIndex = -1;
                cbSegmentationInsurance.SelectedIndex = -1;
                txtZone.Clear();
                txtAgeFrom.Clear();
                txtAgeTo.Clear();
                txtSalaryFrom.Clear();
                txtSalaryTo.Clear();
                dpFromBirthDate.SelectedDate = null;
                dpToBirthDate.SelectedDate = null;
                dgClientsSegmentationBinding();
            }
        }

        private void btnClientsClientDetails_Click(object sender, RoutedEventArgs e)
        {
            ListView lv = null;
            object reportToSelect = null;
            if (dgClientsReports.SelectedItem == null)
            {
                MessageBox.Show("נא לסמן רשומה בטבלה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            frmAddClient clientWindow = new frmAddClient((Client)dgClientsReports.SelectedItem, user);
            clientWindow.Closed += (object o, EventArgs ea) =>
            {
                lv = dgClientsReports;
                reportToSelect = dgClientsReports.SelectedItem;
                if (rbClientsReports.IsChecked == true)
                {
                    dgIsntReportsBinding();
                }
                else if (rbClientsSegmentation.IsChecked == true)
                {
                    dgClientsSegmentationBinding();
                }
                SelectReportInDg(lv, reportToSelect);
            };
            clientWindow.Show();

        }

        private void btnSendEmail_Click(object sender, RoutedEventArgs e)
        {
            if (dgReports.SelectedItem == null)
            {
                MessageBox.Show("נא לסמן לקוח", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            Client client = GetClient();
            if (client != null)
            {
                SendEmail(client);
            }
        }

        private void btnSendEmailClient_Click(object sender, RoutedEventArgs e)
        {
            if (dgClientsReports.SelectedItem == null)
            {
                MessageBox.Show("נא לסמן לקוח", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            Client client = (Client)dgClientsReports.SelectedItem;
            SendEmail(client);
        }

        private void SendEmail(Client client)
        {
            if (client.Email == "")
            {
                MessageBox.Show("אין ללקוח כתובת דוא''ל במערכת", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            string[] address = new string[] { client.Email };
            try
            {
                email.SendEmailFromOutlook("", "", null, address);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void mItemPoliciesExcel_Click(object sender, RoutedEventArgs e)
        {
            excelLogic.AllPoliciesToExcel((List<object>)dgReports.ItemsSource);
        }

        private void mItemClientsExcel_Click(object sender, RoutedEventArgs e)
        {
            excelLogic.ClientsToExcel((List<Client>)dgClientsReports.ItemsSource);
        }

        private void btnExcel_Click(object sender, RoutedEventArgs e)
        {
            excelLogic.AllPoliciesToExcel((List<object>)dgReports.ItemsSource);
        }

        private void btnClientsExcel_Click(object sender, RoutedEventArgs e)
        {
            excelLogic.ClientsToExcel((List<Client>)dgClientsReports.ItemsSource);
        }

        private void dgReports_MouseDown(object sender, MouseButtonEventArgs e)
        {
            (sender as ListView).UnselectAll();
        }

        private void mItemPrintPoliciesReport_Click(object sender, RoutedEventArgs e)
        {
            List<PoliciesReport> reportList = null;

            reportList = ConvertToReportView((List<object>)dgReports.ItemsSource);

            if (reportList != null)
            {
                try
                {
                    frmPrintPreview preview = new frmPrintPreview("PoliciesReport", reportList);
                    preview.ShowDialog();

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private List<PoliciesReport> ConvertToReportView(List<object> list)
        {
            List<PoliciesReport> reportList = new List<PoliciesReport>();
            foreach (var item in list)
            {
                PoliciesReport reportItem = new PoliciesReport();
                if (item is ElementaryPolicy)
                {
                    if (((ElementaryPolicy)item).Client != null)
                    {
                        reportItem.ClientName = ((ElementaryPolicy)item).Client.LastName + " " + ((ElementaryPolicy)item).Client.FirstName;
                        reportItem.IdNumber = ((ElementaryPolicy)item).Client.IdNumber;
                    }
                    if (((ElementaryPolicy)item).Company != null)
                    {
                        reportItem.CompanyName = ((ElementaryPolicy)item).Company.CompanyName;
                    }
                    if (((ElementaryPolicy)item).EndDate != null)
                    {
                        reportItem.EndDate = ((DateTime)((ElementaryPolicy)item).EndDate).ToString("dd/MM/yyyy");
                    }
                    if (((ElementaryPolicy)item).StartDate != null)
                    {
                        reportItem.StartDate = ((DateTime)((ElementaryPolicy)item).StartDate).ToString("dd/MM/yyyy");
                    }
                    if (((ElementaryPolicy)item).TotalPremium != null)
                    {
                        reportItem.Premium = ((decimal)((ElementaryPolicy)item).TotalPremium).ToString("0.##");
                    }
                    if (((ElementaryPolicy)item).InsuranceIndustry != null)
                    {
                        reportItem.Industry = ((ElementaryPolicy)item).InsuranceIndustry.InsuranceIndustryName;
                    }
                    if (((ElementaryPolicy)item).Insurance != null)
                    {
                        reportItem.Insurance = ((ElementaryPolicy)item).Insurance.InsuranceName;
                    }
                    if (((ElementaryPolicy)item).CarPolicy != null)
                    {
                        reportItem.RegistrationNumber = ((ElementaryPolicy)item).CarPolicy.RegistrationNumber;
                    }
                    reportItem.PolicyNumber = ((ElementaryPolicy)item).PolicyNumber;
                }
                else if (item is LifePolicy)
                {
                    if (((LifePolicy)item).Client != null)
                    {
                        reportItem.ClientName = ((LifePolicy)item).Client.LastName + " " + ((LifePolicy)item).Client.FirstName;
                        reportItem.IdNumber = ((LifePolicy)item).Client.IdNumber;
                    }
                    if (((LifePolicy)item).Company != null)
                    {
                        reportItem.CompanyName = ((LifePolicy)item).Company.CompanyName;
                    }
                    if (((LifePolicy)item).EndDate != null)
                    {
                        reportItem.EndDate = ((DateTime)((LifePolicy)item).EndDate).ToString("dd/MM/yyyy");
                    }
                    if (((LifePolicy)item).StartDate != null)
                    {
                        reportItem.StartDate = ((DateTime)((LifePolicy)item).StartDate).ToString("dd/MM/yyyy");
                    }
                    if (((LifePolicy)item).TotalMonthlyPayment != null)
                    {
                        reportItem.Premium = ((decimal)((LifePolicy)item).TotalMonthlyPayment).ToString("0.##");
                    }
                    if (((LifePolicy)item).LifeIndustry != null)
                    {
                        reportItem.Industry = ((LifePolicy)item).LifeIndustry.LifeIndustryName;
                    }
                    if (((LifePolicy)item).Insurance != null)
                    {
                        reportItem.Insurance = ((LifePolicy)item).Insurance.InsuranceName;
                    }
                    
                    reportItem.PolicyNumber = ((LifePolicy)item).PolicyNumber;
                }
                else if (item is HealthPolicy)
                {
                    if (((HealthPolicy)item).Client != null)
                    {
                        reportItem.ClientName = ((HealthPolicy)item).Client.LastName + " " + ((HealthPolicy)item).Client.FirstName;
                        reportItem.IdNumber = ((HealthPolicy)item).Client.IdNumber;
                    }
                    if (((HealthPolicy)item).Company != null)
                    {
                        reportItem.CompanyName = ((HealthPolicy)item).Company.CompanyName;
                    }
                    if (((HealthPolicy)item).EndDate != null)
                    {
                        reportItem.EndDate = ((DateTime)((HealthPolicy)item).EndDate).ToString("dd/MM/yyyy");
                    }
                    if (((HealthPolicy)item).StartDate != null)
                    {
                        reportItem.StartDate = ((DateTime)((HealthPolicy)item).StartDate).ToString("dd/MM/yyyy");
                    }
                    if (((HealthPolicy)item).TotalMonthlyPayment != null)
                    {
                        reportItem.Premium = ((decimal)((HealthPolicy)item).TotalMonthlyPayment).ToString("0.##");
                    }                    
                    if (((HealthPolicy)item).Insurance != null)
                    {
                        reportItem.Insurance = ((HealthPolicy)item).Insurance.InsuranceName;
                    }

                    reportItem.PolicyNumber = ((HealthPolicy)item).PolicyNumber;
                }
                else if (item is TravelPolicy)
                {
                    if (((TravelPolicy)item).Client != null)
                    {
                        reportItem.ClientName = ((TravelPolicy)item).Client.LastName + " " + ((TravelPolicy)item).Client.FirstName;
                        reportItem.IdNumber = ((TravelPolicy)item).Client.IdNumber;
                    }
                    if (((TravelPolicy)item).Company != null)
                    {
                        reportItem.CompanyName = ((TravelPolicy)item).Company.CompanyName;
                    }
                    if (((TravelPolicy)item).EndDate != null)
                    {
                        reportItem.EndDate = ((DateTime)((TravelPolicy)item).EndDate).ToString("dd/MM/yyyy");
                    }
                    if (((TravelPolicy)item).StartDate != null)
                    {
                        reportItem.StartDate = ((DateTime)((TravelPolicy)item).StartDate).ToString("dd/MM/yyyy");
                    }
                    if (((TravelPolicy)item).Premium != null)
                    {
                        reportItem.Premium = ((decimal)((TravelPolicy)item).Premium).ToString("0.##");
                    }                    
                    if (((TravelPolicy)item).Insurance != null)
                    {
                        reportItem.Insurance = ((TravelPolicy)item).Insurance.InsuranceName;
                    }

                    reportItem.PolicyNumber = ((TravelPolicy)item).PolicyNumber;
                }
                else if (item is PersonalAccidentsPolicy)
                {
                    if (((PersonalAccidentsPolicy)item).Client != null)
                    {
                        reportItem.ClientName = ((PersonalAccidentsPolicy)item).Client.LastName + " " + ((PersonalAccidentsPolicy)item).Client.FirstName;
                        reportItem.IdNumber = ((PersonalAccidentsPolicy)item).Client.IdNumber;
                    }
                    if (((PersonalAccidentsPolicy)item).Company != null)
                    {
                        reportItem.CompanyName = ((PersonalAccidentsPolicy)item).Company.CompanyName;
                    }
                    if (((PersonalAccidentsPolicy)item).EndDate != null)
                    {
                        reportItem.EndDate = ((DateTime)((PersonalAccidentsPolicy)item).EndDate).ToString("dd/MM/yyyy");
                    }
                    if (((PersonalAccidentsPolicy)item).StartDate != null)
                    {
                        reportItem.StartDate = ((DateTime)((PersonalAccidentsPolicy)item).StartDate).ToString("dd/MM/yyyy");
                    }
                    if (((PersonalAccidentsPolicy)item).TotalPremium != null)
                    {
                        reportItem.Premium = ((decimal)((PersonalAccidentsPolicy)item).TotalPremium).ToString("0.##");
                    }                    
                    if (((PersonalAccidentsPolicy)item).Insurance != null)
                    {
                        reportItem.Insurance = ((PersonalAccidentsPolicy)item).Insurance.InsuranceName;
                    }
                    reportItem.PolicyNumber = ((PersonalAccidentsPolicy)item).PolicyNumber;
                }
                else if (item is FinancePolicy)
                {
                    if (((FinancePolicy)item).Client != null)
                    {
                        reportItem.ClientName = ((FinancePolicy)item).Client.LastName + " " + ((FinancePolicy)item).Client.FirstName;
                        reportItem.IdNumber = ((FinancePolicy)item).Client.IdNumber;
                    }
                    if (((FinancePolicy)item).Company != null)
                    {
                        reportItem.CompanyName = ((FinancePolicy)item).Company.CompanyName;
                    }
                    if (((FinancePolicy)item).EndDate != null)
                    {
                        reportItem.EndDate = ((DateTime)((FinancePolicy)item).EndDate).ToString("dd/MM/yyyy");
                    }
                    if (((FinancePolicy)item).StartDate != null)
                    {
                        reportItem.StartDate = ((DateTime)((FinancePolicy)item).StartDate).ToString("dd/MM/yyyy");
                    }                    
                    if (((FinancePolicy)item).FinanceProgramType != null)
                    {
                        reportItem.Industry = ((FinancePolicy)item).FinanceProgramType.ProgramTypeName;
                    }
                    if (((FinancePolicy)item).Insurance != null)
                    {
                        reportItem.Insurance = ((FinancePolicy)item).Insurance.InsuranceName;
                    }
                    reportItem.PolicyNumber = ((FinancePolicy)item).AssociateNumber;
                }
                else if (item is ForeingWorkersPolicy)
                {
                    if (((ForeingWorkersPolicy)item).Client != null)
                    {
                        reportItem.ClientName = ((ForeingWorkersPolicy)item).Client.LastName + " " + ((ForeingWorkersPolicy)item).Client.FirstName;
                        reportItem.IdNumber = ((ForeingWorkersPolicy)item).Client.IdNumber;
                    }
                    if (((ForeingWorkersPolicy)item).Company != null)
                    {
                        reportItem.CompanyName = ((ForeingWorkersPolicy)item).Company.CompanyName;
                    }
                    if (((ForeingWorkersPolicy)item).EndDate != null)
                    {
                        reportItem.EndDate = ((DateTime)((ForeingWorkersPolicy)item).EndDate).ToString("dd/MM/yyyy");
                    }
                    if (((ForeingWorkersPolicy)item).StartDate != null)
                    {
                        reportItem.StartDate = ((DateTime)((ForeingWorkersPolicy)item).StartDate).ToString("dd/MM/yyyy");
                    }
                    if (((ForeingWorkersPolicy)item).TotalPremium != null)
                    {
                        reportItem.Premium = ((decimal)((ForeingWorkersPolicy)item).TotalPremium).ToString("0.##");
                    }
                    if (((ForeingWorkersPolicy)item).ForeingWorkersIndustry != null)
                    {
                        reportItem.Industry = ((ForeingWorkersPolicy)item).ForeingWorkersIndustry.ForeingWorkersIndustryName;
                    }
                    if (((ForeingWorkersPolicy)item).Insurance != null)
                    {
                        reportItem.Insurance = ((ForeingWorkersPolicy)item).Insurance.InsuranceName;
                    }

                    reportItem.PolicyNumber = ((ForeingWorkersPolicy)item).PolicyNumber;
                }
                reportItem.Header = string.Format(@"דו''ח פוליסות שלי{0}{1}{0}סה''כ פרמיה שנתית: {2}", Environment.NewLine, lblFilterDetails.Content,lblTotalPremium.Content);

                reportList.Add(reportItem);
            }
            return reportList;
        }

        private void mItemPrintClientsReport_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                frmPrintPreview preview = new frmPrintPreview("ClientsReport", dgClientsReports.ItemsSource);
                preview.ShowDialog();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void rbGoByNew_Checked(object sender, RoutedEventArgs e)
        {
            dpFromDat.SelectedDate = new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.Today.Day);
            dgClientByNewBinding();
            
        }

        private void cbIsNew_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

            selected = int.Parse(cbIsNew.SelectedIndex.ToString());
            dgClientByNewBinding();

            //rbGoByNew_Checked(sender, e);
            //dgClientByNewBinding();
        }

        private void btnSetDate_Click(object sender, RoutedEventArgs e)
        {
            dgClientByNewBinding();
        }

        private void btnMinusYear_Click(object sender, RoutedEventArgs e)
        {
            dpFromDat.SelectedDate = new DateTime(DateTime.Today.Year - 1, DateTime.Today.Month, DateTime.Today.Day);
        }

        private void btnMinusTwo_Click(object sender, RoutedEventArgs e)
        {
            dpFromDat.SelectedDate = new DateTime(DateTime.Today.Year - 2, DateTime.Today.Month, DateTime.Today.Day);
        }

        private void btnMinsThree_Click(object sender, RoutedEventArgs e)
        {
            dpFromDat.SelectedDate = new DateTime(DateTime.Today.Year - 3, DateTime.Today.Month, DateTime.Today.Day);
        }
    }
}
