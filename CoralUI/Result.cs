﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace CoralUI
{
    public class Result
    {
        public String DocPath { get; set; }
        public ImageSource DocIcon { get; set; }
    }
}
