﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using CoralBusinessLogics;
using System.IO;


namespace CoralUI
{
    /// <summary>
    /// Interaction logic for frmHealthPolicy.xaml
    /// </summary>
    public partial class frmHealthPolicy : Window
    {
        Client client = null;
        User handlerUser = null;
        InsurancesLogic insuranceLogic = new InsurancesLogic();
        HealthIndustriesLogic healthIndustryLogic = new HealthIndustriesLogic();
        AgentsLogic agentLogic = new AgentsLogic();
        PolicyHoldersLogic policyHoldersLogic = new PolicyHoldersLogic();
        List<object> nullErrorList = null;
        InputsValidations validations = new InputsValidations();
        HealthPoliciesLogic healthPoliciesLogic = new HealthPoliciesLogic();
        int policyId;
        private bool isAddition;
        private HealthPolicy healthPolicy = null;
        List<HealthCoverage> coverages = new List<HealthCoverage>();
        CoveragesLogic coveragesLogic = new CoveragesLogic();
        List<HealthTracking> trackings = new List<HealthTracking>();
        TrackingsLogics trackingLogics = new TrackingsLogics();
        ListViewSettings lv = new ListViewSettings();
        string path = null;
        string newPath = null;
        string clientDirPath = null;
        string healthPath = null;
        bool isOffer;
        private bool isChanges=false;
        bool confirmBeforeClosing = true;
        bool cancelClose = false;
        StandardMoneyCollection standardMoneyCollection;
        StandardMoneyCollectionLogic moneyCollectionLogic = new StandardMoneyCollectionLogic();
        private List<StandardMoneyCollectionTracking> standardMoneyCollectionTrackings;

        public frmHealthPolicy(Client clientToAddPolicy, User user, bool isOffer)
        {
            InitializeComponent();
            this.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            client = clientToAddPolicy;
            handlerUser = user;
            this.isOffer = isOffer;            
        }

        public frmHealthPolicy(Client client, User user, bool isAddition, HealthPolicy healthPolicy)
        {
            InitializeComponent();
            this.client = client;
            handlerUser = user;
            this.isAddition = isAddition;
            this.healthPolicy = healthPolicy;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            string strAppDir = System.AppDomain.CurrentDomain.BaseDirectory;
            string path = strAppDir + @"Coral Files\ClientsPath.txt";

            if (File.Exists(path))
            {
                clientDirPath = File.ReadAllText(path);
            }
            else
            {
                MessageBox.Show("לא נמצא תיקיית הלקוחות. נא בדוק בהגדרות ראשוניות את נתיב התיקייה");
                Close();
                return;
            }
            healthPath = clientDirPath + @"\" + client.ClientID;
            cbCompanyBinding();
            cbPolicyHolderBinding();
            if (client != null)
            {
                string clientDetails;
                if (client.CellPhone != null && client.CellPhone != "")
                {
                    clientDetails = string.Format("{0} {1}  ת.ז: {2} נייד: {3}", client.FirstName, client.LastName, client.IdNumber, client.CellPhone);
                }
                else
                {
                    clientDetails = string.Format("{0} {1}  ת.ז: {2}", client.FirstName, client.LastName, client.IdNumber);
                }
                lblClientNameAndId.Content = clientDetails;
            }
            if (healthPolicy == null)
            {
                dpOpenDate.SelectedDate = DateTime.Now;
                dpStartDate.SelectedDate = DateTime.Now;
                txtAddition.Text = "0";
            }
            else
            {
                dpOpenDate.SelectedDate = healthPolicy.OpenDate;
                txtPolicyNumber.Text = healthPolicy.PolicyNumber;
                txtAddition.Text = healthPolicy.Addition.ToString();
                dpStartDate.SelectedDate = healthPolicy.StartDate;
                dpEndDate.SelectedDate = healthPolicy.EndDate;
                isOffer = healthPolicy.IsOffer;
                txtInsurancedName.Text = healthPolicy.InsuredName;
                txtInsurancedNumber.Text = healthPolicy.InsuredIdNumber;
                txtPrimaryIndex.Text = healthPolicy.PrimaryIndex;
                if (healthPolicy.SubAnnual!=null)
                {
                    txtSubAnnual.Text = ((decimal)healthPolicy.SubAnnual).ToString("0.##");
                }
                if (healthPolicy.PolicyFactor!=null)
                {
                    txtPolicyFactor.Text = ((decimal)healthPolicy.PolicyFactor).ToString("0.##");
                }
                if (healthPolicy.TotalMonthlyPayment != null)
                {
                    txtMonthlyDeposit.Text = ((decimal)healthPolicy.TotalMonthlyPayment).ToString("0.##");
                }
                if (healthPolicy.TotalMonthlyPayment!=null)
                {
                    txtMonthlyDeposit.Text = ((decimal)healthPolicy.TotalMonthlyPayment).ToString("0.##");
                }
                txtBankName.Text = healthPolicy.Bank;
                txtBankBranch.Text = healthPolicy.BankBranch;
                txtBankAccount.Text = healthPolicy.AccountNumber;
                chbIsMortgaged.IsChecked = healthPolicy.IsMortgaged;
                txtBankMortgaged.Text = healthPolicy.BankName;
                txtBranchMortgaged.Text = healthPolicy.BankBranchNumber;
                txtAddressMortgaged.Text = healthPolicy.BankAddress;
                txtBankPhoneMortgaged.Text = healthPolicy.BankPhone1;
                txtBankFaxMortgaged.Text = healthPolicy.BankFax;
                txtBankContactNameMortgaged.Text = healthPolicy.BankContactName;
                txtBankEmailMortgaged.Text = healthPolicy.BankEmail;
                txtCommentsGeneral.Text = healthPolicy.Comments;
                var principalAgentNumbers = cbPrincipalAgentNumber.Items;
                foreach (var item in principalAgentNumbers)
                {
                    AgentNumber principalAgentNumber = (AgentNumber)item;
                    if (principalAgentNumber.AgentNumberID == healthPolicy.PrincipalAgentNumberID)
                    {
                        cbPrincipalAgentNumber.SelectedItem = item;
                        break;
                    }
                }
                var secundaryAgentNumbers = cbSecundaryAgentNumber.Items;
                foreach (var item in secundaryAgentNumbers)
                {
                    AgentNumber secundaryAgentNumber = (AgentNumber)item;
                    if (secundaryAgentNumber.AgentNumberID == healthPolicy.SubAgentNumberID)
                    {
                        cbSecundaryAgentNumber.SelectedItem = item;
                        break;
                    }
                }
                var paymentMethods = cbPaymentMode.Items;
                foreach (var item in paymentMethods)
                {
                    if (healthPolicy.PaymentMethod == ((ComboBoxItem)item).Content.ToString())
                    {
                        cbPaymentMode.SelectedItem = item;
                        break;
                    }
                }
                //var companies = cbCompany.Items;
                //foreach (var item in companies)
                //{
                //    InsuranceCompany company = (InsuranceCompany)item;
                //    if (company.CompanyID == healthPolicy.CompanyID)
                //    {
                //        cbCompany.SelectedItem = item;
                //        break;
                //    }
                //}
                if (healthPolicy.CompanyID != null)
                {
                    SelectItemInCb((int)healthPolicy.CompanyID);
                }
                var policyHolders = cbPolicyHolder.Items;
                foreach (var item in policyHolders)
                {
                    PolicyOwner holder = (PolicyOwner)item;
                    if (holder.PolicyOwnerID == healthPolicy.PolicyOwnerID)
                    {
                        cbPolicyHolder.SelectedItem = item;
                        break;
                    }
                }
                if (healthPolicy.Hatzmada == true)
                {
                    cbIndexation.SelectedIndex = 0;
                }
                else if (healthPolicy.Hatzmada == false)
                {
                    cbIndexation.SelectedIndex = 1;
                }
                coverages = coveragesLogic.GetAllHealthCoveragesByPolicy(healthPolicy.HealthPolicyID);
                dgCoveragesBinding();
                trackings = trackingLogics.GetAllHealthTrackingsByPolicy(healthPolicy.HealthPolicyID);
                dgTrackingsBinding();
                
                if (healthPolicy.Addition != 0)
                {
                    txtPolicyNumber.IsEnabled = false;
                }
                if (int.Parse(healthPoliciesLogic.GetAdditionNumber(healthPolicy.PolicyNumber)) - 1 > healthPolicy.Addition)
                {
                    dpGeneral.IsEnabled = false;
                    dpCoverage.IsEnabled = false;
                    dpTracking.IsEnabled = false;
                }
                if (isAddition)
                {
                    txtPolicyNumber.IsEnabled = false;
                    txtAddition.Text = healthPoliciesLogic.GetAdditionNumber(healthPolicy.PolicyNumber);
                    DateTime now = DateTime.Now;
                    dpOpenDate.SelectedDate = now;
                    dpStartDate.SelectedDate = now;
                    cbCompany.IsEnabled = false;
                }
                else
                {
                    standardMoneyCollection = moneyCollectionLogic.GetStandardMoneyColletionByPolicy(healthPolicy);
                    if (standardMoneyCollection != null)
                    {
                        chbStandardMoneyCollection.IsChecked = true;
                        chbStandardMoneyCollection.IsEnabled = false;
                        standardMoneyCollectionTrackings = moneyCollectionLogic.GetTrakcingsByStsndardMoneyCollection(standardMoneyCollection);
                    }
                }

            }

        }

        private void cbPolicyHolderBinding()
        {
            cbPolicyHolder.ItemsSource = policyHoldersLogic.GetAllPolicyHolders();
            cbPolicyHolder.DisplayMemberPath = "PolicyOwnerName";
        }

        private void cbCompanyBinding()
        {
            cbCompany.ItemsSource = insuranceLogic.GetActiveCompaniesByInsurance("בריאות");
            cbCompany.DisplayMemberPath = "Company.CompanyName";
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            if (nullErrorList != null && nullErrorList.Count > 0)
            {
                foreach (var item in nullErrorList)
                {
                    if (item is TextBox)
                    {
                        ((TextBox)item).ClearValue(BorderBrushProperty);
                    }
                    else if (item is ComboBox)
                    {
                        ((ComboBox)item).ClearValue(BorderBrushProperty);
                    }
                    else if (item is DatePicker)
                    {
                        ((DatePicker)item).ClearValue(BorderBrushProperty);
                    }
                }
            }
            if (!validations.IsDigitsOnly(txtPolicyNumber.Text))
            {
                cancelClose = true;
                MessageBox.Show("מס' פוליסה יכול להכיל מספרים בלבד", "", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            //בדיקת שדות חובה
            nullErrorList = validations.InputNullValidation(new object[] { txtPolicyNumber, txtAddition, dpStartDate, dpEndDate, cbCompany, cbInsuranceType, txtMonthlyDeposit });
            if (nullErrorList.Count > 0)
            {
                cancelClose = true;
                //tbItemGeneral.Focus();
                MessageBox.Show("נא למלאות שדות חובה", "", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (dpEndDate.SelectedDate <= dpStartDate.SelectedDate)
            {
                cancelClose = true;
                MessageBox.Show("תאריך סיום חייב להיות גדול מתאריך תחילה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            int addition = Convert.ToInt32(txtAddition.Text);
            bool? isIndexation = null;
            if (cbIndexation.SelectedIndex == 0)
            {
                isIndexation = true;
            }
            if (cbIndexation.SelectedIndex == 1)
            {
                isIndexation = false;
            }
            decimal? subAnnual = null;
            decimal subAnnualResult;
            if (Decimal.TryParse(txtSubAnnual.Text, out subAnnualResult))
            {
                subAnnual = subAnnualResult;
            }
            decimal? policyFactor = null;
            decimal policyFactorResult;
            if (Decimal.TryParse(txtPolicyFactor.Text, out policyFactorResult))
            {
                policyFactor = policyFactorResult;
            }
            decimal? totalMonthlyPayment = null;
            decimal totalMonthlyPaymentResult;

            if (Decimal.TryParse(txtMonthlyDeposit.Text, out totalMonthlyPaymentResult))
            {
                totalMonthlyPayment = totalMonthlyPaymentResult;
            }
            int? principalAgentNumberID = null;
            if ((AgentNumber)cbPrincipalAgentNumber.SelectedItem != null)
            {
                principalAgentNumberID = ((AgentNumber)cbPrincipalAgentNumber.SelectedItem).AgentNumberID;
            }
            int? subAgentNumberID = null;
            if ((AgentNumber)cbSecundaryAgentNumber.SelectedItem != null)
            {
                subAgentNumberID = ((AgentNumber)cbSecundaryAgentNumber.SelectedItem).AgentNumberID;
            }
            int? policyOwnerID = null;
            if ((PolicyOwner)cbPolicyHolder.SelectedItem != null)
            {
                policyOwnerID = ((PolicyOwner)cbPolicyHolder.SelectedItem).PolicyOwnerID;
            }
            int? factoryNumberID = null;
            if ((FactoryNumber)cbPolicyHolderNumber.SelectedItem != null)
            {
                factoryNumberID = ((FactoryNumber)cbPolicyHolderNumber.SelectedItem).FactoryNumberID;
            }
            string paymentMode = null;
            if (cbPaymentMode.SelectedItem != null)
            {
                paymentMode = ((ComboBoxItem)cbPaymentMode.SelectedItem).Content.ToString();
            }
            try
            {
                if (healthPolicy == null || isAddition == true)
                {
                    policyId = healthPoliciesLogic.InsertHealthPolicy(client.ClientID, isOffer, txtPolicyNumber.Text, addition, (DateTime)dpOpenDate.SelectedDate, ((InsuranceCompany)cbCompany.SelectedItem).CompanyID, principalAgentNumberID, subAgentNumberID, ((HealthInsuranceType)cbInsuranceType.SelectedItem).HealthInsuranceTypeID, dpStartDate.SelectedDate, dpEndDate.SelectedDate, txtPrimaryIndex.Text, txtInsurancedName.Text, txtInsurancedNumber.Text, policyOwnerID, factoryNumberID, paymentMode, isIndexation, subAnnual, policyFactor, totalMonthlyPayment, txtBankName.Text, txtBankBranch.Text, txtBankAccount.Text, chbIsMortgaged.IsChecked, txtBankMortgaged.Text, txtBranchMortgaged.Text, txtAddressMortgaged.Text, txtBankPhoneMortgaged.Text, txtBankFaxMortgaged.Text, txtBankContactNameMortgaged.Text, txtBankEmailMortgaged.Text, txtCommentsGeneral.Text);
                    if (!isOffer)
                    {
                        path = healthPath + @"\בריאות\בריאות " + txtPolicyNumber.Text;// +" " + ((LifeIndustry)cbLifeIndustry.SelectedItem).LifeIndustryName;
                    }
                    else
                    {
                        path = healthPath + @"\הצעות\בריאות\" + txtPolicyNumber.Text;
                    }
                }
                else
                {
                    healthPoliciesLogic.UpdateHealthPolicy(healthPolicy.HealthPolicyID, txtPolicyNumber.Text, ((InsuranceCompany)cbCompany.SelectedItem).CompanyID, principalAgentNumberID, subAgentNumberID, ((HealthInsuranceType)cbInsuranceType.SelectedItem).HealthInsuranceTypeID, dpStartDate.SelectedDate, dpEndDate.SelectedDate, txtPrimaryIndex.Text, txtInsurancedName.Text, txtInsurancedNumber.Text, policyOwnerID, factoryNumberID, paymentMode, isIndexation, subAnnual, policyFactor, totalMonthlyPayment, txtBankName.Text, txtBankBranch.Text, txtBankAccount.Text, chbIsMortgaged.IsChecked, txtBankMortgaged.Text, txtBranchMortgaged.Text, txtAddressMortgaged.Text, txtBankPhoneMortgaged.Text, txtBankFaxMortgaged.Text, txtBankContactNameMortgaged.Text, txtBankEmailMortgaged.Text, txtCommentsGeneral.Text);
                    if (!isOffer)
                    {
                    string[] dirs = Directory.GetDirectories(healthPath + @"\בריאות", "*" + healthPolicy.PolicyNumber + "*");
                    if (dirs.Count() > 0)
                    {
                        path = dirs[0];
                    }
                    newPath = healthPath + @"\בריאות\בריאות " + txtPolicyNumber.Text;// +" " + ((LifeIndustry)cbLifeIndustry.SelectedItem).LifeIndustryName;
                    }
                    else
                    {
                        string[] dirs = Directory.GetDirectories(healthPath + @"\הצעות\בריאות", "*" + healthPolicy.PolicyNumber + "*");
                        if (dirs.Count() > 0)
                        {
                            path = dirs[0];
                        }
                        newPath = healthPath + @"\הצעות\בריאות\" + txtPolicyNumber.Text;// +" " + ((LifeIndustry)cbLifeIndustry.SelectedItem).LifeIndustryName;
                    }

                }
                if (healthPolicy == null || isAddition == true)
                {
                    if (coverages.Count > 0)
                    {
                        coveragesLogic.InsertCoveragesToHealthPolicy(policyId, coverages, isAddition);
                    }
                    if (trackings.Count > 0)
                    {
                        trackingLogics.InsertHealthTrackings(policyId, trackings, isAddition);
                    }
                    if (chbStandardMoneyCollection.IsChecked == true)
                    {
                        int standardMoneyCollectionId = moneyCollectionLogic.InsertStandardMoneyCollection(standardMoneyCollection, new Insurance() { InsuranceName = "בריאות" }, policyId);
                        if (standardMoneyCollectionTrackings != null)
                        {
                            moneyCollectionLogic.InsertTrackingsToStandardMoneyCollection(standardMoneyCollectionId, standardMoneyCollectionTrackings);
                        }
                    }
                }
                else
                {
                    coveragesLogic.InsertCoveragesToHealthPolicy(healthPolicy.HealthPolicyID, coverages, isAddition);
                    trackingLogics.InsertHealthTrackings(healthPolicy.HealthPolicyID, trackings, isAddition);
                    if (chbStandardMoneyCollection.IsChecked == true)
                    {
                        int standardMoneyCollectionId = moneyCollectionLogic.InsertStandardMoneyCollection(standardMoneyCollection, new Insurance() { InsuranceName = "בריאות" }, healthPolicy.HealthPolicyID);
                        moneyCollectionLogic.InsertTrackingsToStandardMoneyCollection(standardMoneyCollectionId, standardMoneyCollectionTrackings);
                    }
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }

            if (isAddition == false)
            {
                if (healthPolicy == null)
                {
                    if (!Directory.Exists(@path))
                    {
                        Directory.CreateDirectory(@path);
                    }
                }
                else
                {
                    if (path == null && newPath != null)
                    {
                        Directory.CreateDirectory(newPath);
                    }
                    else if (path != newPath)
                    {
                        try
                        {
                            Directory.Move(path, newPath);
                        }
                        catch (Exception)
                        {
                            System.Windows.Forms.MessageBox.Show("לא ניתן למצוא את נתיב התיקייה");
                        }
                    }
                }
            }

            if (sender is Button)
            {
                Button btn = (Button)sender;
                if (btn.Name == "btnSave")
                {
                    confirmBeforeClosing = false;
                    Close();
                }
            }
        }

        private void btnUpdateCompanyTable_Click(object sender, RoutedEventArgs e)
        {
            int? selectedItemId = null;
            if (cbCompany.SelectedItem != null)
            {
                selectedItemId = ((InsuranceCompany)cbCompany.SelectedItem).CompanyID;
            }
            int insuranceID = insuranceLogic.GetInsuranceID("בריאות");
            InsuranceCompany company = new InsuranceCompany() { InsuranceID = insuranceID };
            frmUpdateTable updateCompaniesTable = new frmUpdateTable(company);
            updateCompaniesTable.ShowDialog();
            cbCompanyBinding();
            if (updateCompaniesTable.ItemAdded != null)
            {
                SelectItemInCb(((InsuranceCompany)updateCompaniesTable.ItemAdded).CompanyID);
            }
            else if (selectedItemId != null)
            {
                SelectItemInCb((int)selectedItemId);
            }
        }

        private void SelectItemInCb(int id)
        {
            var items = cbCompany.Items;
            foreach (var item in items)
            {
                InsuranceCompany parsedItem = (InsuranceCompany)item;
                if (parsedItem.CompanyID == id)
                {
                    cbCompany.SelectedItem = item;
                    break;
                }
            }
        }

        private void cbCompany_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cbCompany.SelectedItem == null)
            {
                cbPrincipalAgentNumber.SelectedItem = null;
                cbSecundaryAgentNumber.SelectedItem = null;
                cbPrincipalAgentNumber.IsEnabled = false;
                cbSecundaryAgentNumber.IsEnabled = false;
                return;
            }
            else
            {
                cbInsuranceType.IsEnabled = true;
                btnUpdateInsuranceTypeTable.IsEnabled = true;
                cbInsuranceTypeBinding();
                if (healthPolicy != null)
                {
                    //var insuranceTypes = cbInsuranceType.Items;
                    //foreach (var item in insuranceTypes)
                    //{
                    //    HealthInsuranceType insuranceType = (HealthInsuranceType)item;
                    //    if (insuranceType.HealthInsuranceTypeID == healthPolicy.HealthInsuranceTypeID)
                    //    {
                    //        cbInsuranceType.SelectedItem = item;
                    //        break;
                    //    }
                    //}
                    if (healthPolicy.HealthInsuranceTypeID != null)
                    {
                        SelectInsTypeItemInCb((int)healthPolicy.HealthInsuranceTypeID);
                    }
                }
            }
            int insuranceID = insuranceLogic.GetInsuranceID("בריאות");
            if (client != null)
            {
                cbPrincipalAgentNumber.ItemsSource = agentLogic.GetAgentNumbersByCompanyAndInsurance(client.PrincipalAgentID, (InsuranceCompany)cbCompany.SelectedItem, insuranceID);
                cbPrincipalAgentNumber.DisplayMemberPath = "Number";
                cbPrincipalAgentNumber.IsEnabled = true;
                cbPrincipalAgentNumber.SelectedIndex = 0;
                cbSecundaryAgentNumber.ItemsSource = agentLogic.GetAgentNumbersByCompanyAndInsurance(client.SecundaryAgentID, (InsuranceCompany)cbCompany.SelectedItem, insuranceID);
                cbSecundaryAgentNumber.DisplayMemberPath = "Number";
                cbSecundaryAgentNumber.IsEnabled = true;
                cbSecundaryAgentNumber.SelectedIndex = 0;
            }
            if (cbCompany.SelectedItem != null && cbPolicyHolder.SelectedItem != null)
            {
                cbPolicyHolderNumber.IsEnabled = true;
                cbPolicyHolderNumber.ItemsSource = policyHoldersLogic.GetNumbersByPolicyHolderAndCompany(((PolicyOwner)cbPolicyHolder.SelectedItem).PolicyOwnerID, ((InsuranceCompany)cbCompany.SelectedItem).CompanyID, insuranceID);
                cbPolicyHolderNumber.DisplayMemberPath = "Number";
                cbPolicyHolderNumber.SelectedIndex = 0;
            }
            else
            {
                cbPolicyHolderNumber.SelectedItem = null;
                cbPolicyHolderNumber.IsEnabled = false;
            }
        }

        private void cbInsuranceTypeBinding()
        {
            cbInsuranceType.ItemsSource = healthIndustryLogic.GetActiveHealthInsuranceTypesByCompany(((InsuranceCompany)cbCompany.SelectedItem).CompanyID);
            cbInsuranceType.DisplayMemberPath = "HealthInsuranceTypeName";
        }

        private void btnUpdateInsuranceTypeTable_Click(object sender, RoutedEventArgs e)
        {
            int? selectedItemId = null;
            if (cbInsuranceType.SelectedItem != null)
            {
                selectedItemId = ((HealthInsuranceType)cbInsuranceType.SelectedItem).HealthInsuranceTypeID;
            }
            HealthInsuranceType insuranceType = new HealthInsuranceType() { CompanyID = ((InsuranceCompany)cbCompany.SelectedItem).CompanyID };
            frmUpdateTable updateHealthInsuranceTypeTable = new frmUpdateTable(insuranceType);
            updateHealthInsuranceTypeTable.ShowDialog();
            cbInsuranceTypeBinding();

            if (updateHealthInsuranceTypeTable.ItemAdded != null)
            {
                SelectInsTypeItemInCb(((HealthInsuranceType)updateHealthInsuranceTypeTable.ItemAdded).HealthInsuranceTypeID);
            }
            else if (selectedItemId != null)
            {
                SelectInsTypeItemInCb((int)selectedItemId);
            }
            //SelectCbItem(cbInsuranceCompany,);
        }

        private void SelectInsTypeItemInCb(int id)
        {
            var items = cbInsuranceType.Items;
            foreach (var item in items)
            {
                HealthInsuranceType parsedItem = (HealthInsuranceType)item;
                if (parsedItem.HealthInsuranceTypeID == id)
                {
                    cbInsuranceType.SelectedItem = item;
                    break;
                }
            }
        }

        private void cbPolicyHolder_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            int insuranceID = insuranceLogic.GetInsuranceID("בריאות");
            if (cbCompany.SelectedItem != null && cbPolicyHolder.SelectedItem != null)
            {
                cbPolicyHolderNumber.IsEnabled = true;
                cbPolicyHolderNumber.ItemsSource = policyHoldersLogic.GetNumbersByPolicyHolderAndCompany(((PolicyOwner)cbPolicyHolder.SelectedItem).PolicyOwnerID, ((InsuranceCompany)cbCompany.SelectedItem).CompanyID, insuranceID);
                cbPolicyHolderNumber.DisplayMemberPath = "Number";
                cbPolicyHolderNumber.SelectedIndex = 0;
            }
            else
            {
                cbPolicyHolderNumber.SelectedItem = null;
                cbPolicyHolderNumber.IsEnabled = false;
            }
        }

        private void btnUpdatePolicyHolderTable_Click(object sender, RoutedEventArgs e)
        {
            frmAddPolicyOwner policyOwnerWindow = new frmAddPolicyOwner();
            policyOwnerWindow.ShowDialog();
            cbPolicyHolderBinding();
        }

        private void chbIsMortgaged_Checked(object sender, RoutedEventArgs e)
        {
            spMortgagedBankDetails.IsEnabled = true;
        }

        private void chbIsMortgaged_Unchecked(object sender, RoutedEventArgs e)
        {
            spMortgagedBankDetails.IsEnabled = false;
            txtBankMortgaged.Clear();
            txtBranchMortgaged.Clear();
            txtAddressMortgaged.Clear();
            txtBankPhoneMortgaged.Clear();
            txtBankFaxMortgaged.Clear();
            txtBankContactNameMortgaged.Clear();
            txtBankEmailMortgaged.Clear();
        }

        private void txtSubAnnual_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (sender is TextBox)
            {
                TextBox txtBox = (TextBox)sender;
                txtBox.ClearValue(BorderBrushProperty);
                if (txtBox.Text != "")
                {
                    try
                    {
                        decimal reusult = validations.ConvertStringToDecimal(txtBox.Text);
                    }
                    catch (Exception ex)
                    {
                        txtBox.BorderBrush = System.Windows.Media.Brushes.Red;
                        System.Windows.Forms.MessageBox.Show(ex.Message);
                        txtBox.Text = "";
                    }
                }
            }
        }

        private void btnNewCoverage_Click(object sender, RoutedEventArgs e)
        {
            frmLifeCoverage newCoverageWindow = new frmLifeCoverage((HealthInsuranceType)cbInsuranceType.SelectedItem);
            newCoverageWindow.ShowDialog();
            if (newCoverageWindow.HealthCoverage != null)
            {
                coverages.Add(newCoverageWindow.HealthCoverage);
            }
            dgCoveragesBinding();
        }

        private void dgCoveragesBinding()
        {
            dgHealthCoverages.ItemsSource = coverages.ToList();
            lv.AutoSizeColumns(dgHealthCoverages.View);
        }

        private void btnDeleteCoverage_Click(object sender, RoutedEventArgs e)
        {
            if (dgHealthCoverages.SelectedItem == null)
            {
                MessageBox.Show("נא לסמן כיסוי בטבלה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            if (MessageBox.Show("למחוק כיסוי", "אישור", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                coverages.Remove((HealthCoverage)dgHealthCoverages.SelectedItem);
                dgCoveragesBinding();
            }
            else
            {
                dgHealthCoverages.UnselectAll();
            }
        }

        private void btnUpdateCoverage_Click(object sender, RoutedEventArgs e)
        {
            if (dgHealthCoverages.SelectedItem == null)
            {
                MessageBox.Show("נא לסמן כיסוי בטבלה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            HealthCoverage coverageToUpdate = (HealthCoverage)dgHealthCoverages.SelectedItem;
            frmLifeCoverage updateCoverageWindow = new frmLifeCoverage(coverageToUpdate, (HealthInsuranceType)cbInsuranceType.SelectedItem);
            updateCoverageWindow.ShowDialog();
            coverageToUpdate = updateCoverageWindow.HealthCoverage;
            dgCoveragesBinding();
            dgHealthCoverages.UnselectAll();
        }

        private void dgHealthCoverages_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            DependencyObject src = (DependencyObject)(e.OriginalSource);
            while (!(src is Control))
            {
                if (!(src is Visual))
                {
                    break;
                }
                src = VisualTreeHelper.GetParent(src);
            }

            if ((src is Control && src.GetType() == typeof(ListViewItem)) || src.GetType() == typeof(Run))
            {
                if (dgHealthCoverages.SelectedItem == null)
                {
                    MessageBox.Show("נא לסמן כיסוי בטבלה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    return;
                }
                HealthCoverage coverageToUpdate = (HealthCoverage)dgHealthCoverages.SelectedItem;
                frmLifeCoverage updateCoverageWindow = new frmLifeCoverage(coverageToUpdate, (HealthInsuranceType)cbInsuranceType.SelectedItem);
                updateCoverageWindow.ShowDialog();
                coverageToUpdate = updateCoverageWindow.HealthCoverage;
                dgCoveragesBinding();
                dgHealthCoverages.UnselectAll();
            }
        }

        private void dgHealthCoverages_MouseDown(object sender, MouseButtonEventArgs e)
        {
            dgHealthCoverages.UnselectAll();
        }

        private void btnNewTracking_Click(object sender, RoutedEventArgs e)
        {
            frmTracking newTrackingWindow = new frmTracking(client, healthPolicy, handlerUser);
            newTrackingWindow.ShowDialog();
            if (newTrackingWindow.trackingContent != null)
            {
                trackings.Add(new HealthTracking() { TrackingContent = newTrackingWindow.trackingContent, Date = DateTime.Now, UserID = handlerUser.UserID, User = handlerUser });
                dgTrackingsBinding();
            }
        }

        private void dgTrackingsBinding()
        {
            dgHealthTrackings.ItemsSource = trackings.OrderByDescending(t => t.Date);
            lv.AutoSizeColumns(dgHealthTrackings.View);
        }

        private void btnDeleteTracking_Click(object sender, RoutedEventArgs e)
        {
            if (dgHealthTrackings.SelectedItem == null)
            {
                MessageBox.Show("נא לסמן מעקב בטבלה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            if (MessageBox.Show("למחוק מעקב", "אישור", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                trackings.Remove((HealthTracking)dgHealthTrackings.SelectedItem);
                dgTrackingsBinding();
            }
            else
            {
                dgHealthTrackings.UnselectAll();
            }
        }

        private void btnUpdateTracking_Click(object sender, RoutedEventArgs e)
        {
            if (dgHealthTrackings.SelectedItem == null)
            {
                MessageBox.Show("נא לסמן מעקב בטבלה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            HealthTracking trackingToUpdate = (HealthTracking)dgHealthTrackings.SelectedItem;
            frmTracking updateTrackingWindow = new frmTracking(trackingToUpdate, client, healthPolicy, handlerUser);
            updateTrackingWindow.ShowDialog();
            trackingToUpdate.TrackingContent = updateTrackingWindow.trackingContent;
            dgTrackingsBinding();
            dgHealthTrackings.UnselectAll();
        }

        private void dgHealthTrackings_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            DependencyObject src = (DependencyObject)(e.OriginalSource);
            while (!(src is Control))
            {
                if (!(src is Visual))
                {
                    break;
                }
                src = VisualTreeHelper.GetParent(src);
            }

            if ((src is Control && src.GetType() == typeof(ListViewItem)) || src.GetType() == typeof(Run))
            {
                if (dgHealthTrackings.SelectedItem == null)
                {
                    MessageBox.Show("נא לסמן מעקב בטבלה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    return;
                }
                HealthTracking trackingToUpdate = (HealthTracking)dgHealthTrackings.SelectedItem;
                frmTracking updateTrackingWindow = new frmTracking(trackingToUpdate, client, healthPolicy, handlerUser);
                updateTrackingWindow.ShowDialog();
                trackingToUpdate.TrackingContent = updateTrackingWindow.trackingContent;
                dgTrackingsBinding();
                dgHealthTrackings.UnselectAll();
            }
        }

        private void dgHealthTrackings_MouseDown(object sender, MouseButtonEventArgs e)
        {
            dgHealthTrackings.UnselectAll();
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void GroupBox_GotFocus(object sender, RoutedEventArgs e)
        {
            isChanges = true;
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (confirmBeforeClosing && isChanges)
            {
                if (MessageBox.Show("האם ברצונך לשמור שינוים שבצעת", "אישור", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    btnSave_Click(sender, new RoutedEventArgs());
                    if (cancelClose)
                    {
                        e.Cancel = true;
                        cancelClose = false;
                    }
                }
            }
        }

        private void chbStandardMoneyCollection_Checked(object sender, RoutedEventArgs e)
        {
            btnStandardMoneyCollectionDetails.IsEnabled = true;
        }
        private void chbStandardMoneyCollection_Unchecked(object sender, RoutedEventArgs e)
        {
            btnStandardMoneyCollectionDetails.IsEnabled = false;
        }
        private void btnStandardMoneyCollectionDetails_Click(object sender, RoutedEventArgs e)
        {
            frmStandardMoneyCollectionDetails window = null;
            if (standardMoneyCollection == null)
            {
                window = new frmStandardMoneyCollectionDetails(false, handlerUser);
            }
            else
            {
                window = new frmStandardMoneyCollectionDetails(standardMoneyCollection, standardMoneyCollectionTrackings, false, handlerUser);
            }
            window.ShowDialog();
            if (window.StandardMoneyCollection != null)
            {
                standardMoneyCollection = window.StandardMoneyCollection;
            }
            if (window.Trackings != null)
            {
                standardMoneyCollectionTrackings = window.Trackings;
            }
        }

    }
}
