﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using CoralBusinessLogics;

namespace CoralUI
{
    /// <summary>
    /// Interaction logic for frmClaimItem.xaml
    /// </summary>
    public partial class frmClaimItem : Window
    {
        public TravelDamage Damage { get; set; }
        InputsValidations validations = new InputsValidations();
        private bool isChanges = false;
        bool confirmBeforeClosing = true;
        bool cancelClose = false;
        private object selectedItem;

        public frmClaimItem()
        {
            InitializeComponent();
        }

        public frmClaimItem(object selectedItem)
        {
            InitializeComponent();
            this.selectedItem = selectedItem;
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            if (txtItemDescription.Text == "")
            {
                MessageBox.Show("תיאור הפריט הינו שדה חובה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                cancelClose = true;
                return;
            }
            decimal? amountClaimed = null;
            if (txtItemAmountClaimed.Text != "")
            {
                amountClaimed = decimal.Parse(txtItemAmountClaimed.Text);
            }
            if (selectedItem==null)
            {
                Damage = new TravelDamage() { ItemDescription = txtItemDescription.Text, AmountClaimed = amountClaimed, Comments = txtItemComments.Text };

            }
            else
            {
                Damage.AmountClaimed = amountClaimed;
                Damage.Comments = txtItemComments.Text;
                Damage.ItemDescription = txtItemDescription.Text;   
            }
            if (sender is Button)
            {
                Button btn = (Button)sender;
                if (btn.Name == "btnSave")
                {
                    confirmBeforeClosing = false;
                    Close();
                }
            }
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void txtAmountClaimed_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (sender is TextBox)
            {
                TextBox txtBox = (TextBox)sender;
                txtBox.ClearValue(BorderBrushProperty);
                if (txtBox.Text != "")
                {
                    try
                    {
                        decimal reusult = validations.ConvertStringToDecimal(txtBox.Text);
                    }
                    catch (Exception ex)
                    {
                        txtBox.BorderBrush = System.Windows.Media.Brushes.Red;
                        System.Windows.Forms.MessageBox.Show(ex.Message);
                        txtBox.Text = "";
                    }
                }
            }
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (confirmBeforeClosing && isChanges)
            {
                if (MessageBox.Show("האם ברצונך לשמור שינוים שבצעת", "אישור", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    btnSave_Click(sender, new RoutedEventArgs());
                    if (cancelClose)
                    {
                        e.Cancel = true;
                        cancelClose = false;
                    }
                }
            }
        }

        private void txtItemDescription_GotFocus(object sender, RoutedEventArgs e)
        {
            isChanges = true;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (selectedItem != null)
            {
                if (selectedItem is TravelDamage)
                {
                    Damage = (TravelDamage)selectedItem;
                    if (Damage.AmountClaimed != null)
                    {
                        txtItemAmountClaimed.Text = ((decimal)Damage.AmountClaimed).ToString("0.##");
                    }
                    txtItemComments.Text = Damage.Comments;
                    txtItemDescription.Text = Damage.ItemDescription;
                }
            }
        }
    }
}
