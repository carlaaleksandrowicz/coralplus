﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using CoralBusinessLogics;
using System.IO;

namespace CoralUI
{
    /// <summary>
    /// Interaction logic for frmPolicyOwners.xaml
    /// </summary>
    public partial class frmPolicyOwners : Window
    {
        InsurancesLogic insuranceLogic = new InsurancesLogic();
        PolicyHoldersLogic policyOwnersLogic = new PolicyHoldersLogic();
        List<PolicyOwner> policyOwners = null;
        Email email = new Email();
        LifePoliciesLogic lifeLogic = new LifePoliciesLogic();
        HealthPoliciesLogic healthLogic = new HealthPoliciesLogic();
        FinancePoliciesLogic financeLogic = new FinancePoliciesLogic();
        LifeIndustryLogic lifeIndustryLogic = new LifeIndustryLogic();
        FinanceIndustryLogic financeIndustryLogic = new FinanceIndustryLogic();
        private List<LifePolicy> lifePolicies;
        private List<HealthPolicy> healthPolicies;
        private List<FinancePolicy> financePolicies;
        List<LifePolicy> lifeFiltered = new List<LifePolicy>();
        List<HealthPolicy> healthFiltered = new List<HealthPolicy>();
        List<FinancePolicy> financeFiltered = new List<FinancePolicy>();
        User user;
        string clientDirPath = null;


        public frmPolicyOwners(User user)
        {
            InitializeComponent();
            this.SizeToContent = SizeToContent.Manual;
            this.user = user;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            string strAppDir = System.AppDomain.CurrentDomain.BaseDirectory;
            string path = strAppDir + @"Coral Files\ClientsPath.txt";
            if (File.Exists(path))
            {
                clientDirPath = File.ReadAllText(path);
            }
            else
            {
                MessageBox.Show("לא נמצא תיקיית הלקוחות. נא בדוק בהגדרות ראשוניות את נתיב התיקייה");
                Close();
                return;
            }
            if (policiesTabs.ActualHeight > 400)
            {
                dgPolicyOwners.Height = dgPolicyOwners.MaxHeight;
            }
            GetPolicyOwners();
        }

        private void GetPolicyOwners()
        {
            policyOwners = policyOwnersLogic.GetAllPolicyHolders();

            if (!Directory.Exists(clientDirPath + @"\PolicyOwners") && policyOwners.Count > 0)
            {
                foreach (var item in policyOwners)
                {
                    Directory.CreateDirectory(clientDirPath + @"\PolicyOwners\" + item.PolicyOwnerID);
                }
            }

            dgPolicyOwnersBinding();
        }

        private void dgPolicyOwnersBinding()
        {
            if (txtSearchPolicyOwner.Text != "")
            {
                string key = txtSearchPolicyOwner.Text;
                var filteredPolicyOwners = policyOwners.Where(o => (o.PolicyOwnerName != "" && o.PolicyOwnerName.StartsWith(key)) || (o.IdNumber != "" && o.IdNumber.StartsWith(key))).ToList();
                dgPolicyOwners.ItemsSource = filteredPolicyOwners;
                lblCount.Content = string.Format("סה''כ רשומות: {0}", filteredPolicyOwners.Count.ToString());
            }
            else
            {
                dgPolicyOwners.ItemsSource = policyOwners;
                lblCount.Content = string.Format("סה''כ רשומות: {0}", policyOwners.Count.ToString());
            }
        }

        private void PoliciesBinding()
        {
            if (tbItemLife.IsSelected)
            {
                lifeFiltered.Clear();
                lifeFiltered.AddRange(lifePolicies);
                if (cbStatus.SelectedIndex == 0)
                {
                    lifeFiltered = lifeFiltered.Where(p => p.EndDate >= DateTime.Today).ToList();
                }
                else if (cbStatus.SelectedIndex == 1)
                {
                    lifeFiltered = lifeFiltered.Where(p => p.EndDate < DateTime.Today).ToList();
                }
                if (cbCompany.SelectedItem != null)
                {
                    lifeFiltered = lifeFiltered.Where(p => p.CompanyID == ((InsuranceCompany)cbCompany.SelectedItem).CompanyID).ToList();
                }
                if (cbIndustry.SelectedItem != null)
                {
                    lifeFiltered = lifeFiltered.Where(p => p.LifeIndustryID == ((LifeIndustry)cbIndustry.SelectedItem).LifeIndustryID).ToList();
                }
                if (txtSearchPolicy.Text != "")
                {
                    string key = txtSearchPolicy.Text;
                    lifeFiltered = lifeFiltered.Where(p => p.Client.LastName.StartsWith(key) || p.Client.FirstName.StartsWith(key) || p.Client.IdNumber.StartsWith(key) || p.PolicyNumber.StartsWith(key)).ToList();
                }
                dgLife.ItemsSource = lifeFiltered.ToList();
            }
            else if (tbItemHealth.IsSelected)
            {
                healthFiltered.Clear();
                healthFiltered.AddRange(healthPolicies);
                if (cbStatus.SelectedIndex == 0)
                {
                    healthFiltered = healthFiltered.Where(p => p.EndDate >= DateTime.Today).ToList();
                }
                else if (cbStatus.SelectedIndex == 1)
                {
                    healthFiltered = healthFiltered.Where(p => p.EndDate < DateTime.Today).ToList();
                }
                if (cbCompany.SelectedItem != null)
                {
                    healthFiltered = healthFiltered.Where(p => p.CompanyID == ((InsuranceCompany)cbCompany.SelectedItem).CompanyID).ToList();
                }
                if (txtSearchPolicy.Text != "")
                {
                    string key = txtSearchPolicy.Text;
                    healthFiltered = healthFiltered.Where(p => p.Client.LastName.StartsWith(key) || p.Client.FirstName.StartsWith(key) || p.Client.IdNumber.StartsWith(key) || p.PolicyNumber.StartsWith(key)).ToList();
                }
                dgHealth.ItemsSource = healthFiltered.ToList();
            }
            else if (tbItemFinance.IsSelected)
            {
                financeFiltered.Clear();
                financeFiltered.AddRange(financePolicies);
                if (cbStatus.SelectedIndex == 0)
                {
                    financeFiltered = financeFiltered.Where(p => p.EndDate >= DateTime.Today).ToList();
                }
                else if (cbStatus.SelectedIndex == 1)
                {
                    financeFiltered = financeFiltered.Where(p => p.EndDate < DateTime.Today).ToList();
                }
                if (cbCompany.SelectedItem != null)
                {
                    financeFiltered = financeFiltered.Where(p => p.CompanyID == ((InsuranceCompany)cbCompany.SelectedItem).CompanyID).ToList();
                }
                if (cbIndustry.SelectedItem != null)
                {
                    financeFiltered = financeFiltered.Where(p => p.ProgramTypeID == ((FinanceProgramType)cbIndustry.SelectedItem).ProgramTypeID).ToList();
                }
                if (txtSearchPolicy.Text != "")
                {
                    string key = txtSearchPolicy.Text;
                    financeFiltered = financeFiltered.Where(p => p.Client.LastName.StartsWith(key) || p.Client.FirstName.StartsWith(key) || p.Client.IdNumber.StartsWith(key) || p.AssociateNumber.StartsWith(key)).ToList();
                }
                dgFinance.ItemsSource = financeFiltered.ToList();
            }
        }

        private void cbCompanyBinding()
        {
            if (tbItemLife.IsSelected)
            {
                cbCompany.ItemsSource = insuranceLogic.GetCompaniesByInsurance(insuranceLogic.GetInsuranceID("חיים"));
            }
            else if (tbItemHealth.IsSelected)
            {
                cbCompany.ItemsSource = insuranceLogic.GetCompaniesByInsurance(insuranceLogic.GetInsuranceID("בריאות"));
            }
            else if (tbItemFinance.IsSelected)
            {
                cbCompany.ItemsSource = insuranceLogic.GetCompaniesByInsurance(insuranceLogic.GetInsuranceID("פיננסים"));
            }
            cbCompany.DisplayMemberPath = "Company.CompanyName";
        }

        private void cbIndustryBinding()
        {
            if (tbItemLife.IsSelected)
            {
                cbIndustry.ItemsSource = lifeIndustryLogic.GetAllLifeIndustries();
                cbIndustry.DisplayMemberPath = "LifeIndustryName";
                cbIndustry.IsEnabled = true;
            }

            else if (tbItemFinance.IsSelected)
            {
                cbIndustry.ItemsSource = financeIndustryLogic.GetAllFinanceProgramTypes();
                cbIndustry.DisplayMemberPath = "ProgramTypeName";
                cbIndustry.IsEnabled = true;
            }

            else
            {
                cbIndustry.IsEnabled = false;
            }
        }

        private void ClearSearchInputs()
        {
            cbCompany.SelectedIndex = -1;
            cbIndustry.SelectedIndex = -1;
            txtSearchPolicy.Clear();
        }

        private void policiesTabs_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.Source is TabControl)
            {
                ClearSearchInputs();
                cbIndustryBinding();
                cbCompanyBinding();
                if (dgPolicyOwners.SelectedItem != null)
                {
                    PoliciesBinding();
                }
            }
        }

        private void txtSearchPolicyOwner_TextChanged(object sender, TextChangedEventArgs e)
        {
            dgPolicyOwnersBinding();
        }

        private void btnUpdatePolicyOwner_Click(object sender, RoutedEventArgs e)
        {
            if (dgPolicyOwners.SelectedItem == null)
            {
                MessageBox.Show("נא בחר מפעל", "", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            PolicyOwner ownerSelected = (PolicyOwner)dgPolicyOwners.SelectedItem;
            frmAddPolicyOwner policyOwnerWindow = new frmAddPolicyOwner(ownerSelected);
            policyOwnerWindow.ShowDialog();
            GetPolicyOwners();

            SelectOwnerInDg(ownerSelected, dgPolicyOwners);

        }

        private void SelectOwnerInDg(PolicyOwner selectedItem, ListView lv)
        {
            var dgItems = lv.Items;
            foreach (var item in dgItems)
            {
                PolicyOwner owner = (PolicyOwner)item;
                if (owner.PolicyOwnerID == ((PolicyOwner)selectedItem).PolicyOwnerID)
                {
                    lv.SelectedItem = item;
                    lv.ScrollIntoView(item);
                    ListViewItem listViewItem = lv.ItemContainerGenerator.ContainerFromItem(lv.SelectedItem) as ListViewItem;
                    if (listViewItem != null)
                    {
                        listViewItem.Focus();
                    }
                    break;
                }
            }
        }

        private void btnAddPolicyOwner_Click(object sender, RoutedEventArgs e)
        {
            frmAddPolicyOwner policyOwnerWindow = new frmAddPolicyOwner();
            policyOwnerWindow.ShowDialog();
            GetPolicyOwners();
        }

        private void dgPolicyOwners_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            DependencyObject src = (DependencyObject)(e.OriginalSource);
            while (!(src is Control))
            {
                if (!(src is Visual))
                {
                    break;
                }
                src = VisualTreeHelper.GetParent(src);
            }

            if ((src is Control && src.GetType() == typeof(ListViewItem)) || src.GetType() == typeof(Run))
            {
                btnUpdatePolicyOwner_Click(sender, new RoutedEventArgs());
            }
        }

        private void btnSendEmail_Click(object sender, RoutedEventArgs e)
        {
            if (dgPolicyOwners.SelectedItem == null)
            {
                MessageBox.Show("נא לסמן מפעל", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            PolicyOwner owner = (PolicyOwner)dgPolicyOwners.SelectedItem;
            if (owner.Email == "")
            {
                MessageBox.Show("אין למפעל כתובת דוא''ל במערכת", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            string[] address = new string[] { owner.Email };
            try
            {
                email.SendEmailFromOutlook("", "", null, address);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void dgPolicyOwners_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (dgPolicyOwners.SelectedItem != null)
            {
                GetPoliciesByOwner();
                PoliciesBinding();
            }
            else
            {
                dgFinance.ItemsSource = null;
                dgHealth.ItemsSource = null;
                dgLife.ItemsSource = null;
            }
        }

        private void GetPoliciesByOwner()
        {
            lifePolicies = lifeLogic.GetPoliciesByOwner((PolicyOwner)dgPolicyOwners.SelectedItem);

            healthPolicies = healthLogic.GetPoliciesByOwner((PolicyOwner)dgPolicyOwners.SelectedItem);

            financePolicies = financeLogic.GetPoliciesByOwner((PolicyOwner)dgPolicyOwners.SelectedItem);
        }

        private void cbStatus_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (dgPolicyOwners.SelectedItem != null)
            {
                PoliciesBinding();
            }
        }

        private void txtSearchPolicy_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (dgPolicyOwners.SelectedItem != null)
            {
                PoliciesBinding();
            }
        }

        private void btnClearPolicySearch_Click(object sender, RoutedEventArgs e)
        {
            ClearSearchInputs();
        }

        private void btnClearPolicyOwner_Click(object sender, RoutedEventArgs e)
        {
            txtSearchPolicyOwner.Clear();
        }

        private void btnUpdatePolicy_Click(object sender, RoutedEventArgs e)
        {
            if (tbItemLife.IsSelected)
            {
                if (dgLife.SelectedItem == null)
                {
                    MessageBox.Show("נא לסמן את הפוליסה שברצונך לעדכן", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    return;
                }
                else
                {
                    frmLifePolicy updateLifePolicy = new frmLifePolicy(((LifePolicy)dgLife.SelectedItem).Client, user, false, (LifePolicy)dgLife.SelectedItem);
                    updateLifePolicy.ShowDialog();
                }
            }

            else if (tbItemHealth.IsSelected)
            {
                if (dgHealth.SelectedItem == null)
                {
                    MessageBox.Show("נא לסמן את הפוליסה שברצונך לעדכן", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    return;
                }
                else
                {
                    frmHealthPolicy updateHealthPolicy = new frmHealthPolicy(((HealthPolicy)dgHealth.SelectedItem).Client, user, false, (HealthPolicy)dgHealth.SelectedItem);
                    updateHealthPolicy.ShowDialog();
                }

            }
            else if (tbItemFinance.IsSelected)
            {
                if (dgFinance.SelectedItem == null)
                {
                    MessageBox.Show("נא לסמן את הפוליסה שברצונך לעדכן", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    return;
                }
                else
                {
                    frmFinance updateFinancePolicy = new frmFinance(((FinancePolicy)dgFinance.SelectedItem).Client, user, false, (FinancePolicy)dgFinance.SelectedItem);
                    updateFinancePolicy.ShowDialog();
                }

            }
            GetPoliciesByOwner();
            PoliciesBinding();
        }

        private void dgLife_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            DependencyObject src = (DependencyObject)(e.OriginalSource);
            while (!(src is Control))
            {
                if (!(src is Visual))
                {
                    break;
                }
                src = VisualTreeHelper.GetParent(src);
            }

            if ((src is Control && src.GetType() == typeof(ListViewItem)) || src.GetType() == typeof(Run))
            {
                if (dgLife.SelectedItem == null)
                {
                    MessageBox.Show("נא לסמן את הפוליסה שברצונך לעדכן", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                }
                else
                {
                    frmLifePolicy updateLifePolicy = new frmLifePolicy(((LifePolicy)dgLife.SelectedItem).Client, user, false, (LifePolicy)dgLife.SelectedItem);
                    updateLifePolicy.ShowDialog();
                    GetPoliciesByOwner();
                    PoliciesBinding();
                }
            }
        }

        private void dgHealth_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            DependencyObject src = (DependencyObject)(e.OriginalSource);
            while (!(src is Control))
            {
                if (!(src is Visual))
                {
                    break;
                }
                src = VisualTreeHelper.GetParent(src);
            }

            if ((src is Control && src.GetType() == typeof(ListViewItem)) || src.GetType() == typeof(Run))
            {
                if (dgHealth.SelectedItem == null)
                {
                    MessageBox.Show("נא לסמן את הפוליסה שברצונך לעדכן", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                }
                else
                {
                    frmHealthPolicy updateHealthPolicy = new frmHealthPolicy(((HealthPolicy)dgHealth.SelectedItem).Client, user, false, (HealthPolicy)dgHealth.SelectedItem);
                    updateHealthPolicy.ShowDialog();
                    GetPoliciesByOwner();
                    PoliciesBinding();
                }
            }
        }

        private void dgFinance_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            DependencyObject src = (DependencyObject)(e.OriginalSource);
            while (!(src is Control))
            {
                if (!(src is Visual))
                {
                    break;
                }
                src = VisualTreeHelper.GetParent(src);
            }

            if ((src is Control && src.GetType() == typeof(ListViewItem)) || src.GetType() == typeof(Run))
            {
                if (dgFinance.SelectedItem == null)
                {
                    MessageBox.Show("נא לסמן את הפוליסה שברצונך לעדכן", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                }
                else
                {
                    frmFinance updateFinancePolicy = new frmFinance(((FinancePolicy)dgFinance.SelectedItem).Client, user, false, (FinancePolicy)dgFinance.SelectedItem);
                    updateFinancePolicy.ShowDialog();
                    GetPoliciesByOwner();
                    PoliciesBinding();
                }
            }
        }

        private void btnOpticArchive_Click(object sender, RoutedEventArgs e)
        {
            if (tbItemLife.IsSelected)
            {
                if (dgLife.SelectedItem == null)
                {
                    MessageBox.Show("נא לסמן פוליסה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    return;
                }
                frmScan2 archiveWindow = new frmScan2(((LifePolicy)dgLife.SelectedItem).Client, (LifePolicy)dgLife.SelectedItem, "", user);
                archiveWindow.ShowDialog();
            }
            else if (tbItemHealth.IsSelected)
            {
                if (dgHealth.SelectedItem == null)
                {
                    MessageBox.Show("נא לסמן פוליסה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    return;
                }
                frmScan2 archiveWindow = new frmScan2(((HealthPolicy)dgHealth.SelectedItem).Client, (HealthPolicy)dgHealth.SelectedItem, "", user);
                archiveWindow.ShowDialog();
            }
            else if (tbItemFinance.IsSelected)
            {
                if (dgFinance.SelectedItem == null)
                {
                    MessageBox.Show("נא לסמן פוליסה", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    return;
                }
                frmScan2 archiveWindow = new frmScan2(((FinancePolicy)dgFinance.SelectedItem).Client, (FinancePolicy)dgFinance.SelectedItem, "", user);
                archiveWindow.ShowDialog();
            }
        }

        private void btnOwnerOpticArchive_Click(object sender, RoutedEventArgs e)
        {
            if (dgPolicyOwners.SelectedItem == null)
            {
                MessageBox.Show("נא לסמן מפעל", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            frmScan2 archiveWindow = new frmScan2((PolicyOwner)dgPolicyOwners.SelectedItem);
            archiveWindow.ShowDialog();
        }
    }
}
