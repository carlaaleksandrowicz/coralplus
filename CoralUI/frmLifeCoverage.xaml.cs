﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using CoralBusinessLogics;

namespace CoralUI
{
    /// <summary>
    /// Interaction logic for frmLifeCoverage.xaml
    /// </summary>
    public partial class frmLifeCoverage : Window
    {
        CoveragesLogic coverageLogic = new CoveragesLogic();
        FundType fundType = new FundType();
        InputsValidations validations = new InputsValidations();
        private HealthInsuranceType healthInsuranceType = null;
        private PersonalAccidentsInsuranceType personalAccidentsInsurance;
        private bool isChanges = false;
        bool confirmBeforeClosing = true;
        bool cancelClose = false;


        public LifeCoverage NewCoverage { get; set; }
        public HealthCoverage HealthCoverage { get; set; }
        public PersonalAccidentsCoverage PersonalAccidentsCoverage { get; set; }

        public frmLifeCoverage(FundType fundType)
        {
            InitializeComponent();
            dpStartDate.SelectedDate = DateTime.Now;
            dpEndDate.SelectedDate = DateTime.Now;
            this.fundType = fundType;
        }

        public frmLifeCoverage(LifeCoverage coverageToUpdate, FundType fundType)
        {
            InitializeComponent();
            NewCoverage = coverageToUpdate;
            this.fundType = fundType;
        }

        public frmLifeCoverage(HealthInsuranceType healthInsuranceType)
        {
            InitializeComponent();
            dpStartDate.SelectedDate = DateTime.Now;
            dpEndDate.SelectedDate = DateTime.Now;
            this.healthInsuranceType = healthInsuranceType;
        }

        public frmLifeCoverage(HealthCoverage coverageToUpdate, HealthInsuranceType healthInsuranceType)
        {
            InitializeComponent();
            HealthCoverage = coverageToUpdate;
            this.healthInsuranceType = healthInsuranceType;
        }

        public frmLifeCoverage(PersonalAccidentsInsuranceType personalAccidentsInsurance)
        {
            InitializeComponent();
            dpStartDate.SelectedDate = DateTime.Now;
            dpEndDate.SelectedDate = DateTime.Now;
            this.personalAccidentsInsurance = personalAccidentsInsurance;
        }

        public frmLifeCoverage(PersonalAccidentsCoverage coverageToUpdate, PersonalAccidentsInsuranceType personalAccidentsInsuranceType)
        {
            InitializeComponent();
            PersonalAccidentsCoverage = coverageToUpdate;
            this.personalAccidentsInsurance = personalAccidentsInsuranceType;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            cbCoverageNameBinding();
            if (healthInsuranceType == null && personalAccidentsInsurance == null)
            {
                if (NewCoverage != null)
                {
                    var coverageTypes = cbCoverageName.Items;
                    foreach (var item in coverageTypes)
                    {
                        if (NewCoverage.LifeCoverageTypeID == ((LifeCoverageType)item).LifeCoverageTypeID)
                        {
                            cbCoverageName.SelectedItem = item;
                            break;
                        }
                    }

                    txtComments.Text = NewCoverage.Comments;
                    txtCoverageCode.Text = NewCoverage.LifeCoverageType.LifeCoverageCode;
                    if (NewCoverage.IndexedPremium != null)
                    {
                        txtIndexedAmount.Text = ((decimal)NewCoverage.IndexedAmount).ToString("0.##");
                    }
                    if (NewCoverage.IndexedPremium != null)
                    {
                        txtIndexedPremium.Text = ((decimal)NewCoverage.IndexedPremium).ToString("0.##");
                    }
                    if (NewCoverage.InsuranceAmount != null)
                    {
                        txtInsuranceAmount.Text = ((decimal)NewCoverage.InsuranceAmount).ToString("0.##");
                    }
                    if (NewCoverage.MedicalAddition != null)
                    {
                        txtMedicalAddition.Text = ((decimal)NewCoverage.MedicalAddition).ToString("0.##");
                    }
                    if (NewCoverage.MonthlyPremuim != null)
                    {
                        txtMonthlyPremium.Text = ((decimal)NewCoverage.MonthlyPremuim).ToString("0.##");
                    }
                    if (NewCoverage.ProfessionalAddition != null)
                    {
                        txtProfessionalAddition.Text = ((decimal)NewCoverage.ProfessionalAddition).ToString("0.##");
                    }
                    txtUpToAge.Text = NewCoverage.UpToAge.ToString();
                    dpEndDate.SelectedDate = NewCoverage.EndDate;
                    dpStartDate.SelectedDate = NewCoverage.StartDate;
                    if (NewCoverage.Status == true)
                    {
                        cbStatus.SelectedIndex = 0;
                    }
                    else
                    {
                        cbStatus.SelectedIndex = 1;
                    }
                }
            }
            else if (healthInsuranceType != null)
            {
                lblUpToAge.Content = "תקופה";
                if (HealthCoverage != null)
                {
                    var coverageTypes = cbCoverageName.Items;
                    foreach (var item in coverageTypes)
                    {
                        if (HealthCoverage.HealthCoverageTypeID == ((HealthCoverageType)item).HealthCoverageTypeID)
                        {
                            cbCoverageName.SelectedItem = item;
                            break;
                        }
                    }

                    txtComments.Text = HealthCoverage.Comments;
                    txtCoverageCode.Text = HealthCoverage.HealthCoverageType.HealthCoverageCode;
                    if (HealthCoverage.IndexedAmount != null)
                    {
                        txtIndexedAmount.Text = ((decimal)HealthCoverage.IndexedAmount).ToString("0.##");
                    }
                    if (HealthCoverage.IndexedPremium != null)
                    {
                        txtIndexedPremium.Text = ((decimal)HealthCoverage.IndexedPremium).ToString("0.##");
                    }
                    if (HealthCoverage.InsuranceAmount != null)
                    {
                        txtInsuranceAmount.Text = ((decimal)HealthCoverage.InsuranceAmount).ToString("0.##");
                    }
                    if (HealthCoverage.MedicalAddition != null)
                    {
                        txtMedicalAddition.Text = ((decimal)HealthCoverage.MedicalAddition).ToString("0.##");
                    }
                    if (HealthCoverage.MonthlyPremuim != null)
                    {
                        txtMonthlyPremium.Text = ((decimal)HealthCoverage.MonthlyPremuim).ToString("0.##");
                    }
                    if (HealthCoverage.ProfessionalAddition != null)
                    {
                        txtProfessionalAddition.Text = ((decimal)HealthCoverage.ProfessionalAddition).ToString("0.##");
                    }
                    txtUpToAge.Text = HealthCoverage.Period.ToString();
                    dpEndDate.SelectedDate = HealthCoverage.EndDate;
                    dpStartDate.SelectedDate = HealthCoverage.StartDate;
                    if (HealthCoverage.Status == true)
                    {
                        cbStatus.SelectedIndex = 0;
                    }
                    else
                    {
                        cbStatus.SelectedIndex = 1;
                    }
                }
            }
            else if (personalAccidentsInsurance != null)
            {
                lblUpToAge.Content = "תקופה";
                if (PersonalAccidentsCoverage != null)
                {
                    var coverageTypes = cbCoverageName.Items;
                    foreach (var item in coverageTypes)
                    {
                        if (PersonalAccidentsCoverage.PersonalAccidentsCoverageTypeID == ((PersonalAccidentsCoverageType)item).PersonalAccidentsCoverageTypeID)
                        {
                            cbCoverageName.SelectedItem = item;
                            break;
                        }
                    }

                    txtComments.Text = PersonalAccidentsCoverage.Comments;
                    txtCoverageCode.Text = PersonalAccidentsCoverage.PersonalAccidentsCoverageType.PersonalAccidentsCoverageCode;
                    if (PersonalAccidentsCoverage.IndexedAmount != null)
                    {
                        txtIndexedAmount.Text = ((decimal)PersonalAccidentsCoverage.IndexedAmount).ToString("0.##");
                    }
                    if (PersonalAccidentsCoverage.IndexedPremium != null)
                    {
                        txtIndexedPremium.Text = ((decimal)PersonalAccidentsCoverage.IndexedPremium).ToString("0.##");
                    }
                    if (PersonalAccidentsCoverage.InsuranceAmount != null)
                    {
                        txtInsuranceAmount.Text = ((decimal)PersonalAccidentsCoverage.InsuranceAmount).ToString("0.##");
                    }
                    if (PersonalAccidentsCoverage.MedicalAddition != null)
                    {
                        txtMedicalAddition.Text = ((decimal)PersonalAccidentsCoverage.MedicalAddition).ToString("0.##");
                    }
                    if (PersonalAccidentsCoverage.MonthlyPremuim != null)
                    {
                        txtMonthlyPremium.Text = ((decimal)PersonalAccidentsCoverage.MonthlyPremuim).ToString("0.##");
                    }
                    if (PersonalAccidentsCoverage.ProfessionalAddition != null)
                    {
                        txtProfessionalAddition.Text = ((decimal)PersonalAccidentsCoverage.ProfessionalAddition).ToString("0.##");
                    }
                    txtUpToAge.Text = PersonalAccidentsCoverage.Period.ToString();
                    dpEndDate.SelectedDate = PersonalAccidentsCoverage.EndDate;
                    dpStartDate.SelectedDate = PersonalAccidentsCoverage.StartDate;
                    if (PersonalAccidentsCoverage.Status == true)
                    {
                        cbStatus.SelectedIndex = 0;
                    }
                    else
                    {
                        cbStatus.SelectedIndex = 1;
                    }
                }
            }
        }
        private void cbCoverageNameBinding()
        {
            if (healthInsuranceType == null && personalAccidentsInsurance == null)
            {
                cbCoverageName.ItemsSource = coverageLogic.GetAllActiveLifeCoverageTypesByIndustry((int)fundType.LifeIndustryID);
                cbCoverageName.DisplayMemberPath = "LifeCoverageTypeName";

            }
            else if (healthInsuranceType != null)
            {
                cbCoverageName.ItemsSource = coverageLogic.GetAllActiveHealthCoverageTypes();
                cbCoverageName.DisplayMemberPath = "HealthCoverageTypeName";
            }
            else if (personalAccidentsInsurance != null)
            {
                cbCoverageName.ItemsSource = coverageLogic.GetAllActivePersonalAccidentsCoverageTypes();
                cbCoverageName.DisplayMemberPath = "PersonalAccidentsCoverageTypeName";
            }

        }

        private void btnUpdateLifeCoverageTable_Click(object sender, RoutedEventArgs e)
        {
            int? selectedItemId = null;
            if (healthInsuranceType == null && personalAccidentsInsurance == null)
            {
                if (cbCoverageName.SelectedItem != null)
                {
                    selectedItemId = ((LifeCoverageType)cbCoverageName.SelectedItem).LifeCoverageTypeID;
                }
                LifeCoverageType coverageType = new LifeCoverageType() { LifeIndustryID = (int)fundType.LifeIndustryID };
                frmUpdateTable updateLifeCoverageTypesTable = new frmUpdateTable(coverageType);
                updateLifeCoverageTypesTable.ShowDialog();
                cbCoverageNameBinding();
                if (updateLifeCoverageTypesTable.ItemAdded != null)
                {
                    SelectItemInCb(((LifeCoverageType)updateLifeCoverageTypesTable.ItemAdded).LifeCoverageTypeID, coverageType);
                }
                else if (selectedItemId != null)
                {
                    SelectItemInCb((int)selectedItemId, coverageType);
                }
            }
            else if (healthInsuranceType != null)
            {
                if (cbCoverageName.SelectedItem != null)
                {
                    selectedItemId = ((HealthCoverageType)cbCoverageName.SelectedItem).HealthCoverageTypeID;
                }
                HealthCoverageType newCoverage = new HealthCoverageType();
                frmUpdateTable updateHealthCoverageTypesTable = new frmUpdateTable(newCoverage);
                updateHealthCoverageTypesTable.ShowDialog();
                cbCoverageNameBinding();
                if (updateHealthCoverageTypesTable.ItemAdded != null)
                {
                    SelectItemInCb(((HealthCoverageType)updateHealthCoverageTypesTable.ItemAdded).HealthCoverageTypeID, newCoverage);
                }
                else if (selectedItemId != null)
                {
                    SelectItemInCb((int)selectedItemId, newCoverage);
                }
            }
            else if (personalAccidentsInsurance != null)
            {
                if (cbCoverageName.SelectedItem != null)
                {
                    selectedItemId = ((PersonalAccidentsCoverageType)cbCoverageName.SelectedItem).PersonalAccidentsCoverageTypeID;
                }
                PersonalAccidentsCoverageType newCoverage = new PersonalAccidentsCoverageType();
                frmUpdateTable updatePersonalAccidentsCoverageTypesTable = new frmUpdateTable(newCoverage);
                updatePersonalAccidentsCoverageTypesTable.ShowDialog();
                cbCoverageNameBinding();
                if (updatePersonalAccidentsCoverageTypesTable.ItemAdded != null)
                {
                    SelectItemInCb(((PersonalAccidentsCoverageType)updatePersonalAccidentsCoverageTypesTable.ItemAdded).PersonalAccidentsCoverageTypeID, newCoverage);
                }
                else if (selectedItemId != null)
                {
                    SelectItemInCb((int)selectedItemId, newCoverage);
                }
            }
        }

        private void SelectItemInCb(int id, object table)
        {
            var items = cbCoverageName.Items;
            foreach (var item in items)
            {
                if (table is LifeCoverageType)
                {
                    LifeCoverageType parsedItem = (LifeCoverageType)item;
                    if (parsedItem.LifeCoverageTypeID == id)
                    {
                        cbCoverageName.SelectedItem = item;
                        break;
                    }
                }
                else if (table is HealthCoverageType)
                {
                    HealthCoverageType parsedItem = (HealthCoverageType)item;
                    if (parsedItem.HealthCoverageTypeID == id)
                    {
                        cbCoverageName.SelectedItem = item;
                        break;
                    }
                }
                else if (table is PersonalAccidentsCoverageType)
                {
                    PersonalAccidentsCoverageType parsedItem = (PersonalAccidentsCoverageType)item;
                    if (parsedItem.PersonalAccidentsCoverageTypeID == id)
                    {
                        cbCoverageName.SelectedItem = item;
                        break;
                    }
                }
            }
        }

        public void FillEmptyTextBoxes(TextBox[] inputs)
        {
            foreach (var item in inputs)
            {
                if (item.Text == "")
                {
                    item.Text = "0";
                }
            }
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            if (cbCoverageName.SelectedItem == null)
            {
                cancelClose = true;
                MessageBox.Show("סוג כיסוי הינו שדה חובה", "", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (dpEndDate.SelectedDate < dpStartDate.SelectedDate)
            {
                cancelClose = true;
                MessageBox.Show("תאריך סיום חייב להיות גדול מתאריך התחלה", "", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            FillEmptyTextBoxes(new TextBox[] { txtUpToAge, txtInsuranceAmount, txtIndexedAmount, txtMonthlyPremium, txtIndexedPremium, txtMedicalAddition, txtProfessionalAddition });
            bool? status = null;
            if (cbStatus.SelectedIndex == 0)
            {
                status = true;
            }
            else
            {
                status = false;
            }
            if (healthInsuranceType == null && personalAccidentsInsurance == null)
            {
                if (NewCoverage == null)
                {
                    NewCoverage = new LifeCoverage() { LifeCoverageType = (LifeCoverageType)cbCoverageName.SelectedItem, Comments = txtComments.Text, EndDate = dpEndDate.SelectedDate, InsuranceAmount = decimal.Parse(txtInsuranceAmount.Text), IndexedAmount = decimal.Parse(txtIndexedAmount.Text), LifeCoverageTypeID = ((LifeCoverageType)cbCoverageName.SelectedItem).LifeCoverageTypeID, MedicalAddition = decimal.Parse(txtMedicalAddition.Text), MonthlyPremuim = decimal.Parse(txtMonthlyPremium.Text), IndexedPremium = decimal.Parse(txtIndexedPremium.Text), ProfessionalAddition = decimal.Parse(txtProfessionalAddition.Text), StartDate = dpStartDate.SelectedDate, Status = status, UpToAge = int.Parse(txtUpToAge.Text) };
                }
                else
                {
                    NewCoverage.LifeCoverageType = (LifeCoverageType)cbCoverageName.SelectedItem;
                    NewCoverage.Comments = txtComments.Text;
                    NewCoverage.EndDate = dpEndDate.SelectedDate;
                    NewCoverage.InsuranceAmount = decimal.Parse(txtInsuranceAmount.Text);
                    NewCoverage.IndexedAmount = decimal.Parse(txtIndexedAmount.Text);
                    NewCoverage.LifeCoverageTypeID = ((LifeCoverageType)cbCoverageName.SelectedItem).LifeCoverageTypeID;
                    NewCoverage.MedicalAddition = decimal.Parse(txtMedicalAddition.Text);
                    NewCoverage.MonthlyPremuim = decimal.Parse(txtMonthlyPremium.Text);
                    NewCoverage.IndexedPremium = decimal.Parse(txtIndexedPremium.Text);
                    NewCoverage.ProfessionalAddition = decimal.Parse(txtProfessionalAddition.Text);
                    NewCoverage.StartDate = dpStartDate.SelectedDate;
                    NewCoverage.Status = status;
                    NewCoverage.UpToAge = int.Parse(txtUpToAge.Text);
                }
            }
            else if (healthInsuranceType != null)
            {
                if (HealthCoverage == null)
                {
                    HealthCoverage = new HealthCoverage() { HealthCoverageType = (HealthCoverageType)cbCoverageName.SelectedItem, Comments = txtComments.Text, EndDate = dpEndDate.SelectedDate, InsuranceAmount = decimal.Parse(txtInsuranceAmount.Text), IndexedAmount = decimal.Parse(txtIndexedAmount.Text), HealthCoverageTypeID = ((HealthCoverageType)cbCoverageName.SelectedItem).HealthCoverageTypeID, MedicalAddition = decimal.Parse(txtMedicalAddition.Text), MonthlyPremuim = decimal.Parse(txtMonthlyPremium.Text), IndexedPremium = decimal.Parse(txtIndexedPremium.Text), ProfessionalAddition = decimal.Parse(txtProfessionalAddition.Text), StartDate = dpStartDate.SelectedDate, Status = status, Period = int.Parse(txtUpToAge.Text) };

                }
                else
                {
                    HealthCoverage.HealthCoverageType = (HealthCoverageType)cbCoverageName.SelectedItem;
                    HealthCoverage.Comments = txtComments.Text;
                    HealthCoverage.EndDate = dpEndDate.SelectedDate;
                    HealthCoverage.InsuranceAmount = decimal.Parse(txtInsuranceAmount.Text);
                    HealthCoverage.IndexedAmount = decimal.Parse(txtIndexedAmount.Text);
                    HealthCoverage.HealthCoverageTypeID = ((HealthCoverageType)cbCoverageName.SelectedItem).HealthCoverageTypeID;
                    HealthCoverage.MedicalAddition = decimal.Parse(txtMedicalAddition.Text);
                    HealthCoverage.MonthlyPremuim = decimal.Parse(txtMonthlyPremium.Text);
                    HealthCoverage.IndexedPremium = decimal.Parse(txtIndexedPremium.Text);
                    HealthCoverage.ProfessionalAddition = decimal.Parse(txtProfessionalAddition.Text);
                    HealthCoverage.StartDate = dpStartDate.SelectedDate;
                    HealthCoverage.Status = status;
                    HealthCoverage.Period = int.Parse(txtUpToAge.Text);
                }
            }
            else if (personalAccidentsInsurance != null)
            {
                if (PersonalAccidentsCoverage == null)
                {
                    PersonalAccidentsCoverage = new PersonalAccidentsCoverage() { PersonalAccidentsCoverageType = (PersonalAccidentsCoverageType)cbCoverageName.SelectedItem, Comments = txtComments.Text, EndDate = dpEndDate.SelectedDate, InsuranceAmount = decimal.Parse(txtInsuranceAmount.Text), IndexedAmount = decimal.Parse(txtIndexedAmount.Text), PersonalAccidentsCoverageTypeID = ((PersonalAccidentsCoverageType)cbCoverageName.SelectedItem).PersonalAccidentsCoverageTypeID, MedicalAddition = decimal.Parse(txtMedicalAddition.Text), MonthlyPremuim = decimal.Parse(txtMonthlyPremium.Text), IndexedPremium = decimal.Parse(txtIndexedPremium.Text), ProfessionalAddition = decimal.Parse(txtProfessionalAddition.Text), StartDate = dpStartDate.SelectedDate, Status = status, Period = int.Parse(txtUpToAge.Text) };
                }
                else
                {
                    PersonalAccidentsCoverage.PersonalAccidentsCoverageType = (PersonalAccidentsCoverageType)cbCoverageName.SelectedItem;
                    PersonalAccidentsCoverage.Comments = txtComments.Text;
                    PersonalAccidentsCoverage.EndDate = dpEndDate.SelectedDate;
                    PersonalAccidentsCoverage.InsuranceAmount = decimal.Parse(txtInsuranceAmount.Text);
                    PersonalAccidentsCoverage.IndexedAmount = decimal.Parse(txtIndexedAmount.Text);
                    PersonalAccidentsCoverage.PersonalAccidentsCoverageTypeID = ((PersonalAccidentsCoverageType)cbCoverageName.SelectedItem).PersonalAccidentsCoverageTypeID;
                    PersonalAccidentsCoverage.MedicalAddition = decimal.Parse(txtMedicalAddition.Text);
                    PersonalAccidentsCoverage.MonthlyPremuim = decimal.Parse(txtMonthlyPremium.Text);
                    PersonalAccidentsCoverage.IndexedPremium = decimal.Parse(txtIndexedPremium.Text);
                    PersonalAccidentsCoverage.ProfessionalAddition = decimal.Parse(txtProfessionalAddition.Text);
                    PersonalAccidentsCoverage.StartDate = dpStartDate.SelectedDate;
                    PersonalAccidentsCoverage.Status = status;
                    PersonalAccidentsCoverage.Period = int.Parse(txtUpToAge.Text);
                }
            }
            if (sender is Button)
            {
                Button btn = (Button)sender;
                if (btn.Name == "btnSave")
                {
                    confirmBeforeClosing = false;
                    Close();
                }
            }
        }

        private void txtUpToAge_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (sender is TextBox)
            {
                TextBox txtBox = (TextBox)sender;
                txtBox.ClearValue(BorderBrushProperty);
                if (txtBox.Text != "")
                {
                    try
                    {
                        int reusult = validations.ConvertStringToInt(txtBox.Text);
                    }
                    catch (Exception ex)
                    {
                        txtBox.BorderBrush = System.Windows.Media.Brushes.Red;
                        System.Windows.Forms.MessageBox.Show(ex.Message);
                        txtBox.Text = "";
                    }
                }
            }

        }

        private void txtInsuranceAmount_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (sender is TextBox)
            {
                TextBox txtBox = (TextBox)sender;
                txtBox.ClearValue(BorderBrushProperty);
                if (txtBox.Text != "")
                {
                    try
                    {
                        decimal reusult = validations.ConvertStringToDecimal(txtBox.Text);
                    }
                    catch (Exception ex)
                    {
                        txtBox.BorderBrush = System.Windows.Media.Brushes.Red;
                        System.Windows.Forms.MessageBox.Show(ex.Message);
                        txtBox.Text = "";
                    }
                }
            }
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void GroupBox_GotFocus(object sender, RoutedEventArgs e)
        {
            isChanges = true;
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (confirmBeforeClosing && isChanges)
            {
                if (MessageBox.Show("האם ברצונך לשמור שינוים שבצעת", "אישור", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    btnSave_Click(sender, new RoutedEventArgs());
                    if (cancelClose)
                    {
                        e.Cancel = true;
                        cancelClose = false;
                    }
                }
            }
        }
    }
}
