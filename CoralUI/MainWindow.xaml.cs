﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using CoralBusinessLogics;
using System.Globalization;
using System.IO;

namespace CoralUI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        UserLogic logics = new UserLogic();
        ClientsLogic c = new ClientsLogic();
        PoliciesLogic p = new PoliciesLogic();
        WorkTasksLogic workTasksLogic = new WorkTasksLogic();
        private static System.Timers.Timer oneDayTimer;
        List<Request> oneDayReminders;
        User user;
        RenewalsLogic renewalsLogic = new RenewalsLogic();
        public MainWindow()
        {
            InitializeComponent();
            this.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            txtUsername.Focus();
            //מגדיר את שפת המקלדת לעברית
            InputLanguageManager.Current.CurrentInputLanguage = new CultureInfo("he-IL");

#if DEBUG
            frmClients window = new frmClients("admin");
            this.Close();
            window.Show();
#endif
        }

        //סוגר את התוכנה
        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        //בודק האם משתמש קיים 
        private void btnNext_Click(object sender, RoutedEventArgs e)
        {
            bool isUserExist = logics.IsUserExist(txtUsername.Text, txtPassword.Password);
            bool isUserActive = logics.IsUserActive(txtUsername.Text);
            //אם המשתמש קיים ופעיל- נפתח חלון הלקוחות
            if (isUserExist && isUserActive)
            {
                user = logics.FindUserByUsername(txtUsername.Text);
                Reminders remindersClass = new Reminders(user);
                remindersClass.CreateOnceADayTimer();
                // Create a timer with a two second interval.
                //oneDayTimer = new System.Timers.Timer(86399999);

                //// Hook up the Elapsed event for the timer. 
                //oneDayTimer.Elapsed += OnTimedEvent;

                //// Have the timer fire repeated events (true is the default)
                //oneDayTimer.AutoReset = true;

                //// Start the timer
                //oneDayTimer.Enabled = true;
                remindersClass.SetRemindersForNext24Hours();
                int nonSeenRemindersCount = remindersClass.RaiseNonSeenReminders();
                //foreach (var item in oneDayReminders)
                //{
                //    MessageBox.Show(((DateTime)item.ReminderDate).Date.ToString() + " " + item.ReminderHour.ToString(), "", MessageBoxButton.OK, MessageBoxImage.Error);
                //}
                frmClients window = new frmClients(txtUsername.Text);
                this.Close();
                window.Show();
                if (nonSeenRemindersCount > 0)
                {
                    System.Windows.Forms.MessageBox.Show("שים לב: תזכורות חדשות במערכת");
                }
                renewalsLogic.InsertRenewalStatus("בהפקה", true);
            }

            //אם המשתמש לא קיים או הוא לא פעיל- זורק הודעת שגיאה ומבקש שוב את פירטי המשתמש
            else
            {
                if (!isUserExist)
                {
                    MessageBox.Show("שם משתמש ו/או סיסמה שגויים", "שגיאה", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                else if (!isUserActive)
                {
                    MessageBox.Show("לא ניתן להתחבר, משתמש לא פעיל", "שגיאה", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        private void linkNewAccount_Click(object sender, RoutedEventArgs e)
        {
            frmUsersManager usersWindow = new frmUsersManager();
            usersWindow.ShowDialog();
        }


        //private void OnTimedEvent(Object source, System.Timers.ElapsedEventArgs e)
        //{            
        //    SetRemindersForNext24Hours();           
        //}

        //private void SetRemindersForNext24Hours()
        //{
        //    var reminders= workTasksLogic.Get24HoursReminders(user);
        //    List<System.Timers.Timer> timers = new List<System.Timers.Timer>();
        //    foreach (var item in reminders)
        //    {
        //        System.Timers.Timer reminderTimer = new System.Timers.Timer();
        //        DateTime reminderTime = Convert.ToDateTime(item.ReminderHour.ToString());
        //        DateTime reminderDateTime = new DateTime(((DateTime)item.ReminderDate).Year, ((DateTime)item.ReminderDate).Month, ((DateTime)item.ReminderDate).Day, reminderTime.Hour, reminderTime.Minute, reminderTime.Second, reminderTime.Millisecond);
        //        TimeSpan span = reminderDateTime - DateTime.Now.AddSeconds(-10);                
        //        reminderTimer.Interval = span.TotalMilliseconds;
        //        // Hook up the Elapsed event for the timer. 
        //        reminderTimer.Elapsed += OnReminderEvent;

        //        // Have the timer fire repeated events (true is the default)
        //        reminderTimer.AutoReset = false;

        //        // Start the timer
        //        reminderTimer.Enabled = true;
        //        timers.Add(reminderTimer);
        //    }
        //}

        //private void OnReminderEvent(object sender, System.Timers.ElapsedEventArgs e)
        //{
        //    MessageBox.Show(e.SignalTime.ToString(), "", MessageBoxButton.OK, MessageBoxImage.Error);
        //    //MessageBox.Show(((DateTime)item.ReminderDate).Date.ToString() + " " + item.ReminderHour.ToString(), "", MessageBoxButton.OK, MessageBoxImage.Error);
        //}
    }
}
