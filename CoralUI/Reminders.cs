﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CoralBusinessLogics;
using System.Timers;
using System.Threading;
using System.Windows.Threading;
using System.Windows;

namespace CoralUI
{
    public class Reminders
    {

        WorkTasksLogic workTaskLogic = new WorkTasksLogic();
        public static List<System.Timers.Timer> Timers { get; set; }
        public static List<Request> ReminderRequests { get; set; }
        public static List<Request> NotSeenReminderRequests { get; set; }
        public User User { get; set; }
        public static System.Timers.Timer OneDayTimer { get; set; }

        public Reminders(User userAccount)
        {
            User = userAccount;
        }


        public void CreateOnceADayTimer()
        {
            OneDayTimer = new System.Timers.Timer(86399999);

            // Hook up the Elapsed event for the timer. 
            OneDayTimer.Elapsed += OnTimedEvent;

            // Have the timer fire repeated events (true is the default)
            OneDayTimer.AutoReset = true;

            // Start the timer
            OneDayTimer.Enabled = true;
        }

        public void OnTimedEvent(Object source, System.Timers.ElapsedEventArgs e)
        {
            SetRemindersForNext24Hours();
        }

        public void SetRemindersForNext24Hours()
        {
            ReminderRequests = workTaskLogic.Get24HoursReminders(User);
            if (Timers==null)
            {
                Timers = new List<System.Timers.Timer>();                
            }
            else
            {
                Timers.Clear();
            }
            foreach (var item in ReminderRequests)
            {
                System.Timers.Timer reminderTimer = SetTimer(item);
                Timers.Add(reminderTimer);
            }
        }

        private System.Timers.Timer SetTimer(Request item)
        {
            System.Timers.Timer reminderTimer = new System.Timers.Timer();
            DateTime reminderTime = Convert.ToDateTime(item.ReminderHour.ToString());
            DateTime reminderDateTime = new DateTime(((DateTime)item.ReminderDate).Year, ((DateTime)item.ReminderDate).Month, ((DateTime)item.ReminderDate).Day, reminderTime.Hour, reminderTime.Minute, reminderTime.Second, reminderTime.Millisecond);
            TimeSpan span = reminderDateTime - DateTime.Now.AddSeconds(-1);
            reminderTimer.Interval = span.TotalMilliseconds;
            // Hook up the Elapsed event for the timer. 
            //reminderTimer.Elapsed += OnReminderEvent;
            reminderTimer.Elapsed += (sender, e) => OnReminderEvent(sender, e, item);
            // Have the timer fire repeated events (true is the default)
            reminderTimer.AutoReset = false;
            // Start the timer
            reminderTimer.Enabled = true;
            return reminderTimer;
        }

        public int RaiseNonSeenReminders()
        {
            NotSeenReminderRequests = workTaskLogic.GetNotSeenReminders(User);
            foreach (var item in NotSeenReminderRequests)
            {
                OpenWindow(item,true);
            }
            return NotSeenReminderRequests.Count;
        }

        public void OnReminderEvent(object sender, System.Timers.ElapsedEventArgs e, Request request)
        {
            Application.Current.Dispatcher.Invoke(new Action(() => OpenWindow(request,false)));
        }

        private void OpenWindow(Request request, bool isOldReminder)
        {
            frmReminder reminderWindow = new frmReminder(request,User,isOldReminder);
            reminderWindow.Show();
        }

        public void ResetReminderTimer(Request request, bool? isNonSeenReminder, DateTime reminderDate, TimeSpan reminderTime)
        {
            if (isNonSeenReminder==false)
            {
                int reminderIndex = ReminderRequests.FindIndex(r => r.RequestID == request.RequestID);
                ReminderRequests[reminderIndex].ReminderDate = reminderDate;
                ReminderRequests[reminderIndex].ReminderHour = reminderTime;
                DateTime reminderResetTime = Convert.ToDateTime(reminderTime.ToString());
                DateTime reminderDateTime = new DateTime(reminderDate.Year, reminderDate.Month, reminderDate.Day, reminderResetTime.Hour, reminderResetTime.Minute, reminderResetTime.Second, reminderResetTime.Millisecond);
                TimeSpan span = reminderDateTime - DateTime.Now.AddSeconds(-1);
                Timers[reminderIndex].Interval = span.TotalMilliseconds;                
            }
            else
            {
                ReminderRequests.Add(request);
                Timers.Add(SetTimer(request));                
            }
        }

        public void UpdateReminderTimer(int requestID, DateTime reminderDate, TimeSpan reminderTime)
        {
            DateTime reminderResetTime = Convert.ToDateTime(reminderTime.ToString());
            DateTime reminderDateTime = new DateTime(reminderDate.Year, reminderDate.Month, reminderDate.Day, reminderResetTime.Hour, reminderResetTime.Minute, reminderResetTime.Second, reminderResetTime.Millisecond);
            TimeSpan span = reminderDateTime - DateTime.Now;
            if (OneDayTimer.Interval- span.TotalMilliseconds>0)
            {
                int reminderIndex = ReminderRequests.FindIndex(r => r.RequestID == requestID);
                if (reminderIndex!=-1)
                {
                    ReminderRequests[reminderIndex].ReminderDate = reminderDate;
                    ReminderRequests[reminderIndex].ReminderHour = reminderTime;
                    Timers[reminderIndex].Interval = span.TotalMilliseconds;    
                }
                else
                {
                    Request request = workTaskLogic.GetRequestById(requestID);
                    ReminderRequests.Add(request);
                    Timers.Add(SetTimer(request));   
                }
            }
        }

        public void AddReminderTimer(int requestID, DateTime reminderDate, TimeSpan reminderTime)
        {
            DateTime reminderResetTime = Convert.ToDateTime(reminderTime.ToString());
            DateTime reminderDateTime = new DateTime(reminderDate.Year, reminderDate.Month, reminderDate.Day, reminderResetTime.Hour, reminderResetTime.Minute, reminderResetTime.Second, reminderResetTime.Millisecond);
            TimeSpan span = reminderDateTime - DateTime.Now;
            if (OneDayTimer.Interval - span.TotalMilliseconds > 0)
            {
                Request request=workTaskLogic.GetRequestById(requestID);
                ReminderRequests.Add(request);
                Timers.Add(SetTimer(request));        
            }
        }
    }
}
