﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using CoralBusinessLogics;
using System.Data;

namespace CoralUI
{
    /// <summary>
    /// Interaction logic for frmUsersManager.xaml
    /// </summary>
    public partial class frmUsersManager : Window
    {
        RolesLogic rolesLogic = new RolesLogic();
        UserLogic userLogic = new UserLogic();
        string selectedUsername = null;
        private bool isChanges=false;
        private bool cancelClose=false;

        public frmUsersManager()
        {
            InitializeComponent();
            chbWorker.IsChecked = true;
        }
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            DataGridBinding(false);
        }

        //מרנן את טבלת המשתמשים
        private void DataGridBinding(bool isSearch)
        {
            if (!isSearch)
            {
                dgUsers.ItemsSource = userLogic.GetAllUsers();
            }
            else
            {
                dgUsers.ItemsSource = userLogic.FindUsersByString(txtSearchUser.Text);
            }
            dgColUsername.DisplayMemberBinding = new Binding("Usermame");
            //dgColStatus.Binding = new Binding("Status");
        }

        //בודק האם שדות החובה אינם ריקים
        public bool InputNullValidation()
        {
            bool isValid = true;
            
            if (txtUsername.Text == "")
            {
                txtUsername.BorderBrush = Brushes.Red;   
                isValid = false;
            }
            if (txtPassword.Password == "")
            {
                txtPassword.BorderBrush = Brushes.Red;   
                isValid = false;
            }
            if (txtPasswordConfirmation.Password == "")
            {
                txtPasswordConfirmation.BorderBrush = Brushes.Red;
                isValid = false;
            }
            return isValid;
        }

        //מוסיף/מעדכן לקוח לאחר בדיקה של כמה תנאים
        private void btnAddUser_Click(object sender, RoutedEventArgs e)
        {
            txtUsername.ClearValue(BorderBrushProperty);
            txtPassword.ClearValue(BorderBrushProperty);
            txtPasswordConfirmation.ClearValue(BorderBrushProperty);
            //תנאי 1: שם משתמש וסיסמה הינם שדות חובה
            if (InputNullValidation()==false)
            {
                cancelClose = true;
                return;
            }
            //תנאי 2: אורך מינימאלי של שם משתמש הינו 3 תווים
            if (txtUsername.Text.Length < 3)
            {
                MessageBox.Show("אורך שם משתמש מינימאלי הינו 3 תווים", "שגיאה", MessageBoxButton.OK, MessageBoxImage.Error);
                txtUsername.Clear();
                txtPassword.Clear();
                txtPasswordConfirmation.Clear();
                cancelClose = true;
                return;
            }
            //תנאי 3: אורך מינימאלי של סיסמה הינו 4 תווים 
            if (txtPassword.Password.Length < 4 && txtPassword.Password != "")
            {
                MessageBox.Show("אורך סיסמה מינימאלי הינו 4 תווים", "שגיאה", MessageBoxButton.OK, MessageBoxImage.Error);
                txtPassword.Clear();
                txtPasswordConfirmation.Clear();
                cancelClose = true;
                return;
            }

            //תנאי 5: סיסמה ואישור סיסמה חייבים להיות זהים
            if (txtPassword.Password != txtPasswordConfirmation.Password)
            {
                MessageBox.Show("סיסמה ואישור סיסמה לא מתאימים", "שגיאה", MessageBoxButton.OK, MessageBoxImage.Error);
                txtPassword.Clear();
                txtPasswordConfirmation.Clear();
                cancelClose = true;
                return;
            }
            //בדיקת סטטוס המשתמש שנבחר
            bool status = true;
            if (cbStatus.SelectedIndex == 1)
            {
                status = false;
            }
            //בודק האם המשתמש רוצה להוסיף או לעדכן לקוח עפ"י טקסט הכפתור
            if (txtBtnAddUser.Text != "עדכן משתמש")
            {
                //הוספת משתמש חדש
                try
                {
                    userLogic.InsertUser(txtUsername.Text, txtPassword.Password, status, chbCalendar.IsChecked, chbClientProfile.IsChecked, chbCreateClient.IsChecked, chbElementary.IsChecked, chbFinance.IsChecked, chbForeingWorkers.IsChecked, chbHealth.IsChecked, chbLife.IsChecked, chbOffers.IsChecked, chbPersonalAccidents.IsChecked, chbRenwals.IsChecked, chbReports.IsChecked, chbScan.IsChecked, chbTasks.IsChecked, chbTravel.IsChecked);
                }
                //תופס שגיאות שנוצרו בניסיון להוסיף את המשתמש
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    CleanGroupBox();
                    return;
                }
                //בודק אלו תפקידים נבחרו למשתמש החדש ומוסיף אותם
                List<string> roles = new List<string>();
                if (chbAdmin.IsChecked == true)
                {
                    roles.Add(chbAdmin.Content.ToString());
                }
                if (chbManager.IsChecked == true)
                {
                    roles.Add(chbManager.Content.ToString());
                }
                if (chbWorker.IsChecked == true)
                {
                    roles.Add(chbWorker.Content.ToString());
                }
                List<string> users = new List<string>();
                users.Add(txtUsername.Text);
                rolesLogic.AddUsersToRoles(users, roles);
            }
            else
            {
                //עדכון משתמש קיים ותפדיו
                try
                {
                    userLogic.UpdateUser(selectedUsername, txtPassword.Password, status, chbCalendar.IsChecked, chbClientProfile.IsChecked, chbCreateClient.IsChecked, chbElementary.IsChecked, chbFinance.IsChecked, chbForeingWorkers.IsChecked, chbHealth.IsChecked, chbLife.IsChecked, chbOffers.IsChecked, chbPersonalAccidents.IsChecked, chbRenwals.IsChecked, chbReports.IsChecked, chbScan.IsChecked, chbTasks.IsChecked, chbTravel.IsChecked);
                    List<string> rolesToRemove = new List<string>();
                    List<string> rolesToAdd = new List<string>();
                    List<string> users = new List<string>();
                    users.Add(txtUsername.Text);
                    if (rolesLogic.IsUserInRole(txtUsername.Text, chbAdmin.Content.ToString()) && chbAdmin.IsChecked == false)
                    {
                        rolesToRemove.Add(chbAdmin.Content.ToString());
                    }
                    if (!rolesLogic.IsUserInRole(txtUsername.Text, chbAdmin.Content.ToString()) && chbAdmin.IsChecked == true)
                    {
                        rolesToAdd.Add(chbAdmin.Content.ToString());
                    }
                    if (rolesLogic.IsUserInRole(txtUsername.Text, chbManager.Content.ToString()) && chbManager.IsChecked == false)
                    {
                        rolesToRemove.Add(chbManager.Content.ToString());
                    }
                    if (!rolesLogic.IsUserInRole(txtUsername.Text, chbManager.Content.ToString()) && chbManager.IsChecked == true)
                    {
                        rolesToAdd.Add(chbManager.Content.ToString());
                    }
                    if (rolesLogic.IsUserInRole(txtUsername.Text, chbWorker.Content.ToString()) && chbWorker.IsChecked == false)
                    {
                        rolesToRemove.Add(chbWorker.Content.ToString());
                    }
                    if (!rolesLogic.IsUserInRole(txtUsername.Text, chbWorker.Content.ToString()) && chbWorker.IsChecked == true)
                    {
                        rolesToAdd.Add(chbWorker.Content.ToString());
                    }
                    if (rolesToRemove.Count > 0)
                    {
                        rolesLogic.RemoveUsersFromRoles(users.ToArray(), rolesToRemove.ToArray());
                    }
                    if (rolesToAdd.Count > 0)
                    {
                        rolesLogic.AddUsersToRoles(users, rolesToAdd);
                    }
                    dgUsers.UnselectAll();

                }
                //תופס שגיאות בניסיון לעדכן משתמש
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    CleanGroupBox();
                    return;
                }
            }
            DataGridBinding(false);
            CleanGroupBox();
            isChanges = false;
        }

        //ממלא או מנקה את פירטי המשתמש לפי המשתמש הנבחר בטבלת המשתמשים
        private void dgUsers_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (dgUsers.SelectedItem == null)
            //מנקה את פירטי המשתמש כאין משתמש מסומן בטבלה
            {
                CleanGroupBox();
                return;
            }
            //ממלא את פירטי המשתמש לפי המשתמש המסומן
            txtPasswordConfirmation.Clear();
            chbAdmin.IsChecked = false;
            chbManager.IsChecked = false;
            chbWorker.IsChecked = false;
            txtBtnAddUser.Text = "עדכן משתמש";
            var updateUriSource = new Uri(@"/IconsMind/Add_24px.png", UriKind.Relative);
            imgBtnAddUser.Source = new BitmapImage(updateUriSource);
            User userSelected = (User)dgUsers.SelectedItem;
            selectedUsername = userSelected.Usermame;
            txtUsername.Text = userSelected.Usermame;
            txtUsername.IsEnabled = false;
            txtPassword.Password = userSelected.Password;
            if (userSelected.Status == true)
            {
                cbStatus.SelectedIndex = 0;
            }
            else
            {
                cbStatus.SelectedIndex = 1;
            }
            if (rolesLogic.IsUserInRole(userSelected.Usermame, chbAdmin.Content.ToString()))
            {
                chbAdmin.IsChecked = true;
            }
            if (rolesLogic.IsUserInRole(userSelected.Usermame, chbManager.Content.ToString()))
            {
                chbManager.IsChecked = true;
            }
            if (rolesLogic.IsUserInRole(userSelected.Usermame, chbWorker.Content.ToString()))
            {
                chbWorker.IsChecked = true;
            }
            chbCalendar.IsChecked = userSelected.IsCalendarPermission;
            chbClientProfile.IsChecked = userSelected.IsClientProfilePermission;
            chbCreateClient.IsChecked = userSelected.IsCreateClientPermission;
            chbElementary.IsChecked = userSelected.IsElementaryPermission;
            chbFinance.IsChecked = userSelected.IsFinancePermission;
            chbForeingWorkers.IsChecked = userSelected.IsForeingWorkersPermission;
            chbHealth.IsChecked = userSelected.IsHealthPermission;
            chbLife.IsChecked = userSelected.IsLifePermission;
            chbOffers.IsChecked = userSelected.IsOffersPermission;
            chbPersonalAccidents.IsChecked = userSelected.IsPersonalAccidentsPermission;
            chbRenwals.IsChecked = userSelected.IsRenewalsPermission;
            chbReports.IsChecked = userSelected.IsReportsPermission;
            chbScan.IsChecked = userSelected.IsScanPermission;
            chbTasks.IsChecked = userSelected.IsTasksPermission;
            chbTravel.IsChecked = userSelected.IsTravelPermission;
        }

        //מנקה את טבלת המשתמשים מכל סימון
        private void dgUsers_MouseDown(object sender, MouseButtonEventArgs e)
        {
            dgUsers.UnselectAll();
        }

       
        //מנקה את טבלת המשתמשים מכל סימון כאשר לוחצים בתיבת הטקסט של חיפוש משתמש
        private void txtSearchUser_GotFocus(object sender, RoutedEventArgs e)
        {
            dgUsers.UnselectAll();
        }

        //מנקה את פירטי המשתמש
        private void CleanGroupBox()
        {
            txtUsername.IsEnabled = true;
            txtBtnAddUser.Text = "הוסף משתמש";
            var addUriSource = new Uri(@"/IconsMind/Edit_24px.png", UriKind.Relative);
            imgBtnAddUser.Source = new BitmapImage(addUriSource);
            txtUsername.Clear();
            txtPassword.Clear();
            txtPasswordConfirmation.Clear();
            chbWorker.IsChecked = true;
            chbAdmin.IsChecked = false;
            chbManager.IsChecked = false;
            cbStatus.SelectedIndex = 0;
            chbCalendar.IsChecked = true;
            chbClientProfile.IsChecked = true;
            chbCreateClient.IsChecked = true;
            chbElementary.IsChecked = true;
            chbFinance.IsChecked = true;
            chbForeingWorkers.IsChecked = true;
            chbHealth.IsChecked = true;
            chbLife.IsChecked = true;
            chbOffers.IsChecked = true;
            chbPersonalAccidents.IsChecked = true;
            chbRenwals.IsChecked = true;
            chbReports.IsChecked = true;
            chbScan.IsChecked = true;
            chbTasks.IsChecked = true;
            chbTravel.IsChecked = true;
        }

        //סוגר את החלון
        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        //מסנן תוצאות חיפוש תוך כדי הקלדה
        private void txtSearchUser_TextChanged(object sender, TextChangedEventArgs e)
        {
            DataGridBinding(true);
        }

        private void chbAdmin_GotFocus(object sender, RoutedEventArgs e)
        {
            isChanges = true;
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (isChanges)
            {
                if (MessageBox.Show("האם ברצונך לשמור שינוים שבצעת", "אישור", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    btnAddUser_Click(sender, new RoutedEventArgs());
                    if (cancelClose)
                    {
                        e.Cancel = true;
                        cancelClose = false;
                    }
                }
            }
        }
    }
}
