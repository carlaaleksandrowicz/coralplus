﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using CoralBusinessLogics;


namespace CoralUI
{
    /// <summary>
    /// Interaction logic for frmClientsAdvanceSearch.xaml
    /// </summary>
    public partial class frmClientsAdvanceSearch : Window
    {
        ClientsLogic clientsLogic = new ClientsLogic();
        AgentsLogic agentLogic = new AgentsLogic();
        public List<Client> searchResult { get; set; }
        public ClientType clientType { get; set; }
        public string PolicyNumber { get; set; }
        public string CarNumber { get; set; }
        TabItem tab;

        public frmClientsAdvanceSearch(TabItem tabSelected)
        {
            InitializeComponent();
            tab = tabSelected;
        }

        //טעינת החלון
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            ClientTypeCbBinding();
            var types = cbClientType.Items;
            foreach (var item in types)
            {
                ClientType type = (ClientType)item;
                if (type.ClientTypeName == tab.Header.ToString())
                {
                    cbClientType.SelectedItem = item;
                    break;
                }
            }
            CategoryCbBinding();
            CbAgentBinding();
        }

        //מסנכרן את קומבו בוקס של סוכנים עם בסיס הנתונים
        private void CbAgentBinding()
        {
            cbPrincipalAgent.ItemsSource = agentLogic.GetAllAgents().Where(a=>a.Status==true);
        }

        //מסנכרן את קומבו בוקס של קטגוריות עם בסיס הנתונים
        private void CategoryCbBinding()
        {
            cbCategory.ItemsSource = clientsLogic.GetActiveCategories();
            cbCategory.DisplayMemberPath = "CategoryName";
        }

        //מסנכרן את קומבו בוקס של סוג לקוח עם בסיס הנתונים
        private void ClientTypeCbBinding()
        {
            cbClientType.ItemsSource = clientsLogic.GetAllTypes();
            cbClientType.DisplayMemberPath = "ClientTypeName";
        }

        //שולח את פרטי החיפוש לחיפוש 
        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            if (chbSearchByPolicyDetails.IsChecked != true)
            {
                clientType = (ClientType)cbClientType.SelectedItem;
                //חיפוש עפ"י תיאום של כל השדות
                if (rbAllCellsMatch.IsChecked == true)
                {
                    searchResult = clientsLogic.AdvanceSearchAllCellsMatch(clientType, (Category)cbCategory.SelectedItem, dpOpenDate.SelectedDate, txtClientNumber.Text, (Agent)cbPrincipalAgent.SelectedItem, txtLastName.Text, txtFirstName.Text, txtId.Text, txtCity.Text, txtStreet.Text, txtHomePhone.Text, txtCellPhone.Text, txtWorkPhone.Text, txtEmail.Text, txtCompanyName.Text);
                }
                //חיפוש עפ"י תיאום של לפחות אחד מהשדות
                else
                {
                    searchResult = clientsLogic.AdvanceSearchOneCellMatch(clientType, (Category)cbCategory.SelectedItem, dpOpenDate.SelectedDate, txtClientNumber.Text, (Agent)cbPrincipalAgent.SelectedItem, txtLastName.Text, txtFirstName.Text, txtId.Text, txtCity.Text, txtStreet.Text, txtPolicyNumber.Text, txtCarNumber.Text, txtHomePhone.Text, txtCellPhone.Text, txtWorkPhone.Text, txtEmail.Text, txtCompanyName.Text);
                }
                if (searchResult == null || searchResult.Count == 0)
                { 
                    MessageBox.Show("לא נמצאו תוצאות עבור חיפוש זה");
                }
            }
            else
            {
                if (rbPolicyNumber.IsChecked == true)
                {
                    searchResult = clientsLogic.FindClientByPolicyNumber(txtPolicyNumber.Text);
                    if (searchResult != null&&searchResult.Count>0)
                    {
                        PolicyNumber = txtPolicyNumber.Text;
                    }
                }
                else if (rbCarNumber.IsChecked == true)
                {
                    searchResult = clientsLogic.FindClientByCarNumber(txtCarNumber.Text);
                    if (searchResult != null && searchResult.Count > 0)
                    {
                        CarNumber = txtCarNumber.Text;
                    }
                }
                if (searchResult != null && searchResult.Count > 0)
                {
                    clientType = searchResult.FirstOrDefault().ClientType;                    
                }
                else
                {
                    MessageBox.Show("לא נמצאו תוצאות עבור חיפוש זה");
                }
            }
            Close();
        }

        //סוגר את החלון
        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void CheckBox_Checked(object sender, RoutedEventArgs e)
        {
            rbPolicyNumber.IsEnabled = true;
            rbPolicyNumber.IsChecked = true;
            rbCarNumber.IsEnabled = true;
            spClientDetails.IsEnabled = false;
        }

        private void CheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            rbPolicyNumber.IsEnabled = false;
            rbCarNumber.IsEnabled = false;
            spClientDetails.IsEnabled = true;
        }

        private void rbPolicyNumber_Checked(object sender, RoutedEventArgs e)
        {
            txtPolicyNumber.IsEnabled = true;
            txtPolicyNumber.Focus();
            txtCarNumber.IsEnabled = false;
        }

        private void rbCarNumber_Checked(object sender, RoutedEventArgs e)
        {
            txtPolicyNumber.IsEnabled = false;
            txtCarNumber.IsEnabled = true;
            txtCarNumber.Focus();
        }
    }
}
