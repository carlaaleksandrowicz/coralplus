﻿CREATE TABLE [dbo].[HealthCoverages] (
    [HealthCoverageID]     INT            IDENTITY (1, 1) NOT NULL,
    [HealthPolicyID]       INT            NOT NULL,
    [HealthCoverageTypeID] INT            NOT NULL,
    [Period]               INT            NULL,
    [InsuranceAmount]      MONEY          NULL,
    [IndexedAmount]        MONEY          NULL,
    [MonthlyPremuim]       MONEY          NULL,
    [IndexedPremium]       MONEY          NULL,
    [StartDate]            DATE           NULL,
    [EndDate]              DATE           NULL,
    [Status]               BIT            NULL,
    [MedicalAddition]      DECIMAL (18)   NULL,
    [ProfessionalAddition] DECIMAL (18)   NULL,
    [Comments]             NVARCHAR (MAX) NULL,
    [Addition]             INT            NULL,
    CONSTRAINT [PK_HealthCoverages] PRIMARY KEY CLUSTERED ([HealthCoverageID] ASC),
    CONSTRAINT [FK_HealthCoverages_HealthCoverageTypes] FOREIGN KEY ([HealthCoverageTypeID]) REFERENCES [dbo].[HealthCoverageTypes] ([HealthCoverageTypeID]),
    CONSTRAINT [FK_HealthCoverages_HealthPolicies] FOREIGN KEY ([HealthPolicyID]) REFERENCES [dbo].[HealthPolicies] ([HealthPolicyID])
);

