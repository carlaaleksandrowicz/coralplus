﻿CREATE TABLE [dbo].[CarPolicies] (
    [ElementaryPolicyID]        INT            NOT NULL,
    [RegistrationNumber]        NVARCHAR (50)  NULL,
    [ChovaCertificateNumber]    NVARCHAR (50)  NULL,
    [VehicleTypeID]             INT            NULL,
    [Manufacturer]              NVARCHAR (50)  NULL,
    [ModelCode]                 NVARCHAR (50)  NULL,
    [Year]                      INT            NULL,
    [Engine]                    NVARCHAR (50)  NULL,
    [Weight]                    DECIMAL (18)   NULL,
    [PlacesNumber]              INT            NULL,
    [ProtectionCode]            NVARCHAR (50)  NULL,
    [ChassisNumber]             NVARCHAR (50)  NULL,
    [InsuranceThisYear]         BIT            NULL,
    [InsuranceLastYear]         BIT            NULL,
    [Insurance2YearsAgo]        BIT            NULL,
    [Insurance3YearsAgo]        BIT            NULL,
    [ClaimsNumberThisYear]      INT            NULL,
    [ClaimsNumberLastYear]      INT            NULL,
    [ClaimsNumber2YearsAgo]     INT            NULL,
    [ClaimsNumber3YearsAgo]     INT            NULL,
    [InsuranceCompanyThisYear]  NVARCHAR (50)  NULL,
    [InsuranceCompanyLastYear]  NVARCHAR (50)  NULL,
    [InsuranceCompany2YearsAgo] NVARCHAR (50)  NULL,
    [InsuranceCompany3YearsAgo] NVARCHAR (50)  NULL,
    [YoungestDriverAge]         INT            NULL,
    [YoungestDriverSeniority]   INT            NULL,
    [OtherDriversAge]           INT            NULL,
    [OtherDriversSeniority]     INT            NULL,
    [AllowedToDriveID]          INT            NULL,
    [DrivesInShabbat]           BIT            NULL,
    [Comments]                  NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_CarPolicies_1] PRIMARY KEY CLUSTERED ([ElementaryPolicyID] ASC),
    CONSTRAINT [FK_CarPolicies_AllowedToDrives] FOREIGN KEY ([AllowedToDriveID]) REFERENCES [dbo].[AllowedToDrives] ([AllowedToDriveID]),
    CONSTRAINT [FK_CarPolicies_ElementaryPolicies] FOREIGN KEY ([ElementaryPolicyID]) REFERENCES [dbo].[ElementaryPolicies] ([ElementaryPolicyID]),
    CONSTRAINT [FK_CarPolicies_VehicleTypes] FOREIGN KEY ([VehicleTypeID]) REFERENCES [dbo].[VehicleTypes] ([VehicleTypeID])
);

