﻿CREATE TABLE [dbo].[LifeClaims] (
    [LifeClaimID]            INT            IDENTITY (1, 1) NOT NULL,
    [LifePolicyID]           INT            NOT NULL,
    [LifeClaimTypeID]        INT            NULL,
    [LifeClaimConditionID]   INT            NULL,
    [ClaimStatus]            BIT            NULL,
    [ClaimNumber]            NVARCHAR (50)  NULL,
    [OpenDate]               DATE           NULL,
    [DeliveredToCompanyDate] DATE           NULL,
    [MoneyReceivedDate]      DATE           NULL,
    [ClaimAmount]            MONEY          NULL,
    [AmountReceived]         MONEY          NULL,
    [EventDateAndTime]       DATETIME       NULL,
    [EventPlace]             NVARCHAR (MAX) NULL,
    [EventDescription]       NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_LifeClaims] PRIMARY KEY CLUSTERED ([LifeClaimID] ASC),
    CONSTRAINT [FK_LifeClaims_LifeClaimConditions] FOREIGN KEY ([LifeClaimConditionID]) REFERENCES [dbo].[LifeClaimConditions] ([LifeClaimConditionID]),
    CONSTRAINT [FK_LifeClaims_LifeClaimTypes] FOREIGN KEY ([LifeClaimTypeID]) REFERENCES [dbo].[LifeClaimTypes] ([LifeClaimTypeID]),
    CONSTRAINT [FK_LifeClaims_LifePolicies] FOREIGN KEY ([LifePolicyID]) REFERENCES [dbo].[LifePolicies] ([LifePolicyID])
);

