﻿CREATE TABLE [dbo].[LifeIndustries] (
    [LifeIndustryID]   INT           IDENTITY (1, 1) NOT NULL,
    [LifeIndustryName] NVARCHAR (50) NOT NULL,
    CONSTRAINT [PK_LifeIndustries] PRIMARY KEY CLUSTERED ([LifeIndustryID] ASC)
);

