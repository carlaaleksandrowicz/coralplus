﻿CREATE TABLE [dbo].[TravelCoverages] (
    [TravelCoverageID]     INT            IDENTITY (1, 1) NOT NULL,
    [TravelPolicyID]       INT            NOT NULL,
    [TravelCoverageTypeID] INT            NOT NULL,
    [CoverageAmount]       MONEY          NULL,
    [Porcentage]           DECIMAL (18)   NULL,
    [premium]              MONEY          NULL,
    [Comments]             NVARCHAR (MAX) NULL,
    [Date]                 DATETIME       NULL,
    [Addition]             INT            NULL,
    CONSTRAINT [PK_TravelCoverages] PRIMARY KEY CLUSTERED ([TravelCoverageID] ASC),
    CONSTRAINT [FK_TravelCoverages_TravelCoverageTypes] FOREIGN KEY ([TravelCoverageTypeID]) REFERENCES [dbo].[TravelCoverageTypes] ([TravelCoverageTypeID]),
    CONSTRAINT [FK_TravelCoverages_TravelPolicies] FOREIGN KEY ([TravelPolicyID]) REFERENCES [dbo].[TravelPolicies] ([TravelPolicyID])
);

