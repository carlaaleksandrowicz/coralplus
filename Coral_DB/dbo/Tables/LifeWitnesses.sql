﻿CREATE TABLE [dbo].[LifeWitnesses] (
    [LifeWitnessID]    INT            IDENTITY (1, 1) NOT NULL,
    [LifeClaimID]      INT            NOT NULL,
    [WitnessName]      NVARCHAR (50)  NULL,
    [WittnessAddress]  NVARCHAR (100) NULL,
    [WitnessPhone]     NVARCHAR (20)  NULL,
    [WitnessCellPhone] NVARCHAR (20)  NULL,
    CONSTRAINT [PK_LifeWitnesses] PRIMARY KEY CLUSTERED ([LifeWitnessID] ASC),
    CONSTRAINT [FK_LifeWitnesses_LifeClaims] FOREIGN KEY ([LifeClaimID]) REFERENCES [dbo].[LifeClaims] ([LifeClaimID])
);

