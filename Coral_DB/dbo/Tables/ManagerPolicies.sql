﻿CREATE TABLE [dbo].[ManagerPolicies] (
    [LifePolicyID]                 INT            NOT NULL,
    [IsFundNotPaysForCompensation] BIT            NULL,
    [IsCompulsoryPension]          BIT            NULL,
    [IsShareholder]                BIT            NULL,
    [Comments]                     NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_ManagerPolicies] PRIMARY KEY CLUSTERED ([LifePolicyID] ASC),
    CONSTRAINT [FK_ManagerPolicies_LifePolicies] FOREIGN KEY ([LifePolicyID]) REFERENCES [dbo].[LifePolicies] ([LifePolicyID])
);

