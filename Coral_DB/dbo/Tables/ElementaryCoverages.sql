﻿CREATE TABLE [dbo].[ElementaryCoverages] (
    [ElementaryCoverageID]     INT            IDENTITY (1, 1) NOT NULL,
    [ElementaryPolicyID]       INT            NOT NULL,
    [ElementaryCoverageTypeID] INT            NOT NULL,
    [CoverageAmount]           MONEY          NULL,
    [Porcentage]               DECIMAL (18)   NULL,
    [premium]                  MONEY          NULL,
    [Comments]                 NVARCHAR (MAX) NULL,
    [Date]                     DATETIME       NULL,
    [Addition]                 INT            NULL,
    CONSTRAINT [PK_Coverages] PRIMARY KEY CLUSTERED ([ElementaryCoverageID] ASC),
    CONSTRAINT [FK_ElementaryCoverages_ElementaryCoverageTypes] FOREIGN KEY ([ElementaryCoverageTypeID]) REFERENCES [dbo].[ElementaryCoverageTypes] ([ElementaryCoverageTypeID]),
    CONSTRAINT [FK_ElementaryCoverages_ElementaryPolicies] FOREIGN KEY ([ElementaryPolicyID]) REFERENCES [dbo].[ElementaryPolicies] ([ElementaryPolicyID])
);

