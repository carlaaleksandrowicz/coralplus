﻿CREATE TABLE [dbo].[ApartmentBusinessClaims] (
    [ElementaryClaimID]   INT   NOT NULL,
    [TotalAmountClaimed]  MONEY NULL,
    [IsThirdPartyDamages] BIT   NULL,
    CONSTRAINT [PK_ApartmentBusinessClaims_1] PRIMARY KEY CLUSTERED ([ElementaryClaimID] ASC),
    CONSTRAINT [FK_ApartmentBusinessClaims_ElementaryClaims] FOREIGN KEY ([ElementaryClaimID]) REFERENCES [dbo].[ElementaryClaims] ([ElementaryClaimID])
);

