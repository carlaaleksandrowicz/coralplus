﻿CREATE TABLE [dbo].[ForeingWorkersInsuranceTypes] (
    [ForeingWorkersInsuranceTypeID]   INT           IDENTITY (1, 1) NOT NULL,
    [ForeingWorkersIndustryID]        INT           NOT NULL,
    [CompanyID]                       INT           NOT NULL,
    [ForeingWorkersInsuranceTypeName] NVARCHAR (50) NULL,
    [Status]                          BIT           NULL,
    CONSTRAINT [PK_ForeingWorkersInsuranceTypes] PRIMARY KEY CLUSTERED ([ForeingWorkersInsuranceTypeID] ASC),
    CONSTRAINT [FK_ForeingWorkersInsuranceTypes_Companies] FOREIGN KEY ([CompanyID]) REFERENCES [dbo].[Companies] ([CompanyID]),
    CONSTRAINT [FK_ForeingWorkersInsuranceTypes_ForeingWorkersIndustries] FOREIGN KEY ([ForeingWorkersIndustryID]) REFERENCES [dbo].[ForeingWorkersIndustries] ([ForeingWorkersIndustryID])
);

