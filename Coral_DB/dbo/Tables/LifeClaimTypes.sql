﻿CREATE TABLE [dbo].[LifeClaimTypes] (
    [LifeClaimTypeID] INT           IDENTITY (1, 1) NOT NULL,
    [ClaimTypeName]   NVARCHAR (50) NULL,
    [Status]          BIT           NULL,
    CONSTRAINT [PK_LifeClaimTypes] PRIMARY KEY CLUSTERED ([LifeClaimTypeID] ASC)
);

