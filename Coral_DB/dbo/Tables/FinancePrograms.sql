﻿CREATE TABLE [dbo].[FinancePrograms] (
    [ProgramID]     INT           IDENTITY (1, 1) NOT NULL,
    [ProgramTypeID] INT           NOT NULL,
    [CompanyID]     INT           NOT NULL,
    [ProgramName]   NVARCHAR (50) NULL,
    [Status]        BIT           NULL,
    CONSTRAINT [PK_FinancePrograms] PRIMARY KEY CLUSTERED ([ProgramID] ASC),
    CONSTRAINT [FK_FinancePrograms_Companies] FOREIGN KEY ([CompanyID]) REFERENCES [dbo].[Companies] ([CompanyID]),
    CONSTRAINT [FK_FinancePrograms_FinanceProgramTypes] FOREIGN KEY ([ProgramTypeID]) REFERENCES [dbo].[FinanceProgramTypes] ([ProgramTypeID])
);

