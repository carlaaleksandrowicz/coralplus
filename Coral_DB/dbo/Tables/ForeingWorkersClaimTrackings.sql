﻿CREATE TABLE [dbo].[ForeingWorkersClaimTrackings] (
    [ForeingWorkersClaimTrackingID] INT            IDENTITY (1, 1) NOT NULL,
    [ForeingWorkersClaimID]         INT            NOT NULL,
    [UserID]                        INT            NULL,
    [Date]                          DATETIME       NULL,
    [TrackingContent]               NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_ForeingWorkersClaimTrackings] PRIMARY KEY CLUSTERED ([ForeingWorkersClaimTrackingID] ASC),
    CONSTRAINT [FK_ForeingWorkersClaimTrackings_ForeingWorkersClaims] FOREIGN KEY ([ForeingWorkersClaimID]) REFERENCES [dbo].[ForeingWorkersClaims] ([ForeingWorkersClaimID]),
    CONSTRAINT [FK_ForeingWorkersClaimTrackings_Users] FOREIGN KEY ([UserID]) REFERENCES [dbo].[Users] ([UserID])
);

