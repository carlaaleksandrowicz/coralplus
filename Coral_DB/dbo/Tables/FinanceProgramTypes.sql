﻿CREATE TABLE [dbo].[FinanceProgramTypes] (
    [ProgramTypeID]   INT           IDENTITY (1, 1) NOT NULL,
    [ProgramTypeName] NVARCHAR (50) NOT NULL,
    CONSTRAINT [PK_FinanceProgramTypes] PRIMARY KEY CLUSTERED ([ProgramTypeID] ASC)
);

