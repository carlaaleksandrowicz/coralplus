﻿CREATE TABLE [dbo].[CarRenewalQuotes] (
    [CarRenewalQuoteID]   INT           IDENTITY (1, 1) NOT NULL,
    [ElementaryPolicyID]  INT           NULL,
    [InsuranceIndustryID] INT           NOT NULL,
    [PolicyNumber]        NVARCHAR (50) NULL,
    [Premium]             MONEY         NULL,
    [CompanyID]           INT           NULL,
    [IsPackageIncluded] BIT NULL, 
    [AllowedToDriveID] INT NULL, 
    CONSTRAINT [PK_CarRenewalQuotes] PRIMARY KEY CLUSTERED ([CarRenewalQuoteID] ASC),
    CONSTRAINT [FK_CarRenewalQuotes_Companies] FOREIGN KEY ([CompanyID]) REFERENCES [dbo].[Companies] ([CompanyID]),
    CONSTRAINT [FK_CarRenewalQuotes_ElementaryPolicies] FOREIGN KEY ([ElementaryPolicyID]) REFERENCES [dbo].[ElementaryPolicies] ([ElementaryPolicyID]),
    CONSTRAINT [FK_CarRenewalQuotes_InsuranceIndustries] FOREIGN KEY ([InsuranceIndustryID]) REFERENCES [dbo].[InsuranceIndustries] ([InsuranceIndustryID]), 
    CONSTRAINT [FK_CarRenewalQuotes_AllowedToDrives] FOREIGN KEY (AllowedToDriveID) REFERENCES [dbo].[AllowedToDrives](AllowedToDriveID)
);

