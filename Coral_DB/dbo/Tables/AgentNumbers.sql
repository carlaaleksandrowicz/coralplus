﻿CREATE TABLE [dbo].[AgentNumbers] (
    [AgentNumberID] INT           IDENTITY (1, 1) NOT NULL,
    [AgentID]       INT           NOT NULL,
    [InsuranceID]   INT           NOT NULL,
    [CompanyID]     INT           NOT NULL,
    [Number]        NVARCHAR (50) NOT NULL,
    CONSTRAINT [PK_AgentNumbers] PRIMARY KEY CLUSTERED ([AgentNumberID] ASC),
    CONSTRAINT [FK_AgentNumbers_Agents] FOREIGN KEY ([AgentID]) REFERENCES [dbo].[Agents] ([AgentID]),
    CONSTRAINT [FK_AgentNumbers_Companies] FOREIGN KEY ([CompanyID]) REFERENCES [dbo].[Companies] ([CompanyID]),
    CONSTRAINT [FK_AgentNumbers_Insurances] FOREIGN KEY ([InsuranceID]) REFERENCES [dbo].[Insurances] ([InsuranceID])
);

