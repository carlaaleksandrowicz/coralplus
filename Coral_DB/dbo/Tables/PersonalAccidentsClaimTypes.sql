﻿CREATE TABLE [dbo].[PersonalAccidentsClaimTypes] (
    [PersonalAccidentsClaimTypeID] INT           IDENTITY (1, 1) NOT NULL,
    [ClaimTypeName]                NVARCHAR (50) NULL,
    [Status]                       BIT           NULL,
    CONSTRAINT [PK_PersonalAccidentsClaimTypes] PRIMARY KEY CLUSTERED ([PersonalAccidentsClaimTypeID] ASC)
);

