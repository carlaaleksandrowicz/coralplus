﻿CREATE TABLE [dbo].[ForeingWorkersRenewalTrackings] (
    [ForeingWorkersRenewalTrackingID] INT            IDENTITY (1, 1) NOT NULL,
    [ForeingWorkersPolicyID]          INT            NOT NULL,
    [UserID]                          INT            NOT NULL,
    [Date]                            DATETIME       NULL,
    [TrackingContent]                 NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_ForeingWorkersRenewalTrackings] PRIMARY KEY CLUSTERED ([ForeingWorkersRenewalTrackingID] ASC),
    CONSTRAINT [FK_ForeingWorkersRenewalTrackings_ForeingWorkersPolicies] FOREIGN KEY ([ForeingWorkersPolicyID]) REFERENCES [dbo].[ForeingWorkersPolicies] ([ForeingWorkersPolicyID]),
    CONSTRAINT [FK_ForeingWorkersRenewalTrackings_Users] FOREIGN KEY ([UserID]) REFERENCES [dbo].[Users] ([UserID])
);

