﻿CREATE TABLE [dbo].[RequestCategories] (
    [RequestCategoryID]   INT           IDENTITY (1, 1) NOT NULL,
    [InsuranceID]         INT           NULL,
    [RequestCategoryName] NVARCHAR (50) NOT NULL,
    [Status]              BIT           NULL,
    CONSTRAINT [PK_RequestCategories] PRIMARY KEY CLUSTERED ([RequestCategoryID] ASC),
    CONSTRAINT [FK_RequestCategories_Insurances] FOREIGN KEY ([InsuranceID]) REFERENCES [dbo].[Insurances] ([InsuranceID])
);

