﻿CREATE TABLE [dbo].[Contacts] (
    [ContactID]         INT            IDENTITY (1, 1) NOT NULL,
    [ContactCategoryID] INT            NULL,
    [LastName]          NVARCHAR (50)  NULL,
    [FirstName]         NVARCHAR (50)  NULL,
    [CompanyName]       NVARCHAR (50)  NULL,
    [Phone]             NVARCHAR (50)  NULL,
    [CellPhone]         NVARCHAR (50)  NULL,
    [Fax]               NVARCHAR (50)  NULL,
    [Email]             NVARCHAR (70)  NULL,
    [Website]           NVARCHAR (50)  NULL,
    [City]              NVARCHAR (50)  NULL,
    [Street]            NVARCHAR (70)  NULL,
    [HomeNumber]        NVARCHAR (10)  NULL,
    [ZipCode]           NVARCHAR (10)  NULL,
    [Remarks]           NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_Table_1] PRIMARY KEY CLUSTERED ([ContactID] ASC),
    CONSTRAINT [FK_Contacts_ContactCategories] FOREIGN KEY ([ContactCategoryID]) REFERENCES [dbo].[ContactCategories] ([ContactCategoryID])
);

