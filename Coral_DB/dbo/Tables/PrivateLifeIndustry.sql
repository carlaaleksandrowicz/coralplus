﻿CREATE TABLE [dbo].[PrivateLifeIndustry] (
    [LifePolicyID]                 INT            NOT NULL,
    [IsRewards]                    BIT            NULL,
    [Age]                          INT            NULL,
    [Budget]                       MONEY          NULL,
    [IsFundNotPaysForCompensation] BIT            NULL,
    [StartBudget]                  MONEY          NULL,
    [OneTimeDeposit]               MONEY          NULL,
    [PremiumFees]                  MONEY          NULL,
    [AggregateFees]                MONEY          NULL,
    [InvestmentPlanID]             INT            NULL,
    [Comments]                     NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_PrivateLifeIndustry] PRIMARY KEY CLUSTERED ([LifePolicyID] ASC),
    CONSTRAINT [FK_PrivateLifeIndustry_InvesmentPlans] FOREIGN KEY ([InvestmentPlanID]) REFERENCES [dbo].[InvesmentPlans] ([InvesmentPlanID]),
    CONSTRAINT [FK_PrivateLifeIndustry_LifePolicies] FOREIGN KEY ([LifePolicyID]) REFERENCES [dbo].[LifePolicies] ([LifePolicyID])
);

