﻿CREATE TABLE [dbo].[ApartmentBusinessDamages] (
    [ApartmentBusinessDamageID]    INT            IDENTITY (1, 1) NOT NULL,
    [ElementaryClaimID]            INT            NOT NULL,
    [DamageAndPropertyDescription] NVARCHAR (MAX) NULL,
    [SurveyItemNumber]             NVARCHAR (10)  NULL,
    [SurveyItemAmount]             MONEY          NULL,
    [AmountClaimed]                MONEY          NULL,
    [Comments]                     NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_ApartmentBusinessDamages] PRIMARY KEY CLUSTERED ([ApartmentBusinessDamageID] ASC),
    CONSTRAINT [FK_ApartmentBusinessDamages_ApartmentBusinessClaims] FOREIGN KEY ([ElementaryClaimID]) REFERENCES [dbo].[ApartmentBusinessClaims] ([ElementaryClaimID])
);

