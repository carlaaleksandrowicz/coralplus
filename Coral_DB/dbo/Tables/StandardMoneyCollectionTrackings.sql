﻿CREATE TABLE [dbo].[StandardMoneyCollectionTrackings]
(
	[StandardMoneyCollectionTrackingID] INT NOT NULL PRIMARY KEY IDENTITY, 
    [StandardMoneyCollectionID] INT NOT NULL, 
    [UserID] INT NULL, 
    [Date] DATETIME NULL, 
    [TrackingContent] NVARCHAR(MAX) NULL, 
    CONSTRAINT [FK_StandardMoneyCollectionTrackings_StandardMoneyCollection] FOREIGN KEY ([StandardMoneyCollectionID]) REFERENCES [StandardMoneyCollection]([StandardMoneyCollectionID]), 
    CONSTRAINT [FK_StandardMoneyCollectionTrackings_Users] FOREIGN KEY ([UserID]) REFERENCES [Users]([UserID])
)
