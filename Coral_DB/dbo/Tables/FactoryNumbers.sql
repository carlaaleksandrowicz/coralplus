﻿CREATE TABLE [dbo].[FactoryNumbers] (
    [FactoryNumberID] INT           IDENTITY (1, 1) NOT NULL,
    [PolicyOwnerID]   INT           NOT NULL,
    [InsuranceID]     INT           NOT NULL,
    [Number]          NVARCHAR (50) NOT NULL,
    [CompanyID]       INT           NOT NULL,
    CONSTRAINT [PK_FactoryNumbers] PRIMARY KEY CLUSTERED ([FactoryNumberID] ASC),
    CONSTRAINT [FK_FactoryNumbers_Companies] FOREIGN KEY ([CompanyID]) REFERENCES [dbo].[Companies] ([CompanyID]),
    CONSTRAINT [FK_FactoryNumbers_Insurances] FOREIGN KEY ([InsuranceID]) REFERENCES [dbo].[Insurances] ([InsuranceID]),
    CONSTRAINT [FK_FactoryNumbers_PolicyOwners] FOREIGN KEY ([PolicyOwnerID]) REFERENCES [dbo].[PolicyOwners] ([PolicyOwnerID])
);

