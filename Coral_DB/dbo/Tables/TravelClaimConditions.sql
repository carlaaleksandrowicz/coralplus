﻿CREATE TABLE [dbo].[TravelClaimConditions] (
    [TravelClaimConditionID] INT           IDENTITY (1, 1) NOT NULL,
    [Description]            NVARCHAR (50) NOT NULL,
    [Status]                 BIT           NULL,
    CONSTRAINT [PK_TravelClaimConditions] PRIMARY KEY CLUSTERED ([TravelClaimConditionID] ASC)
);

