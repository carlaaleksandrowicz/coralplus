﻿CREATE TABLE [dbo].[PersonalAccidentsAdditionalInsureds] (
    [PersonalAccidentsAdditionalInsuredID] INT           IDENTITY (1, 1) NOT NULL,
    [PersonalAccidensPolicyID]             INT           NOT NULL,
    [LastName]                             NVARCHAR (50) NULL,
    [FirstName]                            NVARCHAR (50) NULL,
    [IdNumber]                             NVARCHAR (50) NULL,
    [BirthDate]                            DATETIME      NULL,
    [Profession]                           NVARCHAR (50) NULL,
    [IsMale]                               BIT           NULL,
    [MaritalStatus]                        NVARCHAR (50) NULL,
    [InsuredTypeID]                        INT           NOT NULL,
    CONSTRAINT [PK_PersonalAccidentsAdditionalInsureds] PRIMARY KEY CLUSTERED ([PersonalAccidentsAdditionalInsuredID] ASC),
    CONSTRAINT [FK_PersonalAccidentsAdditionalInsureds_InsuredTypes] FOREIGN KEY ([InsuredTypeID]) REFERENCES [dbo].[InsuredTypes] ([InsuredTypeID]),
    CONSTRAINT [FK_PersonalAccidentsAdditionalInsureds_PersonalAccidentsPolicies] FOREIGN KEY ([PersonalAccidensPolicyID]) REFERENCES [dbo].[PersonalAccidentsPolicies] ([PersonalAccidentsPolicyID])
);

