﻿CREATE TABLE [dbo].[ForeingWorkersWitnesses] (
    [ForeingWorkersWitnessID] INT            IDENTITY (1, 1) NOT NULL,
    [ForeingWorkersClaimID]   INT            NOT NULL,
    [WitnessName]             NVARCHAR (50)  NULL,
    [WittnessAddress]         NVARCHAR (100) NULL,
    [WitnessPhone]            NVARCHAR (20)  NULL,
    [WitnessCellPhone]        NVARCHAR (20)  NULL,
    CONSTRAINT [PK_ForeingWorkersWitnesses] PRIMARY KEY CLUSTERED ([ForeingWorkersWitnessID] ASC),
    CONSTRAINT [FK_ForeingWorkersWitnesses_ForeingWorkersClaims] FOREIGN KEY ([ForeingWorkersClaimID]) REFERENCES [dbo].[ForeingWorkersClaims] ([ForeingWorkersClaimID])
);

