﻿CREATE TABLE [dbo].[CarThirdParties] (
    [CarThirdPartyID]           INT            IDENTITY (1, 1) NOT NULL,
    [ElementaryClaimID]         INT            NOT NULL,
    [RegistrationNumber]        NVARCHAR (50)  NULL,
    [VehicleTypeID]             INT            NULL,
    [Manufacturer]              NVARCHAR (50)  NULL,
    [CompanyID]                 INT            NULL,
    [ElementaryInsuranceTypeID] INT            NULL,
    [PolicyNumber]              NVARCHAR (50)  NULL,
    [PolicyHolderName]          NVARCHAR (50)  NULL,
    [PolicyHolderIdNumber]      NVARCHAR (50)  NULL,
    [PolicyHolderAddress]       NVARCHAR (100) NULL,
    [PolicyHolderPhone]         NVARCHAR (50)  NULL,
    [PolicyHolderCellPhone]     NVARCHAR (50)  NULL,
    [AgentName]                 NVARCHAR (50)  NULL,
    [AgentPhone]                NVARCHAR (50)  NULL,
    [AgentSecondPhone]          NVARCHAR (50)  NULL,
    [AgentFax]                  NVARCHAR (50)  NULL,
    [AgentEmail]                NVARCHAR (70)  NULL,
    [DriverName]                NVARCHAR (50)  NULL,
    [DriverIdNumber]            NVARCHAR (50)  NULL,
    [DriverAddress]             NVARCHAR (50)  NULL,
    [DriverPhone]               NVARCHAR (50)  NULL,
    [DriverCellPhone]           NVARCHAR (50)  NULL,
    [IsHurtPeople]              BIT            NULL,
    [DamageDescription]         NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_CarThirdParties] PRIMARY KEY CLUSTERED ([CarThirdPartyID] ASC),
    CONSTRAINT [FK_CarThirdParties_CarClaims] FOREIGN KEY ([ElementaryClaimID]) REFERENCES [dbo].[CarClaims] ([ElementaryClaimID]),
    CONSTRAINT [FK_CarThirdParties_Companies] FOREIGN KEY ([CompanyID]) REFERENCES [dbo].[Companies] ([CompanyID]),
    CONSTRAINT [FK_CarThirdParties_ElementaryInsuranceTypes] FOREIGN KEY ([ElementaryInsuranceTypeID]) REFERENCES [dbo].[ElementaryInsuranceTypes] ([ElementaryInsuranceTypeID]),
    CONSTRAINT [FK_CarThirdParties_VehicleTypes] FOREIGN KEY ([VehicleTypeID]) REFERENCES [dbo].[VehicleTypes] ([VehicleTypeID])
);

