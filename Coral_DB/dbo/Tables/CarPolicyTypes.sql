﻿CREATE TABLE [dbo].[CarPolicyTypes] (
    [CarPolicyTypeID]   INT           IDENTITY (1, 1) NOT NULL,
    [CarPolicyTypeName] NVARCHAR (50) NOT NULL,
    [Status]            BIT           NULL,
    CONSTRAINT [PK_CarPolicyTypes] PRIMARY KEY CLUSTERED ([CarPolicyTypeID] ASC)
);

