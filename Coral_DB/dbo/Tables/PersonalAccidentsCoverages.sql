﻿CREATE TABLE [dbo].[PersonalAccidentsCoverages] (
    [PersonalAccidentsCoverageID]     INT            IDENTITY (1, 1) NOT NULL,
    [PersonalAccidentsPolicyID]       INT            NOT NULL,
    [PersonalAccidentsCoverageTypeID] INT            NOT NULL,
    [Period]                          INT            NULL,
    [InsuranceAmount]                 MONEY          NULL,
    [IndexedAmount]                   MONEY          NULL,
    [MonthlyPremuim]                  MONEY          NULL,
    [IndexedPremium]                  MONEY          NULL,
    [StartDate]                       DATE           NULL,
    [EndDate]                         DATE           NULL,
    [Status]                          BIT            NULL,
    [MedicalAddition]                 DECIMAL (18)   NULL,
    [ProfessionalAddition]            DECIMAL (18)   NULL,
    [Comments]                        NVARCHAR (MAX) NULL,
    [Addition]                        INT            NULL,
    CONSTRAINT [PK_PersonalAccidentsCoverages] PRIMARY KEY CLUSTERED ([PersonalAccidentsCoverageID] ASC),
    CONSTRAINT [FK_PersonalAccidentsCoverages_PersonalAccidentsCoverageTypes] FOREIGN KEY ([PersonalAccidentsCoverageTypeID]) REFERENCES [dbo].[PersonalAccidentsCoverageTypes] ([PersonalAccidentsCoverageTypeID]),
    CONSTRAINT [FK_PersonalAccidentsCoverages_PersonalAccidentsPolicies] FOREIGN KEY ([PersonalAccidentsPolicyID]) REFERENCES [dbo].[PersonalAccidentsPolicies] ([PersonalAccidentsPolicyID])
);

