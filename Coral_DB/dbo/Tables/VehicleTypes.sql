﻿CREATE TABLE [dbo].[VehicleTypes] (
    [VehicleTypeID] INT           IDENTITY (1, 1) NOT NULL,
    [Description]   NVARCHAR (50) NOT NULL,
    [Status]        BIT           NULL,
    CONSTRAINT [PK_VehicleTypes] PRIMARY KEY CLUSTERED ([VehicleTypeID] ASC)
);

