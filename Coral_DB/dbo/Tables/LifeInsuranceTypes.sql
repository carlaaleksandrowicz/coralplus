﻿CREATE TABLE [dbo].[LifeInsuranceTypes] (
    [LifeInsuranceTypeID]   INT           IDENTITY (1, 1) NOT NULL,
    [LifeIndustryID]        INT           NOT NULL,
    [LifeInsuranceTypeName] NVARCHAR (50) NULL,
    [Status]                BIT           NULL,
    CONSTRAINT [PK_LifeInsuranceTypes] PRIMARY KEY CLUSTERED ([LifeInsuranceTypeID] ASC),
    CONSTRAINT [FK_LifeInsuranceTypes_LifeIndustries] FOREIGN KEY ([LifeIndustryID]) REFERENCES [dbo].[LifeIndustries] ([LifeIndustryID])
);

