﻿CREATE TABLE [dbo].[PersonalAccidentsTrackings] (
    [PersonalAccidentsTrackingID] INT            IDENTITY (1, 1) NOT NULL,
    [PersonalAccidentsPolicyID]   INT            NOT NULL,
    [UserID]                      INT            NULL,
    [Date]                        DATETIME       NULL,
    [TrackingContent]             NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_PersonalAccidentsTrackings] PRIMARY KEY CLUSTERED ([PersonalAccidentsTrackingID] ASC),
    CONSTRAINT [FK_PersonalAccidentsTrackings_PersonalAccidentsPolicies] FOREIGN KEY ([PersonalAccidentsPolicyID]) REFERENCES [dbo].[PersonalAccidentsPolicies] ([PersonalAccidentsPolicyID]),
    CONSTRAINT [FK_PersonalAccidentsTrackings_Users] FOREIGN KEY ([UserID]) REFERENCES [dbo].[Users] ([UserID])
);

