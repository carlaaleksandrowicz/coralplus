﻿CREATE TABLE [dbo].[ElementaryClaimTypes] (
    [ElementaryClaimTypeID] INT           IDENTITY (1, 1) NOT NULL,
    [InsuranceIndustryID]   INT           NOT NULL,
    [ClaimTypeName]         NVARCHAR (50) NULL,
    [Status]                BIT           NULL,
    CONSTRAINT [PK_ClaimTypes] PRIMARY KEY CLUSTERED ([ElementaryClaimTypeID] ASC),
    CONSTRAINT [FK_ElementaryClaimTypes_InsuranceIndustries] FOREIGN KEY ([InsuranceIndustryID]) REFERENCES [dbo].[InsuranceIndustries] ([InsuranceIndustryID])
);

