﻿CREATE TABLE [dbo].[Insurances] (
    [InsuranceID]   INT           IDENTITY (1, 1) NOT NULL,
    [InsuranceName] NVARCHAR (50) NOT NULL,
    CONSTRAINT [PK_Insurances] PRIMARY KEY CLUSTERED ([InsuranceID] ASC)
);

