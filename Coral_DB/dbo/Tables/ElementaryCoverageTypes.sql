﻿CREATE TABLE [dbo].[ElementaryCoverageTypes] (
    [ElementaryCoverageTypeID]   INT           IDENTITY (1, 1) NOT NULL,
    [InsuranceIndustryID]        INT           NOT NULL,
    [ElementaryCoverageCode]     NVARCHAR (50) NULL,
    [ElementaryCoverageTypeName] NVARCHAR (70) NULL,
    [Status]                     BIT           NULL,
    CONSTRAINT [PK_CoverageType] PRIMARY KEY CLUSTERED ([ElementaryCoverageTypeID] ASC),
    CONSTRAINT [FK_ElementaryCoverageTypes_InsuranceIndustries] FOREIGN KEY ([InsuranceIndustryID]) REFERENCES [dbo].[InsuranceIndustries] ([InsuranceIndustryID])
);

