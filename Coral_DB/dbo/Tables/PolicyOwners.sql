﻿CREATE TABLE [dbo].[PolicyOwners] (
    [PolicyOwnerID]   INT            IDENTITY (1, 1) NOT NULL,
    [PolicyOwnerName] NVARCHAR (50)  NOT NULL,
    [IdNumber]        NVARCHAR (50)  NULL,
    [City]            NVARCHAR (50)  NULL,
    [Street]          NVARCHAR (50)  NULL,
    [HomeNumber]      NVARCHAR (10)  NULL,
    [AptNumber]       NVARCHAR (50)  NULL,
    [ZipCode]         NVARCHAR (10)  NULL,
    [MailingAddress]  NVARCHAR (MAX) NULL,
    [Phone]           NVARCHAR (50)  NULL,
    [CellPhone]       NVARCHAR (50)  NULL,
    [Fax]             NVARCHAR (50)  NULL,
    [Email]           NVARCHAR (MAX) NULL,
    [ContactPerson]   NVARCHAR (50)  NULL,
    CONSTRAINT [PK_Table2] PRIMARY KEY CLUSTERED ([PolicyOwnerID] ASC)
);

