﻿CREATE TABLE [dbo].[ForeingWorkersClaims] (
    [ForeingWorkersClaimID]          INT            IDENTITY (1, 1) NOT NULL,
    [ForeingWorkersPolicyID]         INT            NOT NULL,
    [ForeingWorkersClaimTypeID]      INT            NULL,
    [ForeingWorkersClaimConditionID] INT            NULL,
    [ClaimStatus]                    BIT            NULL,
    [ClaimNumber]                    NVARCHAR (50)  NULL,
    [OpenDate]                       DATE           NULL,
    [DeliveredToCompanyDate]         DATE           NULL,
    [MoneyReceivedDate]              DATE           NULL,
    [ClaimAmount]                    MONEY          NULL,
    [AmountReceived]                 MONEY          NULL,
    [EventDateAndTime]               DATETIME       NULL,
    [EventPlace]                     NVARCHAR (MAX) NULL,
    [EventDescription]               NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_ForeingWorkersClaims] PRIMARY KEY CLUSTERED ([ForeingWorkersClaimID] ASC),
    CONSTRAINT [FK_ForeingWorkersClaims_ForeingWorkersClaimConditions] FOREIGN KEY ([ForeingWorkersClaimConditionID]) REFERENCES [dbo].[ForeingWorkersClaimConditions] ([ForeingWorkersClaimConditionID]),
    CONSTRAINT [FK_ForeingWorkersClaims_ForeingWorkersClaimTypes] FOREIGN KEY ([ForeingWorkersClaimTypeID]) REFERENCES [dbo].[ForeingWorkersClaimTypes] ([ForeingWorkersClaimTypeID]),
    CONSTRAINT [FK_ForeingWorkersClaims_ForeingWorkersPolicies] FOREIGN KEY ([ForeingWorkersPolicyID]) REFERENCES [dbo].[ForeingWorkersPolicies] ([ForeingWorkersPolicyID])
);

