﻿CREATE TABLE [dbo].[Relatives] (
    [RelativeID]     INT           IDENTITY (1, 1) NOT NULL,
    [LastName]       NVARCHAR (50) NULL,
    [FirstName]      NVARCHAR (50) NULL,
    [IdNumber]       NVARCHAR (50) NULL,
    [BirthDate]      DATE          NULL,
    [PassportNumber] NVARCHAR (50) NULL,
    [Profession]     NVARCHAR (50) NULL,
    [LicenseNumber]  NVARCHAR (50) NULL,
    [LicenseDate]    DATE          NULL,
    [Gender]         BIT           NULL,
    CONSTRAINT [PK_Relatives] PRIMARY KEY CLUSTERED ([RelativeID] ASC)
);

