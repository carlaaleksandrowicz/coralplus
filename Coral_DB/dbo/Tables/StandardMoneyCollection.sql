﻿CREATE TABLE [dbo].StandardMoneyCollection
(
	[StandardMoneyCollectionID] INT NOT NULL PRIMARY KEY IDENTITY, 
    [ElementaryPolicyID] INT NULL, 
    [PersonalAccidentsPolicyID] INT NULL, 
    [TravelPolicyID] INT NULL, 
    [LifePolicyID] INT NULL, 
    [HealthPolicyID] INT NULL, 
    [FinancePolicyID] INT NULL, 
    [ForeingWorkersPolicyID] INT NULL, 
    [MoneyCollectionAmount] MONEY NULL, 
    [MoneyCollectionTypeID] INT NULL, 
    [IsStandardMoneyCollectionOpen] BIT NULL, 
    [IsFirstLetterSended] BIT NULL, 
    [IsSecondLetterSended] BIT NULL, 
    [IsPolicyCanceled] BIT NULL, 
    [PotencialCancelationDate] DATETIME NULL, 
    [Comments] NVARCHAR(MAX) NULL, 
    CONSTRAINT [FK_StandardMoneyCollection_ElementaryPolicies] FOREIGN KEY ([ElementaryPolicyID]) REFERENCES [ElementaryPolicies]([ElementaryPolicyID]), 
    CONSTRAINT [FK_StandardMoneyCollection_PersonalAccidentsPolicies] FOREIGN KEY ([PersonalAccidentsPolicyID]) REFERENCES [PersonalAccidentsPolicies]([PersonalAccidentsPolicyID]), 
    CONSTRAINT [FK_StandardMoneyCollection_TravelPolicies] FOREIGN KEY ([TravelPolicyID]) REFERENCES [TravelPolicies]([TravelPolicyID]), 
    CONSTRAINT [FK_StandardMoneyCollection_LifePolicies] FOREIGN KEY ([LifePolicyID]) REFERENCES [LifePolicies]([LifePolicyID]), 
    CONSTRAINT [FK_StandardMoneyCollection_HealthPolicies] FOREIGN KEY ([HealthPolicyID]) REFERENCES [HealthPolicies]([HealthPolicyID]), 
    CONSTRAINT [FK_StandardMoneyCollection_FinancePolicies] FOREIGN KEY ([FinancePolicyID]) REFERENCES [FinancePolicies]([FinancePolicyID]), 
    CONSTRAINT [FK_StandardMoneyCollection_ForeingWorkersPolicies] FOREIGN KEY ([ForeingWorkersPolicyID]) REFERENCES [ForeingWorkersPolicies]([ForeingWorkersPolicyID]), 
    CONSTRAINT [FK_StandardMoneyCollection_MoneyCollectionTypes] FOREIGN KEY ([MoneyCollectionTypeID]) REFERENCES [MoneyCollectionTypes]([MoneyCollectionTypeID]) 
)
