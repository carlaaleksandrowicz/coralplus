﻿CREATE TABLE [dbo].[Risks] (
    [RiskID]                           INT            IDENTITY (1, 1) NOT NULL,
    [LifePolicyID]                     INT            NOT NULL,
    [LifeAdditionalInsuredID]          INT            NULL,
    [IsDeathType1]                     BIT            NULL,
    [IsDeathType5]                     BIT            NULL,
    [IsDeathTypeFixed]                 BIT            NULL,
    [DeathAmount]                      MONEY          NULL,
    [DeathPeriod]                      NVARCHAR (50)  NULL,
    [DeathCoverageType]                NVARCHAR (50)  NULL,
    [DeathPremium]                     MONEY          NULL,
    [DeathComments]                    NVARCHAR (MAX) NULL,
    [IsProfessionalWorkImpossibility]  BIT            NULL,
    [WorkImpossibilityAmount]          MONEY          NULL,
    [WorkImpossibilityPeriod]          NVARCHAR (50)  NULL,
    [WorkImpossibilityCoverageType]    NVARCHAR (50)  NULL,
    [WorkImpossibilityPremium]         MONEY          NULL,
    [WorkImpossibilityComments]        NVARCHAR (MAX) NULL,
    [SecureIncomeAmount]               MONEY          NULL,
    [SecureIncomePeriod]               NVARCHAR (50)  NULL,
    [SecureIncomeCoverageType]         NVARCHAR (50)  NULL,
    [SecureIncomePremium]              MONEY          NULL,
    [SecureIncomeComments]             NVARCHAR (MAX) NULL,
    [DisabilityByAccidentAmount]       MONEY          NULL,
    [DisabilityByAccidentPeriod]       NVARCHAR (50)  NULL,
    [DisabilityByAccidentCoverageType] NVARCHAR (50)  NULL,
    [DisabilityByAccidentPremium]      MONEY          NULL,
    [DisabilityByAccidentComments]     NVARCHAR (MAX) NULL,
    [DeathByAccidentAmount]            MONEY          NULL,
    [DeathByAccidentPeriod]            NVARCHAR (50)  NULL,
    [DeathByAccidentCoverageType]      NVARCHAR (50)  NULL,
    [DeathByAccidentPremium]           MONEY          NULL,
    [DeathByAccidentComments]          NVARCHAR (MAX) NULL,
    [CriticalIllnessAmount]            MONEY          NULL,
    [CriticalIllnessPeriod]            NVARCHAR (50)  NULL,
    [CriticalIllnessCoverageType]      NVARCHAR (50)  NULL,
    [CriticalIllnessPremium]           MONEY          NULL,
    [CriticalIllnessComments]          NVARCHAR (MAX) NULL,
    [Other1Name]                       NVARCHAR (50)  NULL,
    [Other1Amount]                     MONEY          NULL,
    [Other1Period]                     NVARCHAR (50)  NULL,
    [Other1CoverageType]               NVARCHAR (50)  NULL,
    [Other1Premium]                    MONEY          NULL,
    [Other1Comments]                   NVARCHAR (MAX) NULL,
    [Other2Name]                       NVARCHAR (50)  NULL,
    [Other2Amount]                     MONEY          NULL,
    [Other2Period]                     NVARCHAR (50)  NULL,
    [Other2CoverageType]               NVARCHAR (50)  NULL,
    [Other2Premium]                    MONEY          NULL,
    [Other2Comments]                   NVARCHAR (MAX) NULL,
    [Other3Name]                       NVARCHAR (50)  NULL,
    [Other3Amount]                     MONEY          NULL,
    [Other3Period]                     NVARCHAR (50)  NULL,
    [Other3CoverageType]               NVARCHAR (50)  NULL,
    [Other3Premium]                    MONEY          NULL,
    [Other3Comments]                   NVARCHAR (MAX) NULL,
    [Other4Name]                       NVARCHAR (50)  NULL,
    [Other4Amount]                     MONEY          NULL,
    [Other4Period]                     NVARCHAR (50)  NULL,
    [Other4CoverageType]               NVARCHAR (50)  NULL,
    [Other4Premium]                    MONEY          NULL,
    [Other4Comments]                   NVARCHAR (MAX) NULL,
    [Other5Name]                       NVARCHAR (50)  NULL,
    [Other5Amount]                     MONEY          NULL,
    [Other5Period]                     NVARCHAR (50)  NULL,
    [Other5CoverageType]               NVARCHAR (50)  NULL,
    [Other5Premium]                    MONEY          NULL,
    [Other5Comments]                   NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_Risks] PRIMARY KEY CLUSTERED ([RiskID] ASC),
    CONSTRAINT [FK_Risks_LifeAdditionalInsureds] FOREIGN KEY ([LifeAdditionalInsuredID]) REFERENCES [dbo].[LifeAdditionalInsureds] ([LifeAdditionalInsuredID]),
    CONSTRAINT [FK_Risks_LifePolicies] FOREIGN KEY ([LifePolicyID]) REFERENCES [dbo].[LifePolicies] ([LifePolicyID])
);

