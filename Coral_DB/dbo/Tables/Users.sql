﻿CREATE TABLE [dbo].[Users] (
    [UserID]                        INT           IDENTITY (1, 1) NOT NULL,
    [Usermame]                      NVARCHAR (50) NOT NULL,
    [Password]                      NVARCHAR (50) NOT NULL,
    [Status]                        BIT           NULL,
    [IsElementaryPermission]        BIT           NULL,
    [IsPersonalAccidentsPermission] BIT           NULL,
    [IsTravelPermission]            BIT           NULL,
    [IsLifePermission]              BIT           NULL,
    [IsHealthPermission]            BIT           NULL,
    [IsForeingWorkersPermission]    BIT           NULL,
    [IsFinancePermission]           BIT           NULL,
    [IsRenewalsPermission]          BIT           NULL,
    [IsOffersPermission]            BIT           NULL,
    [IsReportsPermission]           BIT           NULL,
    [IsTasksPermission]             BIT           NULL,
    [IsCalendarPermission]          BIT           NULL,
    [IsScanPermission]              BIT           NULL,
    [IsClientProfilePermission]     BIT           NULL,
    [IsCreateClientPermission]      BIT           NULL,
    CONSTRAINT [PK_Users] PRIMARY KEY CLUSTERED ([UserID] ASC)
);

