﻿CREATE TABLE [dbo].[MoneyCollectionTypes]
(
	[MoneyCollectionTypeID] INT NOT NULL PRIMARY KEY IDENTITY, 
    [MoneyCollectionTypeName] NVARCHAR(50) NOT NULL, 
    [Status] BIT NULL
)
