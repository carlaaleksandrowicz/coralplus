﻿CREATE TABLE [dbo].[InvesmentPlans] (
    [InvesmentPlanID]   INT           IDENTITY (1, 1) NOT NULL,
    [InvesmentPlanName] NVARCHAR (50) NOT NULL,
    [Status]            BIT           NULL,
    CONSTRAINT [PK_InvesmentPlans] PRIMARY KEY CLUSTERED ([InvesmentPlanID] ASC)
);

