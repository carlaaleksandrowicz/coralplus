﻿CREATE TABLE [dbo].[LifeCoverages] (
    [LifeCoverageID]       INT            IDENTITY (1, 1) NOT NULL,
    [LifePolicyID]         INT            NOT NULL,
    [LifeCoverageTypeID]   INT            NOT NULL,
    [UpToAge]              INT            NULL,
    [InsuranceAmount]      MONEY          NULL,
    [IndexedAmount]        MONEY          NULL,
    [MonthlyPremuim]       MONEY          NULL,
    [IndexedPremium]       MONEY          NULL,
    [StartDate]            DATE           NULL,
    [EndDate]              DATE           NULL,
    [Status]               BIT            NULL,
    [MedicalAddition]      MONEY          NULL,
    [ProfessionalAddition] MONEY          NULL,
    [Comments]             NVARCHAR (MAX) NULL,
    [Addition]             INT            NULL,
    CONSTRAINT [PK_LifeCoverages] PRIMARY KEY CLUSTERED ([LifeCoverageID] ASC),
    CONSTRAINT [FK_LifeCoverages_LifeCoverageTypes] FOREIGN KEY ([LifeCoverageTypeID]) REFERENCES [dbo].[LifeCoverageTypes] ([LifeCoverageTypeID]),
    CONSTRAINT [FK_LifeCoverages_LifePolicies] FOREIGN KEY ([LifePolicyID]) REFERENCES [dbo].[LifePolicies] ([LifePolicyID])
);

