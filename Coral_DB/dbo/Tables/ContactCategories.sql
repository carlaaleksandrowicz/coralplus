﻿CREATE TABLE [dbo].[ContactCategories] (
    [ContactCategoryID]   INT           IDENTITY (1, 1) NOT NULL,
    [ContactCategoryName] NVARCHAR (50) NULL,
    [Status]              BIT           NULL,
    CONSTRAINT [PK_ContactCategories] PRIMARY KEY CLUSTERED ([ContactCategoryID] ASC)
);

