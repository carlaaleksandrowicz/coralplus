﻿CREATE TABLE [dbo].[InsuredTypes] (
    [InsuredTypeID]   INT           IDENTITY (1, 1) NOT NULL,
    [InsuredTypeName] NVARCHAR (50) NOT NULL,
    CONSTRAINT [PK_InsuredTypes] PRIMARY KEY CLUSTERED ([InsuredTypeID] ASC)
);

