﻿CREATE TABLE [dbo].[ForeingWorkersClaimTypes] (
    [ForeingWorkersClaimTypeID] INT           IDENTITY (1, 1) NOT NULL,
    [ClaimTypeName]             NVARCHAR (50) NULL,
    [Status]                    BIT           NULL,
    CONSTRAINT [PK_ForeingWorkersClaimTypes] PRIMARY KEY CLUSTERED ([ForeingWorkersClaimTypeID] ASC)
);

