﻿CREATE TABLE [dbo].[ElementaryInsuranceTypes] (
    [ElementaryInsuranceTypeID]   INT           IDENTITY (1, 1) NOT NULL,
    [InsuranceIndustryID]         INT           NOT NULL,
    [ElementaryInsuranceTypeName] NVARCHAR (50) NULL,
    [Status]                      BIT           NULL,
    CONSTRAINT [PK_InsuranceTypes] PRIMARY KEY CLUSTERED ([ElementaryInsuranceTypeID] ASC),
    CONSTRAINT [FK_ElementaryInsuranceTypes_InsuranceIndustries] FOREIGN KEY ([InsuranceIndustryID]) REFERENCES [dbo].[InsuranceIndustries] ([InsuranceIndustryID])
);

