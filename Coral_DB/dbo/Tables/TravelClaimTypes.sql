﻿CREATE TABLE [dbo].[TravelClaimTypes] (
    [TravelClaimTypeID] INT           IDENTITY (1, 1) NOT NULL,
    [ClaimTypeName]     NVARCHAR (50) NOT NULL,
    [Status]            BIT           NULL,
    CONSTRAINT [PK_TravelClaimTypes] PRIMARY KEY CLUSTERED ([TravelClaimTypeID] ASC)
);

