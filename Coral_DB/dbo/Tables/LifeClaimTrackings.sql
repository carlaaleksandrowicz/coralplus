﻿CREATE TABLE [dbo].[LifeClaimTrackings] (
    [LifeClaimTrackingID] INT            IDENTITY (1, 1) NOT NULL,
    [LifeClaimID]         INT            NOT NULL,
    [UserID]              INT            NULL,
    [Date]                DATETIME       NULL,
    [TrackingContent]     NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_LifeClaimTrackings] PRIMARY KEY CLUSTERED ([LifeClaimTrackingID] ASC),
    CONSTRAINT [FK_LifeClaimTrackings_LifeClaims] FOREIGN KEY ([LifeClaimID]) REFERENCES [dbo].[LifeClaims] ([LifeClaimID]),
    CONSTRAINT [FK_LifeClaimTrackings_Users] FOREIGN KEY ([UserID]) REFERENCES [dbo].[Users] ([UserID])
);

