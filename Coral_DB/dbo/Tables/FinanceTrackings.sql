﻿CREATE TABLE [dbo].[FinanceTrackings] (
    [FinanceTrackingID] INT            IDENTITY (1, 1) NOT NULL,
    [FinancePolicyID]   INT            NOT NULL,
    [UserID]            INT            NULL,
    [Date]              DATETIME       NULL,
    [TrackingContent]   NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_FinanceTrackings] PRIMARY KEY CLUSTERED ([FinanceTrackingID] ASC),
    CONSTRAINT [FK_FinanceTrackings_FinancePolicies] FOREIGN KEY ([FinancePolicyID]) REFERENCES [dbo].[FinancePolicies] ([FinancePolicyID]),
    CONSTRAINT [FK_FinanceTrackings_Users] FOREIGN KEY ([UserID]) REFERENCES [dbo].[Users] ([UserID])
);

