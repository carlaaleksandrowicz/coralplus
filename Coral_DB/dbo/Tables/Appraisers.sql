﻿CREATE TABLE [dbo].[Appraisers] (
    [AppraiserID]   INT           IDENTITY (1, 1) NOT NULL,
    [AppraiserName] NVARCHAR (50) NULL,
    [Status]        BIT           NULL,
    CONSTRAINT [PK_Appraisers] PRIMARY KEY CLUSTERED ([AppraiserID] ASC)
);

