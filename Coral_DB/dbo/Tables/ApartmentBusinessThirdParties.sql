﻿CREATE TABLE [dbo].[ApartmentBusinessThirdParties] (
    [ElementaryClaimID]  INT            NOT NULL,
    [DamagedName]        NVARCHAR (50)  NULL,
    [DamagedIdNumber]    NVARCHAR (50)  NULL,
    [DamagedAddress]     NVARCHAR (100) NULL,
    [DamagedPhone]       NVARCHAR (50)  NULL,
    [DamagedCellPhone]   NVARCHAR (50)  NULL,
    [DamagedEmail]       NVARCHAR (70)  NULL,
    [DamageDescription]  NVARCHAR (MAX) NULL,
    [TotalAmoundClaimed] MONEY          NULL,
    [DamagedCompanyID]   INT            NULL,
    CONSTRAINT [PK_ApartmentBusinessThirdParties_1] PRIMARY KEY CLUSTERED ([ElementaryClaimID] ASC),
    CONSTRAINT [FK_ApartmentBusinessThirdParties_ApartmentBusinessClaims] FOREIGN KEY ([ElementaryClaimID]) REFERENCES [dbo].[ApartmentBusinessClaims] ([ElementaryClaimID]),
    CONSTRAINT [FK_ApartmentBusinessThirdParties_Companies] FOREIGN KEY ([DamagedCompanyID]) REFERENCES [dbo].[Companies] ([CompanyID])
);

