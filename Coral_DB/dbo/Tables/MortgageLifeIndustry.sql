﻿CREATE TABLE [dbo].[MortgageLifeIndustry] (
    [LifePolicyID]                INT            NOT NULL,
    [IrrevocableBeneficiary]      NVARCHAR (50)  NULL,
    [BankNumber]                  NVARCHAR (50)  NULL,
    [BankBranch]                  NVARCHAR (50)  NULL,
    [BankAddress]                 NVARCHAR (MAX) NULL,
    [BankContactName]             NVARCHAR (50)  NULL,
    [BankPhone]                   NVARCHAR (50)  NULL,
    [BankFax]                     NVARCHAR (50)  NULL,
    [BankEmail]                   NVARCHAR (70)  NULL,
    [IsReleaseFromPayingPremiums] BIT            NULL,
    [Premium]                     MONEY          NULL,
    [Comments]                    NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_MortgageLifeIndustry] PRIMARY KEY CLUSTERED ([LifePolicyID] ASC),
    CONSTRAINT [FK_MortgageLifeIndustry_LifePolicies] FOREIGN KEY ([LifePolicyID]) REFERENCES [dbo].[LifePolicies] ([LifePolicyID])
);

