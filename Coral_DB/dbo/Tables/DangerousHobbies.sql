﻿CREATE TABLE [dbo].[DangerousHobbies] (
    [DangerousHobbyID] INT           IDENTITY (1, 1) NOT NULL,
    [HobbyName]        NVARCHAR (50) NOT NULL,
    [Status]           BIT           NULL,
    CONSTRAINT [PK_DangerousHobbies] PRIMARY KEY CLUSTERED ([DangerousHobbyID] ASC)
);

