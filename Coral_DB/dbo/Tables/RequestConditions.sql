﻿CREATE TABLE [dbo].[RequestConditions] (
    [RequestConditionID]   INT           IDENTITY (1, 1) NOT NULL,
    [RequestCategoryID]    INT           NOT NULL,
    [RequestConditionName] NVARCHAR (50) NOT NULL,
    [Status]               BIT           NULL,
    CONSTRAINT [PK_RequestConditions] PRIMARY KEY CLUSTERED ([RequestConditionID] ASC),
    CONSTRAINT [FK_RequestConditions_RequestCategories] FOREIGN KEY ([RequestCategoryID]) REFERENCES [dbo].[RequestCategories] ([RequestCategoryID])
);

