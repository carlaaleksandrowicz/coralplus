﻿CREATE TABLE [dbo].[LifeClaimConditions] (
    [LifeClaimConditionID] INT           IDENTITY (1, 1) NOT NULL,
    [Description]          NVARCHAR (50) NULL,
    [Status]               BIT           NULL,
    CONSTRAINT [PK_LifeClaim] PRIMARY KEY CLUSTERED ([LifeClaimConditionID] ASC)
);

