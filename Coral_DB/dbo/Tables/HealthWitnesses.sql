﻿CREATE TABLE [dbo].[HealthWitnesses] (
    [HealthWitnessID]  INT            IDENTITY (1, 1) NOT NULL,
    [HealthClaimID]    INT            NOT NULL,
    [WitnessName]      NVARCHAR (50)  NULL,
    [WittnessAddress]  NVARCHAR (100) NULL,
    [WitnessPhone]     NVARCHAR (20)  NULL,
    [WitnessCellPhone] NVARCHAR (20)  NULL,
    CONSTRAINT [PK_HealthWitnesses] PRIMARY KEY CLUSTERED ([HealthWitnessID] ASC),
    CONSTRAINT [FK_HealthWitnesses_HealthClaims] FOREIGN KEY ([HealthClaimID]) REFERENCES [dbo].[HealthClaims] ([HealthClaimID])
);

