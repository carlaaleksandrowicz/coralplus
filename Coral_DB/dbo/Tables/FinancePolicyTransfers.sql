﻿CREATE TABLE [dbo].[FinancePolicyTransfers] (
    [FinancePolicyTransferID]       INT      IDENTITY (1, 1) NOT NULL,
    [FinancePolicyID]               INT      NOT NULL,
    [TransferringCompanyID]         INT      NULL,
    [FinanceFundTypeID]             INT      NULL,
    [TransferDate]                  DATETIME NULL,
    [TransferAmount]                MONEY    NULL,
    [TransferedToCompanyID]         INT      NULL,
    [TransferedToFinanceFundTypeID] INT      NULL,
    CONSTRAINT [PK_FinancePolicyTransfers] PRIMARY KEY CLUSTERED ([FinancePolicyTransferID] ASC),
    CONSTRAINT [FK_FinancePolicyTransfers_Companies] FOREIGN KEY ([TransferedToCompanyID]) REFERENCES [dbo].[Companies] ([CompanyID]),
    CONSTRAINT [FK_FinancePolicyTransfers_Companies1] FOREIGN KEY ([TransferringCompanyID]) REFERENCES [dbo].[Companies] ([CompanyID]),
    CONSTRAINT [FK_FinancePolicyTransfers_FinanceFundTypes] FOREIGN KEY ([FinanceFundTypeID]) REFERENCES [dbo].[FinanceFundTypes] ([FinanceFundTypeID]),
    CONSTRAINT [FK_FinancePolicyTransfers_FinanceFundTypes1] FOREIGN KEY ([TransferedToFinanceFundTypeID]) REFERENCES [dbo].[FinanceFundTypes] ([FinanceFundTypeID]),
    CONSTRAINT [FK_FinancePolicyTransfers_FinancePolicies] FOREIGN KEY ([FinancePolicyID]) REFERENCES [dbo].[FinancePolicies] ([FinancePolicyID])
);

