﻿CREATE TABLE [dbo].[ClientTypes] (
    [ClientTypeID]   INT           IDENTITY (1, 1) NOT NULL,
    [ClientTypeName] NVARCHAR (50) NOT NULL,
    CONSTRAINT [PK_Client Types] PRIMARY KEY CLUSTERED ([ClientTypeID] ASC)
);

