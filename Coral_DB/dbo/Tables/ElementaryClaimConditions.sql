﻿CREATE TABLE [dbo].[ElementaryClaimConditions] (
    [ElementaryClaimConditionID] INT           IDENTITY (1, 1) NOT NULL,
    [InsuranceIndustryID]        INT           NOT NULL,
    [Description]                NVARCHAR (50) NULL,
    [Status]                     BIT           NULL,
    CONSTRAINT [PK_ClaimCondition] PRIMARY KEY CLUSTERED ([ElementaryClaimConditionID] ASC),
    CONSTRAINT [FK_ClaimConditions_InsuranceIndustries] FOREIGN KEY ([InsuranceIndustryID]) REFERENCES [dbo].[InsuranceIndustries] ([InsuranceIndustryID])
);

