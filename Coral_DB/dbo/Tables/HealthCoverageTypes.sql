﻿CREATE TABLE [dbo].[HealthCoverageTypes] (
    [HealthCoverageTypeID]   INT           IDENTITY (1, 1) NOT NULL,
    [HealthCoverageCode]     NVARCHAR (50) NULL,
    [HealthCoverageTypeName] NVARCHAR (70) NULL,
    [Status]                 BIT           NULL,
    CONSTRAINT [PK_HealthCoverageTypes] PRIMARY KEY CLUSTERED ([HealthCoverageTypeID] ASC)
);

