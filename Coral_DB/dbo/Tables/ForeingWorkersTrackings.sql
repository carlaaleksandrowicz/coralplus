﻿CREATE TABLE [dbo].[ForeingWorkersTrackings] (
    [ForeingWorkersTrackingID] INT            IDENTITY (1, 1) NOT NULL,
    [ForeingWorkersPolicyID]   INT            NOT NULL,
    [UserID]                   INT            NULL,
    [Date]                     DATETIME       NULL,
    [TrackingContent]          NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_ForeingWorkersTrackings] PRIMARY KEY CLUSTERED ([ForeingWorkersTrackingID] ASC),
    CONSTRAINT [FK_ForeingWorkersTrackings_ForeingWorkersPolicies] FOREIGN KEY ([ForeingWorkersPolicyID]) REFERENCES [dbo].[ForeingWorkersPolicies] ([ForeingWorkersPolicyID]),
    CONSTRAINT [FK_ForeingWorkersTrackings_Users] FOREIGN KEY ([UserID]) REFERENCES [dbo].[Users] ([UserID])
);

