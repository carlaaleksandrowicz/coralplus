﻿CREATE TABLE [dbo].[HealthInsuranceTypes] (
    [HealthInsuranceTypeID]   INT           IDENTITY (1, 1) NOT NULL,
    [CompanyID]               INT           NOT NULL,
    [HealthInsuranceTypeName] NVARCHAR (50) NOT NULL,
    [Status]                  BIT           NULL,
    CONSTRAINT [PK_Table1] PRIMARY KEY CLUSTERED ([HealthInsuranceTypeID] ASC),
    CONSTRAINT [FK_HealthInsuranceTypes_Companies] FOREIGN KEY ([CompanyID]) REFERENCES [dbo].[Companies] ([CompanyID])
);

