﻿CREATE TABLE [dbo].[MessagesAndReminders] (
    [MessageID]      INT            IDENTITY (1, 1) NOT NULL,
    [UserID]         INT            NULL,
    [ClientID]       INT            NOT NULL,
    [DateAndHour]    DATETIME       NULL,
    [MessageContent] NVARCHAR (MAX) NULL,
    [IsReminder]     BIT            NULL,
    [ReminderDate]   DATETIME       NULL,
    [ToUserID]       INT            NULL,
    CONSTRAINT [PK_MessagesAndReminders] PRIMARY KEY CLUSTERED ([MessageID] ASC),
    CONSTRAINT [FK_MessagesAndReminders_Clients1] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID]),
    CONSTRAINT [FK_MessagesAndReminders_Users] FOREIGN KEY ([UserID]) REFERENCES [dbo].[Users] ([UserID]),
    CONSTRAINT [FK_MessagesAndReminders_Users1] FOREIGN KEY ([ToUserID]) REFERENCES [dbo].[Users] ([UserID])
);

