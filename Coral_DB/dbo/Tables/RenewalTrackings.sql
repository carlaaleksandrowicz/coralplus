﻿CREATE TABLE [dbo].[RenewalTrackings] (
    [RenewalTrackingID]  INT            IDENTITY (1, 1) NOT NULL,
    [ElementaryPolicyID] INT            NOT NULL,
    [UserID]             INT            NOT NULL,
    [Date]               DATETIME       NULL,
    [TrackingContent]    NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_RenewalTrackings] PRIMARY KEY CLUSTERED ([RenewalTrackingID] ASC),
    CONSTRAINT [FK_RenewalTrackings_ElementaryPolicies] FOREIGN KEY ([ElementaryPolicyID]) REFERENCES [dbo].[ElementaryPolicies] ([ElementaryPolicyID]),
    CONSTRAINT [FK_RenewalTrackings_Users] FOREIGN KEY ([UserID]) REFERENCES [dbo].[Users] ([UserID])
);

