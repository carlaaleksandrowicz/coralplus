﻿CREATE TABLE [dbo].[DocumentNames] (
    [DocumentNameID] INT           IDENTITY (1, 1) NOT NULL,
    [DocName]        NVARCHAR (50) NOT NULL,
    [Status]         BIT           NULL,
    CONSTRAINT [PK_DocumentNames] PRIMARY KEY CLUSTERED ([DocumentNameID] ASC)
);

