﻿CREATE TABLE [dbo].[ElementaryTrackings] (
    [ElementaryTrackingID] INT            IDENTITY (1, 1) NOT NULL,
    [ElementaryPolicyID]   INT            NOT NULL,
    [UserID]               INT            NULL,
    [Date]                 DATETIME       NULL,
    [TrackingContent]      NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_Tracking] PRIMARY KEY CLUSTERED ([ElementaryTrackingID] ASC),
    CONSTRAINT [FK_ElementaryTrackings_ElementaryPolicies] FOREIGN KEY ([ElementaryPolicyID]) REFERENCES [dbo].[ElementaryPolicies] ([ElementaryPolicyID]),
    CONSTRAINT [FK_ElementaryTrackings_Users] FOREIGN KEY ([UserID]) REFERENCES [dbo].[Users] ([UserID])
);

