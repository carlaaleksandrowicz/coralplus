﻿CREATE TABLE [dbo].[TravelDamages] (
    [TravelDamageID]  INT            IDENTITY (1, 1) NOT NULL,
    [TravelClaimID]   INT            NOT NULL,
    [ItemDescription] NVARCHAR (MAX) NULL,
    [AmountClaimed]   MONEY          NULL,
    [Comments]        NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_TravelDamages] PRIMARY KEY CLUSTERED ([TravelDamageID] ASC),
    CONSTRAINT [FK_TravelDamages_TravelClaims] FOREIGN KEY ([TravelClaimID]) REFERENCES [dbo].[TravelClaims] ([TravelClaimID])
);

