﻿CREATE TABLE [dbo].[TravelTrackings] (
    [TravelTrackingID] INT            IDENTITY (1, 1) NOT NULL,
    [TravelPolicyID]   INT            NOT NULL,
    [UserID]           INT            NULL,
    [Date]             DATETIME       NULL,
    [TrackingContent]  NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_TravelTrackings] PRIMARY KEY CLUSTERED ([TravelTrackingID] ASC),
    CONSTRAINT [FK_TravelTrackings_TravelPolicies] FOREIGN KEY ([TravelPolicyID]) REFERENCES [dbo].[TravelPolicies] ([TravelPolicyID]),
    CONSTRAINT [FK_TravelTrackings_Users] FOREIGN KEY ([UserID]) REFERENCES [dbo].[Users] ([UserID])
);

