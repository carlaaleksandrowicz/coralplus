﻿CREATE TABLE [dbo].[RenewalStatuses] (
    [RenewalStatusID]   INT           IDENTITY (1, 1) NOT NULL,
    [RenewalStatusName] NVARCHAR (50) NOT NULL,
    [Status]            BIT           NULL,
    [Color]             NVARCHAR (50) NULL,
    CONSTRAINT [PK_RenewalStatuses] PRIMARY KEY CLUSTERED ([RenewalStatusID] ASC)
);

