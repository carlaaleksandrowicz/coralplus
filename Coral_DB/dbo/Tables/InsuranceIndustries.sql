﻿CREATE TABLE [dbo].[InsuranceIndustries] (
    [InsuranceIndustryID]   INT           IDENTITY (1, 1) NOT NULL,
    [InsuranceIndustryName] NVARCHAR (50) NOT NULL,
    CONSTRAINT [PK_InsuranceType] PRIMARY KEY CLUSTERED ([InsuranceIndustryID] ASC)
);

