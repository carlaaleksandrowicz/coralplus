﻿CREATE TABLE [dbo].[TravelInsuranceDetails] (
    [TravelInsuranceDetailID]    INT           IDENTITY (1, 1) NOT NULL,
    [TravelPolicyID]             INT           NOT NULL,
    [TravelAdditionalInsuredID]  INT           NULL,
    [TravelInsuranceTypeID]      INT           NULL,
    [IsWinterSport]              BIT           NULL,
    [IsExtremeSport]             BIT           NULL,
    [IsSearchAndRescue]          BIT           NULL,
    [IsPregnancy]                BIT           NULL,
    [IsExistingMedicalCondition] BIT           NULL,
    [IsBaggage]                  BIT           NULL,
    [IsComputerOrTablet]         BIT           NULL,
    [IsPhoneOrGPS]               BIT           NULL,
    [IsBicycle]                  BIT           NULL,
    [OtherName]                  NVARCHAR (50) NULL,
    [IsOther]                    BIT           NULL,
    CONSTRAINT [PK_TravelInsuranceDetails] PRIMARY KEY CLUSTERED ([TravelInsuranceDetailID] ASC),
    CONSTRAINT [FK_TravelInsuranceDetails_TravelAdditionalInsureds] FOREIGN KEY ([TravelAdditionalInsuredID]) REFERENCES [dbo].[TravelAdditionalInsureds] ([TavelAdditionalInsuredID]),
    CONSTRAINT [FK_TravelInsuranceDetails_TravelInsuranceTypes] FOREIGN KEY ([TravelInsuranceTypeID]) REFERENCES [dbo].[TravelInsuranceTypes] ([TravelInsuranceTypeID]),
    CONSTRAINT [FK_TravelInsuranceDetails_TravelPolicies] FOREIGN KEY ([TravelPolicyID]) REFERENCES [dbo].[TravelPolicies] ([TravelPolicyID])
);

