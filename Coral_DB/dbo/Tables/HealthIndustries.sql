﻿CREATE TABLE [dbo].[HealthIndustries] (
    [HealthIndustryID]   INT           IDENTITY (1, 1) NOT NULL,
    [HealthIndustryName] NVARCHAR (50) NOT NULL,
    CONSTRAINT [PK_HealthIndustries] PRIMARY KEY CLUSTERED ([HealthIndustryID] ASC)
);

