﻿CREATE TABLE [dbo].[Mortgages] (
    [MortgageID]               INT           IDENTITY (1, 1) NOT NULL,
    [LifePolicyID]             INT           NOT NULL,
    [MortgageNumber]           NVARCHAR (50) NULL,
    [MortgageAmount]           MONEY         NULL,
    [AnualInterest]            DECIMAL (18)  NULL,
    [HatzmadaType]             NVARCHAR (50) NULL,
    [StartDate]                DATETIME      NULL,
    [EndDate]                  DATETIME      NULL,
    [Premium]                  MONEY         NULL,
    [MortgagePeriod]           INT           NULL,
    [PremiumPeriod]            INT           NULL,
    [IsPrincipalInsuredPolicy] BIT           NULL,
    CONSTRAINT [PK_Mortgages] PRIMARY KEY CLUSTERED ([MortgageID] ASC),
    CONSTRAINT [FK_Mortgages_LifePolicies] FOREIGN KEY ([LifePolicyID]) REFERENCES [dbo].[LifePolicies] ([LifePolicyID])
);

