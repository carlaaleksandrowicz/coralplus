﻿CREATE TABLE [dbo].[PersonalAccidentsWitnesses] (
    [PersonalAccidentsWitnessID] INT            IDENTITY (1, 1) NOT NULL,
    [PersonalAccidentsClaimID]   INT            NOT NULL,
    [WitnessName]                NVARCHAR (50)  NULL,
    [WittnessAddress]            NVARCHAR (100) NULL,
    [WitnessPhone]               NVARCHAR (20)  NULL,
    [WitnessCellPhone]           NVARCHAR (20)  NULL,
    CONSTRAINT [PK_PersonalAccidentsWitnesses] PRIMARY KEY CLUSTERED ([PersonalAccidentsWitnessID] ASC),
    CONSTRAINT [FK_PersonalAccidentsWitnesses_PersonalAccidentsClaims] FOREIGN KEY ([PersonalAccidentsClaimID]) REFERENCES [dbo].[PersonalAccidentsClaims] ([PersonalAccidentsClaimID])
);

