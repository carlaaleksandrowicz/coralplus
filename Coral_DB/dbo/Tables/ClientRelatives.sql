﻿CREATE TABLE [dbo].[ClientRelatives] (
    [ClientID]     INT           NOT NULL,
    [RelativeID]   INT           NOT NULL,
    [Relationship] NVARCHAR (50) NULL,
    CONSTRAINT [PK_ClientRelatives] PRIMARY KEY CLUSTERED ([ClientID] ASC, [RelativeID] ASC),
    CONSTRAINT [FK_ClientRelatives_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID]),
    CONSTRAINT [FK_ClientRelatives_Relatives] FOREIGN KEY ([RelativeID]) REFERENCES [dbo].[Relatives] ([RelativeID])
);

