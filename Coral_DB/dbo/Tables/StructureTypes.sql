﻿CREATE TABLE [dbo].[StructureTypes] (
    [StructureTypeID]   INT           IDENTITY (1, 1) NOT NULL,
    [StructureTypeName] NVARCHAR (50) NULL,
    [Status]            BIT           NULL,
    CONSTRAINT [PK_StructureTypes] PRIMARY KEY CLUSTERED ([StructureTypeID] ASC)
);

