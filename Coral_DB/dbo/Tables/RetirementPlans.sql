﻿CREATE TABLE [dbo].[RetirementPlans] (
    [RetirementPlanID]   INT           IDENTITY (1, 1) NOT NULL,
    [RetirementPlanName] NVARCHAR (50) NOT NULL,
    [Status]             BIT           NULL,
    CONSTRAINT [PK_RetirementPlans] PRIMARY KEY CLUSTERED ([RetirementPlanID] ASC)
);

