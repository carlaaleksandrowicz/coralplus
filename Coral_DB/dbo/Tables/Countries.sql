﻿CREATE TABLE [dbo].[Countries] (
    [CountryID]   INT           IDENTITY (1, 1) NOT NULL,
    [CountryName] NVARCHAR (50) NOT NULL,
    [Status]      BIT           NULL,
    CONSTRAINT [PK_Countries] PRIMARY KEY CLUSTERED ([CountryID] ASC)
);

