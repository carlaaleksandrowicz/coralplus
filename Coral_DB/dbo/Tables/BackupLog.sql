﻿CREATE TABLE [dbo].[BackupLog]
(
	[BackupLogID] INT IDENTITY (1, 1) NOT NULL PRIMARY KEY, 
    [BackupDate] DATETIME NULL, 
    [Status] BIT NULL, 
    [UserID] INT NULL, 
    [BackupPath] NVARCHAR(MAX) NULL, 
    CONSTRAINT [FK_BackupLog_Users] FOREIGN KEY ([UserID]) REFERENCES [Users]([UserID])
)
