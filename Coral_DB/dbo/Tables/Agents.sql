﻿CREATE TABLE [dbo].[Agents] (
    [AgentID]              INT           IDENTITY (1, 1) NOT NULL,
    [AgentLevel]           BIT           NOT NULL,
    [SubordinateToAgentID] INT           NULL,
    [LastName]             NVARCHAR (50) NOT NULL,
    [FirstName]            NVARCHAR (50) NOT NULL,
    [IdNumber]             NVARCHAR (50) NULL,
    [BirthDate]            DATE          NULL,
    [City]                 NVARCHAR (50) NULL,
    [Street]               NVARCHAR (50) NULL,
    [HomeNumber]           NVARCHAR (10) NULL,
    [ApartmentNumber]      NVARCHAR (10) NULL,
    [ZipCode]              NVARCHAR (10) NULL,
    [HomePhone]            NVARCHAR (50) NULL,
    [CellPhone]            NVARCHAR (50) NULL,
    [WorkPhone]            NVARCHAR (50) NULL,
    [Fax]                  NVARCHAR (50) NULL,
    [Email]                NVARCHAR (70) NULL,
    [AgencyName]           NVARCHAR (50) NULL,
    [Status]               BIT           NULL,
    CONSTRAINT [PK_Agents] PRIMARY KEY CLUSTERED ([AgentID] ASC),
    CONSTRAINT [FK_Agents_Agents] FOREIGN KEY ([SubordinateToAgentID]) REFERENCES [dbo].[Agents] ([AgentID])
);

