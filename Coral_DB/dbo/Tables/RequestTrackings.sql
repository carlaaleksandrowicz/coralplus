﻿CREATE TABLE [dbo].[RequestTrackings] (
    [RequestTrackingID] INT            IDENTITY (1, 1) NOT NULL,
    [RequestID]         INT            NOT NULL,
    [UserID]            INT            NOT NULL,
    [Date]              DATETIME       NULL,
    [TrackingContent]   NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_RequestTrackings] PRIMARY KEY CLUSTERED ([RequestTrackingID] ASC),
    CONSTRAINT [FK_RequestTrackings_Requests] FOREIGN KEY ([RequestID]) REFERENCES [dbo].[Requests] ([RequestID]),
    CONSTRAINT [FK_RequestTrackings_Users] FOREIGN KEY ([UserID]) REFERENCES [dbo].[Users] ([UserID])
);

