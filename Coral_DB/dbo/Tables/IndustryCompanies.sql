﻿CREATE TABLE [dbo].[IndustryCompanies] (
    [InsuranceIndustryID] INT NOT NULL,
    [CompanyID]           INT NOT NULL,
    CONSTRAINT [PK_IndustryCompanies] PRIMARY KEY CLUSTERED ([InsuranceIndustryID] ASC, [CompanyID] ASC),
    CONSTRAINT [FK_IndustryCompanies_Companies] FOREIGN KEY ([CompanyID]) REFERENCES [dbo].[Companies] ([CompanyID]),
    CONSTRAINT [FK_IndustryCompanies_InsuranceIndustries] FOREIGN KEY ([InsuranceIndustryID]) REFERENCES [dbo].[InsuranceIndustries] ([InsuranceIndustryID])
);

