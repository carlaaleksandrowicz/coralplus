﻿CREATE TABLE [dbo].[ForeingWorkersCoverages] (
    [ForeingWorkersCoverageID]     INT            IDENTITY (1, 1) NOT NULL,
    [ForeingWorkersPolicyID]       INT            NOT NULL,
    [ForeingWorkersCoverageTypeID] INT            NOT NULL,
    [CoverageAmount]               MONEY          NULL,
    [Porcentage]                   DECIMAL (18)   NULL,
    [premium]                      MONEY          NULL,
    [Comments]                     NVARCHAR (MAX) NULL,
    [Date]                         DATETIME       NULL,
    [Addition]                     INT            NULL,
    CONSTRAINT [PK_ForeingWorkersCoverages] PRIMARY KEY CLUSTERED ([ForeingWorkersCoverageID] ASC),
    CONSTRAINT [FK_ForeingWorkersCoverages_ForeingWorkersCoverageTypes] FOREIGN KEY ([ForeingWorkersCoverageTypeID]) REFERENCES [dbo].[ForeingWorkersCoverageTypes] ([ForeingWorkersCoverageTypeID]),
    CONSTRAINT [FK_ForeingWorkersCoverages_ForeingWorkersPolicies] FOREIGN KEY ([ForeingWorkersPolicyID]) REFERENCES [dbo].[ForeingWorkersPolicies] ([ForeingWorkersPolicyID])
);

