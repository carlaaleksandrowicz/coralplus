﻿CREATE TABLE [dbo].[PersonalAccidentsClaimTrackings] (
    [PersonalAccidentsClaimTrackingID] INT            IDENTITY (1, 1) NOT NULL,
    [PersonalAccidentsClaimID]         INT            NOT NULL,
    [UserID]                           INT            NULL,
    [Date]                             DATETIME       NULL,
    [TrackingContent]                  NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_PersonalAccidentsClaimTrackings] PRIMARY KEY CLUSTERED ([PersonalAccidentsClaimTrackingID] ASC),
    CONSTRAINT [FK_PersonalAccidentsClaimTrackings_PersonalAccidentsClaims] FOREIGN KEY ([PersonalAccidentsClaimID]) REFERENCES [dbo].[PersonalAccidentsClaims] ([PersonalAccidentsClaimID]),
    CONSTRAINT [FK_PersonalAccidentsClaimTrackings_Users] FOREIGN KEY ([UserID]) REFERENCES [dbo].[Users] ([UserID])
);

