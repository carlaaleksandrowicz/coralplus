﻿CREATE TABLE [dbo].[HealthClaimConditions] (
    [HealthClaimConditionID] INT           IDENTITY (1, 1) NOT NULL,
    [Description]            NVARCHAR (50) NULL,
    [Status]                 BIT           NULL,
    CONSTRAINT [PK_HealthClaim] PRIMARY KEY CLUSTERED ([HealthClaimConditionID] ASC)
);

