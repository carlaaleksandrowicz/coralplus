﻿CREATE TABLE [dbo].[HealthClaims] (
    [HealthClaimID]          INT            IDENTITY (1, 1) NOT NULL,
    [HealthPolicyID]         INT            NOT NULL,
    [HealthClaimTypeID]      INT            NULL,
    [HealthClaimConditionID] INT            NULL,
    [ClaimStatus]            BIT            NULL,
    [ClaimNumber]            NVARCHAR (50)  NULL,
    [OpenDate]               DATE           NULL,
    [DeliveredToCompanyDate] DATE           NULL,
    [MoneyReceivedDate]      DATE           NULL,
    [ClaimAmount]            MONEY          NULL,
    [AmountReceived]         MONEY          NULL,
    [EventDateAndTime]       DATETIME       NULL,
    [EventPlace]             NVARCHAR (MAX) NULL,
    [EventDescription]       NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_HealthClaims] PRIMARY KEY CLUSTERED ([HealthClaimID] ASC),
    CONSTRAINT [FK_HealthClaims_HealthClaimConditions] FOREIGN KEY ([HealthClaimConditionID]) REFERENCES [dbo].[HealthClaimConditions] ([HealthClaimConditionID]),
    CONSTRAINT [FK_HealthClaims_HealthClaimTypes] FOREIGN KEY ([HealthClaimTypeID]) REFERENCES [dbo].[HealthClaimTypes] ([HealthClaimTypeID]),
    CONSTRAINT [FK_HealthClaims_HealthPolicies] FOREIGN KEY ([HealthPolicyID]) REFERENCES [dbo].[HealthPolicies] ([HealthPolicyID])
);

