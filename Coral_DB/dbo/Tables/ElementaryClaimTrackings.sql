﻿CREATE TABLE [dbo].[ElementaryClaimTrackings] (
    [ElementaryClaimTrackingID] INT            IDENTITY (1, 1) NOT NULL,
    [ElementaryClaimID]         INT            NOT NULL,
    [UserID]                    INT            NULL,
    [Date]                      DATETIME       NULL,
    [TrackingContent]           NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_ClaimTrackings] PRIMARY KEY CLUSTERED ([ElementaryClaimTrackingID] ASC),
    CONSTRAINT [FK_ElementaryClaimTrackings_ElementaryClaims] FOREIGN KEY ([ElementaryClaimID]) REFERENCES [dbo].[ElementaryClaims] ([ElementaryClaimID]),
    CONSTRAINT [FK_ElementaryClaimTrackings_Users] FOREIGN KEY ([UserID]) REFERENCES [dbo].[Users] ([UserID])
);

