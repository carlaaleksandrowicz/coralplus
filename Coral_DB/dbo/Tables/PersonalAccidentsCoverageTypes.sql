﻿CREATE TABLE [dbo].[PersonalAccidentsCoverageTypes] (
    [PersonalAccidentsCoverageTypeID]   INT           IDENTITY (1, 1) NOT NULL,
    [PersonalAccidentsCoverageCode]     NVARCHAR (50) NULL,
    [PersonalAccidentsCoverageTypeName] NVARCHAR (70) NULL,
    [Status]                            BIT           NULL,
    CONSTRAINT [PK_PersonalAccidentsCoverageTypes] PRIMARY KEY CLUSTERED ([PersonalAccidentsCoverageTypeID] ASC)
);

