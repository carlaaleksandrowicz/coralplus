﻿CREATE TABLE [dbo].[ThirdPartyInsuranceTypes] (
    [ThirdPartyInsuranceTypeID]   INT           IDENTITY (1, 1) NOT NULL,
    [ThirdPartyInsuranceTypeName] NVARCHAR (50) NOT NULL,
    [Status]                      BIT           NULL,
    CONSTRAINT [PK_ThirdPartyInsuranceTypes] PRIMARY KEY CLUSTERED ([ThirdPartyInsuranceTypeID] ASC)
);

