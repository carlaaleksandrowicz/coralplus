﻿CREATE TABLE [dbo].[TravelInsuranceTypes] (
    [TravelInsuranceTypeID]      INT           IDENTITY (1, 1) NOT NULL,
    [CompanyID]                  INT           NOT NULL,
    [TravelInsuranceTypeName]    NVARCHAR (50) NOT NULL,
    [IsWinterSport]              BIT           NULL,
    [IsExtremeSport]             BIT           NULL,
    [IsSearchAndRescue]          BIT           NULL,
    [IsPregnancy]                BIT           NULL,
    [IsExistingMedicalCondition] BIT           NULL,
    [IsBaggage]                  BIT           NULL,
    [IsComputerOrTablet]         BIT           NULL,
    [IsPhoneOrGPS]               BIT           NULL,
    [IsBicycle]                  BIT           NULL,
    [OtherName]                  NVARCHAR (50) NULL,
    [IsOther]                    BIT           NULL,
    [Status]                     BIT           NULL,
    CONSTRAINT [PK_TravelInsuranceTypes] PRIMARY KEY CLUSTERED ([TravelInsuranceTypeID] ASC),
    CONSTRAINT [FK_TravelInsuranceTypes_Companies] FOREIGN KEY ([CompanyID]) REFERENCES [dbo].[Companies] ([CompanyID]),
    CONSTRAINT [FK_TravelInsuranceTypes_Companies1] FOREIGN KEY ([CompanyID]) REFERENCES [dbo].[Companies] ([CompanyID])
);

