﻿CREATE TABLE [dbo].[LifeTrackings] (
    [LifeTrackingID]  INT            IDENTITY (1, 1) NOT NULL,
    [LifePolicyID]    INT            NOT NULL,
    [UserID]          INT            NULL,
    [Date]            DATETIME       NULL,
    [TrackingContent] NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_LifeTrackings] PRIMARY KEY CLUSTERED ([LifeTrackingID] ASC),
    CONSTRAINT [FK_LifeTrackings_LifePolicies] FOREIGN KEY ([LifePolicyID]) REFERENCES [dbo].[LifePolicies] ([LifePolicyID]),
    CONSTRAINT [FK_LifeTrackings_Users] FOREIGN KEY ([UserID]) REFERENCES [dbo].[Users] ([UserID])
);

