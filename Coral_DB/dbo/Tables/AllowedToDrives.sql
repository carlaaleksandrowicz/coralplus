﻿CREATE TABLE [dbo].[AllowedToDrives] (
    [AllowedToDriveID] INT            IDENTITY (1, 1) NOT NULL,
    [Description]      NVARCHAR (MAX) NOT NULL,
    [Status]           BIT            NULL,
    CONSTRAINT [PK_AllowedToDrives] PRIMARY KEY CLUSTERED ([AllowedToDriveID] ASC)
);

