﻿CREATE TABLE [dbo].[PersonalAccidentsInsuranceTypes] (
    [PersonalAccidentsInsuranceTypeID]     INT           IDENTITY (1, 1) NOT NULL,
    [CompanyID]                            INT           NOT NULL,
    [PersonalAccidentsInsuranceTypeName]   NVARCHAR (50) NOT NULL,
    [Status]                               BIT           NULL,
    [DeathByAccidentAmount]                MONEY         NULL,
    [IncapacityByAccidentAmount]           MONEY         NULL,
    [FracturesAndBurnsByAccidentAmount]    MONEY         NULL,
    [CompensationForHospitalizationAmount] MONEY         NULL,
    [SiudByAccidentAmount]                 MONEY         NULL,
    CONSTRAINT [PK_PersonalAccidentsInsuranceTypes] PRIMARY KEY CLUSTERED ([PersonalAccidentsInsuranceTypeID] ASC),
    CONSTRAINT [FK_PersonalAccidentsInsuranceTypes_Companies] FOREIGN KEY ([CompanyID]) REFERENCES [dbo].[Companies] ([CompanyID])
);

