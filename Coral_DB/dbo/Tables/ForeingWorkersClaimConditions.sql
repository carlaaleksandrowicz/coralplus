﻿CREATE TABLE [dbo].[ForeingWorkersClaimConditions] (
    [ForeingWorkersClaimConditionID] INT           IDENTITY (1, 1) NOT NULL,
    [Description]                    NVARCHAR (50) NULL,
    [Status]                         BIT           NULL,
    CONSTRAINT [PK_ForeingWorkersClaimConditions] PRIMARY KEY CLUSTERED ([ForeingWorkersClaimConditionID] ASC)
);

