﻿CREATE TABLE [dbo].[ForeingWorkersRenewalQuotes] (
    [ForeingWorkersRenewalQuoteID] INT           IDENTITY (1, 1) NOT NULL,
    [ForeingWorkersPolicyID]       INT           NULL,
    [PolicyNumber]                 NVARCHAR (50) NULL,
    [Premium]                      MONEY         NULL,
    [CompanyID]                    INT           NULL,
    CONSTRAINT [PK_ForeingWorkersRenewalQuotes] PRIMARY KEY CLUSTERED ([ForeingWorkersRenewalQuoteID] ASC),
    CONSTRAINT [FK_ForeingWorkersRenewalQuotes_Companies] FOREIGN KEY ([CompanyID]) REFERENCES [dbo].[Companies] ([CompanyID]),
    CONSTRAINT [FK_ForeingWorkersRenewalQuotes_ForeingWorkersPolicies] FOREIGN KEY ([ForeingWorkersPolicyID]) REFERENCES [dbo].[ForeingWorkersPolicies] ([ForeingWorkersPolicyID])
);

