﻿CREATE TABLE [dbo].[HealthClaimTrackings] (
    [HealthClaimTrackingID] INT            IDENTITY (1, 1) NOT NULL,
    [HealthClaimID]         INT            NOT NULL,
    [UserID]                INT            NULL,
    [Date]                  DATETIME       NULL,
    [TrackingContent]       NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_HealthClaimTrackings] PRIMARY KEY CLUSTERED ([HealthClaimTrackingID] ASC),
    CONSTRAINT [FK_HealthClaimTrackings_HealthClaims] FOREIGN KEY ([HealthClaimID]) REFERENCES [dbo].[HealthClaims] ([HealthClaimID]),
    CONSTRAINT [FK_HealthClaimTrackings_Users] FOREIGN KEY ([UserID]) REFERENCES [dbo].[Users] ([UserID])
);

