﻿CREATE TABLE [dbo].[ElementaryWitnesses] (
    [ElementaryWitnessID] INT            IDENTITY (1, 1) NOT NULL,
    [ElementaryClaimID]   INT            NOT NULL,
    [WitnessName]         NVARCHAR (50)  NULL,
    [WittnessAddress]     NVARCHAR (100) NULL,
    [WitnessPhone]        NVARCHAR (20)  NULL,
    [WitnessCellPhone]    NVARCHAR (20)  NULL,
    CONSTRAINT [PK_ElementaryWitnesses] PRIMARY KEY CLUSTERED ([ElementaryWitnessID] ASC),
    CONSTRAINT [FK_ElementaryWitnesses_ElementaryClaims] FOREIGN KEY ([ElementaryClaimID]) REFERENCES [dbo].[ElementaryClaims] ([ElementaryClaimID])
);

