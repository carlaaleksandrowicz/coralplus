﻿CREATE TABLE [dbo].[TravelClaimTrackings] (
    [TravelClaimTrackingID] INT            IDENTITY (1, 1) NOT NULL,
    [TravelClaimID]         INT            NOT NULL,
    [UserID]                INT            NULL,
    [Date]                  DATETIME       NULL,
    [TrackingContent]       NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_TravelClaimTrackings] PRIMARY KEY CLUSTERED ([TravelClaimTrackingID] ASC),
    CONSTRAINT [FK_TravelClaimTrackings_TravelClaims] FOREIGN KEY ([TravelClaimID]) REFERENCES [dbo].[TravelClaims] ([TravelClaimID]),
    CONSTRAINT [FK_TravelClaimTrackings_Users] FOREIGN KEY ([UserID]) REFERENCES [dbo].[Users] ([UserID])
);

