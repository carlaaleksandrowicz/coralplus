﻿CREATE TABLE [dbo].[HealthTrackings] (
    [HealthTrackingID] INT            IDENTITY (1, 1) NOT NULL,
    [HealthPolicyID]   INT            NOT NULL,
    [UserID]           INT            NULL,
    [Date]             DATETIME       NULL,
    [TrackingContent]  NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_HealthTreckings] PRIMARY KEY CLUSTERED ([HealthTrackingID] ASC),
    CONSTRAINT [FK_HealthTrackings_HealthPolicies] FOREIGN KEY ([HealthPolicyID]) REFERENCES [dbo].[HealthPolicies] ([HealthPolicyID]),
    CONSTRAINT [FK_HealthTrackings_Users] FOREIGN KEY ([UserID]) REFERENCES [dbo].[Users] ([UserID])
);

