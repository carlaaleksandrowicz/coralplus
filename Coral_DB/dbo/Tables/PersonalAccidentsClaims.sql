﻿CREATE TABLE [dbo].[PersonalAccidentsClaims] (
    [PersonalAccidentsClaimID]          INT            IDENTITY (1, 1) NOT NULL,
    [PersonalAccidentsPolicyID]         INT            NOT NULL,
    [PersonalAccidentsClaimTypeID]      INT            NULL,
    [PersonalAccidentsClaimConditionID] INT            NULL,
    [ClaimStatus]                       BIT            NULL,
    [ClaimNumber]                       NVARCHAR (50)  NULL,
    [OpenDate]                          DATE           NULL,
    [DeliveredToCompanyDate]            DATE           NULL,
    [MoneyReceivedDate]                 DATE           NULL,
    [ClaimAmount]                       MONEY          NULL,
    [AmountReceived]                    MONEY          NULL,
    [EventDateAndTime]                  DATETIME       NULL,
    [EventPlace]                        NVARCHAR (MAX) NULL,
    [EventDescription]                  NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_PersonalAccidentsClaims] PRIMARY KEY CLUSTERED ([PersonalAccidentsClaimID] ASC),
    CONSTRAINT [FK_PersonalAccidentsClaims_PersonalAccidentsClaimConditions] FOREIGN KEY ([PersonalAccidentsClaimConditionID]) REFERENCES [dbo].[PersonalAccidentsClaimConditions] ([PersonalAccidentsClaimConditionID]),
    CONSTRAINT [FK_PersonalAccidentsClaims_PersonalAccidentsClaimTypes] FOREIGN KEY ([PersonalAccidentsClaimTypeID]) REFERENCES [dbo].[PersonalAccidentsClaimTypes] ([PersonalAccidentsClaimTypeID]),
    CONSTRAINT [FK_PersonalAccidentsClaims_PersonalAccidentsPolicies] FOREIGN KEY ([PersonalAccidentsPolicyID]) REFERENCES [dbo].[PersonalAccidentsPolicies] ([PersonalAccidentsPolicyID])
);

