﻿CREATE TABLE [dbo].[FundJoiningPlans] (
    [FundJoiningPlanID]   INT           IDENTITY (1, 1) NOT NULL,
    [FundJoiningPlanName] NVARCHAR (50) NULL,
    [Status]              BIT           NULL,
    CONSTRAINT [PK_FundJoiningPlans] PRIMARY KEY CLUSTERED ([FundJoiningPlanID] ASC)
);

