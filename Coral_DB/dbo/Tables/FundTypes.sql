﻿CREATE TABLE [dbo].[FundTypes] (
    [FundTypeID]     INT           IDENTITY (1, 1) NOT NULL,
    [InsuranceID]    INT           NOT NULL,
    [LifeIndustryID] INT           NOT NULL,
    [CompanyID]      INT           NOT NULL,
    [FundTypeName]   NVARCHAR (50) NOT NULL,
    [Status]         BIT           NULL,
    CONSTRAINT [PK_FundTypes] PRIMARY KEY CLUSTERED ([FundTypeID] ASC),
    CONSTRAINT [FK_FundTypes_Companies] FOREIGN KEY ([CompanyID]) REFERENCES [dbo].[Companies] ([CompanyID]),
    CONSTRAINT [FK_FundTypes_Insurances] FOREIGN KEY ([InsuranceID]) REFERENCES [dbo].[Insurances] ([InsuranceID])
);

