﻿CREATE TABLE [dbo].[PersonalAccidentsClaimConditions] (
    [PersonalAccidentsClaimConditionID] INT           IDENTITY (1, 1) NOT NULL,
    [Description]                       NVARCHAR (50) NULL,
    [Status]                            BIT           NULL,
    CONSTRAINT [PK_PersonalAccidentsClaimConditions] PRIMARY KEY CLUSTERED ([PersonalAccidentsClaimConditionID] ASC)
);

