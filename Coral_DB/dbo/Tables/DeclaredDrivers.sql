﻿CREATE TABLE [dbo].[DeclaredDrivers] (
    [DeclaredDriverID]   INT            IDENTITY (1, 1) NOT NULL,
    [ElementaryPolicyID] INT            NOT NULL,
    [DriverLastName]     NVARCHAR (50)  NULL,
    [DriverFirstName]    NVARCHAR (50)  NULL,
    [DriverIdNumber]     NVARCHAR (50)  NULL,
    [BirthDate]          DATE           NULL,
    [LicenseNumber]      NVARCHAR (10)  NULL,
    [LicenseDate]        DATE           NULL,
    [Remarks]            NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_DeclaredDrivers] PRIMARY KEY CLUSTERED ([DeclaredDriverID] ASC),
    CONSTRAINT [FK_DeclaredDrivers_CarPolicies] FOREIGN KEY ([ElementaryPolicyID]) REFERENCES [dbo].[CarPolicies] ([ElementaryPolicyID])
);

