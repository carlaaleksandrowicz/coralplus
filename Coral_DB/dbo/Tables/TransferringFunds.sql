﻿CREATE TABLE [dbo].[TransferringFunds] (
    [TransferringFundID]   INT           IDENTITY (1, 1) NOT NULL,
    [TransferringFundName] NVARCHAR (50) NULL,
    [Status]               BIT           NULL,
    CONSTRAINT [PK_TransferringFunds] PRIMARY KEY CLUSTERED ([TransferringFundID] ASC)
);

