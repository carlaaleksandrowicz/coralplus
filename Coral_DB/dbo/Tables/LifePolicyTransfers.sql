﻿CREATE TABLE [dbo].[LifePolicyTransfers] (
    [LifePolicyTransferID]   INT      IDENTITY (1, 1) NOT NULL,
    [LifePolicyID]           INT      NOT NULL,
    [TransferringCompanyID]  INT      NULL,
    [FundTypeID]             INT      NULL,
    [TransferDate]           DATETIME NULL,
    [TransferAmount]         MONEY    NULL,
    [TransferedToCompanyID]  INT      NULL,
    [TransferedToFundTypeID] INT      NULL,
    CONSTRAINT [PK_LifePolicyTransfers] PRIMARY KEY CLUSTERED ([LifePolicyTransferID] ASC),
    CONSTRAINT [FK_LifePolicyTransfers_Companies] FOREIGN KEY ([TransferringCompanyID]) REFERENCES [dbo].[Companies] ([CompanyID]),
    CONSTRAINT [FK_LifePolicyTransfers_Companies1] FOREIGN KEY ([TransferedToCompanyID]) REFERENCES [dbo].[Companies] ([CompanyID]),
    CONSTRAINT [FK_LifePolicyTransfers_FundTypes] FOREIGN KEY ([FundTypeID]) REFERENCES [dbo].[FundTypes] ([FundTypeID]),
    CONSTRAINT [FK_LifePolicyTransfers_FundTypes1] FOREIGN KEY ([TransferedToFundTypeID]) REFERENCES [dbo].[FundTypes] ([FundTypeID]),
    CONSTRAINT [FK_LifePolicyTransfers_LifePolicies] FOREIGN KEY ([LifePolicyID]) REFERENCES [dbo].[LifePolicies] ([LifePolicyID])
);

