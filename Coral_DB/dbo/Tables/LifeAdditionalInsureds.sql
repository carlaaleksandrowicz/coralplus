﻿CREATE TABLE [dbo].[LifeAdditionalInsureds] (
    [LifeAdditionalInsuredID]          INT           IDENTITY (1, 1) NOT NULL,
    [LifePolicyID]                     INT           NOT NULL,
    [FirstName]                        NVARCHAR (50) NULL,
    [LastName]                         NVARCHAR (50) NULL,
    [IdNumber]                         NVARCHAR (50) NULL,
    [BirthDate]                        DATETIME      NULL,
    [RelationshipWithPrincipalInsured] NVARCHAR (50) NULL,
    CONSTRAINT [PK_LifeAdditionalInsureds] PRIMARY KEY CLUSTERED ([LifeAdditionalInsuredID] ASC),
    CONSTRAINT [FK_LifeAdditionalInsureds_LifeAdditionalInsureds] FOREIGN KEY ([LifePolicyID]) REFERENCES [dbo].[LifePolicies] ([LifePolicyID])
);

