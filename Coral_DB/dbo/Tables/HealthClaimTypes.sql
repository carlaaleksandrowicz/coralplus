﻿CREATE TABLE [dbo].[HealthClaimTypes] (
    [HealthClaimTypeID] INT           IDENTITY (1, 1) NOT NULL,
    [ClaimTypeName]     NVARCHAR (50) NULL,
    [Status]            BIT           NULL,
    CONSTRAINT [PK_HealthClaimTypes] PRIMARY KEY CLUSTERED ([HealthClaimTypeID] ASC)
);

