﻿CREATE TABLE [dbo].[ForeingWorkersIndustries] (
    [ForeingWorkersIndustryID]   INT           IDENTITY (1, 1) NOT NULL,
    [ForeingWorkersIndustryName] NVARCHAR (50) NOT NULL,
    CONSTRAINT [PK_ForeingWorkersIndustries] PRIMARY KEY CLUSTERED ([ForeingWorkersIndustryID] ASC)
);

