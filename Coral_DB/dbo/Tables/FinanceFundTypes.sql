﻿CREATE TABLE [dbo].[FinanceFundTypes] (
    [FinanceFundTypeID] INT           IDENTITY (1, 1) NOT NULL,
    [ProgramTypeID]     INT           NOT NULL,
    [CompanyID]         INT           NOT NULL,
    [FundTypeName]      NVARCHAR (50) NOT NULL,
    [Status]            BIT           NULL,
    CONSTRAINT [PK_FinanceFundTypes] PRIMARY KEY CLUSTERED ([FinanceFundTypeID] ASC)
);

