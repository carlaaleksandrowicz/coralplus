﻿CREATE TABLE [dbo].[ForeingWorkersCoverageTypes] (
    [ForeingWorkersCoverageTypeID]   INT           IDENTITY (1, 1) NOT NULL,
    [ForeingWorkersCoverageCode]     NVARCHAR (50) NULL,
    [ForeingWorkersCoverageTypeName] NVARCHAR (70) NULL,
    [Status]                         BIT           NULL,
    CONSTRAINT [PK_ForeingWorkersCoverageTypes] PRIMARY KEY CLUSTERED ([ForeingWorkersCoverageTypeID] ASC)
);

