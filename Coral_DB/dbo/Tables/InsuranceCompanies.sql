﻿CREATE TABLE [dbo].[InsuranceCompanies] (
    [InsuranceID] INT NOT NULL,
    [CompanyID]   INT NOT NULL,
    [Status]      BIT NULL,
    CONSTRAINT [PK_InsuranceCompanies_1] PRIMARY KEY CLUSTERED ([InsuranceID] ASC, [CompanyID] ASC),
    CONSTRAINT [FK_InsuranceCompanies_Companies] FOREIGN KEY ([CompanyID]) REFERENCES [dbo].[Companies] ([CompanyID]),
    CONSTRAINT [FK_InsuranceCompanies_Insurances] FOREIGN KEY ([InsuranceID]) REFERENCES [dbo].[Insurances] ([InsuranceID])
);

