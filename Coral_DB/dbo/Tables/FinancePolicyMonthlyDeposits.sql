﻿CREATE TABLE [dbo].[FinancePolicyMonthlyDeposits] (
    [FinancePolicyMonthlyDepositID] INT            IDENTITY (1, 1) NOT NULL,
    [FinancePolicyID]               INT            NULL,
    [DepositDate]                   DATETIME       NULL,
    [DepositForTheMonth]            DATETIME       NULL,
    [DepositAmount]                 MONEY          NULL,
    [TransferredTo]                 NVARCHAR (50)  NULL,
    [Comments]                      NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_FinancePolicyMonthlyDeposits] PRIMARY KEY CLUSTERED ([FinancePolicyMonthlyDepositID] ASC),
    CONSTRAINT [FK_FinancePolicyMonthlyDeposits_FinancePolicies] FOREIGN KEY ([FinancePolicyID]) REFERENCES [dbo].[FinancePolicies] ([FinancePolicyID])
);

