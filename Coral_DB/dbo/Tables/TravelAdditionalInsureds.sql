﻿CREATE TABLE [dbo].[TravelAdditionalInsureds] (
    [TavelAdditionalInsuredID] INT           IDENTITY (1, 1) NOT NULL,
    [TravelPolicyID]           INT           NOT NULL,
    [LastName]                 NVARCHAR (50) NULL,
    [FirstName]                NVARCHAR (50) NULL,
    [IdNumber]                 NVARCHAR (50) NULL,
    [BirthDate]                DATETIME      NULL,
    [Profession]               NVARCHAR (50) NULL,
    [IsMale]                   BIT           NULL,
    [MaritalStatus]            NVARCHAR (50) NULL,
    [Relationship]             NVARCHAR (50) NULL,
    CONSTRAINT [PK_TravelAdditionalInsureds] PRIMARY KEY CLUSTERED ([TavelAdditionalInsuredID] ASC),
    CONSTRAINT [FK_TravelAdditionalInsureds_TravelPolicies] FOREIGN KEY ([TravelPolicyID]) REFERENCES [dbo].[TravelPolicies] ([TravelPolicyID])
);

