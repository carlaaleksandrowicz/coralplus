﻿CREATE TABLE [dbo].[LifeCoverageTypes] (
    [LifeCoverageTypeID]   INT           IDENTITY (1, 1) NOT NULL,
    [LifeIndustryID]       INT           NOT NULL,
    [LifeCoverageCode]     NVARCHAR (50) NULL,
    [LifeCoverageTypeName] NVARCHAR (70) NULL,
    [Status]               BIT           NULL,
    CONSTRAINT [PK_LifeCoverageTypes] PRIMARY KEY CLUSTERED ([LifeCoverageTypeID] ASC),
    CONSTRAINT [FK_LifeCoverageTypes_LifeIndustries] FOREIGN KEY ([LifeIndustryID]) REFERENCES [dbo].[LifeIndustries] ([LifeIndustryID])
);

