﻿CREATE TABLE [dbo].[ApartmentRenewalQuotes] (
    [ApartmentRenewalQuoteID] INT           IDENTITY (1, 1) NOT NULL,
    [ElementaryPolicyID]      INT           NULL,
    [PolicyNumber]            NVARCHAR (50) NULL,
    [Premium]                 MONEY         NULL,
    [CompanyID]               INT           NULL,
    CONSTRAINT [PK_ApartmentRenewalQuotes] PRIMARY KEY CLUSTERED ([ApartmentRenewalQuoteID] ASC),
    CONSTRAINT [FK_ApartmentRenewalQuotes_Companies] FOREIGN KEY ([CompanyID]) REFERENCES [dbo].[Companies] ([CompanyID]),
    CONSTRAINT [FK_ApartmentRenewalQuotes_ElementaryPolicies] FOREIGN KEY ([ElementaryPolicyID]) REFERENCES [dbo].[ElementaryPolicies] ([ElementaryPolicyID])
);

