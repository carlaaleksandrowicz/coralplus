﻿CREATE TABLE [dbo].[TravelCoverageTypes] (
    [TravelCoverageTypeID]   INT           IDENTITY (1, 1) NOT NULL,
    [TravelCoverageCode]     NVARCHAR (50) NULL,
    [TravelCoverageTypeName] NVARCHAR (70) NULL,
    [Status]                 BIT           NULL,
    CONSTRAINT [PK_TravelCoverageTypes] PRIMARY KEY CLUSTERED ([TravelCoverageTypeID] ASC)
);

