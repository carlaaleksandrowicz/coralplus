﻿CREATE TABLE [dbo].[TravelWitnesses] (
    [TravelWitnessID]  INT            IDENTITY (1, 1) NOT NULL,
    [TravelClaimID]    INT            NOT NULL,
    [WitnessName]      NVARCHAR (50)  NULL,
    [WittnessAddress]  NVARCHAR (100) NULL,
    [WitnessPhone]     NVARCHAR (20)  NULL,
    [WitnessCellPhone] NVARCHAR (20)  NULL,
    CONSTRAINT [PK_TravelWitnesses] PRIMARY KEY CLUSTERED ([TravelWitnessID] ASC),
    CONSTRAINT [FK_TravelWitnesses_TravelClaims] FOREIGN KEY ([TravelClaimID]) REFERENCES [dbo].[TravelClaims] ([TravelClaimID])
);

