﻿CREATE TABLE [dbo].[InsurancesWaivers] (
    [InsurancesWaiverID]   INT           IDENTITY (1, 1) NOT NULL,
    [InsurancesWaiverName] NVARCHAR (50) NULL,
    [Status]               BIT           NULL,
    CONSTRAINT [PK_InsurancesWaivers] PRIMARY KEY CLUSTERED ([InsurancesWaiverID] ASC)
);

